﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RedBloodCell
struct RedBloodCell_t2376799177;
// CellMovement
struct CellMovement_t2110243311;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CellMovement2110243311.h"

// System.Void RedBloodCell::.ctor()
extern "C"  void RedBloodCell__ctor_m2035063800 (RedBloodCell_t2376799177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CellMovement RedBloodCell::get_move()
extern "C"  CellMovement_t2110243311 * RedBloodCell_get_move_m849695286 (RedBloodCell_t2376799177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RedBloodCell::set_move(CellMovement)
extern "C"  void RedBloodCell_set_move_m1320209217 (RedBloodCell_t2376799177 * __this, CellMovement_t2110243311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RedBloodCell::get_radius()
extern "C"  float RedBloodCell_get_radius_m195900731 (RedBloodCell_t2376799177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RedBloodCell::set_radius(System.Single)
extern "C"  void RedBloodCell_set_radius_m2966666374 (RedBloodCell_t2376799177 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RedBloodCell::Awake()
extern "C"  void RedBloodCell_Awake_m2661662197 (RedBloodCell_t2376799177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RedBloodCell::Start()
extern "C"  void RedBloodCell_Start_m1748079816 (RedBloodCell_t2376799177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RedBloodCell::LateUpdate()
extern "C"  void RedBloodCell_LateUpdate_m1498019821 (RedBloodCell_t2376799177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
