﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>
struct ValueCollection_t3403849956;
// System.Collections.Generic.Dictionary`2<HIV/State,System.Object>
struct Dictionary_2_t405822817;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2092355581.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3675885603_gshared (ValueCollection_t3403849956 * __this, Dictionary_2_t405822817 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3675885603(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3403849956 *, Dictionary_2_t405822817 *, const MethodInfo*))ValueCollection__ctor_m3675885603_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1020789705_gshared (ValueCollection_t3403849956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1020789705(__this, ___item0, method) ((  void (*) (ValueCollection_t3403849956 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1020789705_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1663196884_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1663196884(__this, method) ((  void (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1663196884_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2901311439_gshared (ValueCollection_t3403849956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2901311439(__this, ___item0, method) ((  bool (*) (ValueCollection_t3403849956 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2901311439_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1767065214_gshared (ValueCollection_t3403849956 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1767065214(__this, ___item0, method) ((  bool (*) (ValueCollection_t3403849956 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1767065214_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1985508632_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1985508632(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1985508632_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1695589528_gshared (ValueCollection_t3403849956 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1695589528(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3403849956 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1695589528_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062407325_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062407325(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062407325_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2696719474_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2696719474(__this, method) ((  bool (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2696719474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1231113640_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1231113640(__this, method) ((  bool (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1231113640_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m4181673192_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4181673192(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4181673192_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3657206804_gshared (ValueCollection_t3403849956 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3657206804(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3403849956 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3657206804_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2092355581  ValueCollection_GetEnumerator_m288204367_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m288204367(__this, method) ((  Enumerator_t2092355581  (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_GetEnumerator_m288204367_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1654729584_gshared (ValueCollection_t3403849956 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1654729584(__this, method) ((  int32_t (*) (ValueCollection_t3403849956 *, const MethodInfo*))ValueCollection_get_Count_m1654729584_gshared)(__this, method)
