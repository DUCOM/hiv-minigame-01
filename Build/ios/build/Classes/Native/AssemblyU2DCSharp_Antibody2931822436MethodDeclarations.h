﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Antibody
struct Antibody_t2931822436;
// HIV
struct HIV_t2481767745;
// AntibodyMove
struct AntibodyMove_t4173900141;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"
#include "AssemblyU2DCSharp_HIV2481767745.h"

// System.Void Antibody::.ctor()
extern "C"  void Antibody__ctor_m2809393157 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Antibody/State Antibody::get_state()
extern "C"  int32_t Antibody_get_state_m1127204826 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::set_state(Antibody/State)
extern "C"  void Antibody_set_state_m289699289 (Antibody_t2931822436 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV Antibody::get_target()
extern "C"  HIV_t2481767745 * Antibody_get_target_m885629897 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::set_target(HIV)
extern "C"  void Antibody_set_target_m1658253370 (Antibody_t2931822436 * __this, HIV_t2481767745 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AntibodyMove Antibody::get_move()
extern "C"  AntibodyMove_t4173900141 * Antibody_get_move_m3645813473 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::ChangeState(Antibody/State,HIV)
extern "C"  void Antibody_ChangeState_m1054492657 (Antibody_t2931822436 * __this, int32_t ___s0, HIV_t2481767745 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::Awake()
extern "C"  void Antibody_Awake_m2182685072 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::Start()
extern "C"  void Antibody_Start_m2603722309 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::Update()
extern "C"  void Antibody_Update_m1698463694 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Antibody::OnEnable()
extern "C"  void Antibody_OnEnable_m63630501 (Antibody_t2931822436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
