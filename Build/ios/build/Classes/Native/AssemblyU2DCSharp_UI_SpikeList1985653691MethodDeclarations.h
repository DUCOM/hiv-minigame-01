﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_SpikeList
struct UI_SpikeList_t1985653691;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163.h"

// System.Void UI_SpikeList::.ctor()
extern "C"  void UI_SpikeList__ctor_m908666082 (UI_SpikeList_t1985653691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpikeColor UI_SpikeList::get_colors()
extern "C"  SpikeColor_t3573234163  UI_SpikeList_get_colors_m4254507447 (UI_SpikeList_t1985653691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_SpikeList::set_colors(SpikeColor)
extern "C"  void UI_SpikeList_set_colors_m3164100610 (UI_SpikeList_t1985653691 * __this, SpikeColor_t3573234163  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
