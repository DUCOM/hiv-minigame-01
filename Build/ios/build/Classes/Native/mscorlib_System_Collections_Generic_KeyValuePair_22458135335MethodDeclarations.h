﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22458135335.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1362833532_gshared (KeyValuePair_2_t2458135335 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1362833532(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2458135335 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1362833532_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3344155018_gshared (KeyValuePair_2_t2458135335 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3344155018(__this, method) ((  int32_t (*) (KeyValuePair_2_t2458135335 *, const MethodInfo*))KeyValuePair_2_get_Key_m3344155018_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1343766057_gshared (KeyValuePair_2_t2458135335 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1343766057(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2458135335 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1343766057_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3737669834_gshared (KeyValuePair_2_t2458135335 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3737669834(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2458135335 *, const MethodInfo*))KeyValuePair_2_get_Value_m3737669834_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m97232737_gshared (KeyValuePair_2_t2458135335 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m97232737(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2458135335 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m97232737_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4126039479_gshared (KeyValuePair_2_t2458135335 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4126039479(__this, method) ((  String_t* (*) (KeyValuePair_2_t2458135335 *, const MethodInfo*))KeyValuePair_2_ToString_m4126039479_gshared)(__this, method)
