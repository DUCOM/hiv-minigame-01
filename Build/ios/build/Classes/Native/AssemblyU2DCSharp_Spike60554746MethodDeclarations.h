﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spike
struct Spike_t60554746;
// GP120
struct GP120_t1512820336;
// GP41
struct GP41_t921954836;
// Pivot
struct Pivot_t2110476880;
// SpikeInteract
struct SpikeInteract_t2340666440;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// System.Void Spike::.ctor()
extern "C"  void Spike__ctor_m3544368207 (Spike_t60554746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP120 Spike::get_gp120()
extern "C"  GP120_t1512820336 * Spike_get_gp120_m3432987073 (Spike_t60554746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GP41 Spike::get_gp41()
extern "C"  GP41_t921954836 * Spike_get_gp41_m2571159277 (Spike_t60554746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism Spike::get_tropism()
extern "C"  Tropism_t3662836552  Spike_get_tropism_m1909874245 (Spike_t60554746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::set_tropism(Tropism)
extern "C"  void Spike_set_tropism_m2060441560 (Spike_t60554746 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pivot Spike::get_pivot()
extern "C"  Pivot_t2110476880 * Spike_get_pivot_m660032993 (Spike_t60554746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SpikeInteract Spike::get_interaction()
extern "C"  SpikeInteract_t2340666440 * Spike_get_interaction_m2108312269 (Spike_t60554746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::Scale(System.Single)
extern "C"  void Spike_Scale_m3772594916 (Spike_t60554746 * __this, float ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::MoveTo(UnityEngine.Vector3)
extern "C"  void Spike_MoveTo_m4261606434 (Spike_t60554746 * __this, Vector3_t2243707580  ___translation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::MoveAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Spike_MoveAndRotate_m897337456 (Spike_t60554746 * __this, Vector3_t2243707580  ___translation0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::MoveAndRotate(System.Single,UnityEngine.Quaternion)
extern "C"  void Spike_MoveAndRotate_m1100489612 (Spike_t60554746 * __this, float ___radius0, Quaternion_t4030073918  ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::PivotRotation(UnityEngine.Vector3)
extern "C"  void Spike_PivotRotation_m760914936 (Spike_t60554746 * __this, Vector3_t2243707580  ___eulerAngles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spike::PivotRotation(UnityEngine.Quaternion)
extern "C"  void Spike_PivotRotation_m2863376170 (Spike_t60554746 * __this, Quaternion_t4030073918  ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
