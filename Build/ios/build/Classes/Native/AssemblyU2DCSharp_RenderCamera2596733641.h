﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderCamera
struct  RenderCamera_t2596733641  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RenderCamera::zoom
	float ___zoom_4;
	// UnityEngine.Vector3 RenderCamera::difference
	Vector3_t2243707580  ___difference_5;
	// UnityEngine.Camera RenderCamera::cam
	Camera_t189460977 * ___cam_6;

public:
	inline static int32_t get_offset_of_zoom_4() { return static_cast<int32_t>(offsetof(RenderCamera_t2596733641, ___zoom_4)); }
	inline float get_zoom_4() const { return ___zoom_4; }
	inline float* get_address_of_zoom_4() { return &___zoom_4; }
	inline void set_zoom_4(float value)
	{
		___zoom_4 = value;
	}

	inline static int32_t get_offset_of_difference_5() { return static_cast<int32_t>(offsetof(RenderCamera_t2596733641, ___difference_5)); }
	inline Vector3_t2243707580  get_difference_5() const { return ___difference_5; }
	inline Vector3_t2243707580 * get_address_of_difference_5() { return &___difference_5; }
	inline void set_difference_5(Vector3_t2243707580  value)
	{
		___difference_5 = value;
	}

	inline static int32_t get_offset_of_cam_6() { return static_cast<int32_t>(offsetof(RenderCamera_t2596733641, ___cam_6)); }
	inline Camera_t189460977 * get_cam_6() const { return ___cam_6; }
	inline Camera_t189460977 ** get_address_of_cam_6() { return &___cam_6; }
	inline void set_cam_6(Camera_t189460977 * value)
	{
		___cam_6 = value;
		Il2CppCodeGenWriteBarrier(&___cam_6, value);
	}
};

struct RenderCamera_t2596733641_StaticFields
{
public:
	// UnityEngine.Camera RenderCamera::main
	Camera_t189460977 * ___main_2;

public:
	inline static int32_t get_offset_of_main_2() { return static_cast<int32_t>(offsetof(RenderCamera_t2596733641_StaticFields, ___main_2)); }
	inline Camera_t189460977 * get_main_2() const { return ___main_2; }
	inline Camera_t189460977 ** get_address_of_main_2() { return &___main_2; }
	inline void set_main_2(Camera_t189460977 * value)
	{
		___main_2 = value;
		Il2CppCodeGenWriteBarrier(&___main_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
