﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell_Receptor_Head
struct  TCell_Receptor_Head_t2847248660  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite TCell_Receptor_Head::CD4PositiveFront
	Sprite_t309593783 * ___CD4PositiveFront_2;
	// UnityEngine.Sprite TCell_Receptor_Head::CD4PositiveBack
	Sprite_t309593783 * ___CD4PositiveBack_3;
	// UnityEngine.Sprite TCell_Receptor_Head::CD4NegativeFront
	Sprite_t309593783 * ___CD4NegativeFront_4;
	// UnityEngine.Sprite TCell_Receptor_Head::CD4NegativeBack
	Sprite_t309593783 * ___CD4NegativeBack_5;
	// UnityEngine.GameObject TCell_Receptor_Head::HeadDisplayFront
	GameObject_t1756533147 * ___HeadDisplayFront_6;
	// UnityEngine.GameObject TCell_Receptor_Head::HeadDisplayBack
	GameObject_t1756533147 * ___HeadDisplayBack_7;

public:
	inline static int32_t get_offset_of_CD4PositiveFront_2() { return static_cast<int32_t>(offsetof(TCell_Receptor_Head_t2847248660, ___CD4PositiveFront_2)); }
	inline Sprite_t309593783 * get_CD4PositiveFront_2() const { return ___CD4PositiveFront_2; }
	inline Sprite_t309593783 ** get_address_of_CD4PositiveFront_2() { return &___CD4PositiveFront_2; }
	inline void set_CD4PositiveFront_2(Sprite_t309593783 * value)
	{
		___CD4PositiveFront_2 = value;
		Il2CppCodeGenWriteBarrier(&___CD4PositiveFront_2, value);
	}

	inline static int32_t get_offset_of_CD4PositiveBack_3() { return static_cast<int32_t>(offsetof(TCell_Receptor_Head_t2847248660, ___CD4PositiveBack_3)); }
	inline Sprite_t309593783 * get_CD4PositiveBack_3() const { return ___CD4PositiveBack_3; }
	inline Sprite_t309593783 ** get_address_of_CD4PositiveBack_3() { return &___CD4PositiveBack_3; }
	inline void set_CD4PositiveBack_3(Sprite_t309593783 * value)
	{
		___CD4PositiveBack_3 = value;
		Il2CppCodeGenWriteBarrier(&___CD4PositiveBack_3, value);
	}

	inline static int32_t get_offset_of_CD4NegativeFront_4() { return static_cast<int32_t>(offsetof(TCell_Receptor_Head_t2847248660, ___CD4NegativeFront_4)); }
	inline Sprite_t309593783 * get_CD4NegativeFront_4() const { return ___CD4NegativeFront_4; }
	inline Sprite_t309593783 ** get_address_of_CD4NegativeFront_4() { return &___CD4NegativeFront_4; }
	inline void set_CD4NegativeFront_4(Sprite_t309593783 * value)
	{
		___CD4NegativeFront_4 = value;
		Il2CppCodeGenWriteBarrier(&___CD4NegativeFront_4, value);
	}

	inline static int32_t get_offset_of_CD4NegativeBack_5() { return static_cast<int32_t>(offsetof(TCell_Receptor_Head_t2847248660, ___CD4NegativeBack_5)); }
	inline Sprite_t309593783 * get_CD4NegativeBack_5() const { return ___CD4NegativeBack_5; }
	inline Sprite_t309593783 ** get_address_of_CD4NegativeBack_5() { return &___CD4NegativeBack_5; }
	inline void set_CD4NegativeBack_5(Sprite_t309593783 * value)
	{
		___CD4NegativeBack_5 = value;
		Il2CppCodeGenWriteBarrier(&___CD4NegativeBack_5, value);
	}

	inline static int32_t get_offset_of_HeadDisplayFront_6() { return static_cast<int32_t>(offsetof(TCell_Receptor_Head_t2847248660, ___HeadDisplayFront_6)); }
	inline GameObject_t1756533147 * get_HeadDisplayFront_6() const { return ___HeadDisplayFront_6; }
	inline GameObject_t1756533147 ** get_address_of_HeadDisplayFront_6() { return &___HeadDisplayFront_6; }
	inline void set_HeadDisplayFront_6(GameObject_t1756533147 * value)
	{
		___HeadDisplayFront_6 = value;
		Il2CppCodeGenWriteBarrier(&___HeadDisplayFront_6, value);
	}

	inline static int32_t get_offset_of_HeadDisplayBack_7() { return static_cast<int32_t>(offsetof(TCell_Receptor_Head_t2847248660, ___HeadDisplayBack_7)); }
	inline GameObject_t1756533147 * get_HeadDisplayBack_7() const { return ___HeadDisplayBack_7; }
	inline GameObject_t1756533147 ** get_address_of_HeadDisplayBack_7() { return &___HeadDisplayBack_7; }
	inline void set_HeadDisplayBack_7(GameObject_t1756533147 * value)
	{
		___HeadDisplayBack_7 = value;
		Il2CppCodeGenWriteBarrier(&___HeadDisplayBack_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
