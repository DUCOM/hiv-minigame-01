﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreDisplay
struct ScoreDisplay_t164935196;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ScoreDisplay::.ctor()
extern "C"  void ScoreDisplay__ctor_m3717173057 (ScoreDisplay_t164935196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreDisplay::Awake()
extern "C"  void ScoreDisplay_Awake_m3088139788 (ScoreDisplay_t164935196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreDisplay::ShowScore(UnityEngine.Vector2,System.Int32,System.Single,System.String)
extern "C"  void ScoreDisplay_ShowScore_m293627306 (ScoreDisplay_t164935196 * __this, Vector2_t2243707579  ___position0, int32_t ___score1, float ___scale2, String_t* ___previousText3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
