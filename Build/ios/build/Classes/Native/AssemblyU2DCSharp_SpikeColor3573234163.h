﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpikeColor
struct  SpikeColor_t3573234163 
{
public:
	// UnityEngine.Color SpikeColor::gp41
	Color_t2020392075  ___gp41_0;
	// UnityEngine.Color SpikeColor::gp120_left
	Color_t2020392075  ___gp120_left_1;
	// UnityEngine.Color SpikeColor::gp120_right
	Color_t2020392075  ___gp120_right_2;

public:
	inline static int32_t get_offset_of_gp41_0() { return static_cast<int32_t>(offsetof(SpikeColor_t3573234163, ___gp41_0)); }
	inline Color_t2020392075  get_gp41_0() const { return ___gp41_0; }
	inline Color_t2020392075 * get_address_of_gp41_0() { return &___gp41_0; }
	inline void set_gp41_0(Color_t2020392075  value)
	{
		___gp41_0 = value;
	}

	inline static int32_t get_offset_of_gp120_left_1() { return static_cast<int32_t>(offsetof(SpikeColor_t3573234163, ___gp120_left_1)); }
	inline Color_t2020392075  get_gp120_left_1() const { return ___gp120_left_1; }
	inline Color_t2020392075 * get_address_of_gp120_left_1() { return &___gp120_left_1; }
	inline void set_gp120_left_1(Color_t2020392075  value)
	{
		___gp120_left_1 = value;
	}

	inline static int32_t get_offset_of_gp120_right_2() { return static_cast<int32_t>(offsetof(SpikeColor_t3573234163, ___gp120_right_2)); }
	inline Color_t2020392075  get_gp120_right_2() const { return ___gp120_right_2; }
	inline Color_t2020392075 * get_address_of_gp120_right_2() { return &___gp120_right_2; }
	inline void set_gp120_right_2(Color_t2020392075  value)
	{
		___gp120_right_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
