﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_World_Filler
struct UI_World_Filler_t108322328;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_World_Filler::.ctor()
extern "C"  void UI_World_Filler__ctor_m3703992505 (UI_World_Filler_t108322328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_World_Filler::Awake()
extern "C"  void UI_World_Filler_Awake_m2757545992 (UI_World_Filler_t108322328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_World_Filler::Fill(System.Single)
extern "C"  void UI_World_Filler_Fill_m2266385555 (UI_World_Filler_t108322328 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
