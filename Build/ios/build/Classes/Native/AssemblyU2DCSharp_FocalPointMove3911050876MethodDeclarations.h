﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FocalPointMove
struct FocalPointMove_t3911050876;

#include "codegen/il2cpp-codegen.h"

// System.Void FocalPointMove::.ctor()
extern "C"  void FocalPointMove__ctor_m1823740877 (FocalPointMove_t3911050876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
