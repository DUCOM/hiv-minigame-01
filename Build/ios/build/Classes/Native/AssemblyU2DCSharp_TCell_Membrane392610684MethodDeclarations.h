﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Membrane
struct TCell_Membrane_t392610684;

#include "codegen/il2cpp-codegen.h"

// System.Void TCell_Membrane::.ctor()
extern "C"  void TCell_Membrane__ctor_m2072503969 (TCell_Membrane_t392610684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
