﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Camera
struct Camera_t189460977;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowCam
struct  FollowCam_t1118905480  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform FollowCam::follow
	Transform_t3275118058 * ___follow_2;
	// UnityEngine.Rigidbody2D FollowCam::followRigidbody
	Rigidbody2D_t502193897 * ___followRigidbody_3;
	// UnityEngine.Rect FollowCam::border
	Rect_t3681755626  ___border_4;
	// UnityEngine.Camera FollowCam::cam
	Camera_t189460977 * ___cam_5;
	// UnityEngine.Rigidbody2D FollowCam::rb
	Rigidbody2D_t502193897 * ___rb_6;

public:
	inline static int32_t get_offset_of_follow_2() { return static_cast<int32_t>(offsetof(FollowCam_t1118905480, ___follow_2)); }
	inline Transform_t3275118058 * get_follow_2() const { return ___follow_2; }
	inline Transform_t3275118058 ** get_address_of_follow_2() { return &___follow_2; }
	inline void set_follow_2(Transform_t3275118058 * value)
	{
		___follow_2 = value;
		Il2CppCodeGenWriteBarrier(&___follow_2, value);
	}

	inline static int32_t get_offset_of_followRigidbody_3() { return static_cast<int32_t>(offsetof(FollowCam_t1118905480, ___followRigidbody_3)); }
	inline Rigidbody2D_t502193897 * get_followRigidbody_3() const { return ___followRigidbody_3; }
	inline Rigidbody2D_t502193897 ** get_address_of_followRigidbody_3() { return &___followRigidbody_3; }
	inline void set_followRigidbody_3(Rigidbody2D_t502193897 * value)
	{
		___followRigidbody_3 = value;
		Il2CppCodeGenWriteBarrier(&___followRigidbody_3, value);
	}

	inline static int32_t get_offset_of_border_4() { return static_cast<int32_t>(offsetof(FollowCam_t1118905480, ___border_4)); }
	inline Rect_t3681755626  get_border_4() const { return ___border_4; }
	inline Rect_t3681755626 * get_address_of_border_4() { return &___border_4; }
	inline void set_border_4(Rect_t3681755626  value)
	{
		___border_4 = value;
	}

	inline static int32_t get_offset_of_cam_5() { return static_cast<int32_t>(offsetof(FollowCam_t1118905480, ___cam_5)); }
	inline Camera_t189460977 * get_cam_5() const { return ___cam_5; }
	inline Camera_t189460977 ** get_address_of_cam_5() { return &___cam_5; }
	inline void set_cam_5(Camera_t189460977 * value)
	{
		___cam_5 = value;
		Il2CppCodeGenWriteBarrier(&___cam_5, value);
	}

	inline static int32_t get_offset_of_rb_6() { return static_cast<int32_t>(offsetof(FollowCam_t1118905480, ___rb_6)); }
	inline Rigidbody2D_t502193897 * get_rb_6() const { return ___rb_6; }
	inline Rigidbody2D_t502193897 ** get_address_of_rb_6() { return &___rb_6; }
	inline void set_rb_6(Rigidbody2D_t502193897 * value)
	{
		___rb_6 = value;
		Il2CppCodeGenWriteBarrier(&___rb_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
