﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Movement_Basic
struct  Movement_Basic_t1705390416  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Movement_Basic::moveSpeed
	float ___moveSpeed_2;
	// UnityEngine.Rigidbody2D Movement_Basic::_rb
	Rigidbody2D_t502193897 * ____rb_3;
	// System.Boolean Movement_Basic::_start
	bool ____start_4;
	// UnityEngine.Vector2 Movement_Basic::_startingVelocity
	Vector2_t2243707579  ____startingVelocity_5;
	// UnityEngine.KeyCode Movement_Basic::up
	int32_t ___up_6;
	// UnityEngine.KeyCode Movement_Basic::down
	int32_t ___down_7;
	// UnityEngine.KeyCode Movement_Basic::left
	int32_t ___left_8;
	// UnityEngine.KeyCode Movement_Basic::right
	int32_t ___right_9;

public:
	inline static int32_t get_offset_of_moveSpeed_2() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ___moveSpeed_2)); }
	inline float get_moveSpeed_2() const { return ___moveSpeed_2; }
	inline float* get_address_of_moveSpeed_2() { return &___moveSpeed_2; }
	inline void set_moveSpeed_2(float value)
	{
		___moveSpeed_2 = value;
	}

	inline static int32_t get_offset_of__rb_3() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ____rb_3)); }
	inline Rigidbody2D_t502193897 * get__rb_3() const { return ____rb_3; }
	inline Rigidbody2D_t502193897 ** get_address_of__rb_3() { return &____rb_3; }
	inline void set__rb_3(Rigidbody2D_t502193897 * value)
	{
		____rb_3 = value;
		Il2CppCodeGenWriteBarrier(&____rb_3, value);
	}

	inline static int32_t get_offset_of__start_4() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ____start_4)); }
	inline bool get__start_4() const { return ____start_4; }
	inline bool* get_address_of__start_4() { return &____start_4; }
	inline void set__start_4(bool value)
	{
		____start_4 = value;
	}

	inline static int32_t get_offset_of__startingVelocity_5() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ____startingVelocity_5)); }
	inline Vector2_t2243707579  get__startingVelocity_5() const { return ____startingVelocity_5; }
	inline Vector2_t2243707579 * get_address_of__startingVelocity_5() { return &____startingVelocity_5; }
	inline void set__startingVelocity_5(Vector2_t2243707579  value)
	{
		____startingVelocity_5 = value;
	}

	inline static int32_t get_offset_of_up_6() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ___up_6)); }
	inline int32_t get_up_6() const { return ___up_6; }
	inline int32_t* get_address_of_up_6() { return &___up_6; }
	inline void set_up_6(int32_t value)
	{
		___up_6 = value;
	}

	inline static int32_t get_offset_of_down_7() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ___down_7)); }
	inline int32_t get_down_7() const { return ___down_7; }
	inline int32_t* get_address_of_down_7() { return &___down_7; }
	inline void set_down_7(int32_t value)
	{
		___down_7 = value;
	}

	inline static int32_t get_offset_of_left_8() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ___left_8)); }
	inline int32_t get_left_8() const { return ___left_8; }
	inline int32_t* get_address_of_left_8() { return &___left_8; }
	inline void set_left_8(int32_t value)
	{
		___left_8 = value;
	}

	inline static int32_t get_offset_of_right_9() { return static_cast<int32_t>(offsetof(Movement_Basic_t1705390416, ___right_9)); }
	inline int32_t get_right_9() const { return ___right_9; }
	inline int32_t* get_address_of_right_9() { return &___right_9; }
	inline void set_right_9(int32_t value)
	{
		___right_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
