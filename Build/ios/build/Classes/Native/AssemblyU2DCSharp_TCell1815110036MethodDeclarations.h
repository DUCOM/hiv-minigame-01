﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell
struct TCell_t1815110036;
// CellMovement
struct CellMovement_t2110243311;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void TCell::.ctor()
extern "C"  void TCell__ctor_m2466026901 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TCell::get_radius()
extern "C"  float TCell_get_radius_m399111662 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::set_radius(System.Single)
extern "C"  void TCell_set_radius_m3434622193 (TCell_t1815110036 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TCell::get_infected()
extern "C"  bool TCell_get_infected_m1112633080 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::set_infected(System.Boolean)
extern "C"  void TCell_set_infected_m1220593909 (TCell_t1815110036 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CellMovement TCell::get_move()
extern "C"  CellMovement_t2110243311 * TCell_get_move_m3002539955 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::ChangeType(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color)
extern "C"  void TCell_ChangeType_m2456386377 (TCell_t1815110036 * __this, Color_t2020392075  ___cytoplasmColor0, Color_t2020392075  ___membraneColor1, Color_t2020392075  ___nucleousColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::Instantiate(System.Single)
extern "C"  void TCell_Instantiate_m358529744 (TCell_t1815110036 * __this, float ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::Instantiate(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  void TCell_Instantiate_m866403822 (TCell_t1815110036 * __this, Color_t2020392075  ___cytoplasmColor0, Color_t2020392075  ___membraneColor1, Color_t2020392075  ___nucleousColor2, float ___r3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::Start()
extern "C"  void TCell_Start_m1649656325 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::FixedUpdate()
extern "C"  void TCell_FixedUpdate_m1814111036 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell::LateUpdate()
extern "C"  void TCell_LateUpdate_m691496878 (TCell_t1815110036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
