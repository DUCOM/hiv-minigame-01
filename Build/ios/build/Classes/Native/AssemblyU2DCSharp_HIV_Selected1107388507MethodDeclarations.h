﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Selected
struct HIV_Selected_t1107388507;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Selected::.ctor()
extern "C"  void HIV_Selected__ctor_m4083327364 (HIV_Selected_t1107388507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Selected::OnEnable()
extern "C"  void HIV_Selected_OnEnable_m345684168 (HIV_Selected_t1107388507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Selected::FixedUpdate()
extern "C"  void HIV_Selected_FixedUpdate_m1335899281 (HIV_Selected_t1107388507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HIV_Selected::OutOfBounds()
extern "C"  bool HIV_Selected_OutOfBounds_m3468040698 (HIV_Selected_t1107388507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
