﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioDatabase
struct AudioDatabase_t2585396589;
// Sound
struct Sound_t826716933;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AudioDatabase::.ctor()
extern "C"  void AudioDatabase__ctor_m2526964714 (AudioDatabase_t2585396589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Sound AudioDatabase::get_Item(System.Int32)
extern "C"  Sound_t826716933 * AudioDatabase_get_Item_m1415045717 (AudioDatabase_t2585396589 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Sound AudioDatabase::get_Item(System.String)
extern "C"  Sound_t826716933 * AudioDatabase_get_Item_m342370300 (AudioDatabase_t2585396589 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Sound AudioDatabase::GetSound(System.Int32)
extern "C"  Sound_t826716933 * AudioDatabase_GetSound_m2112300346 (AudioDatabase_t2585396589 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Sound AudioDatabase::GetSound(System.String)
extern "C"  Sound_t826716933 * AudioDatabase_GetSound_m504701663 (AudioDatabase_t2585396589 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::Play()
extern "C"  void AudioDatabase_Play_m1339141818 (AudioDatabase_t2585396589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::Play(System.Int32)
extern "C"  void AudioDatabase_Play_m1657006937 (AudioDatabase_t2585396589 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::Play(System.String)
extern "C"  void AudioDatabase_Play_m2837110456 (AudioDatabase_t2585396589 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::ChangeIndex(System.Int32)
extern "C"  void AudioDatabase_ChangeIndex_m3142360081 (AudioDatabase_t2585396589 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::RefreshSoundIndex()
extern "C"  void AudioDatabase_RefreshSoundIndex_m3816134736 (AudioDatabase_t2585396589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::OnValidate()
extern "C"  void AudioDatabase_OnValidate_m910391179 (AudioDatabase_t2585396589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioDatabase::Awake()
extern "C"  void AudioDatabase_Awake_m2753930889 (AudioDatabase_t2585396589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
