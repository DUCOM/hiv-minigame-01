﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HIV>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2165763026(__this, ___l0, method) ((  void (*) (Enumerator_t1385618551 *, List_1_t1850888877 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HIV>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4054466112(__this, method) ((  void (*) (Enumerator_t1385618551 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HIV>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2531806376(__this, method) ((  Il2CppObject * (*) (Enumerator_t1385618551 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HIV>::Dispose()
#define Enumerator_Dispose_m2762317173(__this, method) ((  void (*) (Enumerator_t1385618551 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HIV>::VerifyState()
#define Enumerator_VerifyState_m134701596(__this, method) ((  void (*) (Enumerator_t1385618551 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HIV>::MoveNext()
#define Enumerator_MoveNext_m1253082444(__this, method) ((  bool (*) (Enumerator_t1385618551 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HIV>::get_Current()
#define Enumerator_get_Current_m2454707853(__this, method) ((  HIV_t2481767745 * (*) (Enumerator_t1385618551 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
