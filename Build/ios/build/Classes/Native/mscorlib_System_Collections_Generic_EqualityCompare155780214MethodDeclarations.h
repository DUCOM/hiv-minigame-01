﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SpriteRender>
struct DefaultComparer_t155780214;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SpriteRender>::.ctor()
extern "C"  void DefaultComparer__ctor_m2679225955_gshared (DefaultComparer_t155780214 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2679225955(__this, method) ((  void (*) (DefaultComparer_t155780214 *, const MethodInfo*))DefaultComparer__ctor_m2679225955_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SpriteRender>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m578758554_gshared (DefaultComparer_t155780214 * __this, SpriteRender_t2828817349  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m578758554(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t155780214 *, SpriteRender_t2828817349 , const MethodInfo*))DefaultComparer_GetHashCode_m578758554_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SpriteRender>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1069673246_gshared (DefaultComparer_t155780214 * __this, SpriteRender_t2828817349  ___x0, SpriteRender_t2828817349  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1069673246(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t155780214 *, SpriteRender_t2828817349 , SpriteRender_t2828817349 , const MethodInfo*))DefaultComparer_Equals_m1069673246_gshared)(__this, ___x0, ___y1, method)
