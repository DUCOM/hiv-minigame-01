﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV
struct HIV_t2481767745;
// System.Collections.Generic.List`1<Statistics>
struct List_1_t1906498139;
// HIVStats
struct HIVStats_t2521285894;
// HIV_Move
struct HIV_Move_t1427226471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Spike
struct Spike_t60554746;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_HIV_Move1427226471.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Spike60554746.h"

// System.Void HIV::.ctor()
extern "C"  void HIV__ctor_m3468708502 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HIV::get_radius()
extern "C"  float HIV_get_radius_m2282812811 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_radius(System.Single)
extern "C"  void HIV_set_radius_m3968955256 (HIV_t2481767745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Statistics> HIV::get_statList()
extern "C"  List_1_t1906498139 * HIV_get_statList_m3052448253 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIVStats HIV::get_stats()
extern "C"  HIVStats_t2521285894 * HIV_get_stats_m4074649537 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV/State HIV::get_state()
extern "C"  int32_t HIV_get_state_m3030343602 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_state(HIV/State)
extern "C"  void HIV_set_state_m109767109 (HIV_t2481767745 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HIV::get_weak()
extern "C"  bool HIV_get_weak_m338777797 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_weak(System.Boolean)
extern "C"  void HIV_set_weak_m4184767996 (HIV_t2481767745 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_capsidColor()
extern "C"  Color_t2020392075  HIV_get_capsidColor_m4139347341 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_capsidColor(UnityEngine.Color)
extern "C"  void HIV_set_capsidColor_m1174700546 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_matrixColor()
extern "C"  Color_t2020392075  HIV_get_matrixColor_m1334576742 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_matrixColor(UnityEngine.Color)
extern "C"  void HIV_set_matrixColor_m3277300941 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_membraneColor()
extern "C"  Color_t2020392075  HIV_get_membraneColor_m1343741098 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_membraneColor(UnityEngine.Color)
extern "C"  void HIV_set_membraneColor_m1240550689 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_rnaColor()
extern "C"  Color_t2020392075  HIV_get_rnaColor_m3258134330 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_rnaColor(UnityEngine.Color)
extern "C"  void HIV_set_rnaColor_m2965164103 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_rtColor()
extern "C"  Color_t2020392075  HIV_get_rtColor_m3140351811 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_rtColor(UnityEngine.Color)
extern "C"  void HIV_set_rtColor_m1723817340 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_GP120_Color_Left()
extern "C"  Color_t2020392075  HIV_get_GP120_Color_Left_m1171295536 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_GP120_Color_Left(UnityEngine.Color)
extern "C"  void HIV_set_GP120_Color_Left_m2190792593 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_GP120_Color_Right()
extern "C"  Color_t2020392075  HIV_get_GP120_Color_Right_m1095216545 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_GP120_Color_Right(UnityEngine.Color)
extern "C"  void HIV_set_GP120_Color_Right_m3365475992 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV::get_GP41_Color()
extern "C"  Color_t2020392075  HIV_get_GP41_Color_m4220143536 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_GP41_Color(UnityEngine.Color)
extern "C"  void HIV_set_GP41_Color_m2193771811 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HIV::get_alpha()
extern "C"  float HIV_get_alpha_m400990519 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_alpha(System.Single)
extern "C"  void HIV_set_alpha_m2192129518 (HIV_t2481767745 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV_Move HIV::get_move()
extern "C"  HIV_Move_t1427226471 * HIV_get_move_m2343449320 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::set_move(HIV_Move)
extern "C"  void HIV_set_move_m632598929 (HIV_t2481767745 * __this, HIV_Move_t1427226471 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HIV::GetSpike(System.Int32)
extern "C"  GameObject_t1756533147 * HIV_GetSpike_m1198366128 (HIV_t2481767745 * __this, int32_t ___spikeNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::ChangeType(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,Tropism)
extern "C"  void HIV_ChangeType_m3637960072 (HIV_t2481767745 * __this, Color_t2020392075  ___matrixColor0, Color_t2020392075  ___capsidColor1, Color_t2020392075  ___membraneColor2, Color_t2020392075  ___rnaColor3, Color_t2020392075  ___rtColor4, Tropism_t3662836552  ___t5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::ChangeType()
extern "C"  void HIV_ChangeType_m1447650064 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::Instantiate(System.Single)
extern "C"  void HIV_Instantiate_m1173439075 (HIV_t2481767745 * __this, float ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::Instantiate(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  void HIV_Instantiate_m1277911629 (HIV_t2481767745 * __this, Color_t2020392075  ___matrixColor0, Color_t2020392075  ___capsidColor1, Color_t2020392075  ___membraneColor2, Color_t2020392075  ___rnaColor3, Color_t2020392075  ___rtColor4, Color_t2020392075  ___GP120Color5, Color_t2020392075  ___GP41Color6, float ___r7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::ChangeState(HIV/State,UnityEngine.GameObject)
extern "C"  void HIV_ChangeState_m1698492408 (HIV_t2481767745 * __this, int32_t ___s0, GameObject_t1756533147 * ___g1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HIV::CheckStateDying()
extern "C"  bool HIV_CheckStateDying_m788117094 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HIV::GetSpikeNumber(Spike)
extern "C"  int32_t HIV_GetSpikeNumber_m1961596675 (HIV_t2481767745 * __this, Spike_t60554746 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::Awake()
extern "C"  void HIV_Awake_m696259965 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::Start()
extern "C"  void HIV_Start_m361889862 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::OnEnable()
extern "C"  void HIV_OnEnable_m3635316966 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::OnDisable()
extern "C"  void HIV_OnDisable_m1323319365 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::LateUpdate()
extern "C"  void HIV_LateUpdate_m2480241205 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV::FixedUpdate()
extern "C"  void HIV_FixedUpdate_m2467446167 (HIV_t2481767745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
