﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Receptor
struct TCell_Receptor_t2424206879;
// TCell_Receptor_Head
struct TCell_Receptor_Head_t2847248660;
// TCell_Receptor_Stem
struct TCell_Receptor_Stem_t4177804179;
// TCell
struct TCell_t1815110036;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

// System.Void TCell_Receptor::.ctor()
extern "C"  void TCell_Receptor__ctor_m2921416192 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism TCell_Receptor::get_type()
extern "C"  Tropism_t3662836552  TCell_Receptor_get_type_m1545012622 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Receptor::set_type(Tropism)
extern "C"  void TCell_Receptor_set_type_m1618614825 (TCell_Receptor_t2424206879 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TCell_Receptor_Head TCell_Receptor::get_head()
extern "C"  TCell_Receptor_Head_t2847248660 * TCell_Receptor_get_head_m3435036360 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TCell_Receptor_Stem TCell_Receptor::get_stem()
extern "C"  TCell_Receptor_Stem_t4177804179 * TCell_Receptor_get_stem_m1676136050 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TCell TCell_Receptor::get_cell()
extern "C"  TCell_t1815110036 * TCell_Receptor_get_cell_m2345807034 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
