﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3396129269.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void System.Array/InternalEnumerator`1<Statistics>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2241630624_gshared (InternalEnumerator_1_t3396129269 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2241630624(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3396129269 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2241630624_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Statistics>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2061353224_gshared (InternalEnumerator_1_t3396129269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2061353224(__this, method) ((  void (*) (InternalEnumerator_1_t3396129269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2061353224_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Statistics>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3721252716_gshared (InternalEnumerator_1_t3396129269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3721252716(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3396129269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3721252716_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Statistics>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2887829367_gshared (InternalEnumerator_1_t3396129269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2887829367(__this, method) ((  void (*) (InternalEnumerator_1_t3396129269 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2887829367_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Statistics>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2706634008_gshared (InternalEnumerator_1_t3396129269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2706634008(__this, method) ((  bool (*) (InternalEnumerator_1_t3396129269 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2706634008_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Statistics>::get_Current()
extern "C"  Statistics_t2537377007  InternalEnumerator_1_get_Current_m2018024775_gshared (InternalEnumerator_1_t3396129269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2018024775(__this, method) ((  Statistics_t2537377007  (*) (InternalEnumerator_1_t3396129269 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2018024775_gshared)(__this, method)
