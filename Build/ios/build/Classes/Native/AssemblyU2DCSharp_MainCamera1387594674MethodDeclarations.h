﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainCamera
struct MainCamera_t1387594674;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void MainCamera::.ctor()
extern "C"  void MainCamera__ctor_m417995029 (MainCamera_t1387594674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MainCamera::get_speed()
extern "C"  float MainCamera_get_speed_m4172356301 (MainCamera_t1387594674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D MainCamera::get_rb()
extern "C"  Rigidbody2D_t502193897 * MainCamera_get_rb_m4323075 (MainCamera_t1387594674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainCamera::ChangeDirection(UnityEngine.Vector3)
extern "C"  void MainCamera_ChangeDirection_m2501752491 (MainCamera_t1387594674 * __this, Vector3_t2243707580  ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainCamera::Stop()
extern "C"  void MainCamera_Stop_m2367445677 (MainCamera_t1387594674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
