﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Return
struct HIV_Return_t3773441690;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Return::.ctor()
extern "C"  void HIV_Return__ctor_m1167862777 (HIV_Return_t3773441690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Return::OnEnable()
extern "C"  void HIV_Return_OnEnable_m987959897 (HIV_Return_t3773441690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Return::Update()
extern "C"  void HIV_Return_Update_m2512504600 (HIV_Return_t3773441690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
