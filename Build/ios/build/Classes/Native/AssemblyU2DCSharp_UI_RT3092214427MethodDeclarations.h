﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_RT
struct UI_RT_t3092214427;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_RT::.ctor()
extern "C"  void UI_RT__ctor_m1810074864 (UI_RT_t3092214427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
