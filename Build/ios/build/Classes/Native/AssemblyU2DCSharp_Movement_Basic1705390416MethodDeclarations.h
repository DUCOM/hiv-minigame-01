﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Movement_Basic
struct Movement_Basic_t1705390416;

#include "codegen/il2cpp-codegen.h"

// System.Void Movement_Basic::.ctor()
extern "C"  void Movement_Basic__ctor_m1101094553 (Movement_Basic_t1705390416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Movement_Basic::Awake()
extern "C"  void Movement_Basic_Awake_m3505725836 (Movement_Basic_t1705390416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Movement_Basic::Start()
extern "C"  void Movement_Basic_Start_m22157681 (Movement_Basic_t1705390416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Movement_Basic::Update()
extern "C"  void Movement_Basic_Update_m351315610 (Movement_Basic_t1705390416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
