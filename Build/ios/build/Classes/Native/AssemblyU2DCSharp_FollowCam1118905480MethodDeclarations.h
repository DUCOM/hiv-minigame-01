﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FollowCam
struct FollowCam_t1118905480;

#include "codegen/il2cpp-codegen.h"

// System.Void FollowCam::.ctor()
extern "C"  void FollowCam__ctor_m2381606933 (FollowCam_t1118905480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowCam::Awake()
extern "C"  void FollowCam_Awake_m2632659812 (FollowCam_t1118905480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FollowCam::FixedUpdate()
extern "C"  void FollowCam_FixedUpdate_m3610844872 (FollowCam_t1118905480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
