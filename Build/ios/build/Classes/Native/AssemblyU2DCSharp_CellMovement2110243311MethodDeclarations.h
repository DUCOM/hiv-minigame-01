﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellMovement
struct CellMovement_t2110243311;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void CellMovement::.ctor()
extern "C"  void CellMovement__ctor_m1969489816 (CellMovement_t2110243311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D CellMovement::get_rb()
extern "C"  Rigidbody2D_t502193897 * CellMovement_get_rb_m3453092686 (CellMovement_t2110243311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellMovement::disable()
extern "C"  void CellMovement_disable_m205775480 (CellMovement_t2110243311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellMovement::SetVelocity(System.Single,System.Single)
extern "C"  void CellMovement_SetVelocity_m3993240599 (CellMovement_t2110243311 * __this, float ___speed0, float ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellMovement::ChangeVelocity(System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void CellMovement_ChangeVelocity_m3635555946 (CellMovement_t2110243311 * __this, float ___speed0, float ___angle1, Vector3_t2243707580  ___startingVector2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellMovement::SetVelocity(UnityEngine.Vector3)
extern "C"  void CellMovement_SetVelocity_m3961975388 (CellMovement_t2110243311 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellMovement::SetVelocity(UnityEngine.Vector3,System.Single)
extern "C"  void CellMovement_SetVelocity_m3802536691 (CellMovement_t2110243311 * __this, Vector3_t2243707580  ___direction0, float ___speed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellMovement::ShootTowards(UnityEngine.Vector3,System.Single)
extern "C"  void CellMovement_ShootTowards_m3653842377 (CellMovement_t2110243311 * __this, Vector3_t2243707580  ___position0, float ___speed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
