﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>
struct Dictionary_2_t2450211488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3770236190.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_207556710.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1331499111_gshared (Enumerator_t3770236190 * __this, Dictionary_2_t2450211488 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1331499111(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3770236190 *, Dictionary_2_t2450211488 *, const MethodInfo*))Enumerator__ctor_m1331499111_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m495744888_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m495744888(__this, method) ((  Il2CppObject * (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m495744888_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2050862182_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2050862182(__this, method) ((  void (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2050862182_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3206339073_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3206339073(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3206339073_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1364806446_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1364806446(__this, method) ((  Il2CppObject * (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1364806446_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1184920668_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1184920668(__this, method) ((  Il2CppObject * (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1184920668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m385052558_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m385052558(__this, method) ((  bool (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_MoveNext_m385052558_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t207556710  Enumerator_get_Current_m1261098558_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1261098558(__this, method) ((  KeyValuePair_2_t207556710  (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_get_Current_m1261098558_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2766001139_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2766001139(__this, method) ((  int32_t (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_get_CurrentKey_m2766001139_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2122533179_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2122533179(__this, method) ((  Il2CppObject * (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_get_CurrentValue_m2122533179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2491434409_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2491434409(__this, method) ((  void (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_Reset_m2491434409_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2223444862_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2223444862(__this, method) ((  void (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_VerifyState_m2223444862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3842411350_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3842411350(__this, method) ((  void (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_VerifyCurrent_m3842411350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1570825211_gshared (Enumerator_t3770236190 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1570825211(__this, method) ((  void (*) (Enumerator_t3770236190 *, const MethodInfo*))Enumerator_Dispose_m1570825211_gshared)(__this, method)
