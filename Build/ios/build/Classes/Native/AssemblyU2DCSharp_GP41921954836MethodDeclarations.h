﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP41
struct GP41_t921954836;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void GP41::.ctor()
extern "C"  void GP41__ctor_m70649397 (GP41_t921954836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics GP41::get_stats()
extern "C"  Statistics_t2537377007  GP41_get_stats_m3961996551 (GP41_t921954836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpriteRenderer GP41::get_spriteRender()
extern "C"  SpriteRenderer_t1209076198 * GP41_get_spriteRender_m1828691819 (GP41_t921954836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshRenderer GP41::get_meshRenderer()
extern "C"  MeshRenderer_t1268241104 * GP41_get_meshRenderer_m864894232 (GP41_t921954836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
