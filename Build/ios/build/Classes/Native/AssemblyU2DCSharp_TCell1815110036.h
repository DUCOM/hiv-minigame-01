﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TCell_Part
struct TCell_Part_t1972431076;
// PairList
struct PairList_t3336234724;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell
struct  TCell_t1815110036  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TCell::_radius
	float ____radius_3;
	// System.Boolean TCell::instantiateOnStart
	bool ___instantiateOnStart_4;
	// System.Boolean TCell::_infected
	bool ____infected_5;
	// TCell_Part TCell::cytoplasm
	TCell_Part_t1972431076 * ___cytoplasm_6;
	// TCell_Part TCell::membrane
	TCell_Part_t1972431076 * ___membrane_7;
	// TCell_Part TCell::nucleous
	TCell_Part_t1972431076 * ___nucleous_8;
	// PairList TCell::pairList
	PairList_t3336234724 * ___pairList_9;
	// System.Single TCell::_timer
	float ____timer_10;
	// System.Single TCell::_rotate
	float ____rotate_11;

public:
	inline static int32_t get_offset_of__radius_3() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ____radius_3)); }
	inline float get__radius_3() const { return ____radius_3; }
	inline float* get_address_of__radius_3() { return &____radius_3; }
	inline void set__radius_3(float value)
	{
		____radius_3 = value;
	}

	inline static int32_t get_offset_of_instantiateOnStart_4() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ___instantiateOnStart_4)); }
	inline bool get_instantiateOnStart_4() const { return ___instantiateOnStart_4; }
	inline bool* get_address_of_instantiateOnStart_4() { return &___instantiateOnStart_4; }
	inline void set_instantiateOnStart_4(bool value)
	{
		___instantiateOnStart_4 = value;
	}

	inline static int32_t get_offset_of__infected_5() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ____infected_5)); }
	inline bool get__infected_5() const { return ____infected_5; }
	inline bool* get_address_of__infected_5() { return &____infected_5; }
	inline void set__infected_5(bool value)
	{
		____infected_5 = value;
	}

	inline static int32_t get_offset_of_cytoplasm_6() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ___cytoplasm_6)); }
	inline TCell_Part_t1972431076 * get_cytoplasm_6() const { return ___cytoplasm_6; }
	inline TCell_Part_t1972431076 ** get_address_of_cytoplasm_6() { return &___cytoplasm_6; }
	inline void set_cytoplasm_6(TCell_Part_t1972431076 * value)
	{
		___cytoplasm_6 = value;
		Il2CppCodeGenWriteBarrier(&___cytoplasm_6, value);
	}

	inline static int32_t get_offset_of_membrane_7() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ___membrane_7)); }
	inline TCell_Part_t1972431076 * get_membrane_7() const { return ___membrane_7; }
	inline TCell_Part_t1972431076 ** get_address_of_membrane_7() { return &___membrane_7; }
	inline void set_membrane_7(TCell_Part_t1972431076 * value)
	{
		___membrane_7 = value;
		Il2CppCodeGenWriteBarrier(&___membrane_7, value);
	}

	inline static int32_t get_offset_of_nucleous_8() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ___nucleous_8)); }
	inline TCell_Part_t1972431076 * get_nucleous_8() const { return ___nucleous_8; }
	inline TCell_Part_t1972431076 ** get_address_of_nucleous_8() { return &___nucleous_8; }
	inline void set_nucleous_8(TCell_Part_t1972431076 * value)
	{
		___nucleous_8 = value;
		Il2CppCodeGenWriteBarrier(&___nucleous_8, value);
	}

	inline static int32_t get_offset_of_pairList_9() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ___pairList_9)); }
	inline PairList_t3336234724 * get_pairList_9() const { return ___pairList_9; }
	inline PairList_t3336234724 ** get_address_of_pairList_9() { return &___pairList_9; }
	inline void set_pairList_9(PairList_t3336234724 * value)
	{
		___pairList_9 = value;
		Il2CppCodeGenWriteBarrier(&___pairList_9, value);
	}

	inline static int32_t get_offset_of__timer_10() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ____timer_10)); }
	inline float get__timer_10() const { return ____timer_10; }
	inline float* get_address_of__timer_10() { return &____timer_10; }
	inline void set__timer_10(float value)
	{
		____timer_10 = value;
	}

	inline static int32_t get_offset_of__rotate_11() { return static_cast<int32_t>(offsetof(TCell_t1815110036, ____rotate_11)); }
	inline float get__rotate_11() const { return ____rotate_11; }
	inline float* get_address_of__rotate_11() { return &____rotate_11; }
	inline void set__rotate_11(float value)
	{
		____rotate_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
