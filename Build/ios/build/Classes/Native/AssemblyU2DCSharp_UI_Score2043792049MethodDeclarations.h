﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Score
struct UI_Score_t2043792049;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Score::.ctor()
extern "C"  void UI_Score__ctor_m1301067138 (UI_Score_t2043792049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Score::OnGUI()
extern "C"  void UI_Score_OnGUI_m578752906 (UI_Score_t2043792049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
