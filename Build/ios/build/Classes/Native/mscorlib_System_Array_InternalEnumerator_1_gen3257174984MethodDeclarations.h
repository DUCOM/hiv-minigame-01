﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3257174984.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22398422722.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m195891490_gshared (InternalEnumerator_1_t3257174984 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m195891490(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3257174984 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m195891490_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1280713946_gshared (InternalEnumerator_1_t3257174984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1280713946(__this, method) ((  void (*) (InternalEnumerator_1_t3257174984 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1280713946_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2000474300_gshared (InternalEnumerator_1_t3257174984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2000474300(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3257174984 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2000474300_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1613063791_gshared (InternalEnumerator_1_t3257174984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1613063791(__this, method) ((  void (*) (InternalEnumerator_1_t3257174984 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1613063791_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m7469914_gshared (InternalEnumerator_1_t3257174984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m7469914(__this, method) ((  bool (*) (InternalEnumerator_1_t3257174984 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m7469914_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>::get_Current()
extern "C"  KeyValuePair_2_t2398422722  InternalEnumerator_1_get_Current_m595861587_gshared (InternalEnumerator_1_t3257174984 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m595861587(__this, method) ((  KeyValuePair_2_t2398422722  (*) (InternalEnumerator_1_t3257174984 *, const MethodInfo*))InternalEnumerator_1_get_Current_m595861587_gshared)(__this, method)
