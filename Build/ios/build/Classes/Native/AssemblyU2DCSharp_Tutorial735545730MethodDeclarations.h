﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tutorial
struct Tutorial_t735545730;

#include "codegen/il2cpp-codegen.h"

// System.Void Tutorial::.ctor()
extern "C"  void Tutorial__ctor_m3772536979 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::Awake()
extern "C"  void Tutorial_Awake_m1360923614 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::OnValidate()
extern "C"  void Tutorial_OnValidate_m950451526 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::NextFrame()
extern "C"  void Tutorial_NextFrame_m1900467925 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::PreviousFrame()
extern "C"  void Tutorial_PreviousFrame_m327594487 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::ToMainMenu()
extern "C"  void Tutorial_ToMainMenu_m2698963726 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tutorial::ToGame()
extern "C"  void Tutorial_ToGame_m2662689906 (Tutorial_t735545730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
