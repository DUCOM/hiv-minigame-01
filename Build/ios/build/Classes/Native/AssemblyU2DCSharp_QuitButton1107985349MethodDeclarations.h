﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuitButton
struct QuitButton_t1107985349;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void QuitButton::.ctor()
extern "C"  void QuitButton__ctor_m2821854874 (QuitButton_t1107985349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuitButton::Quit()
extern "C"  void QuitButton_Quit_m720958885 (QuitButton_t1107985349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuitButton::QuitConfirm(UnityEngine.GameObject)
extern "C"  void QuitButton_QuitConfirm_m3258154715 (QuitButton_t1107985349 * __this, GameObject_t1756533147 * ___quitConfirm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuitButton::PlaySound()
extern "C"  void QuitButton_PlaySound_m443867319 (QuitButton_t1107985349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
