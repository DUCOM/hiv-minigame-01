﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_TryAgain
struct UI_TryAgain_t3222824968;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_TryAgain::.ctor()
extern "C"  void UI_TryAgain__ctor_m3999166147 (UI_TryAgain_t3222824968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_TryAgain::OnGUI()
extern "C"  void UI_TryAgain_OnGUI_m2697421829 (UI_TryAgain_t3222824968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
