﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>
struct ValueCollection_t1153271331;
// System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>
struct Dictionary_2_t2450211488;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4136744252.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1483315994_gshared (ValueCollection_t1153271331 * __this, Dictionary_2_t2450211488 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1483315994(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1153271331 *, Dictionary_2_t2450211488 *, const MethodInfo*))ValueCollection__ctor_m1483315994_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1365621704_gshared (ValueCollection_t1153271331 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1365621704(__this, ___item0, method) ((  void (*) (ValueCollection_t1153271331 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1365621704_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4052963599_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4052963599(__this, method) ((  void (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4052963599_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m983017908_gshared (ValueCollection_t1153271331 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m983017908(__this, ___item0, method) ((  bool (*) (ValueCollection_t1153271331 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m983017908_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1881192871_gshared (ValueCollection_t1153271331 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1881192871(__this, ___item0, method) ((  bool (*) (ValueCollection_t1153271331 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1881192871_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3269857629_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3269857629(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3269857629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3945211245_gshared (ValueCollection_t1153271331 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3945211245(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1153271331 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3945211245_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3160630156_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3160630156(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3160630156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2994529347_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2994529347(__this, method) ((  bool (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2994529347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1349141089_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1349141089(__this, method) ((  bool (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1349141089_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1670992289_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1670992289(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1670992289_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1189756071_gshared (ValueCollection_t1153271331 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1189756071(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1153271331 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1189756071_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::GetEnumerator()
extern "C"  Enumerator_t4136744252  ValueCollection_GetEnumerator_m3068530194_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3068530194(__this, method) ((  Enumerator_t4136744252  (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_GetEnumerator_m3068530194_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1017641181_gshared (ValueCollection_t1153271331 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1017641181(__this, method) ((  int32_t (*) (ValueCollection_t1153271331 *, const MethodInfo*))ValueCollection_get_Count_m1017641181_gshared)(__this, method)
