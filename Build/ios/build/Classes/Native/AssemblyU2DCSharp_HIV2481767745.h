﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<HIV/State,HIV_State>
struct Dictionary_2_t2068941383;
// HIV_Part
struct HIV_Part_t1695996321;
// HIV_Part[]
struct HIV_PartU5BU5D_t327028412;
// SpikeList
struct SpikeList_t1827939702;
// HIV_Move
struct HIV_Move_t1427226471;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV
struct  HIV_t2481767745  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HIV::focalPoint
	GameObject_t1756533147 * ___focalPoint_3;
	// System.Boolean HIV::grouped
	bool ___grouped_4;
	// System.Single HIV::_radius
	float ____radius_5;
	// HIV/State HIV::<state>k__BackingField
	int32_t ___U3CstateU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<HIV/State,HIV_State> HIV::stateList
	Dictionary_2_t2068941383 * ___stateList_7;
	// System.Boolean HIV::selected
	bool ___selected_8;
	// System.Boolean HIV::_weakened
	bool ____weakened_9;
	// HIV_Part HIV::capsid
	HIV_Part_t1695996321 * ___capsid_10;
	// HIV_Part HIV::matrix
	HIV_Part_t1695996321 * ___matrix_11;
	// HIV_Part HIV::membrane
	HIV_Part_t1695996321 * ___membrane_12;
	// HIV_Part[] HIV::rna
	HIV_PartU5BU5D_t327028412* ___rna_13;
	// HIV_Part[] HIV::rt
	HIV_PartU5BU5D_t327028412* ___rt_14;
	// SpikeList HIV::spikeList
	SpikeList_t1827939702 * ___spikeList_15;
	// UnityEngine.GameObject HIV::sparkle
	GameObject_t1756533147 * ___sparkle_16;
	// HIV_Move HIV::<move>k__BackingField
	HIV_Move_t1427226471 * ___U3CmoveU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_focalPoint_3() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___focalPoint_3)); }
	inline GameObject_t1756533147 * get_focalPoint_3() const { return ___focalPoint_3; }
	inline GameObject_t1756533147 ** get_address_of_focalPoint_3() { return &___focalPoint_3; }
	inline void set_focalPoint_3(GameObject_t1756533147 * value)
	{
		___focalPoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___focalPoint_3, value);
	}

	inline static int32_t get_offset_of_grouped_4() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___grouped_4)); }
	inline bool get_grouped_4() const { return ___grouped_4; }
	inline bool* get_address_of_grouped_4() { return &___grouped_4; }
	inline void set_grouped_4(bool value)
	{
		___grouped_4 = value;
	}

	inline static int32_t get_offset_of__radius_5() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ____radius_5)); }
	inline float get__radius_5() const { return ____radius_5; }
	inline float* get_address_of__radius_5() { return &____radius_5; }
	inline void set__radius_5(float value)
	{
		____radius_5 = value;
	}

	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___U3CstateU3Ek__BackingField_6)); }
	inline int32_t get_U3CstateU3Ek__BackingField_6() const { return ___U3CstateU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CstateU3Ek__BackingField_6() { return &___U3CstateU3Ek__BackingField_6; }
	inline void set_U3CstateU3Ek__BackingField_6(int32_t value)
	{
		___U3CstateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_stateList_7() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___stateList_7)); }
	inline Dictionary_2_t2068941383 * get_stateList_7() const { return ___stateList_7; }
	inline Dictionary_2_t2068941383 ** get_address_of_stateList_7() { return &___stateList_7; }
	inline void set_stateList_7(Dictionary_2_t2068941383 * value)
	{
		___stateList_7 = value;
		Il2CppCodeGenWriteBarrier(&___stateList_7, value);
	}

	inline static int32_t get_offset_of_selected_8() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___selected_8)); }
	inline bool get_selected_8() const { return ___selected_8; }
	inline bool* get_address_of_selected_8() { return &___selected_8; }
	inline void set_selected_8(bool value)
	{
		___selected_8 = value;
	}

	inline static int32_t get_offset_of__weakened_9() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ____weakened_9)); }
	inline bool get__weakened_9() const { return ____weakened_9; }
	inline bool* get_address_of__weakened_9() { return &____weakened_9; }
	inline void set__weakened_9(bool value)
	{
		____weakened_9 = value;
	}

	inline static int32_t get_offset_of_capsid_10() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___capsid_10)); }
	inline HIV_Part_t1695996321 * get_capsid_10() const { return ___capsid_10; }
	inline HIV_Part_t1695996321 ** get_address_of_capsid_10() { return &___capsid_10; }
	inline void set_capsid_10(HIV_Part_t1695996321 * value)
	{
		___capsid_10 = value;
		Il2CppCodeGenWriteBarrier(&___capsid_10, value);
	}

	inline static int32_t get_offset_of_matrix_11() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___matrix_11)); }
	inline HIV_Part_t1695996321 * get_matrix_11() const { return ___matrix_11; }
	inline HIV_Part_t1695996321 ** get_address_of_matrix_11() { return &___matrix_11; }
	inline void set_matrix_11(HIV_Part_t1695996321 * value)
	{
		___matrix_11 = value;
		Il2CppCodeGenWriteBarrier(&___matrix_11, value);
	}

	inline static int32_t get_offset_of_membrane_12() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___membrane_12)); }
	inline HIV_Part_t1695996321 * get_membrane_12() const { return ___membrane_12; }
	inline HIV_Part_t1695996321 ** get_address_of_membrane_12() { return &___membrane_12; }
	inline void set_membrane_12(HIV_Part_t1695996321 * value)
	{
		___membrane_12 = value;
		Il2CppCodeGenWriteBarrier(&___membrane_12, value);
	}

	inline static int32_t get_offset_of_rna_13() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___rna_13)); }
	inline HIV_PartU5BU5D_t327028412* get_rna_13() const { return ___rna_13; }
	inline HIV_PartU5BU5D_t327028412** get_address_of_rna_13() { return &___rna_13; }
	inline void set_rna_13(HIV_PartU5BU5D_t327028412* value)
	{
		___rna_13 = value;
		Il2CppCodeGenWriteBarrier(&___rna_13, value);
	}

	inline static int32_t get_offset_of_rt_14() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___rt_14)); }
	inline HIV_PartU5BU5D_t327028412* get_rt_14() const { return ___rt_14; }
	inline HIV_PartU5BU5D_t327028412** get_address_of_rt_14() { return &___rt_14; }
	inline void set_rt_14(HIV_PartU5BU5D_t327028412* value)
	{
		___rt_14 = value;
		Il2CppCodeGenWriteBarrier(&___rt_14, value);
	}

	inline static int32_t get_offset_of_spikeList_15() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___spikeList_15)); }
	inline SpikeList_t1827939702 * get_spikeList_15() const { return ___spikeList_15; }
	inline SpikeList_t1827939702 ** get_address_of_spikeList_15() { return &___spikeList_15; }
	inline void set_spikeList_15(SpikeList_t1827939702 * value)
	{
		___spikeList_15 = value;
		Il2CppCodeGenWriteBarrier(&___spikeList_15, value);
	}

	inline static int32_t get_offset_of_sparkle_16() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___sparkle_16)); }
	inline GameObject_t1756533147 * get_sparkle_16() const { return ___sparkle_16; }
	inline GameObject_t1756533147 ** get_address_of_sparkle_16() { return &___sparkle_16; }
	inline void set_sparkle_16(GameObject_t1756533147 * value)
	{
		___sparkle_16 = value;
		Il2CppCodeGenWriteBarrier(&___sparkle_16, value);
	}

	inline static int32_t get_offset_of_U3CmoveU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(HIV_t2481767745, ___U3CmoveU3Ek__BackingField_17)); }
	inline HIV_Move_t1427226471 * get_U3CmoveU3Ek__BackingField_17() const { return ___U3CmoveU3Ek__BackingField_17; }
	inline HIV_Move_t1427226471 ** get_address_of_U3CmoveU3Ek__BackingField_17() { return &___U3CmoveU3Ek__BackingField_17; }
	inline void set_U3CmoveU3Ek__BackingField_17(HIV_Move_t1427226471 * value)
	{
		___U3CmoveU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoveU3Ek__BackingField_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
