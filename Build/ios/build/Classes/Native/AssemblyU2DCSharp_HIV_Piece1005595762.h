﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Piece
struct  HIV_Piece_t1005595762  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean HIV_Piece::lockColor
	bool ___lockColor_2;
	// UnityEngine.Color HIV_Piece::_color
	Color_t2020392075  ____color_3;
	// Statistics HIV_Piece::_stats
	Statistics_t2537377007  ____stats_4;

public:
	inline static int32_t get_offset_of_lockColor_2() { return static_cast<int32_t>(offsetof(HIV_Piece_t1005595762, ___lockColor_2)); }
	inline bool get_lockColor_2() const { return ___lockColor_2; }
	inline bool* get_address_of_lockColor_2() { return &___lockColor_2; }
	inline void set_lockColor_2(bool value)
	{
		___lockColor_2 = value;
	}

	inline static int32_t get_offset_of__color_3() { return static_cast<int32_t>(offsetof(HIV_Piece_t1005595762, ____color_3)); }
	inline Color_t2020392075  get__color_3() const { return ____color_3; }
	inline Color_t2020392075 * get_address_of__color_3() { return &____color_3; }
	inline void set__color_3(Color_t2020392075  value)
	{
		____color_3 = value;
	}

	inline static int32_t get_offset_of__stats_4() { return static_cast<int32_t>(offsetof(HIV_Piece_t1005595762, ____stats_4)); }
	inline Statistics_t2537377007  get__stats_4() const { return ____stats_4; }
	inline Statistics_t2537377007 * get_address_of__stats_4() { return &____stats_4; }
	inline void set__stats_4(Statistics_t2537377007  value)
	{
		____stats_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
