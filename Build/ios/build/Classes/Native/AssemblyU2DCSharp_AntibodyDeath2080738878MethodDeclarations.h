﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyDeath
struct AntibodyDeath_t2080738878;

#include "codegen/il2cpp-codegen.h"

// System.Void AntibodyDeath::.ctor()
extern "C"  void AntibodyDeath__ctor_m138835577 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyDeath::OnEnable()
extern "C"  void AntibodyDeath_OnEnable_m234199385 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyDeath::OnDisable()
extern "C"  void AntibodyDeath_OnDisable_m646818162 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyDeath::Update()
extern "C"  void AntibodyDeath_Update_m3397076444 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
