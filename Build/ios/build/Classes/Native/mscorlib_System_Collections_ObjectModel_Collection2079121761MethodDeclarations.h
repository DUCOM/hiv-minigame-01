﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Statistics>
struct Collection_1_t2079121761;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Statistics[]
struct StatisticsU5BU5D_t1904610934;
// System.Collections.Generic.IEnumerator`1<Statistics>
struct IEnumerator_1_t12900834;
// System.Collections.Generic.IList`1<Statistics>
struct IList_1_t3078317608;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::.ctor()
extern "C"  void Collection_1__ctor_m2135120956_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2135120956(__this, method) ((  void (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1__ctor_m2135120956_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m208159497_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m208159497(__this, method) ((  bool (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m208159497_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m305778876_gshared (Collection_1_t2079121761 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m305778876(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2079121761 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m305778876_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m411437521_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m411437521(__this, method) ((  Il2CppObject * (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m411437521_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2654853184_gshared (Collection_1_t2079121761 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2654853184(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2079121761 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2654853184_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m4081440194_gshared (Collection_1_t2079121761 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m4081440194(__this, ___value0, method) ((  bool (*) (Collection_1_t2079121761 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m4081440194_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3287861650_gshared (Collection_1_t2079121761 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3287861650(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2079121761 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3287861650_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3888666269_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3888666269(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3888666269_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3178364005_gshared (Collection_1_t2079121761 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3178364005(__this, ___value0, method) ((  void (*) (Collection_1_t2079121761 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3178364005_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3569953572_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3569953572(__this, method) ((  bool (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3569953572_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4000054946_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4000054946(__this, method) ((  Il2CppObject * (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4000054946_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4133335913_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4133335913(__this, method) ((  bool (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4133335913_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1031308080_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1031308080(__this, method) ((  bool (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1031308080_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m25258885_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25258885(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2079121761 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25258885_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2779675702_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2779675702(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2779675702_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::Add(T)
extern "C"  void Collection_1_Add_m196494097_gshared (Collection_1_t2079121761 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define Collection_1_Add_m196494097(__this, ___item0, method) ((  void (*) (Collection_1_t2079121761 *, Statistics_t2537377007 , const MethodInfo*))Collection_1_Add_m196494097_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::Clear()
extern "C"  void Collection_1_Clear_m2055572005_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2055572005(__this, method) ((  void (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_Clear_m2055572005_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3086469267_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3086469267(__this, method) ((  void (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_ClearItems_m3086469267_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::Contains(T)
extern "C"  bool Collection_1_Contains_m2325503495_gshared (Collection_1_t2079121761 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2325503495(__this, ___item0, method) ((  bool (*) (Collection_1_t2079121761 *, Statistics_t2537377007 , const MethodInfo*))Collection_1_Contains_m2325503495_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1452465761_gshared (Collection_1_t2079121761 * __this, StatisticsU5BU5D_t1904610934* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1452465761(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2079121761 *, StatisticsU5BU5D_t1904610934*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1452465761_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Statistics>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3234444308_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3234444308(__this, method) ((  Il2CppObject* (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_GetEnumerator_m3234444308_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Statistics>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3573856097_gshared (Collection_1_t2079121761 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3573856097(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2079121761 *, Statistics_t2537377007 , const MethodInfo*))Collection_1_IndexOf_m3573856097_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4179154478_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, Statistics_t2537377007  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4179154478(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, Statistics_t2537377007 , const MethodInfo*))Collection_1_Insert_m4179154478_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1772170783_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, Statistics_t2537377007  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1772170783(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, Statistics_t2537377007 , const MethodInfo*))Collection_1_InsertItem_m1772170783_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::Remove(T)
extern "C"  bool Collection_1_Remove_m3193268896_gshared (Collection_1_t2079121761 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3193268896(__this, ___item0, method) ((  bool (*) (Collection_1_t2079121761 *, Statistics_t2537377007 , const MethodInfo*))Collection_1_Remove_m3193268896_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2418573810_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2418573810(__this, ___index0, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2418573810_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3505392096_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3505392096(__this, ___index0, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3505392096_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Statistics>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m668585472_gshared (Collection_1_t2079121761 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m668585472(__this, method) ((  int32_t (*) (Collection_1_t2079121761 *, const MethodInfo*))Collection_1_get_Count_m668585472_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Statistics>::get_Item(System.Int32)
extern "C"  Statistics_t2537377007  Collection_1_get_Item_m2110251254_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2110251254(__this, ___index0, method) ((  Statistics_t2537377007  (*) (Collection_1_t2079121761 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2110251254_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3245774761_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, Statistics_t2537377007  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3245774761(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, Statistics_t2537377007 , const MethodInfo*))Collection_1_set_Item_m3245774761_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m564267590_gshared (Collection_1_t2079121761 * __this, int32_t ___index0, Statistics_t2537377007  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m564267590(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2079121761 *, int32_t, Statistics_t2537377007 , const MethodInfo*))Collection_1_SetItem_m564267590_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1634547755_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1634547755(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1634547755_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Statistics>::ConvertItem(System.Object)
extern "C"  Statistics_t2537377007  Collection_1_ConvertItem_m1952764047_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1952764047(__this /* static, unused */, ___item0, method) ((  Statistics_t2537377007  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1952764047_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Statistics>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m667419607_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m667419607(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m667419607_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1917270527_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1917270527(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1917270527_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Statistics>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2963524258_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2963524258(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2963524258_gshared)(__this /* static, unused */, ___list0, method)
