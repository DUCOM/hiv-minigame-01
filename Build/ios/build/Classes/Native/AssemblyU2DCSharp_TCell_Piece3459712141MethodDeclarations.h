﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Piece
struct TCell_Piece_t3459712141;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void TCell_Piece::.ctor()
extern "C"  void TCell_Piece__ctor_m2043918716 (TCell_Piece_t3459712141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TCell_Piece::get_color()
extern "C"  Color_t2020392075  TCell_Piece_get_color_m2225525357 (TCell_Piece_t3459712141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Piece::set_color(UnityEngine.Color)
extern "C"  void TCell_Piece_set_color_m3929169316 (TCell_Piece_t3459712141 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpriteRenderer TCell_Piece::get_spriteRender()
extern "C"  SpriteRenderer_t1209076198 * TCell_Piece_get_spriteRender_m1679985202 (TCell_Piece_t3459712141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
