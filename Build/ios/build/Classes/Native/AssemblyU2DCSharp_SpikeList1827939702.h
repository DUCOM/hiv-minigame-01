﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<Spike>
struct List_1_t3724643174;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpikeList
struct  SpikeList_t1827939702  : public Il2CppObject
{
public:
	// UnityEngine.GameObject SpikeList::prefab
	GameObject_t1756533147 * ___prefab_0;
	// Tropism SpikeList::tropism
	Tropism_t3662836552  ___tropism_1;
	// System.UInt32 SpikeList::_count
	uint32_t ____count_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SpikeList::objectList
	List_1_t1125654279 * ___objectList_3;
	// System.Collections.Generic.List`1<Spike> SpikeList::scriptList
	List_1_t3724643174 * ___scriptList_4;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(SpikeList_t1827939702, ___prefab_0)); }
	inline GameObject_t1756533147 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t1756533147 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t1756533147 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_0, value);
	}

	inline static int32_t get_offset_of_tropism_1() { return static_cast<int32_t>(offsetof(SpikeList_t1827939702, ___tropism_1)); }
	inline Tropism_t3662836552  get_tropism_1() const { return ___tropism_1; }
	inline Tropism_t3662836552 * get_address_of_tropism_1() { return &___tropism_1; }
	inline void set_tropism_1(Tropism_t3662836552  value)
	{
		___tropism_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(SpikeList_t1827939702, ____count_2)); }
	inline uint32_t get__count_2() const { return ____count_2; }
	inline uint32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(uint32_t value)
	{
		____count_2 = value;
	}

	inline static int32_t get_offset_of_objectList_3() { return static_cast<int32_t>(offsetof(SpikeList_t1827939702, ___objectList_3)); }
	inline List_1_t1125654279 * get_objectList_3() const { return ___objectList_3; }
	inline List_1_t1125654279 ** get_address_of_objectList_3() { return &___objectList_3; }
	inline void set_objectList_3(List_1_t1125654279 * value)
	{
		___objectList_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectList_3, value);
	}

	inline static int32_t get_offset_of_scriptList_4() { return static_cast<int32_t>(offsetof(SpikeList_t1827939702, ___scriptList_4)); }
	inline List_1_t3724643174 * get_scriptList_4() const { return ___scriptList_4; }
	inline List_1_t3724643174 ** get_address_of_scriptList_4() { return &___scriptList_4; }
	inline void set_scriptList_4(List_1_t3724643174 * value)
	{
		___scriptList_4 = value;
		Il2CppCodeGenWriteBarrier(&___scriptList_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
