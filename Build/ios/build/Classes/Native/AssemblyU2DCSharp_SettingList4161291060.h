﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingList
struct  SettingList_t4161291060 
{
public:
	// System.Single SettingList::volume
	float ___volume_0;
	// System.Single SettingList::musicVolume
	float ___musicVolume_1;
	// System.Single SettingList::effectsVolume
	float ___effectsVolume_2;

public:
	inline static int32_t get_offset_of_volume_0() { return static_cast<int32_t>(offsetof(SettingList_t4161291060, ___volume_0)); }
	inline float get_volume_0() const { return ___volume_0; }
	inline float* get_address_of_volume_0() { return &___volume_0; }
	inline void set_volume_0(float value)
	{
		___volume_0 = value;
	}

	inline static int32_t get_offset_of_musicVolume_1() { return static_cast<int32_t>(offsetof(SettingList_t4161291060, ___musicVolume_1)); }
	inline float get_musicVolume_1() const { return ___musicVolume_1; }
	inline float* get_address_of_musicVolume_1() { return &___musicVolume_1; }
	inline void set_musicVolume_1(float value)
	{
		___musicVolume_1 = value;
	}

	inline static int32_t get_offset_of_effectsVolume_2() { return static_cast<int32_t>(offsetof(SettingList_t4161291060, ___effectsVolume_2)); }
	inline float get_effectsVolume_2() const { return ___effectsVolume_2; }
	inline float* get_address_of_effectsVolume_2() { return &___effectsVolume_2; }
	inline void set_effectsVolume_2(float value)
	{
		___effectsVolume_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
