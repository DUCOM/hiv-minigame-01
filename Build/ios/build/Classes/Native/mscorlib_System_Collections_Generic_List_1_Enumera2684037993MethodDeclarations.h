﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<ImageRender>
struct List_1_t3149308319;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2684037993.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ImageRender>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3832724304_gshared (Enumerator_t2684037993 * __this, List_1_t3149308319 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3832724304(__this, ___l0, method) ((  void (*) (Enumerator_t2684037993 *, List_1_t3149308319 *, const MethodInfo*))Enumerator__ctor_m3832724304_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ImageRender>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2631533202_gshared (Enumerator_t2684037993 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2631533202(__this, method) ((  void (*) (Enumerator_t2684037993 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2631533202_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ImageRender>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1468174486_gshared (Enumerator_t2684037993 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1468174486(__this, method) ((  Il2CppObject * (*) (Enumerator_t2684037993 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1468174486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ImageRender>::Dispose()
extern "C"  void Enumerator_Dispose_m75521715_gshared (Enumerator_t2684037993 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m75521715(__this, method) ((  void (*) (Enumerator_t2684037993 *, const MethodInfo*))Enumerator_Dispose_m75521715_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ImageRender>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3863846302_gshared (Enumerator_t2684037993 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3863846302(__this, method) ((  void (*) (Enumerator_t2684037993 *, const MethodInfo*))Enumerator_VerifyState_m3863846302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ImageRender>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2672808425_gshared (Enumerator_t2684037993 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2672808425(__this, method) ((  bool (*) (Enumerator_t2684037993 *, const MethodInfo*))Enumerator_MoveNext_m2672808425_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ImageRender>::get_Current()
extern "C"  ImageRender_t3780187187  Enumerator_get_Current_m2697300105_gshared (Enumerator_t2684037993 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2697300105(__this, method) ((  ImageRender_t3780187187  (*) (Enumerator_t2684037993 *, const MethodInfo*))Enumerator_get_Current_m2697300105_gshared)(__this, method)
