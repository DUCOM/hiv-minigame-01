﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ScoreList
struct ScoreList_t2288120898;
// HighScores
struct HighScores_t3229912699;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighScores
struct  HighScores_t3229912699  : public Il2CppObject
{
public:
	// ScoreList HighScores::_highScores
	ScoreList_t2288120898 * ____highScores_2;

public:
	inline static int32_t get_offset_of__highScores_2() { return static_cast<int32_t>(offsetof(HighScores_t3229912699, ____highScores_2)); }
	inline ScoreList_t2288120898 * get__highScores_2() const { return ____highScores_2; }
	inline ScoreList_t2288120898 ** get_address_of__highScores_2() { return &____highScores_2; }
	inline void set__highScores_2(ScoreList_t2288120898 * value)
	{
		____highScores_2 = value;
		Il2CppCodeGenWriteBarrier(&____highScores_2, value);
	}
};

struct HighScores_t3229912699_StaticFields
{
public:
	// System.String HighScores::_filePath
	String_t* ____filePath_1;
	// HighScores HighScores::main
	HighScores_t3229912699 * ___main_3;

public:
	inline static int32_t get_offset_of__filePath_1() { return static_cast<int32_t>(offsetof(HighScores_t3229912699_StaticFields, ____filePath_1)); }
	inline String_t* get__filePath_1() const { return ____filePath_1; }
	inline String_t** get_address_of__filePath_1() { return &____filePath_1; }
	inline void set__filePath_1(String_t* value)
	{
		____filePath_1 = value;
		Il2CppCodeGenWriteBarrier(&____filePath_1, value);
	}

	inline static int32_t get_offset_of_main_3() { return static_cast<int32_t>(offsetof(HighScores_t3229912699_StaticFields, ___main_3)); }
	inline HighScores_t3229912699 * get_main_3() const { return ___main_3; }
	inline HighScores_t3229912699 ** get_address_of_main_3() { return &___main_3; }
	inline void set_main_3(HighScores_t3229912699 * value)
	{
		___main_3 = value;
		Il2CppCodeGenWriteBarrier(&___main_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
