﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RNA
struct RNA_t915683991;

#include "codegen/il2cpp-codegen.h"

// System.Void RNA::.ctor()
extern "C"  void RNA__ctor_m1057206074 (RNA_t915683991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RNA::ChangeStats()
extern "C"  void RNA_ChangeStats_m3690331269 (RNA_t915683991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
