﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<ImageRender>
struct DefaultComparer_t2269457420;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<ImageRender>::.ctor()
extern "C"  void DefaultComparer__ctor_m3077822145_gshared (DefaultComparer_t2269457420 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3077822145(__this, method) ((  void (*) (DefaultComparer_t2269457420 *, const MethodInfo*))DefaultComparer__ctor_m3077822145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<ImageRender>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1047794422_gshared (DefaultComparer_t2269457420 * __this, ImageRender_t3780187187  ___x0, ImageRender_t3780187187  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1047794422(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2269457420 *, ImageRender_t3780187187 , ImageRender_t3780187187 , const MethodInfo*))DefaultComparer_Compare_m1047794422_gshared)(__this, ___x0, ___y1, method)
