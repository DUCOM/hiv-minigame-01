﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>
struct ReadOnlyCollection_1_t2723162699;
// System.Collections.Generic.IList`1<Statistics>
struct IList_1_t3078317608;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Statistics[]
struct StatisticsU5BU5D_t1904610934;
// System.Collections.Generic.IEnumerator`1<Statistics>
struct IEnumerator_1_t12900834;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3486240833_gshared (ReadOnlyCollection_1_t2723162699 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3486240833(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3486240833_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m994263847_gshared (ReadOnlyCollection_1_t2723162699 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m994263847(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, Statistics_t2537377007 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m994263847_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2675124483_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2675124483(__this, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2675124483_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2586673312_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, Statistics_t2537377007  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2586673312(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, Statistics_t2537377007 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2586673312_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2856504038_gshared (ReadOnlyCollection_1_t2723162699 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2856504038(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, Statistics_t2537377007 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2856504038_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1912969060_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1912969060(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1912969060_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Statistics_t2537377007  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3768916312_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3768916312(__this, ___index0, method) ((  Statistics_t2537377007  (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3768916312_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1310940443_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, Statistics_t2537377007  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1310940443(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, Statistics_t2537377007 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1310940443_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1844968943_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1844968943(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1844968943_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2393268934_gshared (ReadOnlyCollection_1_t2723162699 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2393268934(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2393268934_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4249281619_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4249281619(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4249281619_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m404711266_gshared (ReadOnlyCollection_1_t2723162699 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m404711266(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2723162699 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m404711266_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2421358872_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2421358872(__this, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2421358872_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m962481988_gshared (ReadOnlyCollection_1_t2723162699 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m962481988(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m962481988_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2829138000_gshared (ReadOnlyCollection_1_t2723162699 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2829138000(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2723162699 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2829138000_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2554184243_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2554184243(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2554184243_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m265933307_gshared (ReadOnlyCollection_1_t2723162699 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m265933307(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m265933307_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3382325969_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3382325969(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3382325969_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3309485258_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3309485258(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3309485258_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m625169740_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m625169740(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m625169740_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3127331915_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3127331915(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3127331915_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1490440494_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1490440494(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1490440494_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4026658127_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4026658127(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4026658127_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2577927384_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2577927384(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2577927384_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m7457733_gshared (ReadOnlyCollection_1_t2723162699 * __this, Statistics_t2537377007  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m7457733(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2723162699 *, Statistics_t2537377007 , const MethodInfo*))ReadOnlyCollection_1_Contains_m7457733_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m946404491_gshared (ReadOnlyCollection_1_t2723162699 * __this, StatisticsU5BU5D_t1904610934* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m946404491(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2723162699 *, StatisticsU5BU5D_t1904610934*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m946404491_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1195667542_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1195667542(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1195667542_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1712403871_gshared (ReadOnlyCollection_1_t2723162699 * __this, Statistics_t2537377007  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1712403871(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2723162699 *, Statistics_t2537377007 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1712403871_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2507807426_gshared (ReadOnlyCollection_1_t2723162699 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2507807426(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2723162699 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2507807426_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>::get_Item(System.Int32)
extern "C"  Statistics_t2537377007  ReadOnlyCollection_1_get_Item_m2773646060_gshared (ReadOnlyCollection_1_t2723162699 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2773646060(__this, ___index0, method) ((  Statistics_t2537377007  (*) (ReadOnlyCollection_1_t2723162699 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2773646060_gshared)(__this, ___index0, method)
