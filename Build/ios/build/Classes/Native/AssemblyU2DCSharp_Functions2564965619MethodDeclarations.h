﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Vector2 Functions::RandomBorder(UnityEngine.Rect)
extern "C"  Vector2_t2243707579  Functions_RandomBorder_m1218504433 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Functions::CameraToWorldRect(UnityEngine.Camera,UnityEngine.Rect)
extern "C"  Rect_t3681755626  Functions_CameraToWorldRect_m4129867403 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___c0, Rect_t3681755626  ___border1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Functions::OutsideRect(UnityEngine.Vector3,UnityEngine.Rect)
extern "C"  bool Functions_OutsideRect_m364063437 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___pos0, Rect_t3681755626  ___rect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Functions::GetColorPercentage(System.Single)
extern "C"  Color_t2020392075  Functions_GetColorPercentage_m2869151277 (Il2CppObject * __this /* static, unused */, float ___percent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
