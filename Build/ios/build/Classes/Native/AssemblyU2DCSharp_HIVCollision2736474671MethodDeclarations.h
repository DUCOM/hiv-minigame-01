﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIVCollision
struct HIVCollision_t2736474671;

#include "codegen/il2cpp-codegen.h"

// System.Void HIVCollision::.ctor()
extern "C"  void HIVCollision__ctor_m3646795908 (HIVCollision_t2736474671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
