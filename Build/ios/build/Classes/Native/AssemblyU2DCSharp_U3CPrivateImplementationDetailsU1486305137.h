﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A
	U24ArrayTypeU3D16_t3894236545  ___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18
	U24ArrayTypeU3D12_t1568637718  ___U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0)); }
	inline U24ArrayTypeU3D16_t3894236545  get_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0() const { return ___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0; }
	inline U24ArrayTypeU3D16_t3894236545 * get_address_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0() { return &___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0; }
	inline void set_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0(U24ArrayTypeU3D16_t3894236545  value)
	{
		___U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1)); }
	inline U24ArrayTypeU3D12_t1568637718  get_U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1() const { return ___U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1; }
	inline U24ArrayTypeU3D12_t1568637718 * get_address_of_U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1() { return &___U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1; }
	inline void set_U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1(U24ArrayTypeU3D12_t1568637718  value)
	{
		___U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
