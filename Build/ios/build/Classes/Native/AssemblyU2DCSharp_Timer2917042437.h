﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Timer_TimerState3696398080.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer
struct  Timer_t2917042437  : public Il2CppObject
{
public:
	// System.Single Timer::duration
	float ___duration_0;
	// System.String Timer::_name
	String_t* ____name_1;
	// Timer/TimerState Timer::state
	int32_t ___state_2;
	// System.Boolean Timer::loop
	bool ___loop_3;
	// System.Single Timer::_time
	float ____time_4;

public:
	inline static int32_t get_offset_of_duration_0() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___duration_0)); }
	inline float get_duration_0() const { return ___duration_0; }
	inline float* get_address_of_duration_0() { return &___duration_0; }
	inline void set_duration_0(float value)
	{
		___duration_0 = value;
	}

	inline static int32_t get_offset_of__name_1() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ____name_1)); }
	inline String_t* get__name_1() const { return ____name_1; }
	inline String_t** get_address_of__name_1() { return &____name_1; }
	inline void set__name_1(String_t* value)
	{
		____name_1 = value;
		Il2CppCodeGenWriteBarrier(&____name_1, value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}

	inline static int32_t get_offset_of__time_4() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ____time_4)); }
	inline float get__time_4() const { return ____time_4; }
	inline float* get_address_of__time_4() { return &____time_4; }
	inline void set__time_4(float value)
	{
		____time_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
