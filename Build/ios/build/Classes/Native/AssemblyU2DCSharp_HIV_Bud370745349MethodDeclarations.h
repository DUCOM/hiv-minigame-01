﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Bud
struct HIV_Bud_t370745349;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Bud::.ctor()
extern "C"  void HIV_Bud__ctor_m2460691438 (HIV_Bud_t370745349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Bud::OnEnable()
extern "C"  void HIV_Bud_OnEnable_m3457259798 (HIV_Bud_t370745349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Bud::FixedUpdate()
extern "C"  void HIV_Bud_FixedUpdate_m1596422867 (HIV_Bud_t370745349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
