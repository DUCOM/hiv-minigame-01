﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageRender
struct ImageRender_t3780187187;
struct ImageRender_t3780187187_marshaled_pinvoke;
struct ImageRender_t3780187187_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ImageRender_t3780187187;
struct ImageRender_t3780187187_marshaled_pinvoke;

extern "C" void ImageRender_t3780187187_marshal_pinvoke(const ImageRender_t3780187187& unmarshaled, ImageRender_t3780187187_marshaled_pinvoke& marshaled);
extern "C" void ImageRender_t3780187187_marshal_pinvoke_back(const ImageRender_t3780187187_marshaled_pinvoke& marshaled, ImageRender_t3780187187& unmarshaled);
extern "C" void ImageRender_t3780187187_marshal_pinvoke_cleanup(ImageRender_t3780187187_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ImageRender_t3780187187;
struct ImageRender_t3780187187_marshaled_com;

extern "C" void ImageRender_t3780187187_marshal_com(const ImageRender_t3780187187& unmarshaled, ImageRender_t3780187187_marshaled_com& marshaled);
extern "C" void ImageRender_t3780187187_marshal_com_back(const ImageRender_t3780187187_marshaled_com& marshaled, ImageRender_t3780187187& unmarshaled);
extern "C" void ImageRender_t3780187187_marshal_com_cleanup(ImageRender_t3780187187_marshaled_com& marshaled);
