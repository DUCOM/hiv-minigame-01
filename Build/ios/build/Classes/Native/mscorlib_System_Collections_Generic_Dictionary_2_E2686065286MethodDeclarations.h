﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2686065286.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23418353102.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3452573955_gshared (Enumerator_t2686065286 * __this, Dictionary_2_t1366040584 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3452573955(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2686065286 *, Dictionary_2_t1366040584 *, const MethodInfo*))Enumerator__ctor_m3452573955_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3776790514_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3776790514(__this, method) ((  Il2CppObject * (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3776790514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4033208292_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4033208292(__this, method) ((  void (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4033208292_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4274497413_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4274497413(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4274497413_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3086419808_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3086419808(__this, method) ((  Il2CppObject * (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3086419808_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1000559986_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1000559986(__this, method) ((  Il2CppObject * (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1000559986_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3965618828_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3965618828(__this, method) ((  bool (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_MoveNext_m3965618828_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::get_Current()
extern "C"  KeyValuePair_2_t3418353102  Enumerator_get_Current_m1934908044_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1934908044(__this, method) ((  KeyValuePair_2_t3418353102  (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_get_Current_m1934908044_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2724013223_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2724013223(__this, method) ((  int32_t (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_get_CurrentKey_m2724013223_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::get_CurrentValue()
extern "C"  float Enumerator_get_CurrentValue_m327439951_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m327439951(__this, method) ((  float (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_get_CurrentValue_m327439951_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::Reset()
extern "C"  void Enumerator_Reset_m2009395821_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2009395821(__this, method) ((  void (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_Reset_m2009395821_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3432227096_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3432227096(__this, method) ((  void (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_VerifyState_m3432227096_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3564647764_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3564647764(__this, method) ((  void (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_VerifyCurrent_m3564647764_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/CoReceptorType,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m2405210063_gshared (Enumerator_t2686065286 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2405210063(__this, method) ((  void (*) (Enumerator_t2686065286 *, const MethodInfo*))Enumerator_Dispose_m2405210063_gshared)(__this, method)
