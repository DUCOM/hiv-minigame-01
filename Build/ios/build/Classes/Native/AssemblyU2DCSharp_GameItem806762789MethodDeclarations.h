﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// GameItem
struct GameItem_t806762789;
struct GameItem_t806762789_marshaled_pinvoke;
struct GameItem_t806762789_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameItem806762789.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject> GameItem::get_itemList()
extern "C"  List_1_t1125654279 * GameItem_get_itemList_m426376171 (GameItem_t806762789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameItem::get_population()
extern "C"  int32_t GameItem_get_population_m1805638414 (GameItem_t806762789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameItem::get_populationMax()
extern "C"  int32_t GameItem_get_populationMax_m4143654770 (GameItem_t806762789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameItem::get_item()
extern "C"  GameObject_t1756533147 * GameItem_get_item_m1086059871 (GameItem_t806762789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameItem::Initialize(System.Boolean)
extern "C"  void GameItem_Initialize_m3755603307 (GameItem_t806762789 * __this, bool ___instantiate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct GameItem_t806762789;
struct GameItem_t806762789_marshaled_pinvoke;

extern "C" void GameItem_t806762789_marshal_pinvoke(const GameItem_t806762789& unmarshaled, GameItem_t806762789_marshaled_pinvoke& marshaled);
extern "C" void GameItem_t806762789_marshal_pinvoke_back(const GameItem_t806762789_marshaled_pinvoke& marshaled, GameItem_t806762789& unmarshaled);
extern "C" void GameItem_t806762789_marshal_pinvoke_cleanup(GameItem_t806762789_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GameItem_t806762789;
struct GameItem_t806762789_marshaled_com;

extern "C" void GameItem_t806762789_marshal_com(const GameItem_t806762789& unmarshaled, GameItem_t806762789_marshaled_com& marshaled);
extern "C" void GameItem_t806762789_marshal_com_back(const GameItem_t806762789_marshaled_com& marshaled, GameItem_t806762789& unmarshaled);
extern "C" void GameItem_t806762789_marshal_com_cleanup(GameItem_t806762789_marshaled_com& marshaled);
