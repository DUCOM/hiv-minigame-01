﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Immunogenicity
struct UI_Immunogenicity_t1529844486;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity_States3735768305.h"

// System.Void UI_Immunogenicity::.ctor()
extern "C"  void UI_Immunogenicity__ctor_m495285335 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UI_Immunogenicity::get_percentage()
extern "C"  float UI_Immunogenicity_get_percentage_m2444515390 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::ChangeState(UI_Immunogenicity/States)
extern "C"  void UI_Immunogenicity_ChangeState_m614690109 (UI_Immunogenicity_t1529844486 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::Add(System.Single)
extern "C"  void UI_Immunogenicity_Add_m607723539 (UI_Immunogenicity_t1529844486 * __this, float ___amount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::RefreshMaximum()
extern "C"  void UI_Immunogenicity_RefreshMaximum_m2251339466 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::ResetProgress()
extern "C"  void UI_Immunogenicity_ResetProgress_m748700083 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::Start()
extern "C"  void UI_Immunogenicity_Start_m1789039563 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::OnGUI()
extern "C"  void UI_Immunogenicity_OnGUI_m3758604645 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Immunogenicity::Update()
extern "C"  void UI_Immunogenicity_Update_m1396367828 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
