﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Sound>
struct List_1_t195838065;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioDatabase
struct  AudioDatabase_t2585396589  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Sound> AudioDatabase::soundList
	List_1_t195838065 * ___soundList_2;
	// System.Boolean AudioDatabase::playOnAwake
	bool ___playOnAwake_3;
	// System.Int32 AudioDatabase::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.AudioSource AudioDatabase::source
	AudioSource_t1135106623 * ___source_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AudioDatabase::indexDict
	Dictionary_2_t3986656710 * ___indexDict_6;

public:
	inline static int32_t get_offset_of_soundList_2() { return static_cast<int32_t>(offsetof(AudioDatabase_t2585396589, ___soundList_2)); }
	inline List_1_t195838065 * get_soundList_2() const { return ___soundList_2; }
	inline List_1_t195838065 ** get_address_of_soundList_2() { return &___soundList_2; }
	inline void set_soundList_2(List_1_t195838065 * value)
	{
		___soundList_2 = value;
		Il2CppCodeGenWriteBarrier(&___soundList_2, value);
	}

	inline static int32_t get_offset_of_playOnAwake_3() { return static_cast<int32_t>(offsetof(AudioDatabase_t2585396589, ___playOnAwake_3)); }
	inline bool get_playOnAwake_3() const { return ___playOnAwake_3; }
	inline bool* get_address_of_playOnAwake_3() { return &___playOnAwake_3; }
	inline void set_playOnAwake_3(bool value)
	{
		___playOnAwake_3 = value;
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(AudioDatabase_t2585396589, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_source_5() { return static_cast<int32_t>(offsetof(AudioDatabase_t2585396589, ___source_5)); }
	inline AudioSource_t1135106623 * get_source_5() const { return ___source_5; }
	inline AudioSource_t1135106623 ** get_address_of_source_5() { return &___source_5; }
	inline void set_source_5(AudioSource_t1135106623 * value)
	{
		___source_5 = value;
		Il2CppCodeGenWriteBarrier(&___source_5, value);
	}

	inline static int32_t get_offset_of_indexDict_6() { return static_cast<int32_t>(offsetof(AudioDatabase_t2585396589, ___indexDict_6)); }
	inline Dictionary_2_t3986656710 * get_indexDict_6() const { return ___indexDict_6; }
	inline Dictionary_2_t3986656710 ** get_address_of_indexDict_6() { return &___indexDict_6; }
	inline void set_indexDict_6(Dictionary_2_t3986656710 * value)
	{
		___indexDict_6 = value;
		Il2CppCodeGenWriteBarrier(&___indexDict_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
