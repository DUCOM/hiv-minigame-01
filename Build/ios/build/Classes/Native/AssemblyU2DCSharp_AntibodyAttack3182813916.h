﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_AntibodyState3996126191.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AntibodyAttack
struct  AntibodyAttack_t3182813916  : public AntibodyState_t3996126191
{
public:
	// UnityEngine.Transform AntibodyAttack::destination
	Transform_t3275118058 * ___destination_4;

public:
	inline static int32_t get_offset_of_destination_4() { return static_cast<int32_t>(offsetof(AntibodyAttack_t3182813916, ___destination_4)); }
	inline Transform_t3275118058 * get_destination_4() const { return ___destination_4; }
	inline Transform_t3275118058 ** get_address_of_destination_4() { return &___destination_4; }
	inline void set_destination_4(Transform_t3275118058 * value)
	{
		___destination_4 = value;
		Il2CppCodeGenWriteBarrier(&___destination_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
