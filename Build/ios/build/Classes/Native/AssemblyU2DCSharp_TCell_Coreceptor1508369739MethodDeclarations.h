﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Coreceptor
struct TCell_Coreceptor_t1508369739;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"

// System.Void TCell_Coreceptor::.ctor()
extern "C"  void TCell_Coreceptor__ctor_m3581639614 (TCell_Coreceptor_t1508369739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism/CoReceptorType TCell_Coreceptor::get_type()
extern "C"  int32_t TCell_Coreceptor_get_type_m878847845 (TCell_Coreceptor_t1508369739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Coreceptor::set_type(Tropism/CoReceptorType)
extern "C"  void TCell_Coreceptor_set_type_m130603720 (TCell_Coreceptor_t1508369739 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpriteRenderer TCell_Coreceptor::get_spriteRender()
extern "C"  SpriteRenderer_t1209076198 * TCell_Coreceptor_get_spriteRender_m838108580 (TCell_Coreceptor_t1508369739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
