﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController
struct GameController_t3607102586;
// HIV
struct HIV_t2481767745;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HIV2481767745.h"
#include "AssemblyU2DCSharp_GameController_SwipeType864748391.h"

// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV GameController::get_selected()
extern "C"  HIV_t2481767745 * GameController_get_selected_m2687499813 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::set_selected(HIV)
extern "C"  void GameController_set_selected_m2123804800 (GameController_t3607102586 * __this, HIV_t2481767745 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnLeftClick()
extern "C"  void GameController_OnLeftClick_m3920841173 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnLeftDrag()
extern "C"  void GameController_OnLeftDrag_m4000772339 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnLeftRelease()
extern "C"  void GameController_OnLeftRelease_m4236716976 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnRightClick()
extern "C"  void GameController_OnRightClick_m3364544638 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnRightDrag()
extern "C"  void GameController_OnRightDrag_m2248094878 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnRightRelease()
extern "C"  void GameController_OnRightRelease_m2387278377 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::OnSwipe(GameController/SwipeType)
extern "C"  void GameController_OnSwipe_m2976886677 (GameController_t3607102586 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::Update()
extern "C"  void GameController_Update_m1556003900 (GameController_t3607102586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
