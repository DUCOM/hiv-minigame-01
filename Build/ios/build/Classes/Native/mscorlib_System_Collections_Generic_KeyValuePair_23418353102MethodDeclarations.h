﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23418353102.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3812244453_gshared (KeyValuePair_2_t3418353102 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3812244453(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3418353102 *, int32_t, float, const MethodInfo*))KeyValuePair_2__ctor_m3812244453_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2327451231_gshared (KeyValuePair_2_t3418353102 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2327451231(__this, method) ((  int32_t (*) (KeyValuePair_2_t3418353102 *, const MethodInfo*))KeyValuePair_2_get_Key_m2327451231_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3119416430_gshared (KeyValuePair_2_t3418353102 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3119416430(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3418353102 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3119416430_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m1325100383_gshared (KeyValuePair_2_t3418353102 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1325100383(__this, method) ((  float (*) (KeyValuePair_2_t3418353102 *, const MethodInfo*))KeyValuePair_2_get_Value_m1325100383_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1340868062_gshared (KeyValuePair_2_t3418353102 * __this, float ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1340868062(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3418353102 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m1340868062_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3303530920_gshared (KeyValuePair_2_t3418353102 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3303530920(__this, method) ((  String_t* (*) (KeyValuePair_2_t3418353102 *, const MethodInfo*))KeyValuePair_2_ToString_m3303530920_gshared)(__this, method)
