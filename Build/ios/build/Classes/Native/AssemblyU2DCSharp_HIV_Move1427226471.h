﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HIV
struct HIV_t2481767745;

#include "AssemblyU2DCSharp_ObjectMovement2D2992076704.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Move
struct  HIV_Move_t1427226471  : public ObjectMovement2D_t2992076704
{
public:
	// HIV HIV_Move::main
	HIV_t2481767745 * ___main_10;

public:
	inline static int32_t get_offset_of_main_10() { return static_cast<int32_t>(offsetof(HIV_Move_t1427226471, ___main_10)); }
	inline HIV_t2481767745 * get_main_10() const { return ___main_10; }
	inline HIV_t2481767745 ** get_address_of_main_10() { return &___main_10; }
	inline void set_main_10(HIV_t2481767745 * value)
	{
		___main_10 = value;
		Il2CppCodeGenWriteBarrier(&___main_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
