﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_HIV_State57600565.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Selected
struct  HIV_Selected_t1107388507  : public HIV_State_t57600565
{
public:
	// UnityEngine.Vector3 HIV_Selected::destination
	Vector3_t2243707580  ___destination_4;
	// UnityEngine.Rect HIV_Selected::slowdownBorder
	Rect_t3681755626  ___slowdownBorder_5;

public:
	inline static int32_t get_offset_of_destination_4() { return static_cast<int32_t>(offsetof(HIV_Selected_t1107388507, ___destination_4)); }
	inline Vector3_t2243707580  get_destination_4() const { return ___destination_4; }
	inline Vector3_t2243707580 * get_address_of_destination_4() { return &___destination_4; }
	inline void set_destination_4(Vector3_t2243707580  value)
	{
		___destination_4 = value;
	}

	inline static int32_t get_offset_of_slowdownBorder_5() { return static_cast<int32_t>(offsetof(HIV_Selected_t1107388507, ___slowdownBorder_5)); }
	inline Rect_t3681755626  get_slowdownBorder_5() const { return ___slowdownBorder_5; }
	inline Rect_t3681755626 * get_address_of_slowdownBorder_5() { return &___slowdownBorder_5; }
	inline void set_slowdownBorder_5(Rect_t3681755626  value)
	{
		___slowdownBorder_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
