﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity_States3735768305.h"
#include "AssemblyU2DCSharp_UI_Level2621343419.h"
#include "AssemblyU2DCSharp_UI_Pause1107290467.h"
#include "AssemblyU2DCSharp_UI_Pause_States3339147058.h"
#include "AssemblyU2DCSharp_UI_Population2609643076.h"
#include "AssemblyU2DCSharp_UI_Resume2961944548.h"
#include "AssemblyU2DCSharp_UI_Score2043792049.h"
#include "AssemblyU2DCSharp_UI_TryAgain3222824968.h"
#include "AssemblyU2DCSharp_UI_World_Filler108322328.h"
#include "AssemblyU2DCSharp_BackQuitCheck2230274900.h"
#include "AssemblyU2DCSharp_MainMenuOption4210779777.h"
#include "AssemblyU2DCSharp_QuitButton1107985349.h"
#include "AssemblyU2DCSharp_QuitConfirm3413857337.h"
#include "AssemblyU2DCSharp_UI_Developer_Button3399765548.h"
#include "AssemblyU2DCSharp_UI_Developer_Stats2682907225.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "AssemblyU2DCSharp_Controller_LeftState2928884177.h"
#include "AssemblyU2DCSharp_Controller_RightState861943514.h"
#include "AssemblyU2DCSharp_Game1600141214.h"
#include "AssemblyU2DCSharp_GameItem806762789.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_GameController_SwipeType864748391.h"
#include "AssemblyU2DCSharp_GameInitializer3613149210.h"
#include "AssemblyU2DCSharp_SwipeController751295678.h"
#include "AssemblyU2DCSharp_AudioDatabase2585396589.h"
#include "AssemblyU2DCSharp_Sound826716933.h"
#include "AssemblyU2DCSharp_Sound_SoundType2077149641.h"
#include "AssemblyU2DCSharp_ErrorCode4164829903.h"
#include "AssemblyU2DCSharp_ErrorCode_VariableCodes489766538.h"
#include "AssemblyU2DCSharp_Functions2564965619.h"
#include "AssemblyU2DCSharp_GroupColors153290803.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"
#include "AssemblyU2DCSharp_ReadOnlyAttribute3809417938.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_TimerList2973412395.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"
#include "AssemblyU2DCSharp_Timer_TimerState3696398080.h"
#include "AssemblyU2DCSharp_HighScoreDisplay899508680.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_DisplayState37128862.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_ListingState675334696.h"
#include "AssemblyU2DCSharp_HighScoreDisplaySingle3977972496.h"
#include "AssemblyU2DCSharp_HighScores3229912699.h"
#include "AssemblyU2DCSharp_ScoreList2288120898.h"
#include "AssemblyU2DCSharp_Score1518975274.h"
#include "AssemblyU2DCSharp_LayerBackground3535234259.h"
#include "AssemblyU2DCSharp_Movement_Basic1705390416.h"
#include "AssemblyU2DCSharp_SceneMove3418568293.h"
#include "AssemblyU2DCSharp_ScoreAdder1918699446.h"
#include "AssemblyU2DCSharp_ScoreDisplay164935196.h"
#include "AssemblyU2DCSharp_Settings4248570851.h"
#include "AssemblyU2DCSharp_SettingList4161291060.h"
#include "AssemblyU2DCSharp_SpriteDisplay1886630049.h"
#include "AssemblyU2DCSharp_SpriteDisplay_SpriteChange4213660865.h"
#include "AssemblyU2DCSharp_Tutorial735545730.h"
#include "AssemblyU2DCSharp_VersionNumberDisplay1254145757.h"
#include "AssemblyU2DCSharp_ZoomController1627556657.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (States_t3735768305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	States_t3735768305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (UI_Level_t2621343419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	UI_Level_t2621343419::get_offset_of_text_2(),
	UI_Level_t2621343419::get_offset_of__prefix_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (UI_Pause_t1107290467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[7] = 
{
	UI_Pause_t1107290467::get_offset_of_state_2(),
	UI_Pause_t1107290467::get_offset_of_pauseSymbol_3(),
	UI_Pause_t1107290467::get_offset_of_playSymbol_4(),
	UI_Pause_t1107290467::get_offset_of_picture_5(),
	UI_Pause_t1107290467::get_offset_of_pauseMenu_6(),
	UI_Pause_t1107290467::get_offset_of__backgroundMusic_7(),
	UI_Pause_t1107290467::get_offset_of_U3CcontrollerU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (States_t3339147058)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[3] = 
{
	States_t3339147058::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (UI_Population_t2609643076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[2] = 
{
	UI_Population_t2609643076::get_offset_of_text_2(),
	UI_Population_t2609643076::get_offset_of_anim_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (UI_Resume_t2961944548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	UI_Resume_t2961944548::get_offset_of_pause_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (UI_Score_t2043792049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	UI_Score_t2043792049::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (UI_TryAgain_t3222824968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[3] = 
{
	UI_TryAgain_t3222824968::get_offset_of_text_2(),
	UI_TryAgain_t3222824968::get_offset_of_pauseText_3(),
	UI_TryAgain_t3222824968::get_offset_of_endGameText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (UI_World_Filler_t108322328), -1, sizeof(UI_World_Filler_t108322328_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1808[2] = 
{
	UI_World_Filler_t108322328_StaticFields::get_offset_of_main_2(),
	UI_World_Filler_t108322328::get_offset_of__filler_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (BackQuitCheck_t2230274900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (MainMenuOption_t4210779777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[1] = 
{
	MainMenuOption_t4210779777::get_offset_of_sceneName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (QuitButton_t1107985349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (QuitConfirm_t3413857337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	QuitConfirm_t3413857337::get_offset_of_buttonList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (UI_Developer_Button_t3399765548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	UI_Developer_Button_t3399765548::get_offset_of_stats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (UI_Developer_Stats_t2682907225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (Controller_t1937198888), -1, sizeof(Controller_t1937198888_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	Controller_t1937198888_StaticFields::get_offset_of_main_2(),
	Controller_t1937198888::get_offset_of_l_state_3(),
	Controller_t1937198888::get_offset_of_r_state_4(),
	Controller_t1937198888::get_offset_of_lastLocation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (LeftState_t2928884177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[5] = 
{
	LeftState_t2928884177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (RightState_t861943514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[5] = 
{
	RightState_t861943514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (Game_t1600141214), -1, sizeof(Game_t1600141214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1818[29] = 
{
	Game_t1600141214::get_offset_of__timers_2(),
	Game_t1600141214::get_offset_of_hiv_3(),
	Game_t1600141214::get_offset_of_hivRadius_4(),
	Game_t1600141214::get_offset_of_hivSpawn_5(),
	Game_t1600141214::get_offset_of_populationMax_6(),
	Game_t1600141214::get_offset_of__hivList_7(),
	Game_t1600141214::get_offset_of__hivScripts_8(),
	Game_t1600141214::get_offset_of__tCell_9(),
	Game_t1600141214::get_offset_of__tCellRadius_10(),
	Game_t1600141214::get_offset_of__tCellRadiusMin_11(),
	Game_t1600141214::get_offset_of__tCellRadiusMax_12(),
	Game_t1600141214::get_offset_of__spawnBorder_13(),
	Game_t1600141214::get_offset_of__tCellPopMax_14(),
	Game_t1600141214::get_offset_of__tCellList_15(),
	Game_t1600141214::get_offset_of__tCellScripts_16(),
	Game_t1600141214::get_offset_of_redBloodCell_17(),
	Game_t1600141214::get_offset_of__rbcList_18(),
	Game_t1600141214::get_offset_of__rbcScripts_19(),
	Game_t1600141214::get_offset_of__rbcPopMax_20(),
	Game_t1600141214::get_offset_of_rbcTimer_21(),
	Game_t1600141214::get_offset_of__antibodyShooter_22(),
	Game_t1600141214::get_offset_of__antibodyList_23(),
	Game_t1600141214::get_offset_of_score_24(),
	Game_t1600141214::get_offset_of_level_25(),
	Game_t1600141214::get_offset_of_bCellObject_26(),
	Game_t1600141214::get_offset_of_bCellTimer_27(),
	Game_t1600141214::get_offset_of_warning_28(),
	Game_t1600141214::get_offset_of_battleTimer_29(),
	Game_t1600141214_StaticFields::get_offset_of_main_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (GameItem_t806762789)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	GameItem_t806762789::get_offset_of_itemReference_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameItem_t806762789::get_offset_of__maximumNumber_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	GameItem_t806762789::get_offset_of__itemList_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (GameController_t3607102586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	GameController_t3607102586::get_offset_of__selected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (SwipeType_t864748391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1821[3] = 
{
	SwipeType_t864748391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (GameInitializer_t3613149210), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (SwipeController_t751295678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[8] = 
{
	SwipeController_t751295678::get_offset_of__swipeTolerance_2(),
	SwipeController_t751295678::get_offset_of__timeDelay_3(),
	SwipeController_t751295678::get_offset_of__initialPosition_4(),
	SwipeController_t751295678::get_offset_of__dx_5(),
	SwipeController_t751295678::get_offset_of__dy_6(),
	SwipeController_t751295678::get_offset_of__dt_7(),
	SwipeController_t751295678::get_offset_of_swipeVelocity_8(),
	SwipeController_t751295678::get_offset_of__initialObject_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (AudioDatabase_t2585396589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[5] = 
{
	AudioDatabase_t2585396589::get_offset_of_soundList_2(),
	AudioDatabase_t2585396589::get_offset_of_playOnAwake_3(),
	AudioDatabase_t2585396589::get_offset_of_currentIndex_4(),
	AudioDatabase_t2585396589::get_offset_of_source_5(),
	AudioDatabase_t2585396589::get_offset_of_indexDict_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Sound_t826716933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[4] = 
{
	Sound_t826716933::get_offset_of_sound_0(),
	Sound_t826716933::get_offset_of_name_1(),
	Sound_t826716933::get_offset_of_type_2(),
	Sound_t826716933::get_offset_of_loop_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (SoundType_t2077149641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[3] = 
{
	SoundType_t2077149641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ErrorCode_t4164829903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (VariableCodes_t489766538)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1828[2] = 
{
	VariableCodes_t489766538::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Functions_t2564965619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (GroupColors_t153290803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[4] = 
{
	GroupColors_t153290803::get_offset_of_spriteList_2(),
	GroupColors_t153290803::get_offset_of_imageList_3(),
	GroupColors_t153290803::get_offset_of_common_4(),
	GroupColors_t153290803::get_offset_of_commonColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (SpriteRender_t2828817349)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[2] = 
{
	SpriteRender_t2828817349::get_offset_of_render_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteRender_t2828817349::get_offset_of__color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (ImageRender_t3780187187)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	ImageRender_t3780187187::get_offset_of_render_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageRender_t3780187187::get_offset_of__color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (ReadOnlyAttribute_t3809417938), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (Rotate_t4255939431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[1] = 
{
	Rotate_t4255939431::get_offset_of_rotationSpeed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (TimerList_t2973412395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	TimerList_t2973412395::get_offset_of_timers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (Timer_t2917042437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[5] = 
{
	Timer_t2917042437::get_offset_of_duration_0(),
	Timer_t2917042437::get_offset_of__name_1(),
	Timer_t2917042437::get_offset_of_state_2(),
	Timer_t2917042437::get_offset_of_loop_3(),
	Timer_t2917042437::get_offset_of__time_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (TimerState_t3696398080)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1837[4] = 
{
	TimerState_t3696398080::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (HighScoreDisplay_t899508680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[6] = 
{
	HighScoreDisplay_t899508680::get_offset_of_text_2(),
	HighScoreDisplay_t899508680::get_offset_of_display_3(),
	HighScoreDisplay_t899508680::get_offset_of_show_4(),
	HighScoreDisplay_t899508680::get_offset_of_lowNumber_5(),
	HighScoreDisplay_t899508680::get_offset_of_highNumber_6(),
	HighScoreDisplay_t899508680::get_offset_of_spaces_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (DisplayState_t37128862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1839[3] = 
{
	DisplayState_t37128862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (ListingState_t675334696)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1840[3] = 
{
	ListingState_t675334696::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (HighScoreDisplaySingle_t3977972496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[1] = 
{
	HighScoreDisplaySingle_t3977972496::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (HighScores_t3229912699), -1, sizeof(HighScores_t3229912699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1842[4] = 
{
	0,
	HighScores_t3229912699_StaticFields::get_offset_of__filePath_1(),
	HighScores_t3229912699::get_offset_of__highScores_2(),
	HighScores_t3229912699_StaticFields::get_offset_of_main_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (ScoreList_t2288120898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[1] = 
{
	ScoreList_t2288120898::get_offset_of_highScores_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (Score_t1518975274)+ sizeof (Il2CppObject), sizeof(Score_t1518975274 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	Score_t1518975274::get_offset_of_score_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (LayerBackground_t3535234259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[8] = 
{
	LayerBackground_t3535234259::get_offset_of_render_2(),
	LayerBackground_t3535234259::get_offset_of_picture_3(),
	LayerBackground_t3535234259::get_offset_of_layer_4(),
	LayerBackground_t3535234259::get_offset_of_render_width_5(),
	LayerBackground_t3535234259::get_offset_of_render_height_6(),
	LayerBackground_t3535234259::get_offset_of__backgroundBlend_7(),
	LayerBackground_t3535234259::get_offset_of__backgroundSprites_8(),
	LayerBackground_t3535234259::get_offset_of_cameraPosPrev_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (Movement_Basic_t1705390416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[8] = 
{
	Movement_Basic_t1705390416::get_offset_of_moveSpeed_2(),
	Movement_Basic_t1705390416::get_offset_of__rb_3(),
	Movement_Basic_t1705390416::get_offset_of__start_4(),
	Movement_Basic_t1705390416::get_offset_of__startingVelocity_5(),
	Movement_Basic_t1705390416::get_offset_of_up_6(),
	Movement_Basic_t1705390416::get_offset_of_down_7(),
	Movement_Basic_t1705390416::get_offset_of_left_8(),
	Movement_Basic_t1705390416::get_offset_of_right_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (SceneMove_t3418568293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	SceneMove_t3418568293::get_offset_of_sceneName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (ScoreAdder_t1918699446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	ScoreAdder_t1918699446::get_offset_of_input_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ScoreDisplay_t164935196), -1, sizeof(ScoreDisplay_t164935196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[2] = 
{
	ScoreDisplay_t164935196_StaticFields::get_offset_of_main_2(),
	ScoreDisplay_t164935196::get_offset_of__anim_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Settings_t4248570851), -1, sizeof(Settings_t4248570851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	Settings_t4248570851::get_offset_of_VolumeSlider_2(),
	Settings_t4248570851_StaticFields::get_offset_of__settings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (SettingList_t4161291060)+ sizeof (Il2CppObject), sizeof(SettingList_t4161291060 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1851[3] = 
{
	SettingList_t4161291060::get_offset_of_volume_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SettingList_t4161291060::get_offset_of_musicVolume_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SettingList_t4161291060::get_offset_of_effectsVolume_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (SpriteDisplay_t1886630049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[5] = 
{
	SpriteDisplay_t1886630049::get_offset_of_spriteRender_2(),
	SpriteDisplay_t1886630049::get_offset_of_spriteList_3(),
	SpriteDisplay_t1886630049::get_offset_of_index_4(),
	SpriteDisplay_t1886630049::get_offset_of_up_5(),
	SpriteDisplay_t1886630049::get_offset_of_down_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (SpriteChange_t4213660865)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1853[4] = 
{
	SpriteChange_t4213660865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (Tutorial_t735545730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[5] = 
{
	Tutorial_t735545730::get_offset_of_anim_2(),
	Tutorial_t735545730::get_offset_of_gameSceneName_3(),
	Tutorial_t735545730::get_offset_of_next_4(),
	Tutorial_t735545730::get_offset_of_prev_5(),
	Tutorial_t735545730::get_offset_of_main_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (VersionNumberDisplay_t1254145757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (ZoomController_t1627556657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1857[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
