﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Membrane
struct Membrane_t1623556481;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void Membrane::.ctor()
extern "C"  void Membrane__ctor_m2226126012 (Membrane_t1623556481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics Membrane::get_STAT_CHANGE()
extern "C"  Statistics_t2537377007  Membrane_get_STAT_CHANGE_m3156592102 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Membrane::ChangeStats()
extern "C"  void Membrane_ChangeStats_m548636383 (Membrane_t1623556481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
