﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Piece
struct HIV_Piece_t1005595762;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void HIV_Piece::.ctor()
extern "C"  void HIV_Piece__ctor_m2599885317 (HIV_Piece_t1005595762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV_Piece::get_color()
extern "C"  Color_t2020392075  HIV_Piece_get_color_m1290788950 (HIV_Piece_t1005595762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Piece::set_color(UnityEngine.Color)
extern "C"  void HIV_Piece_set_color_m3569103183 (HIV_Piece_t1005595762 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpriteRenderer HIV_Piece::get_spriteRender()
extern "C"  SpriteRenderer_t1209076198 * HIV_Piece_get_spriteRender_m2674879059 (HIV_Piece_t1005595762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics HIV_Piece::get_stats()
extern "C"  Statistics_t2537377007  HIV_Piece_get_stats_m2359098831 (HIV_Piece_t1005595762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
