﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreList
struct ScoreList_t2288120898;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreList::.ctor()
extern "C"  void ScoreList__ctor_m3134941225 (ScoreList_t2288120898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ScoreList::ToString()
extern "C"  String_t* ScoreList_ToString_m2504232022 (ScoreList_t2288120898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScoreList::Sort(System.Int32)
extern "C"  int32_t ScoreList_Sort_m1163570930 (ScoreList_t2288120898 * __this, int32_t ___trackingIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScoreList::MergeSort(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t ScoreList_MergeSort_m220785364 (ScoreList_t2288120898 * __this, int32_t ___lo0, int32_t ___hi1, int32_t ___track2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
