﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_GP41
struct UI_GP41_t2232403251;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void UI_GP41::.ctor()
extern "C"  void UI_GP41__ctor_m2264483998 (UI_GP41_t2232403251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UI_GP41::get_color()
extern "C"  Color_t2020392075  UI_GP41_get_color_m360088967 (UI_GP41_t2232403251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_GP41::set_color(UnityEngine.Color)
extern "C"  void UI_GP41_set_color_m896254274 (UI_GP41_t2232403251 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
