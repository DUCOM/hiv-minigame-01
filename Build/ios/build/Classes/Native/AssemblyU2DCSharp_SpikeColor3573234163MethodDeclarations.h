﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void SpikeColor::.ctor(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color)
extern "C"  void SpikeColor__ctor_m2112352072 (SpikeColor_t3573234163 * __this, Color_t2020392075  ____gp410, Color_t2020392075  ____gp120_l1, Color_t2020392075  ____gp120_r2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
