﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HIV
struct HIV_t2481767745;
// TCell
struct TCell_t1815110036;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_State
struct  HIV_State_t57600565  : public MonoBehaviour_t1158329972
{
public:
	// HIV HIV_State::main
	HIV_t2481767745 * ___main_2;
	// TCell HIV_State::host
	TCell_t1815110036 * ___host_3;

public:
	inline static int32_t get_offset_of_main_2() { return static_cast<int32_t>(offsetof(HIV_State_t57600565, ___main_2)); }
	inline HIV_t2481767745 * get_main_2() const { return ___main_2; }
	inline HIV_t2481767745 ** get_address_of_main_2() { return &___main_2; }
	inline void set_main_2(HIV_t2481767745 * value)
	{
		___main_2 = value;
		Il2CppCodeGenWriteBarrier(&___main_2, value);
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(HIV_State_t57600565, ___host_3)); }
	inline TCell_t1815110036 * get_host_3() const { return ___host_3; }
	inline TCell_t1815110036 ** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(TCell_t1815110036 * value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier(&___host_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
