﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Capsid
struct Capsid_t4245142696;

#include "codegen/il2cpp-codegen.h"

// System.Void Capsid::.ctor()
extern "C"  void Capsid__ctor_m2073913303 (Capsid_t4245142696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Capsid::ChangeStats()
extern "C"  void Capsid_ChangeStats_m1349276672 (Capsid_t4245142696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
