﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t69676727;
// UI_HIV_Button
struct UI_HIV_Button_t3180703511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_HIV_Button
struct  UI_HIV_Button_t3180703511  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator UI_HIV_Button::anim
	Animator_t69676727 * ___anim_2;
	// System.Boolean UI_HIV_Button::active
	bool ___active_3;
	// System.Boolean UI_HIV_Button::ready
	bool ___ready_4;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(UI_HIV_Button_t3180703511, ___anim_2)); }
	inline Animator_t69676727 * get_anim_2() const { return ___anim_2; }
	inline Animator_t69676727 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t69676727 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier(&___anim_2, value);
	}

	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(UI_HIV_Button_t3180703511, ___active_3)); }
	inline bool get_active_3() const { return ___active_3; }
	inline bool* get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(bool value)
	{
		___active_3 = value;
	}

	inline static int32_t get_offset_of_ready_4() { return static_cast<int32_t>(offsetof(UI_HIV_Button_t3180703511, ___ready_4)); }
	inline bool get_ready_4() const { return ___ready_4; }
	inline bool* get_address_of_ready_4() { return &___ready_4; }
	inline void set_ready_4(bool value)
	{
		___ready_4 = value;
	}
};

struct UI_HIV_Button_t3180703511_StaticFields
{
public:
	// UI_HIV_Button UI_HIV_Button::main
	UI_HIV_Button_t3180703511 * ___main_5;

public:
	inline static int32_t get_offset_of_main_5() { return static_cast<int32_t>(offsetof(UI_HIV_Button_t3180703511_StaticFields, ___main_5)); }
	inline UI_HIV_Button_t3180703511 * get_main_5() const { return ___main_5; }
	inline UI_HIV_Button_t3180703511 ** get_address_of_main_5() { return &___main_5; }
	inline void set_main_5(UI_HIV_Button_t3180703511 * value)
	{
		___main_5 = value;
		Il2CppCodeGenWriteBarrier(&___main_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
