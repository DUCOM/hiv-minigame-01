﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LayerBackground
struct LayerBackground_t3535234259;

#include "codegen/il2cpp-codegen.h"

// System.Void LayerBackground::.ctor()
extern "C"  void LayerBackground__ctor_m664790520 (LayerBackground_t3535234259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerBackground::Start()
extern "C"  void LayerBackground_Start_m4140090628 (LayerBackground_t3535234259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerBackground::FixedUpdate()
extern "C"  void LayerBackground_FixedUpdate_m846168869 (LayerBackground_t3535234259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerBackground::OnValidate()
extern "C"  void LayerBackground_OnValidate_m1491581041 (LayerBackground_t3535234259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerBackground::Move()
extern "C"  void LayerBackground_Move_m4102430019 (LayerBackground_t3535234259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
