﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Pair
struct TCell_Pair_t2779000105;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_String2029220233.h"

// System.Void TCell_Pair::.ctor()
extern "C"  void TCell_Pair__ctor_m4209467636 (TCell_Pair_t2779000105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism TCell_Pair::get_tropism()
extern "C"  Tropism_t3662836552  TCell_Pair_get_tropism_m4283886040 (TCell_Pair_t2779000105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::set_tropism(Tropism)
extern "C"  void TCell_Pair_set_tropism_m3515168797 (TCell_Pair_t2779000105 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::Scale(System.Single)
extern "C"  void TCell_Pair_Scale_m1323056481 (TCell_Pair_t2779000105 * __this, float ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::MovePivot(UnityEngine.Vector3)
extern "C"  void TCell_Pair_MovePivot_m1340188852 (TCell_Pair_t2779000105 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::MovePivotAndRotate(System.Single,UnityEngine.Quaternion)
extern "C"  void TCell_Pair_MovePivotAndRotate_m1475944733 (TCell_Pair_t2779000105 * __this, float ___radius0, Quaternion_t4030073918  ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::MovePivotAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void TCell_Pair_MovePivotAndRotate_m1238696853 (TCell_Pair_t2779000105 * __this, Vector3_t2243707580  ___translation0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::PivotRotation(UnityEngine.Vector3)
extern "C"  void TCell_Pair_PivotRotation_m1777389265 (TCell_Pair_t2779000105 * __this, Vector3_t2243707580  ___eulerAngles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Pair::SetLayer(System.String)
extern "C"  void TCell_Pair_SetLayer_m3073446271 (TCell_Pair_t2779000105 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
