﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Statistics
struct  Statistics_t2537377007 
{
public:
	// System.Single Statistics::speed
	float ___speed_0;
	// System.Single Statistics::acceleration
	float ___acceleration_1;
	// System.Single Statistics::lifespan
	float ___lifespan_2;
	// System.Single Statistics::docking
	float ___docking_3;
	// System.UInt32 Statistics::replication_rate
	uint32_t ___replication_rate_4;

public:
	inline static int32_t get_offset_of_speed_0() { return static_cast<int32_t>(offsetof(Statistics_t2537377007, ___speed_0)); }
	inline float get_speed_0() const { return ___speed_0; }
	inline float* get_address_of_speed_0() { return &___speed_0; }
	inline void set_speed_0(float value)
	{
		___speed_0 = value;
	}

	inline static int32_t get_offset_of_acceleration_1() { return static_cast<int32_t>(offsetof(Statistics_t2537377007, ___acceleration_1)); }
	inline float get_acceleration_1() const { return ___acceleration_1; }
	inline float* get_address_of_acceleration_1() { return &___acceleration_1; }
	inline void set_acceleration_1(float value)
	{
		___acceleration_1 = value;
	}

	inline static int32_t get_offset_of_lifespan_2() { return static_cast<int32_t>(offsetof(Statistics_t2537377007, ___lifespan_2)); }
	inline float get_lifespan_2() const { return ___lifespan_2; }
	inline float* get_address_of_lifespan_2() { return &___lifespan_2; }
	inline void set_lifespan_2(float value)
	{
		___lifespan_2 = value;
	}

	inline static int32_t get_offset_of_docking_3() { return static_cast<int32_t>(offsetof(Statistics_t2537377007, ___docking_3)); }
	inline float get_docking_3() const { return ___docking_3; }
	inline float* get_address_of_docking_3() { return &___docking_3; }
	inline void set_docking_3(float value)
	{
		___docking_3 = value;
	}

	inline static int32_t get_offset_of_replication_rate_4() { return static_cast<int32_t>(offsetof(Statistics_t2537377007, ___replication_rate_4)); }
	inline uint32_t get_replication_rate_4() const { return ___replication_rate_4; }
	inline uint32_t* get_address_of_replication_rate_4() { return &___replication_rate_4; }
	inline void set_replication_rate_4(uint32_t value)
	{
		___replication_rate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
