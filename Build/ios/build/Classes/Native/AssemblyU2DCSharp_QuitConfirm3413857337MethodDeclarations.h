﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuitConfirm
struct QuitConfirm_t3413857337;

#include "codegen/il2cpp-codegen.h"

// System.Void QuitConfirm::.ctor()
extern "C"  void QuitConfirm__ctor_m948758264 (QuitConfirm_t3413857337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuitConfirm::Deactivate()
extern "C"  void QuitConfirm_Deactivate_m2175662016 (QuitConfirm_t3413857337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuitConfirm::Update()
extern "C"  void QuitConfirm_Update_m3411984893 (QuitConfirm_t3413857337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
