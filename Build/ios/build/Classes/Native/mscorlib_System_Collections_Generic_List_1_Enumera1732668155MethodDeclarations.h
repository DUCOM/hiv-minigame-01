﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SpriteRender>
struct List_1_t2197938481;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1732668155.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SpriteRender>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3253933660_gshared (Enumerator_t1732668155 * __this, List_1_t2197938481 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3253933660(__this, ___l0, method) ((  void (*) (Enumerator_t1732668155 *, List_1_t2197938481 *, const MethodInfo*))Enumerator__ctor_m3253933660_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SpriteRender>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2957312718_gshared (Enumerator_t1732668155 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2957312718(__this, method) ((  void (*) (Enumerator_t1732668155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2957312718_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SpriteRender>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3295668232_gshared (Enumerator_t1732668155 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3295668232(__this, method) ((  Il2CppObject * (*) (Enumerator_t1732668155 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3295668232_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SpriteRender>::Dispose()
extern "C"  void Enumerator_Dispose_m408052341_gshared (Enumerator_t1732668155 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m408052341(__this, method) ((  void (*) (Enumerator_t1732668155 *, const MethodInfo*))Enumerator_Dispose_m408052341_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SpriteRender>::VerifyState()
extern "C"  void Enumerator_VerifyState_m11320914_gshared (Enumerator_t1732668155 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m11320914(__this, method) ((  void (*) (Enumerator_t1732668155 *, const MethodInfo*))Enumerator_VerifyState_m11320914_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SpriteRender>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m91496003_gshared (Enumerator_t1732668155 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m91496003(__this, method) ((  bool (*) (Enumerator_t1732668155 *, const MethodInfo*))Enumerator_MoveNext_m91496003_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SpriteRender>::get_Current()
extern "C"  SpriteRender_t2828817349  Enumerator_get_Current_m2749088815_gshared (Enumerator_t1732668155 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2749088815(__this, method) ((  SpriteRender_t2828817349  (*) (Enumerator_t1732668155 *, const MethodInfo*))Enumerator_get_Current_m2749088815_gshared)(__this, method)
