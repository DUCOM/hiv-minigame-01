﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SettingList4161291060.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Settings
struct  Settings_t4248570851  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Slider Settings::VolumeSlider
	Slider_t297367283 * ___VolumeSlider_2;

public:
	inline static int32_t get_offset_of_VolumeSlider_2() { return static_cast<int32_t>(offsetof(Settings_t4248570851, ___VolumeSlider_2)); }
	inline Slider_t297367283 * get_VolumeSlider_2() const { return ___VolumeSlider_2; }
	inline Slider_t297367283 ** get_address_of_VolumeSlider_2() { return &___VolumeSlider_2; }
	inline void set_VolumeSlider_2(Slider_t297367283 * value)
	{
		___VolumeSlider_2 = value;
		Il2CppCodeGenWriteBarrier(&___VolumeSlider_2, value);
	}
};

struct Settings_t4248570851_StaticFields
{
public:
	// SettingList Settings::_settings
	SettingList_t4161291060  ____settings_3;

public:
	inline static int32_t get_offset_of__settings_3() { return static_cast<int32_t>(offsetof(Settings_t4248570851_StaticFields, ____settings_3)); }
	inline SettingList_t4161291060  get__settings_3() const { return ____settings_3; }
	inline SettingList_t4161291060 * get_address_of__settings_3() { return &____settings_3; }
	inline void set__settings_3(SettingList_t4161291060  value)
	{
		____settings_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
