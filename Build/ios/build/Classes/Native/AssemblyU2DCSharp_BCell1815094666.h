﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<AntibodyShooter>
struct List_1_t3535571088;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BCell
struct  BCell_t1815094666  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BCell::_shooter
	GameObject_t1756533147 * ____shooter_2;
	// UnityEngine.GameObject BCell::_master
	GameObject_t1756533147 * ____master_3;
	// System.Collections.Generic.List`1<AntibodyShooter> BCell::_shooters
	List_1_t3535571088 * ____shooters_4;
	// System.UInt32 BCell::_number
	uint32_t ____number_5;
	// System.Single BCell::_radius
	float ____radius_6;
	// System.Single BCell::_rotationSpeed
	float ____rotationSpeed_7;
	// System.Int32 BCell::currentIndex
	int32_t ___currentIndex_8;

public:
	inline static int32_t get_offset_of__shooter_2() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ____shooter_2)); }
	inline GameObject_t1756533147 * get__shooter_2() const { return ____shooter_2; }
	inline GameObject_t1756533147 ** get_address_of__shooter_2() { return &____shooter_2; }
	inline void set__shooter_2(GameObject_t1756533147 * value)
	{
		____shooter_2 = value;
		Il2CppCodeGenWriteBarrier(&____shooter_2, value);
	}

	inline static int32_t get_offset_of__master_3() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ____master_3)); }
	inline GameObject_t1756533147 * get__master_3() const { return ____master_3; }
	inline GameObject_t1756533147 ** get_address_of__master_3() { return &____master_3; }
	inline void set__master_3(GameObject_t1756533147 * value)
	{
		____master_3 = value;
		Il2CppCodeGenWriteBarrier(&____master_3, value);
	}

	inline static int32_t get_offset_of__shooters_4() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ____shooters_4)); }
	inline List_1_t3535571088 * get__shooters_4() const { return ____shooters_4; }
	inline List_1_t3535571088 ** get_address_of__shooters_4() { return &____shooters_4; }
	inline void set__shooters_4(List_1_t3535571088 * value)
	{
		____shooters_4 = value;
		Il2CppCodeGenWriteBarrier(&____shooters_4, value);
	}

	inline static int32_t get_offset_of__number_5() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ____number_5)); }
	inline uint32_t get__number_5() const { return ____number_5; }
	inline uint32_t* get_address_of__number_5() { return &____number_5; }
	inline void set__number_5(uint32_t value)
	{
		____number_5 = value;
	}

	inline static int32_t get_offset_of__radius_6() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ____radius_6)); }
	inline float get__radius_6() const { return ____radius_6; }
	inline float* get_address_of__radius_6() { return &____radius_6; }
	inline void set__radius_6(float value)
	{
		____radius_6 = value;
	}

	inline static int32_t get_offset_of__rotationSpeed_7() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ____rotationSpeed_7)); }
	inline float get__rotationSpeed_7() const { return ____rotationSpeed_7; }
	inline float* get_address_of__rotationSpeed_7() { return &____rotationSpeed_7; }
	inline void set__rotationSpeed_7(float value)
	{
		____rotationSpeed_7 = value;
	}

	inline static int32_t get_offset_of_currentIndex_8() { return static_cast<int32_t>(offsetof(BCell_t1815094666, ___currentIndex_8)); }
	inline int32_t get_currentIndex_8() const { return ___currentIndex_8; }
	inline int32_t* get_address_of_currentIndex_8() { return &___currentIndex_8; }
	inline void set_currentIndex_8(int32_t value)
	{
		___currentIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
