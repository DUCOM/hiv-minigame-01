﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t69676727;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tutorial
struct  Tutorial_t735545730  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator Tutorial::anim
	Animator_t69676727 * ___anim_2;
	// System.String Tutorial::gameSceneName
	String_t* ___gameSceneName_3;
	// System.Boolean Tutorial::next
	bool ___next_4;
	// System.Boolean Tutorial::prev
	bool ___prev_5;
	// System.Boolean Tutorial::main
	bool ___main_6;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(Tutorial_t735545730, ___anim_2)); }
	inline Animator_t69676727 * get_anim_2() const { return ___anim_2; }
	inline Animator_t69676727 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t69676727 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier(&___anim_2, value);
	}

	inline static int32_t get_offset_of_gameSceneName_3() { return static_cast<int32_t>(offsetof(Tutorial_t735545730, ___gameSceneName_3)); }
	inline String_t* get_gameSceneName_3() const { return ___gameSceneName_3; }
	inline String_t** get_address_of_gameSceneName_3() { return &___gameSceneName_3; }
	inline void set_gameSceneName_3(String_t* value)
	{
		___gameSceneName_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameSceneName_3, value);
	}

	inline static int32_t get_offset_of_next_4() { return static_cast<int32_t>(offsetof(Tutorial_t735545730, ___next_4)); }
	inline bool get_next_4() const { return ___next_4; }
	inline bool* get_address_of_next_4() { return &___next_4; }
	inline void set_next_4(bool value)
	{
		___next_4 = value;
	}

	inline static int32_t get_offset_of_prev_5() { return static_cast<int32_t>(offsetof(Tutorial_t735545730, ___prev_5)); }
	inline bool get_prev_5() const { return ___prev_5; }
	inline bool* get_address_of_prev_5() { return &___prev_5; }
	inline void set_prev_5(bool value)
	{
		___prev_5 = value;
	}

	inline static int32_t get_offset_of_main_6() { return static_cast<int32_t>(offsetof(Tutorial_t735545730, ___main_6)); }
	inline bool get_main_6() const { return ___main_6; }
	inline bool* get_address_of_main_6() { return &___main_6; }
	inline void set_main_6(bool value)
	{
		___main_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
