﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_DisplayState37128862.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_ListingState675334696.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighScoreDisplay
struct  HighScoreDisplay_t899508680  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text HighScoreDisplay::text
	Text_t356221433 * ___text_2;
	// HighScoreDisplay/DisplayState HighScoreDisplay::display
	int32_t ___display_3;
	// HighScoreDisplay/ListingState HighScoreDisplay::show
	int32_t ___show_4;
	// System.Int32 HighScoreDisplay::lowNumber
	int32_t ___lowNumber_5;
	// System.Int32 HighScoreDisplay::highNumber
	int32_t ___highNumber_6;
	// System.Int32 HighScoreDisplay::spaces
	int32_t ___spaces_7;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(HighScoreDisplay_t899508680, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_display_3() { return static_cast<int32_t>(offsetof(HighScoreDisplay_t899508680, ___display_3)); }
	inline int32_t get_display_3() const { return ___display_3; }
	inline int32_t* get_address_of_display_3() { return &___display_3; }
	inline void set_display_3(int32_t value)
	{
		___display_3 = value;
	}

	inline static int32_t get_offset_of_show_4() { return static_cast<int32_t>(offsetof(HighScoreDisplay_t899508680, ___show_4)); }
	inline int32_t get_show_4() const { return ___show_4; }
	inline int32_t* get_address_of_show_4() { return &___show_4; }
	inline void set_show_4(int32_t value)
	{
		___show_4 = value;
	}

	inline static int32_t get_offset_of_lowNumber_5() { return static_cast<int32_t>(offsetof(HighScoreDisplay_t899508680, ___lowNumber_5)); }
	inline int32_t get_lowNumber_5() const { return ___lowNumber_5; }
	inline int32_t* get_address_of_lowNumber_5() { return &___lowNumber_5; }
	inline void set_lowNumber_5(int32_t value)
	{
		___lowNumber_5 = value;
	}

	inline static int32_t get_offset_of_highNumber_6() { return static_cast<int32_t>(offsetof(HighScoreDisplay_t899508680, ___highNumber_6)); }
	inline int32_t get_highNumber_6() const { return ___highNumber_6; }
	inline int32_t* get_address_of_highNumber_6() { return &___highNumber_6; }
	inline void set_highNumber_6(int32_t value)
	{
		___highNumber_6 = value;
	}

	inline static int32_t get_offset_of_spaces_7() { return static_cast<int32_t>(offsetof(HighScoreDisplay_t899508680, ___spaces_7)); }
	inline int32_t get_spaces_7() const { return ___spaces_7; }
	inline int32_t* get_address_of_spaces_7() { return &___spaces_7; }
	inline void set_spaces_7(int32_t value)
	{
		___spaces_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
