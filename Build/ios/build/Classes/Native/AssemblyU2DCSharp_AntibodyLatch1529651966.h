﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_AntibodyState3996126191.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AntibodyLatch
struct  AntibodyLatch_t1529651966  : public AntibodyState_t3996126191
{
public:
	// System.Single AntibodyLatch::oldMax
	float ___oldMax_4;
	// UnityEngine.Vector2 AntibodyLatch::displacement
	Vector2_t2243707579  ___displacement_5;
	// System.Single AntibodyLatch::maxTime
	float ___maxTime_6;
	// System.Single AntibodyLatch::_timer
	float ____timer_7;
	// System.Boolean AntibodyLatch::death
	bool ___death_8;
	// System.Int32 AntibodyLatch::maxShakeNumber
	int32_t ___maxShakeNumber_9;

public:
	inline static int32_t get_offset_of_oldMax_4() { return static_cast<int32_t>(offsetof(AntibodyLatch_t1529651966, ___oldMax_4)); }
	inline float get_oldMax_4() const { return ___oldMax_4; }
	inline float* get_address_of_oldMax_4() { return &___oldMax_4; }
	inline void set_oldMax_4(float value)
	{
		___oldMax_4 = value;
	}

	inline static int32_t get_offset_of_displacement_5() { return static_cast<int32_t>(offsetof(AntibodyLatch_t1529651966, ___displacement_5)); }
	inline Vector2_t2243707579  get_displacement_5() const { return ___displacement_5; }
	inline Vector2_t2243707579 * get_address_of_displacement_5() { return &___displacement_5; }
	inline void set_displacement_5(Vector2_t2243707579  value)
	{
		___displacement_5 = value;
	}

	inline static int32_t get_offset_of_maxTime_6() { return static_cast<int32_t>(offsetof(AntibodyLatch_t1529651966, ___maxTime_6)); }
	inline float get_maxTime_6() const { return ___maxTime_6; }
	inline float* get_address_of_maxTime_6() { return &___maxTime_6; }
	inline void set_maxTime_6(float value)
	{
		___maxTime_6 = value;
	}

	inline static int32_t get_offset_of__timer_7() { return static_cast<int32_t>(offsetof(AntibodyLatch_t1529651966, ____timer_7)); }
	inline float get__timer_7() const { return ____timer_7; }
	inline float* get_address_of__timer_7() { return &____timer_7; }
	inline void set__timer_7(float value)
	{
		____timer_7 = value;
	}

	inline static int32_t get_offset_of_death_8() { return static_cast<int32_t>(offsetof(AntibodyLatch_t1529651966, ___death_8)); }
	inline bool get_death_8() const { return ___death_8; }
	inline bool* get_address_of_death_8() { return &___death_8; }
	inline void set_death_8(bool value)
	{
		___death_8 = value;
	}

	inline static int32_t get_offset_of_maxShakeNumber_9() { return static_cast<int32_t>(offsetof(AntibodyLatch_t1529651966, ___maxShakeNumber_9)); }
	inline int32_t get_maxShakeNumber_9() const { return ___maxShakeNumber_9; }
	inline int32_t* get_address_of_maxShakeNumber_9() { return &___maxShakeNumber_9; }
	inline void set_maxShakeNumber_9(int32_t value)
	{
		___maxShakeNumber_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
