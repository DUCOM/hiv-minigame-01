﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<HIV/State,System.Object>
struct Dictionary_2_t405822817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2092355581.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<HIV/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1924231338_gshared (Enumerator_t2092355581 * __this, Dictionary_2_t405822817 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1924231338(__this, ___host0, method) ((  void (*) (Enumerator_t2092355581 *, Dictionary_2_t405822817 *, const MethodInfo*))Enumerator__ctor_m1924231338_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<HIV/State,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m679187501_gshared (Enumerator_t2092355581 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m679187501(__this, method) ((  Il2CppObject * (*) (Enumerator_t2092355581 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m679187501_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<HIV/State,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2211617285_gshared (Enumerator_t2092355581 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2211617285(__this, method) ((  void (*) (Enumerator_t2092355581 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2211617285_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<HIV/State,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3364931466_gshared (Enumerator_t2092355581 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3364931466(__this, method) ((  void (*) (Enumerator_t2092355581 *, const MethodInfo*))Enumerator_Dispose_m3364931466_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<HIV/State,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2250571997_gshared (Enumerator_t2092355581 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2250571997(__this, method) ((  bool (*) (Enumerator_t2092355581 *, const MethodInfo*))Enumerator_MoveNext_m2250571997_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<HIV/State,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2006048135_gshared (Enumerator_t2092355581 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2006048135(__this, method) ((  Il2CppObject * (*) (Enumerator_t2092355581 *, const MethodInfo*))Enumerator_get_Current_m2006048135_gshared)(__this, method)
