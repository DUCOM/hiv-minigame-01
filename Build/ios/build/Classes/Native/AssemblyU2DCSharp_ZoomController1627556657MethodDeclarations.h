﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZoomController
struct ZoomController_t1627556657;
// RenderCamera
struct RenderCamera_t2596733641;
// Controller
struct Controller_t1937198888;

#include "codegen/il2cpp-codegen.h"

// System.Void ZoomController::.ctor()
extern "C"  void ZoomController__ctor_m4063515714 (ZoomController_t1627556657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RenderCamera ZoomController::get_renderCam()
extern "C"  RenderCamera_t2596733641 * ZoomController_get_renderCam_m479667980 (ZoomController_t1627556657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Controller ZoomController::get_controller()
extern "C"  Controller_t1937198888 * ZoomController_get_controller_m2798812710 (ZoomController_t1627556657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ZoomController::get_zoomFactor()
extern "C"  float ZoomController_get_zoomFactor_m3614507305 (ZoomController_t1627556657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomController::set_zoomFactor(System.Single)
extern "C"  void ZoomController_set_zoomFactor_m870427094 (ZoomController_t1627556657 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomController::Start()
extern "C"  void ZoomController_Start_m23304402 (ZoomController_t1627556657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoomController::Update()
extern "C"  void ZoomController_Update_m100697297 (ZoomController_t1627556657 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
