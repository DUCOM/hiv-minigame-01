﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3319840691MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Antibody/State,AntibodyState,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1445924136(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3833283219 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2397721945_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Antibody/State,AntibodyState,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m4272933176(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3833283219 *, int32_t, AntibodyState_t3996126191 *, const MethodInfo*))Transform_1_Invoke_m3382475365_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Antibody/State,AntibodyState,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3089079841(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3833283219 *, int32_t, AntibodyState_t3996126191 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m713810308_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Antibody/State,AntibodyState,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1225962078(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3833283219 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3983141791_gshared)(__this, ___result0, method)
