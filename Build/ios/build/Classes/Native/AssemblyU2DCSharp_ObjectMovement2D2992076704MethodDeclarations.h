﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ObjectMovement2D
struct ObjectMovement2D_t2992076704;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ObjectMovement2D_State1000911910.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void ObjectMovement2D::.ctor()
extern "C"  void ObjectMovement2D__ctor_m2674233803 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ObjectMovement2D/State ObjectMovement2D::get_followState()
extern "C"  int32_t ObjectMovement2D_get_followState_m1966773327 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::set_followState(ObjectMovement2D/State)
extern "C"  void ObjectMovement2D_set_followState_m2160966512 (ObjectMovement2D_t2992076704 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 ObjectMovement2D::get_velocity()
extern "C"  Vector2_t2243707579  ObjectMovement2D_get_velocity_m987348582 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::set_velocity(UnityEngine.Vector2)
extern "C"  void ObjectMovement2D_set_velocity_m1559477545 (ObjectMovement2D_t2992076704 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ObjectMovement2D::get_maxSpeed()
extern "C"  float ObjectMovement2D_get_maxSpeed_m1185142611 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::set_maxSpeed(System.Single)
extern "C"  void ObjectMovement2D_set_maxSpeed_m780069868 (ObjectMovement2D_t2992076704 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ObjectMovement2D::get_speed()
extern "C"  float ObjectMovement2D_get_speed_m1576568683 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ObjectMovement2D::get_multiplier()
extern "C"  float ObjectMovement2D_get_multiplier_m3401748055 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::set_multiplier(System.Single)
extern "C"  void ObjectMovement2D_set_multiplier_m3706769178 (ObjectMovement2D_t2992076704 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::ChangeMultiplier(System.Single)
extern "C"  void ObjectMovement2D_ChangeMultiplier_m4218570319 (ObjectMovement2D_t2992076704 * __this, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::ChangeFollowState(UnityEngine.Transform,System.Single,System.Boolean,System.Boolean)
extern "C"  void ObjectMovement2D_ChangeFollowState_m1251427041 (ObjectMovement2D_t2992076704 * __this, Transform_t3275118058 * ___follow0, float ___multiple1, bool ___teleport2, bool ___trail3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::ChangeFollowState(UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  void ObjectMovement2D_ChangeFollowState_m3934905037 (ObjectMovement2D_t2992076704 * __this, Vector2_t2243707579  ___follow0, float ___multiple1, bool ___teleport2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::MoveTo(UnityEngine.Vector3)
extern "C"  void ObjectMovement2D_MoveTo_m3011449144 (ObjectMovement2D_t2992076704 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::Awake()
extern "C"  void ObjectMovement2D_Awake_m3303377024 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::Start()
extern "C"  void ObjectMovement2D_Start_m2558058847 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::Move()
extern "C"  void ObjectMovement2D_Move_m276962098 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectMovement2D::FixedUpdate()
extern "C"  void ObjectMovement2D_FixedUpdate_m1518841780 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
