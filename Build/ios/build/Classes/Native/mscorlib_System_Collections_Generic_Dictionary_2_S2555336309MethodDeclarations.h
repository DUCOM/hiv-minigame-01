﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>
struct ShimEnumerator_t2555336309;
// System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>
struct Dictionary_2_t2450211488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m294392226_gshared (ShimEnumerator_t2555336309 * __this, Dictionary_2_t2450211488 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m294392226(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2555336309 *, Dictionary_2_t2450211488 *, const MethodInfo*))ShimEnumerator__ctor_m294392226_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3988919257_gshared (ShimEnumerator_t2555336309 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3988919257(__this, method) ((  bool (*) (ShimEnumerator_t2555336309 *, const MethodInfo*))ShimEnumerator_MoveNext_m3988919257_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3327531341_gshared (ShimEnumerator_t2555336309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3327531341(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2555336309 *, const MethodInfo*))ShimEnumerator_get_Entry_m3327531341_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m448802642_gshared (ShimEnumerator_t2555336309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m448802642(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2555336309 *, const MethodInfo*))ShimEnumerator_get_Key_m448802642_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1693267656_gshared (ShimEnumerator_t2555336309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1693267656(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2555336309 *, const MethodInfo*))ShimEnumerator_get_Value_m1693267656_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2876938292_gshared (ShimEnumerator_t2555336309 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2876938292(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2555336309 *, const MethodInfo*))ShimEnumerator_get_Current_m2876938292_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Antibody/State,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m565137472_gshared (ShimEnumerator_t2555336309 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m565137472(__this, method) ((  void (*) (ShimEnumerator_t2555336309 *, const MethodInfo*))ShimEnumerator_Reset_m565137472_gshared)(__this, method)
