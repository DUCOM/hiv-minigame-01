﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3687569611.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"

// System.Void System.Array/InternalEnumerator`1<SpriteRender>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3105241154_gshared (InternalEnumerator_1_t3687569611 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3105241154(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3687569611 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3105241154_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SpriteRender>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1013093606_gshared (InternalEnumerator_1_t3687569611 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1013093606(__this, method) ((  void (*) (InternalEnumerator_1_t3687569611 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1013093606_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SpriteRender>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4160052262_gshared (InternalEnumerator_1_t3687569611 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4160052262(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3687569611 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4160052262_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SpriteRender>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m613955025_gshared (InternalEnumerator_1_t3687569611 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m613955025(__this, method) ((  void (*) (InternalEnumerator_1_t3687569611 *, const MethodInfo*))InternalEnumerator_1_Dispose_m613955025_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SpriteRender>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2247403074_gshared (InternalEnumerator_1_t3687569611 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2247403074(__this, method) ((  bool (*) (InternalEnumerator_1_t3687569611 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2247403074_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SpriteRender>::get_Current()
extern "C"  SpriteRender_t2828817349  InternalEnumerator_1_get_Current_m2261697793_gshared (InternalEnumerator_1_t3687569611 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2261697793(__this, method) ((  SpriteRender_t2828817349  (*) (InternalEnumerator_1_t3687569611 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2261697793_gshared)(__this, method)
