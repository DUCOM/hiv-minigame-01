﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageRender
struct  ImageRender_t3780187187 
{
public:
	// UnityEngine.UI.Image ImageRender::render
	Image_t2042527209 * ___render_0;
	// UnityEngine.Color ImageRender::_color
	Color_t2020392075  ____color_1;

public:
	inline static int32_t get_offset_of_render_0() { return static_cast<int32_t>(offsetof(ImageRender_t3780187187, ___render_0)); }
	inline Image_t2042527209 * get_render_0() const { return ___render_0; }
	inline Image_t2042527209 ** get_address_of_render_0() { return &___render_0; }
	inline void set_render_0(Image_t2042527209 * value)
	{
		___render_0 = value;
		Il2CppCodeGenWriteBarrier(&___render_0, value);
	}

	inline static int32_t get_offset_of__color_1() { return static_cast<int32_t>(offsetof(ImageRender_t3780187187, ____color_1)); }
	inline Color_t2020392075  get__color_1() const { return ____color_1; }
	inline Color_t2020392075 * get_address_of__color_1() { return &____color_1; }
	inline void set__color_1(Color_t2020392075  value)
	{
		____color_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ImageRender
struct ImageRender_t3780187187_marshaled_pinvoke
{
	Image_t2042527209 * ___render_0;
	Color_t2020392075  ____color_1;
};
// Native definition for COM marshalling of ImageRender
struct ImageRender_t3780187187_marshaled_com
{
	Image_t2042527209 * ___render_0;
	Color_t2020392075  ____color_1;
};
