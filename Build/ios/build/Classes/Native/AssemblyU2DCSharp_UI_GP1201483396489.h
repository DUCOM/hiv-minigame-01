﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_GP120
struct  UI_GP120_t1483396489  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image UI_GP120::displayLeft
	Image_t2042527209 * ___displayLeft_2;
	// UnityEngine.UI.Image UI_GP120::displayRight
	Image_t2042527209 * ___displayRight_3;

public:
	inline static int32_t get_offset_of_displayLeft_2() { return static_cast<int32_t>(offsetof(UI_GP120_t1483396489, ___displayLeft_2)); }
	inline Image_t2042527209 * get_displayLeft_2() const { return ___displayLeft_2; }
	inline Image_t2042527209 ** get_address_of_displayLeft_2() { return &___displayLeft_2; }
	inline void set_displayLeft_2(Image_t2042527209 * value)
	{
		___displayLeft_2 = value;
		Il2CppCodeGenWriteBarrier(&___displayLeft_2, value);
	}

	inline static int32_t get_offset_of_displayRight_3() { return static_cast<int32_t>(offsetof(UI_GP120_t1483396489, ___displayRight_3)); }
	inline Image_t2042527209 * get_displayRight_3() const { return ___displayRight_3; }
	inline Image_t2042527209 ** get_address_of_displayRight_3() { return &___displayRight_3; }
	inline void set_displayRight_3(Image_t2042527209 * value)
	{
		___displayRight_3 = value;
		Il2CppCodeGenWriteBarrier(&___displayRight_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
