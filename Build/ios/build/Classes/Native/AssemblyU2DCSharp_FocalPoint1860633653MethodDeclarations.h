﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FocalPoint
struct FocalPoint_t1860633653;
// FocalPointMove
struct FocalPointMove_t3911050876;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void FocalPoint::.ctor()
extern "C"  void FocalPoint__ctor_m3949637992 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FocalPointMove FocalPoint::get_move()
extern "C"  FocalPointMove_t3911050876 * FocalPoint_get_move_m4157530869 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FocalPoint::get_speed()
extern "C"  float FocalPoint_get_speed_m745917210 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FocalPoint::get_maxDistance()
extern "C"  float FocalPoint_get_maxDistance_m2498898468 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FocalPoint::get_distanceCutoff()
extern "C"  float FocalPoint_get_distanceCutoff_m3231606637 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FocalPoint::get_midpointCutoff()
extern "C"  float FocalPoint_get_midpointCutoff_m3392978060 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 FocalPoint::get_position()
extern "C"  Vector3_t2243707580  FocalPoint_get_position_m2433401882 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FocalPoint::GetDistance(UnityEngine.Transform)
extern "C"  float FocalPoint_GetDistance_m593464702 (FocalPoint_t1860633653 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FocalPoint::Awake()
extern "C"  void FocalPoint_Awake_m2063840741 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FocalPoint::Start()
extern "C"  void FocalPoint_Start_m2587931424 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FocalPoint::Update()
extern "C"  void FocalPoint_Update_m2326944033 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FocalPoint::OnDestroy()
extern "C"  void FocalPoint_OnDestroy_m1182612051 (FocalPoint_t1860633653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
