﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyLatch
struct AntibodyLatch_t1529651966;

#include "codegen/il2cpp-codegen.h"

// System.Void AntibodyLatch::.ctor()
extern "C"  void AntibodyLatch__ctor_m1441054387 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyLatch::Kill()
extern "C"  void AntibodyLatch_Kill_m3165421869 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyLatch::OnEnable()
extern "C"  void AntibodyLatch_OnEnable_m3550202639 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyLatch::OnDisable()
extern "C"  void AntibodyLatch_OnDisable_m1595349030 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyLatch::Update()
extern "C"  void AntibodyLatch_Update_m3356296392 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
