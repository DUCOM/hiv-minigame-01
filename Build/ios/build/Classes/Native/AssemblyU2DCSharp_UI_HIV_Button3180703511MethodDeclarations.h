﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_HIV_Button
struct UI_HIV_Button_t3180703511;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_HIV_Button::.ctor()
extern "C"  void UI_HIV_Button__ctor_m2573358074 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV_Button::Awake()
extern "C"  void UI_HIV_Button_Awake_m1137632259 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV_Button::Start()
extern "C"  void UI_HIV_Button_Start_m1282947550 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV_Button::Activate()
extern "C"  void UI_HIV_Button_Activate_m2168596695 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV_Button::Deactivate()
extern "C"  void UI_HIV_Button_Deactivate_m2023060262 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
