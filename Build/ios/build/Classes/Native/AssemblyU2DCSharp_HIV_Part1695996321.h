﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// HIV_Piece
struct HIV_Piece_t1005595762;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Part
struct  HIV_Part_t1695996321  : public Il2CppObject
{
public:
	// UnityEngine.GameObject HIV_Part::prefab
	GameObject_t1756533147 * ___prefab_0;
	// HIV_Piece HIV_Part::_script
	HIV_Piece_t1005595762 * ____script_1;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(HIV_Part_t1695996321, ___prefab_0)); }
	inline GameObject_t1756533147 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t1756533147 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t1756533147 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_0, value);
	}

	inline static int32_t get_offset_of__script_1() { return static_cast<int32_t>(offsetof(HIV_Part_t1695996321, ____script_1)); }
	inline HIV_Piece_t1005595762 * get__script_1() const { return ____script_1; }
	inline HIV_Piece_t1005595762 ** get_address_of__script_1() { return &____script_1; }
	inline void set__script_1(HIV_Piece_t1005595762 * value)
	{
		____script_1 = value;
		Il2CppCodeGenWriteBarrier(&____script_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
