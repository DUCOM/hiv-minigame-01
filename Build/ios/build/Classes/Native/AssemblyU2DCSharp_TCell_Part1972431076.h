﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TCell_Piece
struct TCell_Piece_t3459712141;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell_Part
struct  TCell_Part_t1972431076  : public Il2CppObject
{
public:
	// UnityEngine.GameObject TCell_Part::prefab
	GameObject_t1756533147 * ___prefab_0;
	// UnityEngine.Color TCell_Part::color_
	Color_t2020392075  ___color__1;
	// TCell_Piece TCell_Part::instance
	TCell_Piece_t3459712141 * ___instance_2;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(TCell_Part_t1972431076, ___prefab_0)); }
	inline GameObject_t1756533147 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t1756533147 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t1756533147 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_0, value);
	}

	inline static int32_t get_offset_of_color__1() { return static_cast<int32_t>(offsetof(TCell_Part_t1972431076, ___color__1)); }
	inline Color_t2020392075  get_color__1() const { return ___color__1; }
	inline Color_t2020392075 * get_address_of_color__1() { return &___color__1; }
	inline void set_color__1(Color_t2020392075  value)
	{
		___color__1 = value;
	}

	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(TCell_Part_t1972431076, ___instance_2)); }
	inline TCell_Piece_t3459712141 * get_instance_2() const { return ___instance_2; }
	inline TCell_Piece_t3459712141 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TCell_Piece_t3459712141 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
