﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ObjectMovement2D_State1000911910.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectMovement2D
struct  ObjectMovement2D_t2992076704  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Rigidbody2D ObjectMovement2D::_rb
	Rigidbody2D_t502193897 * ____rb_2;
	// ObjectMovement2D/State ObjectMovement2D::<followState>k__BackingField
	int32_t ___U3CfollowStateU3Ek__BackingField_3;
	// UnityEngine.Transform ObjectMovement2D::followObject
	Transform_t3275118058 * ___followObject_4;
	// UnityEngine.Vector3 ObjectMovement2D::followVector
	Vector3_t2243707580  ___followVector_5;
	// System.Single ObjectMovement2D::<maxSpeed>k__BackingField
	float ___U3CmaxSpeedU3Ek__BackingField_6;
	// System.Single ObjectMovement2D::<multiplier>k__BackingField
	float ___U3CmultiplierU3Ek__BackingField_7;
	// System.Boolean ObjectMovement2D::teleportEnabled
	bool ___teleportEnabled_8;
	// System.Single ObjectMovement2D::distanceThreshhold
	float ___distanceThreshhold_9;

public:
	inline static int32_t get_offset_of__rb_2() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ____rb_2)); }
	inline Rigidbody2D_t502193897 * get__rb_2() const { return ____rb_2; }
	inline Rigidbody2D_t502193897 ** get_address_of__rb_2() { return &____rb_2; }
	inline void set__rb_2(Rigidbody2D_t502193897 * value)
	{
		____rb_2 = value;
		Il2CppCodeGenWriteBarrier(&____rb_2, value);
	}

	inline static int32_t get_offset_of_U3CfollowStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___U3CfollowStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CfollowStateU3Ek__BackingField_3() const { return ___U3CfollowStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CfollowStateU3Ek__BackingField_3() { return &___U3CfollowStateU3Ek__BackingField_3; }
	inline void set_U3CfollowStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CfollowStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_followObject_4() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___followObject_4)); }
	inline Transform_t3275118058 * get_followObject_4() const { return ___followObject_4; }
	inline Transform_t3275118058 ** get_address_of_followObject_4() { return &___followObject_4; }
	inline void set_followObject_4(Transform_t3275118058 * value)
	{
		___followObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___followObject_4, value);
	}

	inline static int32_t get_offset_of_followVector_5() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___followVector_5)); }
	inline Vector3_t2243707580  get_followVector_5() const { return ___followVector_5; }
	inline Vector3_t2243707580 * get_address_of_followVector_5() { return &___followVector_5; }
	inline void set_followVector_5(Vector3_t2243707580  value)
	{
		___followVector_5 = value;
	}

	inline static int32_t get_offset_of_U3CmaxSpeedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___U3CmaxSpeedU3Ek__BackingField_6)); }
	inline float get_U3CmaxSpeedU3Ek__BackingField_6() const { return ___U3CmaxSpeedU3Ek__BackingField_6; }
	inline float* get_address_of_U3CmaxSpeedU3Ek__BackingField_6() { return &___U3CmaxSpeedU3Ek__BackingField_6; }
	inline void set_U3CmaxSpeedU3Ek__BackingField_6(float value)
	{
		___U3CmaxSpeedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CmultiplierU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___U3CmultiplierU3Ek__BackingField_7)); }
	inline float get_U3CmultiplierU3Ek__BackingField_7() const { return ___U3CmultiplierU3Ek__BackingField_7; }
	inline float* get_address_of_U3CmultiplierU3Ek__BackingField_7() { return &___U3CmultiplierU3Ek__BackingField_7; }
	inline void set_U3CmultiplierU3Ek__BackingField_7(float value)
	{
		___U3CmultiplierU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_teleportEnabled_8() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___teleportEnabled_8)); }
	inline bool get_teleportEnabled_8() const { return ___teleportEnabled_8; }
	inline bool* get_address_of_teleportEnabled_8() { return &___teleportEnabled_8; }
	inline void set_teleportEnabled_8(bool value)
	{
		___teleportEnabled_8 = value;
	}

	inline static int32_t get_offset_of_distanceThreshhold_9() { return static_cast<int32_t>(offsetof(ObjectMovement2D_t2992076704, ___distanceThreshhold_9)); }
	inline float get_distanceThreshhold_9() const { return ___distanceThreshhold_9; }
	inline float* get_address_of_distanceThreshhold_9() { return &___distanceThreshhold_9; }
	inline void set_distanceThreshhold_9(float value)
	{
		___distanceThreshhold_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
