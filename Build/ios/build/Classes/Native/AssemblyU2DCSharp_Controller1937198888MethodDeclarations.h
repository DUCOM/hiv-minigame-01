﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Controller
struct Controller_t1937198888;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Awake()
extern "C"  void Controller_Awake_m587736580 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::LeftUpdate()
extern "C"  void Controller_LeftUpdate_m3770109509 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::RightUpdate()
extern "C"  void Controller_RightUpdate_m3131252236 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Controller::get_worldPosition()
extern "C"  Vector3_t2243707580  Controller_get_worldPosition_m2261149775 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Controller::get_screenPosition()
extern "C"  Vector3_t2243707580  Controller_get_screenPosition_m2179745093 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Controller::getSelected()
extern "C"  Transform_t3275118058 * Controller_getSelected_m1390842220 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::LeftClick()
extern "C"  bool Controller_LeftClick_m156288658 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::LeftDrag()
extern "C"  bool Controller_LeftDrag_m794894538 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::LeftRelease()
extern "C"  bool Controller_LeftRelease_m547954749 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::RightClick()
extern "C"  bool Controller_RightClick_m3621887713 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::RightDrag()
extern "C"  bool Controller_RightDrag_m4108065131 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::RightRelease()
extern "C"  bool Controller_RightRelease_m1329783046 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller::GetBack()
extern "C"  bool Controller_GetBack_m1477624146 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Controller::Update()
extern "C"  void Controller_Update_m2428618086 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
