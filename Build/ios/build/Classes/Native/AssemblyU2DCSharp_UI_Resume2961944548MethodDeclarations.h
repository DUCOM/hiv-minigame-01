﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Resume
struct UI_Resume_t2961944548;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Resume::.ctor()
extern "C"  void UI_Resume__ctor_m1556349809 (UI_Resume_t2961944548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Resume::OnEnable()
extern "C"  void UI_Resume_OnEnable_m1803953417 (UI_Resume_t2961944548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Resume::Resume()
extern "C"  void UI_Resume_Resume_m3838652464 (UI_Resume_t2961944548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
