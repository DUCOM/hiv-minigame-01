﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pivot
struct Pivot_t2110476880;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Pivot::.ctor()
extern "C"  void Pivot__ctor_m4141519939 (Pivot_t2110476880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Pivot::get_parent()
extern "C"  GameObject_t1756533147 * Pivot_get_parent_m3646862489 (Pivot_t2110476880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pivot::MoveAbout(System.Single,UnityEngine.Quaternion)
extern "C"  void Pivot_MoveAbout_m3407021153 (Pivot_t2110476880 * __this, float ___radius0, Quaternion_t4030073918  ___angle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pivot::MoveAbout(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Pivot_MoveAbout_m999237689 (Pivot_t2110476880 * __this, Vector3_t2243707580  ___translation0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
