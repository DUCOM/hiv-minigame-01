﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Antibody
struct Antibody_t2931822436;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Weak
struct  HIV_Weak_t1951470088  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject HIV_Weak::bubble
	GameObject_t1756533147 * ___bubble_2;
	// UnityEngine.Color HIV_Weak::gp120_l
	Color_t2020392075  ___gp120_l_3;
	// UnityEngine.Color HIV_Weak::gp120_r
	Color_t2020392075  ___gp120_r_4;
	// UnityEngine.Color HIV_Weak::gp41
	Color_t2020392075  ___gp41_5;
	// System.Int32 HIV_Weak::excludedIndex
	int32_t ___excludedIndex_6;
	// Antibody HIV_Weak::antibody
	Antibody_t2931822436 * ___antibody_7;
	// UnityEngine.Vector3 HIV_Weak::prev_velocity
	Vector3_t2243707580  ___prev_velocity_8;
	// System.Int32 HIV_Weak::<shakeNumber>k__BackingField
	int32_t ___U3CshakeNumberU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_bubble_2() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___bubble_2)); }
	inline GameObject_t1756533147 * get_bubble_2() const { return ___bubble_2; }
	inline GameObject_t1756533147 ** get_address_of_bubble_2() { return &___bubble_2; }
	inline void set_bubble_2(GameObject_t1756533147 * value)
	{
		___bubble_2 = value;
		Il2CppCodeGenWriteBarrier(&___bubble_2, value);
	}

	inline static int32_t get_offset_of_gp120_l_3() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___gp120_l_3)); }
	inline Color_t2020392075  get_gp120_l_3() const { return ___gp120_l_3; }
	inline Color_t2020392075 * get_address_of_gp120_l_3() { return &___gp120_l_3; }
	inline void set_gp120_l_3(Color_t2020392075  value)
	{
		___gp120_l_3 = value;
	}

	inline static int32_t get_offset_of_gp120_r_4() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___gp120_r_4)); }
	inline Color_t2020392075  get_gp120_r_4() const { return ___gp120_r_4; }
	inline Color_t2020392075 * get_address_of_gp120_r_4() { return &___gp120_r_4; }
	inline void set_gp120_r_4(Color_t2020392075  value)
	{
		___gp120_r_4 = value;
	}

	inline static int32_t get_offset_of_gp41_5() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___gp41_5)); }
	inline Color_t2020392075  get_gp41_5() const { return ___gp41_5; }
	inline Color_t2020392075 * get_address_of_gp41_5() { return &___gp41_5; }
	inline void set_gp41_5(Color_t2020392075  value)
	{
		___gp41_5 = value;
	}

	inline static int32_t get_offset_of_excludedIndex_6() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___excludedIndex_6)); }
	inline int32_t get_excludedIndex_6() const { return ___excludedIndex_6; }
	inline int32_t* get_address_of_excludedIndex_6() { return &___excludedIndex_6; }
	inline void set_excludedIndex_6(int32_t value)
	{
		___excludedIndex_6 = value;
	}

	inline static int32_t get_offset_of_antibody_7() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___antibody_7)); }
	inline Antibody_t2931822436 * get_antibody_7() const { return ___antibody_7; }
	inline Antibody_t2931822436 ** get_address_of_antibody_7() { return &___antibody_7; }
	inline void set_antibody_7(Antibody_t2931822436 * value)
	{
		___antibody_7 = value;
		Il2CppCodeGenWriteBarrier(&___antibody_7, value);
	}

	inline static int32_t get_offset_of_prev_velocity_8() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___prev_velocity_8)); }
	inline Vector3_t2243707580  get_prev_velocity_8() const { return ___prev_velocity_8; }
	inline Vector3_t2243707580 * get_address_of_prev_velocity_8() { return &___prev_velocity_8; }
	inline void set_prev_velocity_8(Vector3_t2243707580  value)
	{
		___prev_velocity_8 = value;
	}

	inline static int32_t get_offset_of_U3CshakeNumberU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HIV_Weak_t1951470088, ___U3CshakeNumberU3Ek__BackingField_9)); }
	inline int32_t get_U3CshakeNumberU3Ek__BackingField_9() const { return ___U3CshakeNumberU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CshakeNumberU3Ek__BackingField_9() { return &___U3CshakeNumberU3Ek__BackingField_9; }
	inline void set_U3CshakeNumberU3Ek__BackingField_9(int32_t value)
	{
		___U3CshakeNumberU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
