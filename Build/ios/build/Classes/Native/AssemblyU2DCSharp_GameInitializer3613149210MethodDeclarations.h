﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameInitializer
struct GameInitializer_t3613149210;

#include "codegen/il2cpp-codegen.h"

// System.Void GameInitializer::.ctor()
extern "C"  void GameInitializer__ctor_m4077282703 (GameInitializer_t3613149210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitializer::Start()
extern "C"  void GameInitializer_Start_m3124885323 (GameInitializer_t3613149210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
