﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m601251289(__this, ___l0, method) ((  void (*) (Enumerator_t3070300762 *, List_1_t3535571088 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1518995265(__this, method) ((  void (*) (Enumerator_t3070300762 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2135841441(__this, method) ((  Il2CppObject * (*) (Enumerator_t3070300762 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::Dispose()
#define Enumerator_Dispose_m3821168348(__this, method) ((  void (*) (Enumerator_t3070300762 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::VerifyState()
#define Enumerator_VerifyState_m989968583(__this, method) ((  void (*) (Enumerator_t3070300762 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::MoveNext()
#define Enumerator_MoveNext_m254251697(__this, method) ((  bool (*) (Enumerator_t3070300762 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AntibodyShooter>::get_Current()
#define Enumerator_get_Current_m3580171366(__this, method) ((  AntibodyShooter_t4166449956 * (*) (Enumerator_t3070300762 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
