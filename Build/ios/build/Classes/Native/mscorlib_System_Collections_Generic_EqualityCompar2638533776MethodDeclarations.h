﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Tropism/ReceptorType>
struct DefaultComparer_t2638533776;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Tropism/ReceptorType>::.ctor()
extern "C"  void DefaultComparer__ctor_m4219311061_gshared (DefaultComparer_t2638533776 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4219311061(__this, method) ((  void (*) (DefaultComparer_t2638533776 *, const MethodInfo*))DefaultComparer__ctor_m4219311061_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Tropism/ReceptorType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2234491838_gshared (DefaultComparer_t2638533776 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2234491838(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2638533776 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2234491838_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Tropism/ReceptorType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3988420446_gshared (DefaultComparer_t2638533776 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3988420446(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2638533776 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3988420446_gshared)(__this, ___x0, ___y1, method)
