﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Statistics>
struct List_1_t1906498139;
// System.Collections.Generic.IEnumerator`1<Statistics>
struct IEnumerator_1_t12900834;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Statistics>
struct ICollection_1_t3489452312;
// System.Collections.Generic.IEnumerable`1<Statistics>
struct IEnumerable_1_t2829504052;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Statistics>
struct ReadOnlyCollection_1_t2723162699;
// Statistics[]
struct StatisticsU5BU5D_t1904610934;
// System.Predicate`1<Statistics>
struct Predicate_1_t980347122;
// System.Comparison`1<Statistics>
struct Comparison_1_t3799115858;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441227813.h"

// System.Void System.Collections.Generic.List`1<Statistics>::.ctor()
extern "C"  void List_1__ctor_m2243433676_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1__ctor_m2243433676(__this, method) ((  void (*) (List_1_t1906498139 *, const MethodInfo*))List_1__ctor_m2243433676_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3098112440_gshared (List_1_t1906498139 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3098112440(__this, ___capacity0, method) ((  void (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1__ctor_m3098112440_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::.cctor()
extern "C"  void List_1__cctor_m522719692_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m522719692(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m522719692_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Statistics>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m303044171_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m303044171(__this, method) ((  Il2CppObject* (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m303044171_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m213783247_gshared (List_1_t1906498139 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m213783247(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1906498139 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m213783247_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Statistics>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m301124038_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m301124038(__this, method) ((  Il2CppObject * (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m301124038_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2991730215_gshared (List_1_t1906498139 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2991730215(__this, ___item0, method) ((  int32_t (*) (List_1_t1906498139 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2991730215_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3630760455_gshared (List_1_t1906498139 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3630760455(__this, ___item0, method) ((  bool (*) (List_1_t1906498139 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3630760455_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2713056765_gshared (List_1_t1906498139 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2713056765(__this, ___item0, method) ((  int32_t (*) (List_1_t1906498139 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2713056765_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2580145404_gshared (List_1_t1906498139 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2580145404(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1906498139 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2580145404_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Statistics>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1945499770_gshared (List_1_t1906498139 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1945499770(__this, ___item0, method) ((  void (*) (List_1_t1906498139 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1945499770_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m66496666_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m66496666(__this, method) ((  bool (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m66496666_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2832186111_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2832186111(__this, method) ((  bool (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2832186111_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Statistics>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m172094819_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m172094819(__this, method) ((  Il2CppObject * (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m172094819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2415233872_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2415233872(__this, method) ((  bool (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2415233872_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2415438267_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2415438267(__this, method) ((  bool (*) (List_1_t1906498139 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2415438267_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Statistics>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2844797110_gshared (List_1_t1906498139 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2844797110(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2844797110_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1813124873_gshared (List_1_t1906498139 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1813124873(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1906498139 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1813124873_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Add(T)
extern "C"  void List_1_Add_m1137681328_gshared (List_1_t1906498139 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define List_1_Add_m1137681328(__this, ___item0, method) ((  void (*) (List_1_t1906498139 *, Statistics_t2537377007 , const MethodInfo*))List_1_Add_m1137681328_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2122105619_gshared (List_1_t1906498139 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2122105619(__this, ___newCount0, method) ((  void (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2122105619_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3294949683_gshared (List_1_t1906498139 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3294949683(__this, ___collection0, method) ((  void (*) (List_1_t1906498139 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3294949683_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3569815011_gshared (List_1_t1906498139 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3569815011(__this, ___enumerable0, method) ((  void (*) (List_1_t1906498139 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3569815011_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1837633190_gshared (List_1_t1906498139 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1837633190(__this, ___collection0, method) ((  void (*) (List_1_t1906498139 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1837633190_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Statistics>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2723162699 * List_1_AsReadOnly_m2244551875_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2244551875(__this, method) ((  ReadOnlyCollection_1_t2723162699 * (*) (List_1_t1906498139 *, const MethodInfo*))List_1_AsReadOnly_m2244551875_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Clear()
extern "C"  void List_1_Clear_m2999001292_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_Clear_m2999001292(__this, method) ((  void (*) (List_1_t1906498139 *, const MethodInfo*))List_1_Clear_m2999001292_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::Contains(T)
extern "C"  bool List_1_Contains_m621040090_gshared (List_1_t1906498139 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define List_1_Contains_m621040090(__this, ___item0, method) ((  bool (*) (List_1_t1906498139 *, Statistics_t2537377007 , const MethodInfo*))List_1_Contains_m621040090_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3122272036_gshared (List_1_t1906498139 * __this, StatisticsU5BU5D_t1904610934* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3122272036(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1906498139 *, StatisticsU5BU5D_t1904610934*, int32_t, const MethodInfo*))List_1_CopyTo_m3122272036_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Statistics>::Find(System.Predicate`1<T>)
extern "C"  Statistics_t2537377007  List_1_Find_m2923141662_gshared (List_1_t1906498139 * __this, Predicate_1_t980347122 * ___match0, const MethodInfo* method);
#define List_1_Find_m2923141662(__this, ___match0, method) ((  Statistics_t2537377007  (*) (List_1_t1906498139 *, Predicate_1_t980347122 *, const MethodInfo*))List_1_Find_m2923141662_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m4286527335_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t980347122 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m4286527335(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t980347122 *, const MethodInfo*))List_1_CheckMatch_m4286527335_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1917433544_gshared (List_1_t1906498139 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t980347122 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1917433544(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1906498139 *, int32_t, int32_t, Predicate_1_t980347122 *, const MethodInfo*))List_1_GetIndex_m1917433544_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Statistics>::GetEnumerator()
extern "C"  Enumerator_t1441227813  List_1_GetEnumerator_m3185245189_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3185245189(__this, method) ((  Enumerator_t1441227813  (*) (List_1_t1906498139 *, const MethodInfo*))List_1_GetEnumerator_m3185245189_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1865567322_gshared (List_1_t1906498139 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1865567322(__this, ___item0, method) ((  int32_t (*) (List_1_t1906498139 *, Statistics_t2537377007 , const MethodInfo*))List_1_IndexOf_m1865567322_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2724452267_gshared (List_1_t1906498139 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2724452267(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1906498139 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2724452267_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Statistics>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3495237556_gshared (List_1_t1906498139 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3495237556(__this, ___index0, method) ((  void (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3495237556_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m116735005_gshared (List_1_t1906498139 * __this, int32_t ___index0, Statistics_t2537377007  ___item1, const MethodInfo* method);
#define List_1_Insert_m116735005(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1906498139 *, int32_t, Statistics_t2537377007 , const MethodInfo*))List_1_Insert_m116735005_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Statistics>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2519512582_gshared (List_1_t1906498139 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2519512582(__this, ___collection0, method) ((  void (*) (List_1_t1906498139 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2519512582_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Statistics>::Remove(T)
extern "C"  bool List_1_Remove_m784819893_gshared (List_1_t1906498139 * __this, Statistics_t2537377007  ___item0, const MethodInfo* method);
#define List_1_Remove_m784819893(__this, ___item0, method) ((  bool (*) (List_1_t1906498139 *, Statistics_t2537377007 , const MethodInfo*))List_1_Remove_m784819893_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2911491847_gshared (List_1_t1906498139 * __this, Predicate_1_t980347122 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2911491847(__this, ___match0, method) ((  int32_t (*) (List_1_t1906498139 *, Predicate_1_t980347122 *, const MethodInfo*))List_1_RemoveAll_m2911491847_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m780687961_gshared (List_1_t1906498139 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m780687961(__this, ___index0, method) ((  void (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1_RemoveAt_m780687961_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Reverse()
extern "C"  void List_1_Reverse_m1186304443_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_Reverse_m1186304443(__this, method) ((  void (*) (List_1_t1906498139 *, const MethodInfo*))List_1_Reverse_m1186304443_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Sort()
extern "C"  void List_1_Sort_m214520525_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_Sort_m214520525(__this, method) ((  void (*) (List_1_t1906498139 *, const MethodInfo*))List_1_Sort_m214520525_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1059045908_gshared (List_1_t1906498139 * __this, Comparison_1_t3799115858 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1059045908(__this, ___comparison0, method) ((  void (*) (List_1_t1906498139 *, Comparison_1_t3799115858 *, const MethodInfo*))List_1_Sort_m1059045908_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Statistics>::ToArray()
extern "C"  StatisticsU5BU5D_t1904610934* List_1_ToArray_m2689628858_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_ToArray_m2689628858(__this, method) ((  StatisticsU5BU5D_t1904610934* (*) (List_1_t1906498139 *, const MethodInfo*))List_1_ToArray_m2689628858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3835050614_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3835050614(__this, method) ((  void (*) (List_1_t1906498139 *, const MethodInfo*))List_1_TrimExcess_m3835050614_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m589579884_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m589579884(__this, method) ((  int32_t (*) (List_1_t1906498139 *, const MethodInfo*))List_1_get_Capacity_m589579884_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Statistics>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1260941043_gshared (List_1_t1906498139 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1260941043(__this, ___value0, method) ((  void (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1260941043_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Statistics>::get_Count()
extern "C"  int32_t List_1_get_Count_m851736215_gshared (List_1_t1906498139 * __this, const MethodInfo* method);
#define List_1_get_Count_m851736215(__this, method) ((  int32_t (*) (List_1_t1906498139 *, const MethodInfo*))List_1_get_Count_m851736215_gshared)(__this, method)
// T System.Collections.Generic.List`1<Statistics>::get_Item(System.Int32)
extern "C"  Statistics_t2537377007  List_1_get_Item_m4001933677_gshared (List_1_t1906498139 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4001933677(__this, ___index0, method) ((  Statistics_t2537377007  (*) (List_1_t1906498139 *, int32_t, const MethodInfo*))List_1_get_Item_m4001933677_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Statistics>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2773519302_gshared (List_1_t1906498139 * __this, int32_t ___index0, Statistics_t2537377007  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2773519302(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1906498139 *, int32_t, Statistics_t2537377007 , const MethodInfo*))List_1_set_Item_m2773519302_gshared)(__this, ___index0, ___value1, method)
