﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>
struct ReadOnlyCollection_1_t3014603041;
// System.Collections.Generic.IList`1<SpriteRender>
struct IList_1_t3369757950;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SpriteRender[]
struct SpriteRenderU5BU5D_t2233488264;
// System.Collections.Generic.IEnumerator`1<SpriteRender>
struct IEnumerator_1_t304341176;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1811800351_gshared (ReadOnlyCollection_1_t3014603041 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1811800351(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1811800351_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2993711241_gshared (ReadOnlyCollection_1_t3014603041 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2993711241(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, SpriteRender_t2828817349 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2993711241_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1351631005_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1351631005(__this, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1351631005_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m599538618_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, SpriteRender_t2828817349  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m599538618(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m599538618_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1765479656_gshared (ReadOnlyCollection_1_t3014603041 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1765479656(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, SpriteRender_t2828817349 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1765479656_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1371616006_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1371616006(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1371616006_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  SpriteRender_t2828817349  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1442616730_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1442616730(__this, ___index0, method) ((  SpriteRender_t2828817349  (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1442616730_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m232146873_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, SpriteRender_t2828817349  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m232146873(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m232146873_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2461462149_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2461462149(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2461462149_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m446233512_gshared (ReadOnlyCollection_1_t3014603041 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m446233512(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m446233512_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m286085693_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m286085693(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m286085693_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m862145828_gshared (ReadOnlyCollection_1_t3014603041 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m862145828(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3014603041 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m862145828_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3551479930_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3551479930(__this, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3551479930_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2881292142_gshared (ReadOnlyCollection_1_t3014603041 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2881292142(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2881292142_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2868413714_gshared (ReadOnlyCollection_1_t3014603041 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2868413714(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3014603041 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2868413714_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2633061009_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2633061009(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2633061009_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m635584673_gshared (ReadOnlyCollection_1_t3014603041 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m635584673(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m635584673_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2641369971_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2641369971(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2641369971_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m166771424_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m166771424(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m166771424_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1595036430_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1595036430(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1595036430_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3350165773_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3350165773(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3350165773_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m485114436_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m485114436(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m485114436_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1674953041_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1674953041(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1674953041_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m747223038_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m747223038(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m747223038_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3155637871_gshared (ReadOnlyCollection_1_t3014603041 * __this, SpriteRender_t2828817349  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3155637871(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3014603041 *, SpriteRender_t2828817349 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3155637871_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m364981485_gshared (ReadOnlyCollection_1_t3014603041 * __this, SpriteRenderU5BU5D_t2233488264* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m364981485(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3014603041 *, SpriteRenderU5BU5D_t2233488264*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m364981485_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2741316144_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2741316144(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2741316144_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1546690717_gshared (ReadOnlyCollection_1_t3014603041 * __this, SpriteRender_t2828817349  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1546690717(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3014603041 *, SpriteRender_t2828817349 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1546690717_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m131396164_gshared (ReadOnlyCollection_1_t3014603041 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m131396164(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3014603041 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m131396164_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>::get_Item(System.Int32)
extern "C"  SpriteRender_t2828817349  ReadOnlyCollection_1_get_Item_m3713764014_gshared (ReadOnlyCollection_1_t3014603041 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3713764014(__this, ___index0, method) ((  SpriteRender_t2828817349  (*) (ReadOnlyCollection_1_t3014603041 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3713764014_gshared)(__this, ___index0, method)
