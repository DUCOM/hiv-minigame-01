﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyState
struct AntibodyState_t3996126191;

#include "codegen/il2cpp-codegen.h"

// System.Void AntibodyState::.ctor()
extern "C"  void AntibodyState__ctor_m667192040 (AntibodyState_t3996126191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyState::Awake()
extern "C"  void AntibodyState_Awake_m1574285855 (AntibodyState_t3996126191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
