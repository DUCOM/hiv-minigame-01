﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GroupColors
struct GroupColors_t153290803;

#include "codegen/il2cpp-codegen.h"

// System.Void GroupColors::.ctor()
extern "C"  void GroupColors__ctor_m64951518 (GroupColors_t153290803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupColors::OnValidate()
extern "C"  void GroupColors_OnValidate_m2398987905 (GroupColors_t153290803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
