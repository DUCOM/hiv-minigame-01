﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighScoreDisplaySingle
struct HighScoreDisplaySingle_t3977972496;

#include "codegen/il2cpp-codegen.h"

// System.Void HighScoreDisplaySingle::.ctor()
extern "C"  void HighScoreDisplaySingle__ctor_m2739648985 (HighScoreDisplaySingle_t3977972496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScoreDisplaySingle::Awake()
extern "C"  void HighScoreDisplaySingle_Awake_m2105601068 (HighScoreDisplaySingle_t3977972496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScoreDisplaySingle::OnGUI()
extern "C"  void HighScoreDisplaySingle_OnGUI_m1558048811 (HighScoreDisplaySingle_t3977972496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
