﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_FollowCam1118905480.h"
#include "AssemblyU2DCSharp_MainCamera1387594674.h"
#include "AssemblyU2DCSharp_RenderCamera2596733641.h"
#include "AssemblyU2DCSharp_FocalPoint1860633653.h"
#include "AssemblyU2DCSharp_FocalPoint_State2306468321.h"
#include "AssemblyU2DCSharp_FocalPointFollow4210230956.h"
#include "AssemblyU2DCSharp_FocalPointMove3911050876.h"
#include "AssemblyU2DCSharp_Antibody2931822436.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"
#include "AssemblyU2DCSharp_AntibodyMove4173900141.h"
#include "AssemblyU2DCSharp_AntibodyAttack3182813916.h"
#include "AssemblyU2DCSharp_AntibodyDeath2080738878.h"
#include "AssemblyU2DCSharp_AntibodyLatch1529651966.h"
#include "AssemblyU2DCSharp_AntibodyState3996126191.h"
#include "AssemblyU2DCSharp_AntibodyWander3668243203.h"
#include "AssemblyU2DCSharp_AntibodyShooter4166449956.h"
#include "AssemblyU2DCSharp_BCell1815094666.h"
#include "AssemblyU2DCSharp_BCellMaster153800904.h"
#include "AssemblyU2DCSharp_HIV2481767745.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"
#include "AssemblyU2DCSharp_HIVCollision2736474671.h"
#include "AssemblyU2DCSharp_HIVReceptorChecker623690862.h"
#include "AssemblyU2DCSharp_HIVStats2521285894.h"
#include "AssemblyU2DCSharp_HIV_Glow2038930725.h"
#include "AssemblyU2DCSharp_HIV_Move1427226471.h"
#include "AssemblyU2DCSharp_HIV_Piece1005595762.h"
#include "AssemblyU2DCSharp_HIV_Part1695996321.h"
#include "AssemblyU2DCSharp_HIV_Weak1951470088.h"
#include "AssemblyU2DCSharp_Capsid4245142696.h"
#include "AssemblyU2DCSharp_Matrix2094203845.h"
#include "AssemblyU2DCSharp_Membrane1623556481.h"
#include "AssemblyU2DCSharp_RNA915683991.h"
#include "AssemblyU2DCSharp_RT1716568150.h"
#include "AssemblyU2DCSharp_GP1201512820336.h"
#include "AssemblyU2DCSharp_GP41921954836.h"
#include "AssemblyU2DCSharp_Pivot2110476880.h"
#include "AssemblyU2DCSharp_Spike60554746.h"
#include "AssemblyU2DCSharp_SpikeInteract2340666440.h"
#include "AssemblyU2DCSharp_SpikeList1827939702.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"
#include "AssemblyU2DCSharp_HIV_Bud370745349.h"
#include "AssemblyU2DCSharp_HIV_Death750143594.h"
#include "AssemblyU2DCSharp_HIV_Infect1458631357.h"
#include "AssemblyU2DCSharp_HIV_Return3773441690.h"
#include "AssemblyU2DCSharp_HIV_Selected1107388507.h"
#include "AssemblyU2DCSharp_HIV_State57600565.h"
#include "AssemblyU2DCSharp_HIV_Wander1816073829.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "AssemblyU2DCSharp_Statistics_Type1319459628.h"
#include "AssemblyU2DCSharp_ObjectMovement2D2992076704.h"
#include "AssemblyU2DCSharp_ObjectMovement2D_State1000911910.h"
#include "AssemblyU2DCSharp_RedBloodCell2376799177.h"
#include "AssemblyU2DCSharp_CellMovement2110243311.h"
#include "AssemblyU2DCSharp_TCell_Coreceptor1508369739.h"
#include "AssemblyU2DCSharp_TCell_Pair2779000105.h"
#include "AssemblyU2DCSharp_PairList3336234724.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879.h"
#include "AssemblyU2DCSharp_TCell_Receptor_Head2847248660.h"
#include "AssemblyU2DCSharp_TCell_Receptor_Stem4177804179.h"
#include "AssemblyU2DCSharp_TCell_Cytoplasm256238633.h"
#include "AssemblyU2DCSharp_TCell_Membrane392610684.h"
#include "AssemblyU2DCSharp_TCell_Nucleous3012779807.h"
#include "AssemblyU2DCSharp_TCell1815110036.h"
#include "AssemblyU2DCSharp_TCell_Piece3459712141.h"
#include "AssemblyU2DCSharp_TCell_Part1972431076.h"
#include "AssemblyU2DCSharp_UI_Bar1358396100.h"
#include "AssemblyU2DCSharp_UI_Capsid4027023025.h"
#include "AssemblyU2DCSharp_UI_GP1201483396489.h"
#include "AssemblyU2DCSharp_UI_GP412232403251.h"
#include "AssemblyU2DCSharp_UI_HIV1412098442.h"
#include "AssemblyU2DCSharp_UI_HIV_Button3180703511.h"
#include "AssemblyU2DCSharp_UI_Matrix2197715354.h"
#include "AssemblyU2DCSharp_UI_Membrane3925205798.h"
#include "AssemblyU2DCSharp_UI_RNA3307819878.h"
#include "AssemblyU2DCSharp_UI_RT3092214427.h"
#include "AssemblyU2DCSharp_UI_Spike1842279613.h"
#include "AssemblyU2DCSharp_UI_SpikeList1985653691.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163.h"
#include "AssemblyU2DCSharp_PauseMenuButton2369929669.h"
#include "AssemblyU2DCSharp_PauseMenuQuit564462074.h"
#include "AssemblyU2DCSharp_UI_Background3920117671.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity1529844486.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1700[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1705[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (FollowCam_t1118905480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	FollowCam_t1118905480::get_offset_of_follow_2(),
	FollowCam_t1118905480::get_offset_of_followRigidbody_3(),
	FollowCam_t1118905480::get_offset_of_border_4(),
	FollowCam_t1118905480::get_offset_of_cam_5(),
	FollowCam_t1118905480::get_offset_of_rb_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (MainCamera_t1387594674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[1] = 
{
	MainCamera_t1387594674::get_offset_of__speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (RenderCamera_t2596733641), -1, sizeof(RenderCamera_t2596733641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1718[5] = 
{
	RenderCamera_t2596733641_StaticFields::get_offset_of_main_2(),
	0,
	RenderCamera_t2596733641::get_offset_of_zoom_4(),
	RenderCamera_t2596733641::get_offset_of_difference_5(),
	RenderCamera_t2596733641::get_offset_of_cam_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (FocalPoint_t1860633653), -1, sizeof(FocalPoint_t1860633653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[6] = 
{
	FocalPoint_t1860633653::get_offset_of__move_2(),
	FocalPoint_t1860633653::get_offset_of__maxDistance_3(),
	FocalPoint_t1860633653::get_offset_of__additionalCutoff_4(),
	FocalPoint_t1860633653::get_offset_of__speed_5(),
	FocalPoint_t1860633653::get_offset_of_state_6(),
	FocalPoint_t1860633653_StaticFields::get_offset_of_main_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (State_t2306468321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[4] = 
{
	State_t2306468321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (FocalPointFollow_t4210230956), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (FocalPointMove_t3911050876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (Antibody_t2931822436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[5] = 
{
	Antibody_t2931822436::get_offset_of_U3CstateU3Ek__BackingField_2(),
	Antibody_t2931822436::get_offset_of_stateList_3(),
	Antibody_t2931822436::get_offset_of_speed_4(),
	Antibody_t2931822436::get_offset_of__move_5(),
	Antibody_t2931822436::get_offset_of__display_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (State_t1129704178)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[6] = 
{
	State_t1129704178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (AntibodyMove_t4173900141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (AntibodyAttack_t3182813916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[1] = 
{
	AntibodyAttack_t3182813916::get_offset_of_destination_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (AntibodyDeath_t2080738878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	AntibodyDeath_t2080738878::get_offset_of_rotationSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (AntibodyLatch_t1529651966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[6] = 
{
	AntibodyLatch_t1529651966::get_offset_of_oldMax_4(),
	AntibodyLatch_t1529651966::get_offset_of_displacement_5(),
	AntibodyLatch_t1529651966::get_offset_of_maxTime_6(),
	AntibodyLatch_t1529651966::get_offset_of__timer_7(),
	AntibodyLatch_t1529651966::get_offset_of_death_8(),
	AntibodyLatch_t1529651966::get_offset_of_maxShakeNumber_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (AntibodyState_t3996126191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[2] = 
{
	AntibodyState_t3996126191::get_offset_of_main_2(),
	AntibodyState_t3996126191::get_offset_of_target_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (AntibodyWander_t3668243203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (AntibodyShooter_t4166449956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[4] = 
{
	AntibodyShooter_t4166449956::get_offset_of__antibody_2(),
	AntibodyShooter_t4166449956::get_offset_of__shotSpeed_3(),
	AntibodyShooter_t4166449956::get_offset_of__pivot_4(),
	AntibodyShooter_t4166449956::get_offset_of__bulletList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (BCell_t1815094666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[7] = 
{
	BCell_t1815094666::get_offset_of__shooter_2(),
	BCell_t1815094666::get_offset_of__master_3(),
	BCell_t1815094666::get_offset_of__shooters_4(),
	BCell_t1815094666::get_offset_of__number_5(),
	BCell_t1815094666::get_offset_of__radius_6(),
	BCell_t1815094666::get_offset_of__rotationSpeed_7(),
	BCell_t1815094666::get_offset_of_currentIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (BCellMaster_t153800904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[2] = 
{
	BCellMaster_t153800904::get_offset_of_pivot_2(),
	BCellMaster_t153800904::get_offset_of_radius_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (HIV_t2481767745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[16] = 
{
	0,
	HIV_t2481767745::get_offset_of_focalPoint_3(),
	HIV_t2481767745::get_offset_of_grouped_4(),
	HIV_t2481767745::get_offset_of__radius_5(),
	HIV_t2481767745::get_offset_of_U3CstateU3Ek__BackingField_6(),
	HIV_t2481767745::get_offset_of_stateList_7(),
	HIV_t2481767745::get_offset_of_selected_8(),
	HIV_t2481767745::get_offset_of__weakened_9(),
	HIV_t2481767745::get_offset_of_capsid_10(),
	HIV_t2481767745::get_offset_of_matrix_11(),
	HIV_t2481767745::get_offset_of_membrane_12(),
	HIV_t2481767745::get_offset_of_rna_13(),
	HIV_t2481767745::get_offset_of_rt_14(),
	HIV_t2481767745::get_offset_of_spikeList_15(),
	HIV_t2481767745::get_offset_of_sparkle_16(),
	HIV_t2481767745::get_offset_of_U3CmoveU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (State_t4224770181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1735[8] = 
{
	State_t4224770181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (HIVCollision_t2736474671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (HIVReceptorChecker_t623690862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[3] = 
{
	HIVReceptorChecker_t623690862::get_offset_of_stay_2(),
	HIVReceptorChecker_t623690862::get_offset_of_maxTimer_3(),
	HIVReceptorChecker_t623690862::get_offset_of_U3C_timerU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (HIVStats_t2521285894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[5] = 
{
	HIVStats_t2521285894::get_offset_of_U3CstatsU3Ek__BackingField_2(),
	HIVStats_t2521285894::get_offset_of_U3ChealthU3Ek__BackingField_3(),
	HIVStats_t2521285894::get_offset_of_dying_4(),
	HIVStats_t2521285894::get_offset_of_lifeColor_5(),
	HIVStats_t2521285894::get_offset_of_startColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (HIV_Glow_t2038930725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[13] = 
{
	HIV_Glow_t2038930725::get_offset_of_spriteRender_2(),
	HIV_Glow_t2038930725::get_offset_of_radius_3(),
	HIV_Glow_t2038930725::get_offset_of_alpha_4(),
	HIV_Glow_t2038930725::get_offset_of_t_5(),
	HIV_Glow_t2038930725::get_offset_of_up_6(),
	HIV_Glow_t2038930725::get_offset_of_radiusMedium_7(),
	0,
	HIV_Glow_t2038930725::get_offset_of_timeDilation_9(),
	HIV_Glow_t2038930725::get_offset_of_radiusChange_10(),
	HIV_Glow_t2038930725::get_offset_of_radiusMin_11(),
	HIV_Glow_t2038930725::get_offset_of_radiusMax_12(),
	HIV_Glow_t2038930725::get_offset_of_alphaChange_13(),
	HIV_Glow_t2038930725::get_offset_of_alphaOffset_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (HIV_Move_t1427226471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[1] = 
{
	HIV_Move_t1427226471::get_offset_of_main_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (HIV_Piece_t1005595762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[3] = 
{
	HIV_Piece_t1005595762::get_offset_of_lockColor_2(),
	HIV_Piece_t1005595762::get_offset_of__color_3(),
	HIV_Piece_t1005595762::get_offset_of__stats_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (HIV_Part_t1695996321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[2] = 
{
	HIV_Part_t1695996321::get_offset_of_prefab_0(),
	HIV_Part_t1695996321::get_offset_of__script_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (HIV_Weak_t1951470088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[8] = 
{
	HIV_Weak_t1951470088::get_offset_of_bubble_2(),
	HIV_Weak_t1951470088::get_offset_of_gp120_l_3(),
	HIV_Weak_t1951470088::get_offset_of_gp120_r_4(),
	HIV_Weak_t1951470088::get_offset_of_gp41_5(),
	HIV_Weak_t1951470088::get_offset_of_excludedIndex_6(),
	HIV_Weak_t1951470088::get_offset_of_antibody_7(),
	HIV_Weak_t1951470088::get_offset_of_prev_velocity_8(),
	HIV_Weak_t1951470088::get_offset_of_U3CshakeNumberU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (Capsid_t4245142696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (Matrix_t2094203845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (Membrane_t1623556481), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (RNA_t915683991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (RT_t1716568150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (GP120_t1512820336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[4] = 
{
	GP120_t1512820336::get_offset_of__top_2(),
	GP120_t1512820336::get_offset_of__displayLeft_3(),
	GP120_t1512820336::get_offset_of__displayRight_4(),
	GP120_t1512820336::get_offset_of__particles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (GP41_t921954836), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (Pivot_t2110476880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[1] = 
{
	Pivot_t2110476880::get_offset_of__parent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (Spike_t60554746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[1] = 
{
	Spike_t60554746::get_offset_of__tropism_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (SpikeInteract_t2340666440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (SpikeList_t1827939702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[5] = 
{
	SpikeList_t1827939702::get_offset_of_prefab_0(),
	SpikeList_t1827939702::get_offset_of_tropism_1(),
	SpikeList_t1827939702::get_offset_of__count_2(),
	SpikeList_t1827939702::get_offset_of_objectList_3(),
	SpikeList_t1827939702::get_offset_of_scriptList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (Tropism_t3662836552)+ sizeof (Il2CppObject), sizeof(Tropism_t3662836552 ), sizeof(Tropism_t3662836552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1755[12] = 
{
	Tropism_t3662836552_StaticFields::get_offset_of_TCellCoreceptor_0(),
	Tropism_t3662836552_StaticFields::get_offset_of_TCellReceptor_1(),
	Tropism_t3662836552_StaticFields::get_offset_of_HIVCoreceptor_2(),
	Tropism_t3662836552_StaticFields::get_offset_of_HIVReceptor_3(),
	Tropism_t3662836552_StaticFields::get_offset_of_receptorProbabilities_4(),
	Tropism_t3662836552_StaticFields::get_offset_of_coReceptorProbabilities_5(),
	Tropism_t3662836552_StaticFields::get_offset_of_cd4CoReceptorProbabilities_6(),
	Tropism_t3662836552_StaticFields::get_offset_of_hivProbabilities_7(),
	Tropism_t3662836552_StaticFields::get_offset_of_receptorDecoyColorList_8(),
	Tropism_t3662836552_StaticFields::get_offset_of_coreceptorDecoyColorList_9(),
	Tropism_t3662836552::get_offset_of_receptor_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Tropism_t3662836552::get_offset_of_coreceptor_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (ReceptorType_t1016603615)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[3] = 
{
	ReceptorType_t1016603615::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (CoReceptorType_t161380547)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1757[6] = 
{
	CoReceptorType_t161380547::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (HIV_Bud_t370745349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (HIV_Death_t750143594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[2] = 
{
	HIV_Death_t750143594::get_offset_of_anim_4(),
	HIV_Death_t750143594::get_offset_of_alpha_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (HIV_Infect_t1458631357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	HIV_Infect_t1458631357::get_offset_of_infectDistance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (HIV_Return_t3773441690), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (HIV_Selected_t1107388507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[2] = 
{
	HIV_Selected_t1107388507::get_offset_of_destination_4(),
	HIV_Selected_t1107388507::get_offset_of_slowdownBorder_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (HIV_State_t57600565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[2] = 
{
	HIV_State_t57600565::get_offset_of_main_2(),
	HIV_State_t57600565::get_offset_of_host_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (HIV_Wander_t1816073829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (Statistics_t2537377007)+ sizeof (Il2CppObject), sizeof(Statistics_t2537377007 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1765[5] = 
{
	Statistics_t2537377007::get_offset_of_speed_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Statistics_t2537377007::get_offset_of_acceleration_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Statistics_t2537377007::get_offset_of_lifespan_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Statistics_t2537377007::get_offset_of_docking_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Statistics_t2537377007::get_offset_of_replication_rate_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (Type_t1319459628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1766[6] = 
{
	Type_t1319459628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (ObjectMovement2D_t2992076704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[8] = 
{
	ObjectMovement2D_t2992076704::get_offset_of__rb_2(),
	ObjectMovement2D_t2992076704::get_offset_of_U3CfollowStateU3Ek__BackingField_3(),
	ObjectMovement2D_t2992076704::get_offset_of_followObject_4(),
	ObjectMovement2D_t2992076704::get_offset_of_followVector_5(),
	ObjectMovement2D_t2992076704::get_offset_of_U3CmaxSpeedU3Ek__BackingField_6(),
	ObjectMovement2D_t2992076704::get_offset_of_U3CmultiplierU3Ek__BackingField_7(),
	ObjectMovement2D_t2992076704::get_offset_of_teleportEnabled_8(),
	ObjectMovement2D_t2992076704::get_offset_of_distanceThreshhold_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (State_t1000911910)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1768[4] = 
{
	State_t1000911910::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (RedBloodCell_t2376799177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[4] = 
{
	0,
	RedBloodCell_t2376799177::get_offset_of_U3CmoveU3Ek__BackingField_3(),
	RedBloodCell_t2376799177::get_offset_of__radius_4(),
	RedBloodCell_t2376799177::get_offset_of__disableRect_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (CellMovement_t2110243311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[1] = 
{
	CellMovement_t2110243311::get_offset_of_speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (TCell_Coreceptor_t1508369739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[1] = 
{
	TCell_Coreceptor_t1508369739::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (TCell_Pair_t2779000105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[4] = 
{
	TCell_Pair_t2779000105::get_offset_of_receptor_2(),
	TCell_Pair_t2779000105::get_offset_of_coReceptor_3(),
	TCell_Pair_t2779000105::get_offset_of_pivot_4(),
	TCell_Pair_t2779000105::get_offset_of_tropism__5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (PairList_t3336234724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[4] = 
{
	PairList_t3336234724::get_offset_of_prefab_0(),
	PairList_t3336234724::get_offset_of__count_1(),
	PairList_t3336234724::get_offset_of__instantiated_2(),
	PairList_t3336234724::get_offset_of_objectList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (TCell_Receptor_t2424206879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[1] = 
{
	TCell_Receptor_t2424206879::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (TCell_Receptor_Head_t2847248660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[6] = 
{
	TCell_Receptor_Head_t2847248660::get_offset_of_CD4PositiveFront_2(),
	TCell_Receptor_Head_t2847248660::get_offset_of_CD4PositiveBack_3(),
	TCell_Receptor_Head_t2847248660::get_offset_of_CD4NegativeFront_4(),
	TCell_Receptor_Head_t2847248660::get_offset_of_CD4NegativeBack_5(),
	TCell_Receptor_Head_t2847248660::get_offset_of_HeadDisplayFront_6(),
	TCell_Receptor_Head_t2847248660::get_offset_of_HeadDisplayBack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (TCell_Receptor_Stem_t4177804179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (TCell_Cytoplasm_t256238633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (TCell_Membrane_t392610684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (TCell_Nucleous_t3012779807), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (TCell_t1815110036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[10] = 
{
	0,
	TCell_t1815110036::get_offset_of__radius_3(),
	TCell_t1815110036::get_offset_of_instantiateOnStart_4(),
	TCell_t1815110036::get_offset_of__infected_5(),
	TCell_t1815110036::get_offset_of_cytoplasm_6(),
	TCell_t1815110036::get_offset_of_membrane_7(),
	TCell_t1815110036::get_offset_of_nucleous_8(),
	TCell_t1815110036::get_offset_of_pairList_9(),
	TCell_t1815110036::get_offset_of__timer_10(),
	TCell_t1815110036::get_offset_of__rotate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (TCell_Piece_t3459712141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[1] = 
{
	TCell_Piece_t3459712141::get_offset_of__color_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (TCell_Part_t1972431076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[3] = 
{
	TCell_Part_t1972431076::get_offset_of_prefab_0(),
	TCell_Part_t1972431076::get_offset_of_color__1(),
	TCell_Part_t1972431076::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (UI_Bar_t1358396100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[1] = 
{
	UI_Bar_t1358396100::get_offset_of__selected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (UI_Capsid_t4027023025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (UI_GP120_t1483396489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	UI_GP120_t1483396489::get_offset_of_displayLeft_2(),
	UI_GP120_t1483396489::get_offset_of_displayRight_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (UI_GP41_t2232403251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (UI_HIV_t1412098442), -1, sizeof(UI_HIV_t1412098442_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[8] = 
{
	UI_HIV_t1412098442::get_offset_of__selected_2(),
	UI_HIV_t1412098442::get_offset_of_capsid_3(),
	UI_HIV_t1412098442::get_offset_of_membrane_4(),
	UI_HIV_t1412098442::get_offset_of_matrix_5(),
	UI_HIV_t1412098442::get_offset_of_rna_6(),
	UI_HIV_t1412098442::get_offset_of_rt_7(),
	UI_HIV_t1412098442::get_offset_of_spikeList_8(),
	UI_HIV_t1412098442_StaticFields::get_offset_of_defaultColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (UI_HIV_Button_t3180703511), -1, sizeof(UI_HIV_Button_t3180703511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1788[4] = 
{
	UI_HIV_Button_t3180703511::get_offset_of_anim_2(),
	UI_HIV_Button_t3180703511::get_offset_of_active_3(),
	UI_HIV_Button_t3180703511::get_offset_of_ready_4(),
	UI_HIV_Button_t3180703511_StaticFields::get_offset_of_main_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (UI_Matrix_t2197715354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (UI_Membrane_t3925205798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (UI_RNA_t3307819878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (UI_RT_t3092214427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (UI_Spike_t1842279613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (UI_SpikeList_t1985653691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[2] = 
{
	UI_SpikeList_t1985653691::get_offset_of__colors_2(),
	UI_SpikeList_t1985653691::get_offset_of_spikeList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (SpikeColor_t3573234163)+ sizeof (Il2CppObject), sizeof(SpikeColor_t3573234163 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1795[3] = 
{
	SpikeColor_t3573234163::get_offset_of_gp41_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpikeColor_t3573234163::get_offset_of_gp120_left_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpikeColor_t3573234163::get_offset_of_gp120_right_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (PauseMenuButton_t2369929669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (PauseMenuQuit_t564462074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	PauseMenuQuit_t564462074::get_offset_of_activationList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (UI_Background_t3920117671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (UI_Immunogenicity_t1529844486), -1, sizeof(UI_Immunogenicity_t1529844486_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1799[10] = 
{
	UI_Immunogenicity_t1529844486::get_offset_of_barFiller_2(),
	UI_Immunogenicity_t1529844486::get_offset_of_threshhold_3(),
	UI_Immunogenicity_t1529844486::get_offset_of_anim_4(),
	UI_Immunogenicity_t1529844486::get_offset_of_fallRate_5(),
	UI_Immunogenicity_t1529844486::get_offset_of_fillRate_6(),
	UI_Immunogenicity_t1529844486::get_offset_of_progress_7(),
	UI_Immunogenicity_t1529844486::get_offset_of_maxProgress_8(),
	UI_Immunogenicity_t1529844486::get_offset_of_state_9(),
	UI_Immunogenicity_t1529844486::get_offset_of_fillIntended_10(),
	UI_Immunogenicity_t1529844486_StaticFields::get_offset_of_main_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
