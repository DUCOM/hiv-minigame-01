﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairList
struct  PairList_t3336234724  : public Il2CppObject
{
public:
	// UnityEngine.GameObject PairList::prefab
	GameObject_t1756533147 * ___prefab_0;
	// System.UInt32 PairList::_count
	uint32_t ____count_1;
	// System.Boolean PairList::_instantiated
	bool ____instantiated_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PairList::objectList
	List_1_t1125654279 * ___objectList_3;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(PairList_t3336234724, ___prefab_0)); }
	inline GameObject_t1756533147 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t1756533147 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t1756533147 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_0, value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PairList_t3336234724, ____count_1)); }
	inline uint32_t get__count_1() const { return ____count_1; }
	inline uint32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(uint32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__instantiated_2() { return static_cast<int32_t>(offsetof(PairList_t3336234724, ____instantiated_2)); }
	inline bool get__instantiated_2() const { return ____instantiated_2; }
	inline bool* get_address_of__instantiated_2() { return &____instantiated_2; }
	inline void set__instantiated_2(bool value)
	{
		____instantiated_2 = value;
	}

	inline static int32_t get_offset_of_objectList_3() { return static_cast<int32_t>(offsetof(PairList_t3336234724, ___objectList_3)); }
	inline List_1_t1125654279 * get_objectList_3() const { return ___objectList_3; }
	inline List_1_t1125654279 ** get_address_of_objectList_3() { return &___objectList_3; }
	inline void set_objectList_3(List_1_t1125654279 * value)
	{
		___objectList_3 = value;
		Il2CppCodeGenWriteBarrier(&___objectList_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
