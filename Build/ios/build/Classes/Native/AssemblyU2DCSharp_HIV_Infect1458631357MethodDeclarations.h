﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Infect
struct HIV_Infect_t1458631357;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Infect::.ctor()
extern "C"  void HIV_Infect__ctor_m683343340 (HIV_Infect_t1458631357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HIV_Infect::Replicate()
extern "C"  bool HIV_Infect_Replicate_m1959195473 (HIV_Infect_t1458631357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Infect::OnEnable()
extern "C"  void HIV_Infect_OnEnable_m774894780 (HIV_Infect_t1458631357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Infect::Update()
extern "C"  void HIV_Infect_Update_m251775717 (HIV_Infect_t1458631357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
