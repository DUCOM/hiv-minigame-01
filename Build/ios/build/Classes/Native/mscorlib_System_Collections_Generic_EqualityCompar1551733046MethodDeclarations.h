﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<HIV/State>
struct DefaultComparer_t1551733046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<HIV/State>::.ctor()
extern "C"  void DefaultComparer__ctor_m3643493119_gshared (DefaultComparer_t1551733046 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3643493119(__this, method) ((  void (*) (DefaultComparer_t1551733046 *, const MethodInfo*))DefaultComparer__ctor_m3643493119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<HIV/State>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1276955780_gshared (DefaultComparer_t1551733046 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1276955780(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1551733046 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1276955780_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<HIV/State>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2792068852_gshared (DefaultComparer_t1551733046 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2792068852(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1551733046 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2792068852_gshared)(__this, ___x0, ___y1, method)
