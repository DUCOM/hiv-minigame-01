﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwipeController
struct SwipeController_t751295678;
// Controller
struct Controller_t1937198888;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SwipeController::.ctor()
extern "C"  void SwipeController__ctor_m2671769271 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Controller SwipeController::get_controller()
extern "C"  Controller_t1937198888 * SwipeController_get_controller_m201288529 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SwipeController::get_Distance()
extern "C"  float SwipeController_get_Distance_m2275616339 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SwipeController::get_DeltaTime()
extern "C"  float SwipeController_get_DeltaTime_m3847877361 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SwipeController::get_SwipeVelocity()
extern "C"  Vector3_t2243707580  SwipeController_get_SwipeVelocity_m1495297687 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SwipeController::get_SwipeDirection()
extern "C"  Vector3_t2243707580  SwipeController_get_SwipeDirection_m3561092737 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject SwipeController::get_InitialObject()
extern "C"  GameObject_t1756533147 * SwipeController_get_InitialObject_m2745419468 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::StartSwipe()
extern "C"  void SwipeController_StartSwipe_m1039645603 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::UpdateSwipe()
extern "C"  void SwipeController_UpdateSwipe_m3961951132 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::CalculateVelocity()
extern "C"  void SwipeController_CalculateVelocity_m364530922 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::updateTime(System.Boolean)
extern "C"  void SwipeController_updateTime_m2949054136 (SwipeController_t751295678 * __this, bool ___reset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::Release()
extern "C"  void SwipeController_Release_m1932931132 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::Reset()
extern "C"  void SwipeController_Reset_m1423963338 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeController::Update()
extern "C"  void SwipeController_Update_m3955015020 (SwipeController_t751295678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
