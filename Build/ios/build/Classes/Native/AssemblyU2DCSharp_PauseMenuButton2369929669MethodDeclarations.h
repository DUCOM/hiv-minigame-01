﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PauseMenuButton
struct PauseMenuButton_t2369929669;

#include "codegen/il2cpp-codegen.h"

// System.Void PauseMenuButton::.ctor()
extern "C"  void PauseMenuButton__ctor_m2460052464 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenuButton::RestartLevel()
extern "C"  void PauseMenuButton_RestartLevel_m547918073 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenuButton::Restart(System.Single)
extern "C"  void PauseMenuButton_Restart_m1700954672 (PauseMenuButton_t2369929669 * __this, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenuButton::Quit()
extern "C"  void PauseMenuButton_Quit_m2957212369 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenuButton::ToMenu()
extern "C"  void PauseMenuButton_ToMenu_m183229112 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
