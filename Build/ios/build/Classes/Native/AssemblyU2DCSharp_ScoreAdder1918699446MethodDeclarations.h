﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreAdder
struct ScoreAdder_t1918699446;

#include "codegen/il2cpp-codegen.h"

// System.Void ScoreAdder::.ctor()
extern "C"  void ScoreAdder__ctor_m4120211049 (ScoreAdder_t1918699446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreAdder::Awake()
extern "C"  void ScoreAdder_Awake_m454142246 (ScoreAdder_t1918699446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreAdder::UpdateScoreList()
extern "C"  void ScoreAdder_UpdateScoreList_m2886385356 (ScoreAdder_t1918699446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
