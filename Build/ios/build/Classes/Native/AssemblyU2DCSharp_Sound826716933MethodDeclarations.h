﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sound
struct Sound_t826716933;

#include "codegen/il2cpp-codegen.h"

// System.Void Sound::.ctor()
extern "C"  void Sound__ctor_m3476080378 (Sound_t826716933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
