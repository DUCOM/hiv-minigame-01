﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<SpriteRender>
struct DefaultComparer_t1318087582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<SpriteRender>::.ctor()
extern "C"  void DefaultComparer__ctor_m2822736795_gshared (DefaultComparer_t1318087582 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2822736795(__this, method) ((  void (*) (DefaultComparer_t1318087582 *, const MethodInfo*))DefaultComparer__ctor_m2822736795_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<SpriteRender>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2523601488_gshared (DefaultComparer_t1318087582 * __this, SpriteRender_t2828817349  ___x0, SpriteRender_t2828817349  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2523601488(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1318087582 *, SpriteRender_t2828817349 , SpriteRender_t2828817349 , const MethodInfo*))DefaultComparer_Compare_m2523601488_gshared)(__this, ___x0, ___y1, method)
