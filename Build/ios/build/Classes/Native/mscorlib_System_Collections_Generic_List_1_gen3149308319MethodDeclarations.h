﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<ImageRender>
struct List_1_t3149308319;
// System.Collections.Generic.IEnumerator`1<ImageRender>
struct IEnumerator_1_t1255711014;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<ImageRender>
struct ICollection_1_t437295196;
// System.Collections.Generic.IEnumerable`1<ImageRender>
struct IEnumerable_1_t4072314232;
// System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>
struct ReadOnlyCollection_1_t3965972879;
// ImageRender[]
struct ImageRenderU5BU5D_t1559399842;
// System.Predicate`1<ImageRender>
struct Predicate_1_t2223157302;
// System.Comparison`1<ImageRender>
struct Comparison_1_t746958742;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2684037993.h"

// System.Void System.Collections.Generic.List`1<ImageRender>::.ctor()
extern "C"  void List_1__ctor_m2009112471_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1__ctor_m2009112471(__this, method) ((  void (*) (List_1_t3149308319 *, const MethodInfo*))List_1__ctor_m2009112471_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3554993424_gshared (List_1_t3149308319 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3554993424(__this, ___capacity0, method) ((  void (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1__ctor_m3554993424_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::.cctor()
extern "C"  void List_1__cctor_m3696472712_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3696472712(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3696472712_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<ImageRender>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2020578327_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2020578327(__this, method) ((  Il2CppObject* (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2020578327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3590762155_gshared (List_1_t3149308319 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3590762155(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3149308319 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3590762155_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<ImageRender>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2887852854_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2887852854(__this, method) ((  Il2CppObject * (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2887852854_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1169672315_gshared (List_1_t3149308319 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1169672315(__this, ___item0, method) ((  int32_t (*) (List_1_t3149308319 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1169672315_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1680661443_gshared (List_1_t3149308319 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1680661443(__this, ___item0, method) ((  bool (*) (List_1_t3149308319 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1680661443_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1494585473_gshared (List_1_t3149308319 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1494585473(__this, ___item0, method) ((  int32_t (*) (List_1_t3149308319 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1494585473_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2558529748_gshared (List_1_t3149308319 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2558529748(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3149308319 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2558529748_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3738479698_gshared (List_1_t3149308319 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3738479698(__this, ___item0, method) ((  void (*) (List_1_t3149308319 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3738479698_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m49104722_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m49104722(__this, method) ((  bool (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m49104722_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1835831123_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1835831123(__this, method) ((  bool (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1835831123_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ImageRender>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m65579531_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m65579531(__this, method) ((  Il2CppObject * (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m65579531_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3215317864_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3215317864(__this, method) ((  bool (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3215317864_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2675705375_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2675705375(__this, method) ((  bool (*) (List_1_t3149308319 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2675705375_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2692828772_gshared (List_1_t3149308319 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2692828772(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2692828772_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m910061553_gshared (List_1_t3149308319 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m910061553(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3149308319 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m910061553_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Add(T)
extern "C"  void List_1_Add_m3093846536_gshared (List_1_t3149308319 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define List_1_Add_m3093846536(__this, ___item0, method) ((  void (*) (List_1_t3149308319 *, ImageRender_t3780187187 , const MethodInfo*))List_1_Add_m3093846536_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1045917799_gshared (List_1_t3149308319 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1045917799(__this, ___newCount0, method) ((  void (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1045917799_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2307459543_gshared (List_1_t3149308319 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2307459543(__this, ___collection0, method) ((  void (*) (List_1_t3149308319 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2307459543_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m640166615_gshared (List_1_t3149308319 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m640166615(__this, ___enumerable0, method) ((  void (*) (List_1_t3149308319 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m640166615_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3251980878_gshared (List_1_t3149308319 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3251980878(__this, ___collection0, method) ((  void (*) (List_1_t3149308319 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3251980878_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<ImageRender>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3965972879 * List_1_AsReadOnly_m2277316967_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2277316967(__this, method) ((  ReadOnlyCollection_1_t3965972879 * (*) (List_1_t3149308319 *, const MethodInfo*))List_1_AsReadOnly_m2277316967_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Clear()
extern "C"  void List_1_Clear_m2258956036_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_Clear_m2258956036(__this, method) ((  void (*) (List_1_t3149308319 *, const MethodInfo*))List_1_Clear_m2258956036_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::Contains(T)
extern "C"  bool List_1_Contains_m566221062_gshared (List_1_t3149308319 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define List_1_Contains_m566221062(__this, ___item0, method) ((  bool (*) (List_1_t3149308319 *, ImageRender_t3780187187 , const MethodInfo*))List_1_Contains_m566221062_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m869536652_gshared (List_1_t3149308319 * __this, ImageRenderU5BU5D_t1559399842* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m869536652(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3149308319 *, ImageRenderU5BU5D_t1559399842*, int32_t, const MethodInfo*))List_1_CopyTo_m869536652_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<ImageRender>::Find(System.Predicate`1<T>)
extern "C"  ImageRender_t3780187187  List_1_Find_m1572148384_gshared (List_1_t3149308319 * __this, Predicate_1_t2223157302 * ___match0, const MethodInfo* method);
#define List_1_Find_m1572148384(__this, ___match0, method) ((  ImageRender_t3780187187  (*) (List_1_t3149308319 *, Predicate_1_t2223157302 *, const MethodInfo*))List_1_Find_m1572148384_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2210648755_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2223157302 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2210648755(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2223157302 *, const MethodInfo*))List_1_CheckMatch_m2210648755_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1891033392_gshared (List_1_t3149308319 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2223157302 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1891033392(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3149308319 *, int32_t, int32_t, Predicate_1_t2223157302 *, const MethodInfo*))List_1_GetIndex_m1891033392_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<ImageRender>::GetEnumerator()
extern "C"  Enumerator_t2684037993  List_1_GetEnumerator_m3567641789_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3567641789(__this, method) ((  Enumerator_t2684037993  (*) (List_1_t3149308319 *, const MethodInfo*))List_1_GetEnumerator_m3567641789_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3939983570_gshared (List_1_t3149308319 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3939983570(__this, ___item0, method) ((  int32_t (*) (List_1_t3149308319 *, ImageRender_t3780187187 , const MethodInfo*))List_1_IndexOf_m3939983570_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1142771959_gshared (List_1_t3149308319 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1142771959(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3149308319 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1142771959_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m123738204_gshared (List_1_t3149308319 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m123738204(__this, ___index0, method) ((  void (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1_CheckIndex_m123738204_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3862972285_gshared (List_1_t3149308319 * __this, int32_t ___index0, ImageRender_t3780187187  ___item1, const MethodInfo* method);
#define List_1_Insert_m3862972285(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3149308319 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))List_1_Insert_m3862972285_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2982193258_gshared (List_1_t3149308319 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2982193258(__this, ___collection0, method) ((  void (*) (List_1_t3149308319 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2982193258_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<ImageRender>::Remove(T)
extern "C"  bool List_1_Remove_m347759133_gshared (List_1_t3149308319 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define List_1_Remove_m347759133(__this, ___item0, method) ((  bool (*) (List_1_t3149308319 *, ImageRender_t3780187187 , const MethodInfo*))List_1_Remove_m347759133_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2373530939_gshared (List_1_t3149308319 * __this, Predicate_1_t2223157302 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2373530939(__this, ___match0, method) ((  int32_t (*) (List_1_t3149308319 *, Predicate_1_t2223157302 *, const MethodInfo*))List_1_RemoveAll_m2373530939_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3631907177_gshared (List_1_t3149308319 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3631907177(__this, ___index0, method) ((  void (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3631907177_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Reverse()
extern "C"  void List_1_Reverse_m2192419343_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_Reverse_m2192419343(__this, method) ((  void (*) (List_1_t3149308319 *, const MethodInfo*))List_1_Reverse_m2192419343_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Sort()
extern "C"  void List_1_Sort_m1743527253_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_Sort_m1743527253(__this, method) ((  void (*) (List_1_t3149308319 *, const MethodInfo*))List_1_Sort_m1743527253_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3478767776_gshared (List_1_t3149308319 * __this, Comparison_1_t746958742 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3478767776(__this, ___comparison0, method) ((  void (*) (List_1_t3149308319 *, Comparison_1_t746958742 *, const MethodInfo*))List_1_Sort_m3478767776_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<ImageRender>::ToArray()
extern "C"  ImageRenderU5BU5D_t1559399842* List_1_ToArray_m3441980488_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_ToArray_m3441980488(__this, method) ((  ImageRenderU5BU5D_t1559399842* (*) (List_1_t3149308319 *, const MethodInfo*))List_1_ToArray_m3441980488_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::TrimExcess()
extern "C"  void List_1_TrimExcess_m707067390_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m707067390(__this, method) ((  void (*) (List_1_t3149308319 *, const MethodInfo*))List_1_TrimExcess_m707067390_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2714051588_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2714051588(__this, method) ((  int32_t (*) (List_1_t3149308319 *, const MethodInfo*))List_1_get_Capacity_m2714051588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1637278807_gshared (List_1_t3149308319 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1637278807(__this, ___value0, method) ((  void (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1637278807_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<ImageRender>::get_Count()
extern "C"  int32_t List_1_get_Count_m2627362715_gshared (List_1_t3149308319 * __this, const MethodInfo* method);
#define List_1_get_Count_m2627362715(__this, method) ((  int32_t (*) (List_1_t3149308319 *, const MethodInfo*))List_1_get_Count_m2627362715_gshared)(__this, method)
// T System.Collections.Generic.List`1<ImageRender>::get_Item(System.Int32)
extern "C"  ImageRender_t3780187187  List_1_get_Item_m2355669717_gshared (List_1_t3149308319 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2355669717(__this, ___index0, method) ((  ImageRender_t3780187187  (*) (List_1_t3149308319 *, int32_t, const MethodInfo*))List_1_get_Item_m2355669717_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ImageRender>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m248060446_gshared (List_1_t3149308319 * __this, int32_t ___index0, ImageRender_t3780187187  ___value1, const MethodInfo* method);
#define List_1_set_Item_m248060446(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3149308319 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))List_1_set_Item_m248060446_gshared)(__this, ___index0, ___value1, method)
