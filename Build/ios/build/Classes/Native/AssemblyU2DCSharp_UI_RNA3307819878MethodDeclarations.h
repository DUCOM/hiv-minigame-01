﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_RNA
struct UI_RNA_t3307819878;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_RNA::.ctor()
extern "C"  void UI_RNA__ctor_m625863521 (UI_RNA_t3307819878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
