﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RenderCamera
struct RenderCamera_t2596733641;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void RenderCamera::.ctor()
extern "C"  void RenderCamera__ctor_m4086194934 (RenderCamera_t2596733641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RenderCamera::get_origin()
extern "C"  Vector3_t2243707580  RenderCamera_get_origin_m2137376627 (RenderCamera_t2596733641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RenderCamera::GetZoomHeight(System.Single)
extern "C"  float RenderCamera_GetZoomHeight_m3395087457 (RenderCamera_t2596733641 * __this, float ___zoom0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderCamera::Move()
extern "C"  void RenderCamera_Move_m2624179241 (RenderCamera_t2596733641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderCamera::Zoom(System.Single)
extern "C"  void RenderCamera_Zoom_m1502878588 (RenderCamera_t2596733641 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderCamera::Awake()
extern "C"  void RenderCamera_Awake_m1677413369 (RenderCamera_t2596733641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RenderCamera::FixedUpdate()
extern "C"  void RenderCamera_FixedUpdate_m3495663039 (RenderCamera_t2596733641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
