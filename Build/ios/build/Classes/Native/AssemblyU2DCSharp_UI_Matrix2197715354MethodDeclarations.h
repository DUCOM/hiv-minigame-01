﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Matrix
struct UI_Matrix_t2197715354;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Matrix::.ctor()
extern "C"  void UI_Matrix__ctor_m1847062659 (UI_Matrix_t2197715354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
