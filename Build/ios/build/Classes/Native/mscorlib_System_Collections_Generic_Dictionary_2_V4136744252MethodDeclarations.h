﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>
struct Dictionary_2_t2450211488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4136744252.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Antibody/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m283730153_gshared (Enumerator_t4136744252 * __this, Dictionary_2_t2450211488 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m283730153(__this, ___host0, method) ((  void (*) (Enumerator_t4136744252 *, Dictionary_2_t2450211488 *, const MethodInfo*))Enumerator__ctor_m283730153_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Antibody/State,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m757770502_gshared (Enumerator_t4136744252 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m757770502(__this, method) ((  Il2CppObject * (*) (Enumerator_t4136744252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m757770502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Antibody/State,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m980503928_gshared (Enumerator_t4136744252 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m980503928(__this, method) ((  void (*) (Enumerator_t4136744252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m980503928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Antibody/State,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1336870361_gshared (Enumerator_t4136744252 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1336870361(__this, method) ((  void (*) (Enumerator_t4136744252 *, const MethodInfo*))Enumerator_Dispose_m1336870361_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Antibody/State,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m292709604_gshared (Enumerator_t4136744252 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m292709604(__this, method) ((  bool (*) (Enumerator_t4136744252 *, const MethodInfo*))Enumerator_MoveNext_m292709604_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Antibody/State,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1145467146_gshared (Enumerator_t4136744252 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1145467146(__this, method) ((  Il2CppObject * (*) (Enumerator_t4136744252 *, const MethodInfo*))Enumerator_get_Current_m1145467146_gshared)(__this, method)
