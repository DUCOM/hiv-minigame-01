﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighScores
struct HighScores_t3229912699;
// Score[]
struct ScoreU5BU5D_t100793071;
// ScoreList
struct ScoreList_t2288120898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Score1518975274.h"

// System.Void HighScores::.ctor(System.Boolean,System.Boolean)
extern "C"  void HighScores__ctor_m1897854302 (HighScores_t3229912699 * __this, bool ___saveToMain0, bool ___overwrite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScores::.ctor(Score[],System.Boolean,System.Boolean)
extern "C"  void HighScores__ctor_m1484738200 (HighScores_t3229912699 * __this, ScoreU5BU5D_t100793071* ___scoreList0, bool ___saveToMain1, bool ___overwrite2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ScoreList HighScores::get_scores()
extern "C"  ScoreList_t2288120898 * HighScores_get_scores_m927527757 (HighScores_t3229912699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HighScores::Insert(Score)
extern "C"  int32_t HighScores_Insert_m2661071179 (HighScores_t3229912699 * __this, Score_t1518975274  ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScores::Reset()
extern "C"  void HighScores_Reset_m1182637323 (HighScores_t3229912699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HighScores HighScores::get_Default()
extern "C"  HighScores_t3229912699 * HighScores_get_Default_m4260063046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighScores::Save()
extern "C"  bool HighScores_Save_m1918938851 (HighScores_t3229912699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighScores::ResetSave()
extern "C"  bool HighScores_ResetSave_m3265589570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HighScores::Load()
extern "C"  bool HighScores_Load_m3721535986 (HighScores_t3229912699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScores::.cctor()
extern "C"  void HighScores__cctor_m1276341525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
