﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1988456440.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"

// System.Void System.Array/InternalEnumerator`1<Antibody/State>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4178626761_gshared (InternalEnumerator_1_t1988456440 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4178626761(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1988456440 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4178626761_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Antibody/State>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2559362829_gshared (InternalEnumerator_1_t1988456440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2559362829(__this, method) ((  void (*) (InternalEnumerator_1_t1988456440 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2559362829_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Antibody/State>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3513435901_gshared (InternalEnumerator_1_t1988456440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3513435901(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1988456440 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3513435901_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Antibody/State>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1201871498_gshared (InternalEnumerator_1_t1988456440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1201871498(__this, method) ((  void (*) (InternalEnumerator_1_t1988456440 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1201871498_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Antibody/State>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3652626101_gshared (InternalEnumerator_1_t1988456440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3652626101(__this, method) ((  bool (*) (InternalEnumerator_1_t1988456440 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3652626101_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Antibody/State>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1354117912_gshared (InternalEnumerator_1_t1988456440 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1354117912(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1988456440 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1354117912_gshared)(__this, method)
