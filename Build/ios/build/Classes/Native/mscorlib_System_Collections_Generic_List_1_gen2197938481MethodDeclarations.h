﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SpriteRender>
struct List_1_t2197938481;
// System.Collections.Generic.IEnumerator`1<SpriteRender>
struct IEnumerator_1_t304341176;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<SpriteRender>
struct ICollection_1_t3780892654;
// System.Collections.Generic.IEnumerable`1<SpriteRender>
struct IEnumerable_1_t3120944394;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SpriteRender>
struct ReadOnlyCollection_1_t3014603041;
// SpriteRender[]
struct SpriteRenderU5BU5D_t2233488264;
// System.Predicate`1<SpriteRender>
struct Predicate_1_t1271787464;
// System.Comparison`1<SpriteRender>
struct Comparison_1_t4090556200;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1732668155.h"

// System.Void System.Collections.Generic.List`1<SpriteRender>::.ctor()
extern "C"  void List_1__ctor_m2333434113_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1__ctor_m2333434113(__this, method) ((  void (*) (List_1_t2197938481 *, const MethodInfo*))List_1__ctor_m2333434113_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1474730418_gshared (List_1_t2197938481 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1474730418(__this, ___capacity0, method) ((  void (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1__ctor_m1474730418_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::.cctor()
extern "C"  void List_1__cctor_m1340805198_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1340805198(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1340805198_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SpriteRender>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4102464841_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4102464841(__this, method) ((  Il2CppObject* (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4102464841_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3494443849_gshared (List_1_t2197938481 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3494443849(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2197938481 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3494443849_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SpriteRender>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m4127946920_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m4127946920(__this, method) ((  Il2CppObject * (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m4127946920_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m433111665_gshared (List_1_t2197938481 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m433111665(__this, ___item0, method) ((  int32_t (*) (List_1_t2197938481 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m433111665_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m509951081_gshared (List_1_t2197938481 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m509951081(__this, ___item0, method) ((  bool (*) (List_1_t2197938481 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m509951081_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2287647495_gshared (List_1_t2197938481 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2287647495(__this, ___item0, method) ((  int32_t (*) (List_1_t2197938481 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2287647495_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m460665218_gshared (List_1_t2197938481 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m460665218(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2197938481 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m460665218_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3958703160_gshared (List_1_t2197938481 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3958703160(__this, ___item0, method) ((  void (*) (List_1_t2197938481 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3958703160_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1511623672_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1511623672(__this, method) ((  bool (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1511623672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3960347549_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3960347549(__this, method) ((  bool (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3960347549_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SpriteRender>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1533301821_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1533301821(__this, method) ((  Il2CppObject * (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1533301821_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2282502554_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2282502554(__this, method) ((  bool (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2282502554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3720318233_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3720318233(__this, method) ((  bool (*) (List_1_t2197938481 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3720318233_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1855659984_gshared (List_1_t2197938481 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1855659984(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1855659984_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3394720839_gshared (List_1_t2197938481 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3394720839(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2197938481 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3394720839_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Add(T)
extern "C"  void List_1_Add_m3042032150_gshared (List_1_t2197938481 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define List_1_Add_m3042032150(__this, ___item0, method) ((  void (*) (List_1_t2197938481 *, SpriteRender_t2828817349 , const MethodInfo*))List_1_Add_m3042032150_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m4165869913_gshared (List_1_t2197938481 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m4165869913(__this, ___newCount0, method) ((  void (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4165869913_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3400353393_gshared (List_1_t2197938481 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3400353393(__this, ___collection0, method) ((  void (*) (List_1_t2197938481 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3400353393_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2479635617_gshared (List_1_t2197938481 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2479635617(__this, ___enumerable0, method) ((  void (*) (List_1_t2197938481 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2479635617_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2337590376_gshared (List_1_t2197938481 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2337590376(__this, ___collection0, method) ((  void (*) (List_1_t2197938481 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2337590376_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SpriteRender>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3014603041 * List_1_AsReadOnly_m2795859577_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2795859577(__this, method) ((  ReadOnlyCollection_1_t3014603041 * (*) (List_1_t2197938481 *, const MethodInfo*))List_1_AsReadOnly_m2795859577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Clear()
extern "C"  void List_1_Clear_m3884462026_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_Clear_m3884462026(__this, method) ((  void (*) (List_1_t2197938481 *, const MethodInfo*))List_1_Clear_m3884462026_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::Contains(T)
extern "C"  bool List_1_Contains_m3333033276_gshared (List_1_t2197938481 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define List_1_Contains_m3333033276(__this, ___item0, method) ((  bool (*) (List_1_t2197938481 *, SpriteRender_t2828817349 , const MethodInfo*))List_1_Contains_m3333033276_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m340945182_gshared (List_1_t2197938481 * __this, SpriteRenderU5BU5D_t2233488264* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m340945182(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2197938481 *, SpriteRenderU5BU5D_t2233488264*, int32_t, const MethodInfo*))List_1_CopyTo_m340945182_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<SpriteRender>::Find(System.Predicate`1<T>)
extern "C"  SpriteRender_t2828817349  List_1_Find_m2195479296_gshared (List_1_t2197938481 * __this, Predicate_1_t1271787464 * ___match0, const MethodInfo* method);
#define List_1_Find_m2195479296(__this, ___match0, method) ((  SpriteRender_t2828817349  (*) (List_1_t2197938481 *, Predicate_1_t1271787464 *, const MethodInfo*))List_1_Find_m2195479296_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1011143337_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1271787464 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1011143337(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1271787464 *, const MethodInfo*))List_1_CheckMatch_m1011143337_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m733956306_gshared (List_1_t2197938481 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1271787464 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m733956306(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2197938481 *, int32_t, int32_t, Predicate_1_t1271787464 *, const MethodInfo*))List_1_GetIndex_m733956306_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SpriteRender>::GetEnumerator()
extern "C"  Enumerator_t1732668155  List_1_GetEnumerator_m3922742667_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3922742667(__this, method) ((  Enumerator_t1732668155  (*) (List_1_t2197938481 *, const MethodInfo*))List_1_GetEnumerator_m3922742667_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1720501648_gshared (List_1_t2197938481 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1720501648(__this, ___item0, method) ((  int32_t (*) (List_1_t2197938481 *, SpriteRender_t2828817349 , const MethodInfo*))List_1_IndexOf_m1720501648_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3336849325_gshared (List_1_t2197938481 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3336849325(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2197938481 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3336849325_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1775760058_gshared (List_1_t2197938481 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1775760058(__this, ___index0, method) ((  void (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1775760058_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2259076703_gshared (List_1_t2197938481 * __this, int32_t ___index0, SpriteRender_t2828817349  ___item1, const MethodInfo* method);
#define List_1_Insert_m2259076703(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2197938481 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))List_1_Insert_m2259076703_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1398517636_gshared (List_1_t2197938481 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1398517636(__this, ___collection0, method) ((  void (*) (List_1_t2197938481 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1398517636_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<SpriteRender>::Remove(T)
extern "C"  bool List_1_Remove_m3745630443_gshared (List_1_t2197938481 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define List_1_Remove_m3745630443(__this, ___item0, method) ((  bool (*) (List_1_t2197938481 *, SpriteRender_t2828817349 , const MethodInfo*))List_1_Remove_m3745630443_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m406021865_gshared (List_1_t2197938481 * __this, Predicate_1_t1271787464 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m406021865(__this, ___match0, method) ((  int32_t (*) (List_1_t2197938481 *, Predicate_1_t1271787464 *, const MethodInfo*))List_1_RemoveAll_m406021865_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3443750547_gshared (List_1_t2197938481 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3443750547(__this, ___index0, method) ((  void (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3443750547_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Reverse()
extern "C"  void List_1_Reverse_m341872893_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_Reverse_m341872893(__this, method) ((  void (*) (List_1_t2197938481 *, const MethodInfo*))List_1_Reverse_m341872893_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Sort()
extern "C"  void List_1_Sort_m369609747_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_Sort_m369609747(__this, method) ((  void (*) (List_1_t2197938481 *, const MethodInfo*))List_1_Sort_m369609747_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m181394326_gshared (List_1_t2197938481 * __this, Comparison_1_t4090556200 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m181394326(__this, ___comparison0, method) ((  void (*) (List_1_t2197938481 *, Comparison_1_t4090556200 *, const MethodInfo*))List_1_Sort_m181394326_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<SpriteRender>::ToArray()
extern "C"  SpriteRenderU5BU5D_t2233488264* List_1_ToArray_m772393628_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_ToArray_m772393628(__this, method) ((  SpriteRenderU5BU5D_t2233488264* (*) (List_1_t2197938481 *, const MethodInfo*))List_1_ToArray_m772393628_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2252465148_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2252465148(__this, method) ((  void (*) (List_1_t2197938481 *, const MethodInfo*))List_1_TrimExcess_m2252465148_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2116718562_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2116718562(__this, method) ((  int32_t (*) (List_1_t2197938481 *, const MethodInfo*))List_1_get_Capacity_m2116718562_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m304344625_gshared (List_1_t2197938481 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m304344625(__this, ___value0, method) ((  void (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1_set_Capacity_m304344625_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<SpriteRender>::get_Count()
extern "C"  int32_t List_1_get_Count_m3502013473_gshared (List_1_t2197938481 * __this, const MethodInfo* method);
#define List_1_get_Count_m3502013473(__this, method) ((  int32_t (*) (List_1_t2197938481 *, const MethodInfo*))List_1_get_Count_m3502013473_gshared)(__this, method)
// T System.Collections.Generic.List`1<SpriteRender>::get_Item(System.Int32)
extern "C"  SpriteRender_t2828817349  List_1_get_Item_m1154014471_gshared (List_1_t2197938481 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1154014471(__this, ___index0, method) ((  SpriteRender_t2828817349  (*) (List_1_t2197938481 *, int32_t, const MethodInfo*))List_1_get_Item_m1154014471_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SpriteRender>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m967526860_gshared (List_1_t2197938481 * __this, int32_t ___index0, SpriteRender_t2828817349  ___value1, const MethodInfo* method);
#define List_1_set_Item_m967526860(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2197938481 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))List_1_set_Item_m967526860_gshared)(__this, ___index0, ___value1, method)
