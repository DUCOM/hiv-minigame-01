﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HIV_Piece
struct HIV_Piece_t1005595762;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIVStats
struct  HIVStats_t2521285894  : public MonoBehaviour_t1158329972
{
public:
	// Statistics HIVStats::<stats>k__BackingField
	Statistics_t2537377007  ___U3CstatsU3Ek__BackingField_2;
	// System.Single HIVStats::<health>k__BackingField
	float ___U3ChealthU3Ek__BackingField_3;
	// System.Boolean HIVStats::dying
	bool ___dying_4;
	// HIV_Piece HIVStats::lifeColor
	HIV_Piece_t1005595762 * ___lifeColor_5;
	// UnityEngine.Color HIVStats::startColor
	Color_t2020392075  ___startColor_6;

public:
	inline static int32_t get_offset_of_U3CstatsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HIVStats_t2521285894, ___U3CstatsU3Ek__BackingField_2)); }
	inline Statistics_t2537377007  get_U3CstatsU3Ek__BackingField_2() const { return ___U3CstatsU3Ek__BackingField_2; }
	inline Statistics_t2537377007 * get_address_of_U3CstatsU3Ek__BackingField_2() { return &___U3CstatsU3Ek__BackingField_2; }
	inline void set_U3CstatsU3Ek__BackingField_2(Statistics_t2537377007  value)
	{
		___U3CstatsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3ChealthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HIVStats_t2521285894, ___U3ChealthU3Ek__BackingField_3)); }
	inline float get_U3ChealthU3Ek__BackingField_3() const { return ___U3ChealthU3Ek__BackingField_3; }
	inline float* get_address_of_U3ChealthU3Ek__BackingField_3() { return &___U3ChealthU3Ek__BackingField_3; }
	inline void set_U3ChealthU3Ek__BackingField_3(float value)
	{
		___U3ChealthU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_dying_4() { return static_cast<int32_t>(offsetof(HIVStats_t2521285894, ___dying_4)); }
	inline bool get_dying_4() const { return ___dying_4; }
	inline bool* get_address_of_dying_4() { return &___dying_4; }
	inline void set_dying_4(bool value)
	{
		___dying_4 = value;
	}

	inline static int32_t get_offset_of_lifeColor_5() { return static_cast<int32_t>(offsetof(HIVStats_t2521285894, ___lifeColor_5)); }
	inline HIV_Piece_t1005595762 * get_lifeColor_5() const { return ___lifeColor_5; }
	inline HIV_Piece_t1005595762 ** get_address_of_lifeColor_5() { return &___lifeColor_5; }
	inline void set_lifeColor_5(HIV_Piece_t1005595762 * value)
	{
		___lifeColor_5 = value;
		Il2CppCodeGenWriteBarrier(&___lifeColor_5, value);
	}

	inline static int32_t get_offset_of_startColor_6() { return static_cast<int32_t>(offsetof(HIVStats_t2521285894, ___startColor_6)); }
	inline Color_t2020392075  get_startColor_6() const { return ___startColor_6; }
	inline Color_t2020392075 * get_address_of_startColor_6() { return &___startColor_6; }
	inline void set_startColor_6(Color_t2020392075  value)
	{
		___startColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
