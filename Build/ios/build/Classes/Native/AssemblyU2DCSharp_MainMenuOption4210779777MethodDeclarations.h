﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainMenuOption
struct MainMenuOption_t4210779777;

#include "codegen/il2cpp-codegen.h"

// System.Void MainMenuOption::.ctor()
extern "C"  void MainMenuOption__ctor_m2839520338 (MainMenuOption_t4210779777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainMenuOption::Goto()
extern "C"  void MainMenuOption_Goto_m1989634667 (MainMenuOption_t4210779777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
