﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Part
struct TCell_Part_t1972431076;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void TCell_Part::.ctor()
extern "C"  void TCell_Part__ctor_m1329199869 (TCell_Part_t1972431076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TCell_Part::get_color()
extern "C"  Color_t2020392075  TCell_Part_get_color_m2749663366 (TCell_Part_t1972431076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Part::set_color(UnityEngine.Color)
extern "C"  void TCell_Part_set_color_m3036638271 (TCell_Part_t1972431076 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Part::Instantiate(UnityEngine.Transform,UnityEngine.Color,UnityEngine.Vector3)
extern "C"  void TCell_Part_Instantiate_m3847004091 (TCell_Part_t1972431076 * __this, Transform_t3275118058 * ___parent0, Color_t2020392075  ___c1, Vector3_t2243707580  ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Part::Instantiate(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void TCell_Part_Instantiate_m328937377 (TCell_Part_t1972431076 * __this, Transform_t3275118058 * ___parent0, Vector3_t2243707580  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Part::Instantiate()
extern "C"  void TCell_Part_Instantiate_m1841548663 (TCell_Part_t1972431076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
