﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Bar
struct UI_Bar_t1358396100;
// UnityEngine.UI.Image
struct Image_t2042527209;
// GameController
struct GameController_t3607102586;
// HIV
struct HIV_t2481767745;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void UI_Bar::.ctor()
extern "C"  void UI_Bar__ctor_m2477021189 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image UI_Bar::get_image()
extern "C"  Image_t2042527209 * UI_Bar_get_image_m1309572970 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameController UI_Bar::get_controller()
extern "C"  GameController_t3607102586 * UI_Bar_get_controller_m3554715213 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV UI_Bar::get_selected()
extern "C"  HIV_t2481767745 * UI_Bar_get_selected_m518456309 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Bar::Update()
extern "C"  void UI_Bar_Update_m3852447278 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Bar::OnGUI()
extern "C"  void UI_Bar_OnGUI_m1300265943 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UI_Bar::GetColor()
extern "C"  Color_t2020392075  UI_Bar_GetColor_m1110270239 (UI_Bar_t1358396100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
