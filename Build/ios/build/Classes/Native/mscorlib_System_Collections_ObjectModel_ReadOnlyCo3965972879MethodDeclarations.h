﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>
struct ReadOnlyCollection_1_t3965972879;
// System.Collections.Generic.IList`1<ImageRender>
struct IList_1_t26160492;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ImageRender[]
struct ImageRenderU5BU5D_t1559399842;
// System.Collections.Generic.IEnumerator`1<ImageRender>
struct IEnumerator_1_t1255711014;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3413621525_gshared (ReadOnlyCollection_1_t3965972879 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3413621525(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3413621525_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1300988255_gshared (ReadOnlyCollection_1_t3965972879 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1300988255(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, ImageRender_t3780187187 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1300988255_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3039257483_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3039257483(__this, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3039257483_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3620112930_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, ImageRender_t3780187187  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3620112930(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3620112930_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3866708972_gshared (ReadOnlyCollection_1_t3965972879 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3866708972(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, ImageRender_t3780187187 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3866708972_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4097953830_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4097953830(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4097953830_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  ImageRender_t3780187187  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3851962416_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3851962416(__this, ___index0, method) ((  ImageRender_t3780187187  (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3851962416_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3107183491_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, ImageRender_t3780187187  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3107183491(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3107183491_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m629257671_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m629257671(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m629257671_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1148198500_gshared (ReadOnlyCollection_1_t3965972879 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1148198500(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1148198500_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4046258451_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4046258451(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4046258451_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m337511472_gshared (ReadOnlyCollection_1_t3965972879 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m337511472(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3965972879 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m337511472_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m938518730_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m938518730(__this, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m938518730_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m883874134_gshared (ReadOnlyCollection_1_t3965972879 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m883874134(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m883874134_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4232803934_gshared (ReadOnlyCollection_1_t3965972879 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4232803934(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3965972879 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4232803934_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2415414155_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2415414155(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2415414155_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1646092179_gshared (ReadOnlyCollection_1_t3965972879 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1646092179(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1646092179_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m560337629_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m560337629(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m560337629_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2466139560_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2466139560(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2466139560_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1640242484_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1640242484(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1640242484_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m517410003_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m517410003(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m517410003_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2325593132_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2325593132(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2325593132_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m378562731_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m378562731(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m378562731_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1801741330_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1801741330(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1801741330_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3109872097_gshared (ReadOnlyCollection_1_t3965972879 * __this, ImageRender_t3780187187  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3109872097(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3965972879 *, ImageRender_t3780187187 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3109872097_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3990024083_gshared (ReadOnlyCollection_1_t3965972879 * __this, ImageRenderU5BU5D_t1559399842* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3990024083(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3965972879 *, ImageRenderU5BU5D_t1559399842*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3990024083_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1168669964_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1168669964(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1168669964_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3941060887_gshared (ReadOnlyCollection_1_t3965972879 * __this, ImageRender_t3780187187  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3941060887(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3965972879 *, ImageRender_t3780187187 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3941060887_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m167383264_gshared (ReadOnlyCollection_1_t3965972879 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m167383264(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3965972879 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m167383264_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<ImageRender>::get_Item(System.Int32)
extern "C"  ImageRender_t3780187187  ReadOnlyCollection_1_get_Item_m347110180_gshared (ReadOnlyCollection_1_t3965972879 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m347110180(__this, ___index0, method) ((  ImageRender_t3780187187  (*) (ReadOnlyCollection_1_t3965972879 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m347110180_gshared)(__this, ___index0, method)
