﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Glow
struct  HIV_Glow_t2038930725  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.SpriteRenderer HIV_Glow::spriteRender
	SpriteRenderer_t1209076198 * ___spriteRender_2;
	// System.Single HIV_Glow::radius
	float ___radius_3;
	// System.Single HIV_Glow::alpha
	float ___alpha_4;
	// System.Single HIV_Glow::t
	float ___t_5;
	// System.Boolean HIV_Glow::up
	bool ___up_6;
	// System.Single HIV_Glow::radiusMedium
	float ___radiusMedium_7;
	// System.Single HIV_Glow::timeDilation
	float ___timeDilation_9;
	// System.Boolean HIV_Glow::radiusChange
	bool ___radiusChange_10;
	// System.Single HIV_Glow::radiusMin
	float ___radiusMin_11;
	// System.Single HIV_Glow::radiusMax
	float ___radiusMax_12;
	// System.Boolean HIV_Glow::alphaChange
	bool ___alphaChange_13;
	// System.Single HIV_Glow::alphaOffset
	float ___alphaOffset_14;

public:
	inline static int32_t get_offset_of_spriteRender_2() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___spriteRender_2)); }
	inline SpriteRenderer_t1209076198 * get_spriteRender_2() const { return ___spriteRender_2; }
	inline SpriteRenderer_t1209076198 ** get_address_of_spriteRender_2() { return &___spriteRender_2; }
	inline void set_spriteRender_2(SpriteRenderer_t1209076198 * value)
	{
		___spriteRender_2 = value;
		Il2CppCodeGenWriteBarrier(&___spriteRender_2, value);
	}

	inline static int32_t get_offset_of_radius_3() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___radius_3)); }
	inline float get_radius_3() const { return ___radius_3; }
	inline float* get_address_of_radius_3() { return &___radius_3; }
	inline void set_radius_3(float value)
	{
		___radius_3 = value;
	}

	inline static int32_t get_offset_of_alpha_4() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___alpha_4)); }
	inline float get_alpha_4() const { return ___alpha_4; }
	inline float* get_address_of_alpha_4() { return &___alpha_4; }
	inline void set_alpha_4(float value)
	{
		___alpha_4 = value;
	}

	inline static int32_t get_offset_of_t_5() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___t_5)); }
	inline float get_t_5() const { return ___t_5; }
	inline float* get_address_of_t_5() { return &___t_5; }
	inline void set_t_5(float value)
	{
		___t_5 = value;
	}

	inline static int32_t get_offset_of_up_6() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___up_6)); }
	inline bool get_up_6() const { return ___up_6; }
	inline bool* get_address_of_up_6() { return &___up_6; }
	inline void set_up_6(bool value)
	{
		___up_6 = value;
	}

	inline static int32_t get_offset_of_radiusMedium_7() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___radiusMedium_7)); }
	inline float get_radiusMedium_7() const { return ___radiusMedium_7; }
	inline float* get_address_of_radiusMedium_7() { return &___radiusMedium_7; }
	inline void set_radiusMedium_7(float value)
	{
		___radiusMedium_7 = value;
	}

	inline static int32_t get_offset_of_timeDilation_9() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___timeDilation_9)); }
	inline float get_timeDilation_9() const { return ___timeDilation_9; }
	inline float* get_address_of_timeDilation_9() { return &___timeDilation_9; }
	inline void set_timeDilation_9(float value)
	{
		___timeDilation_9 = value;
	}

	inline static int32_t get_offset_of_radiusChange_10() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___radiusChange_10)); }
	inline bool get_radiusChange_10() const { return ___radiusChange_10; }
	inline bool* get_address_of_radiusChange_10() { return &___radiusChange_10; }
	inline void set_radiusChange_10(bool value)
	{
		___radiusChange_10 = value;
	}

	inline static int32_t get_offset_of_radiusMin_11() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___radiusMin_11)); }
	inline float get_radiusMin_11() const { return ___radiusMin_11; }
	inline float* get_address_of_radiusMin_11() { return &___radiusMin_11; }
	inline void set_radiusMin_11(float value)
	{
		___radiusMin_11 = value;
	}

	inline static int32_t get_offset_of_radiusMax_12() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___radiusMax_12)); }
	inline float get_radiusMax_12() const { return ___radiusMax_12; }
	inline float* get_address_of_radiusMax_12() { return &___radiusMax_12; }
	inline void set_radiusMax_12(float value)
	{
		___radiusMax_12 = value;
	}

	inline static int32_t get_offset_of_alphaChange_13() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___alphaChange_13)); }
	inline bool get_alphaChange_13() const { return ___alphaChange_13; }
	inline bool* get_address_of_alphaChange_13() { return &___alphaChange_13; }
	inline void set_alphaChange_13(bool value)
	{
		___alphaChange_13 = value;
	}

	inline static int32_t get_offset_of_alphaOffset_14() { return static_cast<int32_t>(offsetof(HIV_Glow_t2038930725, ___alphaOffset_14)); }
	inline float get_alphaOffset_14() const { return ___alphaOffset_14; }
	inline float* get_address_of_alphaOffset_14() { return &___alphaOffset_14; }
	inline void set_alphaOffset_14(float value)
	{
		___alphaOffset_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
