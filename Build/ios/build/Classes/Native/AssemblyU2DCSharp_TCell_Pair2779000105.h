﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TCell_Receptor
struct TCell_Receptor_t2424206879;
// TCell_Coreceptor
struct TCell_Coreceptor_t1508369739;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell_Pair
struct  TCell_Pair_t2779000105  : public MonoBehaviour_t1158329972
{
public:
	// TCell_Receptor TCell_Pair::receptor
	TCell_Receptor_t2424206879 * ___receptor_2;
	// TCell_Coreceptor TCell_Pair::coReceptor
	TCell_Coreceptor_t1508369739 * ___coReceptor_3;
	// UnityEngine.GameObject TCell_Pair::pivot
	GameObject_t1756533147 * ___pivot_4;
	// Tropism TCell_Pair::tropism_
	Tropism_t3662836552  ___tropism__5;

public:
	inline static int32_t get_offset_of_receptor_2() { return static_cast<int32_t>(offsetof(TCell_Pair_t2779000105, ___receptor_2)); }
	inline TCell_Receptor_t2424206879 * get_receptor_2() const { return ___receptor_2; }
	inline TCell_Receptor_t2424206879 ** get_address_of_receptor_2() { return &___receptor_2; }
	inline void set_receptor_2(TCell_Receptor_t2424206879 * value)
	{
		___receptor_2 = value;
		Il2CppCodeGenWriteBarrier(&___receptor_2, value);
	}

	inline static int32_t get_offset_of_coReceptor_3() { return static_cast<int32_t>(offsetof(TCell_Pair_t2779000105, ___coReceptor_3)); }
	inline TCell_Coreceptor_t1508369739 * get_coReceptor_3() const { return ___coReceptor_3; }
	inline TCell_Coreceptor_t1508369739 ** get_address_of_coReceptor_3() { return &___coReceptor_3; }
	inline void set_coReceptor_3(TCell_Coreceptor_t1508369739 * value)
	{
		___coReceptor_3 = value;
		Il2CppCodeGenWriteBarrier(&___coReceptor_3, value);
	}

	inline static int32_t get_offset_of_pivot_4() { return static_cast<int32_t>(offsetof(TCell_Pair_t2779000105, ___pivot_4)); }
	inline GameObject_t1756533147 * get_pivot_4() const { return ___pivot_4; }
	inline GameObject_t1756533147 ** get_address_of_pivot_4() { return &___pivot_4; }
	inline void set_pivot_4(GameObject_t1756533147 * value)
	{
		___pivot_4 = value;
		Il2CppCodeGenWriteBarrier(&___pivot_4, value);
	}

	inline static int32_t get_offset_of_tropism__5() { return static_cast<int32_t>(offsetof(TCell_Pair_t2779000105, ___tropism__5)); }
	inline Tropism_t3662836552  get_tropism__5() const { return ___tropism__5; }
	inline Tropism_t3662836552 * get_address_of_tropism__5() { return &___tropism__5; }
	inline void set_tropism__5(Tropism_t3662836552  value)
	{
		___tropism__5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
