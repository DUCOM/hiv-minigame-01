﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen343972153.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"

// System.Void System.Array/InternalEnumerator`1<ImageRender>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2724866730_gshared (InternalEnumerator_1_t343972153 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2724866730(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t343972153 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2724866730_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ImageRender>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2148239154_gshared (InternalEnumerator_1_t343972153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2148239154(__this, method) ((  void (*) (InternalEnumerator_1_t343972153 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2148239154_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ImageRender>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2560924364_gshared (InternalEnumerator_1_t343972153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2560924364(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t343972153 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2560924364_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ImageRender>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1978702935_gshared (InternalEnumerator_1_t343972153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1978702935(__this, method) ((  void (*) (InternalEnumerator_1_t343972153 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1978702935_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ImageRender>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m404708002_gshared (InternalEnumerator_1_t343972153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m404708002(__this, method) ((  bool (*) (InternalEnumerator_1_t343972153 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m404708002_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ImageRender>::get_Current()
extern "C"  ImageRender_t3780187187  InternalEnumerator_1_get_Current_m1630010531_gshared (InternalEnumerator_1_t343972153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1630010531(__this, method) ((  ImageRender_t3780187187  (*) (InternalEnumerator_1_t343972153 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1630010531_gshared)(__this, method)
