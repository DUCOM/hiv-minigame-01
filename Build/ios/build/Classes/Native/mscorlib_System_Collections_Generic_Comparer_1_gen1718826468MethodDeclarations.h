﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<SpriteRender>
struct Comparer_1_t1718826468;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<SpriteRender>::.ctor()
extern "C"  void Comparer_1__ctor_m3309567298_gshared (Comparer_1_t1718826468 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3309567298(__this, method) ((  void (*) (Comparer_1_t1718826468 *, const MethodInfo*))Comparer_1__ctor_m3309567298_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<SpriteRender>::.cctor()
extern "C"  void Comparer_1__cctor_m68299409_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m68299409(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m68299409_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<SpriteRender>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3283508557_gshared (Comparer_1_t1718826468 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m3283508557(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1718826468 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m3283508557_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<SpriteRender>::get_Default()
extern "C"  Comparer_1_t1718826468 * Comparer_1_get_Default_m1679160646_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1679160646(__this /* static, unused */, method) ((  Comparer_1_t1718826468 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1679160646_gshared)(__this /* static, unused */, method)
