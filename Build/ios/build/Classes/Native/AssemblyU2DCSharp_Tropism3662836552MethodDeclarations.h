﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// Tropism/ReceptorType[]
struct ReceptorTypeU5BU5D_t2292174278;
// Tropism/CoReceptorType[]
struct CoReceptorTypeU5BU5D_t3312104658;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Tropism::.ctor(Tropism/ReceptorType,Tropism/CoReceptorType)
extern "C"  void Tropism__ctor_m2978840885 (Tropism_t3662836552 * __this, int32_t ___r0, int32_t ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tropism::Equals(System.Object)
extern "C"  bool Tropism_Equals_m1822115980 (Tropism_t3662836552 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tropism::GetHashCode()
extern "C"  int32_t Tropism_GetHashCode_m2171971080 (Tropism_t3662836552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tropism::invalidHIVType(Tropism/ReceptorType)
extern "C"  bool Tropism_invalidHIVType_m720158264 (Il2CppObject * __this /* static, unused */, int32_t ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tropism::invalidHIVType(Tropism/CoReceptorType)
extern "C"  bool Tropism_invalidHIVType_m452341038 (Il2CppObject * __this /* static, unused */, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tropism::invalidHIVType(Tropism)
extern "C"  bool Tropism_invalidHIVType_m130453117 (Il2CppObject * __this /* static, unused */, Tropism_t3662836552  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Tropism::DefaultColor(Tropism/ReceptorType)
extern "C"  Color_t2020392075  Tropism_DefaultColor_m1098737047 (Il2CppObject * __this /* static, unused */, int32_t ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Tropism::DefaultColor(Tropism/CoReceptorType)
extern "C"  Color_t2020392075  Tropism_DefaultColor_m3873948415 (Il2CppObject * __this /* static, unused */, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism Tropism::Random()
extern "C"  Tropism_t3662836552  Tropism_Random_m466220907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism Tropism::Random(Tropism/ReceptorType[],Tropism/CoReceptorType[])
extern "C"  Tropism_t3662836552  Tropism_Random_m938370599 (Il2CppObject * __this /* static, unused */, ReceptorTypeU5BU5D_t2292174278* ___r0, CoReceptorTypeU5BU5D_t3312104658* ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism Tropism::HIVRandom()
extern "C"  Tropism_t3662836552  Tropism_HIVRandom_m3105810886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tropism::op_Equality(Tropism,Tropism)
extern "C"  bool Tropism_op_Equality_m4254594981 (Il2CppObject * __this /* static, unused */, Tropism_t3662836552  ___a0, Tropism_t3662836552  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tropism::op_Inequality(Tropism,Tropism)
extern "C"  bool Tropism_op_Inequality_m1143736396 (Il2CppObject * __this /* static, unused */, Tropism_t3662836552  ___a0, Tropism_t3662836552  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tropism::.cctor()
extern "C"  void Tropism__cctor_m319241906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
