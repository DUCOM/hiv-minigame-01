﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Level
struct  UI_Level_t2621343419  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UI_Level::text
	Text_t356221433 * ___text_2;
	// System.String UI_Level::_prefix
	String_t* ____prefix_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(UI_Level_t2621343419, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of__prefix_3() { return static_cast<int32_t>(offsetof(UI_Level_t2621343419, ____prefix_3)); }
	inline String_t* get__prefix_3() const { return ____prefix_3; }
	inline String_t** get_address_of__prefix_3() { return &____prefix_3; }
	inline void set__prefix_3(String_t* value)
	{
		____prefix_3 = value;
		Il2CppCodeGenWriteBarrier(&____prefix_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
