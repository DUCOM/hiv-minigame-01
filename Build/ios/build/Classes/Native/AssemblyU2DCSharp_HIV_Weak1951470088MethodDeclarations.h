﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Weak
struct HIV_Weak_t1951470088;
// HIV
struct HIV_t2481767745;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Weak::.ctor()
extern "C"  void HIV_Weak__ctor_m2116656939 (HIV_Weak_t1951470088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HIV_Weak::get_shakeNumber()
extern "C"  int32_t HIV_Weak_get_shakeNumber_m3852701743 (HIV_Weak_t1951470088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Weak::set_shakeNumber(System.Int32)
extern "C"  void HIV_Weak_set_shakeNumber_m1038909782 (HIV_Weak_t1951470088 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV HIV_Weak::get_main()
extern "C"  HIV_t2481767745 * HIV_Weak_get_main_m3759538633 (HIV_Weak_t1951470088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Weak::OnEnable()
extern "C"  void HIV_Weak_OnEnable_m2161801735 (HIV_Weak_t1951470088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Weak::OnDisable()
extern "C"  void HIV_Weak_OnDisable_m369821140 (HIV_Weak_t1951470088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Weak::FixedUpdate()
extern "C"  void HIV_Weak_FixedUpdate_m3559831028 (HIV_Weak_t1951470088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Weak::ChangeExcludedIndex(System.Int32)
extern "C"  void HIV_Weak_ChangeExcludedIndex_m1505635500 (HIV_Weak_t1951470088 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
