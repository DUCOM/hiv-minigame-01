﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Part
struct HIV_Part_t1695996321;
// HIV_Piece
struct HIV_Piece_t1005595762;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void HIV_Part::.ctor()
extern "C"  void HIV_Part__ctor_m540774812 (HIV_Part_t1695996321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIV_Part::get_color()
extern "C"  Color_t2020392075  HIV_Part_get_color_m1608807173 (HIV_Part_t1695996321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Part::set_color(UnityEngine.Color)
extern "C"  void HIV_Part_set_color_m3721088932 (HIV_Part_t1695996321 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV_Piece HIV_Part::get_script()
extern "C"  HIV_Piece_t1005595762 * HIV_Part_get_script_m1909646023 (HIV_Part_t1695996321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HIV_Part::get_instance()
extern "C"  GameObject_t1756533147 * HIV_Part_get_instance_m1161421855 (HIV_Part_t1695996321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Part::Instantiate()
extern "C"  void HIV_Part_Instantiate_m1276955724 (HIV_Part_t1695996321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Part::Instantiate(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void HIV_Part_Instantiate_m519142666 (HIV_Part_t1695996321 * __this, Transform_t3275118058 * ___parent0, Vector3_t2243707580  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Part::Instantiate(UnityEngine.Transform,UnityEngine.Color,UnityEngine.Vector3)
extern "C"  void HIV_Part_Instantiate_m2085730656 (HIV_Part_t1695996321 * __this, Transform_t3275118058 * ___parent0, Color_t2020392075  ___c1, Vector3_t2243707580  ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
