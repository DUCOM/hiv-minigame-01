﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FocalPointMove
struct FocalPointMove_t3911050876;
// FocalPoint
struct FocalPoint_t1860633653;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_FocalPoint_State2306468321.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FocalPoint
struct  FocalPoint_t1860633653  : public MonoBehaviour_t1158329972
{
public:
	// FocalPointMove FocalPoint::_move
	FocalPointMove_t3911050876 * ____move_2;
	// System.Single FocalPoint::_maxDistance
	float ____maxDistance_3;
	// System.Single FocalPoint::_additionalCutoff
	float ____additionalCutoff_4;
	// System.Single FocalPoint::_speed
	float ____speed_5;
	// FocalPoint/State FocalPoint::state
	int32_t ___state_6;

public:
	inline static int32_t get_offset_of__move_2() { return static_cast<int32_t>(offsetof(FocalPoint_t1860633653, ____move_2)); }
	inline FocalPointMove_t3911050876 * get__move_2() const { return ____move_2; }
	inline FocalPointMove_t3911050876 ** get_address_of__move_2() { return &____move_2; }
	inline void set__move_2(FocalPointMove_t3911050876 * value)
	{
		____move_2 = value;
		Il2CppCodeGenWriteBarrier(&____move_2, value);
	}

	inline static int32_t get_offset_of__maxDistance_3() { return static_cast<int32_t>(offsetof(FocalPoint_t1860633653, ____maxDistance_3)); }
	inline float get__maxDistance_3() const { return ____maxDistance_3; }
	inline float* get_address_of__maxDistance_3() { return &____maxDistance_3; }
	inline void set__maxDistance_3(float value)
	{
		____maxDistance_3 = value;
	}

	inline static int32_t get_offset_of__additionalCutoff_4() { return static_cast<int32_t>(offsetof(FocalPoint_t1860633653, ____additionalCutoff_4)); }
	inline float get__additionalCutoff_4() const { return ____additionalCutoff_4; }
	inline float* get_address_of__additionalCutoff_4() { return &____additionalCutoff_4; }
	inline void set__additionalCutoff_4(float value)
	{
		____additionalCutoff_4 = value;
	}

	inline static int32_t get_offset_of__speed_5() { return static_cast<int32_t>(offsetof(FocalPoint_t1860633653, ____speed_5)); }
	inline float get__speed_5() const { return ____speed_5; }
	inline float* get_address_of__speed_5() { return &____speed_5; }
	inline void set__speed_5(float value)
	{
		____speed_5 = value;
	}

	inline static int32_t get_offset_of_state_6() { return static_cast<int32_t>(offsetof(FocalPoint_t1860633653, ___state_6)); }
	inline int32_t get_state_6() const { return ___state_6; }
	inline int32_t* get_address_of_state_6() { return &___state_6; }
	inline void set_state_6(int32_t value)
	{
		___state_6 = value;
	}
};

struct FocalPoint_t1860633653_StaticFields
{
public:
	// FocalPoint FocalPoint::main
	FocalPoint_t1860633653 * ___main_7;

public:
	inline static int32_t get_offset_of_main_7() { return static_cast<int32_t>(offsetof(FocalPoint_t1860633653_StaticFields, ___main_7)); }
	inline FocalPoint_t1860633653 * get_main_7() const { return ___main_7; }
	inline FocalPoint_t1860633653 ** get_address_of_main_7() { return &___main_7; }
	inline void set_main_7(FocalPoint_t1860633653 * value)
	{
		___main_7 = value;
		Il2CppCodeGenWriteBarrier(&___main_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
