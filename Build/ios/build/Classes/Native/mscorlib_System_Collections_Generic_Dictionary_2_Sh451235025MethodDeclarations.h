﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>
struct ShimEnumerator_t451235025;
// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m200802578_gshared (ShimEnumerator_t451235025 * __this, Dictionary_2_t346110204 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m200802578(__this, ___host0, method) ((  void (*) (ShimEnumerator_t451235025 *, Dictionary_2_t346110204 *, const MethodInfo*))ShimEnumerator__ctor_m200802578_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1715155521_gshared (ShimEnumerator_t451235025 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1715155521(__this, method) ((  bool (*) (ShimEnumerator_t451235025 *, const MethodInfo*))ShimEnumerator_MoveNext_m1715155521_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1310890917_gshared (ShimEnumerator_t451235025 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1310890917(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t451235025 *, const MethodInfo*))ShimEnumerator_get_Entry_m1310890917_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m838022042_gshared (ShimEnumerator_t451235025 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m838022042(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t451235025 *, const MethodInfo*))ShimEnumerator_get_Key_m838022042_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3842768512_gshared (ShimEnumerator_t451235025 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3842768512(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t451235025 *, const MethodInfo*))ShimEnumerator_get_Value_m3842768512_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3551452152_gshared (ShimEnumerator_t451235025 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3551452152(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t451235025 *, const MethodInfo*))ShimEnumerator_get_Current_m3551452152_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/ReceptorType,System.Single>::Reset()
extern "C"  void ShimEnumerator_Reset_m4268292724_gshared (ShimEnumerator_t451235025 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4268292724(__this, method) ((  void (*) (ShimEnumerator_t451235025 *, const MethodInfo*))ShimEnumerator_Reset_m4268292724_gshared)(__this, method)
