﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Population
struct UI_Population_t2609643076;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Population::.ctor()
extern "C"  void UI_Population__ctor_m2359501799 (UI_Population_t2609643076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Population::Awake()
extern "C"  void UI_Population_Awake_m3958304368 (UI_Population_t2609643076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Population::OnGUI()
extern "C"  void UI_Population_OnGUI_m1257426857 (UI_Population_t2609643076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
