﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpriteRender
struct SpriteRender_t2828817349;
struct SpriteRender_t2828817349_marshaled_pinvoke;
struct SpriteRender_t2828817349_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SpriteRender_t2828817349;
struct SpriteRender_t2828817349_marshaled_pinvoke;

extern "C" void SpriteRender_t2828817349_marshal_pinvoke(const SpriteRender_t2828817349& unmarshaled, SpriteRender_t2828817349_marshaled_pinvoke& marshaled);
extern "C" void SpriteRender_t2828817349_marshal_pinvoke_back(const SpriteRender_t2828817349_marshaled_pinvoke& marshaled, SpriteRender_t2828817349& unmarshaled);
extern "C" void SpriteRender_t2828817349_marshal_pinvoke_cleanup(SpriteRender_t2828817349_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SpriteRender_t2828817349;
struct SpriteRender_t2828817349_marshaled_com;

extern "C" void SpriteRender_t2828817349_marshal_com(const SpriteRender_t2828817349& unmarshaled, SpriteRender_t2828817349_marshaled_com& marshaled);
extern "C" void SpriteRender_t2828817349_marshal_com_back(const SpriteRender_t2828817349_marshaled_com& marshaled, SpriteRender_t2828817349& unmarshaled);
extern "C" void SpriteRender_t2828817349_marshal_com_cleanup(SpriteRender_t2828817349_marshaled_com& marshaled);
