﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Developer_Stats
struct UI_Developer_Stats_t2682907225;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Developer_Stats::.ctor()
extern "C"  void UI_Developer_Stats__ctor_m3041775346 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Developer_Stats::OnGUI()
extern "C"  void UI_Developer_Stats_OnGUI_m1670899234 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UI_Developer_Stats::GetRBCPopulation()
extern "C"  String_t* UI_Developer_Stats_GetRBCPopulation_m1013808615 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UI_Developer_Stats::GetTCellPopulation()
extern "C"  String_t* UI_Developer_Stats_GetTCellPopulation_m1755623494 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UI_Developer_Stats::GetPopulation()
extern "C"  String_t* UI_Developer_Stats_GetPopulation_m1501463094 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UI_Developer_Stats::FPSCounter()
extern "C"  String_t* UI_Developer_Stats_FPSCounter_m342561264 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UI_Developer_Stats::GetFPS()
extern "C"  float UI_Developer_Stats_GetFPS_m346371025 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
