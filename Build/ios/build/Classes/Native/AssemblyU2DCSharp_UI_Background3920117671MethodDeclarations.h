﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Background
struct UI_Background_t3920117671;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Background::.ctor()
extern "C"  void UI_Background__ctor_m1698530442 (UI_Background_t3920117671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image UI_Background::get_image()
extern "C"  Image_t2042527209 * UI_Background_get_image_m339045305 (UI_Background_t3920117671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Background::Start()
extern "C"  void UI_Background_Start_m408145390 (UI_Background_t3920117671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Background::OnGUI()
extern "C"  void UI_Background_OnGUI_m396782758 (UI_Background_t3920117671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
