﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_207556710MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Antibody/State,AntibodyState>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1728859456(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1514233606 *, int32_t, AntibodyState_t3996126191 *, const MethodInfo*))KeyValuePair_2__ctor_m3091761441_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Antibody/State,AntibodyState>::get_Key()
#define KeyValuePair_2_get_Key_m1736241974(__this, method) ((  int32_t (*) (KeyValuePair_2_t1514233606 *, const MethodInfo*))KeyValuePair_2_get_Key_m621784971_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Antibody/State,AntibodyState>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2590692643(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1514233606 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2638512080_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Antibody/State,AntibodyState>::get_Value()
#define KeyValuePair_2_get_Value_m1664858804(__this, method) ((  AntibodyState_t3996126191 * (*) (KeyValuePair_2_t1514233606 *, const MethodInfo*))KeyValuePair_2_get_Value_m2717787259_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Antibody/State,AntibodyState>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3943350995(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1514233606 *, AntibodyState_t3996126191 *, const MethodInfo*))KeyValuePair_2_set_Value_m1193603312_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Antibody/State,AntibodyState>::ToString()
#define KeyValuePair_2_ToString_m1846729885(__this, method) ((  String_t* (*) (KeyValuePair_2_t1514233606 *, const MethodInfo*))KeyValuePair_2_ToString_m3238324930_gshared)(__this, method)
