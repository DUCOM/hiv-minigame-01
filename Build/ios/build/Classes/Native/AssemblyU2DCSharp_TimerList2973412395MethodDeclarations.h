﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimerList
struct TimerList_t2973412395;
// Timer
struct Timer_t2917042437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"

// System.Void TimerList::.ctor()
extern "C"  void TimerList__ctor_m2468386336 (TimerList_t2973412395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Timer TimerList::get_Item(System.Int32)
extern "C"  Timer_t2917042437 * TimerList_get_Item_m499053599 (TimerList_t2973412395 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Timer TimerList::get_Item(System.String)
extern "C"  Timer_t2917042437 * TimerList_get_Item_m1551049948 (TimerList_t2973412395 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Timer TimerList::GetTimer(System.Int32)
extern "C"  Timer_t2917042437 * TimerList_GetTimer_m2536853788 (TimerList_t2973412395 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Timer TimerList::GetTimer(System.String)
extern "C"  Timer_t2917042437 * TimerList_GetTimer_m3694739425 (TimerList_t2973412395 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerList::ResetTimer(Timer)
extern "C"  void TimerList_ResetTimer_m297055939 (TimerList_t2973412395 * __this, Timer_t2917042437 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerList::Awake()
extern "C"  void TimerList_Awake_m3789520375 (TimerList_t2973412395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerList::OnEnable()
extern "C"  void TimerList_OnEnable_m466275068 (TimerList_t2973412395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerList::LateUpdate()
extern "C"  void TimerList_LateUpdate_m438122191 (TimerList_t2973412395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerList::OnValidate()
extern "C"  void TimerList_OnValidate_m3943067401 (TimerList_t2973412395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
