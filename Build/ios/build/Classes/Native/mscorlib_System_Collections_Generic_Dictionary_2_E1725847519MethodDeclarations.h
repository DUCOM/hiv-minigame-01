﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<HIV/State,System.Object>
struct Dictionary_2_t405822817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1725847519.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22458135335.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m645837068_gshared (Enumerator_t1725847519 * __this, Dictionary_2_t405822817 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m645837068(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1725847519 *, Dictionary_2_t405822817 *, const MethodInfo*))Enumerator__ctor_m645837068_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3333250587_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3333250587(__this, method) ((  Il2CppObject * (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3333250587_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3044857175_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3044857175(__this, method) ((  void (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3044857175_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3287737962_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3287737962(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3287737962_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3340591321_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3340591321(__this, method) ((  Il2CppObject * (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3340591321_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1561029905_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1561029905(__this, method) ((  Il2CppObject * (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1561029905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2563378227_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2563378227(__this, method) ((  bool (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_MoveNext_m2563378227_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2458135335  Enumerator_get_Current_m2490648155_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2490648155(__this, method) ((  KeyValuePair_2_t2458135335  (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_get_Current_m2490648155_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m157871196_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m157871196(__this, method) ((  int32_t (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_get_CurrentKey_m157871196_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m130407004_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m130407004(__this, method) ((  Il2CppObject * (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_get_CurrentValue_m130407004_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3476599630_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3476599630(__this, method) ((  void (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_Reset_m3476599630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m788746469_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m788746469(__this, method) ((  void (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_VerifyState_m788746469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m411635231_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m411635231(__this, method) ((  void (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_VerifyCurrent_m411635231_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1491490344_gshared (Enumerator_t1725847519 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1491490344(__this, method) ((  void (*) (Enumerator_t1725847519 *, const MethodInfo*))Enumerator_Dispose_m1491490344_gshared)(__this, method)
