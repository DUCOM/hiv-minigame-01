﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Statistics>
struct DefaultComparer_t4159307168;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Statistics>::.ctor()
extern "C"  void DefaultComparer__ctor_m1653447069_gshared (DefaultComparer_t4159307168 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1653447069(__this, method) ((  void (*) (DefaultComparer_t4159307168 *, const MethodInfo*))DefaultComparer__ctor_m1653447069_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Statistics>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1434188536_gshared (DefaultComparer_t4159307168 * __this, Statistics_t2537377007  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1434188536(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4159307168 *, Statistics_t2537377007 , const MethodInfo*))DefaultComparer_GetHashCode_m1434188536_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Statistics>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m877529192_gshared (DefaultComparer_t4159307168 * __this, Statistics_t2537377007  ___x0, Statistics_t2537377007  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m877529192(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4159307168 *, Statistics_t2537377007 , Statistics_t2537377007 , const MethodInfo*))DefaultComparer_Equals_m877529192_gshared)(__this, ___x0, ___y1, method)
