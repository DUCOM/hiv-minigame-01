﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Developer_Button
struct  UI_Developer_Button_t3399765548  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UI_Developer_Button::stats
	GameObject_t1756533147 * ___stats_2;

public:
	inline static int32_t get_offset_of_stats_2() { return static_cast<int32_t>(offsetof(UI_Developer_Button_t3399765548, ___stats_2)); }
	inline GameObject_t1756533147 * get_stats_2() const { return ___stats_2; }
	inline GameObject_t1756533147 ** get_address_of_stats_2() { return &___stats_2; }
	inline void set_stats_2(GameObject_t1756533147 * value)
	{
		___stats_2 = value;
		Il2CppCodeGenWriteBarrier(&___stats_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
