﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Score1518975274.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Score::.ctor(System.Int32)
extern "C"  void Score__ctor_m51016996 (Score_t1518975274 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::Equals(System.Object)
extern "C"  bool Score_Equals_m1363800802 (Score_t1518975274 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Score::GetHashCode()
extern "C"  int32_t Score_GetHashCode_m195669770 (Score_t1518975274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::LessThan(Score)
extern "C"  bool Score_LessThan_m1927128681 (Score_t1518975274 * __this, Score_t1518975274  ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::op_Equality(Score,Score)
extern "C"  bool Score_op_Equality_m2255379791 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::op_LessThan(Score,Score)
extern "C"  bool Score_op_LessThan_m485616521 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::op_GreaterThan(Score,Score)
extern "C"  bool Score_op_GreaterThan_m2736154516 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::op_Inequality(Score,Score)
extern "C"  bool Score_op_Inequality_m423302070 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::op_LessThanOrEqual(Score,Score)
extern "C"  bool Score_op_LessThanOrEqual_m3420977628 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Score::op_GreaterThanOrEqual(Score,Score)
extern "C"  bool Score_op_GreaterThanOrEqual_m2728979991 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Score::ToString()
extern "C"  String_t* Score_ToString_m2662655366 (Score_t1518975274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
