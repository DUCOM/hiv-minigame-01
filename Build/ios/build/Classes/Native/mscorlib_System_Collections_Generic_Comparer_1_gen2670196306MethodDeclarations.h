﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<ImageRender>
struct Comparer_1_t2670196306;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<ImageRender>::.ctor()
extern "C"  void Comparer_1__ctor_m153211202_gshared (Comparer_1_t2670196306 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m153211202(__this, method) ((  void (*) (Comparer_1_t2670196306 *, const MethodInfo*))Comparer_1__ctor_m153211202_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<ImageRender>::.cctor()
extern "C"  void Comparer_1__cctor_m1897012391_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1897012391(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1897012391_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<ImageRender>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2582576615_gshared (Comparer_1_t2670196306 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m2582576615(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t2670196306 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m2582576615_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<ImageRender>::get_Default()
extern "C"  Comparer_1_t2670196306 * Comparer_1_get_Default_m4157703918_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m4157703918(__this /* static, unused */, method) ((  Comparer_1_t2670196306 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m4157703918_gshared)(__this /* static, unused */, method)
