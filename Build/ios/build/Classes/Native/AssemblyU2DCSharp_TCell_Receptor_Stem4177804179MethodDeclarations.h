﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Receptor_Stem
struct TCell_Receptor_Stem_t4177804179;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void TCell_Receptor_Stem::.ctor()
extern "C"  void TCell_Receptor_Stem__ctor_m90802210 (TCell_Receptor_Stem_t4177804179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TCell_Receptor_Stem::get_color()
extern "C"  Color_t2020392075  TCell_Receptor_Stem_get_color_m792012227 (TCell_Receptor_Stem_t4177804179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Receptor_Stem::set_color(UnityEngine.Color)
extern "C"  void TCell_Receptor_Stem_set_color_m2894468426 (TCell_Receptor_Stem_t4177804179 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
