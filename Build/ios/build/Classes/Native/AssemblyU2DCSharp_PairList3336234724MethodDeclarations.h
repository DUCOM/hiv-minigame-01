﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PairList
struct PairList_t3336234724;
// TCell_Pair
struct TCell_Pair_t2779000105;
// UnityEngine.Transform
struct Transform_t3275118058;
// TCell
struct TCell_t1815110036;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_TCell1815110036.h"

// System.Void PairList::.ctor()
extern "C"  void PairList__ctor_m1786578395 (PairList_t3336234724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PairList::get_count()
extern "C"  int32_t PairList_get_count_m1467844419 (PairList_t3336234724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TCell_Pair PairList::get_Item(System.Int32)
extern "C"  TCell_Pair_t2779000105 * PairList_get_Item_m1768557300 (PairList_t3336234724 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PairList::ChangeTropism()
extern "C"  void PairList_ChangeTropism_m453644655 (PairList_t3336234724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PairList::Instantiate(UnityEngine.Transform,System.Single)
extern "C"  void PairList_Instantiate_m1745309583 (PairList_t3336234724 * __this, Transform_t3275118058 * ___parent0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PairList::Instantiate(UnityEngine.Transform,System.Single,System.UInt32)
extern "C"  void PairList_Instantiate_m1391603615 (PairList_t3336234724 * __this, Transform_t3275118058 * ___parent0, float ___radius1, uint32_t ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PairList::Scale(System.Single)
extern "C"  void PairList_Scale_m1647140082 (PairList_t3336234724 * __this, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PairList::Disable()
extern "C"  void PairList_Disable_m1506047595 (PairList_t3336234724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PairList::Enable(TCell)
extern "C"  void PairList_Enable_m180892362 (PairList_t3336234724 * __this, TCell_t1815110036 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
