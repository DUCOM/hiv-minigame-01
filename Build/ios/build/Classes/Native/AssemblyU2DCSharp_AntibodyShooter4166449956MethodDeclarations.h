﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyShooter
struct AntibodyShooter_t4166449956;
// Pivot
struct Pivot_t2110476880;
// Antibody
struct Antibody_t2931822436;

#include "codegen/il2cpp-codegen.h"

// System.Void AntibodyShooter::.ctor()
extern "C"  void AntibodyShooter__ctor_m2289342925 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pivot AntibodyShooter::get_pivot()
extern "C"  Pivot_t2110476880 * AntibodyShooter_get_pivot_m2515030715 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyShooter::Awake()
extern "C"  void AntibodyShooter_Awake_m3355326212 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyShooter::OnEnable()
extern "C"  void AntibodyShooter_OnEnable_m2866300021 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Antibody AntibodyShooter::Shoot()
extern "C"  Antibody_t2931822436 * AntibodyShooter_Shoot_m4294917967 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
