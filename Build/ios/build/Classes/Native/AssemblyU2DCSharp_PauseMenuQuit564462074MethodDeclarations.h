﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PauseMenuQuit
struct PauseMenuQuit_t564462074;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void PauseMenuQuit::.ctor()
extern "C"  void PauseMenuQuit__ctor_m1289761379 (PauseMenuQuit_t564462074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenuQuit::QuitConfirm(UnityEngine.GameObject)
extern "C"  void PauseMenuQuit_QuitConfirm_m1046657798 (PauseMenuQuit_t564462074 * __this, GameObject_t1756533147 * ___quitConfirm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
