﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Cytoplasm
struct TCell_Cytoplasm_t256238633;

#include "codegen/il2cpp-codegen.h"

// System.Void TCell_Cytoplasm::.ctor()
extern "C"  void TCell_Cytoplasm__ctor_m725607054 (TCell_Cytoplasm_t256238633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
