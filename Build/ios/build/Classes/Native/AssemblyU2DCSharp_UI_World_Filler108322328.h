﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UI_World_Filler
struct UI_World_Filler_t108322328;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_World_Filler
struct  UI_World_Filler_t108322328  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image UI_World_Filler::_filler
	Image_t2042527209 * ____filler_3;

public:
	inline static int32_t get_offset_of__filler_3() { return static_cast<int32_t>(offsetof(UI_World_Filler_t108322328, ____filler_3)); }
	inline Image_t2042527209 * get__filler_3() const { return ____filler_3; }
	inline Image_t2042527209 ** get_address_of__filler_3() { return &____filler_3; }
	inline void set__filler_3(Image_t2042527209 * value)
	{
		____filler_3 = value;
		Il2CppCodeGenWriteBarrier(&____filler_3, value);
	}
};

struct UI_World_Filler_t108322328_StaticFields
{
public:
	// UI_World_Filler UI_World_Filler::main
	UI_World_Filler_t108322328 * ___main_2;

public:
	inline static int32_t get_offset_of_main_2() { return static_cast<int32_t>(offsetof(UI_World_Filler_t108322328_StaticFields, ___main_2)); }
	inline UI_World_Filler_t108322328 * get_main_2() const { return ___main_2; }
	inline UI_World_Filler_t108322328 ** get_address_of_main_2() { return &___main_2; }
	inline void set_main_2(UI_World_Filler_t108322328 * value)
	{
		___main_2 = value;
		Il2CppCodeGenWriteBarrier(&___main_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
