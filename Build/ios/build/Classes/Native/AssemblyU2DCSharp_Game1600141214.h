﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TimerList
struct TimerList_t2973412395;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<HIV>
struct List_1_t1850888877;
// System.Collections.Generic.List`1<TCell>
struct List_1_t1184231168;
// System.Collections.Generic.List`1<RedBloodCell>
struct List_1_t1745920309;
// AntibodyShooter
struct AntibodyShooter_t4166449956;
// System.Collections.Generic.List`1<Antibody>
struct List_1_t2300943568;
// Game
struct Game_t1600141214;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game
struct  Game_t1600141214  : public MonoBehaviour_t1158329972
{
public:
	// TimerList Game::_timers
	TimerList_t2973412395 * ____timers_2;
	// UnityEngine.GameObject Game::hiv
	GameObject_t1756533147 * ___hiv_3;
	// System.Single Game::hivRadius
	float ___hivRadius_4;
	// UnityEngine.Vector3 Game::hivSpawn
	Vector3_t2243707580  ___hivSpawn_5;
	// System.Int32 Game::populationMax
	int32_t ___populationMax_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::_hivList
	List_1_t1125654279 * ____hivList_7;
	// System.Collections.Generic.List`1<HIV> Game::_hivScripts
	List_1_t1850888877 * ____hivScripts_8;
	// UnityEngine.GameObject Game::_tCell
	GameObject_t1756533147 * ____tCell_9;
	// System.Single Game::_tCellRadius
	float ____tCellRadius_10;
	// System.Single Game::_tCellRadiusMin
	float ____tCellRadiusMin_11;
	// System.Single Game::_tCellRadiusMax
	float ____tCellRadiusMax_12;
	// System.Single Game::_spawnBorder
	float ____spawnBorder_13;
	// System.Int32 Game::_tCellPopMax
	int32_t ____tCellPopMax_14;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::_tCellList
	List_1_t1125654279 * ____tCellList_15;
	// System.Collections.Generic.List`1<TCell> Game::_tCellScripts
	List_1_t1184231168 * ____tCellScripts_16;
	// UnityEngine.GameObject Game::redBloodCell
	GameObject_t1756533147 * ___redBloodCell_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::_rbcList
	List_1_t1125654279 * ____rbcList_18;
	// System.Collections.Generic.List`1<RedBloodCell> Game::_rbcScripts
	List_1_t1745920309 * ____rbcScripts_19;
	// System.Int32 Game::_rbcPopMax
	int32_t ____rbcPopMax_20;
	// System.Single Game::rbcTimer
	float ___rbcTimer_21;
	// AntibodyShooter Game::_antibodyShooter
	AntibodyShooter_t4166449956 * ____antibodyShooter_22;
	// System.Collections.Generic.List`1<Antibody> Game::_antibodyList
	List_1_t2300943568 * ____antibodyList_23;
	// System.Int32 Game::score
	int32_t ___score_24;
	// System.Int32 Game::level
	int32_t ___level_25;
	// UnityEngine.GameObject Game::bCellObject
	GameObject_t1756533147 * ___bCellObject_26;
	// TimerList Game::bCellTimer
	TimerList_t2973412395 * ___bCellTimer_27;
	// UnityEngine.GameObject Game::warning
	GameObject_t1756533147 * ___warning_28;
	// System.Single Game::battleTimer
	float ___battleTimer_29;

public:
	inline static int32_t get_offset_of__timers_2() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____timers_2)); }
	inline TimerList_t2973412395 * get__timers_2() const { return ____timers_2; }
	inline TimerList_t2973412395 ** get_address_of__timers_2() { return &____timers_2; }
	inline void set__timers_2(TimerList_t2973412395 * value)
	{
		____timers_2 = value;
		Il2CppCodeGenWriteBarrier(&____timers_2, value);
	}

	inline static int32_t get_offset_of_hiv_3() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___hiv_3)); }
	inline GameObject_t1756533147 * get_hiv_3() const { return ___hiv_3; }
	inline GameObject_t1756533147 ** get_address_of_hiv_3() { return &___hiv_3; }
	inline void set_hiv_3(GameObject_t1756533147 * value)
	{
		___hiv_3 = value;
		Il2CppCodeGenWriteBarrier(&___hiv_3, value);
	}

	inline static int32_t get_offset_of_hivRadius_4() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___hivRadius_4)); }
	inline float get_hivRadius_4() const { return ___hivRadius_4; }
	inline float* get_address_of_hivRadius_4() { return &___hivRadius_4; }
	inline void set_hivRadius_4(float value)
	{
		___hivRadius_4 = value;
	}

	inline static int32_t get_offset_of_hivSpawn_5() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___hivSpawn_5)); }
	inline Vector3_t2243707580  get_hivSpawn_5() const { return ___hivSpawn_5; }
	inline Vector3_t2243707580 * get_address_of_hivSpawn_5() { return &___hivSpawn_5; }
	inline void set_hivSpawn_5(Vector3_t2243707580  value)
	{
		___hivSpawn_5 = value;
	}

	inline static int32_t get_offset_of_populationMax_6() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___populationMax_6)); }
	inline int32_t get_populationMax_6() const { return ___populationMax_6; }
	inline int32_t* get_address_of_populationMax_6() { return &___populationMax_6; }
	inline void set_populationMax_6(int32_t value)
	{
		___populationMax_6 = value;
	}

	inline static int32_t get_offset_of__hivList_7() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____hivList_7)); }
	inline List_1_t1125654279 * get__hivList_7() const { return ____hivList_7; }
	inline List_1_t1125654279 ** get_address_of__hivList_7() { return &____hivList_7; }
	inline void set__hivList_7(List_1_t1125654279 * value)
	{
		____hivList_7 = value;
		Il2CppCodeGenWriteBarrier(&____hivList_7, value);
	}

	inline static int32_t get_offset_of__hivScripts_8() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____hivScripts_8)); }
	inline List_1_t1850888877 * get__hivScripts_8() const { return ____hivScripts_8; }
	inline List_1_t1850888877 ** get_address_of__hivScripts_8() { return &____hivScripts_8; }
	inline void set__hivScripts_8(List_1_t1850888877 * value)
	{
		____hivScripts_8 = value;
		Il2CppCodeGenWriteBarrier(&____hivScripts_8, value);
	}

	inline static int32_t get_offset_of__tCell_9() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCell_9)); }
	inline GameObject_t1756533147 * get__tCell_9() const { return ____tCell_9; }
	inline GameObject_t1756533147 ** get_address_of__tCell_9() { return &____tCell_9; }
	inline void set__tCell_9(GameObject_t1756533147 * value)
	{
		____tCell_9 = value;
		Il2CppCodeGenWriteBarrier(&____tCell_9, value);
	}

	inline static int32_t get_offset_of__tCellRadius_10() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCellRadius_10)); }
	inline float get__tCellRadius_10() const { return ____tCellRadius_10; }
	inline float* get_address_of__tCellRadius_10() { return &____tCellRadius_10; }
	inline void set__tCellRadius_10(float value)
	{
		____tCellRadius_10 = value;
	}

	inline static int32_t get_offset_of__tCellRadiusMin_11() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCellRadiusMin_11)); }
	inline float get__tCellRadiusMin_11() const { return ____tCellRadiusMin_11; }
	inline float* get_address_of__tCellRadiusMin_11() { return &____tCellRadiusMin_11; }
	inline void set__tCellRadiusMin_11(float value)
	{
		____tCellRadiusMin_11 = value;
	}

	inline static int32_t get_offset_of__tCellRadiusMax_12() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCellRadiusMax_12)); }
	inline float get__tCellRadiusMax_12() const { return ____tCellRadiusMax_12; }
	inline float* get_address_of__tCellRadiusMax_12() { return &____tCellRadiusMax_12; }
	inline void set__tCellRadiusMax_12(float value)
	{
		____tCellRadiusMax_12 = value;
	}

	inline static int32_t get_offset_of__spawnBorder_13() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____spawnBorder_13)); }
	inline float get__spawnBorder_13() const { return ____spawnBorder_13; }
	inline float* get_address_of__spawnBorder_13() { return &____spawnBorder_13; }
	inline void set__spawnBorder_13(float value)
	{
		____spawnBorder_13 = value;
	}

	inline static int32_t get_offset_of__tCellPopMax_14() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCellPopMax_14)); }
	inline int32_t get__tCellPopMax_14() const { return ____tCellPopMax_14; }
	inline int32_t* get_address_of__tCellPopMax_14() { return &____tCellPopMax_14; }
	inline void set__tCellPopMax_14(int32_t value)
	{
		____tCellPopMax_14 = value;
	}

	inline static int32_t get_offset_of__tCellList_15() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCellList_15)); }
	inline List_1_t1125654279 * get__tCellList_15() const { return ____tCellList_15; }
	inline List_1_t1125654279 ** get_address_of__tCellList_15() { return &____tCellList_15; }
	inline void set__tCellList_15(List_1_t1125654279 * value)
	{
		____tCellList_15 = value;
		Il2CppCodeGenWriteBarrier(&____tCellList_15, value);
	}

	inline static int32_t get_offset_of__tCellScripts_16() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____tCellScripts_16)); }
	inline List_1_t1184231168 * get__tCellScripts_16() const { return ____tCellScripts_16; }
	inline List_1_t1184231168 ** get_address_of__tCellScripts_16() { return &____tCellScripts_16; }
	inline void set__tCellScripts_16(List_1_t1184231168 * value)
	{
		____tCellScripts_16 = value;
		Il2CppCodeGenWriteBarrier(&____tCellScripts_16, value);
	}

	inline static int32_t get_offset_of_redBloodCell_17() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___redBloodCell_17)); }
	inline GameObject_t1756533147 * get_redBloodCell_17() const { return ___redBloodCell_17; }
	inline GameObject_t1756533147 ** get_address_of_redBloodCell_17() { return &___redBloodCell_17; }
	inline void set_redBloodCell_17(GameObject_t1756533147 * value)
	{
		___redBloodCell_17 = value;
		Il2CppCodeGenWriteBarrier(&___redBloodCell_17, value);
	}

	inline static int32_t get_offset_of__rbcList_18() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____rbcList_18)); }
	inline List_1_t1125654279 * get__rbcList_18() const { return ____rbcList_18; }
	inline List_1_t1125654279 ** get_address_of__rbcList_18() { return &____rbcList_18; }
	inline void set__rbcList_18(List_1_t1125654279 * value)
	{
		____rbcList_18 = value;
		Il2CppCodeGenWriteBarrier(&____rbcList_18, value);
	}

	inline static int32_t get_offset_of__rbcScripts_19() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____rbcScripts_19)); }
	inline List_1_t1745920309 * get__rbcScripts_19() const { return ____rbcScripts_19; }
	inline List_1_t1745920309 ** get_address_of__rbcScripts_19() { return &____rbcScripts_19; }
	inline void set__rbcScripts_19(List_1_t1745920309 * value)
	{
		____rbcScripts_19 = value;
		Il2CppCodeGenWriteBarrier(&____rbcScripts_19, value);
	}

	inline static int32_t get_offset_of__rbcPopMax_20() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____rbcPopMax_20)); }
	inline int32_t get__rbcPopMax_20() const { return ____rbcPopMax_20; }
	inline int32_t* get_address_of__rbcPopMax_20() { return &____rbcPopMax_20; }
	inline void set__rbcPopMax_20(int32_t value)
	{
		____rbcPopMax_20 = value;
	}

	inline static int32_t get_offset_of_rbcTimer_21() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___rbcTimer_21)); }
	inline float get_rbcTimer_21() const { return ___rbcTimer_21; }
	inline float* get_address_of_rbcTimer_21() { return &___rbcTimer_21; }
	inline void set_rbcTimer_21(float value)
	{
		___rbcTimer_21 = value;
	}

	inline static int32_t get_offset_of__antibodyShooter_22() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____antibodyShooter_22)); }
	inline AntibodyShooter_t4166449956 * get__antibodyShooter_22() const { return ____antibodyShooter_22; }
	inline AntibodyShooter_t4166449956 ** get_address_of__antibodyShooter_22() { return &____antibodyShooter_22; }
	inline void set__antibodyShooter_22(AntibodyShooter_t4166449956 * value)
	{
		____antibodyShooter_22 = value;
		Il2CppCodeGenWriteBarrier(&____antibodyShooter_22, value);
	}

	inline static int32_t get_offset_of__antibodyList_23() { return static_cast<int32_t>(offsetof(Game_t1600141214, ____antibodyList_23)); }
	inline List_1_t2300943568 * get__antibodyList_23() const { return ____antibodyList_23; }
	inline List_1_t2300943568 ** get_address_of__antibodyList_23() { return &____antibodyList_23; }
	inline void set__antibodyList_23(List_1_t2300943568 * value)
	{
		____antibodyList_23 = value;
		Il2CppCodeGenWriteBarrier(&____antibodyList_23, value);
	}

	inline static int32_t get_offset_of_score_24() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___score_24)); }
	inline int32_t get_score_24() const { return ___score_24; }
	inline int32_t* get_address_of_score_24() { return &___score_24; }
	inline void set_score_24(int32_t value)
	{
		___score_24 = value;
	}

	inline static int32_t get_offset_of_level_25() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___level_25)); }
	inline int32_t get_level_25() const { return ___level_25; }
	inline int32_t* get_address_of_level_25() { return &___level_25; }
	inline void set_level_25(int32_t value)
	{
		___level_25 = value;
	}

	inline static int32_t get_offset_of_bCellObject_26() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___bCellObject_26)); }
	inline GameObject_t1756533147 * get_bCellObject_26() const { return ___bCellObject_26; }
	inline GameObject_t1756533147 ** get_address_of_bCellObject_26() { return &___bCellObject_26; }
	inline void set_bCellObject_26(GameObject_t1756533147 * value)
	{
		___bCellObject_26 = value;
		Il2CppCodeGenWriteBarrier(&___bCellObject_26, value);
	}

	inline static int32_t get_offset_of_bCellTimer_27() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___bCellTimer_27)); }
	inline TimerList_t2973412395 * get_bCellTimer_27() const { return ___bCellTimer_27; }
	inline TimerList_t2973412395 ** get_address_of_bCellTimer_27() { return &___bCellTimer_27; }
	inline void set_bCellTimer_27(TimerList_t2973412395 * value)
	{
		___bCellTimer_27 = value;
		Il2CppCodeGenWriteBarrier(&___bCellTimer_27, value);
	}

	inline static int32_t get_offset_of_warning_28() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___warning_28)); }
	inline GameObject_t1756533147 * get_warning_28() const { return ___warning_28; }
	inline GameObject_t1756533147 ** get_address_of_warning_28() { return &___warning_28; }
	inline void set_warning_28(GameObject_t1756533147 * value)
	{
		___warning_28 = value;
		Il2CppCodeGenWriteBarrier(&___warning_28, value);
	}

	inline static int32_t get_offset_of_battleTimer_29() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___battleTimer_29)); }
	inline float get_battleTimer_29() const { return ___battleTimer_29; }
	inline float* get_address_of_battleTimer_29() { return &___battleTimer_29; }
	inline void set_battleTimer_29(float value)
	{
		___battleTimer_29 = value;
	}
};

struct Game_t1600141214_StaticFields
{
public:
	// Game Game::main
	Game_t1600141214 * ___main_30;

public:
	inline static int32_t get_offset_of_main_30() { return static_cast<int32_t>(offsetof(Game_t1600141214_StaticFields, ___main_30)); }
	inline Game_t1600141214 * get_main_30() const { return ___main_30; }
	inline Game_t1600141214 ** get_address_of_main_30() { return &___main_30; }
	inline void set_main_30(Game_t1600141214 * value)
	{
		___main_30 = value;
		Il2CppCodeGenWriteBarrier(&___main_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
