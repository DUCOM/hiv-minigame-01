﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIVStats
struct HIVStats_t2521285894;
// System.Collections.Generic.List`1<Statistics>
struct List_1_t1906498139;
// HIV_Piece
struct HIV_Piece_t1005595762;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "AssemblyU2DCSharp_HIV_Piece1005595762.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void HIVStats::.ctor()
extern "C"  void HIVStats__ctor_m26403949 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics HIVStats::get_stats()
extern "C"  Statistics_t2537377007  HIVStats_get_stats_m1941178495 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVStats::set_stats(Statistics)
extern "C"  void HIVStats_set_stats_m177167640 (HIVStats_t2521285894 * __this, Statistics_t2537377007  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HIVStats::get_health()
extern "C"  float HIVStats_get_health_m2635922628 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVStats::set_health(System.Single)
extern "C"  void HIVStats_set_health_m1015513025 (HIVStats_t2521285894 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HIVStats::get_alive()
extern "C"  bool HIVStats_get_alive_m2021401439 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HIVStats::get_hp()
extern "C"  float HIVStats_get_hp_m2274879970 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVStats::RefreshStats(System.Collections.Generic.List`1<Statistics>,HIV_Piece)
extern "C"  void HIVStats_RefreshStats_m3154914018 (HIVStats_t2521285894 * __this, List_1_t1906498139 * ___statChanges0, HIV_Piece_t1005595762 * ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVStats::ResetTimer()
extern "C"  void HIVStats_ResetTimer_m2297303603 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HIVStats::CurrentColor()
extern "C"  Color_t2020392075  HIVStats_CurrentColor_m2490660782 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVStats::Update()
extern "C"  void HIVStats_Update_m688324164 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVStats::LateUpdate()
extern "C"  void HIVStats_LateUpdate_m2726591320 (HIVStats_t2521285894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
