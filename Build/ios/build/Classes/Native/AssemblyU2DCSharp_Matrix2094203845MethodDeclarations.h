﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Matrix
struct Matrix_t2094203845;

#include "codegen/il2cpp-codegen.h"

// System.Void Matrix::.ctor()
extern "C"  void Matrix__ctor_m2389865400 (Matrix_t2094203845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Matrix::ChangeStats()
extern "C"  void Matrix_ChangeStats_m1571985379 (Matrix_t2094203845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
