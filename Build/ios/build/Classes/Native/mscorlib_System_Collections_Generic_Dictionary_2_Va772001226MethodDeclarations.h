﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3403849956MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3689836542(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t772001226 *, Dictionary_2_t2068941383 *, const MethodInfo*))ValueCollection__ctor_m3675885603_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4095293764(__this, ___item0, method) ((  void (*) (ValueCollection_t772001226 *, HIV_State_t57600565 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1020789705_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m550133455(__this, method) ((  void (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1663196884_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m97049520(__this, ___item0, method) ((  bool (*) (ValueCollection_t772001226 *, HIV_State_t57600565 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2901311439_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m680755135(__this, ___item0, method) ((  bool (*) (ValueCollection_t772001226 *, HIV_State_t57600565 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1767065214_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m448814297(__this, method) ((  Il2CppObject* (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1985508632_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3464493373(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t772001226 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1695589528_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2049227196(__this, method) ((  Il2CppObject * (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062407325_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3787143507(__this, method) ((  bool (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2696719474_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4228300809(__this, method) ((  bool (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1231113640_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m443474829(__this, method) ((  Il2CppObject * (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4181673192_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2419226927(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t772001226 *, HIV_StateU5BU5D_t2748966232*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3657206804_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4221532692(__this, method) ((  Enumerator_t3755474147  (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_GetEnumerator_m288204367_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,HIV_State>::get_Count()
#define ValueCollection_get_Count_m514015601(__this, method) ((  int32_t (*) (ValueCollection_t772001226 *, const MethodInfo*))ValueCollection_get_Count_m1654729584_gshared)(__this, method)
