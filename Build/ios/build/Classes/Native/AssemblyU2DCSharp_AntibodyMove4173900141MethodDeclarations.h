﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyMove
struct AntibodyMove_t4173900141;

#include "codegen/il2cpp-codegen.h"

// System.Void AntibodyMove::.ctor()
extern "C"  void AntibodyMove__ctor_m930291536 (AntibodyMove_t4173900141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
