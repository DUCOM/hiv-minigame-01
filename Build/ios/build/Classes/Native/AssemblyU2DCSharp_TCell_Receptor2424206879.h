﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell_Receptor
struct  TCell_Receptor_t2424206879  : public MonoBehaviour_t1158329972
{
public:
	// Tropism TCell_Receptor::_type
	Tropism_t3662836552  ____type_2;

public:
	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(TCell_Receptor_t2424206879, ____type_2)); }
	inline Tropism_t3662836552  get__type_2() const { return ____type_2; }
	inline Tropism_t3662836552 * get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(Tropism_t3662836552  value)
	{
		____type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
