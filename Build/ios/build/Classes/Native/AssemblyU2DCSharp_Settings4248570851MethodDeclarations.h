﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Settings
struct Settings_t4248570851;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingList4161291060.h"

// System.Void Settings::.ctor()
extern "C"  void Settings__ctor_m3923603048 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SettingList Settings::get_settings()
extern "C"  SettingList_t4161291060  Settings_get_settings_m1527194397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::OnEnable()
extern "C"  void Settings_OnEnable_m1757321700 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::OnDisable()
extern "C"  void Settings_OnDisable_m1422008631 (Settings_t4248570851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::ChangeVolume(System.Single)
extern "C"  void Settings_ChangeVolume_m1043473565 (Settings_t4248570851 * __this, float ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Settings::Save()
extern "C"  bool Settings_Save_m3326872871 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Settings::Load()
extern "C"  bool Settings_Load_m2929002520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::Refresh()
extern "C"  void Settings_Refresh_m4147788479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Settings::ResetToLoad()
extern "C"  void Settings_ResetToLoad_m3865441310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
