﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Score[]
struct ScoreU5BU5D_t100793071;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreList
struct  ScoreList_t2288120898  : public Il2CppObject
{
public:
	// Score[] ScoreList::highScores
	ScoreU5BU5D_t100793071* ___highScores_0;

public:
	inline static int32_t get_offset_of_highScores_0() { return static_cast<int32_t>(offsetof(ScoreList_t2288120898, ___highScores_0)); }
	inline ScoreU5BU5D_t100793071* get_highScores_0() const { return ___highScores_0; }
	inline ScoreU5BU5D_t100793071** get_address_of_highScores_0() { return &___highScores_0; }
	inline void set_highScores_0(ScoreU5BU5D_t100793071* value)
	{
		___highScores_0 = value;
		Il2CppCodeGenWriteBarrier(&___highScores_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
