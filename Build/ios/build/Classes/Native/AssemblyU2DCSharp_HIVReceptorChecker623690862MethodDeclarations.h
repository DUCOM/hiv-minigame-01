﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIVReceptorChecker
struct HIVReceptorChecker_t623690862;
// HIV
struct HIV_t2481767745;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// TCell_Receptor
struct TCell_Receptor_t2424206879;
// TCell
struct TCell_t1815110036;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879.h"
#include "AssemblyU2DCSharp_TCell1815110036.h"

// System.Void HIVReceptorChecker::.ctor()
extern "C"  void HIVReceptorChecker__ctor_m2287392421 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism HIVReceptorChecker::get_tropism()
extern "C"  Tropism_t3662836552  HIVReceptorChecker_get_tropism_m3546347411 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV HIVReceptorChecker::get_main()
extern "C"  HIV_t2481767745 * HIVReceptorChecker_get_main_m3762925379 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HIVReceptorChecker::get__timer()
extern "C"  float HIVReceptorChecker_get__timer_m1697399096 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVReceptorChecker::set__timer(System.Single)
extern "C"  void HIVReceptorChecker_set__timer_m2774836377 (HIVReceptorChecker_t623690862 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVReceptorChecker::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void HIVReceptorChecker_OnTriggerEnter2D_m1792328533 (HIVReceptorChecker_t623690862 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVReceptorChecker::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void HIVReceptorChecker_OnTriggerStay2D_m3688501010 (HIVReceptorChecker_t623690862 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVReceptorChecker::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void HIVReceptorChecker_OnTriggerExit2D_m1553920893 (HIVReceptorChecker_t623690862 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HIVReceptorChecker::validCombination(TCell_Receptor)
extern "C"  bool HIVReceptorChecker_validCombination_m1694857471 (HIVReceptorChecker_t623690862 * __this, TCell_Receptor_t2424206879 * ___check0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIVReceptorChecker::Infect(TCell)
extern "C"  void HIVReceptorChecker_Infect_m2960205694 (HIVReceptorChecker_t623690862 * __this, TCell_t1815110036 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
