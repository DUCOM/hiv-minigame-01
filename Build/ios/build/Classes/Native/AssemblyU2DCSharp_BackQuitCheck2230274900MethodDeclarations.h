﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BackQuitCheck
struct BackQuitCheck_t2230274900;

#include "codegen/il2cpp-codegen.h"

// System.Void BackQuitCheck::.ctor()
extern "C"  void BackQuitCheck__ctor_m2668904227 (BackQuitCheck_t2230274900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BackQuitCheck::Update()
extern "C"  void BackQuitCheck_Update_m1830188662 (BackQuitCheck_t2230274900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
