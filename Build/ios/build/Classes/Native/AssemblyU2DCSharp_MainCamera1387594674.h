﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainCamera
struct  MainCamera_t1387594674  : public MonoBehaviour_t1158329972
{
public:
	// System.Single MainCamera::_speed
	float ____speed_2;

public:
	inline static int32_t get_offset_of__speed_2() { return static_cast<int32_t>(offsetof(MainCamera_t1387594674, ____speed_2)); }
	inline float get__speed_2() const { return ____speed_2; }
	inline float* get_address_of__speed_2() { return &____speed_2; }
	inline void set__speed_2(float value)
	{
		____speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
