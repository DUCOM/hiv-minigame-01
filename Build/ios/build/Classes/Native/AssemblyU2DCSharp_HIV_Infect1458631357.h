﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_HIV_State57600565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIV_Infect
struct  HIV_Infect_t1458631357  : public HIV_State_t57600565
{
public:
	// System.Single HIV_Infect::infectDistance
	float ___infectDistance_4;

public:
	inline static int32_t get_offset_of_infectDistance_4() { return static_cast<int32_t>(offsetof(HIV_Infect_t1458631357, ___infectDistance_4)); }
	inline float get_infectDistance_4() const { return ___infectDistance_4; }
	inline float* get_address_of_infectDistance_4() { return &___infectDistance_4; }
	inline void set_infectDistance_4(float value)
	{
		___infectDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
