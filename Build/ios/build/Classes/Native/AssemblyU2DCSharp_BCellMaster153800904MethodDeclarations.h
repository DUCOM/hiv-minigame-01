﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BCellMaster
struct BCellMaster_t153800904;

#include "codegen/il2cpp-codegen.h"

// System.Void BCellMaster::.ctor()
extern "C"  void BCellMaster__ctor_m1875056559 (BCellMaster_t153800904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BCellMaster::OnValidate()
extern "C"  void BCellMaster_OnValidate_m1342664764 (BCellMaster_t153800904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
