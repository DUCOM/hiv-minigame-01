﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2377727536.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Score1518975274.h"

// System.Void System.Array/InternalEnumerator`1<Score>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m756934715_gshared (InternalEnumerator_1_t2377727536 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m756934715(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2377727536 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m756934715_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Score>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1063681739_gshared (InternalEnumerator_1_t2377727536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1063681739(__this, method) ((  void (*) (InternalEnumerator_1_t2377727536 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1063681739_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Score>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3728715467_gshared (InternalEnumerator_1_t2377727536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3728715467(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2377727536 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3728715467_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Score>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3626115158_gshared (InternalEnumerator_1_t2377727536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3626115158(__this, method) ((  void (*) (InternalEnumerator_1_t2377727536 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3626115158_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Score>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2951595375_gshared (InternalEnumerator_1_t2377727536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2951595375(__this, method) ((  bool (*) (InternalEnumerator_1_t2377727536 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2951595375_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Score>::get_Current()
extern "C"  Score_t1518975274  InternalEnumerator_1_get_Current_m2087599570_gshared (InternalEnumerator_1_t2377727536 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2087599570(__this, method) ((  Score_t1518975274  (*) (InternalEnumerator_1_t2377727536 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2087599570_gshared)(__this, method)
