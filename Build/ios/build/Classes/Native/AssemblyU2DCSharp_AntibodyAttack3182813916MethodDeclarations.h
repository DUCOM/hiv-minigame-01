﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyAttack
struct AntibodyAttack_t3182813916;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void AntibodyAttack::.ctor()
extern "C"  void AntibodyAttack__ctor_m1499247545 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AntibodyAttack::AngleToTarget()
extern "C"  float AntibodyAttack_AngleToTarget_m734917698 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyAttack::Latch()
extern "C"  void AntibodyAttack_Latch_m3793772755 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyAttack::OnEnable()
extern "C"  void AntibodyAttack_OnEnable_m253959753 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyAttack::OnDisable()
extern "C"  void AntibodyAttack_OnDisable_m2583327596 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyAttack::Update()
extern "C"  void AntibodyAttack_Update_m4224836906 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyAttack::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void AntibodyAttack_OnTriggerExit2D_m979340769 (AntibodyAttack_t3182813916 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
