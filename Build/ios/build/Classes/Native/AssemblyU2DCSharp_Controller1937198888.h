﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Controller
struct Controller_t1937198888;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Controller_LeftState2928884177.h"
#include "AssemblyU2DCSharp_Controller_RightState861943514.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller
struct  Controller_t1937198888  : public MonoBehaviour_t1158329972
{
public:
	// Controller/LeftState Controller::l_state
	int32_t ___l_state_3;
	// Controller/RightState Controller::r_state
	int32_t ___r_state_4;
	// UnityEngine.Vector2 Controller::lastLocation
	Vector2_t2243707579  ___lastLocation_5;

public:
	inline static int32_t get_offset_of_l_state_3() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___l_state_3)); }
	inline int32_t get_l_state_3() const { return ___l_state_3; }
	inline int32_t* get_address_of_l_state_3() { return &___l_state_3; }
	inline void set_l_state_3(int32_t value)
	{
		___l_state_3 = value;
	}

	inline static int32_t get_offset_of_r_state_4() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___r_state_4)); }
	inline int32_t get_r_state_4() const { return ___r_state_4; }
	inline int32_t* get_address_of_r_state_4() { return &___r_state_4; }
	inline void set_r_state_4(int32_t value)
	{
		___r_state_4 = value;
	}

	inline static int32_t get_offset_of_lastLocation_5() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___lastLocation_5)); }
	inline Vector2_t2243707579  get_lastLocation_5() const { return ___lastLocation_5; }
	inline Vector2_t2243707579 * get_address_of_lastLocation_5() { return &___lastLocation_5; }
	inline void set_lastLocation_5(Vector2_t2243707579  value)
	{
		___lastLocation_5 = value;
	}
};

struct Controller_t1937198888_StaticFields
{
public:
	// Controller Controller::main
	Controller_t1937198888 * ___main_2;

public:
	inline static int32_t get_offset_of_main_2() { return static_cast<int32_t>(offsetof(Controller_t1937198888_StaticFields, ___main_2)); }
	inline Controller_t1937198888 * get_main_2() const { return ___main_2; }
	inline Controller_t1937198888 ** get_address_of_main_2() { return &___main_2; }
	inline void set_main_2(Controller_t1937198888 * value)
	{
		___main_2 = value;
		Il2CppCodeGenWriteBarrier(&___main_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
