﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22398422722.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3932324153_gshared (KeyValuePair_2_t2398422722 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3932324153(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2398422722 *, int32_t, float, const MethodInfo*))KeyValuePair_2__ctor_m3932324153_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2124728575_gshared (KeyValuePair_2_t2398422722 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2124728575(__this, method) ((  int32_t (*) (KeyValuePair_2_t2398422722 *, const MethodInfo*))KeyValuePair_2_get_Key_m2124728575_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m662718016_gshared (KeyValuePair_2_t2398422722 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m662718016(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2398422722 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m662718016_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m3903021303_gshared (KeyValuePair_2_t2398422722 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3903021303(__this, method) ((  float (*) (KeyValuePair_2_t2398422722 *, const MethodInfo*))KeyValuePair_2_get_Value_m3903021303_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1599643432_gshared (KeyValuePair_2_t2398422722 * __this, float ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1599643432(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2398422722 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m1599643432_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4006127670_gshared (KeyValuePair_2_t2398422722 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4006127670(__this, method) ((  String_t* (*) (KeyValuePair_2_t2398422722 *, const MethodInfo*))KeyValuePair_2_ToString_m4006127670_gshared)(__this, method)
