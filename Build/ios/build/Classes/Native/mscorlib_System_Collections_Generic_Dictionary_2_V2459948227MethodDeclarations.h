﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1153271331MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1521649145(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2459948227 *, Dictionary_2_t3756888384 *, const MethodInfo*))ValueCollection__ctor_m1483315994_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m95054939(__this, ___item0, method) ((  void (*) (ValueCollection_t2459948227 *, AntibodyState_t3996126191 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1365621704_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1533524048(__this, method) ((  void (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4052963599_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2772767833(__this, ___item0, method) ((  bool (*) (ValueCollection_t2459948227 *, AntibodyState_t3996126191 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m983017908_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3575849002(__this, ___item0, method) ((  bool (*) (ValueCollection_t2459948227 *, AntibodyState_t3996126191 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1881192871_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3951084346(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3269857629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3857414104(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2459948227 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3945211245_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1819415371(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3160630156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2746854622(__this, method) ((  bool (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2994529347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1911474220(__this, method) ((  bool (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1349141089_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1439627026(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1670992289_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3432862300(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2459948227 *, AntibodyStateU5BU5D_t4128076662*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1189756071_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::GetEnumerator()
#define ValueCollection_GetEnumerator_m356162049(__this, method) ((  Enumerator_t1148453852  (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_GetEnumerator_m3068530194_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,AntibodyState>::get_Count()
#define ValueCollection_get_Count_m3919901444(__this, method) ((  int32_t (*) (ValueCollection_t2459948227 *, const MethodInfo*))ValueCollection_get_Count_m1017641181_gshared)(__this, method)
