﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell_Piece
struct  TCell_Piece_t3459712141  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color TCell_Piece::_color
	Color_t2020392075  ____color_2;

public:
	inline static int32_t get_offset_of__color_2() { return static_cast<int32_t>(offsetof(TCell_Piece_t3459712141, ____color_2)); }
	inline Color_t2020392075  get__color_2() const { return ____color_2; }
	inline Color_t2020392075 * get_address_of__color_2() { return &____color_2; }
	inline void set__color_2(Color_t2020392075  value)
	{
		____color_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
