﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3052573348.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/CoReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1185200309_gshared (Enumerator_t3052573348 * __this, Dictionary_2_t1366040584 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1185200309(__this, ___host0, method) ((  void (*) (Enumerator_t3052573348 *, Dictionary_2_t1366040584 *, const MethodInfo*))Enumerator__ctor_m1185200309_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3663052976_gshared (Enumerator_t3052573348 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3663052976(__this, method) ((  Il2CppObject * (*) (Enumerator_t3052573348 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3663052976_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/CoReceptorType,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2558712326_gshared (Enumerator_t3052573348 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2558712326(__this, method) ((  void (*) (Enumerator_t3052573348 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2558712326_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/CoReceptorType,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m2158845245_gshared (Enumerator_t3052573348 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2158845245(__this, method) ((  void (*) (Enumerator_t3052573348 *, const MethodInfo*))Enumerator_Dispose_m2158845245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/CoReceptorType,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3894226482_gshared (Enumerator_t3052573348 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3894226482(__this, method) ((  bool (*) (Enumerator_t3052573348 *, const MethodInfo*))Enumerator_MoveNext_m3894226482_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/CoReceptorType,System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m825917872_gshared (Enumerator_t3052573348 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m825917872(__this, method) ((  float (*) (Enumerator_t3052573348 *, const MethodInfo*))Enumerator_get_Current_m825917872_gshared)(__this, method)
