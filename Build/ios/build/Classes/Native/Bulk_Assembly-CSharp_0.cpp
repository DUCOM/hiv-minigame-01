﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Antibody
struct Antibody_t2931822436;
// HIV
struct HIV_t2481767745;
// AntibodyMove
struct AntibodyMove_t4173900141;
// AntibodyWander
struct AntibodyWander_t3668243203;
// System.Object
struct Il2CppObject;
// AntibodyLatch
struct AntibodyLatch_t1529651966;
// AntibodyDeath
struct AntibodyDeath_t2080738878;
// AntibodyAttack
struct AntibodyAttack_t3182813916;
// Pivot
struct Pivot_t2110476880;
// Spike
struct Spike_t60554746;
// HIV_Weak
struct HIV_Weak_t1951470088;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// SpikeInteract
struct SpikeInteract_t2340666440;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// AntibodyShooter
struct AntibodyShooter_t4166449956;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// AntibodyState
struct AntibodyState_t3996126191;
// AudioDatabase
struct AudioDatabase_t2585396589;
// Sound
struct Sound_t826716933;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// BackQuitCheck
struct BackQuitCheck_t2230274900;
// QuitButton
struct QuitButton_t1107985349;
// BCell
struct BCell_t1815094666;
// Timer
struct Timer_t2917042437;
// UnityEngine.Transform
struct Transform_t3275118058;
// BCellMaster
struct BCellMaster_t153800904;
// Capsid
struct Capsid_t4245142696;
// CellMovement
struct CellMovement_t2110243311;
// Controller
struct Controller_t1937198888;
// FocalPoint
struct FocalPoint_t1860633653;
// FocalPointMove
struct FocalPointMove_t3911050876;
// FocalPointFollow
struct FocalPointFollow_t4210230956;
// FollowCam
struct FollowCam_t1118905480;
// UnityEngine.Camera
struct Camera_t189460977;
// Game
struct Game_t1600141214;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<HIV>
struct List_1_t1850888877;
// System.Collections.Generic.List`1<TCell>
struct List_1_t1184231168;
// GameController
struct GameController_t3607102586;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// TCell
struct TCell_t1815110036;
// RedBloodCell
struct RedBloodCell_t2376799177;
// TimerList
struct TimerList_t2973412395;
// HIV_Selected
struct HIV_Selected_t1107388507;
// GameInitializer
struct GameInitializer_t3613149210;
// GP120
struct GP120_t1512820336;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// GP41
struct GP41_t921954836;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// GroupColors
struct GroupColors_t153290803;
// HighScoreDisplay
struct HighScoreDisplay_t899508680;
// UnityEngine.UI.Text
struct Text_t356221433;
// HighScoreDisplaySingle
struct HighScoreDisplaySingle_t3977972496;
// HighScores
struct HighScores_t3229912699;
// Score[]
struct ScoreU5BU5D_t100793071;
// ScoreList
struct ScoreList_t2288120898;
// System.Collections.Generic.List`1<Statistics>
struct List_1_t1906498139;
// HIVStats
struct HIVStats_t2521285894;
// HIV_Move
struct HIV_Move_t1427226471;
// HIVCollision
struct HIVCollision_t2736474671;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t13116344;
// HIV_Bud
struct HIV_Bud_t370745349;
// HIV_Infect
struct HIV_Infect_t1458631357;
// HIV_Return
struct HIV_Return_t3773441690;
// HIV_Wander
struct HIV_Wander_t1816073829;
// HIV_Death
struct HIV_Death_t750143594;
// HIV_Glow
struct HIV_Glow_t2038930725;
// HIV_Part
struct HIV_Part_t1695996321;
// HIV_Piece
struct HIV_Piece_t1005595762;
// HIV_State
struct HIV_State_t57600565;
// HIVReceptorChecker
struct HIVReceptorChecker_t623690862;
// TCell_Receptor
struct TCell_Receptor_t2424206879;
// LayerBackground
struct LayerBackground_t3535234259;
// MainCamera
struct MainCamera_t1387594674;
// MainMenuOption
struct MainMenuOption_t4210779777;
// Matrix
struct Matrix_t2094203845;
// Membrane
struct Membrane_t1623556481;
// Movement_Basic
struct Movement_Basic_t1705390416;
// ObjectMovement2D
struct ObjectMovement2D_t2992076704;
// PairList
struct PairList_t3336234724;
// TCell_Pair
struct TCell_Pair_t2779000105;
// PauseMenuButton
struct PauseMenuButton_t2369929669;
// PauseMenuQuit
struct PauseMenuQuit_t564462074;
// QuitConfirm
struct QuitConfirm_t3413857337;
// ReadOnlyAttribute
struct ReadOnlyAttribute_t3809417938;
// RenderCamera
struct RenderCamera_t2596733641;
// RNA
struct RNA_t915683991;
// Rotate
struct Rotate_t4255939431;
// RT
struct RT_t1716568150;
// SceneMove
struct SceneMove_t3418568293;
// ScoreAdder
struct ScoreAdder_t1918699446;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// ScoreDisplay
struct ScoreDisplay_t164935196;
// UnityEngine.Animator
struct Animator_t69676727;
// Settings
struct Settings_t4248570851;
// SpikeList
struct SpikeList_t1827939702;
// SpriteDisplay
struct SpriteDisplay_t1886630049;
// SwipeController
struct SwipeController_t751295678;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545MethodDeclarations.h"
#include "AssemblyU2DCSharp_Antibody2931822436.h"
#include "AssemblyU2DCSharp_Antibody2931822436MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"
#include "AssemblyU2DCSharp_HIV2481767745.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3756888384MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3756888384.h"
#include "AssemblyU2DCSharp_AntibodyState3996126191.h"
#include "AssemblyU2DCSharp_AntibodyMove4173900141.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharp_ObjectMovement2D2992076704MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_AntibodyWander3668243203.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_AntibodyLatch1529651966.h"
#include "AssemblyU2DCSharp_AntibodyDeath2080738878.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21514233606.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En781945790.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En781945790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21514233606MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "AssemblyU2DCSharp_Functions2564965619MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178MethodDeclarations.h"
#include "AssemblyU2DCSharp_AntibodyAttack3182813916.h"
#include "AssemblyU2DCSharp_AntibodyAttack3182813916MethodDeclarations.h"
#include "AssemblyU2DCSharp_AntibodyState3996126191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pivot2110476880MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV2481767745MethodDeclarations.h"
#include "AssemblyU2DCSharp_Spike60554746MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Weak1951470088MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DCSharp_Spike60554746.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_HIV_Weak1951470088.h"
#include "AssemblyU2DCSharp_Pivot2110476880.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_GP41921954836.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpikeInteract2340666440MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpikeInteract2340666440.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"
#include "AssemblyU2DCSharp_AntibodyDeath2080738878MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "AssemblyU2DCSharp_AntibodyLatch1529651966MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Move1427226471MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Move1427226471.h"
#include "AssemblyU2DCSharp_AntibodyMove4173900141MethodDeclarations.h"
#include "AssemblyU2DCSharp_AntibodyShooter4166449956.h"
#include "AssemblyU2DCSharp_AntibodyShooter4166449956MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat660383953.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat660383953MethodDeclarations.h"
#include "AssemblyU2DCSharp_AntibodyWander3668243203MethodDeclarations.h"
#include "AssemblyU2DCSharp_AudioDatabase2585396589.h"
#include "AssemblyU2DCSharp_AudioDatabase2585396589MethodDeclarations.h"
#include "AssemblyU2DCSharp_Sound826716933.h"
#include "mscorlib_System_Collections_Generic_List_1_gen195838065MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen195838065.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "AssemblyU2DCSharp_BackQuitCheck2230274900.h"
#include "AssemblyU2DCSharp_BackQuitCheck2230274900MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller1937198888MethodDeclarations.h"
#include "AssemblyU2DCSharp_QuitButton1107985349MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "AssemblyU2DCSharp_QuitButton1107985349.h"
#include "AssemblyU2DCSharp_BCell1815094666.h"
#include "AssemblyU2DCSharp_BCell1815094666MethodDeclarations.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"
#include "AssemblyU2DCSharp_Timer2917042437MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3535571088MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3535571088.h"
#include "mscorlib_System_UInt322149682021.h"
#include "AssemblyU2DCSharp_ErrorCode4164829903MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AssemblyU2DCSharp_BCellMaster153800904.h"
#include "AssemblyU2DCSharp_BCellMaster153800904MethodDeclarations.h"
#include "AssemblyU2DCSharp_Capsid4245142696.h"
#include "AssemblyU2DCSharp_Capsid4245142696MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Piece1005595762MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellMovement2110243311.h"
#include "AssemblyU2DCSharp_CellMovement2110243311MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D1256536801.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_LeftState2928884177.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "AssemblyU2DCSharp_Controller_RightState861943514.h"
#include "AssemblyU2DCSharp_RenderCamera2596733641.h"
#include "AssemblyU2DCSharp_RenderCamera2596733641MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DCSharp_Controller_LeftState2928884177MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller_RightState861943514MethodDeclarations.h"
#include "AssemblyU2DCSharp_ErrorCode4164829903.h"
#include "AssemblyU2DCSharp_ErrorCode_VariableCodes489766538.h"
#include "AssemblyU2DCSharp_ErrorCode_VariableCodes489766538MethodDeclarations.h"
#include "AssemblyU2DCSharp_FocalPoint1860633653.h"
#include "AssemblyU2DCSharp_FocalPoint1860633653MethodDeclarations.h"
#include "AssemblyU2DCSharp_FocalPointMove3911050876.h"
#include "AssemblyU2DCSharp_Game1600141214.h"
#include "AssemblyU2DCSharp_Game1600141214MethodDeclarations.h"
#include "AssemblyU2DCSharp_FocalPoint_State2306468321.h"
#include "AssemblyU2DCSharp_GameController3607102586MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_ObjectMovement2D2992076704.h"
#include "AssemblyU2DCSharp_FocalPoint_State2306468321MethodDeclarations.h"
#include "AssemblyU2DCSharp_FocalPointFollow4210230956.h"
#include "AssemblyU2DCSharp_FocalPointFollow4210230956MethodDeclarations.h"
#include "AssemblyU2DCSharp_FocalPointMove3911050876MethodDeclarations.h"
#include "AssemblyU2DCSharp_FollowCam1118905480.h"
#include "AssemblyU2DCSharp_FollowCam1118905480MethodDeclarations.h"
#include "AssemblyU2DCSharp_Functions2564965619.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2300943568MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2300943568.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1850888877.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1184231168.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1850888877MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tropism3662836552MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1184231168MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell1815110036MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell1815110036.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1745920309MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1745920309.h"
#include "AssemblyU2DCSharp_RedBloodCell2376799177MethodDeclarations.h"
#include "AssemblyU2DCSharp_RedBloodCell2376799177.h"
#include "AssemblyU2DCSharp_ScoreDisplay164935196MethodDeclarations.h"
#include "AssemblyU2DCSharp_TimerList2973412395MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_HIV_Button3180703511MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreDisplay164935196.h"
#include "AssemblyU2DCSharp_TimerList2973412395.h"
#include "AssemblyU2DCSharp_UI_HIV_Button3180703511.h"
#include "AssemblyU2DCSharp_HighScores3229912699MethodDeclarations.h"
#include "AssemblyU2DCSharp_HighScores3229912699.h"
#include "AssemblyU2DCSharp_Score1518975274.h"
#include "AssemblyU2DCSharp_Score1518975274MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Selected1107388507.h"
#include "AssemblyU2DCSharp_GameController_SwipeType864748391.h"
#include "AssemblyU2DCSharp_GameController_SwipeType864748391MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameInitializer3613149210.h"
#include "AssemblyU2DCSharp_GameInitializer3613149210MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_Settings4248570851MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameItem806762789.h"
#include "AssemblyU2DCSharp_GameItem806762789MethodDeclarations.h"
#include "AssemblyU2DCSharp_GP1201512820336.h"
#include "AssemblyU2DCSharp_GP1201512820336MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"
#include "AssemblyU2DCSharp_Statistics2537377007MethodDeclarations.h"
#include "AssemblyU2DCSharp_Statistics_Type1319459628.h"
#include "AssemblyU2DCSharp_GP41921954836MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "AssemblyU2DCSharp_GroupColors153290803.h"
#include "AssemblyU2DCSharp_GroupColors153290803MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2197938481MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3149308319MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1732668155.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2684037993.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2197938481.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1732668155MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3149308319.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2684037993MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_HighScoreDisplay899508680.h"
#include "AssemblyU2DCSharp_HighScoreDisplay899508680MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_DisplayState37128862.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "AssemblyU2DCSharp_ScoreList2288120898.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_ListingState675334696.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_DisplayState37128862MethodDeclarations.h"
#include "AssemblyU2DCSharp_HighScoreDisplay_ListingState675334696MethodDeclarations.h"
#include "AssemblyU2DCSharp_HighScoreDisplaySingle3977972496.h"
#include "AssemblyU2DCSharp_HighScoreDisplaySingle3977972496MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScoreList2288120898MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1866979105.h"
#include "mscorlib_System_IO_FileStream1695958676.h"
#include "mscorlib_System_IO_FileMode236403845.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Part1695996321MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpikeList1827939702MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Part1695996321.h"
#include "AssemblyU2DCSharp_HIV_Piece1005595762.h"
#include "AssemblyU2DCSharp_SpikeList1827939702.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1906498139.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1906498139MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIVStats2521285894.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3724643174MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3724643174.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3259372848.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3259372848MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIVStats2521285894MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2068941383MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CircleCollider2D13116344.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2068941383.h"
#include "AssemblyU2DCSharp_HIV_State57600565.h"
#include "AssemblyU2DCSharp_HIVCollision2736474671.h"
#include "AssemblyU2DCSharp_HIV_Bud370745349.h"
#include "AssemblyU2DCSharp_HIV_Infect1458631357.h"
#include "AssemblyU2DCSharp_HIV_Return3773441690.h"
#include "AssemblyU2DCSharp_HIV_Wander1816073829.h"
#include "AssemblyU2DCSharp_HIV_Death750143594.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24121253901.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3388966085.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3388966085MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24121253901MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_State4224770181MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Bud370745349MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_State57600565MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Death750143594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "AssemblyU2DCSharp_HIV_Glow2038930725.h"
#include "AssemblyU2DCSharp_HIV_Glow2038930725MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Infect1458631357MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "AssemblyU2DCSharp_HIV_Return3773441690MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_Selected1107388507MethodDeclarations.h"
#include "AssemblyU2DCSharp_ObjectMovement2D_State1000911910.h"
#include "AssemblyU2DCSharp_HIV_Wander1816073829MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIVCollision2736474671MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIVReceptorChecker623690862.h"
#include "AssemblyU2DCSharp_HIVReceptorChecker623690862MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879.h"
#include "AssemblyU2DCSharp_UI_World_Filler108322328.h"
#include "AssemblyU2DCSharp_UI_World_Filler108322328MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity1529844486MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity1529844486.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441227813.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441227813MethodDeclarations.h"
#include "AssemblyU2DCSharp_ImageRender3780187187MethodDeclarations.h"
#include "AssemblyU2DCSharp_LayerBackground3535234259.h"
#include "AssemblyU2DCSharp_LayerBackground3535234259MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_MainCamera1387594674.h"
#include "AssemblyU2DCSharp_MainCamera1387594674MethodDeclarations.h"
#include "AssemblyU2DCSharp_MainMenuOption4210779777.h"
#include "AssemblyU2DCSharp_MainMenuOption4210779777MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "AssemblyU2DCSharp_Matrix2094203845.h"
#include "AssemblyU2DCSharp_Matrix2094203845MethodDeclarations.h"
#include "AssemblyU2DCSharp_Membrane1623556481.h"
#include "AssemblyU2DCSharp_Membrane1623556481MethodDeclarations.h"
#include "AssemblyU2DCSharp_Movement_Basic1705390416.h"
#include "AssemblyU2DCSharp_Movement_Basic1705390416MethodDeclarations.h"
#include "AssemblyU2DCSharp_ObjectMovement2D_State1000911910MethodDeclarations.h"
#include "AssemblyU2DCSharp_PairList3336234724.h"
#include "AssemblyU2DCSharp_PairList3336234724MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Pair2779000105.h"
#include "AssemblyU2DCSharp_TCell_Pair2779000105MethodDeclarations.h"
#include "mscorlib_System_FieldAccessException1797813379MethodDeclarations.h"
#include "mscorlib_System_FieldAccessException1797813379.h"
#include "AssemblyU2DCSharp_PauseMenuButton2369929669.h"
#include "AssemblyU2DCSharp_PauseMenuButton2369929669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666MethodDeclarations.h"
#include "AssemblyU2DCSharp_PauseMenuQuit564462074.h"
#include "AssemblyU2DCSharp_PauseMenuQuit564462074MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "AssemblyU2DCSharp_QuitConfirm3413857337.h"
#include "AssemblyU2DCSharp_QuitConfirm3413857337MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReadOnlyAttribute3809417938.h"
#include "AssemblyU2DCSharp_ReadOnlyAttribute3809417938MethodDeclarations.h"
#include "AssemblyU2DCSharp_RNA915683991.h"
#include "AssemblyU2DCSharp_RNA915683991MethodDeclarations.h"
#include "AssemblyU2DCSharp_Rotate4255939431.h"
#include "AssemblyU2DCSharp_Rotate4255939431MethodDeclarations.h"
#include "AssemblyU2DCSharp_RT1716568150.h"
#include "AssemblyU2DCSharp_RT1716568150MethodDeclarations.h"
#include "AssemblyU2DCSharp_SceneMove3418568293.h"
#include "AssemblyU2DCSharp_SceneMove3418568293MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_ScoreAdder1918699446.h"
#include "AssemblyU2DCSharp_ScoreAdder1918699446MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "AssemblyU2DCSharp_SettingList4161291060.h"
#include "AssemblyU2DCSharp_SettingList4161291060MethodDeclarations.h"
#include "AssemblyU2DCSharp_Settings4248570851.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioListener1996719162MethodDeclarations.h"
#include "AssemblyU2DCSharp_Sound826716933MethodDeclarations.h"
#include "AssemblyU2DCSharp_Sound_SoundType2077149641.h"
#include "AssemblyU2DCSharp_Sound_SoundType2077149641MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpriteDisplay1886630049.h"
#include "AssemblyU2DCSharp_SpriteDisplay1886630049MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3973682211MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3973682211.h"
#include "AssemblyU2DCSharp_SpriteDisplay_SpriteChange4213660865.h"
#include "AssemblyU2DCSharp_SpriteDisplay_SpriteChange4213660865MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "AssemblyU2DCSharp_Statistics_Type1319459628MethodDeclarations.h"
#include "AssemblyU2DCSharp_SwipeController751295678.h"
#include "AssemblyU2DCSharp_SwipeController751295678MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<AntibodyWander>()
#define Component_GetComponent_TisAntibodyWander_t3668243203_m3452682310(__this, method) ((  AntibodyWander_t3668243203 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<AntibodyLatch>()
#define Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303(__this, method) ((  AntibodyLatch_t1529651966 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<AntibodyDeath>()
#define Component_GetComponent_TisAntibodyDeath_t2080738878_m3628454817(__this, method) ((  AntibodyDeath_t2080738878 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Pivot>()
#define Component_GetComponent_TisPivot_t2110476880_m3886145879(__this, method) ((  Pivot_t2110476880 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Spike>()
#define Component_GetComponent_TisSpike_t60554746_m755238411(__this, method) ((  Spike_t60554746 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Spike>()
#define GameObject_GetComponent_TisSpike_t60554746_m2221441807(__this, method) ((  Spike_t60554746 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Weak>()
#define Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823(__this, method) ((  HIV_Weak_t1951470088 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<SpikeInteract>()
#define GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813(__this, method) ((  SpikeInteract_t2340666440 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3829784634_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3829784634(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Antibody>()
#define GameObject_GetComponent_TisAntibody_t2931822436_m1980086405(__this, method) ((  Antibody_t2931822436 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Antibody>()
#define Component_GetComponent_TisAntibody_t2931822436_m432062649(__this, method) ((  Antibody_t2931822436 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<QuitButton>()
#define Component_GetComponent_TisQuitButton_t1107985349_m1360924300(__this, method) ((  QuitButton_t1107985349 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m681991875_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Transform_t3275118058 * p1, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m681991875(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1756533147_m3066053529(__this /* static, unused */, p0, p1, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Transform_t3275118058 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m681991875_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<AntibodyShooter>()
#define GameObject_GetComponent_TisAntibodyShooter_t4166449956_m3602233185(__this, method) ((  AntibodyShooter_t4166449956 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody2D>()
#define GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<GameController>()
#define Component_GetComponent_TisGameController_t3607102586_m72238897(__this, method) ((  GameController_t3607102586 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<HIV>()
#define GameObject_GetComponent_TisHIV_t2481767745_m121922886(__this, method) ((  HIV_t2481767745 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// Type Functions::GetLast<System.Object>(System.Collections.Generic.List`1<Type>)
extern "C"  Il2CppObject * Functions_GetLast_TisIl2CppObject_m988284327_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___list0, const MethodInfo* method);
#define Functions_GetLast_TisIl2CppObject_m988284327(__this /* static, unused */, ___list0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, List_1_t2058570427 *, const MethodInfo*))Functions_GetLast_TisIl2CppObject_m988284327_gshared)(__this /* static, unused */, ___list0, method)
// Type Functions::GetLast<UnityEngine.GameObject>(System.Collections.Generic.List`1<Type>)
#define Functions_GetLast_TisGameObject_t1756533147_m1284610273(__this /* static, unused */, ___list0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, List_1_t1125654279 *, const MethodInfo*))Functions_GetLast_TisIl2CppObject_m988284327_gshared)(__this /* static, unused */, ___list0, method)
// !!0 UnityEngine.GameObject::GetComponent<TCell>()
#define GameObject_GetComponent_TisTCell_t1815110036_m1633460177(__this, method) ((  TCell_t1815110036 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<RedBloodCell>()
#define GameObject_GetComponent_TisRedBloodCell_t2376799177_m3729981484(__this, method) ((  RedBloodCell_t2376799177 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CellMovement>()
#define GameObject_GetComponent_TisCellMovement_t2110243311_m18013212(__this, method) ((  CellMovement_t2110243311 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<TimerList>()
#define Component_GetComponent_TisTimerList_t2973412395_m1006871450(__this, method) ((  TimerList_t2973412395 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Selected>()
#define Component_GetComponent_TisHIV_Selected_t1107388507_m966267950(__this, method) ((  HIV_Selected_t1107388507 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m327292296(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.MeshRenderer>()
#define GameObject_GetComponentInChildren_TisMeshRenderer_t1268241104_m886037100(__this, method) ((  MeshRenderer_t1268241104 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<HIVStats>()
#define GameObject_GetComponent_TisHIVStats_t2521285894_m3513701493(__this, method) ((  HIVStats_t2521285894 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<HIVCollision>()
#define GameObject_GetComponentInChildren_TisHIVCollision_t2736474671_m1265056740(__this, method) ((  HIVCollision_t2736474671 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CircleCollider2D>()
#define Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(__this, method) ((  CircleCollider2D_t13116344 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Move>()
#define Component_GetComponent_TisHIV_Move_t1427226471_m1579293678(__this, method) ((  HIV_Move_t1427226471 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Bud>()
#define Component_GetComponent_TisHIV_Bud_t370745349_m2600086220(__this, method) ((  HIV_Bud_t370745349 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Infect>()
#define Component_GetComponent_TisHIV_Infect_t1458631357_m1679974338(__this, method) ((  HIV_Infect_t1458631357 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Return>()
#define Component_GetComponent_TisHIV_Return_t3773441690_m764620261(__this, method) ((  HIV_Return_t3773441690 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Wander>()
#define Component_GetComponent_TisHIV_Wander_t1816073829_m2551872058(__this, method) ((  HIV_Wander_t1816073829 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV_Death>()
#define Component_GetComponent_TisHIV_Death_t750143594_m1267458579(__this, method) ((  HIV_Death_t750143594 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<AudioDatabase>()
#define Component_GetComponent_TisAudioDatabase_t2585396589_m217869328(__this, method) ((  AudioDatabase_t2585396589 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<HIV>()
#define Component_GetComponent_TisHIV_t2481767745_m2522324452(__this, method) ((  HIV_t2481767745 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<HIV_Piece>()
#define GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225(__this, method) ((  HIV_Piece_t1005595762 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<FocalPoint>()
#define GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940(__this, method) ((  FocalPoint_t1860633653 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m2509612665(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<HIV>()
#define Component_GetComponentInParent_TisHIV_t2481767745_m535771447(__this, method) ((  HIV_t2481767745 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m2509612665_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TCell_Receptor>()
#define GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996(__this, method) ((  TCell_Receptor_t2424206879 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<AudioDatabase>()
#define GameObject_GetComponent_TisAudioDatabase_t2585396589_m3481153250(__this, method) ((  AudioDatabase_t2585396589 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<TCell_Pair>()
#define GameObject_GetComponent_TisTCell_Pair_t2779000105_m1112822816(__this, method) ((  TCell_Pair_t2779000105 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<QuitConfirm>()
#define GameObject_GetComponent_TisQuitConfirm_t3413857337_m1137943888(__this, method) ((  QuitConfirm_t3413857337 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CellMovement>()
#define Component_GetComponent_TisCellMovement_t2110243311_m1786295194(__this, method) ((  CellMovement_t2110243311 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.InputField>()
#define Component_GetComponent_TisInputField_t1631627530_m1177654614(__this, method) ((  InputField_t1631627530 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m2461586036(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
#define Component_GetComponentInChildren_TisText_t356221433_m3065860595(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<GP120>()
#define GameObject_GetComponentInChildren_TisGP120_t1512820336_m2602876863(__this, method) ((  GP120_t1512820336 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<GP41>()
#define GameObject_GetComponentInChildren_TisGP41_t921954836_m1614657349(__this, method) ((  GP41_t921954836 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<Pivot>()
#define GameObject_GetComponentInChildren_TisPivot_t2110476880_m524346231(__this, method) ((  Pivot_t2110476880 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<SpikeInteract>()
#define GameObject_GetComponentInChildren_TisSpikeInteract_t2340666440_m861253405(__this, method) ((  SpikeInteract_t2340666440 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInParent_TisIl2CppObject_m781417156(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<Spike>()
#define GameObject_GetComponentInParent_TisSpike_t60554746_m2979846140(__this, method) ((  Spike_t60554746 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInParent<HIV>()
#define GameObject_GetComponentInParent_TisHIV_t2481767745_m3590185171(__this, method) ((  HIV_t2481767745 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInParent_TisIl2CppObject_m781417156_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Controller>()
#define GameObject_GetComponent_TisController_t1937198888_m3117371903(__this, method) ((  Controller_t1937198888 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Antibody::.ctor()
extern "C"  void Antibody__ctor_m2809393157 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Antibody/State Antibody::get_state()
extern "C"  int32_t Antibody_get_state_m1127204826 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CstateU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Antibody::set_state(Antibody/State)
extern "C"  void Antibody_set_state_m289699289 (Antibody_t2931822436 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CstateU3Ek__BackingField_2(L_0);
		return;
	}
}
// HIV Antibody::get_target()
extern const MethodInfo* Dictionary_2_get_Item_m3648873249_MethodInfo_var;
extern const uint32_t Antibody_get_target_m885629897_MetadataUsageId;
extern "C"  HIV_t2481767745 * Antibody_get_target_m885629897 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Antibody_get_target_m885629897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HIV_t2481767745 * G_B3_0 = NULL;
	{
		int32_t L_0 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((HIV_t2481767745 *)(NULL));
		goto IL_0028;
	}

IL_0012:
	{
		Dictionary_2_t3756888384 * L_1 = __this->get_stateList_3();
		int32_t L_2 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		AntibodyState_t3996126191 * L_3 = Dictionary_2_get_Item_m3648873249(L_1, L_2, /*hidden argument*/Dictionary_2_get_Item_m3648873249_MethodInfo_var);
		NullCheck(L_3);
		HIV_t2481767745 * L_4 = L_3->get_target_3();
		G_B3_0 = L_4;
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// System.Void Antibody::set_target(HIV)
extern const MethodInfo* Dictionary_2_get_Item_m3648873249_MethodInfo_var;
extern const uint32_t Antibody_set_target_m1658253370_MetadataUsageId;
extern "C"  void Antibody_set_target_m1658253370 (Antibody_t2931822436 * __this, HIV_t2481767745 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Antibody_set_target_m1658253370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t3756888384 * L_1 = __this->get_stateList_3();
		int32_t L_2 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		AntibodyState_t3996126191 * L_3 = Dictionary_2_get_Item_m3648873249(L_1, L_2, /*hidden argument*/Dictionary_2_get_Item_m3648873249_MethodInfo_var);
		HIV_t2481767745 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_target_3(L_4);
	}

IL_0023:
	{
		return;
	}
}
// AntibodyMove Antibody::get_move()
extern "C"  AntibodyMove_t4173900141 * Antibody_get_move_m3645813473 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	{
		AntibodyMove_t4173900141 * L_0 = __this->get__move_5();
		return L_0;
	}
}
// System.Void Antibody::ChangeState(Antibody/State,HIV)
extern const MethodInfo* Dictionary_2_get_Item_m3648873249_MethodInfo_var;
extern const uint32_t Antibody_ChangeState_m1054492657_MetadataUsageId;
extern "C"  void Antibody_ChangeState_m1054492657 (Antibody_t2931822436 * __this, int32_t ___s0, HIV_t2481767745 * ___target1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Antibody_ChangeState_m1054492657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t3756888384 * L_1 = __this->get_stateList_3();
		int32_t L_2 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		AntibodyState_t3996126191 * L_3 = Dictionary_2_get_Item_m3648873249(L_1, L_2, /*hidden argument*/Dictionary_2_get_Item_m3648873249_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		int32_t L_4 = ___s0;
		Antibody_set_state_m289699289(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___s0;
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_006b;
		}
	}
	{
		Dictionary_2_t3756888384 * L_6 = __this->get_stateList_3();
		int32_t L_7 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		AntibodyState_t3996126191 * L_8 = Dictionary_2_get_Item_m3648873249(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m3648873249_MethodInfo_var);
		HIV_t2481767745 * L_9 = ___target1;
		NullCheck(L_8);
		L_8->set_target_3(L_9);
		int32_t L_10 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_006b;
		}
	}
	{
		Dictionary_2_t3756888384 * L_11 = __this->get_stateList_3();
		int32_t L_12 = Antibody_get_state_m1127204826(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		AntibodyState_t3996126191 * L_13 = Dictionary_2_get_Item_m3648873249(L_11, L_12, /*hidden argument*/Dictionary_2_get_Item_m3648873249_MethodInfo_var);
		NullCheck(L_13);
		Behaviour_set_enabled_m1796096907(L_13, (bool)1, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void Antibody::Awake()
extern Il2CppClass* Dictionary_2_t3756888384_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1743733219_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAntibodyWander_t3668243203_m3452682310_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1836281115_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAntibodyDeath_t2080738878_m3628454817_MethodInfo_var;
extern const uint32_t Antibody_Awake_m2182685072_MetadataUsageId;
extern "C"  void Antibody_Awake_m2182685072 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Antibody_Awake_m2182685072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3756888384 * V_0 = NULL;
	{
		AntibodyMove_t4173900141 * L_0 = Antibody_get_move_m3645813473(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_speed_4();
		NullCheck(L_0);
		ObjectMovement2D_set_maxSpeed_m780069868(L_0, L_1, /*hidden argument*/NULL);
		Dictionary_2_t3756888384 * L_2 = (Dictionary_2_t3756888384 *)il2cpp_codegen_object_new(Dictionary_2_t3756888384_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1743733219(L_2, /*hidden argument*/Dictionary_2__ctor_m1743733219_MethodInfo_var);
		V_0 = L_2;
		Dictionary_2_t3756888384 * L_3 = V_0;
		AntibodyWander_t3668243203 * L_4 = Component_GetComponent_TisAntibodyWander_t3668243203_m3452682310(__this, /*hidden argument*/Component_GetComponent_TisAntibodyWander_t3668243203_m3452682310_MethodInfo_var);
		NullCheck(L_3);
		Dictionary_2_Add_m1836281115(L_3, 0, L_4, /*hidden argument*/Dictionary_2_Add_m1836281115_MethodInfo_var);
		Dictionary_2_t3756888384 * L_5 = V_0;
		AntibodyLatch_t1529651966 * L_6 = Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303(__this, /*hidden argument*/Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303_MethodInfo_var);
		NullCheck(L_5);
		Dictionary_2_Add_m1836281115(L_5, 2, L_6, /*hidden argument*/Dictionary_2_Add_m1836281115_MethodInfo_var);
		Dictionary_2_t3756888384 * L_7 = V_0;
		AntibodyDeath_t2080738878 * L_8 = Component_GetComponent_TisAntibodyDeath_t2080738878_m3628454817(__this, /*hidden argument*/Component_GetComponent_TisAntibodyDeath_t2080738878_m3628454817_MethodInfo_var);
		NullCheck(L_7);
		Dictionary_2_Add_m1836281115(L_7, 3, L_8, /*hidden argument*/Dictionary_2_Add_m1836281115_MethodInfo_var);
		Dictionary_2_t3756888384 * L_9 = V_0;
		__this->set_stateList_3(L_9);
		return;
	}
}
// System.Void Antibody::Start()
extern const MethodInfo* Dictionary_2_GetEnumerator_m2263289423_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2376774599_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1664858804_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2669982112_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m106152050_MethodInfo_var;
extern const uint32_t Antibody_Start_m2603722309_MetadataUsageId;
extern "C"  void Antibody_Start_m2603722309 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Antibody_Start_m2603722309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t1514233606  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t781945790  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t3756888384 * L_0 = __this->get_stateList_3();
		NullCheck(L_0);
		Enumerator_t781945790  L_1 = Dictionary_2_GetEnumerator_m2263289423(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2263289423_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			KeyValuePair_2_t1514233606  L_2 = Enumerator_get_Current_m2376774599((&V_1), /*hidden argument*/Enumerator_get_Current_m2376774599_MethodInfo_var);
			V_0 = L_2;
			AntibodyState_t3996126191 * L_3 = KeyValuePair_2_get_Value_m1664858804((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1664858804_MethodInfo_var);
			NullCheck(L_3);
			Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		}

IL_0026:
		{
			bool L_4 = Enumerator_MoveNext_m2669982112((&V_1), /*hidden argument*/Enumerator_MoveNext_m2669982112_MethodInfo_var);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x45, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m106152050((&V_1), /*hidden argument*/Enumerator_Dispose_m106152050_MethodInfo_var);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0045:
	{
		Antibody_ChangeState_m1054492657(__this, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Antibody::Update()
extern "C"  void Antibody_Update_m1698463694 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (-1.0f), (-1.0f), (4.0f), (4.0f), /*hidden argument*/NULL);
		Rect_t3681755626  L_4 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		bool L_5 = Functions_OutsideRect_m364063437(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void Antibody::OnEnable()
extern "C"  void Antibody_OnEnable_m63630501 (Antibody_t2931822436 * __this, const MethodInfo* method)
{
	{
		Antibody_ChangeState_m1054492657(__this, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyAttack::.ctor()
extern "C"  void AntibodyAttack__ctor_m1499247545 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method)
{
	{
		AntibodyState__ctor_m667192040(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single AntibodyAttack::AngleToTarget()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t AntibodyAttack_AngleToTarget_m734917698_MetadataUsageId;
extern "C"  float AntibodyAttack_AngleToTarget_m734917698 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyAttack_AngleToTarget_m734917698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B5_0 = 0.0f;
	{
		HIV_t2481767745 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_003d;
		}
	}
	{
		return (0.0f);
	}

IL_003d:
	{
		HIV_t2481767745 * L_8 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_1();
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		float L_14 = (&V_2)->get_x_1();
		if ((!(((float)L_11) > ((float)L_14))))
		{
			goto IL_008c;
		}
	}
	{
		float L_15 = (&V_0)->get_y_1();
		float L_16 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_17 = asinf(((float)((float)L_15/(float)L_16)));
		G_B5_0 = ((float)((float)L_17*(float)(57.2957764f)));
		goto IL_00ad;
	}

IL_008c:
	{
		float L_18 = (&V_0)->get_y_1();
		float L_19 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_20 = asinf(((float)((float)L_18/(float)L_19)));
		G_B5_0 = ((float)((float)((float)((float)((-L_20))*(float)(57.2957764f)))+(float)(180.0f)));
	}

IL_00ad:
	{
		return G_B5_0;
	}
}
// System.Void AntibodyAttack::Latch()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPivot_t2110476880_m3886145879_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpike_t60554746_m755238411_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var;
extern const uint32_t AntibodyAttack_Latch_m3793772755_MetadataUsageId;
extern "C"  void AntibodyAttack_Latch_m3793772755 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyAttack_Latch_m3793772755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	int32_t V_1 = 0;
	HIV_Weak_t1951470088 * V_2 = NULL;
	Spike_t60554746 * G_B4_0 = NULL;
	{
		HIV_t2481767745 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00dd;
		}
	}
	{
		Transform_t3275118058 * L_3 = __this->get_destination_4();
		NullCheck(L_3);
		Pivot_t2110476880 * L_4 = Component_GetComponent_TisPivot_t2110476880_m3886145879(L_3, /*hidden argument*/Component_GetComponent_TisPivot_t2110476880_m3886145879_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		Transform_t3275118058 * L_6 = __this->get_destination_4();
		NullCheck(L_6);
		Spike_t60554746 * L_7 = Component_GetComponent_TisSpike_t60554746_m755238411(L_6, /*hidden argument*/Component_GetComponent_TisSpike_t60554746_m755238411_MethodInfo_var);
		G_B4_0 = L_7;
		goto IL_0050;
	}

IL_003b:
	{
		Transform_t3275118058 * L_8 = __this->get_destination_4();
		NullCheck(L_8);
		Pivot_t2110476880 * L_9 = Component_GetComponent_TisPivot_t2110476880_m3886145879(L_8, /*hidden argument*/Component_GetComponent_TisPivot_t2110476880_m3886145879_MethodInfo_var);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Pivot_get_parent_m3646862489(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Spike_t60554746 * L_11 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_10, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		G_B4_0 = L_11;
	}

IL_0050:
	{
		V_0 = G_B4_0;
		HIV_t2481767745 * L_12 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		Spike_t60554746 * L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = HIV_GetSpikeNumber_m1961596675(L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) < ((int32_t)0)))
		{
			goto IL_00d8;
		}
	}
	{
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Spike_t60554746 * L_17 = V_0;
		NullCheck(L_17);
		GP41_t921954836 * L_18 = Spike_get_gp41_m2571159277(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t2243707580  L_20 = Transform_get_position_m1104419803(L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_m2469242620(L_16, L_20, /*hidden argument*/NULL);
		HIV_t2481767745 * L_21 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_21);
		HIV_Weak_t1951470088 * L_22 = Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823(L_21, /*hidden argument*/Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var);
		V_2 = L_22;
		HIV_Weak_t1951470088 * L_23 = V_2;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		HIV_Weak_ChangeExcludedIndex_m1505635500(L_23, L_24, /*hidden argument*/NULL);
		HIV_Weak_t1951470088 * L_25 = V_2;
		NullCheck(L_25);
		Behaviour_set_enabled_m1796096907(L_25, (bool)1, /*hidden argument*/NULL);
		HIV_Weak_t1951470088 * L_26 = V_2;
		Antibody_t2931822436 * L_27 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_26);
		L_26->set_antibody_7(L_27);
		Antibody_t2931822436 * L_28 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		HIV_t2481767745 * L_29 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_28);
		Antibody_ChangeState_m1054492657(L_28, 2, L_29, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_31 = AntibodyAttack_AngleToTarget_m734917698(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_32 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_rotation_m3411284563(L_30, L_32, /*hidden argument*/NULL);
	}

IL_00d8:
	{
		goto IL_00ea;
	}

IL_00dd:
	{
		Antibody_t2931822436 * L_33 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_33);
		Antibody_ChangeState_m1054492657(L_33, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		return;
	}
}
// System.Void AntibodyAttack::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const uint32_t AntibodyAttack_OnEnable_m253959753_MetadataUsageId;
extern "C"  void AntibodyAttack_OnEnable_m253959753 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyAttack_OnEnable_m253959753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		HIV_t2481767745 * L_2 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		int32_t L_3 = Random_Range_m694320887(NULL /*static, unused*/, 0, 8, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1756533147 * L_4 = HIV_GetSpike_m1198366128(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Spike_t60554746 * L_5 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_4, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		NullCheck(L_5);
		Pivot_t2110476880 * L_6 = Spike_get_pivot_m660032993(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(L_6, /*hidden argument*/NULL);
		__this->set_destination_4(L_7);
		Antibody_t2931822436 * L_8 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_8);
		AntibodyMove_t4173900141 * L_9 = Antibody_get_move_m3645813473(L_8, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = __this->get_destination_4();
		NullCheck(L_9);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_9, L_10, (5.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.Void AntibodyAttack::OnDisable()
extern "C"  void AntibodyAttack_OnDisable_m2583327596 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method)
{
	{
		__this->set_destination_4((Transform_t3275118058 *)NULL);
		Antibody_t2931822436 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_0);
		AntibodyMove_t4173900141 * L_1 = Antibody_get_move_m3645813473(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_1, (Transform_t3275118058 *)NULL, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyAttack::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t AntibodyAttack_Update_m4224836906_MetadataUsageId;
extern "C"  void AntibodyAttack_Update_m4224836906 (AntibodyAttack_t3182813916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyAttack_Update_m4224836906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float G_B5_0 = 0.0f;
	{
		HIV_t2481767745 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Antibody_t2931822436 * L_3 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_3);
		Antibody_ChangeState_m1054492657(L_3, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
	}

IL_0022:
	{
		float L_4 = AntibodyAttack_AngleToTarget_m734917698(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t4030073918  L_6 = Transform_get_rotation_m1033555130(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Vector3_t2243707580  L_7 = Quaternion_get_eulerAngles_m3302573991((&V_2), /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = (&V_3)->get_z_3();
		V_1 = L_8;
		float L_9 = V_0;
		float L_10 = V_1;
		if ((!(((float)L_9) > ((float)L_10))))
		{
			goto IL_005e;
		}
	}
	{
		float L_11 = V_1;
		float L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Min_m1648492575(NULL /*static, unused*/, ((float)((float)L_11+(float)(5.0f))), L_12, /*hidden argument*/NULL);
		G_B5_0 = L_13;
		goto IL_006b;
	}

IL_005e:
	{
		float L_14 = V_1;
		float L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)L_14-(float)(5.0f))), L_15, /*hidden argument*/NULL);
		G_B5_0 = L_16;
	}

IL_006b:
	{
		V_4 = G_B5_0;
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_18 = V_4;
		Quaternion_t4030073918  L_19 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_rotation_m3411284563(L_17, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyAttack::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral788633264;
extern const uint32_t AntibodyAttack_OnTriggerExit2D_m979340769_MetadataUsageId;
extern "C"  void AntibodyAttack_OnTriggerExit2D_m979340769 (AntibodyAttack_t3182813916 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyAttack_OnTriggerExit2D_m979340769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpikeInteract_t2340666440 * V_0 = NULL;
	{
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3838334305(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a7;
		}
	}
	{
		Transform_t3275118058 * L_1 = __this->get_destination_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a7;
		}
	}
	{
		Collider2D_t646061738 * L_3 = ___c0;
		NullCheck(L_3);
		String_t* L_4 = Component_get_tag_m357168014(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral788633264, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00a7;
		}
	}
	{
		Collider2D_t646061738 * L_6 = ___c0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		SpikeInteract_t2340666440 * L_8 = GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813(L_7, /*hidden argument*/GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813_MethodInfo_var);
		V_0 = L_8;
		SpikeInteract_t2340666440 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		SpikeInteract_t2340666440 * L_11 = V_0;
		NullCheck(L_11);
		HIV_t2481767745 * L_12 = SpikeInteract_get_hiv_m2599775787(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = HIV_get_state_m3030343602(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) == ((int32_t)1)))
		{
			goto IL_0079;
		}
	}
	{
		SpikeInteract_t2340666440 * L_14 = V_0;
		NullCheck(L_14);
		HIV_t2481767745 * L_15 = SpikeInteract_get_hiv_m2599775787(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = HIV_get_state_m3030343602(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0079;
		}
	}
	{
		SpikeInteract_t2340666440 * L_17 = V_0;
		NullCheck(L_17);
		HIV_t2481767745 * L_18 = SpikeInteract_get_hiv_m2599775787(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = HIV_get_state_m3030343602(L_18, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)2))))
		{
			goto IL_00a7;
		}
	}

IL_0079:
	{
		SpikeInteract_t2340666440 * L_20 = V_0;
		NullCheck(L_20);
		HIV_t2481767745 * L_21 = SpikeInteract_get_hiv_m2599775787(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		bool L_22 = HIV_get_weak_m338777797(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00a7;
		}
	}
	{
		SpikeInteract_t2340666440 * L_23 = V_0;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(L_23, /*hidden argument*/NULL);
		__this->set_destination_4(L_24);
		SpikeInteract_t2340666440 * L_25 = V_0;
		NullCheck(L_25);
		HIV_t2481767745 * L_26 = SpikeInteract_get_hiv_m2599775787(L_25, /*hidden argument*/NULL);
		((AntibodyState_t3996126191 *)__this)->set_target_3(L_26);
		AntibodyAttack_Latch_m3793772755(__this, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		return;
	}
}
// System.Void AntibodyDeath::.ctor()
extern "C"  void AntibodyDeath__ctor_m138835577 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method)
{
	{
		AntibodyState__ctor_m667192040(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyDeath::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t AntibodyDeath_OnEnable_m234199385_MetadataUsageId;
extern "C"  void AntibodyDeath_OnEnable_m234199385 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyDeath_OnEnable_m234199385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AntibodyMove_t4173900141 * G_B3_0 = NULL;
	AntibodyMove_t4173900141 * G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	AntibodyMove_t4173900141 * G_B4_1 = NULL;
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		NullCheck(L_0);
		Rigidbody2D_set_gravityScale_m1426625078(L_0, (3.0f), /*hidden argument*/NULL);
		Antibody_t2931822436 * L_1 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_1);
		AntibodyMove_t4173900141 * L_2 = Antibody_get_move_m3645813473(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_2, (Transform_t3275118058 *)NULL, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		HIV_t2481767745 * L_3 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0096;
		}
	}
	{
		Antibody_t2931822436 * L_5 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_5);
		AntibodyMove_t4173900141 * L_6 = Antibody_get_move_m3645813473(L_5, /*hidden argument*/NULL);
		HIV_t2481767745 * L_7 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float L_10 = (&V_0)->get_x_1();
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = (&V_1)->get_x_1();
		G_B2_0 = L_6;
		if ((!(((float)L_10) < ((float)L_13))))
		{
			G_B3_0 = L_6;
			goto IL_007d;
		}
	}
	{
		G_B4_0 = (3.0f);
		G_B4_1 = G_B2_0;
		goto IL_0082;
	}

IL_007d:
	{
		G_B4_0 = (-3.0f);
		G_B4_1 = G_B3_0;
	}

IL_0082:
	{
		Vector2_t2243707579  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3067419446(&L_14, G_B4_0, (30.0f), /*hidden argument*/NULL);
		NullCheck(G_B4_1);
		ObjectMovement2D_set_velocity_m1559477545(G_B4_1, L_14, /*hidden argument*/NULL);
		goto IL_00b5;
	}

IL_0096:
	{
		Antibody_t2931822436 * L_15 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_15);
		AntibodyMove_t4173900141 * L_16 = Antibody_get_move_m3645813473(L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3067419446(&L_17, (0.0f), (30.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		ObjectMovement2D_set_velocity_m1559477545(L_16, L_17, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		return;
	}
}
// System.Void AntibodyDeath::OnDisable()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t AntibodyDeath_OnDisable_m646818162_MetadataUsageId;
extern "C"  void AntibodyDeath_OnDisable_m646818162 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyDeath_OnDisable_m646818162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Antibody_t2931822436 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_0);
		Antibody_ChangeState_m1054492657(L_0, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_1 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		NullCheck(L_1);
		Rigidbody2D_set_gravityScale_m1426625078(L_1, (0.0f), /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_rotation_m3411284563(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyDeath::Update()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t AntibodyDeath_Update_m3397076444_MetadataUsageId;
extern "C"  void AntibodyDeath_Update_m3397076444 (AntibodyDeath_t2080738878 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyDeath_Update_m3397076444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Antibody_t2931822436 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_0);
		AntibodyMove_t4173900141 * L_1 = Antibody_get_move_m3645813473(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = ObjectMovement2D_get_velocity_m987348582(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_1();
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0032;
		}
	}
	{
		Rigidbody2D_t502193897 * L_4 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		NullCheck(L_4);
		Rigidbody2D_set_gravityScale_m1426625078(L_4, (6.0f), /*hidden argument*/NULL);
	}

IL_0032:
	{
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_6 = __this->get_rotationSpeed_4();
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m2638739322(&L_8, (0.0f), (0.0f), ((float)((float)((float)((float)L_6*(float)(60.0f)))*(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_Rotate_m1743927093(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyLatch::.ctor()
extern "C"  void AntibodyLatch__ctor_m1441054387 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method)
{
	{
		AntibodyState__ctor_m667192040(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyLatch::Kill()
extern "C"  void AntibodyLatch_Kill_m3165421869 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method)
{
	{
		__this->set_death_8((bool)1);
		return;
	}
}
// System.Void AntibodyLatch::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t AntibodyLatch_OnEnable_m3550202639_MetadataUsageId;
extern "C"  void AntibodyLatch_OnEnable_m3550202639 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyLatch_OnEnable_m3550202639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Antibody_t2931822436 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_0);
		AntibodyMove_t4173900141 * L_1 = Antibody_get_move_m3645813473(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		float L_2 = ObjectMovement2D_get_maxSpeed_m1185142611(L_1, /*hidden argument*/NULL);
		__this->set_oldMax_4(L_2);
		Antibody_t2931822436 * L_3 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_3);
		AntibodyMove_t4173900141 * L_4 = Antibody_get_move_m3645813473(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_4, (Transform_t3275118058 *)NULL, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		HIV_t2481767745 * L_5 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0095;
		}
	}
	{
		Antibody_t2931822436 * L_7 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_7);
		AntibodyMove_t4173900141 * L_8 = Antibody_get_move_m3645813473(L_7, /*hidden argument*/NULL);
		HIV_t2481767745 * L_9 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_9);
		HIV_Move_t1427226471 * L_10 = HIV_get_move_m2343449320(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = HIV_Move_get_maxSpeed_m59335644(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		ObjectMovement2D_set_maxSpeed_m780069868(L_8, L_11, /*hidden argument*/NULL);
		HIV_t2481767745 * L_12 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		Vector2_t2243707579  L_18 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set_displacement_5(L_18);
		float L_19 = __this->get_maxTime_6();
		__this->set__timer_7(L_19);
	}

IL_0095:
	{
		return;
	}
}
// System.Void AntibodyLatch::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var;
extern const uint32_t AntibodyLatch_OnDisable_m1595349030_MetadataUsageId;
extern "C"  void AntibodyLatch_OnDisable_m1595349030 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyLatch_OnDisable_m1595349030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Antibody_t2931822436 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_0);
		AntibodyMove_t4173900141 * L_1 = Antibody_get_move_m3645813473(L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_oldMax_4();
		NullCheck(L_1);
		ObjectMovement2D_set_maxSpeed_m780069868(L_1, L_2, /*hidden argument*/NULL);
		HIV_t2481767745 * L_3 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		HIV_t2481767745 * L_5 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_5);
		HIV_Weak_t1951470088 * L_6 = Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823(L_5, /*hidden argument*/Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var);
		NullCheck(L_6);
		Behaviour_set_enabled_m1796096907(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0037:
	{
		__this->set_death_8((bool)0);
		return;
	}
}
// System.Void AntibodyLatch::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var;
extern const uint32_t AntibodyLatch_Update_m3356296392_MetadataUsageId;
extern "C"  void AntibodyLatch_Update_m3356296392 (AntibodyLatch_t1529651966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyLatch_Update_m3356296392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0069;
		}
	}
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		HIV_t2481767745 * L_3 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = __this->get_displacement_5();
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m2469242620(L_2, L_8, /*hidden argument*/NULL);
		float L_9 = __this->get__timer_7();
		float L_10 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__timer_7(((float)((float)L_9-(float)L_10)));
		float L_11 = __this->get__timer_7();
		if ((!(((float)L_11) < ((float)(0.0f)))))
		{
			goto IL_0064;
		}
	}
	{
		__this->set_death_8((bool)1);
	}

IL_0064:
	{
		goto IL_0076;
	}

IL_0069:
	{
		Antibody_t2931822436 * L_12 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_12);
		Antibody_ChangeState_m1054492657(L_12, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
	}

IL_0076:
	{
		HIV_t2481767745 * L_13 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_13);
		HIV_Weak_t1951470088 * L_14 = Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823(L_13, /*hidden argument*/Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var);
		NullCheck(L_14);
		int32_t L_15 = HIV_Weak_get_shakeNumber_m3852701743(L_14, /*hidden argument*/NULL);
		int32_t L_16 = __this->get_maxShakeNumber_9();
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_009c;
		}
	}
	{
		bool L_17 = __this->get_death_8();
		if (!L_17)
		{
			goto IL_00ae;
		}
	}

IL_009c:
	{
		Antibody_t2931822436 * L_18 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		HIV_t2481767745 * L_19 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_18);
		Antibody_ChangeState_m1054492657(L_18, 3, L_19, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		return;
	}
}
// System.Void AntibodyMove::.ctor()
extern "C"  void AntibodyMove__ctor_m930291536 (AntibodyMove_t4173900141 * __this, const MethodInfo* method)
{
	{
		ObjectMovement2D__ctor_m2674233803(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyShooter::.ctor()
extern "C"  void AntibodyShooter__ctor_m2289342925 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Pivot AntibodyShooter::get_pivot()
extern "C"  Pivot_t2110476880 * AntibodyShooter_get_pivot_m2515030715 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method)
{
	{
		Pivot_t2110476880 * L_0 = __this->get__pivot_4();
		return L_0;
	}
}
// System.Void AntibodyShooter::Awake()
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t AntibodyShooter_Awake_m3355326212_MetadataUsageId;
extern "C"  void AntibodyShooter_Awake_m3355326212 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyShooter_Awake_m3355326212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set__bulletList_5(L_0);
		return;
	}
}
// System.Void AntibodyShooter::OnEnable()
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t AntibodyShooter_OnEnable_m2866300021_MetadataUsageId;
extern "C"  void AntibodyShooter_OnEnable_m2866300021 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyShooter_OnEnable_m2866300021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = __this->get__bulletList_5();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t1125654279 * L_1 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_1, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set__bulletList_5(L_1);
	}

IL_0016:
	{
		return;
	}
}
// Antibody AntibodyShooter::Shoot()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAntibody_t2931822436_m1980086405_MethodInfo_var;
extern const uint32_t AntibodyShooter_Shoot_m4294917967_MetadataUsageId;
extern "C"  Antibody_t2931822436 * AntibodyShooter_Shoot_m4294917967 (AntibodyShooter_t4166449956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyShooter_Shoot_m4294917967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	Enumerator_t660383953  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		List_1_t1125654279 * L_0 = __this->get__bulletList_5();
		NullCheck(L_0);
		Enumerator_t660383953  L_1 = List_1_GetEnumerator_m970439620(L_0, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_0013:
		{
			GameObject_t1756533147 * L_2 = Enumerator_get_Current_m2389888842((&V_2), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_1 = L_2;
			GameObject_t1756533147 * L_3 = V_1;
			NullCheck(L_3);
			bool L_4 = GameObject_get_activeSelf_m313590879(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0065;
			}
		}

IL_0026:
		{
			GameObject_t1756533147 * L_5 = V_1;
			NullCheck(L_5);
			Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
			Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
			NullCheck(L_7);
			Quaternion_t4030073918  L_8 = Transform_get_rotation_m1033555130(L_7, /*hidden argument*/NULL);
			NullCheck(L_6);
			Transform_set_rotation_m3411284563(L_6, L_8, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_9 = V_1;
			NullCheck(L_9);
			Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
			Pivot_t2110476880 * L_11 = AntibodyShooter_get_pivot_m2515030715(__this, /*hidden argument*/NULL);
			NullCheck(L_11);
			Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
			NullCheck(L_12);
			Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
			NullCheck(L_10);
			Transform_set_position_m2469242620(L_10, L_13, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_14 = V_1;
			NullCheck(L_14);
			GameObject_SetActive_m2887581199(L_14, (bool)1, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_15 = V_1;
			V_0 = L_15;
			goto IL_0071;
		}

IL_0065:
		{
			bool L_16 = Enumerator_MoveNext_m3635305532((&V_2), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_16)
			{
				goto IL_0013;
			}
		}

IL_0071:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_2), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0084:
	{
		GameObject_t1756533147 * L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00c3;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get__antibody_2();
		Pivot_t2110476880 * L_20 = AntibodyShooter_get_pivot_m2515030715(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Quaternion_t4030073918  L_24 = Transform_get_rotation_m1033555130(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_25 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_19, L_22, L_24, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_0 = L_25;
		List_1_t1125654279 * L_26 = __this->get__bulletList_5();
		GameObject_t1756533147 * L_27 = V_0;
		NullCheck(L_26);
		List_1_Add_m3441471442(L_26, L_27, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
	}

IL_00c3:
	{
		GameObject_t1756533147 * L_28 = V_0;
		NullCheck(L_28);
		Rigidbody2D_t502193897 * L_29 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_28, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t2243707580  L_32 = Transform_TransformDirection_m1639585047(L_30, L_31, /*hidden argument*/NULL);
		Vector2_t2243707579  L_33 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		float L_34 = __this->get__shotSpeed_3();
		Vector2_t2243707579  L_35 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_29);
		Rigidbody2D_set_velocity_m3592751374(L_29, L_35, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = V_0;
		NullCheck(L_36);
		Antibody_t2931822436 * L_37 = GameObject_GetComponent_TisAntibody_t2931822436_m1980086405(L_36, /*hidden argument*/GameObject_GetComponent_TisAntibody_t2931822436_m1980086405_MethodInfo_var);
		return L_37;
	}
}
// System.Void AntibodyState::.ctor()
extern "C"  void AntibodyState__ctor_m667192040 (AntibodyState_t3996126191 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AntibodyState::Awake()
extern const MethodInfo* Component_GetComponent_TisAntibody_t2931822436_m432062649_MethodInfo_var;
extern const uint32_t AntibodyState_Awake_m1574285855_MetadataUsageId;
extern "C"  void AntibodyState_Awake_m1574285855 (AntibodyState_t3996126191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyState_Awake_m1574285855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Antibody_t2931822436 * L_0 = Component_GetComponent_TisAntibody_t2931822436_m432062649(__this, /*hidden argument*/Component_GetComponent_TisAntibody_t2931822436_m432062649_MethodInfo_var);
		__this->set_main_2(L_0);
		return;
	}
}
// System.Void AntibodyWander::.ctor()
extern "C"  void AntibodyWander__ctor_m2536199068 (AntibodyWander_t3668243203 * __this, const MethodInfo* method)
{
	{
		AntibodyState__ctor_m667192040(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single AntibodyWander::AngleToTarget(HIV)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t AntibodyWander_AngleToTarget_m3984465456_MetadataUsageId;
extern "C"  float AntibodyWander_AngleToTarget_m3984465456 (AntibodyWander_t3668243203 * __this, HIV_t2481767745 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyWander_AngleToTarget_m3984465456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B5_0 = 0.0f;
	{
		HIV_t2481767745 * L_0 = ___t0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_0038;
		}
	}
	{
		return (0.0f);
	}

IL_0038:
	{
		HIV_t2481767745 * L_8 = ___t0;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_1();
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		float L_14 = (&V_2)->get_x_1();
		if ((!(((float)L_11) > ((float)L_14))))
		{
			goto IL_0082;
		}
	}
	{
		float L_15 = (&V_0)->get_y_1();
		float L_16 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_17 = asinf(((float)((float)L_15/(float)L_16)));
		G_B5_0 = ((float)((float)L_17*(float)(57.2957764f)));
		goto IL_00a3;
	}

IL_0082:
	{
		float L_18 = (&V_0)->get_y_1();
		float L_19 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_20 = asinf(((float)((float)L_18/(float)L_19)));
		G_B5_0 = ((float)((float)((float)((float)((-L_20))*(float)(57.2957764f)))+(float)(180.0f)));
	}

IL_00a3:
	{
		return G_B5_0;
	}
}
// System.Void AntibodyWander::OnEnable()
extern "C"  void AntibodyWander_OnEnable_m3346171824 (AntibodyWander_t3668243203 * __this, const MethodInfo* method)
{
	{
		((AntibodyState_t3996126191 *)__this)->set_target_3((HIV_t2481767745 *)NULL);
		return;
	}
}
// System.Void AntibodyWander::OnDisable()
extern "C"  void AntibodyWander_OnDisable_m2576274259 (AntibodyWander_t3668243203 * __this, const MethodInfo* method)
{
	{
		((AntibodyState_t3996126191 *)__this)->set_target_3((HIV_t2481767745 *)NULL);
		return;
	}
}
// System.Void AntibodyWander::Latch(SpikeInteract)
extern const MethodInfo* Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var;
extern const uint32_t AntibodyWander_Latch_m2905961308_MetadataUsageId;
extern "C"  void AntibodyWander_Latch_m2905961308 (AntibodyWander_t3668243203 * __this, SpikeInteract_t2340666440 * ___destination0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyWander_Latch_m2905961308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	int32_t V_1 = 0;
	HIV_Weak_t1951470088 * V_2 = NULL;
	{
		SpikeInteract_t2340666440 * L_0 = ___destination0;
		NullCheck(L_0);
		HIV_t2481767745 * L_1 = SpikeInteract_get_hiv_m2599775787(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeSelf_m313590879(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00ae;
		}
	}
	{
		SpikeInteract_t2340666440 * L_4 = ___destination0;
		NullCheck(L_4);
		Spike_t60554746 * L_5 = SpikeInteract_get_spike_m1941632887(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		SpikeInteract_t2340666440 * L_6 = ___destination0;
		NullCheck(L_6);
		HIV_t2481767745 * L_7 = SpikeInteract_get_hiv_m2599775787(L_6, /*hidden argument*/NULL);
		Spike_t60554746 * L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = HIV_GetSpikeNumber_m1961596675(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) < ((int32_t)0)))
		{
			goto IL_00a9;
		}
	}
	{
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Spike_t60554746 * L_12 = V_0;
		NullCheck(L_12);
		GP41_t921954836 * L_13 = Spike_get_gp41_m2571159277(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_position_m2469242620(L_11, L_15, /*hidden argument*/NULL);
		SpikeInteract_t2340666440 * L_16 = ___destination0;
		NullCheck(L_16);
		HIV_t2481767745 * L_17 = SpikeInteract_get_hiv_m2599775787(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		HIV_Weak_t1951470088 * L_18 = Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823(L_17, /*hidden argument*/Component_GetComponent_TisHIV_Weak_t1951470088_m3087620823_MethodInfo_var);
		V_2 = L_18;
		HIV_Weak_t1951470088 * L_19 = V_2;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		HIV_Weak_ChangeExcludedIndex_m1505635500(L_19, L_20, /*hidden argument*/NULL);
		HIV_Weak_t1951470088 * L_21 = V_2;
		NullCheck(L_21);
		Behaviour_set_enabled_m1796096907(L_21, (bool)1, /*hidden argument*/NULL);
		HIV_Weak_t1951470088 * L_22 = V_2;
		Antibody_t2931822436 * L_23 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_22);
		L_22->set_antibody_7(L_23);
		Antibody_t2931822436 * L_24 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		SpikeInteract_t2340666440 * L_25 = ___destination0;
		NullCheck(L_25);
		HIV_t2481767745 * L_26 = SpikeInteract_get_hiv_m2599775787(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		Antibody_ChangeState_m1054492657(L_24, 2, L_26, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		SpikeInteract_t2340666440 * L_28 = ___destination0;
		NullCheck(L_28);
		HIV_t2481767745 * L_29 = SpikeInteract_get_hiv_m2599775787(L_28, /*hidden argument*/NULL);
		float L_30 = AntibodyWander_AngleToTarget_m3984465456(__this, L_29, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_31 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_30, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_rotation_m3411284563(L_27, L_31, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		goto IL_00bb;
	}

IL_00ae:
	{
		Antibody_t2931822436 * L_32 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		NullCheck(L_32);
		Antibody_ChangeState_m1054492657(L_32, 0, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		return;
	}
}
// System.Void AntibodyWander::LateUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t AntibodyWander_LateUpdate_m1162477055_MetadataUsageId;
extern "C"  void AntibodyWander_LateUpdate_m1162477055 (AntibodyWander_t3668243203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyWander_LateUpdate_m1162477055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Antibody_t2931822436 * L_2 = ((AntibodyState_t3996126191 *)__this)->get_main_2();
		HIV_t2481767745 * L_3 = ((AntibodyState_t3996126191 *)__this)->get_target_3();
		NullCheck(L_2);
		Antibody_ChangeState_m1054492657(L_2, 1, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void AntibodyWander::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral788633264;
extern const uint32_t AntibodyWander_OnTriggerEnter2D_m3157812008_MetadataUsageId;
extern "C"  void AntibodyWander_OnTriggerEnter2D_m3157812008 (AntibodyWander_t3668243203 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AntibodyWander_OnTriggerEnter2D_m3157812008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpikeInteract_t2340666440 * V_0 = NULL;
	HIV_t2481767745 * V_1 = NULL;
	{
		bool L_0 = Behaviour_get_isActiveAndEnabled_m3838334305(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0097;
		}
	}
	{
		Collider2D_t646061738 * L_1 = ___c0;
		NullCheck(L_1);
		String_t* L_2 = Component_get_tag_m357168014(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, _stringLiteral788633264, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0097;
		}
	}
	{
		Collider2D_t646061738 * L_4 = ___c0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		SpikeInteract_t2340666440 * L_6 = GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813(L_5, /*hidden argument*/GameObject_GetComponent_TisSpikeInteract_t2340666440_m523717813_MethodInfo_var);
		V_0 = L_6;
		SpikeInteract_t2340666440 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		SpikeInteract_t2340666440 * L_9 = V_0;
		NullCheck(L_9);
		HIV_t2481767745 * L_10 = SpikeInteract_get_hiv_m2599775787(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = L_10->get_grouped_4();
		if (!L_11)
		{
			goto IL_0097;
		}
	}
	{
		SpikeInteract_t2340666440 * L_12 = V_0;
		NullCheck(L_12);
		HIV_t2481767745 * L_13 = SpikeInteract_get_hiv_m2599775787(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = HIV_get_state_m3030343602(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)1)))
		{
			goto IL_0079;
		}
	}
	{
		SpikeInteract_t2340666440 * L_15 = V_0;
		NullCheck(L_15);
		HIV_t2481767745 * L_16 = SpikeInteract_get_hiv_m2599775787(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = HIV_get_state_m3030343602(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0079;
		}
	}
	{
		SpikeInteract_t2340666440 * L_18 = V_0;
		NullCheck(L_18);
		HIV_t2481767745 * L_19 = SpikeInteract_get_hiv_m2599775787(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = HIV_get_state_m3030343602(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_0097;
		}
	}

IL_0079:
	{
		SpikeInteract_t2340666440 * L_21 = V_0;
		NullCheck(L_21);
		HIV_t2481767745 * L_22 = SpikeInteract_get_hiv_m2599775787(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_23 = HIV_get_weak_m338777797(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0097;
		}
	}
	{
		SpikeInteract_t2340666440 * L_24 = V_0;
		NullCheck(L_24);
		HIV_t2481767745 * L_25 = SpikeInteract_get_hiv_m2599775787(L_24, /*hidden argument*/NULL);
		V_1 = L_25;
		SpikeInteract_t2340666440 * L_26 = V_0;
		AntibodyWander_Latch_m2905961308(__this, L_26, /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void AudioDatabase::.ctor()
extern "C"  void AudioDatabase__ctor_m2526964714 (AudioDatabase_t2585396589 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Sound AudioDatabase::get_Item(System.Int32)
extern "C"  Sound_t826716933 * AudioDatabase_get_Item_m1415045717 (AudioDatabase_t2585396589 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		Sound_t826716933 * L_1 = AudioDatabase_GetSound_m2112300346(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Sound AudioDatabase::get_Item(System.String)
extern "C"  Sound_t826716933 * AudioDatabase_get_Item_m342370300 (AudioDatabase_t2585396589 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Sound_t826716933 * L_1 = AudioDatabase_GetSound_m504701663(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Sound AudioDatabase::GetSound(System.Int32)
extern const MethodInfo* List_1_get_Count_m3314562458_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m110387289_MethodInfo_var;
extern const uint32_t AudioDatabase_GetSound_m2112300346_MetadataUsageId;
extern "C"  Sound_t826716933 * AudioDatabase_GetSound_m2112300346 (AudioDatabase_t2585396589 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_GetSound_m2112300346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t195838065 * L_2 = __this->get_soundList_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3314562458(L_2, /*hidden argument*/List_1_get_Count_m3314562458_MethodInfo_var);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (Sound_t826716933 *)NULL;
	}

IL_001a:
	{
		List_1_t195838065 * L_4 = __this->get_soundList_2();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		Sound_t826716933 * L_6 = List_1_get_Item_m110387289(L_4, L_5, /*hidden argument*/List_1_get_Item_m110387289_MethodInfo_var);
		return L_6;
	}
}
// Sound AudioDatabase::GetSound(System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2505440573_MethodInfo_var;
extern const uint32_t AudioDatabase_GetSound_m504701663_MetadataUsageId;
extern "C"  Sound_t826716933 * AudioDatabase_GetSound_m504701663 (AudioDatabase_t2585396589 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_GetSound_m504701663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3986656710 * L_0 = __this->get_indexDict_6();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m3588976330(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Dictionary_2_t3986656710 * L_3 = __this->get_indexDict_6();
		String_t* L_4 = ___name0;
		NullCheck(L_3);
		int32_t L_5 = Dictionary_2_get_Item_m2505440573(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m2505440573_MethodInfo_var);
		Sound_t826716933 * L_6 = AudioDatabase_GetSound_m2112300346(__this, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0024:
	{
		return (Sound_t826716933 *)NULL;
	}
}
// System.Void AudioDatabase::Play()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m110387289_MethodInfo_var;
extern const uint32_t AudioDatabase_Play_m1339141818_MetadataUsageId;
extern "C"  void AudioDatabase_Play_m1339141818 (AudioDatabase_t2585396589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_Play_m1339141818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t1135106623 * L_0 = __this->get_source_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005d;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = __this->get_source_5();
		List_1_t195838065 * L_3 = __this->get_soundList_2();
		int32_t L_4 = __this->get_currentIndex_4();
		NullCheck(L_3);
		Sound_t826716933 * L_5 = List_1_get_Item_m110387289(L_3, L_4, /*hidden argument*/List_1_get_Item_m110387289_MethodInfo_var);
		NullCheck(L_5);
		AudioClip_t1932558630 * L_6 = L_5->get_sound_0();
		NullCheck(L_2);
		AudioSource_set_clip_m738814682(L_2, L_6, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_7 = __this->get_source_5();
		List_1_t195838065 * L_8 = __this->get_soundList_2();
		int32_t L_9 = __this->get_currentIndex_4();
		NullCheck(L_8);
		Sound_t826716933 * L_10 = List_1_get_Item_m110387289(L_8, L_9, /*hidden argument*/List_1_get_Item_m110387289_MethodInfo_var);
		NullCheck(L_10);
		bool L_11 = L_10->get_loop_3();
		NullCheck(L_7);
		AudioSource_set_loop_m313035616(L_7, L_11, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_12 = __this->get_source_5();
		NullCheck(L_12);
		AudioSource_Play_m353744792(L_12, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void AudioDatabase::Play(System.Int32)
extern const MethodInfo* List_1_get_Count_m3314562458_MethodInfo_var;
extern const uint32_t AudioDatabase_Play_m1657006937_MetadataUsageId;
extern "C"  void AudioDatabase_Play_m1657006937 (AudioDatabase_t2585396589 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_Play_m1657006937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t195838065 * L_2 = __this->get_soundList_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3314562458(L_2, /*hidden argument*/List_1_get_Count_m3314562458_MethodInfo_var);
		if ((((int32_t)L_1) <= ((int32_t)L_3)))
		{
			goto IL_0019;
		}
	}

IL_0018:
	{
		return;
	}

IL_0019:
	{
		int32_t L_4 = ___index0;
		AudioDatabase_ChangeIndex_m3142360081(__this, L_4, /*hidden argument*/NULL);
		AudioDatabase_Play_m1339141818(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioDatabase::Play(System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m3588976330_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2505440573_MethodInfo_var;
extern const uint32_t AudioDatabase_Play_m2837110456_MetadataUsageId;
extern "C"  void AudioDatabase_Play_m2837110456 (AudioDatabase_t2585396589 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_Play_m2837110456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3986656710 * L_0 = __this->get_indexDict_6();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m3588976330(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m3588976330_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t3986656710 * L_3 = __this->get_indexDict_6();
		String_t* L_4 = ___name0;
		NullCheck(L_3);
		int32_t L_5 = Dictionary_2_get_Item_m2505440573(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m2505440573_MethodInfo_var);
		AudioDatabase_Play_m1657006937(__this, L_5, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void AudioDatabase::ChangeIndex(System.Int32)
extern const MethodInfo* List_1_get_Count_m3314562458_MethodInfo_var;
extern const uint32_t AudioDatabase_ChangeIndex_m3142360081_MetadataUsageId;
extern "C"  void AudioDatabase_ChangeIndex_m3142360081 (AudioDatabase_t2585396589 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_ChangeIndex_m3142360081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t195838065 * L_2 = __this->get_soundList_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m3314562458(L_2, /*hidden argument*/List_1_get_Count_m3314562458_MethodInfo_var);
		if ((((int32_t)L_1) <= ((int32_t)L_3)))
		{
			goto IL_0019;
		}
	}

IL_0018:
	{
		return;
	}

IL_0019:
	{
		int32_t L_4 = ___index0;
		__this->set_currentIndex_4(L_4);
		return;
	}
}
// System.Void AudioDatabase::RefreshSoundIndex()
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2063026683_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m110387289_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3314562458_MethodInfo_var;
extern const uint32_t AudioDatabase_RefreshSoundIndex_m3816134736_MetadataUsageId;
extern "C"  void AudioDatabase_RefreshSoundIndex_m3816134736 (AudioDatabase_t2585396589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_RefreshSoundIndex_m3816134736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Dictionary_2_t3986656710 * L_0 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2063026683(L_0, /*hidden argument*/Dictionary_2__ctor_m2063026683_MethodInfo_var);
		__this->set_indexDict_6(L_0);
		V_0 = 0;
		goto IL_0033;
	}

IL_0012:
	{
		Dictionary_2_t3986656710 * L_1 = __this->get_indexDict_6();
		List_1_t195838065 * L_2 = __this->get_soundList_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Sound_t826716933 * L_4 = List_1_get_Item_m110387289(L_2, L_3, /*hidden argument*/List_1_get_Item_m110387289_MethodInfo_var);
		NullCheck(L_4);
		String_t* L_5 = L_4->get_name_1();
		int32_t L_6 = V_0;
		NullCheck(L_1);
		Dictionary_2_Add_m1209957957(L_1, L_5, L_6, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		List_1_t195838065 * L_9 = __this->get_soundList_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m3314562458(L_9, /*hidden argument*/List_1_get_Count_m3314562458_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void AudioDatabase::OnValidate()
extern const MethodInfo* List_1_get_Count_m3314562458_MethodInfo_var;
extern const uint32_t AudioDatabase_OnValidate_m910391179_MetadataUsageId;
extern "C"  void AudioDatabase_OnValidate_m910391179 (AudioDatabase_t2585396589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_OnValidate_m910391179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_currentIndex_4();
		List_1_t195838065 * L_1 = __this->get_soundList_2();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m3314562458(L_1, /*hidden argument*/List_1_get_Count_m3314562458_MethodInfo_var);
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_002e;
		}
	}
	{
		List_1_t195838065 * L_3 = __this->get_soundList_2();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m3314562458(L_3, /*hidden argument*/List_1_get_Count_m3314562458_MethodInfo_var);
		__this->set_currentIndex_4(((int32_t)((int32_t)L_4-(int32_t)1)));
		goto IL_0041;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_currentIndex_4();
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		__this->set_currentIndex_4(0);
	}

IL_0041:
	{
		return;
	}
}
// System.Void AudioDatabase::Awake()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern const uint32_t AudioDatabase_Awake_m2753930889_MetadataUsageId;
extern "C"  void AudioDatabase_Awake_m2753930889 (AudioDatabase_t2585396589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioDatabase_Awake_m2753930889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3986656710 * L_0 = __this->get_indexDict_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		AudioDatabase_RefreshSoundIndex_m3816134736(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		AudioSource_t1135106623 * L_1 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		__this->set_source_5(L_1);
		bool L_2 = __this->get_playOnAwake_3();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		AudioDatabase_Play_m1339141818(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void BackQuitCheck::.ctor()
extern "C"  void BackQuitCheck__ctor_m2668904227 (BackQuitCheck_t2230274900 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BackQuitCheck::Update()
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisQuitButton_t1107985349_m1360924300_MethodInfo_var;
extern const uint32_t BackQuitCheck_Update_m1830188662_MetadataUsageId;
extern "C"  void BackQuitCheck_Update_m1830188662 (BackQuitCheck_t2230274900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackQuitCheck_Update_m1830188662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Controller_t1937198888 * L_0 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_0);
		bool L_1 = Controller_GetBack_m1477624146(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		QuitButton_t1107985349 * L_2 = Component_GetComponent_TisQuitButton_t1107985349_m1360924300(__this, /*hidden argument*/Component_GetComponent_TisQuitButton_t1107985349_m1360924300_MethodInfo_var);
		NullCheck(L_2);
		QuitButton_Quit_m720958885(L_2, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void BCell::.ctor()
extern "C"  void BCell__ctor_m2954648583 (BCell_t1815094666 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BCell::OnTimerEnd(Timer)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m534657791_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2996925095;
extern Il2CppCodeGenString* _stringLiteral2697359927;
extern const uint32_t BCell_OnTimerEnd_m1536704493_MetadataUsageId;
extern "C"  void BCell_OnTimerEnd_m1536704493 (BCell_t1815094666 * __this, Timer_t2917042437 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BCell_OnTimerEnd_m1536704493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Timer_t2917042437 * L_0 = ___t0;
		NullCheck(L_0);
		String_t* L_1 = Timer_get_name_m1851911015(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral2996925095, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral2697359927, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_006e;
	}

IL_0032:
	{
		GameObject_t1756533147 * L_7 = __this->get__master_3();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		goto IL_006e;
	}

IL_0043:
	{
		int32_t L_8 = __this->get_currentIndex_8();
		BCell_Shoot_m3208517109(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_currentIndex_8();
		List_1_t3535571088 * L_10 = __this->get__shooters_4();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m534657791(L_10, /*hidden argument*/List_1_get_Count_m534657791_MethodInfo_var);
		__this->set_currentIndex_8(((int32_t)((int32_t)((int32_t)((int32_t)L_9+(int32_t)1))%(int32_t)L_11)));
		goto IL_006e;
	}

IL_006e:
	{
		return;
	}
}
// System.Void BCell::Awake()
extern Il2CppClass* List_1_t3535571088_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3076336359_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAntibodyShooter_t4166449956_m3602233185_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1071973355_MethodInfo_var;
extern const uint32_t BCell_Awake_m3140834854_MetadataUsageId;
extern "C"  void BCell_Awake_m3140834854 (BCell_t1815094666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BCell_Awake_m3140834854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	GameObject_t1756533147 * V_3 = NULL;
	AntibodyShooter_t4166449956 * V_4 = NULL;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = __this->get__radius_6();
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, (1.0f), (1.0f), L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_2, /*hidden argument*/NULL);
		List_1_t3535571088 * L_3 = (List_1_t3535571088 *)il2cpp_codegen_object_new(List_1_t3535571088_il2cpp_TypeInfo_var);
		List_1__ctor_m3076336359(L_3, /*hidden argument*/List_1__ctor_m3076336359_MethodInfo_var);
		__this->set__shooters_4(L_3);
		uint32_t L_4 = __this->get__number_5();
		V_0 = ((float)((float)(360.0f)/(float)(((float)((float)(((double)((uint32_t)L_4))))))));
		V_1 = 0;
		goto IL_00a8;
	}

IL_0041:
	{
		float L_5 = V_0;
		int32_t L_6 = V_1;
		V_2 = ((float)((float)L_5*(float)(((float)((float)L_6)))));
		GameObject_t1756533147 * L_7 = __this->get__shooter_2();
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_10 = Object_Instantiate_TisGameObject_t1756533147_m3066053529(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3066053529_MethodInfo_var);
		V_3 = L_10;
		GameObject_t1756533147 * L_11 = V_3;
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localPosition_m1026930133(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_3;
		NullCheck(L_14);
		AntibodyShooter_t4166449956 * L_15 = GameObject_GetComponent_TisAntibodyShooter_t4166449956_m3602233185(L_14, /*hidden argument*/GameObject_GetComponent_TisAntibodyShooter_t4166449956_m3602233185_MethodInfo_var);
		V_4 = L_15;
		List_1_t3535571088 * L_16 = __this->get__shooters_4();
		AntibodyShooter_t4166449956 * L_17 = V_4;
		NullCheck(L_16);
		List_1_Add_m1071973355(L_16, L_17, /*hidden argument*/List_1_Add_m1071973355_MethodInfo_var);
		AntibodyShooter_t4166449956 * L_18 = V_4;
		NullCheck(L_18);
		Pivot_t2110476880 * L_19 = AntibodyShooter_get_pivot_m2515030715(L_18, /*hidden argument*/NULL);
		float L_20 = __this->get__radius_6();
		float L_21 = V_2;
		Quaternion_t4030073918  L_22 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		Pivot_MoveAbout_m3407021153(L_19, L_20, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00a8:
	{
		int32_t L_24 = V_1;
		uint32_t L_25 = __this->get__number_5();
		if ((((int64_t)(((int64_t)((int64_t)L_24)))) < ((int64_t)(((int64_t)((uint64_t)L_25))))))
		{
			goto IL_0041;
		}
	}
	{
		return;
	}
}
// System.Void BCell::Shoot(System.Int32)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m534657791_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m746218136_MethodInfo_var;
extern const uint32_t BCell_Shoot_m3208517109_MetadataUsageId;
extern "C"  void BCell_Shoot_m3208517109 (BCell_t1815094666 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BCell_Shoot_m3208517109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AntibodyShooter_t4166449956 * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t3535571088 * L_2 = __this->get__shooters_4();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m534657791(L_2, /*hidden argument*/List_1_get_Count_m534657791_MethodInfo_var);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		String_t* L_4 = ErrorCode_get_EA0OR_m2389741663(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1927440687 * L_5 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0023:
	{
		List_1_t3535571088 * L_6 = __this->get__shooters_4();
		int32_t L_7 = ___index0;
		NullCheck(L_6);
		AntibodyShooter_t4166449956 * L_8 = List_1_get_Item_m746218136(L_6, L_7, /*hidden argument*/List_1_get_Item_m746218136_MethodInfo_var);
		V_0 = L_8;
		AntibodyShooter_t4166449956 * L_9 = V_0;
		NullCheck(L_9);
		AntibodyShooter_Shoot_m4294917967(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BCell::Update()
extern "C"  void BCell_Update_m31384788 (BCell_t1815094666 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = __this->get__rotationSpeed_7();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m4255273365(L_0, (0.0f), (0.0f), ((float)((float)((float)((float)L_1*(float)(60.0f)))*(float)L_2)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BCellMaster::.ctor()
extern "C"  void BCellMaster__ctor_m1875056559 (BCellMaster_t153800904 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BCellMaster::OnValidate()
extern "C"  void BCellMaster_OnValidate_m1342664764 (BCellMaster_t153800904 * __this, const MethodInfo* method)
{
	{
		Pivot_t2110476880 * L_0 = __this->get_pivot_2();
		float L_1 = __this->get_radius_3();
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Quaternion_t4030073918  L_3 = Transform_get_rotation_m1033555130(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Pivot_MoveAbout_m3407021153(L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Capsid::.ctor()
extern "C"  void Capsid__ctor_m2073913303 (Capsid_t4245142696 * __this, const MethodInfo* method)
{
	{
		HIV_Piece__ctor_m2599885317(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Capsid::ChangeStats()
extern "C"  void Capsid_ChangeStats_m1349276672 (Capsid_t4245142696 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CellMovement::.ctor()
extern "C"  void CellMovement__ctor_m1969489816 (CellMovement_t2110243311 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rigidbody2D CellMovement::get_rb()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3798330222;
extern const uint32_t CellMovement_get_rb_m3453092686_MetadataUsageId;
extern "C"  Rigidbody2D_t502193897 * CellMovement_get_rb_m3453092686 (CellMovement_t2110243311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CellMovement_get_rb_m3453092686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t502193897 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_t502193897 * L_1 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		V_0 = L_1;
		Rigidbody2D_t502193897 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004d;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody2D_t502193897 * L_5 = GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396(L_4, /*hidden argument*/GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396_MethodInfo_var);
		V_0 = L_5;
		Rigidbody2D_t502193897 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		NotSupportedException_t1793819818 * L_8 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_8, _stringLiteral3798330222, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003b:
	{
		Rigidbody2D_t502193897 * L_9 = V_0;
		NullCheck(L_9);
		Rigidbody2D_set_gravityScale_m1426625078(L_9, (0.0f), /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_10 = V_0;
		NullCheck(L_10);
		Rigidbody2D_set_constraints_m1595222955(L_10, 4, /*hidden argument*/NULL);
	}

IL_004d:
	{
		Rigidbody2D_t502193897 * L_11 = V_0;
		return L_11;
	}
}
// System.Void CellMovement::disable()
extern "C"  void CellMovement_disable_m205775480 (CellMovement_t2110243311 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellMovement::SetVelocity(System.Single,System.Single)
extern "C"  void CellMovement_SetVelocity_m3993240599 (CellMovement_t2110243311 * __this, float ___speed0, float ___angle1, const MethodInfo* method)
{
	{
		float L_0 = ___speed0;
		float L_1 = ___angle1;
		Vector3_t2243707580  L_2 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		CellMovement_ChangeVelocity_m3635555946(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellMovement::ChangeVelocity(System.Single,System.Single,UnityEngine.Vector3)
extern "C"  void CellMovement_ChangeVelocity_m3635555946 (CellMovement_t2110243311 * __this, float ___speed0, float ___angle1, Vector3_t2243707580  ___startingVector2, const MethodInfo* method)
{
	{
		float L_0 = ___angle1;
		Quaternion_t4030073918  L_1 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___startingVector2;
		Vector3_t2243707580  L_3 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = ___speed0;
		CellMovement_SetVelocity_m3802536691(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellMovement::SetVelocity(UnityEngine.Vector3)
extern "C"  void CellMovement_SetVelocity_m3961975388 (CellMovement_t2110243311 * __this, Vector3_t2243707580  ___velocity0, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = CellMovement_get_rb_m3453092686(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___velocity0;
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellMovement::SetVelocity(UnityEngine.Vector3,System.Single)
extern "C"  void CellMovement_SetVelocity_m3802536691 (CellMovement_t2110243311 * __this, Vector3_t2243707580  ___direction0, float ___speed1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Vector3_get_normalized_m936072361((&___direction0), /*hidden argument*/NULL);
		float L_1 = ___speed1;
		Vector3_t2243707580  L_2 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CellMovement_SetVelocity_m3961975388(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellMovement::ShootTowards(UnityEngine.Vector3,System.Single)
extern "C"  void CellMovement_ShootTowards_m3653842377 (CellMovement_t2110243311 * __this, Vector3_t2243707580  ___position0, float ___speed1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___position0;
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		float L_4 = ___speed1;
		CellMovement_SetVelocity_m3802536691(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, (((float)((float)((int32_t)((int32_t)L_0/(int32_t)2))))), (((float)((float)((int32_t)((int32_t)L_1/(int32_t)2))))), /*hidden argument*/NULL);
		__this->set_lastLocation_5(L_2);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::Awake()
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Controller_Awake_m587736580_MetadataUsageId;
extern "C"  void Controller_Awake_m587736580 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_Awake_m587736580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Controller_t1937198888 * L_0 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->set_main_2(__this);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0026:
	{
		Controller_t1937198888 * L_3 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Controller::LeftUpdate()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Controller_LeftUpdate_m3770109509_MetadataUsageId;
extern "C"  void Controller_LeftUpdate_m3770109509 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_LeftUpdate_m3770109509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t407273883  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Touch_t407273883  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get_l_state_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0022;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
		if (L_1 == 2)
		{
			goto IL_008e;
		}
		if (L_1 == 3)
		{
			goto IL_00cd;
		}
	}
	{
		goto IL_00f0;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		__this->set_l_state_3(1);
	}

IL_0034:
	{
		goto IL_00fc;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		__this->set_l_state_3(2);
		goto IL_0089;
	}

IL_0050:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_4 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0082;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_5 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = Touch_get_phase_m196706494((&V_1), /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)3)))
		{
			goto IL_0082;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_7 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = Touch_get_phase_m196706494((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)4))))
		{
			goto IL_0089;
		}
	}

IL_0082:
	{
		__this->set_l_state_3(3);
	}

IL_0089:
	{
		goto IL_00fc;
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_9 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_10 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_10;
		int32_t L_11 = Touch_get_phase_m196706494((&V_3), /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)3)))
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_12 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_4 = L_12;
		int32_t L_13 = Touch_get_phase_m196706494((&V_4), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)4))))
		{
			goto IL_00c8;
		}
	}

IL_00c1:
	{
		__this->set_l_state_3(3);
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_14 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00e4;
		}
	}
	{
		__this->set_l_state_3(1);
		goto IL_00eb;
	}

IL_00e4:
	{
		__this->set_l_state_3(0);
	}

IL_00eb:
	{
		goto IL_00fc;
	}

IL_00f0:
	{
		__this->set_l_state_3(0);
		goto IL_00fc;
	}

IL_00fc:
	{
		return;
	}
}
// System.Void Controller::RightUpdate()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Controller_RightUpdate_m3131252236_MetadataUsageId;
extern "C"  void Controller_RightUpdate_m3131252236 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_RightUpdate_m3131252236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Touch_t407273883  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t407273883  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Touch_t407273883  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get_r_state_4();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0022;
		}
		if (L_1 == 1)
		{
			goto IL_0039;
		}
		if (L_1 == 2)
		{
			goto IL_008f;
		}
		if (L_1 == 3)
		{
			goto IL_00cf;
		}
	}
	{
		goto IL_00f2;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		__this->set_r_state_4(1);
	}

IL_0034:
	{
		goto IL_00fe;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_0050;
		}
	}
	{
		__this->set_r_state_4(2);
		goto IL_008a;
	}

IL_0050:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_4 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_5 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = Touch_get_phase_m196706494((&V_1), /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)3)))
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_7 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = Touch_get_phase_m196706494((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)4))))
		{
			goto IL_008a;
		}
	}

IL_0083:
	{
		__this->set_r_state_4(3);
	}

IL_008a:
	{
		goto IL_00fe;
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_9 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)1)))
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_10 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_3 = L_10;
		int32_t L_11 = Touch_get_phase_m196706494((&V_3), /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)3)))
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_12 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_4 = L_12;
		int32_t L_13 = Touch_get_phase_m196706494((&V_4), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)4))))
		{
			goto IL_00ca;
		}
	}

IL_00c3:
	{
		__this->set_r_state_4(3);
	}

IL_00ca:
	{
		goto IL_00fe;
	}

IL_00cf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_14 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)1)))
		{
			goto IL_00e6;
		}
	}
	{
		__this->set_r_state_4(1);
		goto IL_00ed;
	}

IL_00e6:
	{
		__this->set_r_state_4(0);
	}

IL_00ed:
	{
		goto IL_00fe;
	}

IL_00f2:
	{
		__this->set_r_state_4(0);
		goto IL_00fe;
	}

IL_00fe:
	{
		return;
	}
}
// UnityEngine.Vector3 Controller::get_worldPosition()
extern Il2CppClass* RenderCamera_t2596733641_il2cpp_TypeInfo_var;
extern const uint32_t Controller_get_worldPosition_m2261149775_MetadataUsageId;
extern "C"  Vector3_t2243707580  Controller_get_worldPosition_m2261149775 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_get_worldPosition_m2261149775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ((RenderCamera_t2596733641_StaticFields*)RenderCamera_t2596733641_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		Vector3_t2243707580  L_1 = Controller_get_screenPosition_m2179745093(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_2 = Camera_ScreenToWorldPoint_m929392728(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 Controller::get_screenPosition()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Controller_get_screenPosition_m2179745093_MetadataUsageId;
extern "C"  Vector3_t2243707580  Controller_get_screenPosition_m2179745093 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_get_screenPosition_m2179745093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_1 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2243707579  L_2 = Touch_get_position_m2079703643((&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0024;
	}

IL_001e:
	{
		Vector2_t2243707579  L_3 = __this->get_lastLocation_5();
		G_B3_0 = L_3;
	}

IL_0024:
	{
		Vector3_t2243707580  L_4 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Transform Controller::getSelected()
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Controller_getSelected_m1390842220_MetadataUsageId;
extern "C"  Transform_t3275118058 * Controller_getSelected_m1390842220 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_getSelected_m1390842220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t4063908774  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = Controller_get_worldPosition_m2261149775(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_3 = Physics2D_Raycast_m3913913442(NULL /*static, unused*/, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		V_0 = L_3;
		RaycastHit2D_t4063908774  L_4 = V_0;
		bool L_5 = RaycastHit2D_op_Implicit_m596912073(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Transform_t3275118058 * L_6 = RaycastHit2D_get_transform_m747355930((&V_0), /*hidden argument*/NULL);
		return L_6;
	}

IL_002e:
	{
		return (Transform_t3275118058 *)NULL;
	}
}
// System.Boolean Controller::LeftClick()
extern "C"  bool Controller_LeftClick_m156288658 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_l_state_3();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Controller::LeftDrag()
extern "C"  bool Controller_LeftDrag_m794894538 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_l_state_3();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean Controller::LeftRelease()
extern "C"  bool Controller_LeftRelease_m547954749 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_l_state_3();
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Boolean Controller::RightClick()
extern "C"  bool Controller_RightClick_m3621887713 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_r_state_4();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Controller::RightDrag()
extern "C"  bool Controller_RightDrag_m4108065131 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_r_state_4();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean Controller::RightRelease()
extern "C"  bool Controller_RightRelease_m1329783046 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_r_state_4();
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Boolean Controller::GetBack()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Controller_GetBack_m1477624146_MetadataUsageId;
extern "C"  bool Controller_GetBack_m1477624146 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_GetBack_m1477624146_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Controller::Update()
extern "C"  void Controller_Update_m2428618086 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		Controller_LeftUpdate_m3770109509(__this, /*hidden argument*/NULL);
		Controller_RightUpdate_m3131252236(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String ErrorCode::get_E0000()
extern Il2CppCodeGenString* _stringLiteral1104292094;
extern Il2CppCodeGenString* _stringLiteral3748426180;
extern const uint32_t ErrorCode_get_E0000_m4290471955_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_E0000_m4290471955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_E0000_m4290471955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral1104292094, _stringLiteral3748426180, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::get_EA0VA()
extern Il2CppCodeGenString* _stringLiteral3140920310;
extern Il2CppCodeGenString* _stringLiteral4283996332;
extern const uint32_t ErrorCode_get_EA0VA_m1394698289_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_EA0VA_m1394698289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_EA0VA_m1394698289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral3140920310, _stringLiteral4283996332, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::get_EA0IN()
extern Il2CppCodeGenString* _stringLiteral3014345889;
extern Il2CppCodeGenString* _stringLiteral666181401;
extern const uint32_t ErrorCode_get_EA0IN_m1408252569_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_EA0IN_m1408252569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_EA0IN_m1408252569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral3014345889, _stringLiteral666181401, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::get_EA0NN()
extern Il2CppCodeGenString* _stringLiteral3014345886;
extern Il2CppCodeGenString* _stringLiteral3485011654;
extern const uint32_t ErrorCode_get_EA0NN_m1408252274_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_EA0NN_m1408252274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_EA0NN_m1408252274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral3014345886, _stringLiteral3485011654, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::get_EA0OR()
extern Il2CppCodeGenString* _stringLiteral2643922809;
extern Il2CppCodeGenString* _stringLiteral2315477820;
extern const uint32_t ErrorCode_get_EA0OR_m2389741663_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_EA0OR_m2389741663 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_EA0OR_m2389741663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral2643922809, _stringLiteral2315477820, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::get_EC0MC()
extern Il2CppCodeGenString* _stringLiteral2684328778;
extern Il2CppCodeGenString* _stringLiteral895447154;
extern const uint32_t ErrorCode_get_EC0MC_m1677016276_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_EC0MC_m1677016276 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_EC0MC_m1677016276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral2684328778, _stringLiteral895447154, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::get_EG0HT()
extern Il2CppCodeGenString* _stringLiteral2522963286;
extern Il2CppCodeGenString* _stringLiteral2200445690;
extern const uint32_t ErrorCode_get_EG0HT_m1542756000_MetadataUsageId;
extern "C"  String_t* ErrorCode_get_EG0HT_m1542756000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_get_EG0HT_m1542756000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral2522963286, _stringLiteral2200445690, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ErrorCode::DisplayVariableCode(ErrorCode/VariableCodes,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral869676594;
extern Il2CppCodeGenString* _stringLiteral2376914199;
extern Il2CppCodeGenString* _stringLiteral3390779443;
extern Il2CppCodeGenString* _stringLiteral4188745191;
extern const uint32_t ErrorCode_DisplayVariableCode_m2670461172_MetadataUsageId;
extern "C"  String_t* ErrorCode_DisplayVariableCode_m2670461172 (Il2CppObject * __this /* static, unused */, int32_t ___variableCode0, String_t* ___variableName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_DisplayVariableCode_m2670461172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___variableCode0;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		goto IL_0021;
	}

IL_000b:
	{
		String_t* L_1 = ___variableName1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, _stringLiteral2376914199, /*hidden argument*/NULL);
		String_t* L_3 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral869676594, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0021:
	{
		String_t* L_4 = ___variableName1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_4, _stringLiteral4188745191, /*hidden argument*/NULL);
		String_t* L_6 = ErrorCode_DisplayCode_m3512282654(NULL /*static, unused*/, _stringLiteral3390779443, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String ErrorCode::DisplayCode(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2021726017;
extern Il2CppCodeGenString* _stringLiteral811305496;
extern const uint32_t ErrorCode_DisplayCode_m3512282654_MetadataUsageId;
extern "C"  String_t* ErrorCode_DisplayCode_m3512282654 (Il2CppObject * __this /* static, unused */, String_t* ___number0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ErrorCode_DisplayCode_m3512282654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___number0;
		String_t* L_1 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral2021726017, L_0, _stringLiteral811305496, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void FocalPoint::.ctor()
extern "C"  void FocalPoint__ctor_m3949637992 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// FocalPointMove FocalPoint::get_move()
extern "C"  FocalPointMove_t3911050876 * FocalPoint_get_move_m4157530869 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		FocalPointMove_t3911050876 * L_0 = __this->get__move_2();
		return L_0;
	}
}
// System.Single FocalPoint::get_speed()
extern "C"  float FocalPoint_get_speed_m745917210 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__speed_5();
		return L_0;
	}
}
// System.Single FocalPoint::get_maxDistance()
extern "C"  float FocalPoint_get_maxDistance_m2498898468 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__maxDistance_3();
		return L_0;
	}
}
// System.Single FocalPoint::get_distanceCutoff()
extern "C"  float FocalPoint_get_distanceCutoff_m3231606637 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__maxDistance_3();
		float L_1 = __this->get__additionalCutoff_4();
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Single FocalPoint::get_midpointCutoff()
extern "C"  float FocalPoint_get_midpointCutoff_m3392978060 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__maxDistance_3();
		float L_1 = FocalPoint_get_distanceCutoff_m3231606637(__this, /*hidden argument*/NULL);
		return ((float)((float)((float)((float)L_0+(float)L_1))/(float)(2.0f)));
	}
}
// UnityEngine.Vector3 FocalPoint::get_position()
extern "C"  Vector3_t2243707580  FocalPoint_get_position_m2433401882 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single FocalPoint::GetDistance(UnityEngine.Transform)
extern "C"  float FocalPoint_GetDistance_m593464702 (FocalPoint_t1860633653 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___t0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = FocalPoint_get_position_m2433401882(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		float L_5 = Vector2_Distance_m280750759(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void FocalPoint::Awake()
extern Il2CppClass* FocalPoint_t1860633653_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t FocalPoint_Awake_m2063840741_MetadataUsageId;
extern "C"  void FocalPoint_Awake_m2063840741 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FocalPoint_Awake_m2063840741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FocalPoint_t1860633653 * L_0 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->set_main_7(__this);
		goto IL_0036;
	}

IL_001b:
	{
		FocalPoint_t1860633653 * L_2 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void FocalPoint::Start()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t FocalPoint_Start_m2587931424_MetadataUsageId;
extern "C"  void FocalPoint_Start_m2587931424 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FocalPoint_Start_m2587931424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->set_state_6(1);
		FocalPointMove_t3911050876 * L_2 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		float L_3 = FocalPoint_get_speed_m745917210(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		ObjectMovement2D_set_maxSpeed_m780069868(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FocalPoint::Update()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t FocalPoint_Update_m2326944033_MetadataUsageId;
extern "C"  void FocalPoint_Update_m2326944033 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FocalPoint_Update_m2326944033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_state_6();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_0182;
		}
	}
	{
		goto IL_0187;
	}

IL_0020:
	{
		Game_t1600141214 * L_4 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_4);
		GameController_t3607102586 * L_5 = Game_get_controller_m2668402245(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00a9;
		}
	}
	{
		Game_t1600141214 * L_7 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_7);
		GameController_t3607102586 * L_8 = Game_get_controller_m2668402245(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		HIV_t2481767745 * L_9 = GameController_get_selected_m2687499813(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00a9;
		}
	}
	{
		FocalPointMove_t3911050876 * L_11 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		Game_t1600141214 * L_12 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_12);
		GameController_t3607102586 * L_13 = Game_get_controller_m2668402245(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		HIV_t2481767745 * L_14 = GameController_get_selected_m2687499813(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_11, L_15, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		FocalPointMove_t3911050876 * L_16 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		Game_t1600141214 * L_17 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_17);
		GameController_t3607102586 * L_18 = Game_get_controller_m2668402245(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		HIV_t2481767745 * L_19 = GameController_get_selected_m2687499813(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		HIV_Move_t1427226471 * L_20 = HIV_get_move_m2343449320(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		float L_21 = HIV_Move_get_maxSpeed_m59335644(L_20, /*hidden argument*/NULL);
		NullCheck(L_16);
		ObjectMovement2D_set_maxSpeed_m780069868(L_16, ((float)((float)L_21*(float)(2.0f))), /*hidden argument*/NULL);
		__this->set_state_6(2);
		goto IL_00e7;
	}

IL_00a9:
	{
		FocalPointMove_t3911050876 * L_22 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		FocalPointMove_t3911050876 * L_23 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = ObjectMovement2D_get_velocity_m987348582(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Vector2_t2243707579  L_25 = Vector2_get_normalized_m2985402409((&V_1), /*hidden argument*/NULL);
		FocalPointMove_t3911050876 * L_26 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		float L_27 = ObjectMovement2D_get_speed_m1576568683(L_26, /*hidden argument*/NULL);
		float L_28 = FocalPoint_get_speed_m745917210(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_29 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_27, L_28, (1.0f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_30 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_25, L_29, /*hidden argument*/NULL);
		NullCheck(L_22);
		ObjectMovement2D_set_velocity_m1559477545(L_22, L_30, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		goto IL_0187;
	}

IL_00ec:
	{
		Game_t1600141214 * L_31 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_31);
		GameController_t3607102586 * L_32 = Game_get_controller_m2668402245(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0119;
		}
	}
	{
		Game_t1600141214 * L_34 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_34);
		GameController_t3607102586 * L_35 = Game_get_controller_m2668402245(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		HIV_t2481767745 * L_36 = GameController_get_selected_m2687499813(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_37 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0159;
		}
	}

IL_0119:
	{
		FocalPointMove_t3911050876 * L_38 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		FocalPointMove_t3911050876 * L_39 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		float L_40 = ObjectMovement2D_get_multiplier_m3401748055(L_39, /*hidden argument*/NULL);
		FocalPointMove_t3911050876 * L_41 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		bool L_42 = ((ObjectMovement2D_t2992076704 *)L_41)->get_teleportEnabled_8();
		NullCheck(L_38);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_38, (Transform_t3275118058 *)NULL, L_40, L_42, (bool)1, /*hidden argument*/NULL);
		FocalPointMove_t3911050876 * L_43 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		float L_44 = FocalPoint_get_speed_m745917210(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		ObjectMovement2D_set_maxSpeed_m780069868(L_43, L_44, /*hidden argument*/NULL);
		__this->set_state_6(1);
		goto IL_017d;
	}

IL_0159:
	{
		FocalPointMove_t3911050876 * L_45 = FocalPoint_get_move_m4157530869(__this, /*hidden argument*/NULL);
		Game_t1600141214 * L_46 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_46);
		GameController_t3607102586 * L_47 = Game_get_controller_m2668402245(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		HIV_t2481767745 * L_48 = GameController_get_selected_m2687499813(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		HIV_Move_t1427226471 * L_49 = HIV_get_move_m2343449320(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector2_t2243707579  L_50 = ObjectMovement2D_get_velocity_m987348582(L_49, /*hidden argument*/NULL);
		NullCheck(L_45);
		ObjectMovement2D_set_velocity_m1559477545(L_45, L_50, /*hidden argument*/NULL);
	}

IL_017d:
	{
		goto IL_0187;
	}

IL_0182:
	{
		goto IL_0187;
	}

IL_0187:
	{
		return;
	}
}
// System.Void FocalPoint::OnDestroy()
extern Il2CppClass* FocalPoint_t1860633653_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t FocalPoint_OnDestroy_m1182612051_MetadataUsageId;
extern "C"  void FocalPoint_OnDestroy_m1182612051 (FocalPoint_t1860633653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FocalPoint_OnDestroy_m1182612051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FocalPoint_t1860633653 * L_0 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->set_main_7((FocalPoint_t1860633653 *)NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void FocalPointFollow::.ctor()
extern "C"  void FocalPointFollow__ctor_m2442169225 (FocalPointFollow_t4210230956 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FocalPointFollow::Update()
extern Il2CppClass* FocalPoint_t1860633653_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t FocalPointFollow_Update_m365053530_MetadataUsageId;
extern "C"  void FocalPointFollow_Update_m365053530 (FocalPointFollow_t4210230956 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FocalPointFollow_Update_m365053530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float G_B4_0 = 0.0f;
	{
		FocalPoint_t1860633653 * L_0 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0084;
		}
	}
	{
		FocalPoint_t1860633653 * L_2 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_2);
		FocalPointMove_t3911050876 * L_3 = FocalPoint_get_move_m4157530869(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector2_t2243707579  L_4 = ObjectMovement2D_get_velocity_m987348582(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_0();
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_004e;
		}
	}
	{
		Vector2_t2243707579  L_6 = Vector2_get_down_m516190382(NULL /*static, unused*/, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_7 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_7);
		FocalPointMove_t3911050876 * L_8 = FocalPoint_get_move_m4157530869(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = ObjectMovement2D_get_velocity_m987348582(L_8, /*hidden argument*/NULL);
		float L_10 = Vector2_Angle_m1064580737(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		G_B4_0 = L_10;
		goto IL_0068;
	}

IL_004e:
	{
		Vector2_t2243707579  L_11 = Vector2_get_up_m977201173(NULL /*static, unused*/, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_12 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_12);
		FocalPointMove_t3911050876 * L_13 = FocalPoint_get_move_m4157530869(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector2_t2243707579  L_14 = ObjectMovement2D_get_velocity_m987348582(L_13, /*hidden argument*/NULL);
		float L_15 = Vector2_Angle_m1064580737(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		G_B4_0 = ((-L_15));
	}

IL_0068:
	{
		V_0 = G_B4_0;
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_17 = V_0;
		Quaternion_t4030073918  L_18 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_rotation_m3411284563(L_16, L_18, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void FocalPointMove::.ctor()
extern "C"  void FocalPointMove__ctor_m1823740877 (FocalPointMove_t3911050876 * __this, const MethodInfo* method)
{
	{
		ObjectMovement2D__ctor_m2674233803(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowCam::.ctor()
extern "C"  void FollowCam__ctor_m2381606933 (FollowCam_t1118905480 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowCam::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t FollowCam_Awake_m2632659812_MetadataUsageId;
extern "C"  void FollowCam_Awake_m2632659812 (FollowCam_t1118905480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FollowCam_Awake_m2632659812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		__this->set_cam_5(L_0);
		Rigidbody2D_t502193897 * L_1 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_rb_6(L_1);
		Transform_t3275118058 * L_2 = __this->get_follow_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3275118058 * L_4 = __this->get_follow_2();
		NullCheck(L_4);
		Rigidbody2D_t502193897 * L_5 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(L_4, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0049;
		}
	}

IL_003d:
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0049:
	{
		Transform_t3275118058 * L_7 = __this->get_follow_2();
		NullCheck(L_7);
		Rigidbody2D_t502193897 * L_8 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(L_7, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set_followRigidbody_3(L_8);
	}

IL_005a:
	{
		return;
	}
}
// System.Void FollowCam::FixedUpdate()
extern "C"  void FollowCam_FixedUpdate_m3610844872 (FollowCam_t1118905480 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t2243707579  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Camera_t189460977 * L_0 = __this->get_cam_5();
		Rect_t3681755626  L_1 = __this->get_border_4();
		Rect_t3681755626  L_2 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2__ctor_m3067419446((&V_1), (0.0f), (0.0f), /*hidden argument*/NULL);
		Rigidbody2D_t502193897 * L_3 = __this->get_followRigidbody_3();
		NullCheck(L_3);
		Vector2_t2243707579  L_4 = Rigidbody2D_get_velocity_m3310151195(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_x_0();
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_005f;
		}
	}
	{
		Transform_t3275118058 * L_6 = __this->get_follow_2();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		float L_8 = (&V_3)->get_x_1();
		float L_9 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		if ((((float)L_8) > ((float)L_9)))
		{
			goto IL_009d;
		}
	}

IL_005f:
	{
		Rigidbody2D_t502193897 * L_10 = __this->get_followRigidbody_3();
		NullCheck(L_10);
		Vector2_t2243707579  L_11 = Rigidbody2D_get_velocity_m3310151195(L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = (&V_4)->get_x_0();
		if ((!(((float)L_12) < ((float)(0.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		Transform_t3275118058 * L_13 = __this->get_follow_2();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		float L_15 = (&V_5)->get_x_1();
		float L_16 = Rect_get_xMin_m1161102488((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_15) < ((float)L_16))))
		{
			goto IL_00b8;
		}
	}

IL_009d:
	{
		Rigidbody2D_t502193897 * L_17 = __this->get_followRigidbody_3();
		NullCheck(L_17);
		Vector2_t2243707579  L_18 = Rigidbody2D_get_velocity_m3310151195(L_17, /*hidden argument*/NULL);
		V_6 = L_18;
		float L_19 = (&V_6)->get_x_0();
		(&V_1)->set_x_0(L_19);
	}

IL_00b8:
	{
		Rigidbody2D_t502193897 * L_20 = __this->get_followRigidbody_3();
		NullCheck(L_20);
		Vector2_t2243707579  L_21 = Rigidbody2D_get_velocity_m3310151195(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = (&V_7)->get_y_1();
		if ((!(((float)L_22) > ((float)(0.0f)))))
		{
			goto IL_00f6;
		}
	}
	{
		Transform_t3275118058 * L_23 = __this->get_follow_2();
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_position_m1104419803(L_23, /*hidden argument*/NULL);
		V_8 = L_24;
		float L_25 = (&V_8)->get_y_2();
		float L_26 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		if ((((float)L_25) > ((float)L_26)))
		{
			goto IL_0134;
		}
	}

IL_00f6:
	{
		Rigidbody2D_t502193897 * L_27 = __this->get_followRigidbody_3();
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = Rigidbody2D_get_velocity_m3310151195(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		float L_29 = (&V_9)->get_y_1();
		if ((!(((float)L_29) < ((float)(0.0f)))))
		{
			goto IL_014f;
		}
	}
	{
		Transform_t3275118058 * L_30 = __this->get_follow_2();
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_position_m1104419803(L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		float L_32 = (&V_10)->get_y_2();
		float L_33 = Rect_get_yMin_m1161103577((&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_32) < ((float)L_33))))
		{
			goto IL_014f;
		}
	}

IL_0134:
	{
		Rigidbody2D_t502193897 * L_34 = __this->get_followRigidbody_3();
		NullCheck(L_34);
		Vector2_t2243707579  L_35 = Rigidbody2D_get_velocity_m3310151195(L_34, /*hidden argument*/NULL);
		V_11 = L_35;
		float L_36 = (&V_11)->get_y_1();
		(&V_1)->set_y_1(L_36);
	}

IL_014f:
	{
		Rigidbody2D_t502193897 * L_37 = __this->get_rb_6();
		Vector2_t2243707579  L_38 = V_1;
		NullCheck(L_37);
		Rigidbody2D_set_velocity_m3592751374(L_37, L_38, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 Functions::RandomBorder(UnityEngine.Rect)
extern "C"  Vector2_t2243707579  Functions_RandomBorder_m1218504433 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___r0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = Rect_get_yMin_m1161103577((&___r0), /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m2915146103((&___r0), /*hidden argument*/NULL);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMin_m1161102488((&___r0), /*hidden argument*/NULL);
		float L_4 = Rect_get_xMax_m2915145014((&___r0), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = V_1;
		float L_8 = V_0;
		Vector2__ctor_m3067419446((&V_3), L_7, L_8, /*hidden argument*/NULL);
		float L_9 = V_2;
		if ((!(((float)L_9) <= ((float)(0.25f)))))
		{
			goto IL_005f;
		}
	}
	{
		float L_10 = Rect_get_xMin_m1161102488((&___r0), /*hidden argument*/NULL);
		(&V_3)->set_x_0(L_10);
		goto IL_00a9;
	}

IL_005f:
	{
		float L_11 = V_2;
		if ((!(((float)L_11) <= ((float)(0.5f)))))
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = Rect_get_yMax_m2915146103((&___r0), /*hidden argument*/NULL);
		(&V_3)->set_y_1(L_12);
		goto IL_00a9;
	}

IL_007d:
	{
		float L_13 = V_2;
		if ((!(((float)L_13) <= ((float)(0.75f)))))
		{
			goto IL_009b;
		}
	}
	{
		float L_14 = Rect_get_xMax_m2915145014((&___r0), /*hidden argument*/NULL);
		(&V_3)->set_x_0(L_14);
		goto IL_00a9;
	}

IL_009b:
	{
		float L_15 = Rect_get_yMin_m1161103577((&___r0), /*hidden argument*/NULL);
		(&V_3)->set_y_1(L_15);
	}

IL_00a9:
	{
		Vector2_t2243707579  L_16 = V_3;
		return L_16;
	}
}
// UnityEngine.Rect Functions::CameraToWorldRect(UnityEngine.Camera,UnityEngine.Rect)
extern "C"  Rect_t3681755626  Functions_CameraToWorldRect_m4129867403 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___c0, Rect_t3681755626  ___border1, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Camera_t189460977 * L_0 = ___c0;
		float L_1 = Rect_get_xMax_m2915145014((&___border1), /*hidden argument*/NULL);
		float L_2 = Rect_get_yMax_m2915146103((&___border1), /*hidden argument*/NULL);
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2720820983(&L_3, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_4 = Camera_ViewportToWorldPoint_m3841010930(L_0, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Camera_t189460977 * L_6 = ___c0;
		float L_7 = Rect_get_xMin_m1161102488((&___border1), /*hidden argument*/NULL);
		float L_8 = Rect_get_yMin_m1161103577((&___border1), /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2720820983(&L_9, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_10 = Camera_ViewportToWorldPoint_m3841010930(L_6, L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_0)->get_x_0();
		float L_13 = (&V_1)->get_x_0();
		V_2 = ((float)((float)L_12-(float)L_13));
		float L_14 = (&V_0)->get_y_1();
		float L_15 = (&V_1)->get_y_1();
		V_3 = ((float)((float)L_14-(float)L_15));
		Vector2_t2243707579  L_16 = V_1;
		float L_17 = V_2;
		float L_18 = V_3;
		Vector2_t2243707579  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector2__ctor_m3067419446(&L_19, L_17, L_18, /*hidden argument*/NULL);
		Rect_t3681755626  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Rect__ctor_m2422810045(&L_20, L_16, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean Functions::OutsideRect(UnityEngine.Vector3,UnityEngine.Rect)
extern "C"  bool Functions_OutsideRect_m364063437 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___pos0, Rect_t3681755626  ___rect1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___pos0)->get_x_1();
		float L_1 = Rect_get_xMax_m2915145014((&___rect1), /*hidden argument*/NULL);
		if ((((float)L_0) > ((float)L_1)))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = (&___pos0)->get_x_1();
		float L_3 = Rect_get_xMin_m1161102488((&___rect1), /*hidden argument*/NULL);
		if ((((float)L_2) < ((float)L_3)))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = (&___pos0)->get_y_2();
		float L_5 = Rect_get_yMax_m2915146103((&___rect1), /*hidden argument*/NULL);
		if ((((float)L_4) > ((float)L_5)))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = (&___pos0)->get_y_2();
		float L_7 = Rect_get_yMin_m1161103577((&___rect1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 1;
	}

IL_004c:
	{
		return (bool)G_B5_0;
	}
}
// UnityEngine.Color Functions::GetColorPercentage(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Functions_GetColorPercentage_m2869151277_MetadataUsageId;
extern "C"  Color_t2020392075  Functions_GetColorPercentage_m2869151277 (Il2CppObject * __this /* static, unused */, float ___percent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Functions_GetColorPercentage_m2869151277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = ___percent0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		___percent0 = (0.0f);
	}

IL_0012:
	{
		float L_1 = ___percent0;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		___percent0 = (1.0f);
	}

IL_0024:
	{
		V_2 = (0.0f);
		float L_2 = ___percent0;
		if ((!(((double)(((double)((double)L_2)))) > ((double)(0.5)))))
		{
			goto IL_005d;
		}
	}
	{
		float L_3 = ___percent0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)(2.0f)-(float)((float)((float)(2.0f)*(float)L_3)))), (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = (1.0f);
		goto IL_0075;
	}

IL_005d:
	{
		V_0 = (1.0f);
		float L_5 = ___percent0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)(2.0f)*(float)L_5)), (0.0f), /*hidden argument*/NULL);
		V_1 = L_6;
	}

IL_0075:
	{
		float L_7 = V_0;
		float L_8 = V_1;
		float L_9 = V_2;
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m3811852957(&L_10, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void Game::.ctor()
extern Il2CppClass* List_1_t2300943568_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1269001131_MethodInfo_var;
extern const uint32_t Game__ctor_m1512360653_MetadataUsageId;
extern "C"  void Game__ctor_m1512360653 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game__ctor_m1512360653_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2300943568 * L_0 = (List_1_t2300943568 *)il2cpp_codegen_object_new(List_1_t2300943568_il2cpp_TypeInfo_var);
		List_1__ctor_m1269001131(L_0, /*hidden argument*/List_1__ctor_m1269001131_MethodInfo_var);
		__this->set__antibodyList_23(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::get_hivList()
extern "C"  List_1_t1125654279 * Game_get_hivList_m514738532 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		List_1_t1125654279 * L_0 = __this->get__hivList_7();
		return L_0;
	}
}
// System.Collections.Generic.List`1<HIV> Game::get_hivScripts()
extern "C"  List_1_t1850888877 * Game_get_hivScripts_m2909757549 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		List_1_t1850888877 * L_0 = __this->get__hivScripts_8();
		return L_0;
	}
}
// System.Int32 Game::get_population()
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const uint32_t Game_get_population_m752974705_MetadataUsageId;
extern "C"  int32_t Game_get_population_m752974705 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_get_population_m752974705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	Enumerator_t660383953  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1125654279 * L_0 = Game_get_hivList_m514738532(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		V_0 = 0;
		List_1_t1125654279 * L_1 = Game_get_hivList_m514738532(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Enumerator_t660383953  L_2 = List_1_GetEnumerator_m970439620(L_1, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_2 = L_2;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0037;
		}

IL_0020:
		{
			GameObject_t1756533147 * L_3 = Enumerator_get_Current_m2389888842((&V_2), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_1 = L_3;
			GameObject_t1756533147 * L_4 = V_1;
			NullCheck(L_4);
			bool L_5 = GameObject_get_activeSelf_m313590879(L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0037;
			}
		}

IL_0033:
		{
			int32_t L_6 = V_0;
			V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		}

IL_0037:
		{
			bool L_7 = Enumerator_MoveNext_m3635305532((&V_2), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_7)
			{
				goto IL_0020;
			}
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x56, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_2), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0056:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Boolean Game::get_extinct()
extern "C"  bool Game_get_extinct_m2737541949 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Game_get_population_m752974705(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_0) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Rect Game::get__tCellSpawn()
extern "C"  Rect_t3681755626  Game_get__tCellSpawn_m1045155104 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__spawnBorder_13();
		float L_1 = __this->get__spawnBorder_13();
		float L_2 = __this->get__spawnBorder_13();
		float L_3 = __this->get__spawnBorder_13();
		Rect_t3681755626  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m1220545469(&L_4, ((float)((float)(1.0f)-(float)L_0)), ((float)((float)(1.0f)-(float)L_1)), ((float)((float)((float)((float)L_2*(float)(2.0f)))-(float)(1.0f))), ((float)((float)((float)((float)L_3*(float)(2.0f)))-(float)(1.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Rect Game::get__tCellDisable()
extern "C"  Rect_t3681755626  Game_get__tCellDisable_m3709581821 (Game_t1600141214 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get__spawnBorder_13();
		V_0 = ((float)((float)L_0+(float)(0.25f)));
		float L_1 = V_0;
		float L_2 = V_0;
		float L_3 = V_0;
		float L_4 = V_0;
		Rect_t3681755626  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Rect__ctor_m1220545469(&L_5, ((float)((float)(1.0f)-(float)L_1)), ((float)((float)(1.0f)-(float)L_2)), ((float)((float)((float)((float)L_3*(float)(2.0f)))-(float)(1.0f))), ((float)((float)((float)((float)L_4*(float)(2.0f)))-(float)(1.0f))), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::get_tCellList()
extern "C"  List_1_t1125654279 * Game_get_tCellList_m3565045317 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		List_1_t1125654279 * L_0 = __this->get__tCellList_15();
		return L_0;
	}
}
// System.Collections.Generic.List`1<TCell> Game::get_tCellScripts()
extern "C"  List_1_t1184231168 * Game_get_tCellScripts_m1223424833 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		List_1_t1184231168 * L_0 = __this->get__tCellScripts_16();
		return L_0;
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::get_redBloodCellList()
extern "C"  List_1_t1125654279 * Game_get_redBloodCellList_m3132366318 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		List_1_t1125654279 * L_0 = __this->get__rbcList_18();
		return L_0;
	}
}
// GameController Game::get_controller()
extern const MethodInfo* Component_GetComponent_TisGameController_t3607102586_m72238897_MethodInfo_var;
extern const uint32_t Game_get_controller_m2668402245_MetadataUsageId;
extern "C"  GameController_t3607102586 * Game_get_controller_m2668402245 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_get_controller_m2668402245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameController_t3607102586 * L_0 = Component_GetComponent_TisGameController_t3607102586_m72238897(__this, /*hidden argument*/Component_GetComponent_TisGameController_t3607102586_m72238897_MethodInfo_var);
		return L_0;
	}
}
// System.Void Game::CreateHIVList(System.Boolean)
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1850888877_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2690578796_MethodInfo_var;
extern const uint32_t Game_CreateHIVList_m609994815_MetadataUsageId;
extern "C"  void Game_CreateHIVList_m609994815 (Game_t1600141214 * __this, bool ___instantiate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateHIVList_m609994815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set__hivList_7(L_0);
		List_1_t1850888877 * L_1 = (List_1_t1850888877 *)il2cpp_codegen_object_new(List_1_t1850888877_il2cpp_TypeInfo_var);
		List_1__ctor_m2690578796(L_1, /*hidden argument*/List_1__ctor_m2690578796_MethodInfo_var);
		__this->set__hivScripts_8(L_1);
		bool L_2 = ___instantiate0;
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Vector3_t2243707580  L_3 = __this->get_hivSpawn_5();
		Quaternion_t4030073918  L_4 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Game_CreateHIV_m2019914367(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// UnityEngine.GameObject Game::CreateHIV(UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* Functions_GetLast_TisGameObject_t1756533147_m1284610273_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3732824408_MethodInfo_var;
extern const uint32_t Game_CreateHIV_m2019914367_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Game_CreateHIV_m2019914367 (Game_t1600141214 * __this, Vector3_t2243707580  ___position0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateHIV_m2019914367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t2020392075  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t2020392075  V_4;
	memset(&V_4, 0, sizeof(V_4));
	GameObject_t1756533147 * V_5 = NULL;
	int32_t V_6 = 0;
	GameObject_t1756533147 * V_7 = NULL;
	HIV_t2481767745 * V_8 = NULL;
	HIV_t2481767745 * V_9 = NULL;
	{
		Color_t2020392075  L_0 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Color_t2020392075  L_1 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		Color_t2020392075  L_2 = Color_get_yellow_m3741935494(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_2;
		Color_t2020392075  L_3 = Color_get_black_m2650940523(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_3;
		Color_t2020392075  L_4 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_4;
		V_5 = (GameObject_t1756533147 *)NULL;
		V_6 = 0;
		goto IL_008f;
	}

IL_002a:
	{
		List_1_t1125654279 * L_5 = __this->get__hivList_7();
		int32_t L_6 = V_6;
		NullCheck(L_5);
		GameObject_t1756533147 * L_7 = List_1_get_Item_m939767277(L_5, L_6, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		V_7 = L_7;
		GameObject_t1756533147 * L_8 = V_7;
		NullCheck(L_8);
		bool L_9 = GameObject_get_activeSelf_m313590879(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0089;
		}
	}
	{
		GameObject_t1756533147 * L_10 = V_7;
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = V_7;
		V_5 = L_11;
		GameObject_t1756533147 * L_12 = V_5;
		NullCheck(L_12);
		HIV_t2481767745 * L_13 = GameObject_GetComponent_TisHIV_t2481767745_m121922886(L_12, /*hidden argument*/GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var);
		V_8 = L_13;
		HIV_t2481767745 * L_14 = V_8;
		float L_15 = __this->get_hivRadius_4();
		NullCheck(L_14);
		HIV_set_radius_m3968955256(L_14, L_15, /*hidden argument*/NULL);
		HIV_t2481767745 * L_16 = V_8;
		Color_t2020392075  L_17 = V_1;
		Color_t2020392075  L_18 = V_0;
		Color_t2020392075  L_19 = V_2;
		Color_t2020392075  L_20 = V_3;
		Color_t2020392075  L_21 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Tropism_t3662836552  L_22 = Tropism_HIVRandom_m3105810886(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		HIV_ChangeType_m3637960072(L_16, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = V_5;
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = ___position0;
		NullCheck(L_24);
		Transform_set_position_m2469242620(L_24, L_25, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = V_5;
		return L_26;
	}

IL_0089:
	{
		int32_t L_27 = V_6;
		V_6 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008f:
	{
		int32_t L_28 = V_6;
		List_1_t1125654279 * L_29 = __this->get__hivList_7();
		NullCheck(L_29);
		int32_t L_30 = List_1_get_Count_m2764296230(L_29, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_002a;
		}
	}
	{
		GameObject_t1756533147 * L_31 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_31, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0117;
		}
	}
	{
		List_1_t1125654279 * L_33 = __this->get__hivList_7();
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m2764296230(L_33, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		if ((((int32_t)L_34) <= ((int32_t)0)))
		{
			goto IL_00d7;
		}
	}
	{
		List_1_t1125654279 * L_35 = __this->get__hivList_7();
		NullCheck(L_35);
		int32_t L_36 = List_1_get_Count_m2764296230(L_35, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		int32_t L_37 = __this->get_populationMax_6();
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_00d7;
		}
	}
	{
		return (GameObject_t1756533147 *)NULL;
	}

IL_00d7:
	{
		List_1_t1125654279 * L_38 = __this->get__hivList_7();
		GameObject_t1756533147 * L_39 = __this->get_hiv_3();
		Vector3_t2243707580  L_40 = ___position0;
		Quaternion_t4030073918  L_41 = ___rotation1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_42 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		NullCheck(L_38);
		List_1_Add_m3441471442(L_38, L_42, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		List_1_t1850888877 * L_43 = __this->get__hivScripts_8();
		List_1_t1125654279 * L_44 = __this->get__hivList_7();
		GameObject_t1756533147 * L_45 = Functions_GetLast_TisGameObject_t1756533147_m1284610273(NULL /*static, unused*/, L_44, /*hidden argument*/Functions_GetLast_TisGameObject_t1756533147_m1284610273_MethodInfo_var);
		NullCheck(L_45);
		HIV_t2481767745 * L_46 = GameObject_GetComponent_TisHIV_t2481767745_m121922886(L_45, /*hidden argument*/GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var);
		NullCheck(L_43);
		List_1_Add_m3732824408(L_43, L_46, /*hidden argument*/List_1_Add_m3732824408_MethodInfo_var);
		List_1_t1125654279 * L_47 = __this->get__hivList_7();
		GameObject_t1756533147 * L_48 = Functions_GetLast_TisGameObject_t1756533147_m1284610273(NULL /*static, unused*/, L_47, /*hidden argument*/Functions_GetLast_TisGameObject_t1756533147_m1284610273_MethodInfo_var);
		V_5 = L_48;
	}

IL_0117:
	{
		GameObject_t1756533147 * L_49 = V_5;
		NullCheck(L_49);
		HIV_t2481767745 * L_50 = GameObject_GetComponent_TisHIV_t2481767745_m121922886(L_49, /*hidden argument*/GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var);
		V_9 = L_50;
		HIV_t2481767745 * L_51 = V_9;
		float L_52 = __this->get_hivRadius_4();
		NullCheck(L_51);
		HIV_Instantiate_m1173439075(L_51, L_52, /*hidden argument*/NULL);
		HIV_t2481767745 * L_53 = V_9;
		Color_t2020392075  L_54 = V_1;
		Color_t2020392075  L_55 = V_0;
		Color_t2020392075  L_56 = V_2;
		Color_t2020392075  L_57 = V_3;
		Color_t2020392075  L_58 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Tropism_t3662836552  L_59 = Tropism_HIVRandom_m3105810886(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_53);
		HIV_ChangeType_m3637960072(L_53, L_54, L_55, L_56, L_57, L_58, L_59, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_60 = V_5;
		return L_60;
	}
}
// System.Void Game::CreateTCellList(System.Boolean)
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1184231168_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1195136759_MethodInfo_var;
extern const uint32_t Game_CreateTCellList_m687649580_MetadataUsageId;
extern "C"  void Game_CreateTCellList_m687649580 (Game_t1600141214 * __this, bool ___instantiate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateTCellList_m687649580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set__tCellList_15(L_0);
		List_1_t1184231168 * L_1 = (List_1_t1184231168 *)il2cpp_codegen_object_new(List_1_t1184231168_il2cpp_TypeInfo_var);
		List_1__ctor_m1195136759(L_1, /*hidden argument*/List_1__ctor_m1195136759_MethodInfo_var);
		__this->set__tCellScripts_16(L_1);
		bool L_2 = ___instantiate0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Game_CreateTCell_m1429495806(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// UnityEngine.GameObject Game::CreateTCell()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Game_CreateTCell_m1429495806_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Game_CreateTCell_m1429495806 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateTCell_m1429495806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t4030073918  L_0 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_1 = V_0;
		GameObject_t1756533147 * L_2 = Game_CreateTCell_m1287670579(__this, L_0, (9.0f), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject Game::CreateTCell(UnityEngine.Quaternion,System.Single,UnityEngine.Vector3)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_t1815110036_m1633460177_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3124080931_MethodInfo_var;
extern const uint32_t Game_CreateTCell_m1287670579_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Game_CreateTCell_m1287670579 (Game_t1600141214 * __this, Quaternion_t4030073918  ___rotation0, float ___speed1, Vector3_t2243707580  ___pos2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateTCell_m1287670579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	GameObject_t1756533147 * V_5 = NULL;
	Enumerator_t660383953  V_6;
	memset(&V_6, 0, sizeof(V_6));
	TCell_t1815110036 * V_7 = NULL;
	GameObject_t1756533147 * V_8 = NULL;
	GameObject_t1756533147 * V_9 = NULL;
	TCell_t1815110036 * V_10 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Color__ctor_m3811852957((&V_0), (0.4f), (0.4f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = Color_get_blue_m4180825090(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		Color__ctor_m3811852957((&V_2), (0.2f), (0.7f), (0.2f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___pos2;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_4));
		Vector3_t2243707580  L_2 = V_4;
		bool L_3 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0067;
		}
	}
	{
		Camera_t189460977 * L_4 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_5 = Game_get__tCellSpawn_m1045155104(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_6 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Functions_RandomBorder_m1218504433(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		goto IL_0069;
	}

IL_0067:
	{
		Vector3_t2243707580  L_9 = ___pos2;
		V_3 = L_9;
	}

IL_0069:
	{
		List_1_t1125654279 * L_10 = __this->get__tCellList_15();
		NullCheck(L_10);
		Enumerator_t660383953  L_11 = List_1_GetEnumerator_m970439620(L_10, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_6 = L_11;
	}

IL_0076:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0118;
		}

IL_007b:
		{
			GameObject_t1756533147 * L_12 = Enumerator_get_Current_m2389888842((&V_6), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_5 = L_12;
			GameObject_t1756533147 * L_13 = V_5;
			NullCheck(L_13);
			bool L_14 = GameObject_get_activeSelf_m313590879(L_13, /*hidden argument*/NULL);
			if (L_14)
			{
				goto IL_0118;
			}
		}

IL_0090:
		{
			GameObject_t1756533147 * L_15 = V_5;
			NullCheck(L_15);
			GameObject_SetActive_m2887581199(L_15, (bool)1, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_16 = V_5;
			NullCheck(L_16);
			TCell_t1815110036 * L_17 = GameObject_GetComponent_TisTCell_t1815110036_m1633460177(L_16, /*hidden argument*/GameObject_GetComponent_TisTCell_t1815110036_m1633460177_MethodInfo_var);
			V_7 = L_17;
			TCell_t1815110036 * L_18 = V_7;
			NullCheck(L_18);
			TCell_set_infected_m1220593909(L_18, (bool)0, /*hidden argument*/NULL);
			TCell_t1815110036 * L_19 = V_7;
			float L_20 = __this->get__tCellRadiusMin_11();
			float L_21 = __this->get__tCellRadiusMax_12();
			float L_22 = Random_Range_m2884721203(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
			NullCheck(L_19);
			TCell_set_radius_m3434622193(L_19, L_22, /*hidden argument*/NULL);
			TCell_t1815110036 * L_23 = V_7;
			Color_t2020392075  L_24 = V_0;
			Color_t2020392075  L_25 = V_1;
			Color_t2020392075  L_26 = V_2;
			NullCheck(L_23);
			TCell_ChangeType_m2456386377(L_23, L_24, L_25, L_26, /*hidden argument*/NULL);
			TCell_t1815110036 * L_27 = V_7;
			NullCheck(L_27);
			Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(L_27, /*hidden argument*/NULL);
			Vector3_t2243707580  L_29 = V_3;
			NullCheck(L_28);
			Transform_set_position_m2469242620(L_28, L_29, /*hidden argument*/NULL);
			TCell_t1815110036 * L_30 = V_7;
			NullCheck(L_30);
			CellMovement_t2110243311 * L_31 = TCell_get_move_m3002539955(L_30, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_32 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_010f;
			}
		}

IL_00e9:
		{
			TCell_t1815110036 * L_33 = V_7;
			NullCheck(L_33);
			CellMovement_t2110243311 * L_34 = TCell_get_move_m3002539955(L_33, /*hidden argument*/NULL);
			Camera_t189460977 * L_35 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector3_t2243707580  L_36;
			memset(&L_36, 0, sizeof(L_36));
			Vector3__ctor_m2720820983(&L_36, (0.5f), (0.5f), /*hidden argument*/NULL);
			NullCheck(L_35);
			Vector3_t2243707580  L_37 = Camera_ViewportToWorldPoint_m3841010930(L_35, L_36, /*hidden argument*/NULL);
			float L_38 = ___speed1;
			NullCheck(L_34);
			CellMovement_ShootTowards_m3653842377(L_34, L_37, L_38, /*hidden argument*/NULL);
		}

IL_010f:
		{
			GameObject_t1756533147 * L_39 = V_5;
			V_8 = L_39;
			IL2CPP_LEAVE(0x1E6, FINALLY_0129);
		}

IL_0118:
		{
			bool L_40 = Enumerator_MoveNext_m3635305532((&V_6), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_40)
			{
				goto IL_007b;
			}
		}

IL_0124:
		{
			IL2CPP_LEAVE(0x137, FINALLY_0129);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0129;
	}

FINALLY_0129:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_6), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(297)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(297)
	{
		IL2CPP_JUMP_TBL(0x1E6, IL_01e6)
		IL2CPP_JUMP_TBL(0x137, IL_0137)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0137:
	{
		List_1_t1125654279 * L_41 = __this->get__tCellList_15();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m2764296230(L_41, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		int32_t L_43 = __this->get__tCellPopMax_14();
		if ((((int32_t)L_42) >= ((int32_t)L_43)))
		{
			goto IL_01e4;
		}
	}
	{
		GameObject_t1756533147 * L_44 = __this->get__tCell_9();
		Vector3_t2243707580  L_45 = V_3;
		Quaternion_t4030073918  L_46 = ___rotation0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_47 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_44, L_45, L_46, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_9 = L_47;
		List_1_t1125654279 * L_48 = __this->get__tCellList_15();
		GameObject_t1756533147 * L_49 = V_9;
		NullCheck(L_48);
		List_1_Add_m3441471442(L_48, L_49, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		GameObject_t1756533147 * L_50 = V_9;
		NullCheck(L_50);
		TCell_t1815110036 * L_51 = GameObject_GetComponent_TisTCell_t1815110036_m1633460177(L_50, /*hidden argument*/GameObject_GetComponent_TisTCell_t1815110036_m1633460177_MethodInfo_var);
		V_10 = L_51;
		List_1_t1184231168 * L_52 = __this->get__tCellScripts_16();
		TCell_t1815110036 * L_53 = V_10;
		NullCheck(L_52);
		List_1_Add_m3124080931(L_52, L_53, /*hidden argument*/List_1_Add_m3124080931_MethodInfo_var);
		TCell_t1815110036 * L_54 = V_10;
		float L_55 = __this->get__tCellRadiusMin_11();
		float L_56 = __this->get__tCellRadiusMax_12();
		float L_57 = Random_Range_m2884721203(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		TCell_Instantiate_m358529744(L_54, L_57, /*hidden argument*/NULL);
		TCell_t1815110036 * L_58 = V_10;
		Color_t2020392075  L_59 = V_0;
		Color_t2020392075  L_60 = V_1;
		Color_t2020392075  L_61 = V_2;
		NullCheck(L_58);
		TCell_ChangeType_m2456386377(L_58, L_59, L_60, L_61, /*hidden argument*/NULL);
		TCell_t1815110036 * L_62 = V_10;
		NullCheck(L_62);
		TCell_set_infected_m1220593909(L_62, (bool)0, /*hidden argument*/NULL);
		TCell_t1815110036 * L_63 = V_10;
		NullCheck(L_63);
		CellMovement_t2110243311 * L_64 = TCell_get_move_m3002539955(L_63, /*hidden argument*/NULL);
		bool L_65 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_64, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_01e1;
		}
	}
	{
		TCell_t1815110036 * L_66 = V_10;
		NullCheck(L_66);
		CellMovement_t2110243311 * L_67 = TCell_get_move_m3002539955(L_66, /*hidden argument*/NULL);
		Camera_t189460977 * L_68 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_69;
		memset(&L_69, 0, sizeof(L_69));
		Vector3__ctor_m2720820983(&L_69, (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_68);
		Vector3_t2243707580  L_70 = Camera_ViewportToWorldPoint_m3841010930(L_68, L_69, /*hidden argument*/NULL);
		float L_71 = ___speed1;
		NullCheck(L_67);
		CellMovement_ShootTowards_m3653842377(L_67, L_70, L_71, /*hidden argument*/NULL);
	}

IL_01e1:
	{
		GameObject_t1756533147 * L_72 = V_9;
		return L_72;
	}

IL_01e4:
	{
		return (GameObject_t1756533147 *)NULL;
	}

IL_01e6:
	{
		GameObject_t1756533147 * L_73 = V_8;
		return L_73;
	}
}
// System.Void Game::CreateRBCList(System.Boolean)
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1745920309_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m571947430_MethodInfo_var;
extern const uint32_t Game_CreateRBCList_m3543786311_MetadataUsageId;
extern "C"  void Game_CreateRBCList_m3543786311 (Game_t1600141214 * __this, bool ___instantiate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateRBCList_m3543786311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set__rbcList_18(L_0);
		List_1_t1745920309 * L_1 = (List_1_t1745920309 *)il2cpp_codegen_object_new(List_1_t1745920309_il2cpp_TypeInfo_var);
		List_1__ctor_m571947430(L_1, /*hidden argument*/List_1__ctor_m571947430_MethodInfo_var);
		__this->set__rbcScripts_19(L_1);
		bool L_2 = ___instantiate0;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Game_CreateRBC_m2781373493(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// UnityEngine.GameObject Game::CreateRBC()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Game_CreateRBC_m2781373493_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Game_CreateRBC_m2781373493 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateRBC_m2781373493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t4030073918  L_0 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_1 = V_0;
		GameObject_t1756533147 * L_2 = Game_CreateRBC_m679804250(__this, L_0, (18.0f), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject Game::CreateRBC(UnityEngine.Quaternion,System.Single,UnityEngine.Vector3)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRedBloodCell_t2376799177_m3729981484_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3932565586_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCellMovement_t2110243311_m18013212_MethodInfo_var;
extern const uint32_t Game_CreateRBC_m679804250_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Game_CreateRBC_m679804250 (Game_t1600141214 * __this, Quaternion_t4030073918  ___rotation0, float ___speed1, Vector3_t2243707580  ___pos2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_CreateRBC_m679804250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	Enumerator_t660383953  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RedBloodCell_t2376799177 * V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	RedBloodCell_t2376799177 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Vector3_t2243707580  L_0 = ___pos2;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_1));
		Vector3_t2243707580  L_1 = V_1;
		bool L_2 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		Camera_t189460977 * L_3 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_4 = Game_get__tCellSpawn_m1045155104(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_5 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Functions_RandomBorder_m1218504433(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0036;
	}

IL_0034:
	{
		Vector3_t2243707580  L_8 = ___pos2;
		V_0 = L_8;
	}

IL_0036:
	{
		List_1_t1125654279 * L_9 = __this->get__rbcList_18();
		NullCheck(L_9);
		Enumerator_t660383953  L_10 = List_1_GetEnumerator_m970439620(L_9, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_3 = L_10;
	}

IL_0042:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c8;
		}

IL_0047:
		{
			GameObject_t1756533147 * L_11 = Enumerator_get_Current_m2389888842((&V_3), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_2 = L_11;
			GameObject_t1756533147 * L_12 = V_2;
			NullCheck(L_12);
			bool L_13 = GameObject_get_activeSelf_m313590879(L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_00c8;
			}
		}

IL_005a:
		{
			GameObject_t1756533147 * L_14 = V_2;
			NullCheck(L_14);
			GameObject_SetActive_m2887581199(L_14, (bool)1, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_15 = V_2;
			NullCheck(L_15);
			RedBloodCell_t2376799177 * L_16 = GameObject_GetComponent_TisRedBloodCell_t2376799177_m3729981484(L_15, /*hidden argument*/GameObject_GetComponent_TisRedBloodCell_t2376799177_m3729981484_MethodInfo_var);
			V_4 = L_16;
			RedBloodCell_t2376799177 * L_17 = V_4;
			float L_18 = __this->get__tCellRadiusMin_11();
			float L_19 = __this->get__tCellRadiusMax_12();
			float L_20 = Random_Range_m2884721203(NULL /*static, unused*/, ((float)((float)L_18*(float)(0.5f))), ((float)((float)L_19*(float)(0.5f))), /*hidden argument*/NULL);
			NullCheck(L_17);
			RedBloodCell_set_radius_m2966666374(L_17, L_20, /*hidden argument*/NULL);
			RedBloodCell_t2376799177 * L_21 = V_4;
			NullCheck(L_21);
			Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(L_21, /*hidden argument*/NULL);
			Vector3_t2243707580  L_23 = V_0;
			NullCheck(L_22);
			Transform_set_position_m2469242620(L_22, L_23, /*hidden argument*/NULL);
			RedBloodCell_t2376799177 * L_24 = V_4;
			NullCheck(L_24);
			CellMovement_t2110243311 * L_25 = RedBloodCell_get_move_m849695286(L_24, /*hidden argument*/NULL);
			Camera_t189460977 * L_26 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector3_t2243707580  L_27;
			memset(&L_27, 0, sizeof(L_27));
			Vector3__ctor_m2720820983(&L_27, (0.5f), (0.5f), /*hidden argument*/NULL);
			NullCheck(L_26);
			Vector3_t2243707580  L_28 = Camera_ViewportToWorldPoint_m3841010930(L_26, L_27, /*hidden argument*/NULL);
			float L_29 = ___speed1;
			NullCheck(L_25);
			CellMovement_ShootTowards_m3653842377(L_25, L_28, L_29, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_30 = V_2;
			V_5 = L_30;
			IL2CPP_LEAVE(0x17E, FINALLY_00d9);
		}

IL_00c8:
		{
			bool L_31 = Enumerator_MoveNext_m3635305532((&V_3), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_31)
			{
				goto IL_0047;
			}
		}

IL_00d4:
		{
			IL2CPP_LEAVE(0xE7, FINALLY_00d9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d9;
	}

FINALLY_00d9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_3), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(217)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(217)
	{
		IL2CPP_JUMP_TBL(0x17E, IL_017e)
		IL2CPP_JUMP_TBL(0xE7, IL_00e7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e7:
	{
		List_1_t1125654279 * L_32 = __this->get__rbcList_18();
		NullCheck(L_32);
		int32_t L_33 = List_1_get_Count_m2764296230(L_32, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		int32_t L_34 = __this->get__rbcPopMax_20();
		if ((((int32_t)L_33) >= ((int32_t)L_34)))
		{
			goto IL_017c;
		}
	}
	{
		GameObject_t1756533147 * L_35 = __this->get_redBloodCell_17();
		Vector3_t2243707580  L_36 = V_0;
		Quaternion_t4030073918  L_37 = ___rotation0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_38 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_35, L_36, L_37, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_MethodInfo_var);
		V_6 = L_38;
		List_1_t1125654279 * L_39 = __this->get__rbcList_18();
		GameObject_t1756533147 * L_40 = V_6;
		NullCheck(L_39);
		List_1_Add_m3441471442(L_39, L_40, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		GameObject_t1756533147 * L_41 = V_6;
		NullCheck(L_41);
		RedBloodCell_t2376799177 * L_42 = GameObject_GetComponent_TisRedBloodCell_t2376799177_m3729981484(L_41, /*hidden argument*/GameObject_GetComponent_TisRedBloodCell_t2376799177_m3729981484_MethodInfo_var);
		V_7 = L_42;
		List_1_t1745920309 * L_43 = __this->get__rbcScripts_19();
		RedBloodCell_t2376799177 * L_44 = V_7;
		NullCheck(L_43);
		List_1_Add_m3932565586(L_43, L_44, /*hidden argument*/List_1_Add_m3932565586_MethodInfo_var);
		GameObject_t1756533147 * L_45 = V_6;
		NullCheck(L_45);
		CellMovement_t2110243311 * L_46 = GameObject_GetComponent_TisCellMovement_t2110243311_m18013212(L_45, /*hidden argument*/GameObject_GetComponent_TisCellMovement_t2110243311_m18013212_MethodInfo_var);
		Camera_t189460977 * L_47 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_48;
		memset(&L_48, 0, sizeof(L_48));
		Vector3__ctor_m2720820983(&L_48, (0.5f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t2243707580  L_49 = Camera_ViewportToWorldPoint_m3841010930(L_47, L_48, /*hidden argument*/NULL);
		float L_50 = ___speed1;
		NullCheck(L_46);
		CellMovement_ShootTowards_m3653842377(L_46, L_49, L_50, /*hidden argument*/NULL);
		RedBloodCell_t2376799177 * L_51 = V_7;
		float L_52 = __this->get__tCellRadiusMin_11();
		float L_53 = __this->get__tCellRadiusMax_12();
		float L_54 = Random_Range_m2884721203(NULL /*static, unused*/, ((float)((float)L_52*(float)(0.5f))), ((float)((float)L_53*(float)(0.5f))), /*hidden argument*/NULL);
		NullCheck(L_51);
		RedBloodCell_set_radius_m2966666374(L_51, L_54, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_55 = V_6;
		return L_55;
	}

IL_017c:
	{
		return (GameObject_t1756533147 *)NULL;
	}

IL_017e:
	{
		GameObject_t1756533147 * L_56 = V_5;
		return L_56;
	}
}
// System.Void Game::LevelUp()
extern Il2CppClass* ScoreDisplay_t164935196_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UI_HIV_Button_t3180703511_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2569958544;
extern Il2CppCodeGenString* _stringLiteral2996925095;
extern Il2CppCodeGenString* _stringLiteral2697359927;
extern Il2CppCodeGenString* _stringLiteral2697392471;
extern Il2CppCodeGenString* _stringLiteral3749811720;
extern const uint32_t Game_LevelUp_m1101585534_MetadataUsageId;
extern "C"  void Game_LevelUp_m1101585534 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_LevelUp_m1101585534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_level_25();
		__this->set_level_25(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_score_24();
		int32_t L_2 = __this->get_level_25();
		int32_t L_3 = Game_LevelGain_m1081789369(NULL /*static, unused*/, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		__this->set_score_24(((int32_t)((int32_t)L_1+(int32_t)L_3)));
		ScoreDisplay_t164935196 * L_4 = ((ScoreDisplay_t164935196_StaticFields*)ScoreDisplay_t164935196_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		Camera_t189460977 * L_5 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_level_25();
		int32_t L_10 = Game_LevelGain_m1081789369(NULL /*static, unused*/, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ScoreDisplay_ShowScore_m293627306(L_4, L_8, L_10, (2.0f), _stringLiteral2569958544, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = __this->get_warning_28();
		NullCheck(L_11);
		GameObject_SetActive_m2887581199(L_11, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_bCellObject_26();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)0, /*hidden argument*/NULL);
		TimerList_t2973412395 * L_13 = __this->get_bCellTimer_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00f1;
		}
	}
	{
		TimerList_t2973412395 * L_15 = __this->get_bCellTimer_27();
		NullCheck(L_15);
		Timer_t2917042437 * L_16 = TimerList_get_Item_m1551049948(L_15, _stringLiteral2996925095, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00bb;
		}
	}
	{
		TimerList_t2973412395 * L_17 = __this->get_bCellTimer_27();
		NullCheck(L_17);
		Timer_t2917042437 * L_18 = TimerList_get_Item_m1551049948(L_17, _stringLiteral2996925095, /*hidden argument*/NULL);
		int32_t L_19 = __this->get_level_25();
		float L_20 = Game_WaveDurationPerLevel_m1181764832(__this, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		L_18->set_duration_0(L_20);
	}

IL_00bb:
	{
		TimerList_t2973412395 * L_21 = __this->get_bCellTimer_27();
		NullCheck(L_21);
		Timer_t2917042437 * L_22 = TimerList_get_Item_m1551049948(L_21, _stringLiteral2697359927, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00f1;
		}
	}
	{
		TimerList_t2973412395 * L_23 = __this->get_bCellTimer_27();
		NullCheck(L_23);
		Timer_t2917042437 * L_24 = TimerList_get_Item_m1551049948(L_23, _stringLiteral2697359927, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_level_25();
		float L_26 = Game_ShooterDurationPerLevel_m230173777(__this, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		L_24->set_duration_0(L_26);
	}

IL_00f1:
	{
		AntibodyShooter_t4166449956 * L_27 = __this->get__antibodyShooter_22();
		NullCheck(L_27);
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		GameObject_SetActive_m2887581199(L_28, (bool)0, /*hidden argument*/NULL);
		TimerList_t2973412395 * L_29 = __this->get__timers_2();
		NullCheck(L_29);
		Timer_t2917042437 * L_30 = TimerList_get_Item_m1551049948(L_29, _stringLiteral2697392471, /*hidden argument*/NULL);
		Timer_t2917042437 * L_31 = L_30;
		NullCheck(L_31);
		float L_32 = L_31->get_duration_0();
		NullCheck(L_31);
		L_31->set_duration_0(((float)((float)L_32*(float)(0.75f))));
		float L_33 = __this->get_battleTimer_29();
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral3749811720, L_33, /*hidden argument*/NULL);
		UI_HIV_Button_t3180703511 * L_34 = ((UI_HIV_Button_t3180703511_StaticFields*)UI_HIV_Button_t3180703511_il2cpp_TypeInfo_var->static_fields)->get_main_5();
		NullCheck(L_34);
		UI_HIV_Button_Activate_m2168596695(L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Game::Battle()
extern "C"  void Game_Battle_m443051511 (Game_t1600141214 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_warning_28();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		AntibodyShooter_t4166449956 * L_1 = __this->get__antibodyShooter_22();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_bCellObject_26();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Game::LevelGain(System.Int32)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Game_LevelGain_m1081789369_MetadataUsageId;
extern "C"  int32_t Game_LevelGain_m1081789369 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_LevelGain_m1081789369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___level0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = powf((1.5f), ((float)((float)(((float)((float)L_0)))/(float)(1.5f))));
		V_0 = (((int32_t)((int32_t)((float)((float)(50.0f)*(float)L_1)))));
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_2-(int32_t)((int32_t)((int32_t)L_3%(int32_t)((int32_t)10)))));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Single Game::ShooterDurationPerLevel(System.Int32)
extern "C"  float Game_ShooterDurationPerLevel_m230173777 (Game_t1600141214 * __this, int32_t ___level0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___level0;
		___level0 = ((int32_t)((int32_t)L_0-(int32_t)1));
		int32_t L_1 = ___level0;
		int32_t L_2 = ___level0;
		int32_t L_3 = ___level0;
		V_0 = (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1*(int32_t)L_2))-(int32_t)((int32_t)((int32_t)2*(int32_t)L_3))))+(int32_t)5)))));
		float L_4 = V_0;
		return ((float)((float)(1.0f)/(float)L_4));
	}
}
// System.Single Game::WaveDurationPerLevel(System.Int32)
extern "C"  float Game_WaveDurationPerLevel_m1181764832 (Game_t1600141214 * __this, int32_t ___level0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		V_0 = (8.5f);
		V_1 = (-42.0f);
		V_2 = (5.0f);
		float L_0 = V_0;
		float L_1 = V_1;
		int32_t L_2 = ___level0;
		float L_3 = V_2;
		return ((float)((float)L_0+(float)((float)((float)L_1/(float)((float)((float)(((float)((float)L_2)))+(float)L_3))))));
	}
}
// System.Void Game::EndGame()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const uint32_t Game_EndGame_m3658353106_MetadataUsageId;
extern "C"  void Game_EndGame_m3658353106 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_EndGame_m3658353106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_0 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_1 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		int32_t L_2 = __this->get_score_24();
		Score_t1518975274  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Score__ctor_m51016996(&L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_4 = HighScores_Insert_m2661071179(L_1, L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_5 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_5);
		HighScores_Save_m1918938851(L_5, /*hidden argument*/NULL);
	}

IL_0030:
	{
		goto IL_009c;
	}

IL_0035:
	{
		HighScores_t3229912699 * L_6 = (HighScores_t3229912699 *)il2cpp_codegen_object_new(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores__ctor_m1897854302(L_6, (bool)1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set_main_3(L_6);
		HighScores_t3229912699 * L_7 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_7);
		bool L_8 = HighScores_Load_m3721535986(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_9 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		int32_t L_10 = __this->get_score_24();
		Score_t1518975274  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Score__ctor_m51016996(&L_11, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_12 = HighScores_Insert_m2661071179(L_9, L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_12) == ((int32_t)(-1))))
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_13 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_13);
		HighScores_Save_m1918938851(L_13, /*hidden argument*/NULL);
	}

IL_0076:
	{
		goto IL_009c;
	}

IL_007b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_14 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		int32_t L_15 = __this->get_score_24();
		Score_t1518975274  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Score__ctor_m51016996(&L_16, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		HighScores_Insert_m2661071179(L_14, L_16, /*hidden argument*/NULL);
		HighScores_t3229912699 * L_17 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_17);
		HighScores_Save_m1918938851(L_17, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// System.Void Game::Awake()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTimerList_t2973412395_m1006871450_MethodInfo_var;
extern const uint32_t Game_Awake_m2145690122_MetadataUsageId;
extern "C"  void Game_Awake_m2145690122 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Awake_m2145690122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->set_main_30(__this);
		TimerList_t2973412395 * L_0 = Component_GetComponent_TisTimerList_t2973412395_m1006871450(__this, /*hidden argument*/Component_GetComponent_TisTimerList_t2973412395_m1006871450_MethodInfo_var);
		__this->set__timers_2(L_0);
		return;
	}
}
// System.Void Game::Start()
extern Il2CppClass* FocalPoint_t1860633653_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m84040496_MethodInfo_var;
extern const uint32_t Game_Start_m3679318685_MetadataUsageId;
extern "C"  void Game_Start_m3679318685 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Start_m3679318685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		__this->set_score_24(0);
		__this->set_level_25(1);
		Game_CreateHIVList_m609994815(__this, (bool)0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (-25.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Game_CreateHIV_m2019914367(__this, L_0, L_1, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_2 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2638739322(&L_4, (-25.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m2469242620(L_3, L_4, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_5 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_5);
		FocalPointMove_t3911050876 * L_6 = FocalPoint_get_move_m4157530869(L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_get_right_m28012078(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		ObjectMovement2D_set_velocity_m1559477545(L_6, L_7, /*hidden argument*/NULL);
		Game_CreateTCellList_m687649580(__this, (bool)0, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_8 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2720820983(&L_9, (0.0f), (20.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Game_CreateTCell_m1287670579(__this, L_8, (9.0f), L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Rigidbody2D_t502193897 * L_11 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_10, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_12 = V_0;
		NullCheck(L_11);
		Rigidbody2D_set_velocity_m3592751374(L_11, L_12, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_13 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2720820983(&L_14, (25.0f), (-20.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = Game_CreateTCell_m1287670579(__this, L_13, (9.0f), L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Rigidbody2D_t502193897 * L_16 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_15, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t2243707579  L_17 = V_1;
		NullCheck(L_16);
		Rigidbody2D_set_velocity_m3592751374(L_16, L_17, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0133;
	}

IL_00de:
	{
		List_1_t1184231168 * L_18 = Game_get_tCellScripts_m1223424833(__this, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		NullCheck(L_18);
		TCell_t1815110036 * L_20 = List_1_get_Item_m84040496(L_18, L_19, /*hidden argument*/List_1_get_Item_m84040496_MethodInfo_var);
		NullCheck(L_20);
		CellMovement_t2110243311 * L_21 = TCell_get_move_m3002539955(L_20, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_22 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_22);
		FocalPointMove_t3911050876 * L_23 = FocalPoint_get_move_m4157530869(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = ObjectMovement2D_get_velocity_m987348582(L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		Vector2_t2243707579  L_25 = Vector2_get_normalized_m2985402409((&V_3), /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_26 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_26);
		float L_27 = FocalPoint_get_speed_m745917210(L_26, /*hidden argument*/NULL);
		float L_28 = Random_Range_m2884721203(NULL /*static, unused*/, (0.25f), (0.5f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_29 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_25, ((float)((float)L_27*(float)L_28)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		NullCheck(L_21);
		CellMovement_SetVelocity_m3961975388(L_21, L_30, /*hidden argument*/NULL);
		int32_t L_31 = V_2;
		V_2 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_0133:
	{
		int32_t L_32 = V_2;
		if ((((int32_t)L_32) < ((int32_t)2)))
		{
			goto IL_00de;
		}
	}
	{
		V_4 = 2;
		goto IL_0163;
	}

IL_0142:
	{
		Quaternion_t4030073918  L_33 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_5));
		Vector3_t2243707580  L_34 = V_5;
		Game_CreateTCell_m1287670579(__this, L_33, (9.0f), L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_4;
		V_4 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0163:
	{
		int32_t L_36 = V_4;
		if ((((int32_t)L_36) < ((int32_t)4)))
		{
			goto IL_0142;
		}
	}
	{
		__this->set_rbcTimer_21((2.0f));
		Game_CreateRBCList_m3543786311(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Game::Update()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Game_Update_m3455215516_MetadataUsageId;
extern "C"  void Game_Update_m3455215516 (Game_t1600141214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Update_m3455215516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_rbcTimer_21();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_rbcTimer_21(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_rbcTimer_21();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		__this->set_rbcTimer_21((2.0f));
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_4 = V_0;
		Game_CreateRBC_m679804250(__this, L_3, (18.0f), L_4, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void Game::OnTimerEnd(Timer)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m791288303_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2697392471;
extern const uint32_t Game_OnTimerEnd_m4079708735_MetadataUsageId;
extern "C"  void Game_OnTimerEnd_m4079708735 (Game_t1600141214 * __this, Timer_t2917042437 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_OnTimerEnd_m4079708735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Timer_t2917042437 * L_0 = ___t0;
		NullCheck(L_0);
		String_t* L_1 = Timer_get_name_m1851911015(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, _stringLiteral2697392471, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0022;
		}
	}
	{
		goto IL_0049;
	}

IL_0022:
	{
		int32_t L_5 = __this->get_level_25();
		if ((((int32_t)L_5) <= ((int32_t)1)))
		{
			goto IL_0044;
		}
	}
	{
		List_1_t2300943568 * L_6 = __this->get__antibodyList_23();
		AntibodyShooter_t4166449956 * L_7 = __this->get__antibodyShooter_22();
		NullCheck(L_7);
		Antibody_t2931822436 * L_8 = AntibodyShooter_Shoot_m4294917967(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		List_1_Add_m791288303(L_6, L_8, /*hidden argument*/List_1_Add_m791288303_MethodInfo_var);
	}

IL_0044:
	{
		goto IL_0049;
	}

IL_0049:
	{
		return;
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// HIV GameController::get_selected()
extern "C"  HIV_t2481767745 * GameController_get_selected_m2687499813 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = __this->get__selected_2();
		return L_0;
	}
}
// System.Void GameController::set_selected(HIV)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GameController_set_selected_m2123804800_MetadataUsageId;
extern "C"  void GameController_set_selected_m2123804800 (GameController_t3607102586 * __this, HIV_t2481767745 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_set_selected_m2123804800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = __this->get__selected_2();
		HIV_t2481767745 * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		HIV_t2481767745 * L_3 = __this->get__selected_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		HIV_t2481767745 * L_5 = __this->get__selected_2();
		NullCheck(L_5);
		int32_t L_6 = HIV_get_state_m3030343602(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		HIV_t2481767745 * L_7 = __this->get__selected_2();
		NullCheck(L_7);
		HIV_ChangeState_m1698492408(L_7, 0, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_0040:
	{
		HIV_t2481767745 * L_8 = __this->get__selected_2();
		NullCheck(L_8);
		L_8->set_selected_8((bool)0);
	}

IL_004c:
	{
		HIV_t2481767745 * L_9 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0066;
		}
	}
	{
		HIV_t2481767745 * L_11 = ___value0;
		NullCheck(L_11);
		L_11->set_selected_8((bool)1);
		HIV_t2481767745 * L_12 = ___value0;
		NullCheck(L_12);
		HIV_ChangeState_m1698492408(L_12, 1, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_0066:
	{
		HIV_t2481767745 * L_13 = ___value0;
		__this->set__selected_2(L_13);
		return;
	}
}
// System.Void GameController::OnLeftClick()
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var;
extern const uint32_t GameController_OnLeftClick_m3920841173_MetadataUsageId;
extern "C"  void GameController_OnLeftClick_m3920841173 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_OnLeftClick_m3920841173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	HIV_t2481767745 * V_1 = NULL;
	{
		Controller_t1937198888 * L_0 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Controller_getSelected_m1390842220(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t3275118058 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		Transform_t3275118058 * L_4 = V_0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		HIV_t2481767745 * L_6 = GameObject_GetComponent_TisHIV_t2481767745_m121922886(L_5, /*hidden argument*/GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}
	{
		Transform_t3275118058 * L_8 = V_0;
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		HIV_t2481767745 * L_10 = GameObject_GetComponent_TisHIV_t2481767745_m121922886(L_9, /*hidden argument*/GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var);
		V_1 = L_10;
		HIV_t2481767745 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = HIV_get_state_m3030343602(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004e;
		}
	}
	{
		HIV_t2481767745 * L_13 = V_1;
		NullCheck(L_13);
		int32_t L_14 = HIV_get_state_m3030343602(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0055;
		}
	}

IL_004e:
	{
		HIV_t2481767745 * L_15 = V_1;
		GameController_set_selected_m2123804800(__this, L_15, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void GameController::OnLeftDrag()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Selected_t1107388507_m966267950_MethodInfo_var;
extern const uint32_t GameController_OnLeftDrag_m4000772339_MetadataUsageId;
extern "C"  void GameController_OnLeftDrag_m4000772339 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_OnLeftDrag_m4000772339_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = GameController_get_selected_m2687499813(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		HIV_t2481767745 * L_2 = GameController_get_selected_m2687499813(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		HIV_Selected_t1107388507 * L_3 = Component_GetComponent_TisHIV_Selected_t1107388507_m966267950(L_2, /*hidden argument*/Component_GetComponent_TisHIV_Selected_t1107388507_m966267950_MethodInfo_var);
		Controller_t1937198888 * L_4 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Controller_get_worldPosition_m2261149775(L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_destination_4(L_7);
	}

IL_0034:
	{
		return;
	}
}
// System.Void GameController::OnLeftRelease()
extern "C"  void GameController_OnLeftRelease_m4236716976 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		GameController_set_selected_m2123804800(__this, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::OnRightClick()
extern "C"  void GameController_OnRightClick_m3364544638 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameController::OnRightDrag()
extern "C"  void GameController_OnRightDrag_m2248094878 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameController::OnRightRelease()
extern "C"  void GameController_OnRightRelease_m2387278377 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameController::OnSwipe(GameController/SwipeType)
extern "C"  void GameController_OnSwipe_m2976886677 (GameController_t3607102586 * __this, int32_t ___s0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___s0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		goto IL_001c;
	}

IL_0012:
	{
		goto IL_0021;
	}

IL_0017:
	{
		goto IL_0021;
	}

IL_001c:
	{
		goto IL_0021;
	}

IL_0021:
	{
		return;
	}
}
// System.Void GameController::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern const uint32_t GameController_Update_m1556003900_MetadataUsageId;
extern "C"  void GameController_Update_m1556003900 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Update_m1556003900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0090;
		}
	}
	{
		HIV_t2481767745 * L_1 = GameController_get_selected_m2687499813(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		HIV_t2481767745 * L_3 = GameController_get_selected_m2687499813(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = HIV_get_state_m3030343602(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		HIV_t2481767745 * L_5 = GameController_get_selected_m2687499813(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Behaviour_get_isActiveAndEnabled_m3838334305(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0047;
		}
	}

IL_0040:
	{
		GameController_set_selected_m2123804800(__this, (HIV_t2481767745 *)NULL, /*hidden argument*/NULL);
	}

IL_0047:
	{
		Controller_t1937198888 * L_7 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_7);
		bool L_8 = Controller_LeftClick_m156288658(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		GameController_OnLeftClick_m3920841173(__this, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_0061:
	{
		Controller_t1937198888 * L_9 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_9);
		bool L_10 = Controller_LeftDrag_m794894538(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007b;
		}
	}
	{
		GameController_OnLeftDrag_m4000772339(__this, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_007b:
	{
		Controller_t1937198888 * L_11 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_11);
		bool L_12 = Controller_LeftRelease_m547954749(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0090;
		}
	}
	{
		GameController_OnLeftRelease_m4236716976(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void GameInitializer::.ctor()
extern "C"  void GameInitializer__ctor_m4077282703 (GameInitializer_t3613149210 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameInitializer::Start()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const uint32_t GameInitializer_Start_m3124885323_MetadataUsageId;
extern "C"  void GameInitializer_Start_m3124885323 (GameInitializer_t3613149210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameInitializer_Start_m3124885323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Application_set_targetFrameRate_m2941880625(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		HighScores_t3229912699 * L_0 = (HighScores_t3229912699 *)il2cpp_codegen_object_new(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores__ctor_m1897854302(L_0, (bool)1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set_main_3(L_0);
		HighScores_t3229912699 * L_1 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_1);
		bool L_2 = HighScores_Load_m3721535986(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_3 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_3);
		HighScores_Save_m1918938851(L_3, /*hidden argument*/NULL);
	}

IL_002d:
	{
		Settings_Load_m2929002520(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> GameItem::get_itemList()
extern "C"  List_1_t1125654279 * GameItem_get_itemList_m426376171 (GameItem_t806762789 * __this, const MethodInfo* method)
{
	{
		List_1_t1125654279 * L_0 = __this->get__itemList_2();
		return L_0;
	}
}
extern "C"  List_1_t1125654279 * GameItem_get_itemList_m426376171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GameItem_t806762789 * _thisAdjusted = reinterpret_cast<GameItem_t806762789 *>(__this + 1);
	return GameItem_get_itemList_m426376171(_thisAdjusted, method);
}
// System.Int32 GameItem::get_population()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const uint32_t GameItem_get_population_m1805638414_MetadataUsageId;
extern "C"  int32_t GameItem_get_population_m1805638414 (GameItem_t806762789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameItem_get_population_m1805638414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = __this->get__itemList_2();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2764296230(L_0, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		return L_1;
	}
}
extern "C"  int32_t GameItem_get_population_m1805638414_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GameItem_t806762789 * _thisAdjusted = reinterpret_cast<GameItem_t806762789 *>(__this + 1);
	return GameItem_get_population_m1805638414(_thisAdjusted, method);
}
// System.Int32 GameItem::get_populationMax()
extern "C"  int32_t GameItem_get_populationMax_m4143654770 (GameItem_t806762789 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__maximumNumber_1();
		return L_0;
	}
}
extern "C"  int32_t GameItem_get_populationMax_m4143654770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GameItem_t806762789 * _thisAdjusted = reinterpret_cast<GameItem_t806762789 *>(__this + 1);
	return GameItem_get_populationMax_m4143654770(_thisAdjusted, method);
}
// UnityEngine.GameObject GameItem::get_item()
extern "C"  GameObject_t1756533147 * GameItem_get_item_m1086059871 (GameItem_t806762789 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_itemReference_0();
		return L_0;
	}
}
extern "C"  GameObject_t1756533147 * GameItem_get_item_m1086059871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GameItem_t806762789 * _thisAdjusted = reinterpret_cast<GameItem_t806762789 *>(__this + 1);
	return GameItem_get_item_m1086059871(_thisAdjusted, method);
}
// System.Void GameItem::Initialize(System.Boolean)
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const uint32_t GameItem_Initialize_m3755603307_MetadataUsageId;
extern "C"  void GameItem_Initialize_m3755603307 (GameItem_t806762789 * __this, bool ___instantiate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameItem_Initialize_m3755603307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set__itemList_2(L_0);
		return;
	}
}
extern "C"  void GameItem_Initialize_m3755603307_AdjustorThunk (Il2CppObject * __this, bool ___instantiate0, const MethodInfo* method)
{
	GameItem_t806762789 * _thisAdjusted = reinterpret_cast<GameItem_t806762789 *>(__this + 1);
	GameItem_Initialize_m3755603307(_thisAdjusted, ___instantiate0, method);
}
// Conversion methods for marshalling of: GameItem
extern "C" void GameItem_t806762789_marshal_pinvoke(const GameItem_t806762789& unmarshaled, GameItem_t806762789_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___itemReference_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'itemReference' of type 'GameItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___itemReference_0Exception);
}
extern "C" void GameItem_t806762789_marshal_pinvoke_back(const GameItem_t806762789_marshaled_pinvoke& marshaled, GameItem_t806762789& unmarshaled)
{
	Il2CppCodeGenException* ___itemReference_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'itemReference' of type 'GameItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___itemReference_0Exception);
}
// Conversion method for clean up from marshalling of: GameItem
extern "C" void GameItem_t806762789_marshal_pinvoke_cleanup(GameItem_t806762789_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: GameItem
extern "C" void GameItem_t806762789_marshal_com(const GameItem_t806762789& unmarshaled, GameItem_t806762789_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___itemReference_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'itemReference' of type 'GameItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___itemReference_0Exception);
}
extern "C" void GameItem_t806762789_marshal_com_back(const GameItem_t806762789_marshaled_com& marshaled, GameItem_t806762789& unmarshaled)
{
	Il2CppCodeGenException* ___itemReference_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'itemReference' of type 'GameItem': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___itemReference_0Exception);
}
// Conversion method for clean up from marshalling of: GameItem
extern "C" void GameItem_t806762789_marshal_com_cleanup(GameItem_t806762789_marshaled_com& marshaled)
{
}
// System.Void GP120::.ctor()
extern "C"  void GP120__ctor_m4265588283 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tropism GP120::get_top()
extern "C"  Tropism_t3662836552  GP120_get_top_m3080711702 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		Tropism_t3662836552  L_0 = __this->get__top_2();
		return L_0;
	}
}
// System.Void GP120::set_top(Tropism)
extern "C"  void GP120_set_top_m2815271707 (GP120_t1512820336 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = GP120_get_particles_m948954212(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		int32_t L_2 = (&___value0)->get_coreceptor_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)((((int32_t)L_2) == ((int32_t)4))? 1 : 0), /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_3 = GP120_get_displayLeft_m2311060087(__this, /*hidden argument*/NULL);
		int32_t L_4 = (&___value0)->get_coreceptor_11();
		Color_t2020392075  L_5 = GP120_LeftDefaultColor_m2491958122(__this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRenderer_set_color_m2339931967(L_3, L_5, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_6 = GP120_get_displayRight_m461027194(__this, /*hidden argument*/NULL);
		int32_t L_7 = (&___value0)->get_coreceptor_11();
		Color_t2020392075  L_8 = GP120_RightDefaultColor_m1880879577(__this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		SpriteRenderer_set_color_m2339931967(L_6, L_8, /*hidden argument*/NULL);
		Tropism_t3662836552  L_9 = ___value0;
		__this->set__top_2(L_9);
		return;
	}
}
// Statistics GP120::get_stats()
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t GP120_get_stats_m664361269_MetadataUsageId;
extern "C"  Statistics_t2537377007  GP120_get_stats_m664361269 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GP120_get_stats_m664361269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Tropism_t3662836552  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Initobj (Statistics_t2537377007_il2cpp_TypeInfo_var, (&V_0));
		Tropism_t3662836552  L_0 = GP120_get_top_m3080711702(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		int32_t L_1 = (&V_1)->get_receptor_10();
		V_2 = L_1;
		int32_t L_2 = V_2;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_3 = V_2;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		goto IL_004d;
	}

IL_0029:
	{
		Statistics_set_Item_m1870614441((&V_0), 4, (9.0f), /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_003b:
	{
		Statistics_set_Item_m1870614441((&V_0), 4, (2.0f), /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_004d:
	{
		Statistics_t2537377007  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.SpriteRenderer GP120::get_displayLeft()
extern "C"  SpriteRenderer_t1209076198 * GP120_get_displayLeft_m2311060087 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get__displayLeft_3();
		return L_0;
	}
}
// UnityEngine.SpriteRenderer GP120::get_displayRight()
extern "C"  SpriteRenderer_t1209076198 * GP120_get_displayRight_m461027194 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get__displayRight_4();
		return L_0;
	}
}
// UnityEngine.ParticleSystem GP120::get_particles()
extern "C"  ParticleSystem_t3394631041 * GP120_get_particles_m948954212 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get__particles_5();
		return L_0;
	}
}
// UnityEngine.Color GP120::GetColorLeft()
extern "C"  Color_t2020392075  GP120_GetColorLeft_m1399135880 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = GP120_get_displayLeft_m2311060087(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Color_t2020392075  L_1 = SpriteRenderer_get_color_m345525162(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Color GP120::GetRightColor()
extern "C"  Color_t2020392075  GP120_GetRightColor_m437877421 (GP120_t1512820336 * __this, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = GP120_get_displayRight_m461027194(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Color_t2020392075  L_1 = SpriteRenderer_get_color_m345525162(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Color GP120::LeftDefaultColor(Tropism/CoReceptorType)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t GP120_LeftDefaultColor_m2491958122_MetadataUsageId;
extern "C"  Color_t2020392075  GP120_LeftDefaultColor_m2491958122 (GP120_t1512820336 * __this, int32_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GP120_LeftDefaultColor_m2491958122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		int32_t L_0 = ___c0;
		if ((!(((uint32_t)L_0) == ((uint32_t)4))))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Color_t2020392075  L_1 = Tropism_DefaultColor_m3873948415(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0012:
	{
		int32_t L_2 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Color_t2020392075  L_3 = Tropism_DefaultColor_m3873948415(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// UnityEngine.Color GP120::RightDefaultColor(Tropism/CoReceptorType)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t GP120_RightDefaultColor_m1880879577_MetadataUsageId;
extern "C"  Color_t2020392075  GP120_RightDefaultColor_m1880879577 (GP120_t1512820336 * __this, int32_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GP120_RightDefaultColor_m1880879577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		int32_t L_0 = ___c0;
		if ((!(((uint32_t)L_0) == ((uint32_t)4))))
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Color_t2020392075  L_1 = Tropism_DefaultColor_m3873948415(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0012:
	{
		int32_t L_2 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Color_t2020392075  L_3 = Tropism_DefaultColor_m3873948415(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Void GP41::.ctor()
extern "C"  void GP41__ctor_m70649397 (GP41_t921954836 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Statistics GP41::get_stats()
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t GP41_get_stats_m3961996551_MetadataUsageId;
extern "C"  Statistics_t2537377007  GP41_get_stats_m3961996551 (GP41_t921954836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GP41_get_stats_m3961996551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Statistics_t2537377007_il2cpp_TypeInfo_var, (&V_0));
		Statistics_t2537377007  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.SpriteRenderer GP41::get_spriteRender()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t GP41_get_spriteRender_m1828691819_MetadataUsageId;
extern "C"  SpriteRenderer_t1209076198 * GP41_get_spriteRender_m1828691819 (GP41_t921954836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GP41_get_spriteRender_m1828691819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		return L_1;
	}
}
// UnityEngine.MeshRenderer GP41::get_meshRenderer()
extern const MethodInfo* GameObject_GetComponentInChildren_TisMeshRenderer_t1268241104_m886037100_MethodInfo_var;
extern const uint32_t GP41_get_meshRenderer_m864894232_MetadataUsageId;
extern "C"  MeshRenderer_t1268241104 * GP41_get_meshRenderer_m864894232 (GP41_t921954836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GP41_get_meshRenderer_m864894232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MeshRenderer_t1268241104 * L_1 = GameObject_GetComponentInChildren_TisMeshRenderer_t1268241104_m886037100(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisMeshRenderer_t1268241104_m886037100_MethodInfo_var);
		return L_1;
	}
}
// System.Void GroupColors::.ctor()
extern "C"  void GroupColors__ctor_m64951518 (GroupColors_t153290803 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GroupColors::OnValidate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3922742667_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2749088815_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m91496003_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m408052341_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3567641789_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2697300105_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2672808425_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m75521715_MethodInfo_var;
extern const uint32_t GroupColors_OnValidate_m2398987905_MetadataUsageId;
extern "C"  void GroupColors_OnValidate_m2398987905 (GroupColors_t153290803 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GroupColors_OnValidate_m2398987905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRender_t2828817349  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1732668155  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ImageRender_t3780187187  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t2684037993  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	SpriteRenderer_t1209076198 * G_B5_0 = NULL;
	SpriteRenderer_t1209076198 * G_B4_0 = NULL;
	Color_t2020392075  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	SpriteRenderer_t1209076198 * G_B6_1 = NULL;
	Image_t2042527209 * G_B15_0 = NULL;
	Image_t2042527209 * G_B14_0 = NULL;
	Color_t2020392075  G_B16_0;
	memset(&G_B16_0, 0, sizeof(G_B16_0));
	Image_t2042527209 * G_B16_1 = NULL;
	{
		List_1_t2197938481 * L_0 = __this->get_spriteList_2();
		NullCheck(L_0);
		Enumerator_t1732668155  L_1 = List_1_GetEnumerator_m3922742667(L_0, /*hidden argument*/List_1_GetEnumerator_m3922742667_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0053;
		}

IL_0011:
		{
			SpriteRender_t2828817349  L_2 = Enumerator_get_Current_m2749088815((&V_1), /*hidden argument*/Enumerator_get_Current_m2749088815_MethodInfo_var);
			V_0 = L_2;
			SpriteRenderer_t1209076198 * L_3 = (&V_0)->get_render_0();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0053;
			}
		}

IL_002a:
		{
			SpriteRenderer_t1209076198 * L_5 = (&V_0)->get_render_0();
			bool L_6 = __this->get_common_4();
			G_B4_0 = L_5;
			if (!L_6)
			{
				G_B5_0 = L_5;
				goto IL_0047;
			}
		}

IL_003c:
		{
			Color_t2020392075  L_7 = __this->get_commonColor_5();
			G_B6_0 = L_7;
			G_B6_1 = G_B4_0;
			goto IL_004e;
		}

IL_0047:
		{
			Color_t2020392075  L_8 = (&V_0)->get__color_1();
			G_B6_0 = L_8;
			G_B6_1 = G_B5_0;
		}

IL_004e:
		{
			NullCheck(G_B6_1);
			SpriteRenderer_set_color_m2339931967(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		}

IL_0053:
		{
			bool L_9 = Enumerator_MoveNext_m91496003((&V_1), /*hidden argument*/Enumerator_MoveNext_m91496003_MethodInfo_var);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x72, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m408052341((&V_1), /*hidden argument*/Enumerator_Dispose_m408052341_MethodInfo_var);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0072:
	{
		List_1_t3149308319 * L_10 = __this->get_imageList_3();
		NullCheck(L_10);
		Enumerator_t2684037993  L_11 = List_1_GetEnumerator_m3567641789(L_10, /*hidden argument*/List_1_GetEnumerator_m3567641789_MethodInfo_var);
		V_3 = L_11;
	}

IL_007e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c5;
		}

IL_0083:
		{
			ImageRender_t3780187187  L_12 = Enumerator_get_Current_m2697300105((&V_3), /*hidden argument*/Enumerator_get_Current_m2697300105_MethodInfo_var);
			V_2 = L_12;
			Image_t2042527209 * L_13 = (&V_2)->get_render_0();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_00c5;
			}
		}

IL_009c:
		{
			Image_t2042527209 * L_15 = (&V_2)->get_render_0();
			bool L_16 = __this->get_common_4();
			G_B14_0 = L_15;
			if (!L_16)
			{
				G_B15_0 = L_15;
				goto IL_00b9;
			}
		}

IL_00ae:
		{
			Color_t2020392075  L_17 = __this->get_commonColor_5();
			G_B16_0 = L_17;
			G_B16_1 = G_B14_0;
			goto IL_00c0;
		}

IL_00b9:
		{
			Color_t2020392075  L_18 = (&V_2)->get__color_1();
			G_B16_0 = L_18;
			G_B16_1 = G_B15_0;
		}

IL_00c0:
		{
			NullCheck(G_B16_1);
			VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B16_1, G_B16_0);
		}

IL_00c5:
		{
			bool L_19 = Enumerator_MoveNext_m2672808425((&V_3), /*hidden argument*/Enumerator_MoveNext_m2672808425_MethodInfo_var);
			if (L_19)
			{
				goto IL_0083;
			}
		}

IL_00d1:
		{
			IL2CPP_LEAVE(0xE4, FINALLY_00d6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d6;
	}

FINALLY_00d6:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m75521715((&V_3), /*hidden argument*/Enumerator_Dispose_m75521715_MethodInfo_var);
		IL2CPP_END_FINALLY(214)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(214)
	{
		IL2CPP_JUMP_TBL(0xE4, IL_00e4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e4:
	{
		return;
	}
}
// System.Void HighScoreDisplay::.ctor()
extern "C"  void HighScoreDisplay__ctor_m504516991 (HighScoreDisplay_t899508680 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighScoreDisplay::Awake()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const uint32_t HighScoreDisplay_Awake_m4172241172_MetadataUsageId;
extern "C"  void HighScoreDisplay_Awake_m4172241172 (HighScoreDisplay_t899508680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScoreDisplay_Awake_m4172241172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_0 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		HighScores_t3229912699 * L_1 = (HighScores_t3229912699 *)il2cpp_codegen_object_new(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores__ctor_m1897854302(L_1, (bool)1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set_main_3(L_1);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_2 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_2);
		HighScores_Load_m3721535986(L_2, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_text_2(L_3);
		return;
	}
}
// System.Void HighScoreDisplay::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* Score_t1518975274_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern const uint32_t HighScoreDisplay_OnGUI_m3445912053_MetadataUsageId;
extern "C"  void HighScoreDisplay_OnGUI_m3445912053 (HighScoreDisplay_t899508680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScoreDisplay_OnGUI_m3445912053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Text_t356221433 * L_0 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		int32_t L_2 = __this->get_display_3();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_017a;
	}

IL_0029:
	{
		Text_t356221433 * L_5 = __this->get_text_2();
		NullCheck(L_5);
		Text_set_alignment_m1816221961(L_5, 1, /*hidden argument*/NULL);
		Text_t356221433 * L_6 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_7 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_7);
		ScoreList_t2288120898 * L_8 = HighScores_get_scores_m927527757(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
		goto IL_017a;
	}

IL_0054:
	{
		int32_t L_10 = __this->get_show_4();
		if (L_10)
		{
			goto IL_0065;
		}
	}
	{
		G_B7_0 = 1;
		goto IL_006b;
	}

IL_0065:
	{
		int32_t L_11 = __this->get_lowNumber_5();
		G_B7_0 = L_11;
	}

IL_006b:
	{
		V_1 = G_B7_0;
		int32_t L_12 = __this->get_show_4();
		if (L_12)
		{
			goto IL_008d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_13 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_13);
		ScoreList_t2288120898 * L_14 = HighScores_get_scores_m927527757(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		ScoreU5BU5D_t100793071* L_15 = L_14->get_highScores_0();
		NullCheck(L_15);
		G_B10_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))));
		goto IL_0093;
	}

IL_008d:
	{
		int32_t L_16 = __this->get_highNumber_6();
		G_B10_0 = L_16;
	}

IL_0093:
	{
		V_2 = G_B10_0;
		int32_t L_17 = V_1;
		V_3 = ((int32_t)((int32_t)L_17-(int32_t)1));
		goto IL_016e;
	}

IL_009d:
	{
		Text_t356221433 * L_18 = __this->get_text_2();
		NullCheck(L_18);
		Text_set_alignment_m1816221961(L_18, 3, /*hidden argument*/NULL);
		Text_t356221433 * L_19 = __this->get_text_2();
		Text_t356221433 * L_20 = __this->get_text_2();
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_20);
		int32_t L_22 = V_3;
		V_4 = ((int32_t)((int32_t)L_22+(int32_t)1));
		String_t* L_23 = Int32_ToString_m2960866144((&V_4), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m612901809(NULL /*static, unused*/, L_21, L_23, _stringLiteral372029316, /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_24);
		int32_t L_25 = __this->get_spaces_7();
		V_5 = L_25;
		goto IL_010b;
	}

IL_00e8:
	{
		Text_t356221433 * L_26 = __this->get_text_2();
		Text_t356221433 * L_27 = L_26;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_27);
		Il2CppChar L_29 = ((Il2CppChar)((int32_t)32));
		Il2CppObject * L_30 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m56707527(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		NullCheck(L_27);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_27, L_31);
		int32_t L_32 = V_5;
		V_5 = ((int32_t)((int32_t)L_32-(int32_t)1));
	}

IL_010b:
	{
		int32_t L_33 = V_5;
		int32_t L_34 = V_3;
		V_6 = ((int32_t)((int32_t)L_34+(int32_t)1));
		String_t* L_35 = Int32_ToString_m2960866144((&V_6), /*hidden argument*/NULL);
		NullCheck(L_35);
		int32_t L_36 = String_get_Length_m1606060069(L_35, /*hidden argument*/NULL);
		if ((((int32_t)L_33) > ((int32_t)L_36)))
		{
			goto IL_00e8;
		}
	}
	{
		Text_t356221433 * L_37 = __this->get_text_2();
		Text_t356221433 * L_38 = __this->get_text_2();
		NullCheck(L_38);
		String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_38);
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_40 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_40);
		ScoreList_t2288120898 * L_41 = HighScores_get_scores_m927527757(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		ScoreU5BU5D_t100793071* L_42 = L_41->get_highScores_0();
		int32_t L_43 = V_3;
		NullCheck(L_42);
		Score_t1518975274  L_44 = (*(Score_t1518975274 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Il2CppObject * L_45 = Box(Score_t1518975274_il2cpp_TypeInfo_var, &L_44);
		Il2CppChar L_46 = ((Il2CppChar)((int32_t)10));
		Il2CppObject * L_47 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m2000667605(NULL /*static, unused*/, L_39, L_45, L_47, /*hidden argument*/NULL);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_48);
		int32_t L_49 = V_3;
		V_3 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_016e:
	{
		int32_t L_50 = V_3;
		int32_t L_51 = V_2;
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_009d;
		}
	}
	{
		goto IL_017a;
	}

IL_017a:
	{
		return;
	}
}
// System.Void HighScoreDisplaySingle::.ctor()
extern "C"  void HighScoreDisplaySingle__ctor_m2739648985 (HighScoreDisplaySingle_t3977972496 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighScoreDisplaySingle::Awake()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const uint32_t HighScoreDisplaySingle_Awake_m2105601068_MetadataUsageId;
extern "C"  void HighScoreDisplaySingle_Awake_m2105601068 (HighScoreDisplaySingle_t3977972496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScoreDisplaySingle_Awake_m2105601068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_0 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		HighScores_t3229912699 * L_1 = (HighScores_t3229912699 *)il2cpp_codegen_object_new(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores__ctor_m1897854302(L_1, (bool)1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set_main_3(L_1);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_2 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_2);
		HighScores_Load_m3721535986(L_2, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_text_2(L_3);
		return;
	}
}
// System.Void HighScoreDisplaySingle::OnGUI()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58974887;
extern const uint32_t HighScoreDisplaySingle_OnGUI_m1558048811_MetadataUsageId;
extern "C"  void HighScoreDisplaySingle_OnGUI_m1558048811 (HighScoreDisplaySingle_t3977972496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScoreDisplaySingle_OnGUI_m1558048811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_1 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		NullCheck(L_1);
		ScoreList_t2288120898 * L_2 = HighScores_get_scores_m927527757(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ScoreU5BU5D_t100793071* L_3 = L_2->get_highScores_0();
		NullCheck(L_3);
		String_t* L_4 = Score_ToString_m2662655366(((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral58974887, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		return;
	}
}
// System.Void HighScores::.ctor(System.Boolean,System.Boolean)
extern Il2CppClass* ScoreList_t2288120898_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const uint32_t HighScores__ctor_m1897854302_MetadataUsageId;
extern "C"  void HighScores__ctor_m1897854302 (HighScores_t3229912699 * __this, bool ___saveToMain0, bool ___overwrite1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores__ctor_m1897854302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ScoreList_t2288120898 * L_0 = (ScoreList_t2288120898 *)il2cpp_codegen_object_new(ScoreList_t2288120898_il2cpp_TypeInfo_var);
		ScoreList__ctor_m3134941225(L_0, /*hidden argument*/NULL);
		__this->set__highScores_2(L_0);
		ScoreList_t2288120898 * L_1 = __this->get__highScores_2();
		NullCheck(L_1);
		L_1->set_highScores_0(((ScoreU5BU5D_t100793071*)SZArrayNew(ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)((int32_t)10))))));
		bool L_2 = ___saveToMain0;
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_3 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		bool L_4 = ___overwrite1;
		if (!L_4)
		{
			goto IL_0040;
		}
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set_main_3(__this);
	}

IL_0040:
	{
		return;
	}
}
// System.Void HighScores::.ctor(Score[],System.Boolean,System.Boolean)
extern Il2CppClass* ScoreList_t2288120898_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const uint32_t HighScores__ctor_m1484738200_MetadataUsageId;
extern "C"  void HighScores__ctor_m1484738200 (HighScores_t3229912699 * __this, ScoreU5BU5D_t100793071* ___scoreList0, bool ___saveToMain1, bool ___overwrite2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores__ctor_m1484738200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ScoreList_t2288120898 * L_0 = (ScoreList_t2288120898 *)il2cpp_codegen_object_new(ScoreList_t2288120898_il2cpp_TypeInfo_var);
		ScoreList__ctor_m3134941225(L_0, /*hidden argument*/NULL);
		__this->set__highScores_2(L_0);
		ScoreList_t2288120898 * L_1 = __this->get__highScores_2();
		NullCheck(L_1);
		L_1->set_highScores_0(((ScoreU5BU5D_t100793071*)SZArrayNew(ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)((int32_t)10))))));
		V_0 = 0;
		goto IL_0051;
	}

IL_002b:
	{
		ScoreList_t2288120898 * L_2 = __this->get__highScores_2();
		NullCheck(L_2);
		ScoreU5BU5D_t100793071* L_3 = L_2->get_highScores_0();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		ScoreU5BU5D_t100793071* L_5 = ___scoreList0;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		(*(Score_t1518975274 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))) = (*(Score_t1518975274 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_8 = V_0;
		ScoreU5BU5D_t100793071* L_9 = ___scoreList0;
		NullCheck(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Min_m1648492575(NULL /*static, unused*/, (((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))))), (10.0f), /*hidden argument*/NULL);
		if ((((float)(((float)((float)L_8)))) < ((float)L_10)))
		{
			goto IL_002b;
		}
	}
	{
		bool L_11 = ___saveToMain1;
		if (!L_11)
		{
			goto IL_0082;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_12 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		if (!L_12)
		{
			goto IL_007c;
		}
	}
	{
		bool L_13 = ___overwrite2;
		if (!L_13)
		{
			goto IL_0082;
		}
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set_main_3(__this);
	}

IL_0082:
	{
		return;
	}
}
// ScoreList HighScores::get_scores()
extern "C"  ScoreList_t2288120898 * HighScores_get_scores_m927527757 (HighScores_t3229912699 * __this, const MethodInfo* method)
{
	{
		ScoreList_t2288120898 * L_0 = __this->get__highScores_2();
		return L_0;
	}
}
// System.Int32 HighScores::Insert(Score)
extern "C"  int32_t HighScores_Insert_m2661071179 (HighScores_t3229912699 * __this, Score_t1518975274  ___s0, const MethodInfo* method)
{
	{
		ScoreList_t2288120898 * L_0 = __this->get__highScores_2();
		NullCheck(L_0);
		ScoreList_Sort_m1163570930(L_0, 0, /*hidden argument*/NULL);
		ScoreList_t2288120898 * L_1 = __this->get__highScores_2();
		NullCheck(L_1);
		ScoreU5BU5D_t100793071* L_2 = L_1->get_highScores_0();
		ScoreList_t2288120898 * L_3 = __this->get__highScores_2();
		NullCheck(L_3);
		ScoreU5BU5D_t100793071* L_4 = L_3->get_highScores_0();
		NullCheck(L_4);
		NullCheck(L_2);
		Score_t1518975274  L_5 = ___s0;
		bool L_6 = Score_op_LessThan_m485616521(NULL /*static, unused*/, (*(Score_t1518975274 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)1)))))), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_007c;
		}
	}
	{
		ScoreList_t2288120898 * L_7 = __this->get__highScores_2();
		NullCheck(L_7);
		ScoreU5BU5D_t100793071* L_8 = L_7->get_highScores_0();
		ScoreList_t2288120898 * L_9 = __this->get__highScores_2();
		NullCheck(L_9);
		ScoreU5BU5D_t100793071* L_10 = L_9->get_highScores_0();
		NullCheck(L_10);
		NullCheck(L_8);
		Score_t1518975274  L_11 = ___s0;
		(*(Score_t1518975274 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))-(int32_t)1)))))) = L_11;
		ScoreList_t2288120898 * L_12 = __this->get__highScores_2();
		ScoreList_t2288120898 * L_13 = __this->get__highScores_2();
		NullCheck(L_13);
		ScoreU5BU5D_t100793071* L_14 = L_13->get_highScores_0();
		NullCheck(L_14);
		NullCheck(L_12);
		int32_t L_15 = ScoreList_Sort_m1163570930(L_12, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))-(int32_t)1)), /*hidden argument*/NULL);
		return L_15;
	}

IL_007c:
	{
		return (-1);
	}
}
// System.Void HighScores::Reset()
extern Il2CppClass* ScoreList_t2288120898_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var;
extern const uint32_t HighScores_Reset_m1182637323_MetadataUsageId;
extern "C"  void HighScores_Reset_m1182637323 (HighScores_t3229912699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores_Reset_m1182637323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ScoreList_t2288120898 * L_0 = (ScoreList_t2288120898 *)il2cpp_codegen_object_new(ScoreList_t2288120898_il2cpp_TypeInfo_var);
		ScoreList__ctor_m3134941225(L_0, /*hidden argument*/NULL);
		__this->set__highScores_2(L_0);
		ScoreList_t2288120898 * L_1 = __this->get__highScores_2();
		NullCheck(L_1);
		L_1->set_highScores_0(((ScoreU5BU5D_t100793071*)SZArrayNew(ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)((int32_t)10))))));
		return;
	}
}
// HighScores HighScores::get_Default()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern const uint32_t HighScores_get_Default_m4260063046_MetadataUsageId;
extern "C"  HighScores_t3229912699 * HighScores_get_Default_m4260063046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores_get_Default_m4260063046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HighScores_t3229912699 * L_0 = (HighScores_t3229912699 *)il2cpp_codegen_object_new(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores__ctor_m1897854302(L_0, (bool)0, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean HighScores::Save()
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreList_t2288120898_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3723799026;
extern const uint32_t HighScores_Save_m1918938851_MetadataUsageId;
extern "C"  bool HighScores_Save_m1918938851 (HighScores_t3229912699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores_Save_m1918938851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryFormatter_t1866979105 * V_0 = NULL;
	FileStream_t1695958676 * V_1 = NULL;
	ScoreList_t2288120898 * V_2 = NULL;
	{
		BinaryFormatter_t1866979105 * L_0 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		String_t* L_1 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get__filePath_1();
		FileStream_t1695958676 * L_2 = File_Open_m1543461971(NULL /*static, unused*/, L_1, 4, /*hidden argument*/NULL);
		V_1 = L_2;
		ScoreList_t2288120898 * L_3 = (ScoreList_t2288120898 *)il2cpp_codegen_object_new(ScoreList_t2288120898_il2cpp_TypeInfo_var);
		ScoreList__ctor_m3134941225(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		ScoreList_t2288120898 * L_4 = V_2;
		ScoreList_t2288120898 * L_5 = __this->get__highScores_2();
		NullCheck(L_5);
		ScoreU5BU5D_t100793071* L_6 = L_5->get_highScores_0();
		NullCheck(L_4);
		L_4->set_highScores_0(L_6);
		BinaryFormatter_t1866979105 * L_7 = V_0;
		FileStream_t1695958676 * L_8 = V_1;
		ScoreList_t2288120898 * L_9 = V_2;
		NullCheck(L_7);
		BinaryFormatter_Serialize_m433301673(L_7, L_8, L_9, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3723799026, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean HighScores::ResetSave()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355970408;
extern const uint32_t HighScores_ResetSave_m3265589570_MetadataUsageId;
extern "C"  bool HighScores_ResetSave_m3265589570 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores_ResetSave_m3265589570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HighScores_t3229912699 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_0 = HighScores_get_Default_m4260063046(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		HighScores_t3229912699 * L_1 = V_0;
		NullCheck(L_1);
		HighScores_Save_m1918938851(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3355970408, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean HighScores::Load()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreList_t2288120898_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4030678255;
extern Il2CppCodeGenString* _stringLiteral449579327;
extern const uint32_t HighScores_Load_m3721535986_MetadataUsageId;
extern "C"  bool HighScores_Load_m3721535986 (HighScores_t3229912699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores_Load_m3721535986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryFormatter_t1866979105 * V_0 = NULL;
	FileStream_t1695958676 * V_1 = NULL;
	ScoreList_t2288120898 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		String_t* L_0 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get__filePath_1();
		bool L_1 = File_Exists_m1685968367(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		BinaryFormatter_t1866979105 * L_2 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		String_t* L_3 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get__filePath_1();
		FileStream_t1695958676 * L_4 = File_Open_m1543461971(NULL /*static, unused*/, L_3, 3, /*hidden argument*/NULL);
		V_1 = L_4;
		BinaryFormatter_t1866979105 * L_5 = V_0;
		FileStream_t1695958676 * L_6 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_7 = BinaryFormatter_Deserialize_m2771853471(L_5, L_6, /*hidden argument*/NULL);
		V_2 = ((ScoreList_t2288120898 *)CastclassClass(L_7, ScoreList_t2288120898_il2cpp_TypeInfo_var));
		FileStream_t1695958676 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_8);
		ScoreList_t2288120898 * L_9 = __this->get__highScores_2();
		ScoreList_t2288120898 * L_10 = V_2;
		NullCheck(L_10);
		ScoreU5BU5D_t100793071* L_11 = L_10->get_highScores_0();
		NullCheck(L_9);
		L_9->set_highScores_0(L_11);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4030678255, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0051:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral449579327, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void HighScores::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3983939685;
extern const uint32_t HighScores__cctor_m1276341525_MetadataUsageId;
extern "C"  void HighScores__cctor_m1276341525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HighScores__cctor_m1276341525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3983939685, /*hidden argument*/NULL);
		((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->set__filePath_1(L_1);
		return;
	}
}
// System.Void HIV::.ctor()
extern "C"  void HIV__ctor_m3468708502 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single HIV::get_radius()
extern "C"  float HIV_get_radius_m2282812811 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__radius_5();
		return L_0;
	}
}
// System.Void HIV::set_radius(System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_set_radius_m3968955256_MetadataUsageId;
extern "C"  void HIV_set_radius_m3968955256 (HIV_t2481767745 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_set_radius_m3968955256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	HIV_Part_t1695996321 * V_1 = NULL;
	HIV_PartU5BU5D_t327028412* V_2 = NULL;
	int32_t V_3 = 0;
	HIV_Part_t1695996321 * V_4 = NULL;
	HIV_PartU5BU5D_t327028412* V_5 = NULL;
	int32_t V_6 = 0;
	{
		float L_0 = ___value0;
		V_0 = ((float)((float)L_0/(float)(7.0f)));
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_2 = V_0;
		float L_3 = V_0;
		float L_4 = V_0;
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m2325460848(L_1, L_5, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_6 = __this->get_capsid_10();
		NullCheck(L_6);
		HIV_Piece_t1005595762 * L_7 = HIV_Part_get_script_m1909646023(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		HIV_Part_t1695996321 * L_9 = __this->get_capsid_10();
		NullCheck(L_9);
		HIV_Piece_t1005595762 * L_10 = HIV_Part_get_script_m1909646023(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m2325460848(L_11, L_12, /*hidden argument*/NULL);
	}

IL_004b:
	{
		HIV_Part_t1695996321 * L_13 = __this->get_membrane_12();
		NullCheck(L_13);
		HIV_Piece_t1005595762 * L_14 = HIV_Part_get_script_m1909646023(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007b;
		}
	}
	{
		HIV_Part_t1695996321 * L_16 = __this->get_membrane_12();
		NullCheck(L_16);
		HIV_Piece_t1005595762 * L_17 = HIV_Part_get_script_m1909646023(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localScale_m2325460848(L_18, L_19, /*hidden argument*/NULL);
	}

IL_007b:
	{
		HIV_Part_t1695996321 * L_20 = __this->get_matrix_11();
		NullCheck(L_20);
		HIV_Piece_t1005595762 * L_21 = HIV_Part_get_script_m1909646023(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ab;
		}
	}
	{
		HIV_Part_t1695996321 * L_23 = __this->get_matrix_11();
		NullCheck(L_23);
		HIV_Piece_t1005595762 * L_24 = HIV_Part_get_script_m1909646023(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_m2325460848(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		HIV_PartU5BU5D_t327028412* L_27 = __this->get_rna_13();
		V_2 = L_27;
		V_3 = 0;
		goto IL_00e7;
	}

IL_00b9:
	{
		HIV_PartU5BU5D_t327028412* L_28 = V_2;
		int32_t L_29 = V_3;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		HIV_Part_t1695996321 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		V_1 = L_31;
		HIV_Part_t1695996321 * L_32 = V_1;
		NullCheck(L_32);
		HIV_Piece_t1005595762 * L_33 = HIV_Part_get_script_m1909646023(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_34 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_33, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00e3;
		}
	}
	{
		HIV_Part_t1695996321 * L_35 = V_1;
		NullCheck(L_35);
		HIV_Piece_t1005595762 * L_36 = HIV_Part_get_script_m1909646023(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = Component_get_transform_m2697483695(L_36, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_localScale_m2325460848(L_37, L_38, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		int32_t L_39 = V_3;
		V_3 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00e7:
	{
		int32_t L_40 = V_3;
		HIV_PartU5BU5D_t327028412* L_41 = V_2;
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00b9;
		}
	}
	{
		HIV_PartU5BU5D_t327028412* L_42 = __this->get_rt_14();
		V_5 = L_42;
		V_6 = 0;
		goto IL_0135;
	}

IL_0100:
	{
		HIV_PartU5BU5D_t327028412* L_43 = V_5;
		int32_t L_44 = V_6;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		HIV_Part_t1695996321 * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		V_4 = L_46;
		HIV_Part_t1695996321 * L_47 = V_4;
		NullCheck(L_47);
		HIV_Piece_t1005595762 * L_48 = HIV_Part_get_script_m1909646023(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_48, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_012f;
		}
	}
	{
		HIV_Part_t1695996321 * L_50 = V_4;
		NullCheck(L_50);
		HIV_Piece_t1005595762 * L_51 = HIV_Part_get_script_m1909646023(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_localScale_m2325460848(L_52, L_53, /*hidden argument*/NULL);
	}

IL_012f:
	{
		int32_t L_54 = V_6;
		V_6 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0135:
	{
		int32_t L_55 = V_6;
		HIV_PartU5BU5D_t327028412* L_56 = V_5;
		NullCheck(L_56);
		if ((((int32_t)L_55) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length)))))))
		{
			goto IL_0100;
		}
	}
	{
		SpikeList_t1827939702 * L_57 = __this->get_spikeList_15();
		float L_58 = V_0;
		NullCheck(L_57);
		SpikeList_Scale_m1908817848(L_57, ((float)((float)(1.0f)/(float)L_58)), /*hidden argument*/NULL);
		float L_59 = ___value0;
		__this->set__radius_5(L_59);
		return;
	}
}
// System.Collections.Generic.List`1<Statistics> HIV::get_statList()
extern Il2CppClass* List_1_t1906498139_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2243433676_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1137681328_MethodInfo_var;
extern const uint32_t HIV_get_statList_m3052448253_MetadataUsageId;
extern "C"  List_1_t1906498139 * HIV_get_statList_m3052448253 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_get_statList_m3052448253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1906498139 * V_0 = NULL;
	{
		List_1_t1906498139 * L_0 = (List_1_t1906498139 *)il2cpp_codegen_object_new(List_1_t1906498139_il2cpp_TypeInfo_var);
		List_1__ctor_m2243433676(L_0, /*hidden argument*/List_1__ctor_m2243433676_MethodInfo_var);
		V_0 = L_0;
		List_1_t1906498139 * L_1 = V_0;
		HIV_Part_t1695996321 * L_2 = __this->get_capsid_10();
		NullCheck(L_2);
		HIV_Piece_t1005595762 * L_3 = HIV_Part_get_script_m1909646023(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Statistics_t2537377007  L_4 = HIV_Piece_get_stats_m2359098831(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m1137681328(L_1, L_4, /*hidden argument*/List_1_Add_m1137681328_MethodInfo_var);
		List_1_t1906498139 * L_5 = V_0;
		HIV_Part_t1695996321 * L_6 = __this->get_membrane_12();
		NullCheck(L_6);
		HIV_Piece_t1005595762 * L_7 = HIV_Part_get_script_m1909646023(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Statistics_t2537377007  L_8 = HIV_Piece_get_stats_m2359098831(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_Add_m1137681328(L_5, L_8, /*hidden argument*/List_1_Add_m1137681328_MethodInfo_var);
		List_1_t1906498139 * L_9 = V_0;
		HIV_Part_t1695996321 * L_10 = __this->get_matrix_11();
		NullCheck(L_10);
		HIV_Piece_t1005595762 * L_11 = HIV_Part_get_script_m1909646023(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Statistics_t2537377007  L_12 = HIV_Piece_get_stats_m2359098831(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m1137681328(L_9, L_12, /*hidden argument*/List_1_Add_m1137681328_MethodInfo_var);
		List_1_t1906498139 * L_13 = V_0;
		HIV_PartU5BU5D_t327028412* L_14 = __this->get_rna_13();
		NullCheck(L_14);
		int32_t L_15 = 0;
		HIV_Part_t1695996321 * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		HIV_Piece_t1005595762 * L_17 = HIV_Part_get_script_m1909646023(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Statistics_t2537377007  L_18 = HIV_Piece_get_stats_m2359098831(L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_Add_m1137681328(L_13, L_18, /*hidden argument*/List_1_Add_m1137681328_MethodInfo_var);
		List_1_t1906498139 * L_19 = V_0;
		HIV_PartU5BU5D_t327028412* L_20 = __this->get_rt_14();
		NullCheck(L_20);
		int32_t L_21 = 0;
		HIV_Part_t1695996321 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		HIV_Piece_t1005595762 * L_23 = HIV_Part_get_script_m1909646023(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Statistics_t2537377007  L_24 = HIV_Piece_get_stats_m2359098831(L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_Add_m1137681328(L_19, L_24, /*hidden argument*/List_1_Add_m1137681328_MethodInfo_var);
		List_1_t1906498139 * L_25 = V_0;
		return L_25;
	}
}
// HIVStats HIV::get_stats()
extern const MethodInfo* GameObject_GetComponent_TisHIVStats_t2521285894_m3513701493_MethodInfo_var;
extern const uint32_t HIV_get_stats_m4074649537_MetadataUsageId;
extern "C"  HIVStats_t2521285894 * HIV_get_stats_m4074649537 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_get_stats_m4074649537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		HIVStats_t2521285894 * L_1 = GameObject_GetComponent_TisHIVStats_t2521285894_m3513701493(L_0, /*hidden argument*/GameObject_GetComponent_TisHIVStats_t2521285894_m3513701493_MethodInfo_var);
		return L_1;
	}
}
// HIV/State HIV::get_state()
extern "C"  int32_t HIV_get_state_m3030343602 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CstateU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void HIV::set_state(HIV/State)
extern "C"  void HIV_set_state_m109767109 (HIV_t2481767745 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CstateU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean HIV::get_weak()
extern "C"  bool HIV_get_weak_m338777797 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__weakened_9();
		return L_0;
	}
}
// System.Void HIV::set_weak(System.Boolean)
extern "C"  void HIV_set_weak_m4184767996 (HIV_t2481767745 * __this, bool ___value0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Color_t2020392075  L_1 = Color_get_gray_m1396712533(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Color_t2020392075  L_2 = V_0;
		HIV_set_GP41_Color_m2193771811(__this, L_2, /*hidden argument*/NULL);
		Color_t2020392075  L_3 = V_0;
		V_0 = L_3;
		Color_t2020392075  L_4 = V_0;
		HIV_set_GP120_Color_Right_m3365475992(__this, L_4, /*hidden argument*/NULL);
		Color_t2020392075  L_5 = V_0;
		HIV_set_GP120_Color_Left_m2190792593(__this, L_5, /*hidden argument*/NULL);
	}

IL_0023:
	{
		bool L_6 = ___value0;
		__this->set__weakened_9(L_6);
		return;
	}
}
// UnityEngine.Color HIV::get_capsidColor()
extern "C"  Color_t2020392075  HIV_get_capsidColor_m4139347341 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_Part_t1695996321 * L_0 = __this->get_capsid_10();
		NullCheck(L_0);
		Color_t2020392075  L_1 = HIV_Part_get_color_m1608807173(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void HIV::set_capsidColor(UnityEngine.Color)
extern "C"  void HIV_set_capsidColor_m1174700546 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		HIV_Part_t1695996321 * L_0 = __this->get_capsid_10();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		HIV_Part_set_color_m3721088932(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color HIV::get_matrixColor()
extern "C"  Color_t2020392075  HIV_get_matrixColor_m1334576742 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_Part_t1695996321 * L_0 = __this->get_matrix_11();
		NullCheck(L_0);
		Color_t2020392075  L_1 = HIV_Part_get_color_m1608807173(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void HIV::set_matrixColor(UnityEngine.Color)
extern "C"  void HIV_set_matrixColor_m3277300941 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		HIV_Part_t1695996321 * L_0 = __this->get_matrix_11();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		HIV_Part_set_color_m3721088932(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color HIV::get_membraneColor()
extern "C"  Color_t2020392075  HIV_get_membraneColor_m1343741098 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_Part_t1695996321 * L_0 = __this->get_membrane_12();
		NullCheck(L_0);
		Color_t2020392075  L_1 = HIV_Part_get_color_m1608807173(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void HIV::set_membraneColor(UnityEngine.Color)
extern "C"  void HIV_set_membraneColor_m1240550689 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		HIV_Part_t1695996321 * L_0 = __this->get_membrane_12();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		HIV_Part_set_color_m3721088932(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color HIV::get_rnaColor()
extern "C"  Color_t2020392075  HIV_get_rnaColor_m3258134330 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_PartU5BU5D_t327028412* L_0 = __this->get_rna_13();
		NullCheck(L_0);
		int32_t L_1 = 0;
		HIV_Part_t1695996321 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		NullCheck(L_2);
		Color_t2020392075  L_3 = HIV_Part_get_color_m1608807173(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void HIV::set_rnaColor(UnityEngine.Color)
extern "C"  void HIV_set_rnaColor_m2965164103 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	HIV_Part_t1695996321 * V_0 = NULL;
	HIV_PartU5BU5D_t327028412* V_1 = NULL;
	int32_t V_2 = 0;
	{
		HIV_PartU5BU5D_t327028412* L_0 = __this->get_rna_13();
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		HIV_PartU5BU5D_t327028412* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		HIV_Part_t1695996321 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		HIV_Part_t1695996321 * L_5 = V_0;
		Color_t2020392075  L_6 = ___value0;
		NullCheck(L_5);
		HIV_Part_set_color_m3721088932(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_8 = V_2;
		HIV_PartU5BU5D_t327028412* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// UnityEngine.Color HIV::get_rtColor()
extern "C"  Color_t2020392075  HIV_get_rtColor_m3140351811 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_PartU5BU5D_t327028412* L_0 = __this->get_rt_14();
		NullCheck(L_0);
		int32_t L_1 = 0;
		HIV_Part_t1695996321 * L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		NullCheck(L_2);
		Color_t2020392075  L_3 = HIV_Part_get_color_m1608807173(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void HIV::set_rtColor(UnityEngine.Color)
extern "C"  void HIV_set_rtColor_m1723817340 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	HIV_Part_t1695996321 * V_0 = NULL;
	HIV_PartU5BU5D_t327028412* V_1 = NULL;
	int32_t V_2 = 0;
	{
		HIV_PartU5BU5D_t327028412* L_0 = __this->get_rt_14();
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		HIV_PartU5BU5D_t327028412* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		HIV_Part_t1695996321 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		HIV_Part_t1695996321 * L_5 = V_0;
		Color_t2020392075  L_6 = ___value0;
		NullCheck(L_5);
		HIV_Part_set_color_m3721088932(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_8 = V_2;
		HIV_PartU5BU5D_t327028412* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// UnityEngine.Color HIV::get_GP120_Color_Left()
extern const MethodInfo* List_1_get_Item_m2229255058_MethodInfo_var;
extern const uint32_t HIV_get_GP120_Color_Left_m1171295536_MetadataUsageId;
extern "C"  Color_t2020392075  HIV_get_GP120_Color_Left_m1171295536 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_get_GP120_Color_Left_m1171295536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		NullCheck(L_0);
		List_1_t3724643174 * L_1 = L_0->get_scriptList_4();
		NullCheck(L_1);
		Spike_t60554746 * L_2 = List_1_get_Item_m2229255058(L_1, 0, /*hidden argument*/List_1_get_Item_m2229255058_MethodInfo_var);
		NullCheck(L_2);
		GP120_t1512820336 * L_3 = Spike_get_gp120_m3432987073(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GP120_get_displayLeft_m2311060087(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Color_t2020392075  L_5 = SpriteRenderer_get_color_m345525162(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void HIV::set_GP120_Color_Left(UnityEngine.Color)
extern const MethodInfo* List_1_GetEnumerator_m3672601738_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2057571424_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1727075858_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1004702650_MethodInfo_var;
extern const uint32_t HIV_set_GP120_Color_Left_m2190792593_MetadataUsageId;
extern "C"  void HIV_set_GP120_Color_Left_m2190792593 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_set_GP120_Color_Left_m2190792593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	Enumerator_t3259372848  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		NullCheck(L_0);
		List_1_t3724643174 * L_1 = L_0->get_scriptList_4();
		NullCheck(L_1);
		Enumerator_t3259372848  L_2 = List_1_GetEnumerator_m3672601738(L_1, /*hidden argument*/List_1_GetEnumerator_m3672601738_MethodInfo_var);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0016:
		{
			Spike_t60554746 * L_3 = Enumerator_get_Current_m2057571424((&V_1), /*hidden argument*/Enumerator_get_Current_m2057571424_MethodInfo_var);
			V_0 = L_3;
			Spike_t60554746 * L_4 = V_0;
			NullCheck(L_4);
			GP120_t1512820336 * L_5 = Spike_get_gp120_m3432987073(L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			SpriteRenderer_t1209076198 * L_6 = GP120_get_displayLeft_m2311060087(L_5, /*hidden argument*/NULL);
			Color_t2020392075  L_7 = ___value0;
			NullCheck(L_6);
			SpriteRenderer_set_color_m2339931967(L_6, L_7, /*hidden argument*/NULL);
		}

IL_002f:
		{
			bool L_8 = Enumerator_MoveNext_m1727075858((&V_1), /*hidden argument*/Enumerator_MoveNext_m1727075858_MethodInfo_var);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1004702650((&V_1), /*hidden argument*/Enumerator_Dispose_m1004702650_MethodInfo_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004e:
	{
		return;
	}
}
// UnityEngine.Color HIV::get_GP120_Color_Right()
extern const MethodInfo* List_1_get_Item_m2229255058_MethodInfo_var;
extern const uint32_t HIV_get_GP120_Color_Right_m1095216545_MetadataUsageId;
extern "C"  Color_t2020392075  HIV_get_GP120_Color_Right_m1095216545 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_get_GP120_Color_Right_m1095216545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		NullCheck(L_0);
		List_1_t3724643174 * L_1 = L_0->get_scriptList_4();
		NullCheck(L_1);
		Spike_t60554746 * L_2 = List_1_get_Item_m2229255058(L_1, 0, /*hidden argument*/List_1_get_Item_m2229255058_MethodInfo_var);
		NullCheck(L_2);
		GP120_t1512820336 * L_3 = Spike_get_gp120_m3432987073(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GP120_get_displayRight_m461027194(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Color_t2020392075  L_5 = SpriteRenderer_get_color_m345525162(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void HIV::set_GP120_Color_Right(UnityEngine.Color)
extern const MethodInfo* List_1_GetEnumerator_m3672601738_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2057571424_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1727075858_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1004702650_MethodInfo_var;
extern const uint32_t HIV_set_GP120_Color_Right_m3365475992_MetadataUsageId;
extern "C"  void HIV_set_GP120_Color_Right_m3365475992 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_set_GP120_Color_Right_m3365475992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	Enumerator_t3259372848  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		NullCheck(L_0);
		List_1_t3724643174 * L_1 = L_0->get_scriptList_4();
		NullCheck(L_1);
		Enumerator_t3259372848  L_2 = List_1_GetEnumerator_m3672601738(L_1, /*hidden argument*/List_1_GetEnumerator_m3672601738_MethodInfo_var);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0016:
		{
			Spike_t60554746 * L_3 = Enumerator_get_Current_m2057571424((&V_1), /*hidden argument*/Enumerator_get_Current_m2057571424_MethodInfo_var);
			V_0 = L_3;
			Spike_t60554746 * L_4 = V_0;
			NullCheck(L_4);
			GP120_t1512820336 * L_5 = Spike_get_gp120_m3432987073(L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			SpriteRenderer_t1209076198 * L_6 = GP120_get_displayRight_m461027194(L_5, /*hidden argument*/NULL);
			Color_t2020392075  L_7 = ___value0;
			NullCheck(L_6);
			SpriteRenderer_set_color_m2339931967(L_6, L_7, /*hidden argument*/NULL);
		}

IL_002f:
		{
			bool L_8 = Enumerator_MoveNext_m1727075858((&V_1), /*hidden argument*/Enumerator_MoveNext_m1727075858_MethodInfo_var);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1004702650((&V_1), /*hidden argument*/Enumerator_Dispose_m1004702650_MethodInfo_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004e:
	{
		return;
	}
}
// UnityEngine.Color HIV::get_GP41_Color()
extern const MethodInfo* List_1_get_Item_m2229255058_MethodInfo_var;
extern const uint32_t HIV_get_GP41_Color_m4220143536_MetadataUsageId;
extern "C"  Color_t2020392075  HIV_get_GP41_Color_m4220143536 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_get_GP41_Color_m4220143536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		NullCheck(L_0);
		List_1_t3724643174 * L_1 = L_0->get_scriptList_4();
		NullCheck(L_1);
		Spike_t60554746 * L_2 = List_1_get_Item_m2229255058(L_1, 0, /*hidden argument*/List_1_get_Item_m2229255058_MethodInfo_var);
		NullCheck(L_2);
		GP41_t921954836 * L_3 = Spike_get_gp41_m2571159277(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GP41_get_spriteRender_m1828691819(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Color_t2020392075  L_5 = SpriteRenderer_get_color_m345525162(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void HIV::set_GP41_Color(UnityEngine.Color)
extern const MethodInfo* List_1_GetEnumerator_m3672601738_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2057571424_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1727075858_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1004702650_MethodInfo_var;
extern const uint32_t HIV_set_GP41_Color_m2193771811_MetadataUsageId;
extern "C"  void HIV_set_GP41_Color_m2193771811 (HIV_t2481767745 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_set_GP41_Color_m2193771811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	Enumerator_t3259372848  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		NullCheck(L_0);
		List_1_t3724643174 * L_1 = L_0->get_scriptList_4();
		NullCheck(L_1);
		Enumerator_t3259372848  L_2 = List_1_GetEnumerator_m3672601738(L_1, /*hidden argument*/List_1_GetEnumerator_m3672601738_MethodInfo_var);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_0016:
		{
			Spike_t60554746 * L_3 = Enumerator_get_Current_m2057571424((&V_1), /*hidden argument*/Enumerator_get_Current_m2057571424_MethodInfo_var);
			V_0 = L_3;
			Spike_t60554746 * L_4 = V_0;
			NullCheck(L_4);
			GP41_t921954836 * L_5 = Spike_get_gp41_m2571159277(L_4, /*hidden argument*/NULL);
			NullCheck(L_5);
			SpriteRenderer_t1209076198 * L_6 = GP41_get_spriteRender_m1828691819(L_5, /*hidden argument*/NULL);
			Color_t2020392075  L_7 = ___value0;
			NullCheck(L_6);
			SpriteRenderer_set_color_m2339931967(L_6, L_7, /*hidden argument*/NULL);
		}

IL_002f:
		{
			bool L_8 = Enumerator_MoveNext_m1727075858((&V_1), /*hidden argument*/Enumerator_MoveNext_m1727075858_MethodInfo_var);
			if (L_8)
			{
				goto IL_0016;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1004702650((&V_1), /*hidden argument*/Enumerator_Dispose_m1004702650_MethodInfo_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004e:
	{
		return;
	}
}
// System.Single HIV::get_alpha()
extern "C"  float HIV_get_alpha_m400990519 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = HIV_get_capsidColor_m4139347341(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_a_3();
		return L_1;
	}
}
// System.Void HIV::set_alpha(System.Single)
extern "C"  void HIV_set_alpha_m2192129518 (HIV_t2481767745 * __this, float ___value0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t2020392075  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t2020392075  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t2020392075  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t2020392075  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Color_t2020392075  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Color_t2020392075  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Color_t2020392075  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Color_t2020392075  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Color_t2020392075  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Color_t2020392075  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Color_t2020392075  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Color_t2020392075  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Color_t2020392075  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Color_t2020392075  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Color_t2020392075  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Color_t2020392075  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Color_t2020392075  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Color_t2020392075  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Color_t2020392075  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Color_t2020392075  V_22;
	memset(&V_22, 0, sizeof(V_22));
	Color_t2020392075  V_23;
	memset(&V_23, 0, sizeof(V_23));
	{
		Color_t2020392075  L_0 = HIV_get_capsidColor_m4139347341(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_r_0();
		Color_t2020392075  L_2 = HIV_get_capsidColor_m4139347341(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_g_1();
		Color_t2020392075  L_4 = HIV_get_capsidColor_m4139347341(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_b_2();
		float L_6 = ___value0;
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m1909920690(&L_7, L_1, L_3, L_5, L_6, /*hidden argument*/NULL);
		HIV_set_capsidColor_m1174700546(__this, L_7, /*hidden argument*/NULL);
		Color_t2020392075  L_8 = HIV_get_matrixColor_m1334576742(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_r_0();
		Color_t2020392075  L_10 = HIV_get_matrixColor_m1334576742(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = (&V_4)->get_g_1();
		Color_t2020392075  L_12 = HIV_get_matrixColor_m1334576742(__this, /*hidden argument*/NULL);
		V_5 = L_12;
		float L_13 = (&V_5)->get_b_2();
		float L_14 = ___value0;
		Color_t2020392075  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m1909920690(&L_15, L_9, L_11, L_13, L_14, /*hidden argument*/NULL);
		HIV_set_matrixColor_m3277300941(__this, L_15, /*hidden argument*/NULL);
		Color_t2020392075  L_16 = HIV_get_membraneColor_m1343741098(__this, /*hidden argument*/NULL);
		V_6 = L_16;
		float L_17 = (&V_6)->get_r_0();
		Color_t2020392075  L_18 = HIV_get_membraneColor_m1343741098(__this, /*hidden argument*/NULL);
		V_7 = L_18;
		float L_19 = (&V_7)->get_g_1();
		Color_t2020392075  L_20 = HIV_get_membraneColor_m1343741098(__this, /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = (&V_8)->get_b_2();
		float L_22 = ___value0;
		Color_t2020392075  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Color__ctor_m1909920690(&L_23, L_17, L_19, L_21, L_22, /*hidden argument*/NULL);
		HIV_set_membraneColor_m1240550689(__this, L_23, /*hidden argument*/NULL);
		Color_t2020392075  L_24 = HIV_get_rnaColor_m3258134330(__this, /*hidden argument*/NULL);
		V_9 = L_24;
		float L_25 = (&V_9)->get_r_0();
		Color_t2020392075  L_26 = HIV_get_rnaColor_m3258134330(__this, /*hidden argument*/NULL);
		V_10 = L_26;
		float L_27 = (&V_10)->get_g_1();
		Color_t2020392075  L_28 = HIV_get_rnaColor_m3258134330(__this, /*hidden argument*/NULL);
		V_11 = L_28;
		float L_29 = (&V_11)->get_b_2();
		float L_30 = ___value0;
		Color_t2020392075  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Color__ctor_m1909920690(&L_31, L_25, L_27, L_29, L_30, /*hidden argument*/NULL);
		HIV_set_rnaColor_m2965164103(__this, L_31, /*hidden argument*/NULL);
		Color_t2020392075  L_32 = HIV_get_rtColor_m3140351811(__this, /*hidden argument*/NULL);
		V_12 = L_32;
		float L_33 = (&V_12)->get_r_0();
		Color_t2020392075  L_34 = HIV_get_rtColor_m3140351811(__this, /*hidden argument*/NULL);
		V_13 = L_34;
		float L_35 = (&V_13)->get_g_1();
		Color_t2020392075  L_36 = HIV_get_rtColor_m3140351811(__this, /*hidden argument*/NULL);
		V_14 = L_36;
		float L_37 = (&V_14)->get_b_2();
		float L_38 = ___value0;
		Color_t2020392075  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Color__ctor_m1909920690(&L_39, L_33, L_35, L_37, L_38, /*hidden argument*/NULL);
		HIV_set_rtColor_m1723817340(__this, L_39, /*hidden argument*/NULL);
		Color_t2020392075  L_40 = HIV_get_GP120_Color_Right_m1095216545(__this, /*hidden argument*/NULL);
		V_15 = L_40;
		float L_41 = (&V_15)->get_r_0();
		Color_t2020392075  L_42 = HIV_get_GP120_Color_Right_m1095216545(__this, /*hidden argument*/NULL);
		V_16 = L_42;
		float L_43 = (&V_16)->get_g_1();
		Color_t2020392075  L_44 = HIV_get_GP120_Color_Right_m1095216545(__this, /*hidden argument*/NULL);
		V_17 = L_44;
		float L_45 = (&V_17)->get_b_2();
		float L_46 = ___value0;
		Color_t2020392075  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Color__ctor_m1909920690(&L_47, L_41, L_43, L_45, L_46, /*hidden argument*/NULL);
		HIV_set_GP120_Color_Left_m2190792593(__this, L_47, /*hidden argument*/NULL);
		Color_t2020392075  L_48 = HIV_get_GP120_Color_Right_m1095216545(__this, /*hidden argument*/NULL);
		V_18 = L_48;
		float L_49 = (&V_18)->get_r_0();
		Color_t2020392075  L_50 = HIV_get_GP120_Color_Right_m1095216545(__this, /*hidden argument*/NULL);
		V_19 = L_50;
		float L_51 = (&V_19)->get_g_1();
		Color_t2020392075  L_52 = HIV_get_GP120_Color_Right_m1095216545(__this, /*hidden argument*/NULL);
		V_20 = L_52;
		float L_53 = (&V_20)->get_b_2();
		float L_54 = ___value0;
		Color_t2020392075  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Color__ctor_m1909920690(&L_55, L_49, L_51, L_53, L_54, /*hidden argument*/NULL);
		HIV_set_GP120_Color_Right_m3365475992(__this, L_55, /*hidden argument*/NULL);
		Color_t2020392075  L_56 = HIV_get_GP41_Color_m4220143536(__this, /*hidden argument*/NULL);
		V_21 = L_56;
		float L_57 = (&V_21)->get_r_0();
		Color_t2020392075  L_58 = HIV_get_GP41_Color_m4220143536(__this, /*hidden argument*/NULL);
		V_22 = L_58;
		float L_59 = (&V_22)->get_g_1();
		Color_t2020392075  L_60 = HIV_get_GP41_Color_m4220143536(__this, /*hidden argument*/NULL);
		V_23 = L_60;
		float L_61 = (&V_23)->get_b_2();
		float L_62 = ___value0;
		Color_t2020392075  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Color__ctor_m1909920690(&L_63, L_57, L_59, L_61, L_62, /*hidden argument*/NULL);
		HIV_set_GP41_Color_m2193771811(__this, L_63, /*hidden argument*/NULL);
		return;
	}
}
// HIV_Move HIV::get_move()
extern "C"  HIV_Move_t1427226471 * HIV_get_move_m2343449320 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_Move_t1427226471 * L_0 = __this->get_U3CmoveU3Ek__BackingField_17();
		return L_0;
	}
}
// System.Void HIV::set_move(HIV_Move)
extern "C"  void HIV_set_move_m632598929 (HIV_t2481767745 * __this, HIV_Move_t1427226471 * ___value0, const MethodInfo* method)
{
	{
		HIV_Move_t1427226471 * L_0 = ___value0;
		__this->set_U3CmoveU3Ek__BackingField_17(L_0);
		return;
	}
}
// UnityEngine.GameObject HIV::GetSpike(System.Int32)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2229255058_MethodInfo_var;
extern const uint32_t HIV_GetSpike_m1198366128_MetadataUsageId;
extern "C"  GameObject_t1756533147 * HIV_GetSpike_m1198366128 (HIV_t2481767745 * __this, int32_t ___spikeNumber0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_GetSpike_m1198366128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___spikeNumber0;
		SpikeList_t1827939702 * L_1 = __this->get_spikeList_15();
		NullCheck(L_1);
		int32_t L_2 = SpikeList_get_count_m3219171817(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_3 = ___spikeNumber0;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		String_t* L_4 = ErrorCode_get_EA0OR_m2389741663(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1927440687 * L_5 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0023:
	{
		SpikeList_t1827939702 * L_6 = __this->get_spikeList_15();
		NullCheck(L_6);
		List_1_t3724643174 * L_7 = L_6->get_scriptList_4();
		int32_t L_8 = ___spikeNumber0;
		NullCheck(L_7);
		Spike_t60554746 * L_9 = List_1_get_Item_m2229255058(L_7, L_8, /*hidden argument*/List_1_get_Item_m2229255058_MethodInfo_var);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void HIV::ChangeType(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,Tropism)
extern "C"  void HIV_ChangeType_m3637960072 (HIV_t2481767745 * __this, Color_t2020392075  ___matrixColor0, Color_t2020392075  ___capsidColor1, Color_t2020392075  ___membraneColor2, Color_t2020392075  ___rnaColor3, Color_t2020392075  ___rtColor4, Tropism_t3662836552  ___t5, const MethodInfo* method)
{
	HIV_Part_t1695996321 * V_0 = NULL;
	HIV_PartU5BU5D_t327028412* V_1 = NULL;
	int32_t V_2 = 0;
	HIV_Part_t1695996321 * V_3 = NULL;
	HIV_PartU5BU5D_t327028412* V_4 = NULL;
	int32_t V_5 = 0;
	{
		HIV_Part_t1695996321 * L_0 = __this->get_matrix_11();
		Color_t2020392075  L_1 = ___matrixColor0;
		NullCheck(L_0);
		HIV_Part_set_color_m3721088932(L_0, L_1, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_2 = __this->get_capsid_10();
		Color_t2020392075  L_3 = ___capsidColor1;
		NullCheck(L_2);
		HIV_Part_set_color_m3721088932(L_2, L_3, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_4 = __this->get_membrane_12();
		Color_t2020392075  L_5 = ___membraneColor2;
		NullCheck(L_4);
		HIV_Part_set_color_m3721088932(L_4, L_5, /*hidden argument*/NULL);
		HIV_PartU5BU5D_t327028412* L_6 = __this->get_rna_13();
		V_1 = L_6;
		V_2 = 0;
		goto IL_0042;
	}

IL_0032:
	{
		HIV_PartU5BU5D_t327028412* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		HIV_Part_t1695996321 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		HIV_Part_t1695996321 * L_11 = V_0;
		Color_t2020392075  L_12 = ___rnaColor3;
		NullCheck(L_11);
		HIV_Part_set_color_m3721088932(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_2;
		HIV_PartU5BU5D_t327028412* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		HIV_PartU5BU5D_t327028412* L_16 = __this->get_rt_14();
		V_4 = L_16;
		V_5 = 0;
		goto IL_006f;
	}

IL_005b:
	{
		HIV_PartU5BU5D_t327028412* L_17 = V_4;
		int32_t L_18 = V_5;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		HIV_Part_t1695996321 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_3 = L_20;
		HIV_Part_t1695996321 * L_21 = V_3;
		Color_t2020392075  L_22 = ___rtColor4;
		NullCheck(L_21);
		HIV_Part_set_color_m3721088932(L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = V_5;
		V_5 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_24 = V_5;
		HIV_PartU5BU5D_t327028412* L_25 = V_4;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_005b;
		}
	}
	{
		SpikeList_t1827939702 * L_26 = __this->get_spikeList_15();
		Tropism_t3662836552  L_27 = ___t5;
		NullCheck(L_26);
		SpikeList_ChangeTropism_m419626291(L_26, L_27, /*hidden argument*/NULL);
		HIVStats_t2521285894 * L_28 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		List_1_t1906498139 * L_29 = HIV_get_statList_m3052448253(__this, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_30 = __this->get_matrix_11();
		NullCheck(L_30);
		HIV_Piece_t1005595762 * L_31 = HIV_Part_get_script_m1909646023(L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		HIVStats_RefreshStats_m3154914018(L_28, L_29, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::ChangeType()
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t HIV_ChangeType_m1447650064_MetadataUsageId;
extern "C"  void HIV_ChangeType_m1447650064 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_ChangeType_m1447650064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_1 = __this->get_capsid_10();
		NullCheck(L_1);
		Color_t2020392075  L_2 = HIV_Part_get_color_m1608807173(L_1, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_3 = __this->get_membrane_12();
		NullCheck(L_3);
		Color_t2020392075  L_4 = HIV_Part_get_color_m1608807173(L_3, /*hidden argument*/NULL);
		HIV_PartU5BU5D_t327028412* L_5 = __this->get_rna_13();
		NullCheck(L_5);
		int32_t L_6 = 0;
		HIV_Part_t1695996321 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Color_t2020392075  L_8 = HIV_Part_get_color_m1608807173(L_7, /*hidden argument*/NULL);
		HIV_PartU5BU5D_t327028412* L_9 = __this->get_rt_14();
		NullCheck(L_9);
		int32_t L_10 = 0;
		HIV_Part_t1695996321 * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Color_t2020392075  L_12 = HIV_Part_get_color_m1608807173(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Tropism_t3662836552  L_13 = Tropism_HIVRandom_m3105810886(NULL /*static, unused*/, /*hidden argument*/NULL);
		HIV_ChangeType_m3637960072(__this, L_0, L_2, L_4, L_8, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::Instantiate(System.Single)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494321980;
extern const uint32_t HIV_Instantiate_m1173439075_MetadataUsageId;
extern "C"  void HIV_Instantiate_m1173439075 (HIV_t2481767745 * __this, float ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Instantiate_m1173439075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		float L_0 = ___r0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ErrorCode_DisplayVariableCode_m2670461172(NULL /*static, unused*/, 0, _stringLiteral3494321980, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		float L_3 = ___r0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		float L_4 = HIV_get_radius_m2282812811(__this, /*hidden argument*/NULL);
		___r0 = L_4;
	}

IL_002f:
	{
		HIV_Part_t1695996321 * L_5 = __this->get_capsid_10();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_8 = V_0;
		NullCheck(L_5);
		HIV_Part_Instantiate_m519142666(L_5, L_7, L_8, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_9 = __this->get_matrix_11();
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_12 = V_0;
		NullCheck(L_9);
		HIV_Part_Instantiate_m519142666(L_9, L_11, L_12, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_13 = __this->get_membrane_12();
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_16 = V_0;
		NullCheck(L_13);
		HIV_Part_Instantiate_m519142666(L_13, L_15, L_16, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_00bf;
	}

IL_0093:
	{
		HIV_PartU5BU5D_t327028412* L_17 = __this->get_rna_13();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		HIV_Part_t1695996321 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m2638739322(&L_23, (0.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_22*(int32_t)2))-(int32_t)1))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_20);
		HIV_Part_Instantiate_m519142666(L_20, L_21, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_25 = V_1;
		HIV_PartU5BU5D_t327028412* L_26 = __this->get_rna_13();
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_0093;
		}
	}
	{
		V_2 = 0;
		goto IL_0100;
	}

IL_00d4:
	{
		HIV_PartU5BU5D_t327028412* L_27 = __this->get_rt_14();
		int32_t L_28 = V_2;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		HIV_Part_t1695996321 * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		Transform_t3275118058 * L_31 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		int32_t L_32 = V_2;
		Vector3_t2243707580  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m2638739322(&L_33, (0.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)2))-(int32_t)1))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		HIV_Part_Instantiate_m519142666(L_30, L_31, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_2;
		V_2 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_0100:
	{
		int32_t L_35 = V_2;
		HIV_PartU5BU5D_t327028412* L_36 = __this->get_rt_14();
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length)))))))
		{
			goto IL_00d4;
		}
	}
	{
		SpikeList_t1827939702 * L_37 = __this->get_spikeList_15();
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_39 = ___r0;
		NullCheck(L_37);
		SpikeList_Instantiate_m1789477637(L_37, L_38, L_39, /*hidden argument*/NULL);
		float L_40 = ___r0;
		HIV_set_radius_m3968955256(__this, L_40, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::Instantiate(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494321980;
extern const uint32_t HIV_Instantiate_m1277911629_MetadataUsageId;
extern "C"  void HIV_Instantiate_m1277911629 (HIV_t2481767745 * __this, Color_t2020392075  ___matrixColor0, Color_t2020392075  ___capsidColor1, Color_t2020392075  ___membraneColor2, Color_t2020392075  ___rnaColor3, Color_t2020392075  ___rtColor4, Color_t2020392075  ___GP120Color5, Color_t2020392075  ___GP41Color6, float ___r7, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Instantiate_m1277911629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		float L_0 = ___r7;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_1 = ErrorCode_DisplayVariableCode_m2670461172(NULL /*static, unused*/, 0, _stringLiteral3494321980, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		float L_3 = ___r7;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0031;
		}
	}
	{
		float L_4 = HIV_get_radius_m2282812811(__this, /*hidden argument*/NULL);
		___r7 = L_4;
	}

IL_0031:
	{
		HIV_Part_t1695996321 * L_5 = __this->get_capsid_10();
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		Color_t2020392075  L_8 = ___capsidColor1;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_9 = V_0;
		NullCheck(L_5);
		HIV_Part_Instantiate_m2085730656(L_5, L_7, L_8, L_9, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_10 = __this->get_matrix_11();
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		Color_t2020392075  L_13 = ___matrixColor0;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_14 = V_0;
		NullCheck(L_10);
		HIV_Part_Instantiate_m2085730656(L_10, L_12, L_13, L_14, /*hidden argument*/NULL);
		HIV_Part_t1695996321 * L_15 = __this->get_membrane_12();
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		Color_t2020392075  L_18 = ___membraneColor2;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_19 = V_0;
		NullCheck(L_15);
		HIV_Part_Instantiate_m2085730656(L_15, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_00c6;
	}

IL_0098:
	{
		HIV_PartU5BU5D_t327028412* L_20 = __this->get_rna_13();
		int32_t L_21 = V_1;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		HIV_Part_t1695996321 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_25 = ___rnaColor3;
		int32_t L_26 = V_1;
		Vector3_t2243707580  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2638739322(&L_27, (0.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_26*(int32_t)2))-(int32_t)1))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		HIV_Part_Instantiate_m2085730656(L_23, L_24, L_25, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00c6:
	{
		int32_t L_29 = V_1;
		HIV_PartU5BU5D_t327028412* L_30 = __this->get_rna_13();
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_0098;
		}
	}
	{
		V_2 = 0;
		goto IL_0109;
	}

IL_00db:
	{
		HIV_PartU5BU5D_t327028412* L_31 = __this->get_rt_14();
		int32_t L_32 = V_2;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		HIV_Part_t1695996321 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		Transform_t3275118058 * L_35 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_36 = ___rtColor4;
		int32_t L_37 = V_2;
		Vector3_t2243707580  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector3__ctor_m2638739322(&L_38, (0.0f), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_37*(int32_t)2))-(int32_t)1))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_34);
		HIV_Part_Instantiate_m2085730656(L_34, L_35, L_36, L_38, /*hidden argument*/NULL);
		int32_t L_39 = V_2;
		V_2 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0109:
	{
		int32_t L_40 = V_2;
		HIV_PartU5BU5D_t327028412* L_41 = __this->get_rt_14();
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00db;
		}
	}
	{
		SpikeList_t1827939702 * L_42 = __this->get_spikeList_15();
		Transform_t3275118058 * L_43 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_44 = ___r7;
		NullCheck(L_42);
		SpikeList_Instantiate_m1789477637(L_42, L_43, L_44, /*hidden argument*/NULL);
		float L_45 = ___r7;
		HIV_set_radius_m3968955256(__this, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::ChangeState(HIV/State,UnityEngine.GameObject)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m300762504_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisHIVCollision_t2736474671_m1265056740_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_t1815110036_m1633460177_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3672601738_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2057571424_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1727075858_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1004702650_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1034633038;
extern Il2CppCodeGenString* _stringLiteral3209846263;
extern const uint32_t HIV_ChangeState_m1698492408_MetadataUsageId;
extern "C"  void HIV_ChangeState_m1698492408 (HIV_t2481767745 * __this, int32_t ___s0, GameObject_t1756533147 * ___g1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_ChangeState_m1698492408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CircleCollider2D_t13116344 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Spike_t60554746 * V_9 = NULL;
	Enumerator_t3259372848  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2068941383 * L_1 = __this->get_stateList_7();
		int32_t L_2 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		HIV_State_t57600565 * L_3 = Dictionary_2_get_Item_m300762504(L_1, L_2, /*hidden argument*/Dictionary_2_get_Item_m300762504_MethodInfo_var);
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		HIVCollision_t2736474671 * L_5 = GameObject_GetComponentInChildren_TisHIVCollision_t2736474671_m1265056740(L_4, /*hidden argument*/GameObject_GetComponentInChildren_TisHIVCollision_t2736474671_m1265056740_MethodInfo_var);
		NullCheck(L_5);
		CircleCollider2D_t13116344 * L_6 = Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442(L_5, /*hidden argument*/Component_GetComponent_TisCircleCollider2D_t13116344_m1279094442_MethodInfo_var);
		V_0 = L_6;
		int32_t L_7 = ___s0;
		if (L_7 == 0)
		{
			goto IL_00db;
		}
		if (L_7 == 1)
		{
			goto IL_0057;
		}
		if (L_7 == 2)
		{
			goto IL_0160;
		}
		if (L_7 == 3)
		{
			goto IL_01d1;
		}
		if (L_7 == 4)
		{
			goto IL_01d1;
		}
		if (L_7 == 5)
		{
			goto IL_0264;
		}
	}
	{
		goto IL_02e2;
	}

IL_0057:
	{
		CircleCollider2D_t13116344 * L_8 = V_0;
		NullCheck(L_8);
		Behaviour_set_enabled_m1796096907(L_8, (bool)1, /*hidden argument*/NULL);
		CircleCollider2D_t13116344 * L_9 = V_0;
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(L_9, /*hidden argument*/NULL);
		int32_t L_11 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, _stringLiteral1034633038, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_set_layer_m2712461877(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_sparkle_16();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
		HIVStats_t2521285894 * L_13 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009b;
		}
	}
	{
		HIVStats_t2521285894 * L_15 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_dying_4((bool)1);
	}

IL_009b:
	{
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		float L_19 = (&V_1)->get_x_1();
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		float L_22 = (&V_2)->get_y_2();
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m2638739322(&L_23, L_19, L_22, (-1.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_m2469242620(L_16, L_23, /*hidden argument*/NULL);
		goto IL_02e2;
	}

IL_00db:
	{
		CircleCollider2D_t13116344 * L_24 = V_0;
		NullCheck(L_24);
		Behaviour_set_enabled_m1796096907(L_24, (bool)1, /*hidden argument*/NULL);
		CircleCollider2D_t13116344 * L_25 = V_0;
		NullCheck(L_25);
		GameObject_t1756533147 * L_26 = Component_get_gameObject_m3105766835(L_25, /*hidden argument*/NULL);
		int32_t L_27 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, _stringLiteral3209846263, /*hidden argument*/NULL);
		NullCheck(L_26);
		GameObject_set_layer_m2712461877(L_26, L_27, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = __this->get_sparkle_16();
		NullCheck(L_28);
		GameObject_SetActive_m2887581199(L_28, (bool)0, /*hidden argument*/NULL);
		HIVStats_t2521285894 * L_29 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_011f;
		}
	}
	{
		HIVStats_t2521285894 * L_31 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_dying_4((bool)1);
	}

IL_011f:
	{
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_33 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_position_m1104419803(L_33, /*hidden argument*/NULL);
		V_3 = L_34;
		float L_35 = (&V_3)->get_x_1();
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_position_m1104419803(L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		float L_38 = (&V_4)->get_y_2();
		Vector3_t2243707580  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector3__ctor_m2638739322(&L_39, L_35, L_38, (-1.0f), /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_set_position_m2469242620(L_32, L_39, /*hidden argument*/NULL);
		goto IL_02e2;
	}

IL_0160:
	{
		CircleCollider2D_t13116344 * L_40 = V_0;
		NullCheck(L_40);
		Behaviour_set_enabled_m1796096907(L_40, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_41 = __this->get_sparkle_16();
		NullCheck(L_41);
		GameObject_SetActive_m2887581199(L_41, (bool)0, /*hidden argument*/NULL);
		HIVStats_t2521285894 * L_42 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_018f;
		}
	}
	{
		HIVStats_t2521285894 * L_44 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		L_44->set_dying_4((bool)1);
	}

IL_018f:
	{
		Transform_t3275118058 * L_45 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_46 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t2243707580  L_47 = Transform_get_position_m1104419803(L_46, /*hidden argument*/NULL);
		V_5 = L_47;
		float L_48 = (&V_5)->get_x_1();
		Transform_t3275118058 * L_49 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		Vector3_t2243707580  L_50 = Transform_get_position_m1104419803(L_49, /*hidden argument*/NULL);
		V_6 = L_50;
		float L_51 = (&V_6)->get_y_2();
		Vector3_t2243707580  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector3__ctor_m2638739322(&L_52, L_48, L_51, (-1.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_position_m2469242620(L_45, L_52, /*hidden argument*/NULL);
		goto IL_02e2;
	}

IL_01d1:
	{
		GameObject_t1756533147 * L_53 = ___g1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01f3;
		}
	}
	{
		Dictionary_2_t2068941383 * L_55 = __this->get_stateList_7();
		int32_t L_56 = ___s0;
		NullCheck(L_55);
		HIV_State_t57600565 * L_57 = Dictionary_2_get_Item_m300762504(L_55, L_56, /*hidden argument*/Dictionary_2_get_Item_m300762504_MethodInfo_var);
		GameObject_t1756533147 * L_58 = ___g1;
		NullCheck(L_58);
		TCell_t1815110036 * L_59 = GameObject_GetComponent_TisTCell_t1815110036_m1633460177(L_58, /*hidden argument*/GameObject_GetComponent_TisTCell_t1815110036_m1633460177_MethodInfo_var);
		NullCheck(L_57);
		L_57->set_host_3(L_59);
	}

IL_01f3:
	{
		CircleCollider2D_t13116344 * L_60 = V_0;
		NullCheck(L_60);
		Behaviour_set_enabled_m1796096907(L_60, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_61 = __this->get_sparkle_16();
		NullCheck(L_61);
		GameObject_SetActive_m2887581199(L_61, (bool)0, /*hidden argument*/NULL);
		HIVStats_t2521285894 * L_62 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0222;
		}
	}
	{
		HIVStats_t2521285894 * L_64 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		L_64->set_dying_4((bool)0);
	}

IL_0222:
	{
		Transform_t3275118058 * L_65 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_66 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_66);
		Vector3_t2243707580  L_67 = Transform_get_position_m1104419803(L_66, /*hidden argument*/NULL);
		V_7 = L_67;
		float L_68 = (&V_7)->get_x_1();
		Transform_t3275118058 * L_69 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_69);
		Vector3_t2243707580  L_70 = Transform_get_position_m1104419803(L_69, /*hidden argument*/NULL);
		V_8 = L_70;
		float L_71 = (&V_8)->get_y_2();
		Vector3_t2243707580  L_72;
		memset(&L_72, 0, sizeof(L_72));
		Vector3__ctor_m2638739322(&L_72, L_68, L_71, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_set_position_m2469242620(L_65, L_72, /*hidden argument*/NULL);
		goto IL_02e2;
	}

IL_0264:
	{
		HIV_Move_t1427226471 * L_73 = HIV_get_move_m2343449320(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_74 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_73);
		ObjectMovement2D_set_velocity_m1559477545(L_73, L_74, /*hidden argument*/NULL);
		CircleCollider2D_t13116344 * L_75 = V_0;
		NullCheck(L_75);
		Behaviour_set_enabled_m1796096907(L_75, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_76 = __this->get_sparkle_16();
		NullCheck(L_76);
		GameObject_SetActive_m2887581199(L_76, (bool)0, /*hidden argument*/NULL);
		SpikeList_t1827939702 * L_77 = __this->get_spikeList_15();
		NullCheck(L_77);
		List_1_t3724643174 * L_78 = L_77->get_scriptList_4();
		NullCheck(L_78);
		Enumerator_t3259372848  L_79 = List_1_GetEnumerator_m3672601738(L_78, /*hidden argument*/List_1_GetEnumerator_m3672601738_MethodInfo_var);
		V_10 = L_79;
	}

IL_0299:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02be;
		}

IL_029e:
		{
			Spike_t60554746 * L_80 = Enumerator_get_Current_m2057571424((&V_10), /*hidden argument*/Enumerator_get_Current_m2057571424_MethodInfo_var);
			V_9 = L_80;
			Spike_t60554746 * L_81 = V_9;
			NullCheck(L_81);
			GP120_t1512820336 * L_82 = Spike_get_gp120_m3432987073(L_81, /*hidden argument*/NULL);
			NullCheck(L_82);
			ParticleSystem_t3394631041 * L_83 = GP120_get_particles_m948954212(L_82, /*hidden argument*/NULL);
			NullCheck(L_83);
			GameObject_t1756533147 * L_84 = Component_get_gameObject_m3105766835(L_83, /*hidden argument*/NULL);
			NullCheck(L_84);
			GameObject_SetActive_m2887581199(L_84, (bool)0, /*hidden argument*/NULL);
		}

IL_02be:
		{
			bool L_85 = Enumerator_MoveNext_m1727075858((&V_10), /*hidden argument*/Enumerator_MoveNext_m1727075858_MethodInfo_var);
			if (L_85)
			{
				goto IL_029e;
			}
		}

IL_02ca:
		{
			IL2CPP_LEAVE(0x2DD, FINALLY_02cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_02cf;
	}

FINALLY_02cf:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1004702650((&V_10), /*hidden argument*/Enumerator_Dispose_m1004702650_MethodInfo_var);
		IL2CPP_END_FINALLY(719)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(719)
	{
		IL2CPP_JUMP_TBL(0x2DD, IL_02dd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02dd:
	{
		goto IL_02e2;
	}

IL_02e2:
	{
		int32_t L_86 = ___s0;
		HIV_set_state_m109767109(__this, L_86, /*hidden argument*/NULL);
		int32_t L_87 = ___s0;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_0307;
		}
	}
	{
		Dictionary_2_t2068941383 * L_88 = __this->get_stateList_7();
		int32_t L_89 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		NullCheck(L_88);
		HIV_State_t57600565 * L_90 = Dictionary_2_get_Item_m300762504(L_88, L_89, /*hidden argument*/Dictionary_2_get_Item_m300762504_MethodInfo_var);
		NullCheck(L_90);
		Behaviour_set_enabled_m1796096907(L_90, (bool)1, /*hidden argument*/NULL);
	}

IL_0307:
	{
		return;
	}
}
// System.Boolean HIV::CheckStateDying()
extern "C"  bool HIV_CheckStateDying_m788117094 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 1;
	}

IL_0023:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 HIV::GetSpikeNumber(Spike)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_GetSpikeNumber_m1961596675_MetadataUsageId;
extern "C"  int32_t HIV_GetSpikeNumber_m1961596675 (HIV_t2481767745 * __this, Spike_t60554746 * ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_GetSpikeNumber_m1961596675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0024;
	}

IL_0007:
	{
		SpikeList_t1827939702 * L_0 = __this->get_spikeList_15();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Spike_t60554746 * L_2 = SpikeList_get_Item_m546146431(L_0, L_1, /*hidden argument*/NULL);
		Spike_t60554746 * L_3 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_5 = V_0;
		return L_5;
	}

IL_0020:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_7 = V_0;
		SpikeList_t1827939702 * L_8 = __this->get_spikeList_15();
		NullCheck(L_8);
		int32_t L_9 = SpikeList_get_count_m3219171817(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// System.Void HIV::Awake()
extern Il2CppClass* Dictionary_2_t2068941383_il2cpp_TypeInfo_var;
extern Il2CppClass* FocalPoint_t1860633653_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Move_t1427226471_m1579293678_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1914274686_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Bud_t370745349_m2600086220_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2756113830_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Infect_t1458631357_m1679974338_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Selected_t1107388507_m966267950_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Return_t3773441690_m764620261_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Wander_t1816073829_m2551872058_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_Death_t750143594_m1267458579_MethodInfo_var;
extern const uint32_t HIV_Awake_m696259965_MetadataUsageId;
extern "C"  void HIV_Awake_m696259965 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Awake_m696259965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2068941383 * V_0 = NULL;
	HIV_t2481767745 * G_B2_0 = NULL;
	HIV_t2481767745 * G_B1_0 = NULL;
	GameObject_t1756533147 * G_B3_0 = NULL;
	HIV_t2481767745 * G_B3_1 = NULL;
	{
		HIV_Move_t1427226471 * L_0 = Component_GetComponent_TisHIV_Move_t1427226471_m1579293678(__this, /*hidden argument*/Component_GetComponent_TisHIV_Move_t1427226471_m1579293678_MethodInfo_var);
		HIV_set_move_m632598929(__this, L_0, /*hidden argument*/NULL);
		Dictionary_2_t2068941383 * L_1 = (Dictionary_2_t2068941383 *)il2cpp_codegen_object_new(Dictionary_2_t2068941383_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1914274686(L_1, /*hidden argument*/Dictionary_2__ctor_m1914274686_MethodInfo_var);
		V_0 = L_1;
		Dictionary_2_t2068941383 * L_2 = V_0;
		HIV_Bud_t370745349 * L_3 = Component_GetComponent_TisHIV_Bud_t370745349_m2600086220(__this, /*hidden argument*/Component_GetComponent_TisHIV_Bud_t370745349_m2600086220_MethodInfo_var);
		NullCheck(L_2);
		Dictionary_2_Add_m2756113830(L_2, 4, L_3, /*hidden argument*/Dictionary_2_Add_m2756113830_MethodInfo_var);
		Dictionary_2_t2068941383 * L_4 = V_0;
		HIV_Infect_t1458631357 * L_5 = Component_GetComponent_TisHIV_Infect_t1458631357_m1679974338(__this, /*hidden argument*/Component_GetComponent_TisHIV_Infect_t1458631357_m1679974338_MethodInfo_var);
		NullCheck(L_4);
		Dictionary_2_Add_m2756113830(L_4, 3, L_5, /*hidden argument*/Dictionary_2_Add_m2756113830_MethodInfo_var);
		Dictionary_2_t2068941383 * L_6 = V_0;
		HIV_Selected_t1107388507 * L_7 = Component_GetComponent_TisHIV_Selected_t1107388507_m966267950(__this, /*hidden argument*/Component_GetComponent_TisHIV_Selected_t1107388507_m966267950_MethodInfo_var);
		NullCheck(L_6);
		Dictionary_2_Add_m2756113830(L_6, 1, L_7, /*hidden argument*/Dictionary_2_Add_m2756113830_MethodInfo_var);
		Dictionary_2_t2068941383 * L_8 = V_0;
		HIV_Return_t3773441690 * L_9 = Component_GetComponent_TisHIV_Return_t3773441690_m764620261(__this, /*hidden argument*/Component_GetComponent_TisHIV_Return_t3773441690_m764620261_MethodInfo_var);
		NullCheck(L_8);
		Dictionary_2_Add_m2756113830(L_8, 2, L_9, /*hidden argument*/Dictionary_2_Add_m2756113830_MethodInfo_var);
		Dictionary_2_t2068941383 * L_10 = V_0;
		HIV_Wander_t1816073829 * L_11 = Component_GetComponent_TisHIV_Wander_t1816073829_m2551872058(__this, /*hidden argument*/Component_GetComponent_TisHIV_Wander_t1816073829_m2551872058_MethodInfo_var);
		NullCheck(L_10);
		Dictionary_2_Add_m2756113830(L_10, 0, L_11, /*hidden argument*/Dictionary_2_Add_m2756113830_MethodInfo_var);
		Dictionary_2_t2068941383 * L_12 = V_0;
		HIV_Death_t750143594 * L_13 = Component_GetComponent_TisHIV_Death_t750143594_m1267458579(__this, /*hidden argument*/Component_GetComponent_TisHIV_Death_t750143594_m1267458579_MethodInfo_var);
		NullCheck(L_12);
		Dictionary_2_Add_m2756113830(L_12, 5, L_13, /*hidden argument*/Dictionary_2_Add_m2756113830_MethodInfo_var);
		Dictionary_2_t2068941383 * L_14 = V_0;
		__this->set_stateList_7(L_14);
		FocalPoint_t1860633653 * L_15 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_16)
		{
			G_B2_0 = __this;
			goto IL_0086;
		}
	}
	{
		FocalPoint_t1860633653 * L_17 = ((FocalPoint_t1860633653_StaticFields*)FocalPoint_t1860633653_il2cpp_TypeInfo_var->static_fields)->get_main_7();
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(L_17, /*hidden argument*/NULL);
		G_B3_0 = L_18;
		G_B3_1 = G_B1_0;
		goto IL_0087;
	}

IL_0086:
	{
		G_B3_0 = ((GameObject_t1756533147 *)(NULL));
		G_B3_1 = G_B2_0;
	}

IL_0087:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_focalPoint_3(G_B3_0);
		GameObject_t1756533147 * L_19 = __this->get_focalPoint_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00a3;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00a3:
	{
		return;
	}
}
// System.Void HIV::Start()
extern const MethodInfo* Dictionary_2_GetEnumerator_m2946250960_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3979652818_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1401288323_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2718372185_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m660894727_MethodInfo_var;
extern const uint32_t HIV_Start_m361889862_MetadataUsageId;
extern "C"  void HIV_Start_m361889862 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Start_m361889862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t4121253901  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3388966085  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2068941383 * L_0 = __this->get_stateList_7();
		NullCheck(L_0);
		Enumerator_t3388966085  L_1 = Dictionary_2_GetEnumerator_m2946250960(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2946250960_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			KeyValuePair_2_t4121253901  L_2 = Enumerator_get_Current_m3979652818((&V_1), /*hidden argument*/Enumerator_get_Current_m3979652818_MethodInfo_var);
			V_0 = L_2;
			HIV_State_t57600565 * L_3 = KeyValuePair_2_get_Value_m1401288323((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1401288323_MethodInfo_var);
			NullCheck(L_3);
			Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
		}

IL_0026:
		{
			bool L_4 = Enumerator_MoveNext_m2718372185((&V_1), /*hidden argument*/Enumerator_MoveNext_m2718372185_MethodInfo_var);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x45, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m660894727((&V_1), /*hidden argument*/Enumerator_Dispose_m660894727_MethodInfo_var);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0045:
	{
		HIV_ChangeState_m1698492408(__this, 4, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::OnEnable()
extern "C"  void HIV_OnEnable_m3635316966 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_set_weak_m4184767996(__this, (bool)0, /*hidden argument*/NULL);
		HIV_ChangeState_m1698492408(__this, 4, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::OnDisable()
extern "C"  void HIV_OnDisable_m1323319365 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		HIV_set_weak_m4184767996(__this, (bool)0, /*hidden argument*/NULL);
		HIV_ChangeState_m1698492408(__this, 4, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV::LateUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_LateUpdate_m2480241205_MetadataUsageId;
extern "C"  void HIV_LateUpdate_m2480241205 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_LateUpdate_m2480241205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIVStats_t2521285894 * L_0 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		HIVStats_t2521285894 * L_2 = HIV_get_stats_m4074649537(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = HIVStats_get_alive_m2021401439(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_4 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)5)))
		{
			goto IL_0034;
		}
	}
	{
		HIV_ChangeState_m1698492408(__this, 5, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void HIV::FixedUpdate()
extern "C"  void HIV_FixedUpdate_m2467446167 (HIV_t2481767745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = HIV_get_weak_m338777797(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_1 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = HIV_get_state_m3030343602(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_003c;
		}
	}

IL_0022:
	{
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_Rotate_m4255273365(L_3, (0.0f), (0.0f), (-1.5f), /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void HIV_Bud::.ctor()
extern "C"  void HIV_Bud__ctor_m2460691438 (HIV_Bud_t370745349 * __this, const MethodInfo* method)
{
	{
		HIV_State__ctor_m1425936220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Bud::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioDatabase_t2585396589_m217869328_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743623852;
extern const uint32_t HIV_Bud_OnEnable_m3457259798_MetadataUsageId;
extern "C"  void HIV_Bud_OnEnable_m3457259798 (HIV_Bud_t370745349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Bud_OnEnable_m3457259798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Tropism_t3662836552  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		TCell_t1815110036 * L_0 = ((HIV_State_t57600565 *)__this)->get_host_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ca;
		}
	}
	{
		HIV_t2481767745 * L_2 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_2);
		HIV_Move_t1427226471 * L_3 = HIV_get_move_m2343449320(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_3, (Transform_t3275118058 *)NULL, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		HIV_t2481767745 * L_4 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_4);
		HIV_Move_t1427226471 * L_5 = HIV_get_move_m2343449320(L_4, /*hidden argument*/NULL);
		int32_t L_6 = Random_Range_m694320887(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_7 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (((float)((float)L_6))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_get_up_m977201173(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		HIV_t2481767745 * L_11 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_11);
		HIVStats_t2521285894 * L_12 = HIV_get_stats_m4074649537(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Statistics_t2537377007  L_13 = HIVStats_get_stats_m1941178495(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		float L_14 = Statistics_get_Item_m3520282030((&V_0), 0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_10, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_5);
		ObjectMovement2D_set_velocity_m1559477545(L_5, L_16, /*hidden argument*/NULL);
		HIV_t2481767745 * L_17 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = HIV_GetSpike_m1198366128(L_17, 0, /*hidden argument*/NULL);
		NullCheck(L_18);
		Spike_t60554746 * L_19 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_18, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		NullCheck(L_19);
		Tropism_t3662836552  L_20 = Spike_get_tropism_m1909874245(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = (&V_1)->get_coreceptor_11();
		if ((!(((uint32_t)L_21) == ((uint32_t)4))))
		{
			goto IL_00be;
		}
	}
	{
		HIV_t2481767745 * L_22 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_22);
		AudioDatabase_t2585396589 * L_23 = Component_GetComponent_TisAudioDatabase_t2585396589_m217869328(L_22, /*hidden argument*/Component_GetComponent_TisAudioDatabase_t2585396589_m217869328_MethodInfo_var);
		NullCheck(L_23);
		AudioDatabase_Play_m2837110456(L_23, _stringLiteral1743623852, /*hidden argument*/NULL);
	}

IL_00be:
	{
		HIV_t2481767745 * L_24 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_24);
		L_24->set_grouped_4((bool)0);
	}

IL_00ca:
	{
		return;
	}
}
// System.Void HIV_Bud::FixedUpdate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_Bud_FixedUpdate_m1596422867_MetadataUsageId;
extern "C"  void HIV_Bud_FixedUpdate_m1596422867 (HIV_Bud_t370745349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Bud_FixedUpdate_m1596422867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TCell_t1815110036 * L_0 = ((HIV_State_t57600565 *)__this)->get_host_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005f;
		}
	}
	{
		TCell_t1815110036 * L_2 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m313590879(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005f;
		}
	}
	{
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		TCell_t1815110036 * L_8 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		float L_12 = Vector2_Distance_m280750759(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		TCell_t1815110036 * L_13 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_13);
		float L_14 = TCell_get_radius_m399111662(L_13, /*hidden argument*/NULL);
		if ((!(((float)L_12) > ((float)L_14))))
		{
			goto IL_006c;
		}
	}

IL_005f:
	{
		HIV_t2481767745 * L_15 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_15);
		HIV_ChangeState_m1698492408(L_15, 2, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void HIV_Death::.ctor()
extern "C"  void HIV_Death__ctor_m3815110599 (HIV_Death_t750143594 * __this, const MethodInfo* method)
{
	{
		HIV_State__ctor_m1425936220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Death::Die()
extern "C"  void HIV_Death_Die_m3759854903 (HIV_Death_t750143594 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		HIV_set_alpha_m2192129518(L_0, (1.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Death::OnEnable()
extern Il2CppCodeGenString* _stringLiteral339798784;
extern const uint32_t HIV_Death_OnEnable_m3110694267_MetadataUsageId;
extern "C"  void HIV_Death_OnEnable_m3110694267 (HIV_Death_t750143594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Death_OnEnable_m3110694267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_alpha_5((1.0f));
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		HIV_set_alpha_m2192129518(L_0, (1.0f), /*hidden argument*/NULL);
		Animator_t69676727 * L_1 = __this->get_anim_4();
		NullCheck(L_1);
		Animator_SetTrigger_m3418492570(L_1, _stringLiteral339798784, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Death::Update()
extern "C"  void HIV_Death_Update_m2607022356 (HIV_Death_t750143594 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		float L_1 = __this->get_alpha_5();
		NullCheck(L_0);
		HIV_set_alpha_m2192129518(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Glow::.ctor()
extern "C"  void HIV_Glow__ctor_m3588245212 (HIV_Glow_t2038930725 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Glow::Start()
extern "C"  void HIV_Glow_Start_m733136548 (HIV_Glow_t2038930725 * __this, const MethodInfo* method)
{
	{
		__this->set_t_5((0.0f));
		float L_0 = __this->get_radiusMax_12();
		float L_1 = __this->get_radiusMin_11();
		__this->set_radiusMedium_7(((float)((float)((float)((float)L_0+(float)L_1))/(float)(2.0f))));
		bool L_2 = __this->get_radiusChange_10();
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		float L_3 = __this->get_radiusMedium_7();
		__this->set_radius_3(L_3);
		goto IL_004c;
	}

IL_0040:
	{
		float L_4 = __this->get_radiusMax_12();
		__this->set_radius_3(L_4);
	}

IL_004c:
	{
		bool L_5 = __this->get_alphaChange_13();
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		__this->set_alpha_4((0.5f));
		goto IL_0072;
	}

IL_0067:
	{
		__this->set_alpha_4((1.0f));
	}

IL_0072:
	{
		return;
	}
}
// System.Void HIV_Glow::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t HIV_Glow_Update_m2677857741_MetadataUsageId;
extern "C"  void HIV_Glow_Update_m2677857741 (HIV_Glow_t2038930725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Glow_Update_m2677857741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B2_0 = 0.0f;
	HIV_Glow_t2038930725 * G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	HIV_Glow_t2038930725 * G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	HIV_Glow_t2038930725 * G_B3_2 = NULL;
	{
		float L_0 = __this->get_t_5();
		bool L_1 = __this->get_up_6();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (!L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001c;
		}
	}
	{
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0022;
	}

IL_001c:
	{
		float L_3 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((-L_3));
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0022:
	{
		NullCheck(G_B3_2);
		G_B3_2->set_t_5(((float)((float)G_B3_1+(float)G_B3_0)));
		bool L_4 = __this->get_up_6();
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		float L_5 = __this->get_t_5();
		float L_6 = __this->get_timeDilation_9();
		if ((((float)L_5) >= ((float)L_6)))
		{
			goto IL_0061;
		}
	}

IL_0044:
	{
		bool L_7 = __this->get_up_6();
		if (L_7)
		{
			goto IL_0070;
		}
	}
	{
		float L_8 = __this->get_t_5();
		float L_9 = __this->get_timeDilation_9();
		if ((!(((float)L_8) <= ((float)((-L_9))))))
		{
			goto IL_0070;
		}
	}

IL_0061:
	{
		bool L_10 = __this->get_up_6();
		__this->set_up_6((bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0));
	}

IL_0070:
	{
		float L_11 = __this->get_t_5();
		float L_12 = __this->get_radiusMax_12();
		float L_13 = __this->get_radiusMin_11();
		float L_14 = __this->get_radiusMedium_7();
		__this->set_radius_3(((float)((float)((float)((float)((float)((float)L_11*(float)((float)((float)L_12-(float)L_13))))+(float)((float)((float)L_14*(float)(2.0f)))))/(float)(2.0f))));
		bool L_15 = __this->get_alphaChange_13();
		if (!L_15)
		{
			goto IL_00d1;
		}
	}
	{
		float L_16 = __this->get_t_5();
		float L_17 = __this->get_alphaOffset_14();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Min_m1648492575(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)L_16+(float)(1.0f)))/(float)(2.0f)))+(float)L_17)), (1.0f), /*hidden argument*/NULL);
		__this->set_alpha_4(L_18);
	}

IL_00d1:
	{
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = __this->get_radius_3();
		Vector3_t2243707580  L_22 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_22, (5.12f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_localScale_m2325460848(L_19, L_23, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_24 = __this->get_spriteRender_2();
		SpriteRenderer_t1209076198 * L_25 = __this->get_spriteRender_2();
		NullCheck(L_25);
		Color_t2020392075  L_26 = SpriteRenderer_get_color_m345525162(L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		float L_27 = (&V_0)->get_r_0();
		SpriteRenderer_t1209076198 * L_28 = __this->get_spriteRender_2();
		NullCheck(L_28);
		Color_t2020392075  L_29 = SpriteRenderer_get_color_m345525162(L_28, /*hidden argument*/NULL);
		V_1 = L_29;
		float L_30 = (&V_1)->get_g_1();
		SpriteRenderer_t1209076198 * L_31 = __this->get_spriteRender_2();
		NullCheck(L_31);
		Color_t2020392075  L_32 = SpriteRenderer_get_color_m345525162(L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		float L_33 = (&V_2)->get_b_2();
		float L_34 = __this->get_alpha_4();
		Color_t2020392075  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Color__ctor_m1909920690(&L_35, L_27, L_30, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_24);
		SpriteRenderer_set_color_m2339931967(L_24, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Infect::.ctor()
extern "C"  void HIV_Infect__ctor_m683343340 (HIV_Infect_t1458631357 * __this, const MethodInfo* method)
{
	{
		HIV_State__ctor_m1425936220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HIV_Infect::Replicate()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var;
extern const uint32_t HIV_Infect_Replicate_m1959195473_MetadataUsageId;
extern "C"  bool HIV_Infect_Replicate_m1959195473 (HIV_Infect_t1458631357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Infect_Replicate_m1959195473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	HIV_t2481767745 * V_2 = NULL;
	Statistics_t2537377007  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = 0;
		goto IL_0051;
	}

IL_0007:
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1756533147 * L_4 = Game_CreateHIV_m2019914367(L_0, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t1756533147 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1756533147 * L_7 = V_1;
		NullCheck(L_7);
		HIV_t2481767745 * L_8 = GameObject_GetComponent_TisHIV_t2481767745_m121922886(L_7, /*hidden argument*/GameObject_GetComponent_TisHIV_t2481767745_m121922886_MethodInfo_var);
		V_2 = L_8;
		HIV_t2481767745 * L_9 = V_2;
		TCell_t1815110036 * L_10 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		HIV_ChangeState_m1698492408(L_9, 4, L_11, /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_004b:
	{
		return (bool)0;
	}

IL_004d:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_13 = V_0;
		HIV_t2481767745 * L_14 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_14);
		HIVStats_t2521285894 * L_15 = HIV_get_stats_m4074649537(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Statistics_t2537377007  L_16 = HIVStats_get_stats_m1941178495(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = Statistics_get_Item_m3520282030((&V_3), 4, /*hidden argument*/NULL);
		if ((((float)(((float)((float)L_13)))) < ((float)L_17)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void HIV_Infect::OnEnable()
extern "C"  void HIV_Infect_OnEnable_m774894780 (HIV_Infect_t1458631357 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		HIV_Move_t1427226471 * L_1 = HIV_get_move_m2343449320(L_0, /*hidden argument*/NULL);
		TCell_t1815110036 * L_2 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_1, L_3, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Infect::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_Infect_Update_m251775717_MetadataUsageId;
extern "C"  void HIV_Infect_Update_m251775717 (HIV_Infect_t1458631357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Infect_Update_m251775717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		TCell_t1815110036 * L_3 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		float L_7 = Vector2_Distance_m280750759(NULL /*static, unused*/, L_2, L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_infectDistance_4();
		if ((!(((float)L_7) < ((float)L_8))))
		{
			goto IL_00a9;
		}
	}
	{
		bool L_9 = HIV_Infect_Replicate_m1959195473(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0051;
		}
	}
	{
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
		goto IL_00a4;
	}

IL_0051:
	{
		HIV_t2481767745 * L_11 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_11);
		HIV_ChangeType_m1447650064(L_11, /*hidden argument*/NULL);
		HIV_t2481767745 * L_12 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_12);
		HIVStats_t2521285894 * L_13 = HIV_get_stats_m4074649537(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008d;
		}
	}
	{
		HIV_t2481767745 * L_15 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_15);
		HIVStats_t2521285894 * L_16 = HIV_get_stats_m4074649537(L_15, /*hidden argument*/NULL);
		HIV_t2481767745 * L_17 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_17);
		List_1_t1906498139 * L_18 = HIV_get_statList_m3052448253(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		HIVStats_RefreshStats_m3154914018(L_16, L_18, (HIV_Piece_t1005595762 *)NULL, /*hidden argument*/NULL);
	}

IL_008d:
	{
		HIV_t2481767745 * L_19 = ((HIV_State_t57600565 *)__this)->get_main_2();
		TCell_t1815110036 * L_20 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		HIV_ChangeState_m1698492408(L_19, 4, L_21, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		goto IL_00cb;
	}

IL_00a9:
	{
		TCell_t1815110036 * L_22 = ((HIV_State_t57600565 *)__this)->get_host_3();
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m3105766835(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = GameObject_get_activeSelf_m313590879(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00cb;
		}
	}
	{
		HIV_t2481767745 * L_25 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_25);
		HIV_ChangeState_m1698492408(L_25, 0, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		return;
	}
}
// System.Void HIV_Move::.ctor()
extern "C"  void HIV_Move__ctor_m785377476 (HIV_Move_t1427226471 * __this, const MethodInfo* method)
{
	{
		ObjectMovement2D__ctor_m2674233803(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single HIV_Move::get_maxSpeed()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_Move_get_maxSpeed_m59335644_MetadataUsageId;
extern "C"  float HIV_Move_get_maxSpeed_m59335644 (HIV_Move_t1427226471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Move_get_maxSpeed_m59335644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Statistics_t2537377007  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float G_B3_0 = 0.0f;
	{
		HIV_t2481767745 * L_0 = __this->get_main_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		HIV_t2481767745 * L_2 = __this->get_main_10();
		NullCheck(L_2);
		HIVStats_t2521285894 * L_3 = HIV_get_stats_m4074649537(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Statistics_t2537377007  L_4 = HIVStats_get_stats_m1941178495(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = Statistics_get_Item_m3520282030((&V_0), 0, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_003c;
	}

IL_002e:
	{
		Statistics_t2537377007  L_6 = Statistics_get_base_stats_m579726902(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = Statistics_get_Item_m3520282030((&V_1), 0, /*hidden argument*/NULL);
		G_B3_0 = L_7;
	}

IL_003c:
	{
		return G_B3_0;
	}
}
// System.Void HIV_Move::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHIV_t2481767745_m2522324452_MethodInfo_var;
extern const uint32_t HIV_Move_Awake_m2675884359_MetadataUsageId;
extern "C"  void HIV_Move_Awake_m2675884359 (HIV_Move_t1427226471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Move_Awake_m2675884359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		((ObjectMovement2D_t2992076704 *)__this)->set__rb_2(L_0);
		HIV_t2481767745 * L_1 = Component_GetComponent_TisHIV_t2481767745_m2522324452(__this, /*hidden argument*/Component_GetComponent_TisHIV_t2481767745_m2522324452_MethodInfo_var);
		__this->set_main_10(L_1);
		return;
	}
}
// System.Void HIV_Move::FixedUpdate()
extern "C"  void HIV_Move_FixedUpdate_m3816133837 (HIV_Move_t1427226471 * __this, const MethodInfo* method)
{
	{
		ObjectMovement2D_Move_m276962098(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Part::.ctor()
extern "C"  void HIV_Part__ctor_m540774812 (HIV_Part_t1695996321 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color HIV_Part::get_color()
extern "C"  Color_t2020392075  HIV_Part_get_color_m1608807173 (HIV_Part_t1695996321 * __this, const MethodInfo* method)
{
	{
		HIV_Piece_t1005595762 * L_0 = __this->get__script_1();
		NullCheck(L_0);
		Color_t2020392075  L_1 = HIV_Piece_get_color_m1290788950(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void HIV_Part::set_color(UnityEngine.Color)
extern "C"  void HIV_Part_set_color_m3721088932 (HIV_Part_t1695996321 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		HIV_Piece_t1005595762 * L_0 = __this->get__script_1();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		HIV_Piece_set_color_m3569103183(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// HIV_Piece HIV_Part::get_script()
extern "C"  HIV_Piece_t1005595762 * HIV_Part_get_script_m1909646023 (HIV_Part_t1695996321 * __this, const MethodInfo* method)
{
	{
		HIV_Piece_t1005595762 * L_0 = __this->get__script_1();
		return L_0;
	}
}
// UnityEngine.GameObject HIV_Part::get_instance()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_Part_get_instance_m1161421855_MetadataUsageId;
extern "C"  GameObject_t1756533147 * HIV_Part_get_instance_m1161421855 (HIV_Part_t1695996321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Part_get_instance_m1161421855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_Piece_t1005595762 * L_0 = __this->get__script_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return (GameObject_t1756533147 *)NULL;
	}

IL_0013:
	{
		HIV_Piece_t1005595762 * L_2 = HIV_Part_get_script_m1909646023(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void HIV_Part::Instantiate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3890207715;
extern Il2CppCodeGenString* _stringLiteral441962826;
extern const uint32_t HIV_Part_Instantiate_m1276955724_MetadataUsageId;
extern "C"  void HIV_Part_Instantiate_m1276955724 (HIV_Part_t1695996321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Part_Instantiate_m1276955724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_2, _stringLiteral3890207715, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		HIV_Piece_t1005595762 * L_5 = GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225(L_4, /*hidden argument*/GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225_MethodInfo_var);
		__this->set__script_1(L_5);
		HIV_Piece_t1005595762 * L_6 = __this->get__script_1();
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		NullReferenceException_t3156209119 * L_8 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_8, _stringLiteral441962826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004e:
	{
		return;
	}
}
// System.Void HIV_Part::Instantiate(UnityEngine.Transform,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2860266820;
extern Il2CppCodeGenString* _stringLiteral441962826;
extern const uint32_t HIV_Part_Instantiate_m519142666_MetadataUsageId;
extern "C"  void HIV_Part_Instantiate_m519142666 (HIV_Part_t1695996321 * __this, Transform_t3275118058 * ___parent0, Vector3_t2243707580  ___position1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Part_Instantiate_m519142666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_2, _stringLiteral2860266820, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		HIV_Piece_t1005595762 * L_5 = GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225(L_4, /*hidden argument*/GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225_MethodInfo_var);
		__this->set__script_1(L_5);
		HIV_Piece_t1005595762 * L_6 = __this->get__script_1();
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		NullReferenceException_t3156209119 * L_8 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_8, _stringLiteral441962826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004e:
	{
		Transform_t3275118058 * L_9 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		HIV_Piece_t1005595762 * L_11 = __this->get__script_1();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = ___parent0;
		NullCheck(L_12);
		Transform_SetParent_m4124909910(L_12, L_13, /*hidden argument*/NULL);
	}

IL_006b:
	{
		HIV_Piece_t1005595762 * L_14 = __this->get__script_1();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = ___position1;
		NullCheck(L_15);
		Transform_set_localPosition_m1026930133(L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Part::Instantiate(UnityEngine.Transform,UnityEngine.Color,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2860266820;
extern Il2CppCodeGenString* _stringLiteral441962826;
extern const uint32_t HIV_Part_Instantiate_m2085730656_MetadataUsageId;
extern "C"  void HIV_Part_Instantiate_m2085730656 (HIV_Part_t1695996321 * __this, Transform_t3275118058 * ___parent0, Color_t2020392075  ___c1, Vector3_t2243707580  ___position2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Part_Instantiate_m2085730656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_2, _stringLiteral2860266820, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		HIV_Piece_t1005595762 * L_5 = GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225(L_4, /*hidden argument*/GameObject_GetComponent_TisHIV_Piece_t1005595762_m3024644225_MethodInfo_var);
		__this->set__script_1(L_5);
		HIV_Piece_t1005595762 * L_6 = __this->get__script_1();
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		NullReferenceException_t3156209119 * L_8 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_8, _stringLiteral441962826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004e:
	{
		HIV_Piece_t1005595762 * L_9 = __this->get__script_1();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = ___parent0;
		NullCheck(L_10);
		Transform_SetParent_m4124909910(L_10, L_11, /*hidden argument*/NULL);
		HIV_Piece_t1005595762 * L_12 = __this->get__script_1();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = ___position2;
		NullCheck(L_13);
		Transform_set_localPosition_m1026930133(L_13, L_14, /*hidden argument*/NULL);
		HIV_Piece_t1005595762 * L_15 = __this->get__script_1();
		Color_t2020392075  L_16 = ___c1;
		NullCheck(L_15);
		HIV_Piece_set_color_m3569103183(L_15, L_16, /*hidden argument*/NULL);
		Color_t2020392075  L_17 = ___c1;
		HIV_Part_set_color_m3721088932(__this, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Piece::.ctor()
extern "C"  void HIV_Piece__ctor_m2599885317 (HIV_Piece_t1005595762 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color HIV_Piece::get_color()
extern "C"  Color_t2020392075  HIV_Piece_get_color_m1290788950 (HIV_Piece_t1005595762 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get__color_3();
		return L_0;
	}
}
// System.Void HIV_Piece::set_color(UnityEngine.Color)
extern "C"  void HIV_Piece_set_color_m3569103183 (HIV_Piece_t1005595762 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_lockColor_2();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_1 = HIV_Piece_get_spriteRender_m2674879059(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = ___value0;
		NullCheck(L_1);
		SpriteRenderer_set_color_m2339931967(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		VirtActionInvoker0::Invoke(4 /* System.Void HIV_Piece::ChangeStats() */, __this);
		Color_t2020392075  L_3 = ___value0;
		__this->set__color_3(L_3);
		return;
	}
}
// UnityEngine.SpriteRenderer HIV_Piece::get_spriteRender()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t HIV_Piece_get_spriteRender_m2674879059_MetadataUsageId;
extern "C"  SpriteRenderer_t1209076198 * HIV_Piece_get_spriteRender_m2674879059 (HIV_Piece_t1005595762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Piece_get_spriteRender_m2674879059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		return L_1;
	}
}
// Statistics HIV_Piece::get_stats()
extern "C"  Statistics_t2537377007  HIV_Piece_get_stats_m2359098831 (HIV_Piece_t1005595762 * __this, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0 = __this->get__stats_4();
		return L_0;
	}
}
// System.Void HIV_Return::.ctor()
extern "C"  void HIV_Return__ctor_m1167862777 (HIV_Return_t3773441690 * __this, const MethodInfo* method)
{
	{
		HIV_State__ctor_m1425936220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Return::OnEnable()
extern "C"  void HIV_Return_OnEnable_m987959897 (HIV_Return_t3773441690 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		HIV_Move_t1427226471 * L_1 = HIV_get_move_m2343449320(L_0, /*hidden argument*/NULL);
		HIV_t2481767745 * L_2 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_focalPoint_3();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_1, L_4, (2.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Return::Update()
extern const MethodInfo* GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940_MethodInfo_var;
extern const uint32_t HIV_Return_Update_m2512504600_MetadataUsageId;
extern "C"  void HIV_Return_Update_m2512504600 (HIV_Return_t3773441690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Return_Update_m2512504600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = L_0->get_focalPoint_3();
		NullCheck(L_1);
		FocalPoint_t1860633653 * L_2 = GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940(L_1, /*hidden argument*/GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940_MethodInfo_var);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_4 = FocalPoint_GetDistance_m593464702(L_2, L_3, /*hidden argument*/NULL);
		HIV_t2481767745 * L_5 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_focalPoint_3();
		NullCheck(L_6);
		FocalPoint_t1860633653 * L_7 = GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940(L_6, /*hidden argument*/GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940_MethodInfo_var);
		NullCheck(L_7);
		float L_8 = FocalPoint_get_midpointCutoff_m3392978060(L_7, /*hidden argument*/NULL);
		if ((!(((float)L_4) < ((float)L_8))))
		{
			goto IL_004c;
		}
	}
	{
		HIV_t2481767745 * L_9 = ((HIV_State_t57600565 *)__this)->get_main_2();
		HIV_t2481767745 * L_10 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = L_10->get_focalPoint_3();
		NullCheck(L_9);
		HIV_ChangeState_m1698492408(L_9, 0, L_11, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void HIV_Selected::.ctor()
extern "C"  void HIV_Selected__ctor_m4083327364 (HIV_Selected_t1107388507 * __this, const MethodInfo* method)
{
	{
		HIV_State__ctor_m1425936220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Selected::OnEnable()
extern "C"  void HIV_Selected_OnEnable_m345684168 (HIV_Selected_t1107388507 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		HIV_Move_t1427226471 * L_1 = HIV_get_move_m2343449320(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = __this->get_destination_4();
		Vector2_t2243707579  L_3 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ObjectMovement2D_ChangeFollowState_m3934905037(L_1, L_3, (5.0f), (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Selected::FixedUpdate()
extern "C"  void HIV_Selected_FixedUpdate_m1335899281 (HIV_Selected_t1107388507 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		bool L_1 = L_0->get_selected_8();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		HIV_t2481767745 * L_2 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_2);
		HIV_ChangeState_m1698492408(L_2, 0, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		goto IL_009e;
	}

IL_0022:
	{
		HIV_t2481767745 * L_3 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_3);
		HIV_Move_t1427226471 * L_4 = HIV_get_move_m2343449320(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = ObjectMovement2D_get_followState_m1966773327(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_005e;
		}
	}
	{
		HIV_t2481767745 * L_6 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_6);
		HIV_Move_t1427226471 * L_7 = HIV_get_move_m2343449320(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = __this->get_destination_4();
		Vector2_t2243707579  L_9 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		ObjectMovement2D_ChangeFollowState_m3934905037(L_7, L_9, (1.0f), (bool)0, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_005e:
	{
		HIV_t2481767745 * L_10 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_10);
		HIV_Move_t1427226471 * L_11 = HIV_get_move_m2343449320(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = __this->get_destination_4();
		NullCheck(L_11);
		((ObjectMovement2D_t2992076704 *)L_11)->set_followVector_5(L_12);
	}

IL_0074:
	{
		HIV_t2481767745 * L_13 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_13);
		bool L_14 = HIV_get_weak_m338777797(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_009e;
		}
	}
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_Rotate_m4255273365(L_15, (0.0f), (0.0f), (3.0f), /*hidden argument*/NULL);
	}

IL_009e:
	{
		bool L_16 = HIV_Selected_OutOfBounds_m3468040698(__this, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c3;
		}
	}
	{
		HIV_t2481767745 * L_17 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_17);
		HIV_Move_t1427226471 * L_18 = HIV_get_move_m2343449320(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		ObjectMovement2D_ChangeMultiplier_m4218570319(L_18, (1.5f), /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00c3:
	{
		HIV_t2481767745 * L_19 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_19);
		HIV_Move_t1427226471 * L_20 = HIV_get_move_m2343449320(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		ObjectMovement2D_ChangeMultiplier_m4218570319(L_20, (5.0f), /*hidden argument*/NULL);
	}

IL_00d8:
	{
		return;
	}
}
// System.Boolean HIV_Selected::OutOfBounds()
extern "C"  bool HIV_Selected_OutOfBounds_m3468040698 (HIV_Selected_t1107388507 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Rect_t3681755626  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t2243707580  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Rect_t3681755626  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		HIV_Move_t1427226471 * L_1 = HIV_get_move_m2343449320(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = ObjectMovement2D_get_velocity_m987348582(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_0();
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_0054;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_x_1();
		Camera_t189460977 * L_7 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_8 = __this->get_slowdownBorder_5();
		Rect_t3681755626  L_9 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = Rect_get_xMax_m2915145014((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)L_10))))
		{
			goto IL_0054;
		}
	}
	{
		return (bool)1;
	}

IL_0054:
	{
		HIV_t2481767745 * L_11 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_11);
		HIV_Move_t1427226471 * L_12 = HIV_get_move_m2343449320(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t2243707579  L_13 = ObjectMovement2D_get_velocity_m987348582(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_x_0();
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_00aa;
		}
	}
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = (&V_4)->get_x_1();
		Camera_t189460977 * L_18 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_19 = __this->get_slowdownBorder_5();
		Rect_t3681755626  L_20 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		float L_21 = Rect_get_xMin_m1161102488((&V_5), /*hidden argument*/NULL);
		if ((!(((float)L_17) < ((float)L_21))))
		{
			goto IL_00aa;
		}
	}
	{
		return (bool)1;
	}

IL_00aa:
	{
		HIV_t2481767745 * L_22 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_22);
		HIV_Move_t1427226471 * L_23 = HIV_get_move_m2343449320(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = ObjectMovement2D_get_velocity_m987348582(L_23, /*hidden argument*/NULL);
		V_6 = L_24;
		float L_25 = (&V_6)->get_y_1();
		if ((!(((float)L_25) > ((float)(0.0f)))))
		{
			goto IL_0101;
		}
	}
	{
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t2243707580  L_27 = Transform_get_position_m1104419803(L_26, /*hidden argument*/NULL);
		V_7 = L_27;
		float L_28 = (&V_7)->get_y_2();
		Camera_t189460977 * L_29 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_30 = __this->get_slowdownBorder_5();
		Rect_t3681755626  L_31 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_8 = L_31;
		float L_32 = Rect_get_yMax_m2915146103((&V_8), /*hidden argument*/NULL);
		if ((!(((float)L_28) > ((float)L_32))))
		{
			goto IL_0101;
		}
	}
	{
		return (bool)1;
	}

IL_0101:
	{
		HIV_t2481767745 * L_33 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_33);
		HIV_Move_t1427226471 * L_34 = HIV_get_move_m2343449320(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector2_t2243707579  L_35 = ObjectMovement2D_get_velocity_m987348582(L_34, /*hidden argument*/NULL);
		V_9 = L_35;
		float L_36 = (&V_9)->get_y_1();
		if ((!(((float)L_36) < ((float)(0.0f)))))
		{
			goto IL_0158;
		}
	}
	{
		Transform_t3275118058 * L_37 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_position_m1104419803(L_37, /*hidden argument*/NULL);
		V_10 = L_38;
		float L_39 = (&V_10)->get_y_2();
		Camera_t189460977 * L_40 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_41 = __this->get_slowdownBorder_5();
		Rect_t3681755626  L_42 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_11 = L_42;
		float L_43 = Rect_get_yMin_m1161103577((&V_11), /*hidden argument*/NULL);
		if ((!(((float)L_39) < ((float)L_43))))
		{
			goto IL_0158;
		}
	}
	{
		return (bool)1;
	}

IL_0158:
	{
		return (bool)0;
	}
}
// System.Void HIV_State::.ctor()
extern "C"  void HIV_State__ctor_m1425936220 (HIV_State_t57600565 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_State::Awake()
extern const MethodInfo* Component_GetComponent_TisHIV_t2481767745_m2522324452_MethodInfo_var;
extern const uint32_t HIV_State_Awake_m3048860341_MetadataUsageId;
extern "C"  void HIV_State_Awake_m3048860341 (HIV_State_t57600565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_State_Awake_m3048860341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = Component_GetComponent_TisHIV_t2481767745_m2522324452(__this, /*hidden argument*/Component_GetComponent_TisHIV_t2481767745_m2522324452_MethodInfo_var);
		__this->set_main_2(L_0);
		return;
	}
}
// System.Void HIV_Wander::.ctor()
extern "C"  void HIV_Wander__ctor_m803011428 (HIV_Wander_t1816073829 * __this, const MethodInfo* method)
{
	{
		HIV_State__ctor_m1425936220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Wander::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIV_Wander_Start_m3407966372_MetadataUsageId;
extern "C"  void HIV_Wander_Start_m3407966372 (HIV_Wander_t1816073829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Wander_Start_m3407966372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = L_0->get_focalPoint_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0021:
	{
		HIV_t2481767745 * L_3 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_3);
		HIV_Move_t1427226471 * L_4 = HIV_get_move_m2343449320(L_3, /*hidden argument*/NULL);
		HIV_t2481767745 * L_5 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_focalPoint_3();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_4, L_7, (1.5f), (bool)0, (bool)0, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void HIV_Wander::OnEnable()
extern "C"  void HIV_Wander_OnEnable_m3204205284 (HIV_Wander_t1816073829 * __this, const MethodInfo* method)
{
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		L_0->set_grouped_4((bool)1);
		return;
	}
}
// System.Void HIV_Wander::FixedUpdate()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940_MethodInfo_var;
extern const uint32_t HIV_Wander_FixedUpdate_m572207003_MetadataUsageId;
extern "C"  void HIV_Wander_FixedUpdate_m572207003 (HIV_Wander_t1816073829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Wander_FixedUpdate_m572207003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FocalPoint_t1860633653 * V_0 = NULL;
	int32_t V_1 = 0;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Statistics_t2537377007  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float G_B14_0 = 0.0f;
	Vector2_t2243707579  G_B14_1;
	memset(&G_B14_1, 0, sizeof(G_B14_1));
	HIV_Move_t1427226471 * G_B14_2 = NULL;
	float G_B13_0 = 0.0f;
	Vector2_t2243707579  G_B13_1;
	memset(&G_B13_1, 0, sizeof(G_B13_1));
	HIV_Move_t1427226471 * G_B13_2 = NULL;
	float G_B15_0 = 0.0f;
	float G_B15_1 = 0.0f;
	Vector2_t2243707579  G_B15_2;
	memset(&G_B15_2, 0, sizeof(G_B15_2));
	HIV_Move_t1427226471 * G_B15_3 = NULL;
	{
		HIV_t2481767745 * L_0 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_0);
		bool L_1 = L_0->get_selected_8();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		HIV_t2481767745 * L_2 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_2);
		HIV_ChangeState_m1698492408(L_2, 1, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		goto IL_01c4;
	}

IL_0022:
	{
		HIV_t2481767745 * L_3 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = L_3->get_focalPoint_3();
		NullCheck(L_4);
		FocalPoint_t1860633653 * L_5 = GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940(L_4, /*hidden argument*/GameObject_GetComponent_TisFocalPoint_t1860633653_m1277035940_MethodInfo_var);
		V_0 = L_5;
		HIV_t2481767745 * L_6 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_6);
		HIV_Move_t1427226471 * L_7 = HIV_get_move_m2343449320(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = ObjectMovement2D_get_followState_m1966773327(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		if (!L_9)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_00b3;
		}
	}
	{
		goto IL_01a7;
	}

IL_0056:
	{
		FocalPoint_t1860633653 * L_11 = V_0;
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		float L_13 = FocalPoint_GetDistance_m593464702(L_11, L_12, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_14 = V_0;
		NullCheck(L_14);
		float L_15 = FocalPoint_get_maxDistance_m2498898468(L_14, /*hidden argument*/NULL);
		if ((!(((float)L_13) < ((float)L_15))))
		{
			goto IL_008a;
		}
	}
	{
		HIV_t2481767745 * L_16 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_16);
		HIV_Move_t1427226471 * L_17 = HIV_get_move_m2343449320(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_17, (Transform_t3275118058 *)NULL, (1.5f), (bool)0, (bool)0, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_008a:
	{
		FocalPoint_t1860633653 * L_18 = V_0;
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_20 = FocalPoint_GetDistance_m593464702(L_18, L_19, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_21 = V_0;
		NullCheck(L_21);
		float L_22 = FocalPoint_get_distanceCutoff_m3231606637(L_21, /*hidden argument*/NULL);
		if ((!(((float)L_20) > ((float)L_22))))
		{
			goto IL_00ae;
		}
	}
	{
		HIV_t2481767745 * L_23 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_23);
		HIV_ChangeState_m1698492408(L_23, 2, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		goto IL_01c4;
	}

IL_00b3:
	{
		FocalPoint_t1860633653 * L_24 = V_0;
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		float L_26 = FocalPoint_GetDistance_m593464702(L_24, L_25, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_27 = V_0;
		NullCheck(L_27);
		float L_28 = FocalPoint_get_midpointCutoff_m3392978060(L_27, /*hidden argument*/NULL);
		if ((!(((float)L_26) > ((float)L_28))))
		{
			goto IL_00f6;
		}
	}
	{
		HIV_t2481767745 * L_29 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_29);
		HIV_Move_t1427226471 * L_30 = HIV_get_move_m2343449320(L_29, /*hidden argument*/NULL);
		HIV_t2481767745 * L_31 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_31);
		GameObject_t1756533147 * L_32 = L_31->get_focalPoint_3();
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = GameObject_get_transform_m909382139(L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_30, L_33, (1.5f), (bool)0, (bool)0, /*hidden argument*/NULL);
		goto IL_01a2;
	}

IL_00f6:
	{
		HIV_t2481767745 * L_34 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_34);
		HIV_Move_t1427226471 * L_35 = HIV_get_move_m2343449320(L_34, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_36 = V_0;
		NullCheck(L_36);
		FocalPointMove_t3911050876 * L_37 = FocalPoint_get_move_m4157530869(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector2_t2243707579  L_38 = ObjectMovement2D_get_velocity_m987348582(L_37, /*hidden argument*/NULL);
		V_2 = L_38;
		Vector2_t2243707579  L_39 = Vector2_get_normalized_m2985402409((&V_2), /*hidden argument*/NULL);
		HIV_t2481767745 * L_40 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_40);
		HIV_Move_t1427226471 * L_41 = HIV_get_move_m2343449320(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector2_t2243707579  L_42 = ObjectMovement2D_get_velocity_m987348582(L_41, /*hidden argument*/NULL);
		V_3 = L_42;
		float L_43 = Vector2_get_magnitude_m33802565((&V_3), /*hidden argument*/NULL);
		HIV_t2481767745 * L_44 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_44);
		HIV_Move_t1427226471 * L_45 = HIV_get_move_m2343449320(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		float L_46 = ObjectMovement2D_get_speed_m1576568683(L_45, /*hidden argument*/NULL);
		FocalPoint_t1860633653 * L_47 = V_0;
		NullCheck(L_47);
		FocalPointMove_t3911050876 * L_48 = FocalPoint_get_move_m4157530869(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector2_t2243707579  L_49 = ObjectMovement2D_get_velocity_m987348582(L_48, /*hidden argument*/NULL);
		V_4 = L_49;
		float L_50 = Vector2_get_magnitude_m33802565((&V_4), /*hidden argument*/NULL);
		G_B13_0 = L_43;
		G_B13_1 = L_39;
		G_B13_2 = L_35;
		if ((!(((float)((float)((float)L_46/(float)(2.0f)))) > ((float)L_50))))
		{
			G_B14_0 = L_43;
			G_B14_1 = L_39;
			G_B14_2 = L_35;
			goto IL_017a;
		}
	}
	{
		HIV_t2481767745 * L_51 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_51);
		HIVStats_t2521285894 * L_52 = HIV_get_stats_m4074649537(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Statistics_t2537377007  L_53 = HIVStats_get_stats_m1941178495(L_52, /*hidden argument*/NULL);
		V_5 = L_53;
		float L_54 = Statistics_get_Item_m3520282030((&V_5), 0, /*hidden argument*/NULL);
		G_B15_0 = L_54;
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		goto IL_018e;
	}

IL_017a:
	{
		FocalPoint_t1860633653 * L_55 = V_0;
		NullCheck(L_55);
		FocalPointMove_t3911050876 * L_56 = FocalPoint_get_move_m4157530869(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector2_t2243707579  L_57 = ObjectMovement2D_get_velocity_m987348582(L_56, /*hidden argument*/NULL);
		V_6 = L_57;
		float L_58 = Vector2_get_magnitude_m33802565((&V_6), /*hidden argument*/NULL);
		G_B15_0 = L_58;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
	}

IL_018e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_59 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, G_B15_1, G_B15_0, (0.1f), /*hidden argument*/NULL);
		Vector2_t2243707579  L_60 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, G_B15_2, L_59, /*hidden argument*/NULL);
		NullCheck(G_B15_3);
		ObjectMovement2D_set_velocity_m1559477545(G_B15_3, L_60, /*hidden argument*/NULL);
	}

IL_01a2:
	{
		goto IL_01c4;
	}

IL_01a7:
	{
		HIV_t2481767745 * L_61 = ((HIV_State_t57600565 *)__this)->get_main_2();
		NullCheck(L_61);
		HIV_Move_t1427226471 * L_62 = HIV_get_move_m2343449320(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		ObjectMovement2D_ChangeFollowState_m1251427041(L_62, (Transform_t3275118058 *)NULL, (1.0f), (bool)0, (bool)0, /*hidden argument*/NULL);
		goto IL_01c4;
	}

IL_01c4:
	{
		return;
	}
}
// System.Void HIV_Weak::.ctor()
extern "C"  void HIV_Weak__ctor_m2116656939 (HIV_Weak_t1951470088 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 HIV_Weak::get_shakeNumber()
extern "C"  int32_t HIV_Weak_get_shakeNumber_m3852701743 (HIV_Weak_t1951470088 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CshakeNumberU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void HIV_Weak::set_shakeNumber(System.Int32)
extern "C"  void HIV_Weak_set_shakeNumber_m1038909782 (HIV_Weak_t1951470088 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CshakeNumberU3Ek__BackingField_9(L_0);
		return;
	}
}
// HIV HIV_Weak::get_main()
extern const MethodInfo* Component_GetComponent_TisHIV_t2481767745_m2522324452_MethodInfo_var;
extern const uint32_t HIV_Weak_get_main_m3759538633_MetadataUsageId;
extern "C"  HIV_t2481767745 * HIV_Weak_get_main_m3759538633 (HIV_Weak_t1951470088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Weak_get_main_m3759538633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = Component_GetComponent_TisHIV_t2481767745_m2522324452(__this, /*hidden argument*/Component_GetComponent_TisHIV_t2481767745_m2522324452_MethodInfo_var);
		return L_0;
	}
}
// System.Void HIV_Weak::OnEnable()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioDatabase_t2585396589_m217869328_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3808647324;
extern const uint32_t HIV_Weak_OnEnable_m2161801735_MetadataUsageId;
extern "C"  void HIV_Weak_OnEnable_m2161801735 (HIV_Weak_t1951470088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Weak_OnEnable_m2161801735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1756533147 * L_0 = __this->get_bubble_2();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		HIV_t2481767745 * L_1 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Color_t2020392075  L_2 = HIV_get_GP120_Color_Left_m1171295536(L_1, /*hidden argument*/NULL);
		__this->set_gp120_l_3(L_2);
		HIV_t2481767745 * L_3 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t2020392075  L_4 = HIV_get_GP120_Color_Right_m1095216545(L_3, /*hidden argument*/NULL);
		__this->set_gp120_r_4(L_4);
		HIV_t2481767745 * L_5 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Color_t2020392075  L_6 = HIV_get_GP41_Color_m4220143536(L_5, /*hidden argument*/NULL);
		__this->set_gp41_5(L_6);
		HIV_t2481767745 * L_7 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		HIV_set_weak_m4184767996(L_7, (bool)1, /*hidden argument*/NULL);
		HIV_t2481767745 * L_8 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_excludedIndex_6();
		NullCheck(L_8);
		GameObject_t1756533147 * L_10 = HIV_GetSpike_m1198366128(L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Spike_t60554746 * L_11 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_10, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		V_0 = L_11;
		Spike_t60554746 * L_12 = V_0;
		NullCheck(L_12);
		GP41_t921954836 * L_13 = Spike_get_gp41_m2571159277(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		SpriteRenderer_t1209076198 * L_14 = GP41_get_spriteRender_m1828691819(L_13, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_gp41_5();
		NullCheck(L_14);
		SpriteRenderer_set_color_m2339931967(L_14, L_15, /*hidden argument*/NULL);
		Spike_t60554746 * L_16 = V_0;
		NullCheck(L_16);
		GP120_t1512820336 * L_17 = Spike_get_gp120_m3432987073(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		SpriteRenderer_t1209076198 * L_18 = GP120_get_displayLeft_m2311060087(L_17, /*hidden argument*/NULL);
		Color_t2020392075  L_19 = __this->get_gp120_l_3();
		NullCheck(L_18);
		SpriteRenderer_set_color_m2339931967(L_18, L_19, /*hidden argument*/NULL);
		Spike_t60554746 * L_20 = V_0;
		NullCheck(L_20);
		GP120_t1512820336 * L_21 = Spike_get_gp120_m3432987073(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		SpriteRenderer_t1209076198 * L_22 = GP120_get_displayRight_m461027194(L_21, /*hidden argument*/NULL);
		Color_t2020392075  L_23 = __this->get_gp120_r_4();
		NullCheck(L_22);
		SpriteRenderer_set_color_m2339931967(L_22, L_23, /*hidden argument*/NULL);
		HIV_t2481767745 * L_24 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		AudioDatabase_t2585396589 * L_25 = Component_GetComponent_TisAudioDatabase_t2585396589_m217869328(L_24, /*hidden argument*/Component_GetComponent_TisAudioDatabase_t2585396589_m217869328_MethodInfo_var);
		NullCheck(L_25);
		AudioDatabase_Play_m2837110456(L_25, _stringLiteral3808647324, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_1));
		Vector3_t2243707580  L_26 = V_1;
		__this->set_prev_velocity_8(L_26);
		HIV_Weak_set_shakeNumber_m1038909782(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Weak::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303_MethodInfo_var;
extern const uint32_t HIV_Weak_OnDisable_m369821140_MetadataUsageId;
extern "C"  void HIV_Weak_OnDisable_m369821140 (HIV_Weak_t1951470088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Weak_OnDisable_m369821140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_bubble_2();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		HIV_t2481767745 * L_1 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = __this->get_gp120_l_3();
		NullCheck(L_1);
		HIV_set_GP120_Color_Left_m2190792593(L_1, L_2, /*hidden argument*/NULL);
		HIV_t2481767745 * L_3 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_4 = __this->get_gp120_r_4();
		NullCheck(L_3);
		HIV_set_GP120_Color_Right_m3365475992(L_3, L_4, /*hidden argument*/NULL);
		HIV_t2481767745 * L_5 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_6 = __this->get_gp41_5();
		NullCheck(L_5);
		HIV_set_GP41_Color_m2193771811(L_5, L_6, /*hidden argument*/NULL);
		HIV_t2481767745 * L_7 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		HIV_set_weak_m4184767996(L_7, (bool)0, /*hidden argument*/NULL);
		Antibody_t2931822436 * L_8 = __this->get_antibody_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		Antibody_t2931822436 * L_10 = __this->get_antibody_7();
		NullCheck(L_10);
		AntibodyLatch_t1529651966 * L_11 = Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303(L_10, /*hidden argument*/Component_GetComponent_TisAntibodyLatch_t1529651966_m3493195303_MethodInfo_var);
		NullCheck(L_11);
		AntibodyLatch_Kill_m3165421869(L_11, /*hidden argument*/NULL);
	}

IL_006b:
	{
		HIV_Weak_set_shakeNumber_m1038909782(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIV_Weak::FixedUpdate()
extern "C"  void HIV_Weak_FixedUpdate_m3559831028 (HIV_Weak_t1951470088 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		HIV_t2481767745 * L_0 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = HIV_get_state_m3030343602(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_00a8;
		}
	}
	{
		Vector3_t2243707580 * L_2 = __this->get_address_of_prev_velocity_8();
		float L_3 = L_2->get_x_1();
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_0048;
		}
	}
	{
		HIV_t2481767745 * L_4 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		HIV_Move_t1427226471 * L_5 = HIV_get_move_m2343449320(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector2_t2243707579  L_6 = ObjectMovement2D_get_velocity_m987348582(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (&V_0)->get_x_0();
		if ((((float)L_7) < ((float)(0.0f))))
		{
			goto IL_007f;
		}
	}

IL_0048:
	{
		Vector3_t2243707580 * L_8 = __this->get_address_of_prev_velocity_8();
		float L_9 = L_8->get_x_1();
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}
	{
		HIV_t2481767745 * L_10 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		HIV_Move_t1427226471 * L_11 = HIV_get_move_m2343449320(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector2_t2243707579  L_12 = ObjectMovement2D_get_velocity_m987348582(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = (&V_1)->get_x_0();
		if ((!(((float)L_13) > ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}

IL_007f:
	{
		int32_t L_14 = HIV_Weak_get_shakeNumber_m3852701743(__this, /*hidden argument*/NULL);
		HIV_Weak_set_shakeNumber_m1038909782(__this, ((int32_t)((int32_t)L_14+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_008d:
	{
		HIV_t2481767745 * L_15 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		HIV_Move_t1427226471 * L_16 = HIV_get_move_m2343449320(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector2_t2243707579  L_17 = ObjectMovement2D_get_velocity_m987348582(L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->set_prev_velocity_8(L_18);
	}

IL_00a8:
	{
		return;
	}
}
// System.Void HIV_Weak::ChangeExcludedIndex(System.Int32)
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const uint32_t HIV_Weak_ChangeExcludedIndex_m1505635500_MetadataUsageId;
extern "C"  void HIV_Weak_ChangeExcludedIndex_m1505635500 (HIV_Weak_t1951470088 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIV_Weak_ChangeExcludedIndex_m1505635500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * V_0 = NULL;
	{
		bool L_0 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		HIV_t2481767745 * L_1 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		HIV_set_weak_m4184767996(L_1, (bool)1, /*hidden argument*/NULL);
		HIV_t2481767745 * L_2 = HIV_Weak_get_main_m3759538633(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___index0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_4 = HIV_GetSpike_m1198366128(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Spike_t60554746 * L_5 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_4, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		V_0 = L_5;
		Spike_t60554746 * L_6 = V_0;
		NullCheck(L_6);
		GP41_t921954836 * L_7 = Spike_get_gp41_m2571159277(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		SpriteRenderer_t1209076198 * L_8 = GP41_get_spriteRender_m1828691819(L_7, /*hidden argument*/NULL);
		Color_t2020392075  L_9 = __this->get_gp41_5();
		NullCheck(L_8);
		SpriteRenderer_set_color_m2339931967(L_8, L_9, /*hidden argument*/NULL);
		Spike_t60554746 * L_10 = V_0;
		NullCheck(L_10);
		GP120_t1512820336 * L_11 = Spike_get_gp120_m3432987073(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		SpriteRenderer_t1209076198 * L_12 = GP120_get_displayLeft_m2311060087(L_11, /*hidden argument*/NULL);
		Color_t2020392075  L_13 = __this->get_gp120_l_3();
		NullCheck(L_12);
		SpriteRenderer_set_color_m2339931967(L_12, L_13, /*hidden argument*/NULL);
		Spike_t60554746 * L_14 = V_0;
		NullCheck(L_14);
		GP120_t1512820336 * L_15 = Spike_get_gp120_m3432987073(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		SpriteRenderer_t1209076198 * L_16 = GP120_get_displayRight_m461027194(L_15, /*hidden argument*/NULL);
		Color_t2020392075  L_17 = __this->get_gp120_r_4();
		NullCheck(L_16);
		SpriteRenderer_set_color_m2339931967(L_16, L_17, /*hidden argument*/NULL);
	}

IL_006b:
	{
		int32_t L_18 = ___index0;
		__this->set_excludedIndex_6(L_18);
		return;
	}
}
// System.Void HIVCollision::.ctor()
extern "C"  void HIVCollision__ctor_m3646795908 (HIVCollision_t2736474671 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIVReceptorChecker::.ctor()
extern "C"  void HIVReceptorChecker__ctor_m2287392421 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tropism HIVReceptorChecker::get_tropism()
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const uint32_t HIVReceptorChecker_get_tropism_m3546347411_MetadataUsageId;
extern "C"  Tropism_t3662836552  HIVReceptorChecker_get_tropism_m3546347411 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_get_tropism_m3546347411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = HIV_GetSpike_m1198366128(L_0, 0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Spike_t60554746 * L_2 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_1, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		NullCheck(L_2);
		Tropism_t3662836552  L_3 = Spike_get_tropism_m1909874245(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// HIV HIVReceptorChecker::get_main()
extern const MethodInfo* Component_GetComponentInParent_TisHIV_t2481767745_m535771447_MethodInfo_var;
extern const uint32_t HIVReceptorChecker_get_main_m3762925379_MetadataUsageId;
extern "C"  HIV_t2481767745 * HIVReceptorChecker_get_main_m3762925379 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_get_main_m3762925379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = Component_GetComponentInParent_TisHIV_t2481767745_m535771447(__this, /*hidden argument*/Component_GetComponentInParent_TisHIV_t2481767745_m535771447_MethodInfo_var);
		return L_0;
	}
}
// System.Single HIVReceptorChecker::get__timer()
extern "C"  float HIVReceptorChecker_get__timer_m1697399096 (HIVReceptorChecker_t623690862 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3C_timerU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void HIVReceptorChecker::set__timer(System.Single)
extern "C"  void HIVReceptorChecker_set__timer_m2774836377 (HIVReceptorChecker_t623690862 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3C_timerU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void HIVReceptorChecker::OnTriggerEnter2D(UnityEngine.Collider2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UI_World_Filler_t108322328_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996_MethodInfo_var;
extern const uint32_t HIVReceptorChecker_OnTriggerEnter2D_m1792328533_MetadataUsageId;
extern "C"  void HIVReceptorChecker_OnTriggerEnter2D_m1792328533 (HIVReceptorChecker_t623690862 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_OnTriggerEnter2D_m1792328533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_selected_8();
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Collider2D_t646061738 * L_2 = ___c0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		TCell_Receptor_t2424206879 * L_4 = GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996(L_3, /*hidden argument*/GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		HIV_t2481767745 * L_6 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = L_6->get_selected_8();
		if (!L_7)
		{
			goto IL_00af;
		}
	}
	{
		Collider2D_t646061738 * L_8 = ___c0;
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		TCell_Receptor_t2424206879 * L_10 = GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996(L_9, /*hidden argument*/GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996_MethodInfo_var);
		bool L_11 = HIVReceptorChecker_validCombination_m1694857471(__this, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00af;
		}
	}
	{
		HIV_t2481767745 * L_12 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = HIV_get_weak_m338777797(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00af;
		}
	}
	{
		float L_14 = __this->get_maxTimer_3();
		HIVReceptorChecker_set__timer_m2774836377(__this, L_14, /*hidden argument*/NULL);
		Collider2D_t646061738 * L_15 = ___c0;
		__this->set_stay_2(L_15);
		UI_World_Filler_t108322328 * L_16 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)1, /*hidden argument*/NULL);
		UI_World_Filler_t108322328 * L_18 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(L_18, /*hidden argument*/NULL);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2720820983(&L_22, (5.0f), (5.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_position_m2469242620(L_19, L_23, /*hidden argument*/NULL);
	}

IL_00af:
	{
		return;
	}
}
// System.Void HIVReceptorChecker::OnTriggerStay2D(UnityEngine.Collider2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UI_World_Filler_t108322328_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996_MethodInfo_var;
extern const uint32_t HIVReceptorChecker_OnTriggerStay2D_m3688501010_MetadataUsageId;
extern "C"  void HIVReceptorChecker_OnTriggerStay2D_m3688501010 (HIVReceptorChecker_t623690862 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_OnTriggerStay2D_m3688501010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___c0;
		Collider2D_t646061738 * L_1 = __this->get_stay_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00d1;
		}
	}
	{
		HIV_t2481767745 * L_3 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = L_3->get_selected_8();
		if (!L_4)
		{
			goto IL_00b9;
		}
	}
	{
		float L_5 = HIVReceptorChecker_get__timer_m1697399096(__this, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		HIVReceptorChecker_set__timer_m2774836377(__this, ((float)((float)L_5-(float)L_6)), /*hidden argument*/NULL);
		UI_World_Filler_t108322328 * L_7 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m2720820983(&L_11, (5.0f), (5.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_position_m2469242620(L_8, L_12, /*hidden argument*/NULL);
		UI_World_Filler_t108322328 * L_13 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		float L_14 = HIVReceptorChecker_get__timer_m1697399096(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_maxTimer_3();
		NullCheck(L_13);
		UI_World_Filler_Fill_m2266385555(L_13, ((float)((float)(1.0f)-(float)((float)((float)L_14/(float)L_15)))), /*hidden argument*/NULL);
		float L_16 = HIVReceptorChecker_get__timer_m1697399096(__this, /*hidden argument*/NULL);
		if ((!(((float)L_16) < ((float)(0.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		Collider2D_t646061738 * L_17 = ___c0;
		NullCheck(L_17);
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		TCell_Receptor_t2424206879 * L_19 = GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996(L_18, /*hidden argument*/GameObject_GetComponent_TisTCell_Receptor_t2424206879_m4283321996_MethodInfo_var);
		NullCheck(L_19);
		TCell_t1815110036 * L_20 = TCell_Receptor_get_cell_m2345807034(L_19, /*hidden argument*/NULL);
		HIVReceptorChecker_Infect_m2960205694(__this, L_20, /*hidden argument*/NULL);
		UI_World_Filler_t108322328 * L_21 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_21);
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		GameObject_SetActive_m2887581199(L_22, (bool)0, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		goto IL_00d1;
	}

IL_00b9:
	{
		__this->set_stay_2((Collider2D_t646061738 *)NULL);
		UI_World_Filler_t108322328 * L_23 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		GameObject_SetActive_m2887581199(L_24, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_00d1:
	{
		return;
	}
}
// System.Void HIVReceptorChecker::OnTriggerExit2D(UnityEngine.Collider2D)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UI_World_Filler_t108322328_il2cpp_TypeInfo_var;
extern const uint32_t HIVReceptorChecker_OnTriggerExit2D_m1553920893_MetadataUsageId;
extern "C"  void HIVReceptorChecker_OnTriggerExit2D_m1553920893 (HIVReceptorChecker_t623690862 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_OnTriggerExit2D_m1553920893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t646061738 * L_0 = ___c0;
		Collider2D_t646061738 * L_1 = __this->get_stay_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		__this->set_stay_2((Collider2D_t646061738 *)NULL);
		UI_World_Filler_t108322328 * L_3 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean HIVReceptorChecker::validCombination(TCell_Receptor)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIVReceptorChecker_validCombination_m1694857471_MetadataUsageId;
extern "C"  bool HIVReceptorChecker_validCombination_m1694857471 (HIVReceptorChecker_t623690862 * __this, TCell_Receptor_t2424206879 * ___check0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_validCombination_m1694857471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tropism_t3662836552  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Tropism_t3662836552  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Tropism_t3662836552  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Tropism_t3662836552  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Tropism_t3662836552  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Tropism_t3662836552  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	{
		TCell_Receptor_t2424206879 * L_0 = ___check0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0081;
		}
	}
	{
		TCell_Receptor_t2424206879 * L_2 = ___check0;
		NullCheck(L_2);
		Tropism_t3662836552  L_3 = TCell_Receptor_get_type_m1545012622(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = (&V_0)->get_receptor_10();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0081;
		}
	}
	{
		Tropism_t3662836552  L_5 = HIVReceptorChecker_get_tropism_m3546347411(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = (&V_1)->get_coreceptor_11();
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_0060;
		}
	}
	{
		TCell_Receptor_t2424206879 * L_7 = ___check0;
		NullCheck(L_7);
		Tropism_t3662836552  L_8 = TCell_Receptor_get_type_m1545012622(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = (&V_2)->get_coreceptor_11();
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_005a;
		}
	}
	{
		TCell_Receptor_t2424206879 * L_10 = ___check0;
		NullCheck(L_10);
		Tropism_t3662836552  L_11 = TCell_Receptor_get_type_m1545012622(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		int32_t L_12 = (&V_3)->get_coreceptor_11();
		G_B6_0 = ((((int32_t)L_12) == ((int32_t)2))? 1 : 0);
		goto IL_005b;
	}

IL_005a:
	{
		G_B6_0 = 1;
	}

IL_005b:
	{
		G_B8_0 = G_B6_0;
		goto IL_0080;
	}

IL_0060:
	{
		Tropism_t3662836552  L_13 = HIVReceptorChecker_get_tropism_m3546347411(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = (&V_4)->get_coreceptor_11();
		TCell_Receptor_t2424206879 * L_15 = ___check0;
		NullCheck(L_15);
		Tropism_t3662836552  L_16 = TCell_Receptor_get_type_m1545012622(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		int32_t L_17 = (&V_5)->get_coreceptor_11();
		G_B8_0 = ((((int32_t)L_14) == ((int32_t)L_17))? 1 : 0);
	}

IL_0080:
	{
		return (bool)G_B8_0;
	}

IL_0081:
	{
		return (bool)0;
	}
}
// System.Void HIVReceptorChecker::Infect(TCell)
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* UI_Immunogenicity_t1529844486_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreDisplay_t164935196_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAudioDatabase_t2585396589_m3481153250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1507681642;
extern const uint32_t HIVReceptorChecker_Infect_m2960205694_MetadataUsageId;
extern "C"  void HIVReceptorChecker_Infect_m2960205694 (HIVReceptorChecker_t623690862 * __this, TCell_t1815110036 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVReceptorChecker_Infect_m2960205694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Tropism_t3662836552  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B4_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B5_1 = 0;
	{
		TCell_t1815110036 * L_0 = ___c0;
		NullCheck(L_0);
		bool L_1 = TCell_get_infected_m1112633080(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		TCell_t1815110036 * L_2 = ___c0;
		NullCheck(L_2);
		TCell_set_infected_m1220593909(L_2, (bool)1, /*hidden argument*/NULL);
		HIV_t2481767745 * L_3 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		TCell_t1815110036 * L_4 = ___c0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		HIV_ChangeState_m1698492408(L_3, 3, L_5, /*hidden argument*/NULL);
		Game_t1600141214 * L_6 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_6);
		int32_t L_7 = Game_get_population_m752974705(L_6, /*hidden argument*/NULL);
		Tropism_t3662836552  L_8 = HIVReceptorChecker_get_tropism_m3546347411(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = (&V_1)->get_coreceptor_11();
		G_B3_0 = ((int32_t)((int32_t)((int32_t)10)*(int32_t)L_7));
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			G_B4_0 = ((int32_t)((int32_t)((int32_t)10)*(int32_t)L_7));
			goto IL_004c;
		}
	}
	{
		G_B5_0 = 2;
		G_B5_1 = G_B3_0;
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 1;
		G_B5_1 = G_B4_0;
	}

IL_004d:
	{
		V_0 = ((int32_t)((int32_t)G_B5_1*(int32_t)G_B5_0));
		Game_t1600141214 * L_10 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		Game_t1600141214 * L_11 = L_10;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_score_24();
		int32_t L_13 = V_0;
		NullCheck(L_11);
		L_11->set_score_24(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		UI_Immunogenicity_t1529844486 * L_14 = ((UI_Immunogenicity_t1529844486_StaticFields*)UI_Immunogenicity_t1529844486_il2cpp_TypeInfo_var->static_fields)->get_main_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007f;
		}
	}
	{
		UI_Immunogenicity_t1529844486 * L_16 = ((UI_Immunogenicity_t1529844486_StaticFields*)UI_Immunogenicity_t1529844486_il2cpp_TypeInfo_var->static_fields)->get_main_11();
		NullCheck(L_16);
		UI_Immunogenicity_Add_m607723539(L_16, (0.1f), /*hidden argument*/NULL);
	}

IL_007f:
	{
		ScoreDisplay_t164935196 * L_17 = ((ScoreDisplay_t164935196_StaticFields*)ScoreDisplay_t164935196_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_17);
		ScoreDisplay_ShowScore_m293627306(L_17, L_20, L_21, (1.5f), L_22, /*hidden argument*/NULL);
		HIV_t2481767745 * L_23 = HIVReceptorChecker_get_main_m3762925379(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameObject_t1756533147 * L_24 = Component_get_gameObject_m3105766835(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		AudioDatabase_t2585396589 * L_25 = GameObject_GetComponent_TisAudioDatabase_t2585396589_m3481153250(L_24, /*hidden argument*/GameObject_GetComponent_TisAudioDatabase_t2585396589_m3481153250_MethodInfo_var);
		NullCheck(L_25);
		AudioDatabase_Play_m2837110456(L_25, _stringLiteral1507681642, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HIVStats::.ctor()
extern "C"  void HIVStats__ctor_m26403949 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	{
		__this->set_dying_4((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Statistics HIVStats::get_stats()
extern "C"  Statistics_t2537377007  HIVStats_get_stats_m1941178495 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0 = __this->get_U3CstatsU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void HIVStats::set_stats(Statistics)
extern "C"  void HIVStats_set_stats_m177167640 (HIVStats_t2521285894 * __this, Statistics_t2537377007  ___value0, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0 = ___value0;
		__this->set_U3CstatsU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Single HIVStats::get_health()
extern "C"  float HIVStats_get_health_m2635922628 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3ChealthU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void HIVStats::set_health(System.Single)
extern "C"  void HIVStats_set_health_m1015513025 (HIVStats_t2521285894 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3ChealthU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean HIVStats::get_alive()
extern "C"  bool HIVStats_get_alive_m2021401439 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = __this->get_dying_4();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		float L_1 = HIVStats_get_health_m2635922628(__this, /*hidden argument*/NULL);
		G_B3_0 = ((((float)L_1) < ((float)(0.0f)))? 1 : 0);
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
	}

IL_001b:
	{
		return (bool)((((int32_t)G_B3_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Single HIVStats::get_hp()
extern "C"  float HIVStats_get_hp_m2274879970 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = HIVStats_get_health_m2635922628(__this, /*hidden argument*/NULL);
		Statistics_t2537377007  L_1 = HIVStats_get_stats_m1941178495(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Statistics_get_Item_m3520282030((&V_0), 2, /*hidden argument*/NULL);
		return ((float)((float)L_0/(float)L_2));
	}
}
// System.Void HIVStats::RefreshStats(System.Collections.Generic.List`1<Statistics>,HIV_Piece)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3185245189_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3341236517_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m694413241_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1453949619_MethodInfo_var;
extern const uint32_t HIVStats_RefreshStats_m3154914018_MetadataUsageId;
extern "C"  void HIVStats_RefreshStats_m3154914018 (HIVStats_t2521285894 * __this, List_1_t1906498139 * ___statChanges0, HIV_Piece_t1005595762 * ___c1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVStats_RefreshStats_m3154914018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1441227813  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Statistics_t2537377007  L_0 = Statistics_get_base_stats_m579726902(NULL /*static, unused*/, /*hidden argument*/NULL);
		HIVStats_set_stats_m177167640(__this, L_0, /*hidden argument*/NULL);
		List_1_t1906498139 * L_1 = ___statChanges0;
		NullCheck(L_1);
		Enumerator_t1441227813  L_2 = List_1_GetEnumerator_m3185245189(L_1, /*hidden argument*/List_1_GetEnumerator_m3185245189_MethodInfo_var);
		V_1 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0017:
		{
			Statistics_t2537377007  L_3 = Enumerator_get_Current_m3341236517((&V_1), /*hidden argument*/Enumerator_get_Current_m3341236517_MethodInfo_var);
			V_0 = L_3;
			Statistics_t2537377007  L_4 = HIVStats_get_stats_m1941178495(__this, /*hidden argument*/NULL);
			Statistics_t2537377007  L_5 = V_0;
			Statistics_t2537377007  L_6 = Statistics_op_Addition_m3593248452(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			HIVStats_set_stats_m177167640(__this, L_6, /*hidden argument*/NULL);
		}

IL_0031:
		{
			bool L_7 = Enumerator_MoveNext_m694413241((&V_1), /*hidden argument*/Enumerator_MoveNext_m694413241_MethodInfo_var);
			if (L_7)
			{
				goto IL_0017;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1453949619((&V_1), /*hidden argument*/Enumerator_Dispose_m1453949619_MethodInfo_var);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0050:
	{
		Statistics_t2537377007  L_8 = HIVStats_get_stats_m1941178495(__this, /*hidden argument*/NULL);
		Statistics_t2537377007  L_9 = Statistics_LevelStats_m2675799454(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		HIVStats_set_stats_m177167640(__this, L_9, /*hidden argument*/NULL);
		HIVStats_ResetTimer_m2297303603(__this, /*hidden argument*/NULL);
		__this->set_dying_4((bool)1);
		HIV_Piece_t1005595762 * L_10 = ___c1;
		__this->set_lifeColor_5(L_10);
		HIV_Piece_t1005595762 * L_11 = ___c1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008c;
		}
	}
	{
		HIV_Piece_t1005595762 * L_13 = ___c1;
		NullCheck(L_13);
		Color_t2020392075  L_14 = HIV_Piece_get_color_m1290788950(L_13, /*hidden argument*/NULL);
		__this->set_startColor_6(L_14);
	}

IL_008c:
	{
		return;
	}
}
// System.Void HIVStats::ResetTimer()
extern "C"  void HIVStats_ResetTimer_m2297303603 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Statistics_t2537377007  L_0 = HIVStats_get_stats_m1941178495(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Statistics_get_Item_m3520282030((&V_0), 2, /*hidden argument*/NULL);
		HIVStats_set_health_m1015513025(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color HIVStats::CurrentColor()
extern "C"  Color_t2020392075  HIVStats_CurrentColor_m2490660782 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = HIVStats_get_health_m2635922628(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0025;
		}
	}
	{
		Color_t2020392075  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m3811852957(&L_1, (0.5f), (0.5f), (0.5f), /*hidden argument*/NULL);
		return L_1;
	}

IL_0025:
	{
		Color_t2020392075 * L_2 = __this->get_address_of_startColor_6();
		float L_3 = L_2->get_r_0();
		float L_4 = HIVStats_get_hp_m2274879970(__this, /*hidden argument*/NULL);
		V_0 = ((float)((float)((float)((float)((float)((float)L_3-(float)(0.5f)))*(float)L_4))+(float)(0.5f)));
		Color_t2020392075 * L_5 = __this->get_address_of_startColor_6();
		float L_6 = L_5->get_g_1();
		float L_7 = HIVStats_get_hp_m2274879970(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)((float)((float)L_6-(float)(0.5f)))*(float)L_7))+(float)(0.5f)));
		Color_t2020392075 * L_8 = __this->get_address_of_startColor_6();
		float L_9 = L_8->get_b_2();
		float L_10 = HIVStats_get_hp_m2274879970(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)((float)((float)((float)((float)L_9-(float)(0.5f)))*(float)L_10))+(float)(0.5f)));
		float L_11 = V_0;
		float L_12 = V_1;
		float L_13 = V_2;
		Color_t2020392075  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Color__ctor_m3811852957(&L_14, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void HIVStats::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HIVStats_Update_m688324164_MetadataUsageId;
extern "C"  void HIVStats_Update_m688324164 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HIVStats_Update_m688324164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_Piece_t1005595762 * L_0 = __this->get_lifeColor_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		HIV_Piece_t1005595762 * L_2 = __this->get_lifeColor_5();
		Color_t2020392075  L_3 = HIVStats_CurrentColor_m2490660782(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		HIV_Piece_set_color_m3569103183(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void HIVStats::LateUpdate()
extern "C"  void HIVStats_LateUpdate_m2726591320 (HIVStats_t2521285894 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_m313590879(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		__this->set_dying_4((bool)0);
	}

IL_0017:
	{
		bool L_2 = __this->get_dying_4();
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		float L_3 = HIVStats_get_health_m2635922628(__this, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		HIVStats_set_health_m1015513025(__this, ((float)((float)L_3-(float)L_4)), /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// Conversion methods for marshalling of: ImageRender
extern "C" void ImageRender_t3780187187_marshal_pinvoke(const ImageRender_t3780187187& unmarshaled, ImageRender_t3780187187_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'ImageRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
extern "C" void ImageRender_t3780187187_marshal_pinvoke_back(const ImageRender_t3780187187_marshaled_pinvoke& marshaled, ImageRender_t3780187187& unmarshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'ImageRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
// Conversion method for clean up from marshalling of: ImageRender
extern "C" void ImageRender_t3780187187_marshal_pinvoke_cleanup(ImageRender_t3780187187_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: ImageRender
extern "C" void ImageRender_t3780187187_marshal_com(const ImageRender_t3780187187& unmarshaled, ImageRender_t3780187187_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'ImageRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
extern "C" void ImageRender_t3780187187_marshal_com_back(const ImageRender_t3780187187_marshaled_com& marshaled, ImageRender_t3780187187& unmarshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'ImageRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
// Conversion method for clean up from marshalling of: ImageRender
extern "C" void ImageRender_t3780187187_marshal_com_cleanup(ImageRender_t3780187187_marshaled_com& marshaled)
{
}
// System.Void LayerBackground::.ctor()
extern "C"  void LayerBackground__ctor_m664790520 (LayerBackground_t3535234259 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LayerBackground::Start()
extern "C"  void LayerBackground_Start_m4140090628 (LayerBackground_t3535234259 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_layer_4();
		if ((((int32_t)L_0) >= ((int32_t)1)))
		{
			goto IL_0013;
		}
	}
	{
		__this->set_layer_4(1);
	}

IL_0013:
	{
		Sprite_t309593783 * L_1 = __this->get_picture_3();
		NullCheck(L_1);
		Rect_t3681755626  L_2 = Sprite_get_rect_m4043737881(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		Sprite_t309593783 * L_4 = __this->get_picture_3();
		NullCheck(L_4);
		float L_5 = Sprite_get_pixelsPerUnit_m1785791149(L_4, /*hidden argument*/NULL);
		__this->set_render_width_5(((float)((float)L_3/(float)L_5)));
		Sprite_t309593783 * L_6 = __this->get_picture_3();
		NullCheck(L_6);
		Rect_t3681755626  L_7 = Sprite_get_rect_m4043737881(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		Sprite_t309593783 * L_9 = __this->get_picture_3();
		NullCheck(L_9);
		float L_10 = Sprite_get_pixelsPerUnit_m1785791149(L_9, /*hidden argument*/NULL);
		__this->set_render_height_6(((float)((float)L_8/(float)L_10)));
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_12 = __this->get_render_2();
		NullCheck(L_12);
		float L_13 = Camera_get_orthographicSize_m1801974358(L_12, /*hidden argument*/NULL);
		Camera_t189460977 * L_14 = __this->get_render_2();
		NullCheck(L_14);
		float L_15 = Camera_get_orthographicSize_m1801974358(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322(&L_16, ((float)((float)L_13/(float)(5.0f))), ((float)((float)L_15/(float)(5.0f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m2325460848(L_11, L_16, /*hidden argument*/NULL);
		float L_17 = __this->get_render_width_5();
		Camera_t189460977 * L_18 = __this->get_render_2();
		NullCheck(L_18);
		float L_19 = Camera_get_orthographicSize_m1801974358(L_18, /*hidden argument*/NULL);
		__this->set_render_width_5(((float)((float)L_17*(float)((float)((float)L_19/(float)(5.0f))))));
		float L_20 = __this->get_render_height_6();
		Camera_t189460977 * L_21 = __this->get_render_2();
		NullCheck(L_21);
		float L_22 = Camera_get_orthographicSize_m1801974358(L_21, /*hidden argument*/NULL);
		__this->set_render_height_6(((float)((float)L_20*(float)((float)((float)L_22/(float)(5.0f))))));
		Camera_t189460977 * L_23 = __this->get_render_2();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_position_m1104419803(L_24, /*hidden argument*/NULL);
		__this->set_cameraPosPrev_9(L_25);
		return;
	}
}
// System.Void LayerBackground::FixedUpdate()
extern "C"  void LayerBackground_FixedUpdate_m846168869 (LayerBackground_t3535234259 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		LayerBackground_Move_m4102430019(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_x_1();
		Camera_t189460977 * L_3 = __this->get_render_2();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = (&V_2)->get_x_1();
		V_0 = ((float)((float)L_2-(float)L_6));
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = (&V_4)->get_y_2();
		Camera_t189460977 * L_10 = __this->get_render_2();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		V_5 = L_12;
		float L_13 = (&V_5)->get_y_2();
		V_3 = ((float)((float)L_9-(float)L_13));
		float L_14 = V_0;
		float L_15 = __this->get_render_width_5();
		if ((!(((float)L_14) < ((float)((-L_15))))))
		{
			goto IL_009a;
		}
	}
	{
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = L_16;
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		float L_19 = __this->get_render_width_5();
		Vector3_t2243707580  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m2720820983(&L_20, L_19, (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m2469242620(L_17, L_21, /*hidden argument*/NULL);
		goto IL_00c2;
	}

IL_009a:
	{
		float L_22 = V_0;
		float L_23 = __this->get_render_width_5();
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_00c2;
		}
	}
	{
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_25 = __this->get_render_width_5();
		Vector3_t2243707580  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector3__ctor_m2720820983(&L_26, ((-L_25)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_Translate_m3316827744(L_24, L_26, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		float L_27 = V_3;
		float L_28 = __this->get_render_height_6();
		if ((!(((float)L_27) < ((float)((-L_28))))))
		{
			goto IL_00ef;
		}
	}
	{
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_30 = __this->get_render_height_6();
		Vector3_t2243707580  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m2720820983(&L_31, (0.0f), L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_Translate_m3316827744(L_29, L_31, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_00ef:
	{
		float L_32 = V_3;
		float L_33 = __this->get_render_height_6();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_0117;
		}
	}
	{
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_35 = __this->get_render_height_6();
		Vector3_t2243707580  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m2720820983(&L_36, (0.0f), ((-L_35)), /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_Translate_m3316827744(L_34, L_36, /*hidden argument*/NULL);
	}

IL_0117:
	{
		return;
	}
}
// System.Void LayerBackground::OnValidate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t LayerBackground_OnValidate_m1491581041_MetadataUsageId;
extern "C"  void LayerBackground_OnValidate_m1491581041 (LayerBackground_t3535234259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LayerBackground_OnValidate_m1491581041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t1209076198 * V_0 = NULL;
	SpriteRendererU5BU5D_t1098056643* V_1 = NULL;
	int32_t V_2 = 0;
	{
		SpriteRendererU5BU5D_t1098056643* L_0 = __this->get__backgroundSprites_8();
		V_1 = L_0;
		V_2 = 0;
		goto IL_002d;
	}

IL_000e:
	{
		SpriteRendererU5BU5D_t1098056643* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		SpriteRenderer_t1209076198 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		SpriteRenderer_t1209076198 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0029;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_7 = V_0;
		Color_t2020392075  L_8 = __this->get__backgroundBlend_7();
		NullCheck(L_7);
		SpriteRenderer_set_color_m2339931967(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_2;
		SpriteRendererU5BU5D_t1098056643* L_11 = V_1;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void LayerBackground::Move()
extern "C"  void LayerBackground_Move_m4102430019 (LayerBackground_t3535234259 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3_t2243707580  L_0 = __this->get_cameraPosPrev_9();
		Camera_t189460977 * L_1 = __this->get_render_2();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		bool L_4 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		return;
	}

IL_0021:
	{
		Camera_t189460977 * L_5 = __this->get_render_2();
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_1();
		Vector3_t2243707580 * L_9 = __this->get_address_of_cameraPosPrev_9();
		float L_10 = L_9->get_x_1();
		V_0 = ((float)((float)L_8-(float)L_10));
		Camera_t189460977 * L_11 = __this->get_render_2();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_y_2();
		Vector3_t2243707580 * L_15 = __this->get_address_of_cameraPosPrev_9();
		float L_16 = L_15->get_y_2();
		V_2 = ((float)((float)L_14-(float)L_16));
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = L_17;
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		float L_20 = V_0;
		float L_21 = V_0;
		int32_t L_22 = __this->get_layer_4();
		float L_23 = V_2;
		float L_24 = V_2;
		int32_t L_25 = __this->get_layer_4();
		Vector3_t2243707580  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector3__ctor_m2720820983(&L_26, ((float)((float)L_20-(float)((float)((float)L_21/(float)(((float)((float)L_22))))))), ((float)((float)L_23-(float)((float)((float)L_24/(float)(((float)((float)L_25))))))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_19, L_26, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_position_m2469242620(L_18, L_27, /*hidden argument*/NULL);
		Camera_t189460977 * L_28 = __this->get_render_2();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		__this->set_cameraPosPrev_9(L_30);
		return;
	}
}
// System.Void MainCamera::.ctor()
extern "C"  void MainCamera__ctor_m417995029 (MainCamera_t1387594674 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single MainCamera::get_speed()
extern "C"  float MainCamera_get_speed_m4172356301 (MainCamera_t1387594674 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__speed_2();
		return L_0;
	}
}
// UnityEngine.Rigidbody2D MainCamera::get_rb()
extern const MethodInfo* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var;
extern const uint32_t MainCamera_get_rb_m4323075_MetadataUsageId;
extern "C"  Rigidbody2D_t502193897 * MainCamera_get_rb_m4323075 (MainCamera_t1387594674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCamera_get_rb_m4323075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_t502193897 * L_1 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_MethodInfo_var);
		return L_1;
	}
}
// System.Void MainCamera::ChangeDirection(UnityEngine.Vector3)
extern "C"  void MainCamera_ChangeDirection_m2501752491 (MainCamera_t1387594674 * __this, Vector3_t2243707580  ___d0, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = MainCamera_get_rb_m4323075(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_normalized_m936072361((&___d0), /*hidden argument*/NULL);
		float L_2 = MainCamera_get_speed_m4172356301(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainCamera::Stop()
extern "C"  void MainCamera_Stop_m2367445677 (MainCamera_t1387594674 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = MainCamera_get_rb_m4323075(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuOption::.ctor()
extern "C"  void MainMenuOption__ctor_m2839520338 (MainMenuOption_t4210779777 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenuOption::Goto()
extern "C"  void MainMenuOption_Goto_m1989634667 (MainMenuOption_t4210779777 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_sceneName_2();
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Matrix::.ctor()
extern "C"  void Matrix__ctor_m2389865400 (Matrix_t2094203845 * __this, const MethodInfo* method)
{
	{
		HIV_Piece__ctor_m2599885317(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Matrix::ChangeStats()
extern "C"  void Matrix_ChangeStats_m1571985379 (Matrix_t2094203845 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Membrane::.ctor()
extern "C"  void Membrane__ctor_m2226126012 (Membrane_t1623556481 * __this, const MethodInfo* method)
{
	{
		HIV_Piece__ctor_m2599885317(__this, /*hidden argument*/NULL);
		return;
	}
}
// Statistics Membrane::get_STAT_CHANGE()
extern "C"  Statistics_t2537377007  Membrane_get_STAT_CHANGE_m3156592102 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Statistics__ctor_m1821818148(&L_0, (4.0f), (0.0f), (0.0f), (0.0f), 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Membrane::ChangeStats()
extern "C"  void Membrane_ChangeStats_m548636383 (Membrane_t1623556481 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Statistics_t2537377007  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Color_t2020392075  L_0 = HIV_Piece_get_color_m1290788950(__this, /*hidden argument*/NULL);
		Color_RGBToHSV_m24058554(NULL /*static, unused*/, L_0, (&V_0), (&V_1), (&V_2), /*hidden argument*/NULL);
		Statistics_t2537377007 * L_1 = ((HIV_Piece_t1005595762 *)__this)->get_address_of__stats_4();
		Statistics_t2537377007  L_2 = Membrane_get_STAT_CHANGE_m3156592102(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3 = Statistics_get_Item_m3520282030((&V_3), 0, /*hidden argument*/NULL);
		float L_4 = V_0;
		float L_5 = V_0;
		float L_6 = V_0;
		Statistics_set_Item_m1870614441(L_1, 0, ((float)((float)L_3*(float)((float)((float)((float)((float)((float)((float)((float)((float)(9.0f)*(float)L_4))*(float)L_5))-(float)((float)((float)(9.0f)*(float)L_6))))+(float)(1.0f))))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Movement_Basic::.ctor()
extern "C"  void Movement_Basic__ctor_m1101094553 (Movement_Basic_t1705390416 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Movement_Basic::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t Movement_Basic_Awake_m3505725836_MetadataUsageId;
extern "C"  void Movement_Basic_Awake_m3505725836 (Movement_Basic_t1705390416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Movement_Basic_Awake_m3505725836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set__rb_3(L_0);
		Rigidbody2D_t502193897 * L_1 = __this->get__rb_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Movement_Basic::Start()
extern "C"  void Movement_Basic_Start_m22157681 (Movement_Basic_t1705390416 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__start_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Rigidbody2D_t502193897 * L_1 = __this->get__rb_3();
		Vector2_t2243707579  L_2 = __this->get__startingVelocity_5();
		NullCheck(L_1);
		Rigidbody2D_set_velocity_m3592751374(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Movement_Basic::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Movement_Basic_Update_m351315610_MetadataUsageId;
extern "C"  void Movement_Basic_Update_m351315610 (Movement_Basic_t1705390416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Movement_Basic_Update_m351315610_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get__rb_3();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_velocity_m3310151195(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_up_6();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetKey_m3849524999(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		(&V_0)->set_y_1((1.0f));
		goto IL_0049;
	}

IL_002d:
	{
		int32_t L_4 = __this->get_down_7();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKey_m3849524999(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		(&V_0)->set_y_1((-1.0f));
	}

IL_0049:
	{
		int32_t L_6 = __this->get_left_8();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetKey_m3849524999(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		(&V_0)->set_x_0((-1.0f));
		goto IL_0086;
	}

IL_006a:
	{
		int32_t L_8 = __this->get_right_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetKey_m3849524999(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0086;
		}
	}
	{
		(&V_0)->set_x_0((1.0f));
	}

IL_0086:
	{
		Rigidbody2D_t502193897 * L_10 = __this->get__rb_3();
		Vector2_t2243707579  L_11 = Vector2_get_normalized_m2985402409((&V_0), /*hidden argument*/NULL);
		float L_12 = __this->get_moveSpeed_2();
		Vector2_t2243707579  L_13 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		Rigidbody2D_set_velocity_m3592751374(L_10, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectMovement2D::.ctor()
extern "C"  void ObjectMovement2D__ctor_m2674233803 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// ObjectMovement2D/State ObjectMovement2D::get_followState()
extern "C"  int32_t ObjectMovement2D_get_followState_m1966773327 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CfollowStateU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void ObjectMovement2D::set_followState(ObjectMovement2D/State)
extern "C"  void ObjectMovement2D_set_followState_m2160966512 (ObjectMovement2D_t2992076704 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfollowStateU3Ek__BackingField_3(L_0);
		return;
	}
}
// UnityEngine.Vector2 ObjectMovement2D::get_velocity()
extern "C"  Vector2_t2243707579  ObjectMovement2D_get_velocity_m987348582 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get__rb_2();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_velocity_m3310151195(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ObjectMovement2D::set_velocity(UnityEngine.Vector2)
extern "C"  void ObjectMovement2D_set_velocity_m1559477545 (ObjectMovement2D_t2992076704 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get__rb_2();
		Vector2_t2243707579  L_1 = ___value0;
		NullCheck(L_0);
		Rigidbody2D_set_velocity_m3592751374(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ObjectMovement2D::get_maxSpeed()
extern "C"  float ObjectMovement2D_get_maxSpeed_m1185142611 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CmaxSpeedU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void ObjectMovement2D::set_maxSpeed(System.Single)
extern "C"  void ObjectMovement2D_set_maxSpeed_m780069868 (ObjectMovement2D_t2992076704 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CmaxSpeedU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single ObjectMovement2D::get_speed()
extern "C"  float ObjectMovement2D_get_speed_m1576568683 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get__rb_2();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_velocity_m3310151195(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single ObjectMovement2D::get_multiplier()
extern "C"  float ObjectMovement2D_get_multiplier_m3401748055 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CmultiplierU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void ObjectMovement2D::set_multiplier(System.Single)
extern "C"  void ObjectMovement2D_set_multiplier_m3706769178 (ObjectMovement2D_t2992076704 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CmultiplierU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void ObjectMovement2D::ChangeMultiplier(System.Single)
extern "C"  void ObjectMovement2D_ChangeMultiplier_m4218570319 (ObjectMovement2D_t2992076704 * __this, float ___a0, const MethodInfo* method)
{
	{
		float L_0 = ___a0;
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = ___a0;
		ObjectMovement2D_set_multiplier_m3706769178(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObjectMovement2D::ChangeFollowState(UnityEngine.Transform,System.Single,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ObjectMovement2D_ChangeFollowState_m1251427041_MetadataUsageId;
extern "C"  void ObjectMovement2D_ChangeFollowState_m1251427041 (ObjectMovement2D_t2992076704 * __this, Transform_t3275118058 * ___follow0, float ___multiple1, bool ___teleport2, bool ___trail3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectMovement2D_ChangeFollowState_m1251427041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectMovement2D_t2992076704 * G_B7_0 = NULL;
	ObjectMovement2D_t2992076704 * G_B6_0 = NULL;
	float G_B8_0 = 0.0f;
	ObjectMovement2D_t2992076704 * G_B8_1 = NULL;
	{
		Transform_t3275118058 * L_0 = ___follow0;
		__this->set_followObject_4(L_0);
		Transform_t3275118058 * L_1 = ___follow0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		ObjectMovement2D_set_followState_m2160966512(__this, 2, /*hidden argument*/NULL);
		bool L_3 = ___trail3;
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		Vector2_t2243707579  L_4 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectMovement2D_set_velocity_m1559477545(__this, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		goto IL_0038;
	}

IL_0031:
	{
		ObjectMovement2D_set_followState_m2160966512(__this, 0, /*hidden argument*/NULL);
	}

IL_0038:
	{
		float L_5 = ___multiple1;
		G_B6_0 = __this;
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_004e;
		}
	}
	{
		G_B8_0 = (1.0f);
		G_B8_1 = G_B6_0;
		goto IL_004f;
	}

IL_004e:
	{
		float L_6 = ___multiple1;
		G_B8_0 = L_6;
		G_B8_1 = G_B7_0;
	}

IL_004f:
	{
		NullCheck(G_B8_1);
		ObjectMovement2D_set_multiplier_m3706769178(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		bool L_7 = ___teleport2;
		__this->set_teleportEnabled_8(L_7);
		return;
	}
}
// System.Void ObjectMovement2D::ChangeFollowState(UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  void ObjectMovement2D_ChangeFollowState_m3934905037 (ObjectMovement2D_t2992076704 * __this, Vector2_t2243707579  ___follow0, float ___multiple1, bool ___teleport2, const MethodInfo* method)
{
	ObjectMovement2D_t2992076704 * G_B2_0 = NULL;
	ObjectMovement2D_t2992076704 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	ObjectMovement2D_t2992076704 * G_B3_1 = NULL;
	{
		ObjectMovement2D_set_followState_m2160966512(__this, 1, /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = ___follow0;
		Vector3_t2243707580  L_1 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_followVector_5(L_1);
		float L_2 = ___multiple1;
		G_B1_0 = __this;
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0029;
		}
	}
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0029:
	{
		float L_3 = ___multiple1;
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		ObjectMovement2D_set_multiplier_m3706769178(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		bool L_4 = ___teleport2;
		__this->set_teleportEnabled_8(L_4);
		return;
	}
}
// System.Void ObjectMovement2D::MoveTo(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t ObjectMovement2D_MoveTo_m3011449144_MetadataUsageId;
extern "C"  void ObjectMovement2D_MoveTo_m3011449144 (ObjectMovement2D_t2992076704 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectMovement2D_MoveTo_m3011449144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_teleportEnabled_8();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___pos0;
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_2, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_001c:
	{
		Vector3_t2243707580  L_3 = ___pos0;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		float L_8 = ObjectMovement2D_get_maxSpeed_m1185142611(__this, /*hidden argument*/NULL);
		if ((!(((float)L_7) < ((float)((float)((float)L_8/(float)(50.0f)))))))
		{
			goto IL_0077;
		}
	}
	{
		Vector3_t2243707580  L_9 = Vector3_get_normalized_m936072361((&V_0), /*hidden argument*/NULL);
		float L_10 = ObjectMovement2D_get_speed_m1576568683(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_10, (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		ObjectMovement2D_set_velocity_m1559477545(__this, L_13, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_0077:
	{
		Rigidbody2D_t502193897 * L_14 = __this->get__rb_2();
		Vector3_t2243707580  L_15 = Vector3_get_normalized_m936072361((&V_0), /*hidden argument*/NULL);
		float L_16 = ObjectMovement2D_get_speed_m1576568683(__this, /*hidden argument*/NULL);
		float L_17 = ObjectMovement2D_get_maxSpeed_m1185142611(__this, /*hidden argument*/NULL);
		float L_18 = ObjectMovement2D_get_multiplier_m3401748055(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_19 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_16, ((float)((float)L_17*(float)L_18)), (0.1f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
		Vector2_t2243707579  L_21 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_14);
		Rigidbody2D_set_velocity_m3592751374(L_14, L_21, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void ObjectMovement2D::Awake()
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var;
extern const uint32_t ObjectMovement2D_Awake_m3303377024_MetadataUsageId;
extern "C"  void ObjectMovement2D_Awake_m3303377024 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectMovement2D_Awake_m3303377024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_MethodInfo_var);
		__this->set__rb_2(L_0);
		return;
	}
}
// System.Void ObjectMovement2D::Start()
extern "C"  void ObjectMovement2D_Start_m2558058847 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		float L_0 = ObjectMovement2D_get_maxSpeed_m1185142611(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_001b;
		}
	}
	{
		ObjectMovement2D_set_maxSpeed_m780069868(__this, (50.0f), /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void ObjectMovement2D::Move()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ObjectMovement2D_Move_m276962098_MetadataUsageId;
extern "C"  void ObjectMovement2D_Move_m276962098 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectMovement2D_Move_m276962098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ObjectMovement2D_get_followState_m1966773327(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_006d;
		}
	}
	{
		goto IL_007e;
	}

IL_0019:
	{
		Transform_t3275118058 * L_3 = __this->get_followObject_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0054;
		}
	}
	{
		Transform_t3275118058 * L_5 = __this->get_followObject_4();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m4242915935(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0054;
		}
	}
	{
		Transform_t3275118058 * L_8 = __this->get_followObject_4();
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		ObjectMovement2D_MoveTo_m3011449144(__this, L_9, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_0054:
	{
		float L_10 = ObjectMovement2D_get_multiplier_m3401748055(__this, /*hidden argument*/NULL);
		bool L_11 = __this->get_teleportEnabled_8();
		ObjectMovement2D_ChangeFollowState_m1251427041(__this, (Transform_t3275118058 *)NULL, L_10, L_11, (bool)1, /*hidden argument*/NULL);
	}

IL_0068:
	{
		goto IL_007e;
	}

IL_006d:
	{
		Vector3_t2243707580  L_12 = __this->get_followVector_5();
		ObjectMovement2D_MoveTo_m3011449144(__this, L_12, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_007e:
	{
		return;
	}
}
// System.Void ObjectMovement2D::FixedUpdate()
extern "C"  void ObjectMovement2D_FixedUpdate_m1518841780 (ObjectMovement2D_t2992076704 * __this, const MethodInfo* method)
{
	{
		ObjectMovement2D_Move_m276962098(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PairList::.ctor()
extern "C"  void PairList__ctor_m1786578395 (PairList_t3336234724 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PairList::get_count()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const uint32_t PairList_get_count_m1467844419_MetadataUsageId;
extern "C"  int32_t PairList_get_count_m1467844419 (PairList_t3336234724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PairList_get_count_m1467844419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1125654279 * L_0 = __this->get_objectList_3();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		List_1_t1125654279 * L_1 = __this->get_objectList_3();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2764296230(L_1, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		return L_2;
	}
}
// TCell_Pair PairList::get_Item(System.Int32)
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_Pair_t2779000105_m1112822816_MethodInfo_var;
extern const uint32_t PairList_get_Item_m1768557300_MetadataUsageId;
extern "C"  TCell_Pair_t2779000105 * PairList_get_Item_m1768557300 (PairList_t3336234724 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PairList_get_Item_m1768557300_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TCell_Pair_t2779000105 * G_B4_0 = NULL;
	{
		List_1_t1125654279 * L_0 = __this->get_objectList_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = ___i0;
		int32_t L_2 = PairList_get_count_m1467844419(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0017:
	{
		G_B4_0 = ((TCell_Pair_t2779000105 *)(NULL));
		goto IL_002e;
	}

IL_001d:
	{
		List_1_t1125654279 * L_3 = __this->get_objectList_3();
		int32_t L_4 = ___i0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_5 = List_1_get_Item_m939767277(L_3, L_4, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_5);
		TCell_Pair_t2779000105 * L_6 = GameObject_GetComponent_TisTCell_Pair_t2779000105_m1112822816(L_5, /*hidden argument*/GameObject_GetComponent_TisTCell_Pair_t2779000105_m1112822816_MethodInfo_var);
		G_B4_0 = L_6;
	}

IL_002e:
	{
		return G_B4_0;
	}
}
// System.Void PairList::ChangeTropism()
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t PairList_ChangeTropism_m453644655_MetadataUsageId;
extern "C"  void PairList_ChangeTropism_m453644655 (PairList_t3336234724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PairList_ChangeTropism_m453644655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001c;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		TCell_Pair_t2779000105 * L_1 = PairList_get_Item_m1768557300(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Tropism_t3662836552  L_2 = Tropism_Random_m466220907(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		TCell_Pair_set_tropism_m3515168797(L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_4 = V_0;
		int32_t L_5 = PairList_get_count_m1467844419(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PairList::Instantiate(UnityEngine.Transform,System.Single)
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldAccessException_t1797813379_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const uint32_t PairList_Instantiate_m1745309583_MetadataUsageId;
extern "C"  void PairList_Instantiate_m1745309583 (PairList_t3336234724 * __this, Transform_t3275118058 * ___parent0, float ___radius1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PairList_Instantiate_m1745309583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	TCell_Pair_t2779000105 * V_2 = NULL;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		bool L_0 = __this->get__instantiated_2();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t1125654279 * L_1 = __this->get_objectList_3();
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		List_1_t1125654279 * L_2 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_2, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set_objectList_3(L_2);
	}

IL_0022:
	{
		V_0 = 0;
		goto IL_017c;
	}

IL_0029:
	{
		List_1_t1125654279 * L_3 = __this->get_objectList_3();
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m2764296230(L_3, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0135;
		}
	}
	{
		GameObject_t1756533147 * L_6 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_7 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_1 = L_7;
		List_1_t1125654279 * L_8 = __this->get_objectList_3();
		GameObject_t1756533147 * L_9 = V_1;
		NullCheck(L_8);
		List_1_Add_m3441471442(L_8, L_9, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		int32_t L_10 = PairList_get_count_m1467844419(__this, /*hidden argument*/NULL);
		TCell_Pair_t2779000105 * L_11 = PairList_get_Item_m1768557300(__this, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_11;
		TCell_Pair_t2779000105 * L_12 = V_2;
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_012a;
		}
	}
	{
		Transform_t3275118058 * L_14 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0085;
		}
	}
	{
		TCell_Pair_t2779000105 * L_16 = V_2;
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(L_16, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = ___parent0;
		NullCheck(L_17);
		Transform_SetParent_m4124909910(L_17, L_18, /*hidden argument*/NULL);
	}

IL_0085:
	{
		TCell_Pair_t2779000105 * L_19 = V_2;
		NullCheck(L_19);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(L_19, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t2243707580  L_21 = V_3;
		NullCheck(L_20);
		Transform_set_localPosition_m1026930133(L_20, L_21, /*hidden argument*/NULL);
		uint32_t L_22 = __this->get__count_1();
		uint32_t L_23 = __this->get__count_1();
		float L_24 = Random_Range_m2884721203(NULL /*static, unused*/, ((float)((float)(-90.0f)/(float)(((float)((float)(((double)((uint32_t)L_22)))))))), ((float)((float)(90.0f)/(float)(((float)((float)(((double)((uint32_t)L_23)))))))), /*hidden argument*/NULL);
		V_4 = L_24;
		int32_t L_25 = V_0;
		uint32_t L_26 = __this->get__count_1();
		float L_27 = V_4;
		V_5 = ((float)((float)((float)((float)((float)((float)(((float)((float)L_25)))/(float)(((float)((float)(((double)((uint32_t)L_26))))))))*(float)(360.0f)))+(float)L_27));
		TCell_Pair_t2779000105 * L_28 = V_2;
		float L_29 = ___radius1;
		float L_30 = V_5;
		Quaternion_t4030073918  L_31 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		TCell_Pair_MovePivotAndRotate_m1475944733(L_28, L_29, L_31, /*hidden argument*/NULL);
		float L_32 = ___radius1;
		if ((((float)L_32) == ((float)(0.0f))))
		{
			goto IL_011a;
		}
	}
	{
		TCell_Pair_t2779000105 * L_33 = V_2;
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(L_33, /*hidden argument*/NULL);
		float L_35 = ___radius1;
		float L_36 = ___radius1;
		float L_37 = ___radius1;
		Vector3_t2243707580  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector3__ctor_m2638739322(&L_38, ((float)((float)(10.0f)/(float)L_35)), ((float)((float)(10.0f)/(float)L_36)), ((float)((float)(10.0f)/(float)L_37)), /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_localScale_m2325460848(L_34, L_38, /*hidden argument*/NULL);
	}

IL_011a:
	{
		TCell_Pair_t2779000105 * L_39 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Tropism_t3662836552  L_40 = Tropism_Random_m466220907(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		TCell_Pair_set_tropism_m3515168797(L_39, L_40, /*hidden argument*/NULL);
		goto IL_0130;
	}

IL_012a:
	{
		FieldAccessException_t1797813379 * L_41 = (FieldAccessException_t1797813379 *)il2cpp_codegen_object_new(FieldAccessException_t1797813379_il2cpp_TypeInfo_var);
		FieldAccessException__ctor_m1240495912(L_41, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_41);
	}

IL_0130:
	{
		goto IL_0178;
	}

IL_0135:
	{
		List_1_t1125654279 * L_42 = __this->get_objectList_3();
		int32_t L_43 = V_0;
		NullCheck(L_42);
		GameObject_t1756533147 * L_44 = List_1_get_Item_m939767277(L_42, L_43, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_44);
		bool L_45 = GameObject_get_activeSelf_m313590879(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0178;
		}
	}
	{
		List_1_t1125654279 * L_46 = __this->get_objectList_3();
		int32_t L_47 = V_0;
		NullCheck(L_46);
		GameObject_t1756533147 * L_48 = List_1_get_Item_m939767277(L_46, L_47, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_48);
		GameObject_SetActive_m2887581199(L_48, (bool)1, /*hidden argument*/NULL);
		List_1_t1125654279 * L_49 = __this->get_objectList_3();
		int32_t L_50 = V_0;
		NullCheck(L_49);
		GameObject_t1756533147 * L_51 = List_1_get_Item_m939767277(L_49, L_50, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = GameObject_get_transform_m909382139(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = Transform_get_parent_m147407266(L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
	}

IL_0178:
	{
		int32_t L_54 = V_0;
		V_0 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_017c:
	{
		int32_t L_55 = V_0;
		uint32_t L_56 = __this->get__count_1();
		if ((((int64_t)(((int64_t)((int64_t)L_55)))) < ((int64_t)(((int64_t)((uint64_t)L_56))))))
		{
			goto IL_0029;
		}
	}
	{
		__this->set__instantiated_2((bool)1);
		return;
	}
}
// System.Void PairList::Instantiate(UnityEngine.Transform,System.Single,System.UInt32)
extern "C"  void PairList_Instantiate_m1391603615 (PairList_t3336234724 * __this, Transform_t3275118058 * ___parent0, float ___radius1, uint32_t ___c2, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___c2;
		__this->set__count_1(L_0);
		Transform_t3275118058 * L_1 = ___parent0;
		float L_2 = ___radius1;
		PairList_Instantiate_m1745309583(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PairList::Scale(System.Single)
extern "C"  void PairList_Scale_m1647140082 (PairList_t3336234724 * __this, float ___a0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___a0;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = 0;
		goto IL_0024;
	}

IL_0013:
	{
		int32_t L_1 = V_0;
		TCell_Pair_t2779000105 * L_2 = PairList_get_Item_m1768557300(__this, L_1, /*hidden argument*/NULL);
		float L_3 = ___a0;
		NullCheck(L_2);
		TCell_Pair_Scale_m1323056481(L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = PairList_get_count_m1467844419(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void PairList::Disable()
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const uint32_t PairList_Disable_m1506047595_MetadataUsageId;
extern "C"  void PairList_Disable_m1506047595 (PairList_t3336234724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PairList_Disable_m1506047595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Enumerator_t660383953  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1125654279 * L_0 = __this->get_objectList_3();
		NullCheck(L_0);
		Enumerator_t660383953  L_1 = List_1_GetEnumerator_m970439620(L_0, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_0011:
		{
			GameObject_t1756533147 * L_2 = Enumerator_get_Current_m2389888842((&V_1), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_0 = L_2;
			GameObject_t1756533147 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		}

IL_0020:
		{
			bool L_4 = Enumerator_MoveNext_m3635305532((&V_1), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_1), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void PairList::Enable(TCell)
extern const MethodInfo* List_1_GetEnumerator_m970439620_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2389888842_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3635305532_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1242097970_MethodInfo_var;
extern const uint32_t PairList_Enable_m180892362_MetadataUsageId;
extern "C"  void PairList_Enable_m180892362 (PairList_t3336234724 * __this, TCell_t1815110036 * ___parent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PairList_Enable_m180892362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Enumerator_t660383953  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1125654279 * L_0 = __this->get_objectList_3();
		NullCheck(L_0);
		Enumerator_t660383953  L_1 = List_1_GetEnumerator_m970439620(L_0, /*hidden argument*/List_1_GetEnumerator_m970439620_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0011:
		{
			GameObject_t1756533147 * L_2 = Enumerator_get_Current_m2389888842((&V_1), /*hidden argument*/Enumerator_get_Current_m2389888842_MethodInfo_var);
			V_0 = L_2;
			GameObject_t1756533147 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_4 = V_0;
			NullCheck(L_4);
			Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
			TCell_t1815110036 * L_6 = ___parent0;
			NullCheck(L_6);
			Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			Transform_SetParent_m4124909910(L_5, L_7, /*hidden argument*/NULL);
		}

IL_0031:
		{
			bool L_8 = Enumerator_MoveNext_m3635305532((&V_1), /*hidden argument*/Enumerator_MoveNext_m3635305532_MethodInfo_var);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_1), /*hidden argument*/Enumerator_Dispose_m1242097970_MethodInfo_var);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0050:
	{
		return;
	}
}
// System.Void PauseMenuButton::.ctor()
extern "C"  void PauseMenuButton__ctor_m2460052464 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenuButton::RestartLevel()
extern "C"  void PauseMenuButton_RestartLevel_m547918073 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		Scene_t1684909666  L_0 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3735680091((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenuButton::Restart(System.Single)
extern Il2CppCodeGenString* _stringLiteral1833427585;
extern const uint32_t PauseMenuButton_Restart_m1700954672_MetadataUsageId;
extern "C"  void PauseMenuButton_Restart_m1700954672 (PauseMenuButton_t2369929669 * __this, float ___delay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenuButton_Restart_m1700954672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___delay0;
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		PauseMenuButton_RestartLevel_m547918073(__this, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_0016:
	{
		float L_1 = ___delay0;
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral1833427585, L_1, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void PauseMenuButton::Quit()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2812316625;
extern const uint32_t PauseMenuButton_Quit_m2957212369_MetadataUsageId;
extern "C"  void PauseMenuButton_Quit_m2957212369 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenuButton_Quit_m2957212369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2812316625, /*hidden argument*/NULL);
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenuButton::ToMenu()
extern "C"  void PauseMenuButton_ToMenu_m183229112 (PauseMenuButton_t2369929669 * __this, const MethodInfo* method)
{
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenuQuit::.ctor()
extern "C"  void PauseMenuQuit__ctor_m1289761379 (PauseMenuQuit_t564462074 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenuQuit::QuitConfirm(UnityEngine.GameObject)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisQuitConfirm_t3413857337_m1137943888_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382587364;
extern const uint32_t PauseMenuQuit_QuitConfirm_m1046657798_MetadataUsageId;
extern "C"  void PauseMenuQuit_QuitConfirm_m1046657798 (PauseMenuQuit_t564462074 * __this, GameObject_t1756533147 * ___quitConfirm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenuQuit_QuitConfirm_m1046657798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	ButtonU5BU5D_t3071100561* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2382587364, /*hidden argument*/NULL);
		ButtonU5BU5D_t3071100561* L_0 = __this->get_activationList_2();
		V_1 = L_0;
		V_2 = 0;
		goto IL_0027;
	}

IL_0018:
	{
		ButtonU5BU5D_t3071100561* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Button_t2872111280 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		Button_t2872111280 * L_5 = V_0;
		NullCheck(L_5);
		Selectable_set_interactable_m63718297(L_5, (bool)0, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_7 = V_2;
		ButtonU5BU5D_t3071100561* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		GameObject_t1756533147 * L_9 = ___quitConfirm0;
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = ___quitConfirm0;
		NullCheck(L_10);
		QuitConfirm_t3413857337 * L_11 = GameObject_GetComponent_TisQuitConfirm_t3413857337_m1137943888(L_10, /*hidden argument*/GameObject_GetComponent_TisQuitConfirm_t3413857337_m1137943888_MethodInfo_var);
		ButtonU5BU5D_t3071100561* L_12 = __this->get_activationList_2();
		NullCheck(L_11);
		L_11->set_buttonList_2(L_12);
		return;
	}
}
// System.Void Pivot::.ctor()
extern "C"  void Pivot__ctor_m4141519939 (Pivot_t2110476880 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject Pivot::get_parent()
extern "C"  GameObject_t1756533147 * Pivot_get_parent_m3646862489 (Pivot_t2110476880 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get__parent_2();
		return L_0;
	}
}
// System.Void Pivot::MoveAbout(System.Single,UnityEngine.Quaternion)
extern "C"  void Pivot_MoveAbout_m3407021153 (Pivot_t2110476880 * __this, float ___radius0, Quaternion_t4030073918  ___angle1, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = ___radius0;
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, (0.0f), L_1, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localPosition_m1026930133(L_0, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = Pivot_get_parent_m3646862489(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = ___angle1;
		NullCheck(L_4);
		Transform_set_localRotation_m2055111962(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pivot::MoveAbout(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Pivot_MoveAbout_m999237689 (Pivot_t2110476880 * __this, Vector3_t2243707580  ___translation0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___translation0;
		NullCheck(L_0);
		Transform_set_localPosition_m1026930133(L_0, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = Pivot_get_parent_m3646862489(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_4 = ___rotation1;
		NullCheck(L_3);
		Transform_set_localRotation_m2055111962(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitButton::.ctor()
extern "C"  void QuitButton__ctor_m2821854874 (QuitButton_t1107985349 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitButton::Quit()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2812316625;
extern const uint32_t QuitButton_Quit_m720958885_MetadataUsageId;
extern "C"  void QuitButton_Quit_m720958885 (QuitButton_t1107985349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuitButton_Quit_m720958885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2812316625, /*hidden argument*/NULL);
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitButton::QuitConfirm(UnityEngine.GameObject)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382587364;
extern const uint32_t QuitButton_QuitConfirm_m3258154715_MetadataUsageId;
extern "C"  void QuitButton_QuitConfirm_m3258154715 (QuitButton_t1107985349 * __this, GameObject_t1756533147 * ___quitConfirm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuitButton_QuitConfirm_m3258154715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2382587364, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = ___quitConfirm0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitButton::PlaySound()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern const uint32_t QuitButton_PlaySound_m443867319_MetadataUsageId;
extern "C"  void QuitButton_PlaySound_m443867319 (QuitButton_t1107985349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuitButton_PlaySound_m443867319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AudioSource_t1135106623 * V_0 = NULL;
	{
		AudioSource_t1135106623 * L_0 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		V_0 = L_0;
		AudioSource_t1135106623 * L_1 = V_0;
		NullCheck(L_1);
		AudioSource_Play_m353744792(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitConfirm::.ctor()
extern "C"  void QuitConfirm__ctor_m948758264 (QuitConfirm_t3413857337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitConfirm::Deactivate()
extern "C"  void QuitConfirm_Deactivate_m2175662016 (QuitConfirm_t3413857337 * __this, const MethodInfo* method)
{
	Button_t2872111280 * V_0 = NULL;
	ButtonU5BU5D_t3071100561* V_1 = NULL;
	int32_t V_2 = 0;
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_buttonList_2();
		V_1 = L_0;
		V_2 = 0;
		goto IL_001d;
	}

IL_000e:
	{
		ButtonU5BU5D_t3071100561* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Button_t2872111280 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		Button_t2872111280 * L_5 = V_0;
		NullCheck(L_5);
		Selectable_set_interactable_m63718297(L_5, (bool)1, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_7 = V_2;
		ButtonU5BU5D_t3071100561* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void QuitConfirm::Update()
extern Il2CppClass* Controller_t1937198888_il2cpp_TypeInfo_var;
extern const uint32_t QuitConfirm_Update_m3411984893_MetadataUsageId;
extern "C"  void QuitConfirm_Update_m3411984893 (QuitConfirm_t3413857337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QuitConfirm_Update_m3411984893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Controller_t1937198888 * L_0 = ((Controller_t1937198888_StaticFields*)Controller_t1937198888_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		NullCheck(L_0);
		bool L_1 = Controller_GetBack_m1477624146(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		QuitConfirm_Deactivate_m2175662016(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void ReadOnlyAttribute::.ctor()
extern "C"  void ReadOnlyAttribute__ctor_m4159498297 (ReadOnlyAttribute_t3809417938 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RedBloodCell::.ctor()
extern "C"  void RedBloodCell__ctor_m2035063800 (RedBloodCell_t2376799177 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// CellMovement RedBloodCell::get_move()
extern "C"  CellMovement_t2110243311 * RedBloodCell_get_move_m849695286 (RedBloodCell_t2376799177 * __this, const MethodInfo* method)
{
	{
		CellMovement_t2110243311 * L_0 = __this->get_U3CmoveU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void RedBloodCell::set_move(CellMovement)
extern "C"  void RedBloodCell_set_move_m1320209217 (RedBloodCell_t2376799177 * __this, CellMovement_t2110243311 * ___value0, const MethodInfo* method)
{
	{
		CellMovement_t2110243311 * L_0 = ___value0;
		__this->set_U3CmoveU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Single RedBloodCell::get_radius()
extern "C"  float RedBloodCell_get_radius_m195900731 (RedBloodCell_t2376799177 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__radius_4();
		return L_0;
	}
}
// System.Void RedBloodCell::set_radius(System.Single)
extern "C"  void RedBloodCell_set_radius_m2966666374 (RedBloodCell_t2376799177 * __this, float ___value0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		float L_2 = ___value0;
		float L_3 = ___value0;
		Vector3_t2243707580  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2638739322(&L_4, ((float)((float)L_2/(float)(1.5f))), ((float)((float)L_3/(float)(1.5f))), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m2325460848(L_1, L_4, /*hidden argument*/NULL);
		float L_5 = ___value0;
		__this->set__radius_4(L_5);
		return;
	}
}
// System.Void RedBloodCell::Awake()
extern const MethodInfo* Component_GetComponent_TisCellMovement_t2110243311_m1786295194_MethodInfo_var;
extern const uint32_t RedBloodCell_Awake_m2661662197_MetadataUsageId;
extern "C"  void RedBloodCell_Awake_m2661662197 (RedBloodCell_t2376799177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RedBloodCell_Awake_m2661662197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CellMovement_t2110243311 * L_0 = Component_GetComponent_TisCellMovement_t2110243311_m1786295194(__this, /*hidden argument*/Component_GetComponent_TisCellMovement_t2110243311_m1786295194_MethodInfo_var);
		RedBloodCell_set_move_m1320209217(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RedBloodCell::Start()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RedBloodCell_Start_m1748079816_MetadataUsageId;
extern "C"  void RedBloodCell_Start_m1748079816 (RedBloodCell_t2376799177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RedBloodCell_Start_m1748079816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void RedBloodCell::LateUpdate()
extern "C"  void RedBloodCell_LateUpdate_m1498019821 (RedBloodCell_t2376799177 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_3 = __this->get__disableRect_5();
		Rect_t3681755626  L_4 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		bool L_5 = Functions_OutsideRect_m364063437(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void RenderCamera::.ctor()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t RenderCamera__ctor_m4086194934_MetadataUsageId;
extern "C"  void RenderCamera__ctor_m4086194934 (RenderCamera_t2596733641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderCamera__ctor_m4086194934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		__this->set_zoom_4((1.0f));
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_0 = V_0;
		__this->set_difference_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 RenderCamera::get_origin()
extern Il2CppCodeGenString* _stringLiteral2115673192;
extern const uint32_t RenderCamera_get_origin_m2137376627_MetadataUsageId;
extern "C"  Vector3_t2243707580  RenderCamera_get_origin_m2137376627 (RenderCamera_t2596733641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderCamera_get_origin_m2137376627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral2115673192, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single RenderCamera::GetZoomHeight(System.Single)
extern "C"  float RenderCamera_GetZoomHeight_m3395087457 (RenderCamera_t2596733641 * __this, float ___zoom0, const MethodInfo* method)
{
	{
		float L_0 = ___zoom0;
		if ((!(((float)L_0) < ((float)(1.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		___zoom0 = (1.0f);
	}

IL_0012:
	{
		float L_1 = ___zoom0;
		return ((float)((float)(40.0f)/(float)L_1));
	}
}
// System.Void RenderCamera::Move()
extern "C"  void RenderCamera_Move_m2624179241 (RenderCamera_t2596733641 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = RenderCamera_get_origin_m2137376627(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = __this->get_difference_5();
		Vector3_t2243707580  L_3 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RenderCamera::Zoom(System.Single)
extern "C"  void RenderCamera_Zoom_m1502878588 (RenderCamera_t2596733641 * __this, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		if ((!(((float)L_0) < ((float)(1.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		___f0 = (1.0f);
	}

IL_0012:
	{
		Camera_t189460977 * L_1 = __this->get_cam_6();
		float L_2 = ___f0;
		NullCheck(L_1);
		Camera_set_orthographicSize_m2708824189(L_1, ((float)((float)(40.0f)/(float)L_2)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void RenderCamera::Awake()
extern Il2CppClass* RenderCamera_t2596733641_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const uint32_t RenderCamera_Awake_m1677413369_MetadataUsageId;
extern "C"  void RenderCamera_Awake_m1677413369 (RenderCamera_t2596733641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderCamera_Awake_m1677413369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		__this->set_cam_6(L_0);
		Camera_t189460977 * L_1 = __this->get_cam_6();
		((RenderCamera_t2596733641_StaticFields*)RenderCamera_t2596733641_il2cpp_TypeInfo_var->static_fields)->set_main_2(L_1);
		return;
	}
}
// System.Void RenderCamera::FixedUpdate()
extern "C"  void RenderCamera_FixedUpdate_m3495663039 (RenderCamera_t2596733641 * __this, const MethodInfo* method)
{
	{
		RenderCamera_Move_m2624179241(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RNA::.ctor()
extern "C"  void RNA__ctor_m1057206074 (RNA_t915683991 * __this, const MethodInfo* method)
{
	{
		HIV_Piece__ctor_m2599885317(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RNA::ChangeStats()
extern "C"  void RNA_ChangeStats_m3690331269 (RNA_t915683991 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Rotate::.ctor()
extern "C"  void Rotate__ctor_m1487335446 (Rotate_t4255939431 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotate::FixedUpdate()
extern "C"  void Rotate_FixedUpdate_m2114026893 (Rotate_t4255939431 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_rotationSpeed_2();
		NullCheck(L_0);
		Transform_Rotate_m4255273365(L_0, (0.0f), (0.0f), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RT::.ctor()
extern "C"  void RT__ctor_m3029194389 (RT_t1716568150 * __this, const MethodInfo* method)
{
	{
		HIV_Piece__ctor_m2599885317(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RT::ChangeStats()
extern "C"  void RT_ChangeStats_m92326530 (RT_t1716568150 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SceneMove::.ctor()
extern "C"  void SceneMove__ctor_m874022028 (SceneMove_t3418568293 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneMove::Start()
extern "C"  void SceneMove_Start_m2831273516 (SceneMove_t3418568293 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_sceneName_2();
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Score::.ctor(System.Int32)
extern "C"  void Score__ctor_m51016996 (Score_t1518975274 * __this, int32_t ___s0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___s0;
		__this->set_score_0(L_0);
		return;
	}
}
extern "C"  void Score__ctor_m51016996_AdjustorThunk (Il2CppObject * __this, int32_t ___s0, const MethodInfo* method)
{
	Score_t1518975274 * _thisAdjusted = reinterpret_cast<Score_t1518975274 *>(__this + 1);
	Score__ctor_m51016996(_thisAdjusted, ___s0, method);
}
// System.Boolean Score::Equals(System.Object)
extern Il2CppClass* Score_t1518975274_il2cpp_TypeInfo_var;
extern const uint32_t Score_Equals_m1363800802_MetadataUsageId;
extern "C"  bool Score_Equals_m1363800802 (Score_t1518975274 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Score_Equals_m1363800802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Score_t1518975274  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___o0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		int32_t L_2 = Score_GetHashCode_m195669770(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}
	{
		return (bool)0;
	}

IL_0019:
	{
		Il2CppObject * L_3 = ___o0;
		V_0 = ((*(Score_t1518975274 *)((Score_t1518975274 *)UnBox (L_3, Score_t1518975274_il2cpp_TypeInfo_var))));
		int32_t L_4 = (&V_0)->get_score_0();
		int32_t L_5 = __this->get_score_0();
		return (bool)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
	}
}
extern "C"  bool Score_Equals_m1363800802_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	Score_t1518975274 * _thisAdjusted = reinterpret_cast<Score_t1518975274 *>(__this + 1);
	return Score_Equals_m1363800802(_thisAdjusted, ___o0, method);
}
// System.Int32 Score::GetHashCode()
extern Il2CppClass* Score_t1518975274_il2cpp_TypeInfo_var;
extern const uint32_t Score_GetHashCode_m195669770_MetadataUsageId;
extern "C"  int32_t Score_GetHashCode_m195669770 (Score_t1518975274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Score_GetHashCode_m195669770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Score_t1518975274  L_0 = (*(Score_t1518975274 *)__this);
		Il2CppObject * L_1 = Box(Score_t1518975274_il2cpp_TypeInfo_var, &L_0);
		NullCheck((ValueType_t3507792607 *)L_1);
		int32_t L_2 = ValueType_GetHashCode_m3018627007((ValueType_t3507792607 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Score_GetHashCode_m195669770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Score_t1518975274 * _thisAdjusted = reinterpret_cast<Score_t1518975274 *>(__this + 1);
	return Score_GetHashCode_m195669770(_thisAdjusted, method);
}
// System.Boolean Score::LessThan(Score)
extern "C"  bool Score_LessThan_m1927128681 (Score_t1518975274 * __this, Score_t1518975274  ___s0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_score_0();
		int32_t L_1 = (&___s0)->get_score_0();
		return (bool)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
	}
}
extern "C"  bool Score_LessThan_m1927128681_AdjustorThunk (Il2CppObject * __this, Score_t1518975274  ___s0, const MethodInfo* method)
{
	Score_t1518975274 * _thisAdjusted = reinterpret_cast<Score_t1518975274 *>(__this + 1);
	return Score_LessThan_m1927128681(_thisAdjusted, ___s0, method);
}
// System.Boolean Score::op_Equality(Score,Score)
extern Il2CppClass* Score_t1518975274_il2cpp_TypeInfo_var;
extern const uint32_t Score_op_Equality_m2255379791_MetadataUsageId;
extern "C"  bool Score_op_Equality_m2255379791 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Score_op_Equality_m2255379791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Score_t1518975274  L_0 = ___b1;
		Score_t1518975274  L_1 = L_0;
		Il2CppObject * L_2 = Box(Score_t1518975274_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = Score_Equals_m1363800802((&___a0), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Score::op_LessThan(Score,Score)
extern "C"  bool Score_op_LessThan_m485616521 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method)
{
	{
		Score_t1518975274  L_0 = ___b1;
		bool L_1 = Score_LessThan_m1927128681((&___a0), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Score::op_GreaterThan(Score,Score)
extern "C"  bool Score_op_GreaterThan_m2736154516 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method)
{
	{
		Score_t1518975274  L_0 = ___a0;
		bool L_1 = Score_LessThan_m1927128681((&___b1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Score::op_Inequality(Score,Score)
extern "C"  bool Score_op_Inequality_m423302070 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method)
{
	{
		Score_t1518975274  L_0 = ___a0;
		Score_t1518975274  L_1 = ___b1;
		bool L_2 = Score_op_Equality_m2255379791(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Score::op_LessThanOrEqual(Score,Score)
extern "C"  bool Score_op_LessThanOrEqual_m3420977628 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Score_t1518975274  L_0 = ___a0;
		Score_t1518975274  L_1 = ___b1;
		bool L_2 = Score_op_LessThan_m485616521(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		Score_t1518975274  L_3 = ___a0;
		Score_t1518975274  L_4 = ___b1;
		bool L_5 = Score_op_Equality_m2255379791(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
	}

IL_0016:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Score::op_GreaterThanOrEqual(Score,Score)
extern "C"  bool Score_op_GreaterThanOrEqual_m2728979991 (Il2CppObject * __this /* static, unused */, Score_t1518975274  ___a0, Score_t1518975274  ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Score_t1518975274  L_0 = ___a0;
		Score_t1518975274  L_1 = ___b1;
		bool L_2 = Score_op_GreaterThan_m2736154516(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		Score_t1518975274  L_3 = ___a0;
		Score_t1518975274  L_4 = ___b1;
		bool L_5 = Score_op_Equality_m2255379791(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
	}

IL_0016:
	{
		return (bool)G_B3_0;
	}
}
// System.String Score::ToString()
extern "C"  String_t* Score_ToString_m2662655366 (Score_t1518975274 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_score_0();
		String_t* L_1 = Int32_ToString_m2960866144(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* Score_ToString_m2662655366_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Score_t1518975274 * _thisAdjusted = reinterpret_cast<Score_t1518975274 *>(__this + 1);
	return Score_ToString_m2662655366(_thisAdjusted, method);
}
// System.Void ScoreAdder::.ctor()
extern "C"  void ScoreAdder__ctor_m4120211049 (ScoreAdder_t1918699446 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreAdder::Awake()
extern const MethodInfo* Component_GetComponent_TisInputField_t1631627530_m1177654614_MethodInfo_var;
extern const uint32_t ScoreAdder_Awake_m454142246_MetadataUsageId;
extern "C"  void ScoreAdder_Awake_m454142246 (ScoreAdder_t1918699446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreAdder_Awake_m454142246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t1631627530 * L_0 = Component_GetComponent_TisInputField_t1631627530_m1177654614(__this, /*hidden argument*/Component_GetComponent_TisInputField_t1631627530_m1177654614_MethodInfo_var);
		__this->set_input_2(L_0);
		return;
	}
}
// System.Void ScoreAdder::UpdateScoreList()
extern Il2CppClass* HighScores_t3229912699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral62580425;
extern const uint32_t ScoreAdder_UpdateScoreList_m2886385356_MetadataUsageId;
extern "C"  void ScoreAdder_UpdateScoreList_m2886385356 (ScoreAdder_t1918699446 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreAdder_UpdateScoreList_m2886385356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		InputField_t1631627530 * L_0 = __this->get_input_2();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		bool L_2 = Int32_TryParse_m656840904(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScores_t3229912699_il2cpp_TypeInfo_var);
		HighScores_t3229912699 * L_3 = ((HighScores_t3229912699_StaticFields*)HighScores_t3229912699_il2cpp_TypeInfo_var->static_fields)->get_main_3();
		int32_t L_4 = V_0;
		Score_t1518975274  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Score__ctor_m51016996(&L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_6 = HighScores_Insert_m2661071179(L_3, L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral62580425, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void ScoreDisplay::.ctor()
extern "C"  void ScoreDisplay__ctor_m3717173057 (ScoreDisplay_t164935196 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreDisplay::Awake()
extern Il2CppClass* ScoreDisplay_t164935196_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t ScoreDisplay_Awake_m3088139788_MetadataUsageId;
extern "C"  void ScoreDisplay_Awake_m3088139788 (ScoreDisplay_t164935196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreDisplay_Awake_m3088139788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set__anim_3(L_0);
		((ScoreDisplay_t164935196_StaticFields*)ScoreDisplay_t164935196_il2cpp_TypeInfo_var->static_fields)->set_main_2(__this);
		return;
	}
}
// System.Void ScoreDisplay::ShowScore(UnityEngine.Vector2,System.Int32,System.Single,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029319;
extern Il2CppCodeGenString* _stringLiteral2054778433;
extern const uint32_t ScoreDisplay_ShowScore_m293627306_MetadataUsageId;
extern "C"  void ScoreDisplay_ShowScore_m293627306 (ScoreDisplay_t164935196 * __this, Vector2_t2243707579  ___position0, int32_t ___score1, float ___scale2, String_t* ___previousText3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreDisplay_ShowScore_m293627306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = ___position0;
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_4 = ___scale2;
		float L_5 = ___scale2;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2720820983(&L_6, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m2325460848(L_3, L_6, /*hidden argument*/NULL);
		Text_t356221433 * L_7 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(__this, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		String_t* L_8 = ___previousText3;
		String_t* L_9 = Int32_ToString_m2960866144((&___score1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m612901809(NULL /*static, unused*/, L_8, _stringLiteral372029319, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_10);
		Text_t356221433 * L_11 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(__this, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		float L_12 = ___scale2;
		NullCheck(L_11);
		Text_set_fontSize_m2101304336(L_11, (((int32_t)((int32_t)((float)((float)(15.0f)*(float)L_12))))), /*hidden argument*/NULL);
		Animator_t69676727 * L_13 = __this->get__anim_3();
		NullCheck(L_13);
		Animator_SetTrigger_m3418492570(L_13, _stringLiteral2054778433, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreList::.ctor()
extern "C"  void ScoreList__ctor_m3134941225 (ScoreList_t2288120898 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String ScoreList::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Score_t1518975274_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t ScoreList_ToString_m2504232022_MetadataUsageId;
extern "C"  String_t* ScoreList_ToString_m2504232022 (ScoreList_t2288120898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreList_ToString_m2504232022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0035;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		ScoreU5BU5D_t100793071* L_2 = __this->get_highScores_0();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Score_t1518975274  L_4 = (*(Score_t1518975274 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
		Il2CppObject * L_5 = Box(Score_t1518975274_il2cpp_TypeInfo_var, &L_4);
		Il2CppChar L_6 = ((Il2CppChar)((int32_t)10));
		Il2CppObject * L_7 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2000667605(NULL /*static, unused*/, L_1, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_10 = V_1;
		ScoreU5BU5D_t100793071* L_11 = __this->get_highScores_0();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))-(int32_t)1)))))
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_12 = V_0;
		ScoreU5BU5D_t100793071* L_13 = __this->get_highScores_0();
		ScoreU5BU5D_t100793071* L_14 = __this->get_highScores_0();
		NullCheck(L_14);
		NullCheck(L_13);
		Score_t1518975274  L_15 = (*(Score_t1518975274 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))-(int32_t)1))))));
		Il2CppObject * L_16 = Box(Score_t1518975274_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m56707527(NULL /*static, unused*/, L_12, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		String_t* L_18 = V_0;
		return L_18;
	}
}
// System.Int32 ScoreList::Sort(System.Int32)
extern "C"  int32_t ScoreList_Sort_m1163570930 (ScoreList_t2288120898 * __this, int32_t ___trackingIndex0, const MethodInfo* method)
{
	{
		ScoreU5BU5D_t100793071* L_0 = __this->get_highScores_0();
		NullCheck(L_0);
		int32_t L_1 = ___trackingIndex0;
		int32_t L_2 = ScoreList_MergeSort_m220785364(__this, 0, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))-(int32_t)1)), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 ScoreList::MergeSort(System.Int32,System.Int32,System.Int32)
extern Il2CppClass* ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var;
extern const uint32_t ScoreList_MergeSort_m220785364_MetadataUsageId;
extern "C"  int32_t ScoreList_MergeSort_m220785364 (ScoreList_t2288120898 * __this, int32_t ___lo0, int32_t ___hi1, int32_t ___track2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScoreList_MergeSort_m220785364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ScoreU5BU5D_t100793071* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = ___lo0;
		int32_t L_1 = ___hi1;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_2 = ___track2;
		return L_2;
	}

IL_0009:
	{
		int32_t L_3 = ___lo0;
		int32_t L_4 = ___track2;
		if ((((int32_t)L_3) > ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___hi1;
		int32_t L_6 = ___track2;
		G_B5_0 = ((((int32_t)((((int32_t)L_5) < ((int32_t)L_6))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B5_0 = 0;
	}

IL_001a:
	{
		V_0 = (bool)G_B5_0;
		int32_t L_7 = ___hi1;
		int32_t L_8 = ___lo0;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_7-(int32_t)L_8))+(int32_t)1));
		int32_t L_9 = ___lo0;
		int32_t L_10 = ___hi1;
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))/(int32_t)2));
		int32_t L_11 = ___lo0;
		int32_t L_12 = V_2;
		int32_t L_13 = ___track2;
		int32_t L_14 = ScoreList_MergeSort_m220785364(__this, L_11, L_12, L_13, /*hidden argument*/NULL);
		___track2 = L_14;
		int32_t L_15 = V_2;
		int32_t L_16 = ___hi1;
		int32_t L_17 = ___track2;
		int32_t L_18 = ScoreList_MergeSort_m220785364(__this, ((int32_t)((int32_t)L_15+(int32_t)1)), L_16, L_17, /*hidden argument*/NULL);
		___track2 = L_18;
		int32_t L_19 = V_1;
		V_3 = ((ScoreU5BU5D_t100793071*)SZArrayNew(ScoreU5BU5D_t100793071_il2cpp_TypeInfo_var, (uint32_t)L_19));
		V_4 = 0;
		goto IL_0075;
	}

IL_004e:
	{
		ScoreU5BU5D_t100793071* L_20 = V_3;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		ScoreU5BU5D_t100793071* L_22 = __this->get_highScores_0();
		int32_t L_23 = ___lo0;
		int32_t L_24 = V_4;
		NullCheck(L_22);
		(*(Score_t1518975274 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21)))) = (*(Score_t1518975274 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23+(int32_t)L_24))))));
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0075:
	{
		int32_t L_26 = V_4;
		int32_t L_27 = V_1;
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_004e;
		}
	}
	{
		V_5 = 0;
		int32_t L_28 = V_2;
		int32_t L_29 = ___lo0;
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_28-(int32_t)L_29))+(int32_t)1));
		int32_t L_30 = ___track2;
		int32_t L_31 = ___lo0;
		V_7 = ((int32_t)((int32_t)L_30-(int32_t)L_31));
		V_8 = 0;
		goto IL_01c5;
	}

IL_0094:
	{
		int32_t L_32 = V_6;
		int32_t L_33 = ___hi1;
		int32_t L_34 = ___lo0;
		if ((((int32_t)L_32) > ((int32_t)((int32_t)((int32_t)L_33-(int32_t)L_34)))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_35 = V_5;
		int32_t L_36 = V_2;
		int32_t L_37 = ___lo0;
		if ((((int32_t)L_35) > ((int32_t)((int32_t)((int32_t)L_36-(int32_t)L_37)))))
		{
			goto IL_0148;
		}
	}
	{
		ScoreU5BU5D_t100793071* L_38 = V_3;
		int32_t L_39 = V_5;
		NullCheck(L_38);
		ScoreU5BU5D_t100793071* L_40 = V_3;
		int32_t L_41 = V_6;
		NullCheck(L_40);
		bool L_42 = Score_op_LessThan_m485616521(NULL /*static, unused*/, (*(Score_t1518975274 *)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39)))), (*(Score_t1518975274 *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))), /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_010a;
		}
	}
	{
		bool L_43 = V_0;
		if (!L_43)
		{
			goto IL_00df;
		}
	}
	{
		int32_t L_44 = V_7;
		int32_t L_45 = V_6;
		if ((!(((uint32_t)L_44) == ((uint32_t)L_45))))
		{
			goto IL_00df;
		}
	}
	{
		int32_t L_46 = V_8;
		V_7 = L_46;
	}

IL_00df:
	{
		ScoreU5BU5D_t100793071* L_47 = __this->get_highScores_0();
		int32_t L_48 = V_8;
		int32_t L_49 = ___lo0;
		NullCheck(L_47);
		ScoreU5BU5D_t100793071* L_50 = V_3;
		int32_t L_51 = V_6;
		int32_t L_52 = L_51;
		V_6 = ((int32_t)((int32_t)L_52+(int32_t)1));
		NullCheck(L_50);
		(*(Score_t1518975274 *)((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_48+(int32_t)L_49)))))) = (*(Score_t1518975274 *)((L_50)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_52))));
		goto IL_0143;
	}

IL_010a:
	{
		bool L_53 = V_0;
		if (!L_53)
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_54 = V_7;
		int32_t L_55 = V_5;
		if ((!(((uint32_t)L_54) == ((uint32_t)L_55))))
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_56 = V_8;
		V_7 = L_56;
	}

IL_011d:
	{
		ScoreU5BU5D_t100793071* L_57 = __this->get_highScores_0();
		int32_t L_58 = V_8;
		int32_t L_59 = ___lo0;
		NullCheck(L_57);
		ScoreU5BU5D_t100793071* L_60 = V_3;
		int32_t L_61 = V_5;
		int32_t L_62 = L_61;
		V_5 = ((int32_t)((int32_t)L_62+(int32_t)1));
		NullCheck(L_60);
		(*(Score_t1518975274 *)((L_57)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_58+(int32_t)L_59)))))) = (*(Score_t1518975274 *)((L_60)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62))));
	}

IL_0143:
	{
		goto IL_0181;
	}

IL_0148:
	{
		bool L_63 = V_0;
		if (!L_63)
		{
			goto IL_015b;
		}
	}
	{
		int32_t L_64 = V_7;
		int32_t L_65 = V_6;
		if ((!(((uint32_t)L_64) == ((uint32_t)L_65))))
		{
			goto IL_015b;
		}
	}
	{
		int32_t L_66 = V_8;
		V_7 = L_66;
	}

IL_015b:
	{
		ScoreU5BU5D_t100793071* L_67 = __this->get_highScores_0();
		int32_t L_68 = V_8;
		int32_t L_69 = ___lo0;
		NullCheck(L_67);
		ScoreU5BU5D_t100793071* L_70 = V_3;
		int32_t L_71 = V_6;
		int32_t L_72 = L_71;
		V_6 = ((int32_t)((int32_t)L_72+(int32_t)1));
		NullCheck(L_70);
		(*(Score_t1518975274 *)((L_67)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_68+(int32_t)L_69)))))) = (*(Score_t1518975274 *)((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_72))));
	}

IL_0181:
	{
		goto IL_01bf;
	}

IL_0186:
	{
		bool L_73 = V_0;
		if (!L_73)
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_74 = V_7;
		int32_t L_75 = V_5;
		if ((!(((uint32_t)L_74) == ((uint32_t)L_75))))
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_76 = V_8;
		V_7 = L_76;
	}

IL_0199:
	{
		ScoreU5BU5D_t100793071* L_77 = __this->get_highScores_0();
		int32_t L_78 = V_8;
		int32_t L_79 = ___lo0;
		NullCheck(L_77);
		ScoreU5BU5D_t100793071* L_80 = V_3;
		int32_t L_81 = V_5;
		int32_t L_82 = L_81;
		V_5 = ((int32_t)((int32_t)L_82+(int32_t)1));
		NullCheck(L_80);
		(*(Score_t1518975274 *)((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_78+(int32_t)L_79)))))) = (*(Score_t1518975274 *)((L_80)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_82))));
	}

IL_01bf:
	{
		int32_t L_83 = V_8;
		V_8 = ((int32_t)((int32_t)L_83+(int32_t)1));
	}

IL_01c5:
	{
		int32_t L_84 = V_8;
		int32_t L_85 = V_1;
		if ((((int32_t)L_84) < ((int32_t)L_85)))
		{
			goto IL_0094;
		}
	}
	{
		bool L_86 = V_0;
		if (!L_86)
		{
			goto IL_01d9;
		}
	}
	{
		int32_t L_87 = V_7;
		int32_t L_88 = ___lo0;
		___track2 = ((int32_t)((int32_t)L_87+(int32_t)L_88));
	}

IL_01d9:
	{
		int32_t L_89 = ___track2;
		return L_89;
	}
}
// System.String SettingList::FilePath()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3364344101;
extern const uint32_t SettingList_FilePath_m1899888445_MetadataUsageId;
extern "C"  String_t* SettingList_FilePath_m1899888445 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SettingList_FilePath_m1899888445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3364344101, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SettingList::SetDefault()
extern "C"  void SettingList_SetDefault_m3909643798 (SettingList_t4161291060 * __this, const MethodInfo* method)
{
	{
		__this->set_volume_0((1.0f));
		return;
	}
}
extern "C"  void SettingList_SetDefault_m3909643798_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SettingList_t4161291060 * _thisAdjusted = reinterpret_cast<SettingList_t4161291060 *>(__this + 1);
	SettingList_SetDefault_m3909643798(_thisAdjusted, method);
}
// System.Void Settings::.ctor()
extern "C"  void Settings__ctor_m3923603048 (Settings_t4248570851 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// SettingList Settings::get_settings()
extern Il2CppClass* Settings_t4248570851_il2cpp_TypeInfo_var;
extern const uint32_t Settings_get_settings_m1527194397_MetadataUsageId;
extern "C"  SettingList_t4161291060  Settings_get_settings_m1527194397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Settings_get_settings_m1527194397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SettingList_t4161291060  L_0 = ((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get__settings_3();
		return L_0;
	}
}
// System.Void Settings::OnEnable()
extern Il2CppClass* Settings_t4248570851_il2cpp_TypeInfo_var;
extern const uint32_t Settings_OnEnable_m1757321700_MetadataUsageId;
extern "C"  void Settings_OnEnable_m1757321700 (Settings_t4248570851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Settings_OnEnable_m1757321700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Settings_Load_m2929002520(NULL /*static, unused*/, /*hidden argument*/NULL);
		Slider_t297367283 * L_0 = __this->get_VolumeSlider_2();
		float L_1 = (((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get_address_of__settings_3())->get_volume_0();
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_0, L_1);
		return;
	}
}
// System.Void Settings::OnDisable()
extern "C"  void Settings_OnDisable_m1422008631 (Settings_t4248570851 * __this, const MethodInfo* method)
{
	{
		Settings_Save_m3326872871(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Settings::ChangeVolume(System.Single)
extern Il2CppClass* Settings_t4248570851_il2cpp_TypeInfo_var;
extern const uint32_t Settings_ChangeVolume_m1043473565_MetadataUsageId;
extern "C"  void Settings_ChangeVolume_m1043473565 (Settings_t4248570851 * __this, float ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Settings_ChangeVolume_m1043473565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___v0;
		if ((!(((float)L_0) > ((float)(1.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		___v0 = (1.0f);
	}

IL_0012:
	{
		float L_1 = ___v0;
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		___v0 = (0.0f);
	}

IL_0024:
	{
		float L_2 = ___v0;
		AudioListener_set_volume_m1233107753(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_3 = ___v0;
		(((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get_address_of__settings_3())->set_volume_0(L_3);
		return;
	}
}
// System.Boolean Settings::Save()
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* SettingList_t4161291060_il2cpp_TypeInfo_var;
extern Il2CppClass* Settings_t4248570851_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral700331505;
extern const uint32_t Settings_Save_m3326872871_MetadataUsageId;
extern "C"  bool Settings_Save_m3326872871 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Settings_Save_m3326872871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryFormatter_t1866979105 * V_0 = NULL;
	FileStream_t1695958676 * V_1 = NULL;
	SettingList_t4161291060  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		BinaryFormatter_t1866979105 * L_0 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = SettingList_FilePath_m1899888445(NULL /*static, unused*/, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_2 = File_Open_m1543461971(NULL /*static, unused*/, L_1, 4, /*hidden argument*/NULL);
		V_1 = L_2;
		Initobj (SettingList_t4161291060_il2cpp_TypeInfo_var, (&V_2));
		float L_3 = (((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get_address_of__settings_3())->get_volume_0();
		(&V_2)->set_volume_0(L_3);
		BinaryFormatter_t1866979105 * L_4 = V_0;
		FileStream_t1695958676 * L_5 = V_1;
		SettingList_t4161291060  L_6 = V_2;
		SettingList_t4161291060  L_7 = L_6;
		Il2CppObject * L_8 = Box(SettingList_t4161291060_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_4);
		BinaryFormatter_Serialize_m433301673(L_4, L_5, L_8, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral700331505, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean Settings::Load()
extern Il2CppClass* BinaryFormatter_t1866979105_il2cpp_TypeInfo_var;
extern Il2CppClass* SettingList_t4161291060_il2cpp_TypeInfo_var;
extern Il2CppClass* Settings_t4248570851_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1507407726;
extern Il2CppCodeGenString* _stringLiteral2665543795;
extern const uint32_t Settings_Load_m2929002520_MetadataUsageId;
extern "C"  bool Settings_Load_m2929002520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Settings_Load_m2929002520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryFormatter_t1866979105 * V_0 = NULL;
	FileStream_t1695958676 * V_1 = NULL;
	SettingList_t4161291060  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		String_t* L_0 = SettingList_FilePath_m1899888445(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = File_Exists_m1685968367(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		BinaryFormatter_t1866979105 * L_2 = (BinaryFormatter_t1866979105 *)il2cpp_codegen_object_new(BinaryFormatter_t1866979105_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m4171832002(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = SettingList_FilePath_m1899888445(NULL /*static, unused*/, /*hidden argument*/NULL);
		FileStream_t1695958676 * L_4 = File_Open_m1543461971(NULL /*static, unused*/, L_3, 3, /*hidden argument*/NULL);
		V_1 = L_4;
		BinaryFormatter_t1866979105 * L_5 = V_0;
		FileStream_t1695958676 * L_6 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_7 = BinaryFormatter_Deserialize_m2771853471(L_5, L_6, /*hidden argument*/NULL);
		V_2 = ((*(SettingList_t4161291060 *)((SettingList_t4161291060 *)UnBox (L_7, SettingList_t4161291060_il2cpp_TypeInfo_var))));
		FileStream_t1695958676 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_8);
		float L_9 = (&V_2)->get_volume_0();
		(((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get_address_of__settings_3())->set_volume_0(L_9);
		Settings_Refresh_m4147788479(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1507407726, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2665543795, /*hidden argument*/NULL);
		SettingList_SetDefault_m3909643798((((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get_address_of__settings_3()), /*hidden argument*/NULL);
		Settings_Refresh_m4147788479(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void Settings::Refresh()
extern Il2CppClass* Settings_t4248570851_il2cpp_TypeInfo_var;
extern const uint32_t Settings_Refresh_m4147788479_MetadataUsageId;
extern "C"  void Settings_Refresh_m4147788479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Settings_Refresh_m4147788479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (((Settings_t4248570851_StaticFields*)Settings_t4248570851_il2cpp_TypeInfo_var->static_fields)->get_address_of__settings_3())->get_volume_0();
		AudioListener_set_volume_m1233107753(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Settings::ResetToLoad()
extern "C"  void Settings_ResetToLoad_m3865441310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Settings_Load_m2929002520(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sound::.ctor()
extern "C"  void Sound__ctor_m3476080378 (Sound_t826716933 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spike::.ctor()
extern "C"  void Spike__ctor_m3544368207 (Spike_t60554746 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// GP120 Spike::get_gp120()
extern const MethodInfo* GameObject_GetComponentInChildren_TisGP120_t1512820336_m2602876863_MethodInfo_var;
extern const uint32_t Spike_get_gp120_m3432987073_MetadataUsageId;
extern "C"  GP120_t1512820336 * Spike_get_gp120_m3432987073 (Spike_t60554746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spike_get_gp120_m3432987073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GP120_t1512820336 * L_1 = GameObject_GetComponentInChildren_TisGP120_t1512820336_m2602876863(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisGP120_t1512820336_m2602876863_MethodInfo_var);
		return L_1;
	}
}
// GP41 Spike::get_gp41()
extern const MethodInfo* GameObject_GetComponentInChildren_TisGP41_t921954836_m1614657349_MethodInfo_var;
extern const uint32_t Spike_get_gp41_m2571159277_MetadataUsageId;
extern "C"  GP41_t921954836 * Spike_get_gp41_m2571159277 (Spike_t60554746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spike_get_gp41_m2571159277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GP41_t921954836 * L_1 = GameObject_GetComponentInChildren_TisGP41_t921954836_m1614657349(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisGP41_t921954836_m1614657349_MethodInfo_var);
		return L_1;
	}
}
// Tropism Spike::get_tropism()
extern "C"  Tropism_t3662836552  Spike_get_tropism_m1909874245 (Spike_t60554746 * __this, const MethodInfo* method)
{
	{
		Tropism_t3662836552  L_0 = __this->get__tropism_2();
		return L_0;
	}
}
// System.Void Spike::set_tropism(Tropism)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Spike_set_tropism_m2060441560_MetadataUsageId;
extern "C"  void Spike_set_tropism_m2060441560 (Spike_t60554746 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spike_set_tropism_m2060441560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (&___value0)->get_coreceptor_11();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = (&___value0)->get_coreceptor_11();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0024;
		}
	}

IL_0019:
	{
		String_t* L_2 = ErrorCode_get_EG0HT_m1542756000(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		GP120_t1512820336 * L_4 = Spike_get_gp120_m3432987073(__this, /*hidden argument*/NULL);
		Tropism_t3662836552  L_5 = ___value0;
		NullCheck(L_4);
		GP120_set_top_m2815271707(L_4, L_5, /*hidden argument*/NULL);
		Tropism_t3662836552  L_6 = ___value0;
		__this->set__tropism_2(L_6);
		return;
	}
}
// Pivot Spike::get_pivot()
extern const MethodInfo* GameObject_GetComponentInChildren_TisPivot_t2110476880_m524346231_MethodInfo_var;
extern const uint32_t Spike_get_pivot_m660032993_MetadataUsageId;
extern "C"  Pivot_t2110476880 * Spike_get_pivot_m660032993 (Spike_t60554746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spike_get_pivot_m660032993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Pivot_t2110476880 * L_1 = GameObject_GetComponentInChildren_TisPivot_t2110476880_m524346231(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisPivot_t2110476880_m524346231_MethodInfo_var);
		return L_1;
	}
}
// SpikeInteract Spike::get_interaction()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpikeInteract_t2340666440_m861253405_MethodInfo_var;
extern const uint32_t Spike_get_interaction_m2108312269_MetadataUsageId;
extern "C"  SpikeInteract_t2340666440 * Spike_get_interaction_m2108312269 (Spike_t60554746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spike_get_interaction_m2108312269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpikeInteract_t2340666440 * L_1 = GameObject_GetComponentInChildren_TisSpikeInteract_t2340666440_m861253405(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpikeInteract_t2340666440_m861253405_MethodInfo_var);
		return L_1;
	}
}
// System.Void Spike::Scale(System.Single)
extern "C"  void Spike_Scale_m3772594916 (Spike_t60554746 * __this, float ___s0, const MethodInfo* method)
{
	{
		float L_0 = ___s0;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_2 = ___s0;
		float L_3 = ___s0;
		float L_4 = ___s0;
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m2325460848(L_1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spike::MoveTo(UnityEngine.Vector3)
extern "C"  void Spike_MoveTo_m4261606434 (Spike_t60554746 * __this, Vector3_t2243707580  ___translation0, const MethodInfo* method)
{
	{
		Pivot_t2110476880 * L_0 = Spike_get_pivot_m660032993(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___translation0;
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spike::MoveAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Spike_MoveAndRotate_m897337456 (Spike_t60554746 * __this, Vector3_t2243707580  ___translation0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___translation0;
		Spike_MoveTo_m4261606434(__this, L_0, /*hidden argument*/NULL);
		Pivot_t2110476880 * L_1 = Spike_get_pivot_m660032993(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(L_1, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = ___rotation1;
		NullCheck(L_2);
		Transform_set_localRotation_m2055111962(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spike::MoveAndRotate(System.Single,UnityEngine.Quaternion)
extern "C"  void Spike_MoveAndRotate_m1100489612 (Spike_t60554746 * __this, float ___radius0, Quaternion_t4030073918  ___angle1, const MethodInfo* method)
{
	{
		Pivot_t2110476880 * L_0 = Spike_get_pivot_m660032993(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		float L_2 = ___radius0;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, (0.0f), L_2, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = ___angle1;
		NullCheck(L_4);
		Transform_set_rotation_m3411284563(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spike::PivotRotation(UnityEngine.Vector3)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Spike_PivotRotation_m760914936_MetadataUsageId;
extern "C"  void Spike_PivotRotation_m760914936 (Spike_t60554746 * __this, Vector3_t2243707580  ___eulerAngles0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spike_PivotRotation_m760914936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Pivot_t2110476880 * L_0 = Spike_get_pivot_m660032993(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		Initobj (Quaternion_t4030073918_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t4030073918  L_2 = V_0;
		NullCheck(L_1);
		Transform_set_rotation_m3411284563(L_1, L_2, /*hidden argument*/NULL);
		Pivot_t2110476880 * L_3 = Spike_get_pivot_m660032993(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = ___eulerAngles0;
		NullCheck(L_4);
		Transform_Rotate_m1743927093(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spike::PivotRotation(UnityEngine.Quaternion)
extern "C"  void Spike_PivotRotation_m2863376170 (Spike_t60554746 * __this, Quaternion_t4030073918  ___angle0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Quaternion_get_eulerAngles_m3302573991((&___angle0), /*hidden argument*/NULL);
		Spike_PivotRotation_m760914936(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpikeColor::.ctor(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color)
extern "C"  void SpikeColor__ctor_m2112352072 (SpikeColor_t3573234163 * __this, Color_t2020392075  ____gp410, Color_t2020392075  ____gp120_l1, Color_t2020392075  ____gp120_r2, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ____gp410;
		__this->set_gp41_0(L_0);
		Color_t2020392075  L_1 = ____gp120_l1;
		__this->set_gp120_left_1(L_1);
		Color_t2020392075  L_2 = ____gp120_r2;
		__this->set_gp120_right_2(L_2);
		return;
	}
}
extern "C"  void SpikeColor__ctor_m2112352072_AdjustorThunk (Il2CppObject * __this, Color_t2020392075  ____gp410, Color_t2020392075  ____gp120_l1, Color_t2020392075  ____gp120_r2, const MethodInfo* method)
{
	SpikeColor_t3573234163 * _thisAdjusted = reinterpret_cast<SpikeColor_t3573234163 *>(__this + 1);
	SpikeColor__ctor_m2112352072(_thisAdjusted, ____gp410, ____gp120_l1, ____gp120_r2, method);
}
// System.Void SpikeInteract::.ctor()
extern "C"  void SpikeInteract__ctor_m2361209129 (SpikeInteract_t2340666440 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tropism SpikeInteract::get_tropism()
extern "C"  Tropism_t3662836552  SpikeInteract_get_tropism_m1350703263 (SpikeInteract_t2340666440 * __this, const MethodInfo* method)
{
	{
		Spike_t60554746 * L_0 = SpikeInteract_get_spike_m1941632887(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Tropism_t3662836552  L_1 = Spike_get_tropism_m1909874245(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Spike SpikeInteract::get_spike()
extern const MethodInfo* GameObject_GetComponentInParent_TisSpike_t60554746_m2979846140_MethodInfo_var;
extern const uint32_t SpikeInteract_get_spike_m1941632887_MetadataUsageId;
extern "C"  Spike_t60554746 * SpikeInteract_get_spike_m1941632887 (SpikeInteract_t2340666440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpikeInteract_get_spike_m1941632887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Spike_t60554746 * L_1 = GameObject_GetComponentInParent_TisSpike_t60554746_m2979846140(L_0, /*hidden argument*/GameObject_GetComponentInParent_TisSpike_t60554746_m2979846140_MethodInfo_var);
		return L_1;
	}
}
// HIV SpikeInteract::get_hiv()
extern const MethodInfo* GameObject_GetComponentInParent_TisHIV_t2481767745_m3590185171_MethodInfo_var;
extern const uint32_t SpikeInteract_get_hiv_m2599775787_MetadataUsageId;
extern "C"  HIV_t2481767745 * SpikeInteract_get_hiv_m2599775787 (SpikeInteract_t2340666440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpikeInteract_get_hiv_m2599775787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		HIV_t2481767745 * L_1 = GameObject_GetComponentInParent_TisHIV_t2481767745_m3590185171(L_0, /*hidden argument*/GameObject_GetComponentInParent_TisHIV_t2481767745_m3590185171_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean SpikeInteract::validCombination(TCell_Receptor)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SpikeInteract_validCombination_m4061672919_MetadataUsageId;
extern "C"  bool SpikeInteract_validCombination_m4061672919 (SpikeInteract_t2340666440 * __this, TCell_Receptor_t2424206879 * ___check0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpikeInteract_validCombination_m4061672919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tropism_t3662836552  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Tropism_t3662836552  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Tropism_t3662836552  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Tropism_t3662836552  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Tropism_t3662836552  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Tropism_t3662836552  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	{
		TCell_Receptor_t2424206879 * L_0 = ___check0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0081;
		}
	}
	{
		TCell_Receptor_t2424206879 * L_2 = ___check0;
		NullCheck(L_2);
		Tropism_t3662836552  L_3 = TCell_Receptor_get_type_m1545012622(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = (&V_0)->get_receptor_10();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0081;
		}
	}
	{
		Tropism_t3662836552  L_5 = SpikeInteract_get_tropism_m1350703263(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = (&V_1)->get_coreceptor_11();
		if ((!(((uint32_t)L_6) == ((uint32_t)4))))
		{
			goto IL_0060;
		}
	}
	{
		TCell_Receptor_t2424206879 * L_7 = ___check0;
		NullCheck(L_7);
		Tropism_t3662836552  L_8 = TCell_Receptor_get_type_m1545012622(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		int32_t L_9 = (&V_2)->get_coreceptor_11();
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_005a;
		}
	}
	{
		TCell_Receptor_t2424206879 * L_10 = ___check0;
		NullCheck(L_10);
		Tropism_t3662836552  L_11 = TCell_Receptor_get_type_m1545012622(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		int32_t L_12 = (&V_3)->get_coreceptor_11();
		G_B6_0 = ((((int32_t)L_12) == ((int32_t)2))? 1 : 0);
		goto IL_005b;
	}

IL_005a:
	{
		G_B6_0 = 1;
	}

IL_005b:
	{
		G_B8_0 = G_B6_0;
		goto IL_0080;
	}

IL_0060:
	{
		Tropism_t3662836552  L_13 = SpikeInteract_get_tropism_m1350703263(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = (&V_4)->get_coreceptor_11();
		TCell_Receptor_t2424206879 * L_15 = ___check0;
		NullCheck(L_15);
		Tropism_t3662836552  L_16 = TCell_Receptor_get_type_m1545012622(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		int32_t L_17 = (&V_5)->get_coreceptor_11();
		G_B8_0 = ((((int32_t)L_14) == ((int32_t)L_17))? 1 : 0);
	}

IL_0080:
	{
		return (bool)G_B8_0;
	}

IL_0081:
	{
		return (bool)0;
	}
}
// System.Void SpikeList::.ctor()
extern "C"  void SpikeList__ctor_m3681244969 (SpikeList_t1827939702 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SpikeList::get_count()
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern const uint32_t SpikeList_get_count_m3219171817_MetadataUsageId;
extern "C"  int32_t SpikeList_get_count_m3219171817 (SpikeList_t1827939702 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpikeList_get_count_m3219171817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		List_1_t1125654279 * L_0 = __this->get_objectList_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		List_1_t1125654279 * L_1 = __this->get_objectList_3();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2764296230(L_1, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// Spike SpikeList::get_Item(System.Int32)
extern const MethodInfo* List_1_get_Item_m939767277_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const uint32_t SpikeList_get_Item_m546146431_MetadataUsageId;
extern "C"  Spike_t60554746 * SpikeList_get_Item_m546146431 (SpikeList_t1827939702 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpikeList_get_Item_m546146431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Spike_t60554746 * G_B4_0 = NULL;
	{
		List_1_t1125654279 * L_0 = __this->get_objectList_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = ___i0;
		int32_t L_2 = SpikeList_get_count_m3219171817(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}

IL_0017:
	{
		G_B4_0 = ((Spike_t60554746 *)(NULL));
		goto IL_002e;
	}

IL_001d:
	{
		List_1_t1125654279 * L_3 = __this->get_objectList_3();
		int32_t L_4 = ___i0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_5 = List_1_get_Item_m939767277(L_3, L_4, /*hidden argument*/List_1_get_Item_m939767277_MethodInfo_var);
		NullCheck(L_5);
		Spike_t60554746 * L_6 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_5, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		G_B4_0 = L_6;
	}

IL_002e:
	{
		return G_B4_0;
	}
}
// System.Void SpikeList::Instantiate(UnityEngine.Transform,System.Single)
extern Il2CppClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3724643174_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldAccessException_t1797813379_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m704351054_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m551678385_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3441471442_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2132142653_MethodInfo_var;
extern const uint32_t SpikeList_Instantiate_m1789477637_MetadataUsageId;
extern "C"  void SpikeList_Instantiate_m1789477637 (SpikeList_t1827939702 * __this, Transform_t3275118058 * ___parent0, float ___radius1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpikeList_Instantiate_m1789477637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1756533147 * V_1 = NULL;
	Spike_t60554746 * V_2 = NULL;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		List_1_t1125654279 * L_0 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_0, /*hidden argument*/List_1__ctor_m704351054_MethodInfo_var);
		__this->set_objectList_3(L_0);
		List_1_t3724643174 * L_1 = (List_1_t3724643174 *)il2cpp_codegen_object_new(List_1_t3724643174_il2cpp_TypeInfo_var);
		List_1__ctor_m551678385(L_1, /*hidden argument*/List_1__ctor_m551678385_MethodInfo_var);
		__this->set_scriptList_4(L_1);
		V_0 = 0;
		goto IL_00cb;
	}

IL_001d:
	{
		GameObject_t1756533147 * L_2 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_3 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_1 = L_3;
		List_1_t1125654279 * L_4 = __this->get_objectList_3();
		GameObject_t1756533147 * L_5 = V_1;
		NullCheck(L_4);
		List_1_Add_m3441471442(L_4, L_5, /*hidden argument*/List_1_Add_m3441471442_MethodInfo_var);
		GameObject_t1756533147 * L_6 = V_1;
		NullCheck(L_6);
		Spike_t60554746 * L_7 = GameObject_GetComponent_TisSpike_t60554746_m2221441807(L_6, /*hidden argument*/GameObject_GetComponent_TisSpike_t60554746_m2221441807_MethodInfo_var);
		V_2 = L_7;
		Spike_t60554746 * L_8 = V_2;
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00bc;
		}
	}
	{
		List_1_t3724643174 * L_10 = __this->get_scriptList_4();
		Spike_t60554746 * L_11 = V_2;
		NullCheck(L_10);
		List_1_Add_m2132142653(L_10, L_11, /*hidden argument*/List_1_Add_m2132142653_MethodInfo_var);
		Transform_t3275118058 * L_12 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006c;
		}
	}
	{
		Spike_t60554746 * L_14 = V_2;
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = ___parent0;
		NullCheck(L_15);
		Transform_SetParent_m4124909910(L_15, L_16, /*hidden argument*/NULL);
	}

IL_006c:
	{
		Spike_t60554746 * L_17 = V_2;
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t2243707580  L_19 = V_3;
		NullCheck(L_18);
		Transform_set_localPosition_m1026930133(L_18, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		uint32_t L_21 = __this->get__count_2();
		V_4 = ((float)((float)((float)((float)(((float)((float)L_20)))/(float)(((float)((float)(((double)((uint32_t)L_21))))))))*(float)(360.0f)));
		Spike_t60554746 * L_22 = V_2;
		float L_23 = ___radius1;
		float L_24 = V_4;
		Quaternion_t4030073918  L_25 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Spike_MoveAndRotate_m1100489612(L_22, L_23, L_25, /*hidden argument*/NULL);
		Spike_t60554746 * L_26 = V_2;
		Tropism_t3662836552  L_27 = __this->get_tropism_1();
		NullCheck(L_26);
		Spike_set_tropism_m2060441560(L_26, L_27, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00bc:
	{
		String_t* L_28 = ErrorCode_get_EC0MC_m1677016276(NULL /*static, unused*/, /*hidden argument*/NULL);
		FieldAccessException_t1797813379 * L_29 = (FieldAccessException_t1797813379 *)il2cpp_codegen_object_new(FieldAccessException_t1797813379_il2cpp_TypeInfo_var);
		FieldAccessException__ctor_m3893881490(L_29, L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}

IL_00c7:
	{
		int32_t L_30 = V_0;
		V_0 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_31 = V_0;
		uint32_t L_32 = __this->get__count_2();
		if ((((int64_t)(((int64_t)((int64_t)L_31)))) < ((int64_t)(((int64_t)((uint64_t)L_32))))))
		{
			goto IL_001d;
		}
	}
	{
		return;
	}
}
// System.Void SpikeList::Instantiate(UnityEngine.Transform,System.Single,System.UInt32)
extern "C"  void SpikeList_Instantiate_m417580653 (SpikeList_t1827939702 * __this, Transform_t3275118058 * ___parent0, float ___radius1, uint32_t ___c2, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___c2;
		__this->set__count_2(L_0);
		Transform_t3275118058 * L_1 = ___parent0;
		float L_2 = ___radius1;
		SpikeList_Instantiate_m1789477637(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpikeList::Instantiate(UnityEngine.Transform,System.Single,System.UInt32,Tropism)
extern "C"  void SpikeList_Instantiate_m1425764107 (SpikeList_t1827939702 * __this, Transform_t3275118058 * ___parent0, float ___radius1, uint32_t ___c2, Tropism_t3662836552  ___t3, const MethodInfo* method)
{
	{
		Tropism_t3662836552  L_0 = ___t3;
		__this->set_tropism_1(L_0);
		Transform_t3275118058 * L_1 = ___parent0;
		float L_2 = ___radius1;
		uint32_t L_3 = ___c2;
		SpikeList_Instantiate_m417580653(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpikeList::ChangeTropism(Tropism)
extern "C"  void SpikeList_ChangeTropism_m419626291 (SpikeList_t1827939702 * __this, Tropism_t3662836552  ___t0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Tropism_t3662836552  L_0 = ___t0;
		__this->set_tropism_1(L_0);
		V_0 = 0;
		goto IL_001f;
	}

IL_000e:
	{
		int32_t L_1 = V_0;
		Spike_t60554746 * L_2 = SpikeList_get_Item_m546146431(__this, L_1, /*hidden argument*/NULL);
		Tropism_t3662836552  L_3 = ___t0;
		NullCheck(L_2);
		Spike_set_tropism_m2060441560(L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = SpikeList_get_count_m3219171817(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void SpikeList::Scale(System.Single)
extern "C"  void SpikeList_Scale_m1908817848 (SpikeList_t1827939702 * __this, float ___a0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___a0;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = 0;
		goto IL_0024;
	}

IL_0013:
	{
		int32_t L_1 = V_0;
		Spike_t60554746 * L_2 = SpikeList_get_Item_m546146431(__this, L_1, /*hidden argument*/NULL);
		float L_3 = ___a0;
		NullCheck(L_2);
		Spike_Scale_m3772594916(L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = SpikeList_get_count_m3219171817(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void SpriteDisplay::.ctor()
extern "C"  void SpriteDisplay__ctor_m3083842324 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SpriteDisplay::get_spriteCount()
extern const MethodInfo* List_1_get_Count_m2653746482_MethodInfo_var;
extern const uint32_t SpriteDisplay_get_spriteCount_m1357692313_MetadataUsageId;
extern "C"  int32_t SpriteDisplay_get_spriteCount_m1357692313 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDisplay_get_spriteCount_m1357692313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3973682211 * L_0 = __this->get_spriteList_3();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2653746482(L_0, /*hidden argument*/List_1_get_Count_m2653746482_MethodInfo_var);
		return L_1;
	}
}
// System.Int32 SpriteDisplay::get_maxSprite()
extern const MethodInfo* List_1_get_Count_m2653746482_MethodInfo_var;
extern const uint32_t SpriteDisplay_get_maxSprite_m4157804466_MetadataUsageId;
extern "C"  int32_t SpriteDisplay_get_maxSprite_m4157804466 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDisplay_get_maxSprite_m4157804466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3973682211 * L_0 = __this->get_spriteList_3();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2653746482(L_0, /*hidden argument*/List_1_get_Count_m2653746482_MethodInfo_var);
		return ((int32_t)((int32_t)L_1-(int32_t)1));
	}
}
// System.Void SpriteDisplay::Start()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SpriteDisplay_Start_m2382151612_MetadataUsageId;
extern "C"  void SpriteDisplay_Start_m2382151612 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDisplay_Start_m2382151612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteDisplay_UpdateSprite_m2828589862(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_index_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = powf((2.0f), (((float)((float)L_0))));
		SpriteDisplay_Scale_m4190160741(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpriteDisplay::UpdateSprite()
extern const MethodInfo* List_1_get_Item_m2973515843_MethodInfo_var;
extern const uint32_t SpriteDisplay_UpdateSprite_m2828589862_MetadataUsageId;
extern "C"  void SpriteDisplay_UpdateSprite_m2828589862 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteDisplay_UpdateSprite_m2828589862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_index_4();
		int32_t L_1 = SpriteDisplay_get_spriteCount_m1357692313(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = SpriteDisplay_get_maxSprite_m4157804466(__this, /*hidden argument*/NULL);
		__this->set_index_4(L_2);
	}

IL_001d:
	{
		SpriteRenderer_t1209076198 * L_3 = __this->get_spriteRender_2();
		List_1_t3973682211 * L_4 = __this->get_spriteList_3();
		int32_t L_5 = __this->get_index_4();
		NullCheck(L_4);
		Sprite_t309593783 * L_6 = List_1_get_Item_m2973515843(L_4, L_5, /*hidden argument*/List_1_get_Item_m2973515843_MethodInfo_var);
		NullCheck(L_3);
		SpriteRenderer_set_sprite_m617298623(L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpriteDisplay::Update()
extern "C"  void SpriteDisplay_Update_m455565409 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_up_5();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		SpriteDisplay_UpdateSprite_m2878970303(__this, 0, /*hidden argument*/NULL);
		__this->set_up_5((bool)0);
		goto IL_0037;
	}

IL_001e:
	{
		bool L_1 = __this->get_down_6();
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		SpriteDisplay_UpdateSprite_m2878970303(__this, 1, /*hidden argument*/NULL);
		__this->set_down_6((bool)0);
	}

IL_0037:
	{
		return;
	}
}
// System.Void SpriteDisplay::UpdateSprite(SpriteDisplay/SpriteChange)
extern "C"  void SpriteDisplay_UpdateSprite_m2878970303 (SpriteDisplay_t1886630049 * __this, int32_t ___s0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___s0;
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = __this->get_index_4();
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_2 = __this->get_index_4();
		__this->set_index_4(((int32_t)((int32_t)L_2-(int32_t)1)));
		SpriteDisplay_UpdateSprite_m2828589862(__this, /*hidden argument*/NULL);
		SpriteDisplay_Scale_m4190160741(__this, (0.5f), /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_0036:
	{
		int32_t L_3 = ___s0;
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_4 = __this->get_index_4();
		int32_t L_5 = SpriteDisplay_get_maxSprite_m4157804466(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_6 = __this->get_index_4();
		__this->set_index_4(((int32_t)((int32_t)L_6+(int32_t)1)));
		SpriteDisplay_UpdateSprite_m2828589862(__this, /*hidden argument*/NULL);
		SpriteDisplay_Scale_m4190160741(__this, (2.0f), /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Void SpriteDisplay::Scale(System.Single)
extern "C"  void SpriteDisplay_Scale_m4190160741 (SpriteDisplay_t1886630049 * __this, float ___s0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_localScale_m3074381503(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		float L_4 = ___s0;
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_localScale_m3074381503(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		float L_8 = ___s0;
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_localScale_m3074381503(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_z_3();
		float L_12 = ___s0;
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, ((float)((float)L_3*(float)L_4)), ((float)((float)L_7*(float)L_8)), ((float)((float)L_11*(float)L_12)), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_13, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: SpriteRender
extern "C" void SpriteRender_t2828817349_marshal_pinvoke(const SpriteRender_t2828817349& unmarshaled, SpriteRender_t2828817349_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'SpriteRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
extern "C" void SpriteRender_t2828817349_marshal_pinvoke_back(const SpriteRender_t2828817349_marshaled_pinvoke& marshaled, SpriteRender_t2828817349& unmarshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'SpriteRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
// Conversion method for clean up from marshalling of: SpriteRender
extern "C" void SpriteRender_t2828817349_marshal_pinvoke_cleanup(SpriteRender_t2828817349_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: SpriteRender
extern "C" void SpriteRender_t2828817349_marshal_com(const SpriteRender_t2828817349& unmarshaled, SpriteRender_t2828817349_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'SpriteRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
extern "C" void SpriteRender_t2828817349_marshal_com_back(const SpriteRender_t2828817349_marshaled_com& marshaled, SpriteRender_t2828817349& unmarshaled)
{
	Il2CppCodeGenException* ___render_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'render' of type 'SpriteRender': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___render_0Exception);
}
// Conversion method for clean up from marshalling of: SpriteRender
extern "C" void SpriteRender_t2828817349_marshal_com_cleanup(SpriteRender_t2828817349_marshaled_com& marshaled)
{
}
// System.Void Statistics::.ctor(System.Single,System.Single,System.Single,System.Single,System.UInt32)
extern "C"  void Statistics__ctor_m1821818148 (Statistics_t2537377007 * __this, float ___s0, float ___a1, float ___l2, float ___d3, uint32_t ___r4, const MethodInfo* method)
{
	{
		float L_0 = ___s0;
		__this->set_speed_0(L_0);
		float L_1 = ___a1;
		__this->set_acceleration_1(L_1);
		float L_2 = ___l2;
		__this->set_lifespan_2(L_2);
		float L_3 = ___d3;
		__this->set_docking_3(L_3);
		uint32_t L_4 = ___r4;
		__this->set_replication_rate_4(L_4);
		return;
	}
}
extern "C"  void Statistics__ctor_m1821818148_AdjustorThunk (Il2CppObject * __this, float ___s0, float ___a1, float ___l2, float ___d3, uint32_t ___r4, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	Statistics__ctor_m1821818148(_thisAdjusted, ___s0, ___a1, ___l2, ___d3, ___r4, method);
}
// System.Void Statistics::.ctor(Statistics)
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t Statistics__ctor_m2616661291_MetadataUsageId;
extern "C"  void Statistics__ctor_m2616661291 (Statistics_t2537377007 * __this, Statistics_t2537377007  ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics__ctor_m2616661291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Statistics_t2537377007  L_0 = ___s0;
		Initobj (Statistics_t2537377007_il2cpp_TypeInfo_var, (&V_0));
		Statistics_t2537377007  L_1 = V_0;
		bool L_2 = Statistics_op_Equality_m463644592(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Statistics_t2537377007  L_3 = Statistics_get_base_stats_m579726902(NULL /*static, unused*/, /*hidden argument*/NULL);
		___s0 = L_3;
	}

IL_001b:
	{
		Statistics_t2537377007  L_4 = ___s0;
		(*(Statistics_t2537377007 *)__this) = L_4;
		return;
	}
}
extern "C"  void Statistics__ctor_m2616661291_AdjustorThunk (Il2CppObject * __this, Statistics_t2537377007  ___s0, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	Statistics__ctor_m2616661291(_thisAdjusted, ___s0, method);
}
// System.Single Statistics::GetIndex(Statistics/Type)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_GetIndex_m814139896_MetadataUsageId;
extern "C"  float Statistics_GetIndex_m814139896 (Statistics_t2537377007 * __this, int32_t ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_GetIndex_m814139896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___t0;
		if (L_0 == 0)
		{
			goto IL_003d;
		}
		if (L_0 == 1)
		{
			goto IL_001f;
		}
		if (L_0 == 2)
		{
			goto IL_002d;
		}
		if (L_0 == 3)
		{
			goto IL_0026;
		}
		if (L_0 == 4)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0044;
	}

IL_001f:
	{
		float L_1 = __this->get_acceleration_1();
		return L_1;
	}

IL_0026:
	{
		float L_2 = __this->get_docking_3();
		return L_2;
	}

IL_002d:
	{
		float L_3 = __this->get_lifespan_2();
		return L_3;
	}

IL_0034:
	{
		uint32_t L_4 = __this->get_replication_rate_4();
		return (((float)((float)(((double)((uint32_t)L_4))))));
	}

IL_003d:
	{
		float L_5 = __this->get_speed_0();
		return L_5;
	}

IL_0044:
	{
		IndexOutOfRangeException_t3527622107 * L_6 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3497760912(L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
extern "C"  float Statistics_GetIndex_m814139896_AdjustorThunk (Il2CppObject * __this, int32_t ___t0, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	return Statistics_GetIndex_m814139896(_thisAdjusted, ___t0, method);
}
// System.Void Statistics::SetIndex(Statistics/Type,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_SetIndex_m3744871941_MetadataUsageId;
extern "C"  void Statistics_SetIndex_m3744871941 (Statistics_t2537377007 * __this, int32_t ___t0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_SetIndex_m3744871941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___t0;
		if (L_0 == 0)
		{
			goto IL_0050;
		}
		if (L_0 == 1)
		{
			goto IL_001f;
		}
		if (L_0 == 2)
		{
			goto IL_0037;
		}
		if (L_0 == 3)
		{
			goto IL_002b;
		}
		if (L_0 == 4)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_005c;
	}

IL_001f:
	{
		float L_1 = ___value1;
		__this->set_acceleration_1(L_1);
		goto IL_0062;
	}

IL_002b:
	{
		float L_2 = ___value1;
		__this->set_docking_3(L_2);
		goto IL_0062;
	}

IL_0037:
	{
		float L_3 = ___value1;
		__this->set_lifespan_2(L_3);
		goto IL_0062;
	}

IL_0043:
	{
		float L_4 = ___value1;
		__this->set_replication_rate_4((((int32_t)((uint32_t)L_4))));
		goto IL_0062;
	}

IL_0050:
	{
		float L_5 = ___value1;
		__this->set_speed_0(L_5);
		goto IL_0062;
	}

IL_005c:
	{
		IndexOutOfRangeException_t3527622107 * L_6 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3497760912(L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0062:
	{
		return;
	}
}
extern "C"  void Statistics_SetIndex_m3744871941_AdjustorThunk (Il2CppObject * __this, int32_t ___t0, float ___value1, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	Statistics_SetIndex_m3744871941(_thisAdjusted, ___t0, ___value1, method);
}
// System.Void Statistics::LevelStats(Statistics&)
extern "C"  void Statistics_LevelStats_m2515529360 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007 * ___s0, const MethodInfo* method)
{
	{
		Statistics_t2537377007 * L_0 = ___s0;
		Statistics_setMax_m372170337(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		Statistics_t2537377007 * L_1 = ___s0;
		Statistics_setMax_m372170337(NULL /*static, unused*/, L_1, 3, /*hidden argument*/NULL);
		Statistics_t2537377007 * L_2 = ___s0;
		Statistics_setMax_m372170337(NULL /*static, unused*/, L_2, 2, /*hidden argument*/NULL);
		Statistics_t2537377007 * L_3 = ___s0;
		Statistics_setMax_m372170337(NULL /*static, unused*/, L_3, 4, /*hidden argument*/NULL);
		Statistics_t2537377007 * L_4 = ___s0;
		Statistics_setMax_m372170337(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Statistics::setMax(Statistics&,Statistics/Type)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_setMax_m372170337_MetadataUsageId;
extern "C"  void Statistics_setMax_m372170337 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007 * ___s0, int32_t ___t1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_setMax_m372170337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Statistics_t2537377007 * L_0 = ___s0;
		int32_t L_1 = ___t1;
		Statistics_t2537377007  L_2 = Statistics_get_minimum_stats_m1828799693(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = ___t1;
		float L_4 = Statistics_get_Item_m3520282030((&V_0), L_3, /*hidden argument*/NULL);
		Statistics_t2537377007 * L_5 = ___s0;
		int32_t L_6 = ___t1;
		float L_7 = Statistics_get_Item_m3520282030(L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		Statistics_set_Item_m1870614441(L_0, L_1, L_8, /*hidden argument*/NULL);
		return;
	}
}
// Statistics Statistics::LevelStats(Statistics)
extern "C"  Statistics_t2537377007  Statistics_LevelStats_m2675799454 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___s0, const MethodInfo* method)
{
	{
		Statistics_LevelStats_m2515529360(NULL /*static, unused*/, (&___s0), /*hidden argument*/NULL);
		Statistics_t2537377007  L_0 = ___s0;
		return L_0;
	}
}
// Statistics Statistics::Add(Statistics)
extern "C"  Statistics_t2537377007  Statistics_Add_m185531530 (Statistics_t2537377007 * __this, Statistics_t2537377007  ___stat0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	uint32_t V_4 = 0;
	{
		float L_0 = __this->get_speed_0();
		float L_1 = Statistics_get_Item_m3520282030((&___stat0), 0, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0+(float)L_1));
		float L_2 = __this->get_acceleration_1();
		float L_3 = Statistics_get_Item_m3520282030((&___stat0), 1, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2+(float)L_3));
		float L_4 = __this->get_lifespan_2();
		float L_5 = Statistics_get_Item_m3520282030((&___stat0), 2, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_4+(float)L_5));
		float L_6 = __this->get_docking_3();
		float L_7 = Statistics_get_Item_m3520282030((&___stat0), 3, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_6+(float)L_7));
		uint32_t L_8 = __this->get_replication_rate_4();
		float L_9 = Statistics_get_Item_m3520282030((&___stat0), 4, /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)(((int32_t)((uint32_t)L_9)))));
		float L_10 = V_0;
		float L_11 = V_1;
		float L_12 = V_2;
		float L_13 = V_3;
		uint32_t L_14 = V_4;
		Statistics_t2537377007  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Statistics__ctor_m1821818148(&L_15, L_10, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  Statistics_t2537377007  Statistics_Add_m185531530_AdjustorThunk (Il2CppObject * __this, Statistics_t2537377007  ___stat0, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	return Statistics_Add_m185531530(_thisAdjusted, ___stat0, method);
}
// System.Boolean Statistics::Equals(System.Object)
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_Equals_m1761574963_MetadataUsageId;
extern "C"  bool Statistics_Equals_m1761574963 (Statistics_t2537377007 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_Equals_m1761574963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Statistics_t2537377007  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___o0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		int32_t L_2 = Statistics_GetHashCode_m1112660161(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}
	{
		return (bool)0;
	}

IL_0019:
	{
		Il2CppObject * L_3 = ___o0;
		V_0 = ((*(Statistics_t2537377007 *)((Statistics_t2537377007 *)UnBox (L_3, Statistics_t2537377007_il2cpp_TypeInfo_var))));
		V_1 = 0;
		goto IL_0041;
	}

IL_0027:
	{
		int32_t L_4 = V_1;
		float L_5 = Statistics_get_Item_m3520282030((&V_0), L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		float L_7 = Statistics_GetIndex_m814139896(__this, L_6, /*hidden argument*/NULL);
		if ((((float)L_5) == ((float)L_7)))
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_9 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			goto IL_0027;
		}
	}
	{
		return (bool)1;
	}
}
extern "C"  bool Statistics_Equals_m1761574963_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	return Statistics_Equals_m1761574963(_thisAdjusted, ___o0, method);
}
// System.Int32 Statistics::GetHashCode()
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_GetHashCode_m1112660161_MetadataUsageId;
extern "C"  int32_t Statistics_GetHashCode_m1112660161 (Statistics_t2537377007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_GetHashCode_m1112660161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Statistics_t2537377007  L_0 = (*(Statistics_t2537377007 *)__this);
		Il2CppObject * L_1 = Box(Statistics_t2537377007_il2cpp_TypeInfo_var, &L_0);
		NullCheck((ValueType_t3507792607 *)L_1);
		int32_t L_2 = ValueType_GetHashCode_m3018627007((ValueType_t3507792607 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Statistics_GetHashCode_m1112660161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	return Statistics_GetHashCode_m1112660161(_thisAdjusted, method);
}
// System.Single Statistics::get_Item(Statistics/Type)
extern "C"  float Statistics_get_Item_m3520282030 (Statistics_t2537377007 * __this, int32_t ___t0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___t0;
		float L_1 = Statistics_GetIndex_m814139896(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  float Statistics_get_Item_m3520282030_AdjustorThunk (Il2CppObject * __this, int32_t ___t0, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	return Statistics_get_Item_m3520282030(_thisAdjusted, ___t0, method);
}
// System.Void Statistics::set_Item(Statistics/Type,System.Single)
extern "C"  void Statistics_set_Item_m1870614441 (Statistics_t2537377007 * __this, int32_t ___t0, float ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___t0;
		float L_1 = ___value1;
		Statistics_SetIndex_m3744871941(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Statistics_set_Item_m1870614441_AdjustorThunk (Il2CppObject * __this, int32_t ___t0, float ___value1, const MethodInfo* method)
{
	Statistics_t2537377007 * _thisAdjusted = reinterpret_cast<Statistics_t2537377007 *>(__this + 1);
	Statistics_set_Item_m1870614441(_thisAdjusted, ___t0, ___value1, method);
}
// Statistics Statistics::op_Addition(Statistics,Statistics)
extern "C"  Statistics_t2537377007  Statistics_op_Addition_m3593248452 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___a0, Statistics_t2537377007  ___b1, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0 = ___b1;
		Statistics_t2537377007  L_1 = Statistics_Add_m185531530((&___a0), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Statistics::op_Equality(Statistics,Statistics)
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_op_Equality_m463644592_MetadataUsageId;
extern "C"  bool Statistics_op_Equality_m463644592 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___a0, Statistics_t2537377007  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_op_Equality_m463644592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Statistics_t2537377007  L_0 = ___b1;
		Statistics_t2537377007  L_1 = L_0;
		Il2CppObject * L_2 = Box(Statistics_t2537377007_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = Statistics_Equals_m1761574963((&___a0), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Statistics::op_Inequality(Statistics,Statistics)
extern Il2CppClass* Statistics_t2537377007_il2cpp_TypeInfo_var;
extern const uint32_t Statistics_op_Inequality_m1100537845_MetadataUsageId;
extern "C"  bool Statistics_op_Inequality_m1100537845 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___a0, Statistics_t2537377007  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Statistics_op_Inequality_m1100537845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Statistics_t2537377007  L_0 = ___b1;
		Statistics_t2537377007  L_1 = L_0;
		Il2CppObject * L_2 = Box(Statistics_t2537377007_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = Statistics_Equals_m1761574963((&___a0), L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}
}
// Statistics Statistics::get_base_stats()
extern "C"  Statistics_t2537377007  Statistics_get_base_stats_m579726902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Statistics__ctor_m1821818148(&L_0, (40.0f), (2.0f), (20.0f), (4.0f), 2, /*hidden argument*/NULL);
		return L_0;
	}
}
// Statistics Statistics::get_minimum_stats()
extern "C"  Statistics_t2537377007  Statistics_get_minimum_stats_m1828799693 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Statistics_t2537377007  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Statistics__ctor_m1821818148(&L_0, (1.0f), (0.0f), (0.0f), (0.0f), 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SwipeController::.ctor()
extern "C"  void SwipeController__ctor_m2671769271 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		__this->set__swipeTolerance_2((20.0f));
		__this->set__timeDelay_3((0.5f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__initialPosition_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Controller SwipeController::get_controller()
extern const MethodInfo* GameObject_GetComponent_TisController_t1937198888_m3117371903_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2665277406;
extern const uint32_t SwipeController_get_controller_m201288529_MetadataUsageId;
extern "C"  Controller_t1937198888 * SwipeController_get_controller_m201288529 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SwipeController_get_controller_m201288529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral2665277406, /*hidden argument*/NULL);
		NullCheck(L_0);
		Controller_t1937198888 * L_1 = GameObject_GetComponent_TisController_t1937198888_m3117371903(L_0, /*hidden argument*/GameObject_GetComponent_TisController_t1937198888_m3117371903_MethodInfo_var);
		return L_1;
	}
}
// System.Single SwipeController::get_Distance()
extern "C"  float SwipeController_get_Distance_m2275616339 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get__dx_5();
		float L_1 = __this->get__dy_6();
		Vector3__ctor_m2720820983((&V_0), L_0, L_1, /*hidden argument*/NULL);
		float L_2 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single SwipeController::get_DeltaTime()
extern "C"  float SwipeController_get_DeltaTime_m3847877361 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__dt_7();
		return L_0;
	}
}
// UnityEngine.Vector3 SwipeController::get_SwipeVelocity()
extern "C"  Vector3_t2243707580  SwipeController_get_SwipeVelocity_m1495297687 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_swipeVelocity_8();
		return L_0;
	}
}
// UnityEngine.Vector3 SwipeController::get_SwipeDirection()
extern "C"  Vector3_t2243707580  SwipeController_get_SwipeDirection_m3561092737 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = SwipeController_get_SwipeVelocity_m1495297687(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t2243707580  L_1 = Vector3_get_normalized_m936072361((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.GameObject SwipeController::get_InitialObject()
extern "C"  GameObject_t1756533147 * SwipeController_get_InitialObject_m2745419468 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get__initialObject_9();
		return L_0;
	}
}
// System.Void SwipeController::StartSwipe()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SwipeController_StartSwipe_m1039645603_MetadataUsageId;
extern "C"  void SwipeController_StartSwipe_m1039645603 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SwipeController_StartSwipe_m1039645603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Controller_t1937198888 * L_0 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Controller_getSelected_m1390842220(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Controller_t1937198888 * L_3 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Controller_getSelected_m1390842220(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		__this->set__initialObject_9(L_5);
	}

IL_002c:
	{
		Controller_t1937198888 * L_6 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Controller_get_screenPosition_m2179745093(L_6, /*hidden argument*/NULL);
		__this->set__initialPosition_4(L_7);
		SwipeController_updateTime_m2949054136(__this, (bool)1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_swipeVelocity_8(L_8);
		return;
	}
}
// System.Void SwipeController::UpdateSwipe()
extern "C"  void SwipeController_UpdateSwipe_m3961951132 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Controller_t1937198888 * L_0 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Controller_get_screenPosition_m2179745093(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		Vector3_t2243707580 * L_3 = __this->get_address_of__initialPosition_4();
		float L_4 = L_3->get_x_1();
		__this->set__dx_5(((float)((float)L_2-(float)L_4)));
		Controller_t1937198888 * L_5 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Controller_get_screenPosition_m2179745093(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		Vector3_t2243707580 * L_8 = __this->get_address_of__initialPosition_4();
		float L_9 = L_8->get_y_2();
		__this->set__dy_6(((float)((float)L_7-(float)L_9)));
		SwipeController_updateTime_m2949054136(__this, (bool)0, /*hidden argument*/NULL);
		SwipeController_CalculateVelocity_m364530922(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SwipeController::CalculateVelocity()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t SwipeController_CalculateVelocity_m364530922_MetadataUsageId;
extern "C"  void SwipeController_CalculateVelocity_m364530922 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SwipeController_CalculateVelocity_m364530922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get__dt_7();
		float L_1 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0037;
		}
	}
	{
		float L_2 = __this->get__dx_5();
		float L_3 = __this->get__dy_6();
		Vector3_t2243707580  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m2720820983(&L_4, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get__dt_7();
		Vector3_t2243707580  L_6 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->set_swipeVelocity_8(L_6);
		goto IL_0046;
	}

IL_0037:
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_7 = V_0;
		__this->set_swipeVelocity_8(L_7);
	}

IL_0046:
	{
		return;
	}
}
// System.Void SwipeController::updateTime(System.Boolean)
extern "C"  void SwipeController_updateTime_m2949054136 (SwipeController_t751295678 * __this, bool ___reset0, const MethodInfo* method)
{
	SwipeController_t751295678 * G_B2_0 = NULL;
	SwipeController_t751295678 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	SwipeController_t751295678 * G_B3_1 = NULL;
	{
		bool L_0 = ___reset0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B1_0;
		goto IL_001d;
	}

IL_0011:
	{
		float L_1 = __this->get__dt_7();
		float L_2 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)L_1+(float)L_2));
		G_B3_1 = G_B2_0;
	}

IL_001d:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__dt_7(G_B3_0);
		return;
	}
}
// System.Void SwipeController::Release()
extern "C"  void SwipeController_Release_m1932931132 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		SwipeController_CalculateVelocity_m364530922(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__initialPosition_4(L_0);
		return;
	}
}
// System.Void SwipeController::Reset()
extern "C"  void SwipeController_Reset_m1423963338 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		__this->set__dx_5((0.0f));
		__this->set__dy_6((0.0f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__initialPosition_4(L_0);
		__this->set__initialObject_9((GameObject_t1756533147 *)NULL);
		return;
	}
}
// System.Void SwipeController::Update()
extern "C"  void SwipeController_Update_m3955015020 (SwipeController_t751295678 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Controller_LeftClick_m156288658(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		SwipeController_StartSwipe_m1039645603(__this, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_001b:
	{
		Controller_t1937198888 * L_2 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = Controller_LeftDrag_m794894538(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		SwipeController_UpdateSwipe_m3961951132(__this, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0036:
	{
		Controller_t1937198888 * L_4 = SwipeController_get_controller_m201288529(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = Controller_LeftRelease_m547954749(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		SwipeController_Release_m1932931132(__this, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0051:
	{
		SwipeController_Reset_m1423963338(__this, /*hidden argument*/NULL);
		SwipeController_updateTime_m2949054136(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
