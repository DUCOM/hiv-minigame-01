﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BCell
struct BCell_t1815094666;
// Timer
struct Timer_t2917042437;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"

// System.Void BCell::.ctor()
extern "C"  void BCell__ctor_m2954648583 (BCell_t1815094666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BCell::OnTimerEnd(Timer)
extern "C"  void BCell_OnTimerEnd_m1536704493 (BCell_t1815094666 * __this, Timer_t2917042437 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BCell::Awake()
extern "C"  void BCell_Awake_m3140834854 (BCell_t1815094666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BCell::Shoot(System.Int32)
extern "C"  void BCell_Shoot_m3208517109 (BCell_t1815094666 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BCell::Update()
extern "C"  void BCell_Update_m31384788 (BCell_t1815094666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
