﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Capsid
struct UI_Capsid_t4027023025;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Capsid::.ctor()
extern "C"  void UI_Capsid__ctor_m2497196506 (UI_Capsid_t4027023025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
