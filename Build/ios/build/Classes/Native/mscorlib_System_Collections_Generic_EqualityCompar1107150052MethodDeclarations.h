﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ImageRender>
struct DefaultComparer_t1107150052;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ImageRender>::.ctor()
extern "C"  void DefaultComparer__ctor_m3342185353_gshared (DefaultComparer_t1107150052 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3342185353(__this, method) ((  void (*) (DefaultComparer_t1107150052 *, const MethodInfo*))DefaultComparer__ctor_m3342185353_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ImageRender>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2005302624_gshared (DefaultComparer_t1107150052 * __this, ImageRender_t3780187187  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2005302624(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1107150052 *, ImageRender_t3780187187 , const MethodInfo*))DefaultComparer_GetHashCode_m2005302624_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ImageRender>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m614163168_gshared (DefaultComparer_t1107150052 * __this, ImageRender_t3780187187  ___x0, ImageRender_t3780187187  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m614163168(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1107150052 *, ImageRender_t3780187187 , ImageRender_t3780187187 , const MethodInfo*))DefaultComparer_Equals_m614163168_gshared)(__this, ___x0, ___y1, method)
