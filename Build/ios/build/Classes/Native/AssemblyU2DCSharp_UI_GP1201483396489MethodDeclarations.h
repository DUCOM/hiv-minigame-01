﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_GP120
struct UI_GP120_t1483396489;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void UI_GP120::.ctor()
extern "C"  void UI_GP120__ctor_m1982076012 (UI_GP120_t1483396489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UI_GP120::get_color()
extern "C"  Color_t2020392075  UI_GP120_get_color_m2651926781 (UI_GP120_t1483396489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_GP120::set_color(UnityEngine.Color)
extern "C"  void UI_GP120_set_color_m1014429084 (UI_GP120_t1483396489 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UI_GP120::get_colorLeft()
extern "C"  Color_t2020392075  UI_GP120_get_colorLeft_m2212695404 (UI_GP120_t1483396489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_GP120::set_colorLeft(UnityEngine.Color)
extern "C"  void UI_GP120_set_colorLeft_m1174730493 (UI_GP120_t1483396489 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UI_GP120::get_colorRight()
extern "C"  Color_t2020392075  UI_GP120_get_colorRight_m2617596573 (UI_GP120_t1483396489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_GP120::set_colorRight(UnityEngine.Color)
extern "C"  void UI_GP120_set_colorRight_m370336612 (UI_GP120_t1483396489 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
