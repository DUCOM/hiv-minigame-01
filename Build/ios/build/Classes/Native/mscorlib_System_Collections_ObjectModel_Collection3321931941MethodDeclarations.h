﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<ImageRender>
struct Collection_1_t3321931941;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ImageRender[]
struct ImageRenderU5BU5D_t1559399842;
// System.Collections.Generic.IEnumerator`1<ImageRender>
struct IEnumerator_1_t1255711014;
// System.Collections.Generic.IList`1<ImageRender>
struct IList_1_t26160492;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ImageRender3780187187.h"

// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::.ctor()
extern "C"  void Collection_1__ctor_m3231331818_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3231331818(__this, method) ((  void (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1__ctor_m3231331818_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1721060065_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1721060065(__this, method) ((  bool (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1721060065_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m633073666_gshared (Collection_1_t3321931941 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m633073666(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3321931941 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m633073666_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1421724309_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1421724309(__this, method) ((  Il2CppObject * (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1421724309_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2981949970_gshared (Collection_1_t3321931941 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2981949970(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3321931941 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2981949970_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1532525916_gshared (Collection_1_t3321931941 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1532525916(__this, ___value0, method) ((  bool (*) (Collection_1_t3321931941 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1532525916_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m356281312_gshared (Collection_1_t3321931941 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m356281312(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3321931941 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m356281312_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2913454541_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2913454541(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2913454541_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4178273341_gshared (Collection_1_t3321931941 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4178273341(__this, ___value0, method) ((  void (*) (Collection_1_t3321931941 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4178273341_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3917404226_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3917404226(__this, method) ((  bool (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3917404226_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3331269554_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3331269554(__this, method) ((  Il2CppObject * (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3331269554_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3919335281_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3919335281(__this, method) ((  bool (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3919335281_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m232913382_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m232913382(__this, method) ((  bool (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m232913382_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3716545609_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3716545609(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3321931941 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3716545609_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1722624668_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1722624668(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1722624668_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::Add(T)
extern "C"  void Collection_1_Add_m3492046425_gshared (Collection_1_t3321931941 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3492046425(__this, ___item0, method) ((  void (*) (Collection_1_t3321931941 *, ImageRender_t3780187187 , const MethodInfo*))Collection_1_Add_m3492046425_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::Clear()
extern "C"  void Collection_1_Clear_m1827095045_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1827095045(__this, method) ((  void (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_Clear_m1827095045_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3446581411_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3446581411(__this, method) ((  void (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_ClearItems_m3446581411_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::Contains(T)
extern "C"  bool Collection_1_Contains_m23232231_gshared (Collection_1_t3321931941 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m23232231(__this, ___item0, method) ((  bool (*) (Collection_1_t3321931941 *, ImageRender_t3780187187 , const MethodInfo*))Collection_1_Contains_m23232231_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4226739409_gshared (Collection_1_t3321931941 * __this, ImageRenderU5BU5D_t1559399842* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4226739409(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3321931941 *, ImageRenderU5BU5D_t1559399842*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4226739409_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<ImageRender>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4078732302_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4078732302(__this, method) ((  Il2CppObject* (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_GetEnumerator_m4078732302_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ImageRender>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4026636629_gshared (Collection_1_t3321931941 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4026636629(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3321931941 *, ImageRender_t3780187187 , const MethodInfo*))Collection_1_IndexOf_m4026636629_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1332534676_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, ImageRender_t3780187187  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1332534676(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))Collection_1_Insert_m1332534676_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3257579175_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, ImageRender_t3780187187  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3257579175(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))Collection_1_InsertItem_m3257579175_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::Remove(T)
extern "C"  bool Collection_1_Remove_m2495963442_gshared (Collection_1_t3321931941 * __this, ImageRender_t3780187187  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2495963442(__this, ___item0, method) ((  bool (*) (Collection_1_t3321931941 *, ImageRender_t3780187187 , const MethodInfo*))Collection_1_Remove_m2495963442_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3901678656_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3901678656(__this, ___index0, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3901678656_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4180476898_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4180476898(__this, ___index0, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4180476898_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ImageRender>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3596854242_gshared (Collection_1_t3321931941 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3596854242(__this, method) ((  int32_t (*) (Collection_1_t3321931941 *, const MethodInfo*))Collection_1_get_Count_m3596854242_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<ImageRender>::get_Item(System.Int32)
extern "C"  ImageRender_t3780187187  Collection_1_get_Item_m2220657838_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2220657838(__this, ___index0, method) ((  ImageRender_t3780187187  (*) (Collection_1_t3321931941 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2220657838_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1554986585_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, ImageRender_t3780187187  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1554986585(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))Collection_1_set_Item_m1554986585_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2298083836_gshared (Collection_1_t3321931941 * __this, int32_t ___index0, ImageRender_t3780187187  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2298083836(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3321931941 *, int32_t, ImageRender_t3780187187 , const MethodInfo*))Collection_1_SetItem_m2298083836_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2117483_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2117483(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2117483_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<ImageRender>::ConvertItem(System.Object)
extern "C"  ImageRender_t3780187187  Collection_1_ConvertItem_m216001067_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m216001067(__this /* static, unused */, ___item0, method) ((  ImageRender_t3780187187  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m216001067_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ImageRender>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2205678903_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2205678903(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2205678903_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m665617967_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m665617967(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m665617967_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ImageRender>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2112742208_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2112742208(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2112742208_gshared)(__this /* static, unused */, ___list0, method)
