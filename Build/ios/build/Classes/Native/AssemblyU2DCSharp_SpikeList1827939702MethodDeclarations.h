﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpikeList
struct SpikeList_t1827939702;
// Spike
struct Spike_t60554746;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

// System.Void SpikeList::.ctor()
extern "C"  void SpikeList__ctor_m3681244969 (SpikeList_t1827939702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpikeList::get_count()
extern "C"  int32_t SpikeList_get_count_m3219171817 (SpikeList_t1827939702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Spike SpikeList::get_Item(System.Int32)
extern "C"  Spike_t60554746 * SpikeList_get_Item_m546146431 (SpikeList_t1827939702 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpikeList::Instantiate(UnityEngine.Transform,System.Single)
extern "C"  void SpikeList_Instantiate_m1789477637 (SpikeList_t1827939702 * __this, Transform_t3275118058 * ___parent0, float ___radius1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpikeList::Instantiate(UnityEngine.Transform,System.Single,System.UInt32)
extern "C"  void SpikeList_Instantiate_m417580653 (SpikeList_t1827939702 * __this, Transform_t3275118058 * ___parent0, float ___radius1, uint32_t ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpikeList::Instantiate(UnityEngine.Transform,System.Single,System.UInt32,Tropism)
extern "C"  void SpikeList_Instantiate_m1425764107 (SpikeList_t1827939702 * __this, Transform_t3275118058 * ___parent0, float ___radius1, uint32_t ___c2, Tropism_t3662836552  ___t3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpikeList::ChangeTropism(Tropism)
extern "C"  void SpikeList_ChangeTropism_m419626291 (SpikeList_t1827939702 * __this, Tropism_t3662836552  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpikeList::Scale(System.Single)
extern "C"  void SpikeList_Scale_m1908817848 (SpikeList_t1827939702 * __this, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
