﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1666134906.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22398422722.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2141168683_gshared (Enumerator_t1666134906 * __this, Dictionary_2_t346110204 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2141168683(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1666134906 *, Dictionary_2_t346110204 *, const MethodInfo*))Enumerator__ctor_m2141168683_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3758649980_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3758649980(__this, method) ((  Il2CppObject * (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3758649980_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3754824034_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3754824034(__this, method) ((  void (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3754824034_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4269452057_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4269452057(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4269452057_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m771697758_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m771697758(__this, method) ((  Il2CppObject * (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m771697758_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2095843620_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2095843620(__this, method) ((  Il2CppObject * (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2095843620_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2382907618_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2382907618(__this, method) ((  bool (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_MoveNext_m2382907618_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::get_Current()
extern "C"  KeyValuePair_2_t2398422722  Enumerator_get_Current_m3721160634_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3721160634(__this, method) ((  KeyValuePair_2_t2398422722  (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_get_Current_m3721160634_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3378286407_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3378286407(__this, method) ((  int32_t (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_get_CurrentKey_m3378286407_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::get_CurrentValue()
extern "C"  float Enumerator_get_CurrentValue_m3481442679_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3481442679(__this, method) ((  float (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_get_CurrentValue_m3481442679_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::Reset()
extern "C"  void Enumerator_Reset_m2063147177_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2063147177(__this, method) ((  void (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_Reset_m2063147177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::VerifyState()
extern "C"  void Enumerator_VerifyState_m248812886_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m248812886(__this, method) ((  void (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_VerifyState_m248812886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3264064202_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3264064202(__this, method) ((  void (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_VerifyCurrent_m3264064202_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Tropism/ReceptorType,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m880523095_gshared (Enumerator_t1666134906 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m880523095(__this, method) ((  void (*) (Enumerator_t1666134906 *, const MethodInfo*))Enumerator_Dispose_m880523095_gshared)(__this, method)
