﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "AssemblyU2DCSharp_Statistics_Type1319459628.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Statistics::.ctor(System.Single,System.Single,System.Single,System.Single,System.UInt32)
extern "C"  void Statistics__ctor_m1821818148 (Statistics_t2537377007 * __this, float ___s0, float ___a1, float ___l2, float ___d3, uint32_t ___r4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Statistics::.ctor(Statistics)
extern "C"  void Statistics__ctor_m2616661291 (Statistics_t2537377007 * __this, Statistics_t2537377007  ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Statistics::GetIndex(Statistics/Type)
extern "C"  float Statistics_GetIndex_m814139896 (Statistics_t2537377007 * __this, int32_t ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Statistics::SetIndex(Statistics/Type,System.Single)
extern "C"  void Statistics_SetIndex_m3744871941 (Statistics_t2537377007 * __this, int32_t ___t0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Statistics::LevelStats(Statistics&)
extern "C"  void Statistics_LevelStats_m2515529360 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Statistics::setMax(Statistics&,Statistics/Type)
extern "C"  void Statistics_setMax_m372170337 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007 * ___s0, int32_t ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics Statistics::LevelStats(Statistics)
extern "C"  Statistics_t2537377007  Statistics_LevelStats_m2675799454 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics Statistics::Add(Statistics)
extern "C"  Statistics_t2537377007  Statistics_Add_m185531530 (Statistics_t2537377007 * __this, Statistics_t2537377007  ___stat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Statistics::Equals(System.Object)
extern "C"  bool Statistics_Equals_m1761574963 (Statistics_t2537377007 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Statistics::GetHashCode()
extern "C"  int32_t Statistics_GetHashCode_m1112660161 (Statistics_t2537377007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Statistics::get_Item(Statistics/Type)
extern "C"  float Statistics_get_Item_m3520282030 (Statistics_t2537377007 * __this, int32_t ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Statistics::set_Item(Statistics/Type,System.Single)
extern "C"  void Statistics_set_Item_m1870614441 (Statistics_t2537377007 * __this, int32_t ___t0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics Statistics::op_Addition(Statistics,Statistics)
extern "C"  Statistics_t2537377007  Statistics_op_Addition_m3593248452 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___a0, Statistics_t2537377007  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Statistics::op_Equality(Statistics,Statistics)
extern "C"  bool Statistics_op_Equality_m463644592 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___a0, Statistics_t2537377007  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Statistics::op_Inequality(Statistics,Statistics)
extern "C"  bool Statistics_op_Inequality_m1100537845 (Il2CppObject * __this /* static, unused */, Statistics_t2537377007  ___a0, Statistics_t2537377007  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics Statistics::get_base_stats()
extern "C"  Statistics_t2537377007  Statistics_get_base_stats_m579726902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics Statistics::get_minimum_stats()
extern "C"  Statistics_t2537377007  Statistics_get_minimum_stats_m1828799693 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
