﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1020132809.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"

// System.Void System.Array/InternalEnumerator`1<Tropism/CoReceptorType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4190242528_gshared (InternalEnumerator_1_t1020132809 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4190242528(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1020132809 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4190242528_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Tropism/CoReceptorType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3310644904_gshared (InternalEnumerator_1_t1020132809 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3310644904(__this, method) ((  void (*) (InternalEnumerator_1_t1020132809 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3310644904_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Tropism/CoReceptorType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m309989004_gshared (InternalEnumerator_1_t1020132809 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m309989004(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1020132809 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m309989004_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Tropism/CoReceptorType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1377329639_gshared (InternalEnumerator_1_t1020132809 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1377329639(__this, method) ((  void (*) (InternalEnumerator_1_t1020132809 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1377329639_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Tropism/CoReceptorType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2320351512_gshared (InternalEnumerator_1_t1020132809 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2320351512(__this, method) ((  bool (*) (InternalEnumerator_1_t1020132809 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2320351512_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Tropism/CoReceptorType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2393814855_gshared (InternalEnumerator_1_t1020132809 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2393814855(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1020132809 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2393814855_gshared)(__this, method)
