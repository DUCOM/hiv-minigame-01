﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScoreDisplay
struct ScoreDisplay_t164935196;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreDisplay
struct  ScoreDisplay_t164935196  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator ScoreDisplay::_anim
	Animator_t69676727 * ____anim_3;

public:
	inline static int32_t get_offset_of__anim_3() { return static_cast<int32_t>(offsetof(ScoreDisplay_t164935196, ____anim_3)); }
	inline Animator_t69676727 * get__anim_3() const { return ____anim_3; }
	inline Animator_t69676727 ** get_address_of__anim_3() { return &____anim_3; }
	inline void set__anim_3(Animator_t69676727 * value)
	{
		____anim_3 = value;
		Il2CppCodeGenWriteBarrier(&____anim_3, value);
	}
};

struct ScoreDisplay_t164935196_StaticFields
{
public:
	// ScoreDisplay ScoreDisplay::main
	ScoreDisplay_t164935196 * ___main_2;

public:
	inline static int32_t get_offset_of_main_2() { return static_cast<int32_t>(offsetof(ScoreDisplay_t164935196_StaticFields, ___main_2)); }
	inline ScoreDisplay_t164935196 * get_main_2() const { return ___main_2; }
	inline ScoreDisplay_t164935196 ** get_address_of_main_2() { return &___main_2; }
	inline void set_main_2(ScoreDisplay_t164935196 * value)
	{
		___main_2 = value;
		Il2CppCodeGenWriteBarrier(&___main_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
