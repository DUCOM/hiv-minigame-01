﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2032642968.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/ReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m585691433_gshared (Enumerator_t2032642968 * __this, Dictionary_2_t346110204 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m585691433(__this, ___host0, method) ((  void (*) (Enumerator_t2032642968 *, Dictionary_2_t346110204 *, const MethodInfo*))Enumerator__ctor_m585691433_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1938521454_gshared (Enumerator_t2032642968 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1938521454(__this, method) ((  Il2CppObject * (*) (Enumerator_t2032642968 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1938521454_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/ReceptorType,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m138965968_gshared (Enumerator_t2032642968 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m138965968(__this, method) ((  void (*) (Enumerator_t2032642968 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m138965968_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/ReceptorType,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m3713367033_gshared (Enumerator_t2032642968 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3713367033(__this, method) ((  void (*) (Enumerator_t2032642968 *, const MethodInfo*))Enumerator_Dispose_m3713367033_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/ReceptorType,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m96815756_gshared (Enumerator_t2032642968 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m96815756(__this, method) ((  bool (*) (Enumerator_t2032642968 *, const MethodInfo*))Enumerator_MoveNext_m96815756_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Tropism/ReceptorType,System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m396044398_gshared (Enumerator_t2032642968 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m396044398(__this, method) ((  float (*) (Enumerator_t2032642968 *, const MethodInfo*))Enumerator_get_Current_m396044398_gshared)(__this, method)
