﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Animator
struct Animator_t69676727;
// UI_Immunogenicity
struct UI_Immunogenicity_t1529844486;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity_States3735768305.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Immunogenicity
struct  UI_Immunogenicity_t1529844486  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image UI_Immunogenicity::barFiller
	Image_t2042527209 * ___barFiller_2;
	// System.Single UI_Immunogenicity::threshhold
	float ___threshhold_3;
	// UnityEngine.Animator UI_Immunogenicity::anim
	Animator_t69676727 * ___anim_4;
	// System.Single UI_Immunogenicity::fallRate
	float ___fallRate_5;
	// System.Single UI_Immunogenicity::fillRate
	float ___fillRate_6;
	// System.Single UI_Immunogenicity::progress
	float ___progress_7;
	// System.Single UI_Immunogenicity::maxProgress
	float ___maxProgress_8;
	// UI_Immunogenicity/States UI_Immunogenicity::state
	int32_t ___state_9;
	// System.Single UI_Immunogenicity::fillIntended
	float ___fillIntended_10;

public:
	inline static int32_t get_offset_of_barFiller_2() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___barFiller_2)); }
	inline Image_t2042527209 * get_barFiller_2() const { return ___barFiller_2; }
	inline Image_t2042527209 ** get_address_of_barFiller_2() { return &___barFiller_2; }
	inline void set_barFiller_2(Image_t2042527209 * value)
	{
		___barFiller_2 = value;
		Il2CppCodeGenWriteBarrier(&___barFiller_2, value);
	}

	inline static int32_t get_offset_of_threshhold_3() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___threshhold_3)); }
	inline float get_threshhold_3() const { return ___threshhold_3; }
	inline float* get_address_of_threshhold_3() { return &___threshhold_3; }
	inline void set_threshhold_3(float value)
	{
		___threshhold_3 = value;
	}

	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___anim_4)); }
	inline Animator_t69676727 * get_anim_4() const { return ___anim_4; }
	inline Animator_t69676727 ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animator_t69676727 * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier(&___anim_4, value);
	}

	inline static int32_t get_offset_of_fallRate_5() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___fallRate_5)); }
	inline float get_fallRate_5() const { return ___fallRate_5; }
	inline float* get_address_of_fallRate_5() { return &___fallRate_5; }
	inline void set_fallRate_5(float value)
	{
		___fallRate_5 = value;
	}

	inline static int32_t get_offset_of_fillRate_6() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___fillRate_6)); }
	inline float get_fillRate_6() const { return ___fillRate_6; }
	inline float* get_address_of_fillRate_6() { return &___fillRate_6; }
	inline void set_fillRate_6(float value)
	{
		___fillRate_6 = value;
	}

	inline static int32_t get_offset_of_progress_7() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___progress_7)); }
	inline float get_progress_7() const { return ___progress_7; }
	inline float* get_address_of_progress_7() { return &___progress_7; }
	inline void set_progress_7(float value)
	{
		___progress_7 = value;
	}

	inline static int32_t get_offset_of_maxProgress_8() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___maxProgress_8)); }
	inline float get_maxProgress_8() const { return ___maxProgress_8; }
	inline float* get_address_of_maxProgress_8() { return &___maxProgress_8; }
	inline void set_maxProgress_8(float value)
	{
		___maxProgress_8 = value;
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___state_9)); }
	inline int32_t get_state_9() const { return ___state_9; }
	inline int32_t* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(int32_t value)
	{
		___state_9 = value;
	}

	inline static int32_t get_offset_of_fillIntended_10() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486, ___fillIntended_10)); }
	inline float get_fillIntended_10() const { return ___fillIntended_10; }
	inline float* get_address_of_fillIntended_10() { return &___fillIntended_10; }
	inline void set_fillIntended_10(float value)
	{
		___fillIntended_10 = value;
	}
};

struct UI_Immunogenicity_t1529844486_StaticFields
{
public:
	// UI_Immunogenicity UI_Immunogenicity::main
	UI_Immunogenicity_t1529844486 * ___main_11;

public:
	inline static int32_t get_offset_of_main_11() { return static_cast<int32_t>(offsetof(UI_Immunogenicity_t1529844486_StaticFields, ___main_11)); }
	inline UI_Immunogenicity_t1529844486 * get_main_11() const { return ___main_11; }
	inline UI_Immunogenicity_t1529844486 ** get_address_of_main_11() { return &___main_11; }
	inline void set_main_11(UI_Immunogenicity_t1529844486 * value)
	{
		___main_11 = value;
		Il2CppCodeGenWriteBarrier(&___main_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
