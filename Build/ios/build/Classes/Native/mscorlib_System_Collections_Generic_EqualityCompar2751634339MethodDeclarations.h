﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Antibody/State>
struct DefaultComparer_t2751634339;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Antibody/State>::.ctor()
extern "C"  void DefaultComparer__ctor_m2590192138_gshared (DefaultComparer_t2751634339 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2590192138(__this, method) ((  void (*) (DefaultComparer_t2751634339 *, const MethodInfo*))DefaultComparer__ctor_m2590192138_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Antibody/State>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1132863469_gshared (DefaultComparer_t2751634339 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1132863469(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2751634339 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1132863469_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Antibody/State>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3014930017_gshared (DefaultComparer_t2751634339 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3014930017(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2751634339 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3014930017_gshared)(__this, ___x0, ___y1, method)
