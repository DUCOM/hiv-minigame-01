﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1875355877.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"

// System.Void System.Array/InternalEnumerator`1<Tropism/ReceptorType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3363556078_gshared (InternalEnumerator_1_t1875355877 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3363556078(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1875355877 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3363556078_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Tropism/ReceptorType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1280819038_gshared (InternalEnumerator_1_t1875355877 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1280819038(__this, method) ((  void (*) (InternalEnumerator_1_t1875355877 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1280819038_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Tropism/ReceptorType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3053066850_gshared (InternalEnumerator_1_t1875355877 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3053066850(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1875355877 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3053066850_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Tropism/ReceptorType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1370326079_gshared (InternalEnumerator_1_t1875355877 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1370326079(__this, method) ((  void (*) (InternalEnumerator_1_t1875355877 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1370326079_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Tropism/ReceptorType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1991643158_gshared (InternalEnumerator_1_t1875355877 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1991643158(__this, method) ((  bool (*) (InternalEnumerator_1_t1875355877 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1991643158_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Tropism/ReceptorType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2791479431_gshared (InternalEnumerator_1_t1875355877 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2791479431(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1875355877 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2791479431_gshared)(__this, method)
