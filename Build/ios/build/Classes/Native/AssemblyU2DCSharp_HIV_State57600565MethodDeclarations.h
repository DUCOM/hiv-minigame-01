﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_State
struct HIV_State_t57600565;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_State::.ctor()
extern "C"  void HIV_State__ctor_m1425936220 (HIV_State_t57600565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_State::Awake()
extern "C"  void HIV_State_Awake_m3048860341 (HIV_State_t57600565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
