﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpriteDisplay
struct SpriteDisplay_t1886630049;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SpriteDisplay_SpriteChange4213660865.h"

// System.Void SpriteDisplay::.ctor()
extern "C"  void SpriteDisplay__ctor_m3083842324 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpriteDisplay::get_spriteCount()
extern "C"  int32_t SpriteDisplay_get_spriteCount_m1357692313 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpriteDisplay::get_maxSprite()
extern "C"  int32_t SpriteDisplay_get_maxSprite_m4157804466 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteDisplay::Start()
extern "C"  void SpriteDisplay_Start_m2382151612 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteDisplay::UpdateSprite()
extern "C"  void SpriteDisplay_UpdateSprite_m2828589862 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteDisplay::Update()
extern "C"  void SpriteDisplay_Update_m455565409 (SpriteDisplay_t1886630049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteDisplay::UpdateSprite(SpriteDisplay/SpriteChange)
extern "C"  void SpriteDisplay_UpdateSprite_m2878970303 (SpriteDisplay_t1886630049 * __this, int32_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteDisplay::Scale(System.Single)
extern "C"  void SpriteDisplay_Scale_m4190160741 (SpriteDisplay_t1886630049 * __this, float ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
