﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>
struct ShimEnumerator_t1471165405;
// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1875939108_gshared (ShimEnumerator_t1471165405 * __this, Dictionary_2_t1366040584 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1875939108(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1471165405 *, Dictionary_2_t1366040584 *, const MethodInfo*))ShimEnumerator__ctor_m1875939108_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3099403541_gshared (ShimEnumerator_t1471165405 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3099403541(__this, method) ((  bool (*) (ShimEnumerator_t1471165405 *, const MethodInfo*))ShimEnumerator_MoveNext_m3099403541_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4294565425_gshared (ShimEnumerator_t1471165405 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m4294565425(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1471165405 *, const MethodInfo*))ShimEnumerator_get_Entry_m4294565425_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m227999964_gshared (ShimEnumerator_t1471165405 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m227999964(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1471165405 *, const MethodInfo*))ShimEnumerator_get_Key_m227999964_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m281936918_gshared (ShimEnumerator_t1471165405 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m281936918(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1471165405 *, const MethodInfo*))ShimEnumerator_get_Value_m281936918_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m604558310_gshared (ShimEnumerator_t1471165405 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m604558310(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1471165405 *, const MethodInfo*))ShimEnumerator_get_Current_m604558310_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Tropism/CoReceptorType,System.Single>::Reset()
extern "C"  void ShimEnumerator_Reset_m1693536858_gshared (ShimEnumerator_t1471165405 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1693536858(__this, method) ((  void (*) (ShimEnumerator_t1471165405 *, const MethodInfo*))ShimEnumerator_Reset_m1693536858_gshared)(__this, method)
