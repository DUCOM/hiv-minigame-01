﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GP120
struct GP120_t1512820336;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"

// System.Void GP120::.ctor()
extern "C"  void GP120__ctor_m4265588283 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism GP120::get_top()
extern "C"  Tropism_t3662836552  GP120_get_top_m3080711702 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GP120::set_top(Tropism)
extern "C"  void GP120_set_top_m2815271707 (GP120_t1512820336 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Statistics GP120::get_stats()
extern "C"  Statistics_t2537377007  GP120_get_stats_m664361269 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpriteRenderer GP120::get_displayLeft()
extern "C"  SpriteRenderer_t1209076198 * GP120_get_displayLeft_m2311060087 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SpriteRenderer GP120::get_displayRight()
extern "C"  SpriteRenderer_t1209076198 * GP120_get_displayRight_m461027194 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem GP120::get_particles()
extern "C"  ParticleSystem_t3394631041 * GP120_get_particles_m948954212 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GP120::GetColorLeft()
extern "C"  Color_t2020392075  GP120_GetColorLeft_m1399135880 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GP120::GetRightColor()
extern "C"  Color_t2020392075  GP120_GetRightColor_m437877421 (GP120_t1512820336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GP120::LeftDefaultColor(Tropism/CoReceptorType)
extern "C"  Color_t2020392075  GP120_LeftDefaultColor_m2491958122 (GP120_t1512820336 * __this, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GP120::RightDefaultColor(Tropism/CoReceptorType)
extern "C"  Color_t2020392075  GP120_RightDefaultColor_m1880879577 (GP120_t1512820336 * __this, int32_t ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
