﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pivot
struct Pivot_t2110476880;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BCellMaster
struct  BCellMaster_t153800904  : public MonoBehaviour_t1158329972
{
public:
	// Pivot BCellMaster::pivot
	Pivot_t2110476880 * ___pivot_2;
	// System.Single BCellMaster::radius
	float ___radius_3;

public:
	inline static int32_t get_offset_of_pivot_2() { return static_cast<int32_t>(offsetof(BCellMaster_t153800904, ___pivot_2)); }
	inline Pivot_t2110476880 * get_pivot_2() const { return ___pivot_2; }
	inline Pivot_t2110476880 ** get_address_of_pivot_2() { return &___pivot_2; }
	inline void set_pivot_2(Pivot_t2110476880 * value)
	{
		___pivot_2 = value;
		Il2CppCodeGenWriteBarrier(&___pivot_2, value);
	}

	inline static int32_t get_offset_of_radius_3() { return static_cast<int32_t>(offsetof(BCellMaster_t153800904, ___radius_3)); }
	inline float get_radius_3() const { return ___radius_3; }
	inline float* get_address_of_radius_3() { return &___radius_3; }
	inline void set_radius_3(float value)
	{
		___radius_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
