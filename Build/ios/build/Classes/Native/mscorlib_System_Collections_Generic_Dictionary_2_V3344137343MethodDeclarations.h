﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>
struct ValueCollection_t3344137343;
// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3847001055;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2032642968.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m290583614_gshared (ValueCollection_t3344137343 * __this, Dictionary_2_t346110204 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m290583614(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3344137343 *, Dictionary_2_t346110204 *, const MethodInfo*))ValueCollection__ctor_m290583614_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2660137800_gshared (ValueCollection_t3344137343 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2660137800(__this, ___item0, method) ((  void (*) (ValueCollection_t3344137343 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2660137800_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2881740691_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2881740691(__this, method) ((  void (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2881740691_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3820666240_gshared (ValueCollection_t3344137343 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3820666240(__this, ___item0, method) ((  bool (*) (ValueCollection_t3344137343 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3820666240_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4235768291_gshared (ValueCollection_t3344137343 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4235768291(__this, ___item0, method) ((  bool (*) (ValueCollection_t3344137343 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4235768291_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m631511661_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m631511661(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m631511661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3450819053_gshared (ValueCollection_t3344137343 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3450819053(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3344137343 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3450819053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m835086620_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m835086620(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m835086620_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1756096903_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1756096903(__this, method) ((  bool (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1756096903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3414674729_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3414674729(__this, method) ((  bool (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3414674729_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3073564321_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3073564321(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3073564321_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3590029427_gshared (ValueCollection_t3344137343 * __this, SingleU5BU5D_t577127397* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3590029427(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3344137343 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3590029427_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::GetEnumerator()
extern "C"  Enumerator_t2032642968  ValueCollection_GetEnumerator_m1056184222_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1056184222(__this, method) ((  Enumerator_t2032642968  (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_GetEnumerator_m1056184222_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3322603373_gshared (ValueCollection_t3344137343 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3322603373(__this, method) ((  int32_t (*) (ValueCollection_t3344137343 *, const MethodInfo*))ValueCollection_get_Count_m3322603373_gshared)(__this, method)
