﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_TryAgain
struct  UI_TryAgain_t3222824968  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text UI_TryAgain::text
	Text_t356221433 * ___text_2;
	// System.String UI_TryAgain::pauseText
	String_t* ___pauseText_3;
	// System.String UI_TryAgain::endGameText
	String_t* ___endGameText_4;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(UI_TryAgain_t3222824968, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_pauseText_3() { return static_cast<int32_t>(offsetof(UI_TryAgain_t3222824968, ___pauseText_3)); }
	inline String_t* get_pauseText_3() const { return ___pauseText_3; }
	inline String_t** get_address_of_pauseText_3() { return &___pauseText_3; }
	inline void set_pauseText_3(String_t* value)
	{
		___pauseText_3 = value;
		Il2CppCodeGenWriteBarrier(&___pauseText_3, value);
	}

	inline static int32_t get_offset_of_endGameText_4() { return static_cast<int32_t>(offsetof(UI_TryAgain_t3222824968, ___endGameText_4)); }
	inline String_t* get_endGameText_4() const { return ___endGameText_4; }
	inline String_t** get_address_of_endGameText_4() { return &___endGameText_4; }
	inline void set_endGameText_4(String_t* value)
	{
		___endGameText_4 = value;
		Il2CppCodeGenWriteBarrier(&___endGameText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
