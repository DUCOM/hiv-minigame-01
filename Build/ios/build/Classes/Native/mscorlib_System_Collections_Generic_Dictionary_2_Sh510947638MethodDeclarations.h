﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>
struct ShimEnumerator_t510947638;
// System.Collections.Generic.Dictionary`2<HIV/State,System.Object>
struct Dictionary_2_t405822817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m863632937_gshared (ShimEnumerator_t510947638 * __this, Dictionary_2_t405822817 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m863632937(__this, ___host0, method) ((  void (*) (ShimEnumerator_t510947638 *, Dictionary_2_t405822817 *, const MethodInfo*))ShimEnumerator__ctor_m863632937_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3811834258_gshared (ShimEnumerator_t510947638 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3811834258(__this, method) ((  bool (*) (ShimEnumerator_t510947638 *, const MethodInfo*))ShimEnumerator_MoveNext_m3811834258_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4250603574_gshared (ShimEnumerator_t510947638 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m4250603574(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t510947638 *, const MethodInfo*))ShimEnumerator_get_Entry_m4250603574_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3921124213_gshared (ShimEnumerator_t510947638 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3921124213(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t510947638 *, const MethodInfo*))ShimEnumerator_get_Key_m3921124213_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3109648989_gshared (ShimEnumerator_t510947638 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3109648989(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t510947638 *, const MethodInfo*))ShimEnumerator_get_Value_m3109648989_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2825441203_gshared (ShimEnumerator_t510947638 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2825441203(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t510947638 *, const MethodInfo*))ShimEnumerator_get_Current_m2825441203_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<HIV/State,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1633446539_gshared (ShimEnumerator_t510947638 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1633446539(__this, method) ((  void (*) (ShimEnumerator_t510947638 *, const MethodInfo*))ShimEnumerator_Reset_m1633446539_gshared)(__this, method)
