﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3770236190MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4262235146(__this, ___dictionary0, method) ((  void (*) (Enumerator_t781945790 *, Dictionary_2_t3756888384 *, const MethodInfo*))Enumerator__ctor_m1331499111_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m87020229(__this, method) ((  Il2CppObject * (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m495744888_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1877107313(__this, method) ((  void (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2050862182_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m578655486(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3206339073_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m689735603(__this, method) ((  Il2CppObject * (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1364806446_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1352762667(__this, method) ((  Il2CppObject * (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1184920668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::MoveNext()
#define Enumerator_MoveNext_m2669982112(__this, method) ((  bool (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_MoveNext_m385052558_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::get_Current()
#define Enumerator_get_Current_m2376774599(__this, method) ((  KeyValuePair_2_t1514233606  (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_get_Current_m1261098558_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1934425154(__this, method) ((  int32_t (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_get_CurrentKey_m2766001139_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1504059882(__this, method) ((  AntibodyState_t3996126191 * (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_get_CurrentValue_m2122533179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::Reset()
#define Enumerator_Reset_m3632734388(__this, method) ((  void (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_Reset_m2491434409_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::VerifyState()
#define Enumerator_VerifyState_m2178546367(__this, method) ((  void (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_VerifyState_m2223444862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3817604225(__this, method) ((  void (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_VerifyCurrent_m3842411350_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Antibody/State,AntibodyState>::Dispose()
#define Enumerator_Dispose_m106152050(__this, method) ((  void (*) (Enumerator_t781945790 *, const MethodInfo*))Enumerator_Dispose_m1570825211_gshared)(__this, method)
