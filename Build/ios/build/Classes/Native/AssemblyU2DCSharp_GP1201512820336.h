﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GP120
struct  GP120_t1512820336  : public MonoBehaviour_t1158329972
{
public:
	// Tropism GP120::_top
	Tropism_t3662836552  ____top_2;
	// UnityEngine.SpriteRenderer GP120::_displayLeft
	SpriteRenderer_t1209076198 * ____displayLeft_3;
	// UnityEngine.SpriteRenderer GP120::_displayRight
	SpriteRenderer_t1209076198 * ____displayRight_4;
	// UnityEngine.ParticleSystem GP120::_particles
	ParticleSystem_t3394631041 * ____particles_5;

public:
	inline static int32_t get_offset_of__top_2() { return static_cast<int32_t>(offsetof(GP120_t1512820336, ____top_2)); }
	inline Tropism_t3662836552  get__top_2() const { return ____top_2; }
	inline Tropism_t3662836552 * get_address_of__top_2() { return &____top_2; }
	inline void set__top_2(Tropism_t3662836552  value)
	{
		____top_2 = value;
	}

	inline static int32_t get_offset_of__displayLeft_3() { return static_cast<int32_t>(offsetof(GP120_t1512820336, ____displayLeft_3)); }
	inline SpriteRenderer_t1209076198 * get__displayLeft_3() const { return ____displayLeft_3; }
	inline SpriteRenderer_t1209076198 ** get_address_of__displayLeft_3() { return &____displayLeft_3; }
	inline void set__displayLeft_3(SpriteRenderer_t1209076198 * value)
	{
		____displayLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&____displayLeft_3, value);
	}

	inline static int32_t get_offset_of__displayRight_4() { return static_cast<int32_t>(offsetof(GP120_t1512820336, ____displayRight_4)); }
	inline SpriteRenderer_t1209076198 * get__displayRight_4() const { return ____displayRight_4; }
	inline SpriteRenderer_t1209076198 ** get_address_of__displayRight_4() { return &____displayRight_4; }
	inline void set__displayRight_4(SpriteRenderer_t1209076198 * value)
	{
		____displayRight_4 = value;
		Il2CppCodeGenWriteBarrier(&____displayRight_4, value);
	}

	inline static int32_t get_offset_of__particles_5() { return static_cast<int32_t>(offsetof(GP120_t1512820336, ____particles_5)); }
	inline ParticleSystem_t3394631041 * get__particles_5() const { return ____particles_5; }
	inline ParticleSystem_t3394631041 ** get_address_of__particles_5() { return &____particles_5; }
	inline void set__particles_5(ParticleSystem_t3394631041 * value)
	{
		____particles_5 = value;
		Il2CppCodeGenWriteBarrier(&____particles_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
