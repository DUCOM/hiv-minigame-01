﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<HIV/State,System.Object>
struct Dictionary_2_t405822817;
// System.Collections.Generic.IEqualityComparer`1<HIV/State>
struct IEqualityComparer_1_t3437402959;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>[]
struct KeyValuePair_2U5BU5D_t1651873886;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>
struct IEnumerator_1_t4228626458;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<HIV/State,System.Object>
struct ValueCollection_t3403849956;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22458135335.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1725847519.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2961091826_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2961091826(__this, method) ((  void (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2__ctor_m2961091826_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3726717501_gshared (Dictionary_2_t405822817 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3726717501(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3726717501_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3844875547_gshared (Dictionary_2_t405822817 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3844875547(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t405822817 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3844875547_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m640019561_gshared (Dictionary_2_t405822817 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m640019561(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t405822817 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m640019561_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3801201170_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3801201170(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t405822817 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3801201170_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2045725965_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2045725965(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2045725965_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1444185792_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1444185792(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1444185792_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m409892961_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m409892961(__this, ___key0, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m409892961_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3773122850_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3773122850(__this, method) ((  bool (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3773122850_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3100453334_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3100453334(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3100453334_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1811173684_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1811173684(__this, method) ((  bool (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1811173684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3721927401_gshared (Dictionary_2_t405822817 * __this, KeyValuePair_2_t2458135335  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3721927401(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t405822817 *, KeyValuePair_2_t2458135335 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3721927401_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2050566223_gshared (Dictionary_2_t405822817 * __this, KeyValuePair_2_t2458135335  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2050566223(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t405822817 *, KeyValuePair_2_t2458135335 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2050566223_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1453337781_gshared (Dictionary_2_t405822817 * __this, KeyValuePair_2U5BU5D_t1651873886* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1453337781(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t405822817 *, KeyValuePair_2U5BU5D_t1651873886*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1453337781_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2599039830_gshared (Dictionary_2_t405822817 * __this, KeyValuePair_2_t2458135335  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2599039830(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t405822817 *, KeyValuePair_2_t2458135335 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2599039830_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m846334214_gshared (Dictionary_2_t405822817 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m846334214(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m846334214_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3465605623_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3465605623(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3465605623_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1542219228_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1542219228(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1542219228_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2893072825_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2893072825(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2893072825_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3654330698_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3654330698(__this, method) ((  int32_t (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_get_Count_m3654330698_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m500708121_gshared (Dictionary_2_t405822817 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m500708121(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t405822817 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m500708121_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2999905924_gshared (Dictionary_2_t405822817 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2999905924(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t405822817 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m2999905924_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4053193314_gshared (Dictionary_2_t405822817 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4053193314(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t405822817 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4053193314_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m968613919_gshared (Dictionary_2_t405822817 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m968613919(__this, ___size0, method) ((  void (*) (Dictionary_2_t405822817 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m968613919_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2733797081_gshared (Dictionary_2_t405822817 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2733797081(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2733797081_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2458135335  Dictionary_2_make_pair_m2148110075_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2148110075(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2458135335  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2148110075_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m4224543931_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m4224543931(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m4224543931_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3973879816_gshared (Dictionary_2_t405822817 * __this, KeyValuePair_2U5BU5D_t1651873886* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3973879816(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t405822817 *, KeyValuePair_2U5BU5D_t1651873886*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3973879816_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m275124720_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m275124720(__this, method) ((  void (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_Resize_m275124720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1384164703_gshared (Dictionary_2_t405822817 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1384164703(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t405822817 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1384164703_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m166717103_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m166717103(__this, method) ((  void (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_Clear_m166717103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m257935641_gshared (Dictionary_2_t405822817 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m257935641(__this, ___key0, method) ((  bool (*) (Dictionary_2_t405822817 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m257935641_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3041755665_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3041755665(__this, ___value0, method) ((  bool (*) (Dictionary_2_t405822817 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3041755665_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3754789178_gshared (Dictionary_2_t405822817 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3754789178(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t405822817 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3754789178_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3127841004_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3127841004(__this, ___sender0, method) ((  void (*) (Dictionary_2_t405822817 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3127841004_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m629846015_gshared (Dictionary_2_t405822817 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m629846015(__this, ___key0, method) ((  bool (*) (Dictionary_2_t405822817 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m629846015_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m447720784_gshared (Dictionary_2_t405822817 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m447720784(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t405822817 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m447720784_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::get_Values()
extern "C"  ValueCollection_t3403849956 * Dictionary_2_get_Values_m3315233373_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3315233373(__this, method) ((  ValueCollection_t3403849956 * (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_get_Values_m3315233373_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m4117604350_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4117604350(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t405822817 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4117604350_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1965291758_gshared (Dictionary_2_t405822817 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1965291758(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t405822817 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1965291758_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m541629648_gshared (Dictionary_2_t405822817 * __this, KeyValuePair_2_t2458135335  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m541629648(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t405822817 *, KeyValuePair_2_t2458135335 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m541629648_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1725847519  Dictionary_2_GetEnumerator_m3814640911_gshared (Dictionary_2_t405822817 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m3814640911(__this, method) ((  Enumerator_t1725847519  (*) (Dictionary_2_t405822817 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3814640911_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<HIV/State,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3130214742_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3130214742(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3130214742_gshared)(__this /* static, unused */, ___key0, ___value1, method)
