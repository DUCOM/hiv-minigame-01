﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_Sound_SoundType2077149641.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sound
struct  Sound_t826716933  : public Il2CppObject
{
public:
	// UnityEngine.AudioClip Sound::sound
	AudioClip_t1932558630 * ___sound_0;
	// System.String Sound::name
	String_t* ___name_1;
	// Sound/SoundType Sound::type
	int32_t ___type_2;
	// System.Boolean Sound::loop
	bool ___loop_3;

public:
	inline static int32_t get_offset_of_sound_0() { return static_cast<int32_t>(offsetof(Sound_t826716933, ___sound_0)); }
	inline AudioClip_t1932558630 * get_sound_0() const { return ___sound_0; }
	inline AudioClip_t1932558630 ** get_address_of_sound_0() { return &___sound_0; }
	inline void set_sound_0(AudioClip_t1932558630 * value)
	{
		___sound_0 = value;
		Il2CppCodeGenWriteBarrier(&___sound_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Sound_t826716933, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Sound_t826716933, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(Sound_t826716933, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
