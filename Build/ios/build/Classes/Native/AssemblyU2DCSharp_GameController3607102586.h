﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HIV
struct HIV_t2481767745;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// HIV GameController::_selected
	HIV_t2481767745 * ____selected_2;

public:
	inline static int32_t get_offset_of__selected_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ____selected_2)); }
	inline HIV_t2481767745 * get__selected_2() const { return ____selected_2; }
	inline HIV_t2481767745 ** get_address_of__selected_2() { return &____selected_2; }
	inline void set__selected_2(HIV_t2481767745 * value)
	{
		____selected_2 = value;
		Il2CppCodeGenWriteBarrier(&____selected_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
