﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22458135335MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<HIV/State,HIV_State>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2041309057(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4121253901 *, int32_t, HIV_State_t57600565 *, const MethodInfo*))KeyValuePair_2__ctor_m1362833532_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<HIV/State,HIV_State>::get_Key()
#define KeyValuePair_2_get_Key_m628757003(__this, method) ((  int32_t (*) (KeyValuePair_2_t4121253901 *, const MethodInfo*))KeyValuePair_2_get_Key_m3344155018_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<HIV/State,HIV_State>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3103564996(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4121253901 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1343766057_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<HIV/State,HIV_State>::get_Value()
#define KeyValuePair_2_get_Value_m1401288323(__this, method) ((  HIV_State_t57600565 * (*) (KeyValuePair_2_t4121253901 *, const MethodInfo*))KeyValuePair_2_get_Value_m3737669834_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<HIV/State,HIV_State>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1836711996(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4121253901 *, HIV_State_t57600565 *, const MethodInfo*))KeyValuePair_2_set_Value_m97232737_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<HIV/State,HIV_State>::ToString()
#define KeyValuePair_2_ToString_m3930631324(__this, method) ((  String_t* (*) (KeyValuePair_2_t4121253901 *, const MethodInfo*))KeyValuePair_2_ToString_m4126039479_gshared)(__this, method)
