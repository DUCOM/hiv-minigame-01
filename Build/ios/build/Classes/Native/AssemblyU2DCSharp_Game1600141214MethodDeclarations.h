﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Game
struct Game_t1600141214;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<HIV>
struct List_1_t1850888877;
// System.Collections.Generic.List`1<TCell>
struct List_1_t1184231168;
// GameController
struct GameController_t3607102586;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Timer
struct Timer_t2917042437;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"

// System.Void Game::.ctor()
extern "C"  void Game__ctor_m1512360653 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::get_hivList()
extern "C"  List_1_t1125654279 * Game_get_hivList_m514738532 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HIV> Game::get_hivScripts()
extern "C"  List_1_t1850888877 * Game_get_hivScripts_m2909757549 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Game::get_population()
extern "C"  int32_t Game_get_population_m752974705 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Game::get_extinct()
extern "C"  bool Game_get_extinct_m2737541949 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Game::get__tCellSpawn()
extern "C"  Rect_t3681755626  Game_get__tCellSpawn_m1045155104 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Game::get__tCellDisable()
extern "C"  Rect_t3681755626  Game_get__tCellDisable_m3709581821 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::get_tCellList()
extern "C"  List_1_t1125654279 * Game_get_tCellList_m3565045317 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<TCell> Game::get_tCellScripts()
extern "C"  List_1_t1184231168 * Game_get_tCellScripts_m1223424833 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> Game::get_redBloodCellList()
extern "C"  List_1_t1125654279 * Game_get_redBloodCellList_m3132366318 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameController Game::get_controller()
extern "C"  GameController_t3607102586 * Game_get_controller_m2668402245 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::CreateHIVList(System.Boolean)
extern "C"  void Game_CreateHIVList_m609994815 (Game_t1600141214 * __this, bool ___instantiate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Game::CreateHIV(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * Game_CreateHIV_m2019914367 (Game_t1600141214 * __this, Vector3_t2243707580  ___position0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::CreateTCellList(System.Boolean)
extern "C"  void Game_CreateTCellList_m687649580 (Game_t1600141214 * __this, bool ___instantiate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Game::CreateTCell()
extern "C"  GameObject_t1756533147 * Game_CreateTCell_m1429495806 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Game::CreateTCell(UnityEngine.Quaternion,System.Single,UnityEngine.Vector3)
extern "C"  GameObject_t1756533147 * Game_CreateTCell_m1287670579 (Game_t1600141214 * __this, Quaternion_t4030073918  ___rotation0, float ___speed1, Vector3_t2243707580  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::CreateRBCList(System.Boolean)
extern "C"  void Game_CreateRBCList_m3543786311 (Game_t1600141214 * __this, bool ___instantiate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Game::CreateRBC()
extern "C"  GameObject_t1756533147 * Game_CreateRBC_m2781373493 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Game::CreateRBC(UnityEngine.Quaternion,System.Single,UnityEngine.Vector3)
extern "C"  GameObject_t1756533147 * Game_CreateRBC_m679804250 (Game_t1600141214 * __this, Quaternion_t4030073918  ___rotation0, float ___speed1, Vector3_t2243707580  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::LevelUp()
extern "C"  void Game_LevelUp_m1101585534 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::Battle()
extern "C"  void Game_Battle_m443051511 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Game::LevelGain(System.Int32)
extern "C"  int32_t Game_LevelGain_m1081789369 (Il2CppObject * __this /* static, unused */, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Game::ShooterDurationPerLevel(System.Int32)
extern "C"  float Game_ShooterDurationPerLevel_m230173777 (Game_t1600141214 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Game::WaveDurationPerLevel(System.Int32)
extern "C"  float Game_WaveDurationPerLevel_m1181764832 (Game_t1600141214 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::EndGame()
extern "C"  void Game_EndGame_m3658353106 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::Awake()
extern "C"  void Game_Awake_m2145690122 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::Start()
extern "C"  void Game_Start_m3679318685 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::Update()
extern "C"  void Game_Update_m3455215516 (Game_t1600141214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game::OnTimerEnd(Timer)
extern "C"  void Game_OnTimerEnd_m4079708735 (Game_t1600141214 * __this, Timer_t2917042437 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
