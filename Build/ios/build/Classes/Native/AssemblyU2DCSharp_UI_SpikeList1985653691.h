﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UI_Spike[]
struct UI_SpikeU5BU5D_t2474440624;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_SpikeList
struct  UI_SpikeList_t1985653691  : public MonoBehaviour_t1158329972
{
public:
	// SpikeColor UI_SpikeList::_colors
	SpikeColor_t3573234163  ____colors_2;
	// UI_Spike[] UI_SpikeList::spikeList
	UI_SpikeU5BU5D_t2474440624* ___spikeList_3;

public:
	inline static int32_t get_offset_of__colors_2() { return static_cast<int32_t>(offsetof(UI_SpikeList_t1985653691, ____colors_2)); }
	inline SpikeColor_t3573234163  get__colors_2() const { return ____colors_2; }
	inline SpikeColor_t3573234163 * get_address_of__colors_2() { return &____colors_2; }
	inline void set__colors_2(SpikeColor_t3573234163  value)
	{
		____colors_2 = value;
	}

	inline static int32_t get_offset_of_spikeList_3() { return static_cast<int32_t>(offsetof(UI_SpikeList_t1985653691, ___spikeList_3)); }
	inline UI_SpikeU5BU5D_t2474440624* get_spikeList_3() const { return ___spikeList_3; }
	inline UI_SpikeU5BU5D_t2474440624** get_address_of_spikeList_3() { return &___spikeList_3; }
	inline void set_spikeList_3(UI_SpikeU5BU5D_t2474440624* value)
	{
		___spikeList_3 = value;
		Il2CppCodeGenWriteBarrier(&___spikeList_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
