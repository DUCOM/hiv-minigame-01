﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Move
struct HIV_Move_t1427226471;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Move::.ctor()
extern "C"  void HIV_Move__ctor_m785377476 (HIV_Move_t1427226471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HIV_Move::get_maxSpeed()
extern "C"  float HIV_Move_get_maxSpeed_m59335644 (HIV_Move_t1427226471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Move::Awake()
extern "C"  void HIV_Move_Awake_m2675884359 (HIV_Move_t1427226471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Move::FixedUpdate()
extern "C"  void HIV_Move_FixedUpdate_m3816133837 (HIV_Move_t1427226471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
