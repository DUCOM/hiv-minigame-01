﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteDisplay
struct  SpriteDisplay_t1886630049  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.SpriteRenderer SpriteDisplay::spriteRender
	SpriteRenderer_t1209076198 * ___spriteRender_2;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> SpriteDisplay::spriteList
	List_1_t3973682211 * ___spriteList_3;
	// System.Int32 SpriteDisplay::index
	int32_t ___index_4;
	// System.Boolean SpriteDisplay::up
	bool ___up_5;
	// System.Boolean SpriteDisplay::down
	bool ___down_6;

public:
	inline static int32_t get_offset_of_spriteRender_2() { return static_cast<int32_t>(offsetof(SpriteDisplay_t1886630049, ___spriteRender_2)); }
	inline SpriteRenderer_t1209076198 * get_spriteRender_2() const { return ___spriteRender_2; }
	inline SpriteRenderer_t1209076198 ** get_address_of_spriteRender_2() { return &___spriteRender_2; }
	inline void set_spriteRender_2(SpriteRenderer_t1209076198 * value)
	{
		___spriteRender_2 = value;
		Il2CppCodeGenWriteBarrier(&___spriteRender_2, value);
	}

	inline static int32_t get_offset_of_spriteList_3() { return static_cast<int32_t>(offsetof(SpriteDisplay_t1886630049, ___spriteList_3)); }
	inline List_1_t3973682211 * get_spriteList_3() const { return ___spriteList_3; }
	inline List_1_t3973682211 ** get_address_of_spriteList_3() { return &___spriteList_3; }
	inline void set_spriteList_3(List_1_t3973682211 * value)
	{
		___spriteList_3 = value;
		Il2CppCodeGenWriteBarrier(&___spriteList_3, value);
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(SpriteDisplay_t1886630049, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_up_5() { return static_cast<int32_t>(offsetof(SpriteDisplay_t1886630049, ___up_5)); }
	inline bool get_up_5() const { return ___up_5; }
	inline bool* get_address_of_up_5() { return &___up_5; }
	inline void set_up_5(bool value)
	{
		___up_5 = value;
	}

	inline static int32_t get_offset_of_down_6() { return static_cast<int32_t>(offsetof(SpriteDisplay_t1886630049, ___down_6)); }
	inline bool get_down_6() const { return ___down_6; }
	inline bool* get_address_of_down_6() { return &___down_6; }
	inline void set_down_6(bool value)
	{
		___down_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
