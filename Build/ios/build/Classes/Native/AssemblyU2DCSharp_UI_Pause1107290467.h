﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// Controller
struct Controller_t1937198888;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_UI_Pause_States3339147058.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Pause
struct  UI_Pause_t1107290467  : public MonoBehaviour_t1158329972
{
public:
	// UI_Pause/States UI_Pause::state
	int32_t ___state_2;
	// UnityEngine.Sprite UI_Pause::pauseSymbol
	Sprite_t309593783 * ___pauseSymbol_3;
	// UnityEngine.Sprite UI_Pause::playSymbol
	Sprite_t309593783 * ___playSymbol_4;
	// UnityEngine.UI.Image UI_Pause::picture
	Image_t2042527209 * ___picture_5;
	// UnityEngine.GameObject UI_Pause::pauseMenu
	GameObject_t1756533147 * ___pauseMenu_6;
	// UnityEngine.AudioSource UI_Pause::_backgroundMusic
	AudioSource_t1135106623 * ____backgroundMusic_7;
	// Controller UI_Pause::<controller>k__BackingField
	Controller_t1937198888 * ___U3CcontrollerU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_pauseSymbol_3() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ___pauseSymbol_3)); }
	inline Sprite_t309593783 * get_pauseSymbol_3() const { return ___pauseSymbol_3; }
	inline Sprite_t309593783 ** get_address_of_pauseSymbol_3() { return &___pauseSymbol_3; }
	inline void set_pauseSymbol_3(Sprite_t309593783 * value)
	{
		___pauseSymbol_3 = value;
		Il2CppCodeGenWriteBarrier(&___pauseSymbol_3, value);
	}

	inline static int32_t get_offset_of_playSymbol_4() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ___playSymbol_4)); }
	inline Sprite_t309593783 * get_playSymbol_4() const { return ___playSymbol_4; }
	inline Sprite_t309593783 ** get_address_of_playSymbol_4() { return &___playSymbol_4; }
	inline void set_playSymbol_4(Sprite_t309593783 * value)
	{
		___playSymbol_4 = value;
		Il2CppCodeGenWriteBarrier(&___playSymbol_4, value);
	}

	inline static int32_t get_offset_of_picture_5() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ___picture_5)); }
	inline Image_t2042527209 * get_picture_5() const { return ___picture_5; }
	inline Image_t2042527209 ** get_address_of_picture_5() { return &___picture_5; }
	inline void set_picture_5(Image_t2042527209 * value)
	{
		___picture_5 = value;
		Il2CppCodeGenWriteBarrier(&___picture_5, value);
	}

	inline static int32_t get_offset_of_pauseMenu_6() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ___pauseMenu_6)); }
	inline GameObject_t1756533147 * get_pauseMenu_6() const { return ___pauseMenu_6; }
	inline GameObject_t1756533147 ** get_address_of_pauseMenu_6() { return &___pauseMenu_6; }
	inline void set_pauseMenu_6(GameObject_t1756533147 * value)
	{
		___pauseMenu_6 = value;
		Il2CppCodeGenWriteBarrier(&___pauseMenu_6, value);
	}

	inline static int32_t get_offset_of__backgroundMusic_7() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ____backgroundMusic_7)); }
	inline AudioSource_t1135106623 * get__backgroundMusic_7() const { return ____backgroundMusic_7; }
	inline AudioSource_t1135106623 ** get_address_of__backgroundMusic_7() { return &____backgroundMusic_7; }
	inline void set__backgroundMusic_7(AudioSource_t1135106623 * value)
	{
		____backgroundMusic_7 = value;
		Il2CppCodeGenWriteBarrier(&____backgroundMusic_7, value);
	}

	inline static int32_t get_offset_of_U3CcontrollerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UI_Pause_t1107290467, ___U3CcontrollerU3Ek__BackingField_8)); }
	inline Controller_t1937198888 * get_U3CcontrollerU3Ek__BackingField_8() const { return ___U3CcontrollerU3Ek__BackingField_8; }
	inline Controller_t1937198888 ** get_address_of_U3CcontrollerU3Ek__BackingField_8() { return &___U3CcontrollerU3Ek__BackingField_8; }
	inline void set_U3CcontrollerU3Ek__BackingField_8(Controller_t1937198888 * value)
	{
		___U3CcontrollerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontrollerU3Ek__BackingField_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
