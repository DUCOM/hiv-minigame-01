﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<Antibody/State,AntibodyState>
struct Dictionary_2_t3756888384;
// AntibodyMove
struct AntibodyMove_t4173900141;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Antibody
struct  Antibody_t2931822436  : public MonoBehaviour_t1158329972
{
public:
	// Antibody/State Antibody::<state>k__BackingField
	int32_t ___U3CstateU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<Antibody/State,AntibodyState> Antibody::stateList
	Dictionary_2_t3756888384 * ___stateList_3;
	// System.Single Antibody::speed
	float ___speed_4;
	// AntibodyMove Antibody::_move
	AntibodyMove_t4173900141 * ____move_5;
	// UnityEngine.SpriteRenderer Antibody::_display
	SpriteRenderer_t1209076198 * ____display_6;

public:
	inline static int32_t get_offset_of_U3CstateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Antibody_t2931822436, ___U3CstateU3Ek__BackingField_2)); }
	inline int32_t get_U3CstateU3Ek__BackingField_2() const { return ___U3CstateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CstateU3Ek__BackingField_2() { return &___U3CstateU3Ek__BackingField_2; }
	inline void set_U3CstateU3Ek__BackingField_2(int32_t value)
	{
		___U3CstateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_stateList_3() { return static_cast<int32_t>(offsetof(Antibody_t2931822436, ___stateList_3)); }
	inline Dictionary_2_t3756888384 * get_stateList_3() const { return ___stateList_3; }
	inline Dictionary_2_t3756888384 ** get_address_of_stateList_3() { return &___stateList_3; }
	inline void set_stateList_3(Dictionary_2_t3756888384 * value)
	{
		___stateList_3 = value;
		Il2CppCodeGenWriteBarrier(&___stateList_3, value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Antibody_t2931822436, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of__move_5() { return static_cast<int32_t>(offsetof(Antibody_t2931822436, ____move_5)); }
	inline AntibodyMove_t4173900141 * get__move_5() const { return ____move_5; }
	inline AntibodyMove_t4173900141 ** get_address_of__move_5() { return &____move_5; }
	inline void set__move_5(AntibodyMove_t4173900141 * value)
	{
		____move_5 = value;
		Il2CppCodeGenWriteBarrier(&____move_5, value);
	}

	inline static int32_t get_offset_of__display_6() { return static_cast<int32_t>(offsetof(Antibody_t2931822436, ____display_6)); }
	inline SpriteRenderer_t1209076198 * get__display_6() const { return ____display_6; }
	inline SpriteRenderer_t1209076198 ** get_address_of__display_6() { return &____display_6; }
	inline void set__display_6(SpriteRenderer_t1209076198 * value)
	{
		____display_6 = value;
		Il2CppCodeGenWriteBarrier(&____display_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
