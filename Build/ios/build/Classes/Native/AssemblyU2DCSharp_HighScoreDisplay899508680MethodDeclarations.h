﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighScoreDisplay
struct HighScoreDisplay_t899508680;

#include "codegen/il2cpp-codegen.h"

// System.Void HighScoreDisplay::.ctor()
extern "C"  void HighScoreDisplay__ctor_m504516991 (HighScoreDisplay_t899508680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScoreDisplay::Awake()
extern "C"  void HighScoreDisplay_Awake_m4172241172 (HighScoreDisplay_t899508680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScoreDisplay::OnGUI()
extern "C"  void HighScoreDisplay_OnGUI_m3445912053 (HighScoreDisplay_t899508680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
