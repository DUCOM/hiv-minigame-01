﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Level
struct UI_Level_t2621343419;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Level::.ctor()
extern "C"  void UI_Level__ctor_m2389024474 (UI_Level_t2621343419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Level::Awake()
extern "C"  void UI_Level_Awake_m4278608247 (UI_Level_t2621343419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Level::OnGUI()
extern "C"  void UI_Level_OnGUI_m3336008194 (UI_Level_t2621343419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
