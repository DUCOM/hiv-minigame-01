﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpikeInteract
struct SpikeInteract_t2340666440;
// Spike
struct Spike_t60554746;
// HIV
struct HIV_t2481767745;
// TCell_Receptor
struct TCell_Receptor_t2424206879;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879.h"

// System.Void SpikeInteract::.ctor()
extern "C"  void SpikeInteract__ctor_m2361209129 (SpikeInteract_t2340666440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Tropism SpikeInteract::get_tropism()
extern "C"  Tropism_t3662836552  SpikeInteract_get_tropism_m1350703263 (SpikeInteract_t2340666440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Spike SpikeInteract::get_spike()
extern "C"  Spike_t60554746 * SpikeInteract_get_spike_m1941632887 (SpikeInteract_t2340666440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV SpikeInteract::get_hiv()
extern "C"  HIV_t2481767745 * SpikeInteract_get_hiv_m2599775787 (SpikeInteract_t2340666440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SpikeInteract::validCombination(TCell_Receptor)
extern "C"  bool SpikeInteract_validCombination_m4061672919 (SpikeInteract_t2340666440 * __this, TCell_Receptor_t2424206879 * ___check0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
