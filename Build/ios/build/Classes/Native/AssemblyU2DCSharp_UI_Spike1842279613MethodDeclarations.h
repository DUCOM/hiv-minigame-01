﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Spike
struct UI_Spike_t1842279613;
// UI_GP120
struct UI_GP120_t1483396489;
// UI_GP41
struct UI_GP41_t2232403251;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Spike::.ctor()
extern "C"  void UI_Spike__ctor_m1742877090 (UI_Spike_t1842279613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UI_GP120 UI_Spike::get_gp120()
extern "C"  UI_GP120_t1483396489 * UI_Spike_get_gp120_m384494825 (UI_Spike_t1842279613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UI_GP41 UI_Spike::get_gp41()
extern "C"  UI_GP41_t2232403251 * UI_Spike_get_gp41_m470670369 (UI_Spike_t1842279613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
