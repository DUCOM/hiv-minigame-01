﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3316887597.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22458135335.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2415610531_gshared (InternalEnumerator_1_t3316887597 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2415610531(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3316887597 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2415610531_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m765801851_gshared (InternalEnumerator_1_t3316887597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m765801851(__this, method) ((  void (*) (InternalEnumerator_1_t3316887597 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m765801851_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2151972367_gshared (InternalEnumerator_1_t3316887597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2151972367(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3316887597 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2151972367_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2323651034_gshared (InternalEnumerator_1_t3316887597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2323651034(__this, method) ((  void (*) (InternalEnumerator_1_t3316887597 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2323651034_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2019767663_gshared (InternalEnumerator_1_t3316887597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2019767663(__this, method) ((  bool (*) (InternalEnumerator_1_t3316887597 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2019767663_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<HIV/State,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t2458135335  InternalEnumerator_1_get_Current_m1152631988_gshared (InternalEnumerator_1_t3316887597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1152631988(__this, method) ((  KeyValuePair_2_t2458135335  (*) (InternalEnumerator_1_t3316887597 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1152631988_gshared)(__this, method)
