﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Membrane
struct UI_Membrane_t3925205798;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Membrane::.ctor()
extern "C"  void UI_Membrane__ctor_m2868329911 (UI_Membrane_t3925205798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
