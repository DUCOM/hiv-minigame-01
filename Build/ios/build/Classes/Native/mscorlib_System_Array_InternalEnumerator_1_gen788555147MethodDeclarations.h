﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen788555147.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"

// System.Void System.Array/InternalEnumerator`1<HIV/State>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2081301310_gshared (InternalEnumerator_1_t788555147 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2081301310(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t788555147 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2081301310_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HIV/State>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1304391434_gshared (InternalEnumerator_1_t788555147 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1304391434(__this, method) ((  void (*) (InternalEnumerator_1_t788555147 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1304391434_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HIV/State>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m738451020_gshared (InternalEnumerator_1_t788555147 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m738451020(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t788555147 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m738451020_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HIV/State>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3658644785_gshared (InternalEnumerator_1_t788555147 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3658644785(__this, method) ((  void (*) (InternalEnumerator_1_t788555147 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3658644785_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HIV/State>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2011268414_gshared (InternalEnumerator_1_t788555147 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2011268414(__this, method) ((  bool (*) (InternalEnumerator_1_t788555147 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2011268414_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HIV/State>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1587712409_gshared (InternalEnumerator_1_t788555147 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1587712409(__this, method) ((  int32_t (*) (InternalEnumerator_1_t788555147 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1587712409_gshared)(__this, method)
