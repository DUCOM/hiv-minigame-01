﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Timer
struct Timer_t2917042437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Timer::.ctor()
extern "C"  void Timer__ctor_m128890648 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Timer::get_name()
extern "C"  String_t* Timer_get_name_m1851911015 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Timer::get_remain()
extern "C"  float Timer_get_remain_m3404491543 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Timer::Update(System.Single)
extern "C"  bool Timer_Update_m3634683558 (Timer_t2917042437 * __this, float ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Timer::ResetTimer()
extern "C"  void Timer_ResetTimer_m1290734566 (Timer_t2917042437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
