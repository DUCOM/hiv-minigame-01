﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Pivot
struct Pivot_t2110476880;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AntibodyShooter
struct  AntibodyShooter_t4166449956  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AntibodyShooter::_antibody
	GameObject_t1756533147 * ____antibody_2;
	// System.Single AntibodyShooter::_shotSpeed
	float ____shotSpeed_3;
	// Pivot AntibodyShooter::_pivot
	Pivot_t2110476880 * ____pivot_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> AntibodyShooter::_bulletList
	List_1_t1125654279 * ____bulletList_5;

public:
	inline static int32_t get_offset_of__antibody_2() { return static_cast<int32_t>(offsetof(AntibodyShooter_t4166449956, ____antibody_2)); }
	inline GameObject_t1756533147 * get__antibody_2() const { return ____antibody_2; }
	inline GameObject_t1756533147 ** get_address_of__antibody_2() { return &____antibody_2; }
	inline void set__antibody_2(GameObject_t1756533147 * value)
	{
		____antibody_2 = value;
		Il2CppCodeGenWriteBarrier(&____antibody_2, value);
	}

	inline static int32_t get_offset_of__shotSpeed_3() { return static_cast<int32_t>(offsetof(AntibodyShooter_t4166449956, ____shotSpeed_3)); }
	inline float get__shotSpeed_3() const { return ____shotSpeed_3; }
	inline float* get_address_of__shotSpeed_3() { return &____shotSpeed_3; }
	inline void set__shotSpeed_3(float value)
	{
		____shotSpeed_3 = value;
	}

	inline static int32_t get_offset_of__pivot_4() { return static_cast<int32_t>(offsetof(AntibodyShooter_t4166449956, ____pivot_4)); }
	inline Pivot_t2110476880 * get__pivot_4() const { return ____pivot_4; }
	inline Pivot_t2110476880 ** get_address_of__pivot_4() { return &____pivot_4; }
	inline void set__pivot_4(Pivot_t2110476880 * value)
	{
		____pivot_4 = value;
		Il2CppCodeGenWriteBarrier(&____pivot_4, value);
	}

	inline static int32_t get_offset_of__bulletList_5() { return static_cast<int32_t>(offsetof(AntibodyShooter_t4166449956, ____bulletList_5)); }
	inline List_1_t1125654279 * get__bulletList_5() const { return ____bulletList_5; }
	inline List_1_t1125654279 ** get_address_of__bulletList_5() { return &____bulletList_5; }
	inline void set__bulletList_5(List_1_t1125654279 * value)
	{
		____bulletList_5 = value;
		Il2CppCodeGenWriteBarrier(&____bulletList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
