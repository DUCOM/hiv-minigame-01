﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Antibody
struct Antibody_t2931822436;
// HIV
struct HIV_t2481767745;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AntibodyState
struct  AntibodyState_t3996126191  : public MonoBehaviour_t1158329972
{
public:
	// Antibody AntibodyState::main
	Antibody_t2931822436 * ___main_2;
	// HIV AntibodyState::target
	HIV_t2481767745 * ___target_3;

public:
	inline static int32_t get_offset_of_main_2() { return static_cast<int32_t>(offsetof(AntibodyState_t3996126191, ___main_2)); }
	inline Antibody_t2931822436 * get_main_2() const { return ___main_2; }
	inline Antibody_t2931822436 ** get_address_of_main_2() { return &___main_2; }
	inline void set_main_2(Antibody_t2931822436 * value)
	{
		___main_2 = value;
		Il2CppCodeGenWriteBarrier(&___main_2, value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(AntibodyState_t3996126191, ___target_3)); }
	inline HIV_t2481767745 * get_target_3() const { return ___target_3; }
	inline HIV_t2481767745 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(HIV_t2481767745 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
