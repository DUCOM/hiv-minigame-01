﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1725847519MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3856050219(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3388966085 *, Dictionary_2_t2068941383 *, const MethodInfo*))Enumerator__ctor_m645837068_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1026230460(__this, method) ((  Il2CppObject * (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3333250587_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2801305528(__this, method) ((  void (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3044857175_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1649231941(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3287737962_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2805374106(__this, method) ((  Il2CppObject * (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3340591321_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m104466704(__this, method) ((  Il2CppObject * (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1561029905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::MoveNext()
#define Enumerator_MoveNext_m2718372185(__this, method) ((  bool (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_MoveNext_m2563378227_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::get_Current()
#define Enumerator_get_Current_m3979652818(__this, method) ((  KeyValuePair_2_t4121253901  (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_get_Current_m2490648155_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4074331735(__this, method) ((  int32_t (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_get_CurrentKey_m157871196_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3833351991(__this, method) ((  HIV_State_t57600565 * (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_get_CurrentValue_m130407004_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::Reset()
#define Enumerator_Reset_m4186449421(__this, method) ((  void (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_Reset_m3476599630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::VerifyState()
#define Enumerator_VerifyState_m3676316836(__this, method) ((  void (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_VerifyState_m788746469_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1249184352(__this, method) ((  void (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_VerifyCurrent_m411635231_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HIV/State,HIV_State>::Dispose()
#define Enumerator_Dispose_m660894727(__this, method) ((  void (*) (Enumerator_t3388966085 *, const MethodInfo*))Enumerator_Dispose_m1491490344_gshared)(__this, method)
