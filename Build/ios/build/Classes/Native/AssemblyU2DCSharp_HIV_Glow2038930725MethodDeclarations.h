﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Glow
struct HIV_Glow_t2038930725;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Glow::.ctor()
extern "C"  void HIV_Glow__ctor_m3588245212 (HIV_Glow_t2038930725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Glow::Start()
extern "C"  void HIV_Glow_Start_m733136548 (HIV_Glow_t2038930725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Glow::Update()
extern "C"  void HIV_Glow_Update_m2677857741 (HIV_Glow_t2038930725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
