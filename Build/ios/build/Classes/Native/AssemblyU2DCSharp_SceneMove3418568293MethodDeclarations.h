﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneMove
struct SceneMove_t3418568293;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneMove::.ctor()
extern "C"  void SceneMove__ctor_m874022028 (SceneMove_t3418568293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMove::Start()
extern "C"  void SceneMove_Start_m2831273516 (SceneMove_t3418568293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
