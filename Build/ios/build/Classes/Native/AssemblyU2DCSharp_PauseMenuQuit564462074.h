﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenuQuit
struct  PauseMenuQuit_t564462074  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button[] PauseMenuQuit::activationList
	ButtonU5BU5D_t3071100561* ___activationList_2;

public:
	inline static int32_t get_offset_of_activationList_2() { return static_cast<int32_t>(offsetof(PauseMenuQuit_t564462074, ___activationList_2)); }
	inline ButtonU5BU5D_t3071100561* get_activationList_2() const { return ___activationList_2; }
	inline ButtonU5BU5D_t3071100561** get_address_of_activationList_2() { return &___activationList_2; }
	inline void set_activationList_2(ButtonU5BU5D_t3071100561* value)
	{
		___activationList_2 = value;
		Il2CppCodeGenWriteBarrier(&___activationList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
