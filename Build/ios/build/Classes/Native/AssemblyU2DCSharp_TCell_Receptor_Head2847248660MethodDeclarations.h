﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TCell_Receptor_Head
struct TCell_Receptor_Head_t2847248660;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void TCell_Receptor_Head::.ctor()
extern "C"  void TCell_Receptor_Head__ctor_m112972757 (TCell_Receptor_Head_t2847248660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color TCell_Receptor_Head::get_color()
extern "C"  Color_t2020392075  TCell_Receptor_Head_get_color_m2836949628 (TCell_Receptor_Head_t2847248660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Receptor_Head::set_color(UnityEngine.Color)
extern "C"  void TCell_Receptor_Head_set_color_m2322111347 (TCell_Receptor_Head_t2847248660 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TCell_Receptor_Head::ChangeSprite(System.Boolean)
extern "C"  void TCell_Receptor_Head_ChangeSprite_m3064916449 (TCell_Receptor_Head_t2847248660 * __this, bool ___cd40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
