﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// System.Collections.Generic.IEqualityComparer`1<Tropism/CoReceptorType>
struct IEqualityComparer_1_t3668980621;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>[]
struct KeyValuePair_2U5BU5D_t2228800315;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>
struct IEnumerator_1_t893876929;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>
struct ValueCollection_t69100427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23418353102.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2686065286.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::.ctor()
extern "C"  void Dictionary_2__ctor_m547845402_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m547845402(__this, method) ((  void (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2__ctor_m547845402_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m711133990_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m711133990(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m711133990_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2708424126_gshared (Dictionary_2_t1366040584 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2708424126(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1366040584 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2708424126_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2362532016_gshared (Dictionary_2_t1366040584 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2362532016(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1366040584 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2362532016_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4139215111_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4139215111(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1366040584 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4139215111_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3959814108_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3959814108(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3959814108_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3366866701_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3366866701(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3366866701_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3987324872_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3987324872(__this, ___key0, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3987324872_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1002426527_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1002426527(__this, method) ((  bool (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1002426527_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3696135339_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3696135339(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3696135339_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1774768749_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1774768749(__this, method) ((  bool (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1774768749_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1578235610_gshared (Dictionary_2_t1366040584 * __this, KeyValuePair_2_t3418353102  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1578235610(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1366040584 *, KeyValuePair_2_t3418353102 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1578235610_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2714325606_gshared (Dictionary_2_t1366040584 * __this, KeyValuePair_2_t3418353102  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2714325606(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1366040584 *, KeyValuePair_2_t3418353102 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2714325606_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m782518798_gshared (Dictionary_2_t1366040584 * __this, KeyValuePair_2U5BU5D_t2228800315* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m782518798(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1366040584 *, KeyValuePair_2U5BU5D_t2228800315*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m782518798_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m262660971_gshared (Dictionary_2_t1366040584 * __this, KeyValuePair_2_t3418353102  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m262660971(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1366040584 *, KeyValuePair_2_t3418353102 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m262660971_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m367041839_gshared (Dictionary_2_t1366040584 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m367041839(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m367041839_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3446013464_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3446013464(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3446013464_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1824318261_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1824318261(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1824318261_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3129001058_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3129001058(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3129001058_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1298873415_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1298873415(__this, method) ((  int32_t (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_get_Count_m1298873415_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::get_Item(TKey)
extern "C"  float Dictionary_2_get_Item_m1577236470_gshared (Dictionary_2_t1366040584 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1577236470(__this, ___key0, method) ((  float (*) (Dictionary_2_t1366040584 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1577236470_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2344412491_gshared (Dictionary_2_t1366040584 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2344412491(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1366040584 *, int32_t, float, const MethodInfo*))Dictionary_2_set_Item_m2344412491_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1098497899_gshared (Dictionary_2_t1366040584 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1098497899(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1366040584 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1098497899_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3587809602_gshared (Dictionary_2_t1366040584 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3587809602(__this, ___size0, method) ((  void (*) (Dictionary_2_t1366040584 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3587809602_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4169937424_gshared (Dictionary_2_t1366040584 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4169937424(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4169937424_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3418353102  Dictionary_2_make_pair_m448119754_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m448119754(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3418353102  (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_make_pair_m448119754_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::pick_value(TKey,TValue)
extern "C"  float Dictionary_2_pick_value_m2353434676_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2353434676(__this /* static, unused */, ___key0, ___value1, method) ((  float (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_pick_value_m2353434676_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1074179295_gshared (Dictionary_2_t1366040584 * __this, KeyValuePair_2U5BU5D_t2228800315* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1074179295(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1366040584 *, KeyValuePair_2U5BU5D_t2228800315*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1074179295_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::Resize()
extern "C"  void Dictionary_2_Resize_m4208566597_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m4208566597(__this, method) ((  void (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_Resize_m4208566597_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m245902210_gshared (Dictionary_2_t1366040584 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m245902210(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1366040584 *, int32_t, float, const MethodInfo*))Dictionary_2_Add_m245902210_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::Clear()
extern "C"  void Dictionary_2_Clear_m1329736082_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1329736082(__this, method) ((  void (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_Clear_m1329736082_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m340150990_gshared (Dictionary_2_t1366040584 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m340150990(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1366040584 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m340150990_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1161843902_gshared (Dictionary_2_t1366040584 * __this, float ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1161843902(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1366040584 *, float, const MethodInfo*))Dictionary_2_ContainsValue_m1161843902_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m391836563_gshared (Dictionary_2_t1366040584 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m391836563(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1366040584 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m391836563_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2867214059_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2867214059(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1366040584 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2867214059_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m150107982_gshared (Dictionary_2_t1366040584 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m150107982(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1366040584 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m150107982_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2090594715_gshared (Dictionary_2_t1366040584 * __this, int32_t ___key0, float* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2090594715(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1366040584 *, int32_t, float*, const MethodInfo*))Dictionary_2_TryGetValue_m2090594715_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::get_Values()
extern "C"  ValueCollection_t69100427 * Dictionary_2_get_Values_m1184726390_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1184726390(__this, method) ((  ValueCollection_t69100427 * (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_get_Values_m1184726390_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2542569589_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2542569589(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1366040584 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2542569589_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::ToTValue(System.Object)
extern "C"  float Dictionary_2_ToTValue_m4255554389_gshared (Dictionary_2_t1366040584 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4255554389(__this, ___value0, method) ((  float (*) (Dictionary_2_t1366040584 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4255554389_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2911003555_gshared (Dictionary_2_t1366040584 * __this, KeyValuePair_2_t3418353102  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2911003555(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1366040584 *, KeyValuePair_2_t3418353102 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2911003555_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::GetEnumerator()
extern "C"  Enumerator_t2686065286  Dictionary_2_GetEnumerator_m1008879512_gshared (Dictionary_2_t1366040584 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1008879512(__this, method) ((  Enumerator_t2686065286  (*) (Dictionary_2_t1366040584 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1008879512_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m4207443653_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m4207443653(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m4207443653_gshared)(__this /* static, unused */, ___key0, ___value1, method)
