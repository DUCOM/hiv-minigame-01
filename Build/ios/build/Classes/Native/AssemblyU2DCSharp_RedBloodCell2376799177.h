﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellMovement
struct CellMovement_t2110243311;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RedBloodCell
struct  RedBloodCell_t2376799177  : public MonoBehaviour_t1158329972
{
public:
	// CellMovement RedBloodCell::<move>k__BackingField
	CellMovement_t2110243311 * ___U3CmoveU3Ek__BackingField_3;
	// System.Single RedBloodCell::_radius
	float ____radius_4;
	// UnityEngine.Rect RedBloodCell::_disableRect
	Rect_t3681755626  ____disableRect_5;

public:
	inline static int32_t get_offset_of_U3CmoveU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RedBloodCell_t2376799177, ___U3CmoveU3Ek__BackingField_3)); }
	inline CellMovement_t2110243311 * get_U3CmoveU3Ek__BackingField_3() const { return ___U3CmoveU3Ek__BackingField_3; }
	inline CellMovement_t2110243311 ** get_address_of_U3CmoveU3Ek__BackingField_3() { return &___U3CmoveU3Ek__BackingField_3; }
	inline void set_U3CmoveU3Ek__BackingField_3(CellMovement_t2110243311 * value)
	{
		___U3CmoveU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmoveU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of__radius_4() { return static_cast<int32_t>(offsetof(RedBloodCell_t2376799177, ____radius_4)); }
	inline float get__radius_4() const { return ____radius_4; }
	inline float* get_address_of__radius_4() { return &____radius_4; }
	inline void set__radius_4(float value)
	{
		____radius_4 = value;
	}

	inline static int32_t get_offset_of__disableRect_5() { return static_cast<int32_t>(offsetof(RedBloodCell_t2376799177, ____disableRect_5)); }
	inline Rect_t3681755626  get__disableRect_5() const { return ____disableRect_5; }
	inline Rect_t3681755626 * get_address_of__disableRect_5() { return &____disableRect_5; }
	inline void set__disableRect_5(Rect_t3681755626  value)
	{
		____disableRect_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
