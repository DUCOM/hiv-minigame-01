﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Pause
struct UI_Pause_t1107290467;
// Controller
struct Controller_t1937198888;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"

// System.Void UI_Pause::.ctor()
extern "C"  void UI_Pause__ctor_m3520464454 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Controller UI_Pause::get_controller()
extern "C"  Controller_t1937198888 * UI_Pause_get_controller_m2786843706 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Pause::set_controller(Controller)
extern "C"  void UI_Pause_set_controller_m1138709003 (UI_Pause_t1107290467 * __this, Controller_t1937198888 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button UI_Pause::get_button()
extern "C"  Button_t2872111280 * UI_Pause_get_button_m1688983065 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UI_Pause::get_transform()
extern "C"  RectTransform_t3349966182 * UI_Pause_get_transform_m2136912435 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Pause::Pause()
extern "C"  void UI_Pause_Pause_m2324070012 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Pause::Start()
extern "C"  void UI_Pause_Start_m3851690914 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Pause::Update()
extern "C"  void UI_Pause_Update_m1187948687 (UI_Pause_t1107290467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
