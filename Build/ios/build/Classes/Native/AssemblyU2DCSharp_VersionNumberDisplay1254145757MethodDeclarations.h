﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionNumberDisplay
struct VersionNumberDisplay_t1254145757;

#include "codegen/il2cpp-codegen.h"

// System.Void VersionNumberDisplay::.ctor()
extern "C"  void VersionNumberDisplay__ctor_m4250395844 (VersionNumberDisplay_t1254145757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionNumberDisplay::Start()
extern "C"  void VersionNumberDisplay_Start_m2875642844 (VersionNumberDisplay_t1254145757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
