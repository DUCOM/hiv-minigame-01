﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AntibodyWander
struct AntibodyWander_t3668243203;
// HIV
struct HIV_t2481767745;
// SpikeInteract
struct SpikeInteract_t2340666440;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HIV2481767745.h"
#include "AssemblyU2DCSharp_SpikeInteract2340666440.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void AntibodyWander::.ctor()
extern "C"  void AntibodyWander__ctor_m2536199068 (AntibodyWander_t3668243203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AntibodyWander::AngleToTarget(HIV)
extern "C"  float AntibodyWander_AngleToTarget_m3984465456 (AntibodyWander_t3668243203 * __this, HIV_t2481767745 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyWander::OnEnable()
extern "C"  void AntibodyWander_OnEnable_m3346171824 (AntibodyWander_t3668243203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyWander::OnDisable()
extern "C"  void AntibodyWander_OnDisable_m2576274259 (AntibodyWander_t3668243203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyWander::Latch(SpikeInteract)
extern "C"  void AntibodyWander_Latch_m2905961308 (AntibodyWander_t3668243203 * __this, SpikeInteract_t2340666440 * ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyWander::LateUpdate()
extern "C"  void AntibodyWander_LateUpdate_m1162477055 (AntibodyWander_t3668243203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AntibodyWander::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void AntibodyWander_OnTriggerEnter2D_m3157812008 (AntibodyWander_t3668243203 * __this, Collider2D_t646061738 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
