﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HIV
struct HIV_t2481767745;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UI_SpikeList
struct UI_SpikeList_t1985653691;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_HIV
struct  UI_HIV_t1412098442  : public MonoBehaviour_t1158329972
{
public:
	// HIV UI_HIV::_selected
	HIV_t2481767745 * ____selected_2;
	// UnityEngine.UI.Image UI_HIV::capsid
	Image_t2042527209 * ___capsid_3;
	// UnityEngine.UI.Image UI_HIV::membrane
	Image_t2042527209 * ___membrane_4;
	// UnityEngine.UI.Image UI_HIV::matrix
	Image_t2042527209 * ___matrix_5;
	// UnityEngine.UI.Image[] UI_HIV::rna
	ImageU5BU5D_t590162004* ___rna_6;
	// UnityEngine.UI.Image[] UI_HIV::rt
	ImageU5BU5D_t590162004* ___rt_7;
	// UI_SpikeList UI_HIV::spikeList
	UI_SpikeList_t1985653691 * ___spikeList_8;

public:
	inline static int32_t get_offset_of__selected_2() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ____selected_2)); }
	inline HIV_t2481767745 * get__selected_2() const { return ____selected_2; }
	inline HIV_t2481767745 ** get_address_of__selected_2() { return &____selected_2; }
	inline void set__selected_2(HIV_t2481767745 * value)
	{
		____selected_2 = value;
		Il2CppCodeGenWriteBarrier(&____selected_2, value);
	}

	inline static int32_t get_offset_of_capsid_3() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ___capsid_3)); }
	inline Image_t2042527209 * get_capsid_3() const { return ___capsid_3; }
	inline Image_t2042527209 ** get_address_of_capsid_3() { return &___capsid_3; }
	inline void set_capsid_3(Image_t2042527209 * value)
	{
		___capsid_3 = value;
		Il2CppCodeGenWriteBarrier(&___capsid_3, value);
	}

	inline static int32_t get_offset_of_membrane_4() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ___membrane_4)); }
	inline Image_t2042527209 * get_membrane_4() const { return ___membrane_4; }
	inline Image_t2042527209 ** get_address_of_membrane_4() { return &___membrane_4; }
	inline void set_membrane_4(Image_t2042527209 * value)
	{
		___membrane_4 = value;
		Il2CppCodeGenWriteBarrier(&___membrane_4, value);
	}

	inline static int32_t get_offset_of_matrix_5() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ___matrix_5)); }
	inline Image_t2042527209 * get_matrix_5() const { return ___matrix_5; }
	inline Image_t2042527209 ** get_address_of_matrix_5() { return &___matrix_5; }
	inline void set_matrix_5(Image_t2042527209 * value)
	{
		___matrix_5 = value;
		Il2CppCodeGenWriteBarrier(&___matrix_5, value);
	}

	inline static int32_t get_offset_of_rna_6() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ___rna_6)); }
	inline ImageU5BU5D_t590162004* get_rna_6() const { return ___rna_6; }
	inline ImageU5BU5D_t590162004** get_address_of_rna_6() { return &___rna_6; }
	inline void set_rna_6(ImageU5BU5D_t590162004* value)
	{
		___rna_6 = value;
		Il2CppCodeGenWriteBarrier(&___rna_6, value);
	}

	inline static int32_t get_offset_of_rt_7() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ___rt_7)); }
	inline ImageU5BU5D_t590162004* get_rt_7() const { return ___rt_7; }
	inline ImageU5BU5D_t590162004** get_address_of_rt_7() { return &___rt_7; }
	inline void set_rt_7(ImageU5BU5D_t590162004* value)
	{
		___rt_7 = value;
		Il2CppCodeGenWriteBarrier(&___rt_7, value);
	}

	inline static int32_t get_offset_of_spikeList_8() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442, ___spikeList_8)); }
	inline UI_SpikeList_t1985653691 * get_spikeList_8() const { return ___spikeList_8; }
	inline UI_SpikeList_t1985653691 ** get_address_of_spikeList_8() { return &___spikeList_8; }
	inline void set_spikeList_8(UI_SpikeList_t1985653691 * value)
	{
		___spikeList_8 = value;
		Il2CppCodeGenWriteBarrier(&___spikeList_8, value);
	}
};

struct UI_HIV_t1412098442_StaticFields
{
public:
	// UnityEngine.Color UI_HIV::defaultColor
	Color_t2020392075  ___defaultColor_9;

public:
	inline static int32_t get_offset_of_defaultColor_9() { return static_cast<int32_t>(offsetof(UI_HIV_t1412098442_StaticFields, ___defaultColor_9)); }
	inline Color_t2020392075  get_defaultColor_9() const { return ___defaultColor_9; }
	inline Color_t2020392075 * get_address_of_defaultColor_9() { return &___defaultColor_9; }
	inline void set_defaultColor_9(Color_t2020392075  value)
	{
		___defaultColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
