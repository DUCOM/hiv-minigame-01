﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayerBackground
struct  LayerBackground_t3535234259  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera LayerBackground::render
	Camera_t189460977 * ___render_2;
	// UnityEngine.Sprite LayerBackground::picture
	Sprite_t309593783 * ___picture_3;
	// System.Int32 LayerBackground::layer
	int32_t ___layer_4;
	// System.Single LayerBackground::render_width
	float ___render_width_5;
	// System.Single LayerBackground::render_height
	float ___render_height_6;
	// UnityEngine.Color LayerBackground::_backgroundBlend
	Color_t2020392075  ____backgroundBlend_7;
	// UnityEngine.SpriteRenderer[] LayerBackground::_backgroundSprites
	SpriteRendererU5BU5D_t1098056643* ____backgroundSprites_8;
	// UnityEngine.Vector3 LayerBackground::cameraPosPrev
	Vector3_t2243707580  ___cameraPosPrev_9;

public:
	inline static int32_t get_offset_of_render_2() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ___render_2)); }
	inline Camera_t189460977 * get_render_2() const { return ___render_2; }
	inline Camera_t189460977 ** get_address_of_render_2() { return &___render_2; }
	inline void set_render_2(Camera_t189460977 * value)
	{
		___render_2 = value;
		Il2CppCodeGenWriteBarrier(&___render_2, value);
	}

	inline static int32_t get_offset_of_picture_3() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ___picture_3)); }
	inline Sprite_t309593783 * get_picture_3() const { return ___picture_3; }
	inline Sprite_t309593783 ** get_address_of_picture_3() { return &___picture_3; }
	inline void set_picture_3(Sprite_t309593783 * value)
	{
		___picture_3 = value;
		Il2CppCodeGenWriteBarrier(&___picture_3, value);
	}

	inline static int32_t get_offset_of_layer_4() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ___layer_4)); }
	inline int32_t get_layer_4() const { return ___layer_4; }
	inline int32_t* get_address_of_layer_4() { return &___layer_4; }
	inline void set_layer_4(int32_t value)
	{
		___layer_4 = value;
	}

	inline static int32_t get_offset_of_render_width_5() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ___render_width_5)); }
	inline float get_render_width_5() const { return ___render_width_5; }
	inline float* get_address_of_render_width_5() { return &___render_width_5; }
	inline void set_render_width_5(float value)
	{
		___render_width_5 = value;
	}

	inline static int32_t get_offset_of_render_height_6() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ___render_height_6)); }
	inline float get_render_height_6() const { return ___render_height_6; }
	inline float* get_address_of_render_height_6() { return &___render_height_6; }
	inline void set_render_height_6(float value)
	{
		___render_height_6 = value;
	}

	inline static int32_t get_offset_of__backgroundBlend_7() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ____backgroundBlend_7)); }
	inline Color_t2020392075  get__backgroundBlend_7() const { return ____backgroundBlend_7; }
	inline Color_t2020392075 * get_address_of__backgroundBlend_7() { return &____backgroundBlend_7; }
	inline void set__backgroundBlend_7(Color_t2020392075  value)
	{
		____backgroundBlend_7 = value;
	}

	inline static int32_t get_offset_of__backgroundSprites_8() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ____backgroundSprites_8)); }
	inline SpriteRendererU5BU5D_t1098056643* get__backgroundSprites_8() const { return ____backgroundSprites_8; }
	inline SpriteRendererU5BU5D_t1098056643** get_address_of__backgroundSprites_8() { return &____backgroundSprites_8; }
	inline void set__backgroundSprites_8(SpriteRendererU5BU5D_t1098056643* value)
	{
		____backgroundSprites_8 = value;
		Il2CppCodeGenWriteBarrier(&____backgroundSprites_8, value);
	}

	inline static int32_t get_offset_of_cameraPosPrev_9() { return static_cast<int32_t>(offsetof(LayerBackground_t3535234259, ___cameraPosPrev_9)); }
	inline Vector3_t2243707580  get_cameraPosPrev_9() const { return ___cameraPosPrev_9; }
	inline Vector3_t2243707580 * get_address_of_cameraPosPrev_9() { return &___cameraPosPrev_9; }
	inline void set_cameraPosPrev_9(Vector3_t2243707580  value)
	{
		___cameraPosPrev_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
