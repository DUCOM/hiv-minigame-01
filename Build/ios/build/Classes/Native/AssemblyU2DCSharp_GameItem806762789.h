﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameItem
struct  GameItem_t806762789 
{
public:
	// UnityEngine.GameObject GameItem::itemReference
	GameObject_t1756533147 * ___itemReference_0;
	// System.Int32 GameItem::_maximumNumber
	int32_t ____maximumNumber_1;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameItem::_itemList
	List_1_t1125654279 * ____itemList_2;

public:
	inline static int32_t get_offset_of_itemReference_0() { return static_cast<int32_t>(offsetof(GameItem_t806762789, ___itemReference_0)); }
	inline GameObject_t1756533147 * get_itemReference_0() const { return ___itemReference_0; }
	inline GameObject_t1756533147 ** get_address_of_itemReference_0() { return &___itemReference_0; }
	inline void set_itemReference_0(GameObject_t1756533147 * value)
	{
		___itemReference_0 = value;
		Il2CppCodeGenWriteBarrier(&___itemReference_0, value);
	}

	inline static int32_t get_offset_of__maximumNumber_1() { return static_cast<int32_t>(offsetof(GameItem_t806762789, ____maximumNumber_1)); }
	inline int32_t get__maximumNumber_1() const { return ____maximumNumber_1; }
	inline int32_t* get_address_of__maximumNumber_1() { return &____maximumNumber_1; }
	inline void set__maximumNumber_1(int32_t value)
	{
		____maximumNumber_1 = value;
	}

	inline static int32_t get_offset_of__itemList_2() { return static_cast<int32_t>(offsetof(GameItem_t806762789, ____itemList_2)); }
	inline List_1_t1125654279 * get__itemList_2() const { return ____itemList_2; }
	inline List_1_t1125654279 ** get_address_of__itemList_2() { return &____itemList_2; }
	inline void set__itemList_2(List_1_t1125654279 * value)
	{
		____itemList_2 = value;
		Il2CppCodeGenWriteBarrier(&____itemList_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GameItem
struct GameItem_t806762789_marshaled_pinvoke
{
	GameObject_t1756533147 * ___itemReference_0;
	int32_t ____maximumNumber_1;
	List_1_t1125654279 * ____itemList_2;
};
// Native definition for COM marshalling of GameItem
struct GameItem_t806762789_marshaled_com
{
	GameObject_t1756533147 * ___itemReference_0;
	int32_t ____maximumNumber_1;
	List_1_t1125654279 * ____itemList_2;
};
