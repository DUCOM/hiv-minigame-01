﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3951188146MethodDeclarations.h"

// System.Void System.Comparison`1<Sound>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3278727470(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2088455784 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Sound>::Invoke(T,T)
#define Comparison_1_Invoke_m2682219450(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2088455784 *, Sound_t826716933 *, Sound_t826716933 *, const MethodInfo*))Comparison_1_Invoke_m2798106261_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Sound>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m3305926609(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2088455784 *, Sound_t826716933 *, Sound_t826716933 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1817828810_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Sound>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m3539206568(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2088455784 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m1056665895_gshared)(__this, ___result0, method)
