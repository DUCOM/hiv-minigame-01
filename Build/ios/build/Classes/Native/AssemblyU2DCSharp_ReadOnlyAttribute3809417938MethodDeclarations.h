﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReadOnlyAttribute
struct ReadOnlyAttribute_t3809417938;

#include "codegen/il2cpp-codegen.h"

// System.Void ReadOnlyAttribute::.ctor()
extern "C"  void ReadOnlyAttribute__ctor_m4159498297 (ReadOnlyAttribute_t3809417938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
