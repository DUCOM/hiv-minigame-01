﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FocalPointFollow
struct FocalPointFollow_t4210230956;

#include "codegen/il2cpp-codegen.h"

// System.Void FocalPointFollow::.ctor()
extern "C"  void FocalPointFollow__ctor_m2442169225 (FocalPointFollow_t4210230956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FocalPointFollow::Update()
extern "C"  void FocalPointFollow_Update_m365053530 (FocalPointFollow_t4210230956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
