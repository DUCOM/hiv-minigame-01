﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteRender
struct  SpriteRender_t2828817349 
{
public:
	// UnityEngine.SpriteRenderer SpriteRender::render
	SpriteRenderer_t1209076198 * ___render_0;
	// UnityEngine.Color SpriteRender::_color
	Color_t2020392075  ____color_1;

public:
	inline static int32_t get_offset_of_render_0() { return static_cast<int32_t>(offsetof(SpriteRender_t2828817349, ___render_0)); }
	inline SpriteRenderer_t1209076198 * get_render_0() const { return ___render_0; }
	inline SpriteRenderer_t1209076198 ** get_address_of_render_0() { return &___render_0; }
	inline void set_render_0(SpriteRenderer_t1209076198 * value)
	{
		___render_0 = value;
		Il2CppCodeGenWriteBarrier(&___render_0, value);
	}

	inline static int32_t get_offset_of__color_1() { return static_cast<int32_t>(offsetof(SpriteRender_t2828817349, ____color_1)); }
	inline Color_t2020392075  get__color_1() const { return ____color_1; }
	inline Color_t2020392075 * get_address_of__color_1() { return &____color_1; }
	inline void set__color_1(Color_t2020392075  value)
	{
		____color_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SpriteRender
struct SpriteRender_t2828817349_marshaled_pinvoke
{
	SpriteRenderer_t1209076198 * ___render_0;
	Color_t2020392075  ____color_1;
};
// Native definition for COM marshalling of SpriteRender
struct SpriteRender_t2828817349_marshaled_com
{
	SpriteRenderer_t1209076198 * ___render_0;
	Color_t2020392075  ____color_1;
};
