﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// TCell
struct TCell_t1815110036;
// CellMovement
struct CellMovement_t2110243311;
// System.Object
struct Il2CppObject;
// TCell_Coreceptor
struct TCell_Coreceptor_t1508369739;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// TCell_Cytoplasm
struct TCell_Cytoplasm_t256238633;
// TCell_Membrane
struct TCell_Membrane_t392610684;
// TCell_Nucleous
struct TCell_Nucleous_t3012779807;
// TCell_Pair
struct TCell_Pair_t2779000105;
// System.String
struct String_t;
// TCell_Part
struct TCell_Part_t1972431076;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TCell_Piece
struct TCell_Piece_t3459712141;
// TCell_Receptor
struct TCell_Receptor_t2424206879;
// TCell_Receptor_Head
struct TCell_Receptor_Head_t2847248660;
// TCell_Receptor_Stem
struct TCell_Receptor_Stem_t4177804179;
// Timer
struct Timer_t2917042437;
// TimerList
struct TimerList_t2973412395;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// Tropism/ReceptorType[]
struct ReceptorTypeU5BU5D_t2292174278;
// Tropism/CoReceptorType[]
struct CoReceptorTypeU5BU5D_t3312104658;
// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// Tutorial
struct Tutorial_t735545730;
// UnityEngine.Animator
struct Animator_t69676727;
// UI_Background
struct UI_Background_t3920117671;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UI_Bar
struct UI_Bar_t1358396100;
// GameController
struct GameController_t3607102586;
// HIV
struct HIV_t2481767745;
// UI_Capsid
struct UI_Capsid_t4027023025;
// UI_Developer_Button
struct UI_Developer_Button_t3399765548;
// UI_Developer_Stats
struct UI_Developer_Stats_t2682907225;
// UnityEngine.UI.Text
struct Text_t356221433;
// UI_GP120
struct UI_GP120_t1483396489;
// UI_GP41
struct UI_GP41_t2232403251;
// UI_HIV
struct UI_HIV_t1412098442;
// UI_HIV_Button
struct UI_HIV_Button_t3180703511;
// UI_Immunogenicity
struct UI_Immunogenicity_t1529844486;
// UI_Level
struct UI_Level_t2621343419;
// UI_Matrix
struct UI_Matrix_t2197715354;
// UI_Membrane
struct UI_Membrane_t3925205798;
// UI_Pause
struct UI_Pause_t1107290467;
// Controller
struct Controller_t1937198888;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UI_Population
struct UI_Population_t2609643076;
// UI_Resume
struct UI_Resume_t2961944548;
// UI_RNA
struct UI_RNA_t3307819878;
// UI_RT
struct UI_RT_t3092214427;
// UI_Score
struct UI_Score_t2043792049;
// UI_Spike
struct UI_Spike_t1842279613;
// UI_SpikeList
struct UI_SpikeList_t1985653691;
// UI_TryAgain
struct UI_TryAgain_t3222824968;
// UI_World_Filler
struct UI_World_Filler_t108322328;
// VersionNumberDisplay
struct VersionNumberDisplay_t1254145757;
// ZoomController
struct ZoomController_t1627556657;
// RenderCamera
struct RenderCamera_t2596733641;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_TCell1815110036.h"
#include "AssemblyU2DCSharp_TCell1815110036MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_PairList3336234724MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Pair2779000105MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_TCell_Part1972431076.h"
#include "AssemblyU2DCSharp_TCell_Piece3459712141.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharp_PairList3336234724.h"
#include "AssemblyU2DCSharp_TCell_Pair2779000105.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_Game1600141214MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "AssemblyU2DCSharp_Game1600141214.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_CellMovement2110243311.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Part1972431076MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "AssemblyU2DCSharp_Functions2564965619MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "AssemblyU2DCSharp_TCell_Coreceptor1508369739.h"
#include "AssemblyU2DCSharp_TCell_Coreceptor1508369739MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"
#include "AssemblyU2DCSharp_Tropism3662836552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "AssemblyU2DCSharp_TCell_Cytoplasm256238633.h"
#include "AssemblyU2DCSharp_TCell_Cytoplasm256238633MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Piece3459712141MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Membrane392610684.h"
#include "AssemblyU2DCSharp_TCell_Membrane392610684MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Nucleous3012779807.h"
#include "AssemblyU2DCSharp_TCell_Nucleous3012779807MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Receptor2424206879.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "AssemblyU2DCSharp_TCell_Receptor_Head2847248660MethodDeclarations.h"
#include "AssemblyU2DCSharp_TCell_Receptor_Head2847248660.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"
#include "AssemblyU2DCSharp_TCell_Receptor_Stem4177804179.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "AssemblyU2DCSharp_TCell_Receptor_Stem4177804179MethodDeclarations.h"
#include "AssemblyU2DCSharp_Timer2917042437.h"
#include "AssemblyU2DCSharp_Timer2917042437MethodDeclarations.h"
#include "AssemblyU2DCSharp_Timer_TimerState3696398080.h"
#include "AssemblyU2DCSharp_Timer_TimerState3696398080MethodDeclarations.h"
#include "AssemblyU2DCSharp_TimerList2973412395.h"
#include "AssemblyU2DCSharp_TimerList2973412395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2286163569MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2286163569.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_Functions2564965619.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge346110204.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1366040584.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge346110204MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1366040584MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tutorial735545730.h"
#include "AssemblyU2DCSharp_Tutorial735545730MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Background3920117671.h"
#include "AssemblyU2DCSharp_UI_Background3920117671MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "AssemblyU2DCSharp_UI_Bar1358396100.h"
#include "AssemblyU2DCSharp_UI_Bar1358396100MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_HIV2481767745.h"
#include "AssemblyU2DCSharp_GameController3607102586MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV2481767745MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIV_State4224770181.h"
#include "AssemblyU2DCSharp_HIVStats2521285894MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "AssemblyU2DCSharp_HIVStats2521285894.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_UI_Capsid4027023025.h"
#include "AssemblyU2DCSharp_UI_Capsid4027023025MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Developer_Button3399765548.h"
#include "AssemblyU2DCSharp_UI_Developer_Button3399765548MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Developer_Stats2682907225.h"
#include "AssemblyU2DCSharp_UI_Developer_Stats2682907225MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1125654279.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_GP1201483396489.h"
#include "AssemblyU2DCSharp_UI_GP1201483396489MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_GP412232403251.h"
#include "AssemblyU2DCSharp_UI_GP412232403251MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_HIV1412098442.h"
#include "AssemblyU2DCSharp_UI_HIV1412098442MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_SpikeList1985653691MethodDeclarations.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "AssemblyU2DCSharp_UI_SpikeList1985653691.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163.h"
#include "AssemblyU2DCSharp_SpikeColor3573234163MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_HIV_Button3180703511.h"
#include "AssemblyU2DCSharp_UI_HIV_Button3180703511MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity1529844486.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity1529844486MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity_States3735768305.h"
#include "AssemblyU2DCSharp_UI_Immunogenicity_States3735768305MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Level2621343419.h"
#include "AssemblyU2DCSharp_UI_Level2621343419MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Matrix2197715354.h"
#include "AssemblyU2DCSharp_UI_Matrix2197715354MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Membrane3925205798.h"
#include "AssemblyU2DCSharp_UI_Membrane3925205798MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Pause1107290467.h"
#include "AssemblyU2DCSharp_UI_Pause1107290467MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188MethodDeclarations.h"
#include "AssemblyU2DCSharp_Controller1937198888MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Pause_States3339147058.h"
#include "AssemblyU2DCSharp_UI_Pause_States3339147058MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Population2609643076.h"
#include "AssemblyU2DCSharp_UI_Population2609643076MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Resume2961944548.h"
#include "AssemblyU2DCSharp_UI_Resume2961944548MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_RNA3307819878.h"
#include "AssemblyU2DCSharp_UI_RNA3307819878MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_RT3092214427.h"
#include "AssemblyU2DCSharp_UI_RT3092214427MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Score2043792049.h"
#include "AssemblyU2DCSharp_UI_Score2043792049MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_Spike1842279613.h"
#include "AssemblyU2DCSharp_UI_Spike1842279613MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_TryAgain3222824968.h"
#include "AssemblyU2DCSharp_UI_TryAgain3222824968MethodDeclarations.h"
#include "AssemblyU2DCSharp_UI_World_Filler108322328.h"
#include "AssemblyU2DCSharp_UI_World_Filler108322328MethodDeclarations.h"
#include "AssemblyU2DCSharp_VersionNumberDisplay1254145757.h"
#include "AssemblyU2DCSharp_VersionNumberDisplay1254145757MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZoomController1627556657.h"
#include "AssemblyU2DCSharp_ZoomController1627556657MethodDeclarations.h"
#include "AssemblyU2DCSharp_RenderCamera2596733641.h"
#include "AssemblyU2DCSharp_RenderCamera2596733641MethodDeclarations.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CellMovement>()
#define GameObject_GetComponent_TisCellMovement_t2110243311_m18013212(__this, method) ((  CellMovement_t2110243311 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m327292296(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<TCell_Piece>()
#define GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852(__this, method) ((  TCell_Piece_t3459712141 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<TCell_Receptor_Head>()
#define GameObject_GetComponentInChildren_TisTCell_Receptor_Head_t2847248660_m1731563457(__this, method) ((  TCell_Receptor_Head_t2847248660 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<TCell_Receptor_Stem>()
#define GameObject_GetComponentInChildren_TisTCell_Receptor_Stem_t4177804179_m2163575762(__this, method) ((  TCell_Receptor_Stem_t4177804179 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<TCell>()
#define Component_GetComponent_TisTCell_t1815110036_m427081541(__this, method) ((  TCell_t1815110036 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// T Functions::Random<UnityEngine.Color>(T[])
extern "C"  Color_t2020392075  Functions_Random_TisColor_t2020392075_m1668057946_gshared (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___randList0, const MethodInfo* method);
#define Functions_Random_TisColor_t2020392075_m1668057946(__this /* static, unused */, ___randList0, method) ((  Color_t2020392075  (*) (Il2CppObject * /* static, unused */, ColorU5BU5D_t672350442*, const MethodInfo*))Functions_Random_TisColor_t2020392075_m1668057946_gshared)(__this /* static, unused */, ___randList0, method)
// T Functions::Random<Tropism/ReceptorType>(T[],System.Collections.Generic.Dictionary`2<T,System.Single>)
extern "C"  int32_t Functions_Random_TisReceptorType_t1016603615_m1423371369_gshared (Il2CppObject * __this /* static, unused */, ReceptorTypeU5BU5D_t2292174278* ___randList0, Dictionary_2_t346110204 * ___probVal1, const MethodInfo* method);
#define Functions_Random_TisReceptorType_t1016603615_m1423371369(__this /* static, unused */, ___randList0, ___probVal1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ReceptorTypeU5BU5D_t2292174278*, Dictionary_2_t346110204 *, const MethodInfo*))Functions_Random_TisReceptorType_t1016603615_m1423371369_gshared)(__this /* static, unused */, ___randList0, ___probVal1, method)
// T Functions::Random<Tropism/CoReceptorType>(T[],System.Collections.Generic.Dictionary`2<T,System.Single>)
extern "C"  int32_t Functions_Random_TisCoReceptorType_t161380547_m3024276845_gshared (Il2CppObject * __this /* static, unused */, CoReceptorTypeU5BU5D_t3312104658* ___randList0, Dictionary_2_t1366040584 * ___probVal1, const MethodInfo* method);
#define Functions_Random_TisCoReceptorType_t161380547_m3024276845(__this /* static, unused */, ___randList0, ___probVal1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CoReceptorTypeU5BU5D_t3312104658*, Dictionary_2_t1366040584 *, const MethodInfo*))Functions_Random_TisCoReceptorType_t161380547_m3024276845_gshared)(__this /* static, unused */, ___randList0, ___probVal1, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m1008560876(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(__this, method) ((  RectTransform_t3349966182 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Controller>()
#define GameObject_GetComponent_TisController_t1937198888_m3117371903(__this, method) ((  Controller_t1937198888 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3412601438(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UI_GP120>()
#define GameObject_GetComponentInChildren_TisUI_GP120_t1483396489_m1328587960(__this, method) ((  UI_GP120_t1483396489 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UI_GP41>()
#define GameObject_GetComponentInChildren_TisUI_GP41_t2232403251_m3759450054(__this, method) ((  UI_GP41_t2232403251 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<RenderCamera>()
#define Component_GetComponent_TisRenderCamera_t2596733641_m551332000(__this, method) ((  RenderCamera_t2596733641 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TCell::.ctor()
extern "C"  void TCell__ctor_m2466026901 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single TCell::get_radius()
extern "C"  float TCell_get_radius_m399111662 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__radius_3();
		return L_0;
	}
}
// System.Void TCell::set_radius(System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TCell_set_radius_m3434622193_MetadataUsageId;
extern "C"  void TCell_set_radius_m3434622193 (TCell_t1815110036 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_set_radius_m3434622193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	{
		float L_0 = ___value0;
		V_0 = ((float)((float)L_0/(float)(4.0f)));
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_2 = V_0;
		float L_3 = V_0;
		float L_4 = V_0;
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m2325460848(L_1, L_5, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_6 = __this->get_cytoplasm_6();
		NullCheck(L_6);
		TCell_Piece_t3459712141 * L_7 = L_6->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		TCell_Part_t1972431076 * L_9 = __this->get_cytoplasm_6();
		NullCheck(L_9);
		TCell_Piece_t3459712141 * L_10 = L_9->get_instance_2();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m2325460848(L_11, L_12, /*hidden argument*/NULL);
	}

IL_004b:
	{
		TCell_Part_t1972431076 * L_13 = __this->get_membrane_7();
		NullCheck(L_13);
		TCell_Piece_t3459712141 * L_14 = L_13->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007b;
		}
	}
	{
		TCell_Part_t1972431076 * L_16 = __this->get_membrane_7();
		NullCheck(L_16);
		TCell_Piece_t3459712141 * L_17 = L_16->get_instance_2();
		NullCheck(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_localScale_m2325460848(L_18, L_19, /*hidden argument*/NULL);
	}

IL_007b:
	{
		TCell_Part_t1972431076 * L_20 = __this->get_nucleous_8();
		NullCheck(L_20);
		TCell_Piece_t3459712141 * L_21 = L_20->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ab;
		}
	}
	{
		TCell_Part_t1972431076 * L_23 = __this->get_nucleous_8();
		NullCheck(L_23);
		TCell_Piece_t3459712141 * L_24 = L_23->get_instance_2();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_localScale_m2325460848(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		PairList_t3336234724 * L_27 = __this->get_pairList_9();
		float L_28 = V_0;
		NullCheck(L_27);
		PairList_Scale_m1647140082(L_27, ((float)((float)(1.0f)/(float)L_28)), /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0100;
	}

IL_00c4:
	{
		int32_t L_29 = V_1;
		PairList_t3336234724 * L_30 = __this->get_pairList_9();
		NullCheck(L_30);
		int32_t L_31 = PairList_get_count_m1467844419(L_30, /*hidden argument*/NULL);
		V_2 = ((float)((float)((float)((float)(((float)((float)L_29)))/(float)(((float)((float)L_31)))))*(float)(360.0f)));
		PairList_t3336234724 * L_32 = __this->get_pairList_9();
		int32_t L_33 = V_1;
		NullCheck(L_32);
		TCell_Pair_t2779000105 * L_34 = PairList_get_Item_m1768557300(L_32, L_33, /*hidden argument*/NULL);
		float L_35 = ___value0;
		float L_36 = V_2;
		Quaternion_t4030073918  L_37 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		TCell_Pair_MovePivotAndRotate_m1475944733(L_34, L_35, L_37, /*hidden argument*/NULL);
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_0100:
	{
		int32_t L_39 = V_1;
		PairList_t3336234724 * L_40 = __this->get_pairList_9();
		NullCheck(L_40);
		int32_t L_41 = PairList_get_count_m1467844419(L_40, /*hidden argument*/NULL);
		if ((((int32_t)L_39) < ((int32_t)L_41)))
		{
			goto IL_00c4;
		}
	}
	{
		float L_42 = ___value0;
		__this->set__radius_3(L_42);
		return;
	}
}
// System.Boolean TCell::get_infected()
extern "C"  bool TCell_get_infected_m1112633080 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__infected_5();
		return L_0;
	}
}
// System.Void TCell::set_infected(System.Boolean)
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t TCell_set_infected_m1220593909_MetadataUsageId;
extern "C"  void TCell_set_infected_m1220593909 (TCell_t1815110036 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_set_infected_m1220593909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		Color__ctor_m3811852957((&V_0), (0.3f), (0.3f), (0.3f), /*hidden argument*/NULL);
		float L_1 = (&V_0)->get_r_0();
		float L_2 = (&V_0)->get_g_1();
		float L_3 = (&V_0)->get_b_2();
		Color__ctor_m1909920690((&V_1), L_1, L_2, L_3, (0.5f), /*hidden argument*/NULL);
		Color_t2020392075  L_4 = V_1;
		Color_t2020392075  L_5 = V_0;
		Color_t2020392075  L_6 = V_0;
		TCell_ChangeType_m2456386377(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
		PairList_t3336234724 * L_7 = __this->get_pairList_9();
		NullCheck(L_7);
		PairList_Disable_m1506047595(L_7, /*hidden argument*/NULL);
		Game_t1600141214 * L_8 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		Quaternion_t4030073918  L_9 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t2243707580  L_10 = V_2;
		NullCheck(L_8);
		Game_CreateTCell_m1287670579(L_8, L_9, (9.0f), L_10, /*hidden argument*/NULL);
		goto IL_0080;
	}

IL_0074:
	{
		PairList_t3336234724 * L_11 = __this->get_pairList_9();
		NullCheck(L_11);
		PairList_Enable_m180892362(L_11, __this, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// CellMovement TCell::get_move()
extern const MethodInfo* GameObject_GetComponent_TisCellMovement_t2110243311_m18013212_MethodInfo_var;
extern const uint32_t TCell_get_move_m3002539955_MetadataUsageId;
extern "C"  CellMovement_t2110243311 * TCell_get_move_m3002539955 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_get_move_m3002539955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		CellMovement_t2110243311 * L_1 = GameObject_GetComponent_TisCellMovement_t2110243311_m18013212(L_0, /*hidden argument*/GameObject_GetComponent_TisCellMovement_t2110243311_m18013212_MethodInfo_var);
		return L_1;
	}
}
// System.Void TCell::ChangeType(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color)
extern "C"  void TCell_ChangeType_m2456386377 (TCell_t1815110036 * __this, Color_t2020392075  ___cytoplasmColor0, Color_t2020392075  ___membraneColor1, Color_t2020392075  ___nucleousColor2, const MethodInfo* method)
{
	{
		TCell_Part_t1972431076 * L_0 = __this->get_cytoplasm_6();
		Color_t2020392075  L_1 = ___cytoplasmColor0;
		NullCheck(L_0);
		TCell_Part_set_color_m3036638271(L_0, L_1, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_2 = __this->get_membrane_7();
		Color_t2020392075  L_3 = ___membraneColor1;
		NullCheck(L_2);
		TCell_Part_set_color_m3036638271(L_2, L_3, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_4 = __this->get_nucleous_8();
		Color_t2020392075  L_5 = ___nucleousColor2;
		NullCheck(L_4);
		TCell_Part_set_color_m3036638271(L_4, L_5, /*hidden argument*/NULL);
		PairList_t3336234724 * L_6 = __this->get_pairList_9();
		NullCheck(L_6);
		PairList_ChangeTropism_m453644655(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell::Instantiate(System.Single)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3681806819;
extern const uint32_t TCell_Instantiate_m358529744_MetadataUsageId;
extern "C"  void TCell_Instantiate_m358529744 (TCell_t1815110036 * __this, float ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Instantiate_m358529744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___r0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral3681806819, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		float L_2 = ___r0;
		if ((!(((float)L_2) == ((float)(0.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		___r0 = (4.0f);
	}

IL_0028:
	{
		TCell_Part_t1972431076 * L_3 = __this->get_cytoplasm_6();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_6 = V_0;
		NullCheck(L_3);
		TCell_Part_Instantiate_m328937377(L_3, L_5, L_6, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_7 = __this->get_membrane_7();
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_10 = V_0;
		NullCheck(L_7);
		TCell_Part_Instantiate_m328937377(L_7, L_9, L_10, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_11 = __this->get_nucleous_8();
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		TCell_Part_Instantiate_m328937377(L_11, L_13, L_14, /*hidden argument*/NULL);
		PairList_t3336234724 * L_15 = __this->get_pairList_9();
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		float L_18 = ___r0;
		NullCheck(L_15);
		PairList_Instantiate_m1745309583(L_15, L_17, L_18, /*hidden argument*/NULL);
		float L_19 = ___r0;
		TCell_set_radius_m3434622193(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell::Instantiate(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3681806819;
extern const uint32_t TCell_Instantiate_m866403822_MetadataUsageId;
extern "C"  void TCell_Instantiate_m866403822 (TCell_t1815110036 * __this, Color_t2020392075  ___cytoplasmColor0, Color_t2020392075  ___membraneColor1, Color_t2020392075  ___nucleousColor2, float ___r3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Instantiate_m866403822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___r3;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral3681806819, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		float L_2 = ___r3;
		if ((!(((float)L_2) == ((float)(0.0f)))))
		{
			goto IL_002a;
		}
	}
	{
		___r3 = (4.0f);
	}

IL_002a:
	{
		TCell_Part_t1972431076 * L_3 = __this->get_cytoplasm_6();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Color_t2020392075  L_6 = ___cytoplasmColor0;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_7 = V_0;
		NullCheck(L_3);
		TCell_Part_Instantiate_m3847004091(L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_8 = __this->get_membrane_7();
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		Color_t2020392075  L_11 = ___membraneColor1;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_12 = V_0;
		NullCheck(L_8);
		TCell_Part_Instantiate_m3847004091(L_8, L_10, L_11, L_12, /*hidden argument*/NULL);
		TCell_Part_t1972431076 * L_13 = __this->get_nucleous_8();
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_16 = ___nucleousColor2;
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_17 = V_0;
		NullCheck(L_13);
		TCell_Part_Instantiate_m3847004091(L_13, L_15, L_16, L_17, /*hidden argument*/NULL);
		PairList_t3336234724 * L_18 = __this->get_pairList_9();
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_20 = TCell_get_radius_m399111662(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		PairList_Instantiate_m1745309583(L_18, L_19, L_20, /*hidden argument*/NULL);
		float L_21 = ___r3;
		TCell_set_radius_m3434622193(__this, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell::Start()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TCell_Start_m1649656325_MetadataUsageId;
extern "C"  void TCell_Start_m1649656325 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Start_m1649656325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_instantiateOnStart_4();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = TCell_get_radius_m399111662(__this, /*hidden argument*/NULL);
		TCell_Instantiate_m358529744(__this, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void TCell::FixedUpdate()
extern "C"  void TCell_FixedUpdate_m1814111036 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__timer_10();
		if ((!(((float)L_0) <= ((float)(0.0f)))))
		{
			goto IL_0030;
		}
	}
	{
		float L_1 = Random_Range_m2884721203(NULL /*static, unused*/, (-0.5f), (0.5f), /*hidden argument*/NULL);
		__this->set__rotate_11(L_1);
		__this->set__timer_10((2.0f));
	}

IL_0030:
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_3 = __this->get__rotate_11();
		NullCheck(L_2);
		Transform_Rotate_m4255273365(L_2, (0.0f), (0.0f), L_3, /*hidden argument*/NULL);
		float L_4 = __this->get__timer_10();
		float L_5 = Time_get_fixedDeltaTime_m2734072926(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__timer_10(((float)((float)L_4-(float)L_5)));
		return;
	}
}
// System.Void TCell::LateUpdate()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t TCell_LateUpdate_m691496878_MetadataUsageId;
extern "C"  void TCell_LateUpdate_m691496878 (TCell_t1815110036 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_LateUpdate_m691496878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		Game_t1600141214 * L_3 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_3);
		Rect_t3681755626  L_4 = Game_get__tCellDisable_m3709581821(L_3, /*hidden argument*/NULL);
		Rect_t3681755626  L_5 = Functions_CameraToWorldRect_m4129867403(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		bool L_6 = Functions_OutsideRect_m364063437(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		Game_t1600141214 * L_7 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		Quaternion_t4030073918  L_8 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_9 = V_0;
		NullCheck(L_7);
		Game_CreateTCell_m1287670579(L_7, L_8, (9.0f), L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void TCell_Coreceptor::.ctor()
extern "C"  void TCell_Coreceptor__ctor_m3581639614 (TCell_Coreceptor_t1508369739 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tropism/CoReceptorType TCell_Coreceptor::get_type()
extern "C"  int32_t TCell_Coreceptor_get_type_m878847845 (TCell_Coreceptor_t1508369739 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__type_2();
		return L_0;
	}
}
// System.Void TCell_Coreceptor::set_type(Tropism/CoReceptorType)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t TCell_Coreceptor_set_type_m130603720_MetadataUsageId;
extern "C"  void TCell_Coreceptor_set_type_m130603720 (TCell_Coreceptor_t1508369739 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Coreceptor_set_type_m130603720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpriteRenderer_t1209076198 * L_0 = TCell_Coreceptor_get_spriteRender_m838108580(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Color_t2020392075  L_2 = Tropism_DefaultColor_m3873948415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_set_color_m2339931967(L_0, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___value0;
		__this->set__type_2(L_3);
		return;
	}
}
// UnityEngine.SpriteRenderer TCell_Coreceptor::get_spriteRender()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t TCell_Coreceptor_get_spriteRender_m838108580_MetadataUsageId;
extern "C"  SpriteRenderer_t1209076198 * TCell_Coreceptor_get_spriteRender_m838108580 (TCell_Coreceptor_t1508369739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Coreceptor_get_spriteRender_m838108580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		return L_1;
	}
}
// System.Void TCell_Cytoplasm::.ctor()
extern "C"  void TCell_Cytoplasm__ctor_m725607054 (TCell_Cytoplasm_t256238633 * __this, const MethodInfo* method)
{
	{
		TCell_Piece__ctor_m2043918716(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Membrane::.ctor()
extern "C"  void TCell_Membrane__ctor_m2072503969 (TCell_Membrane_t392610684 * __this, const MethodInfo* method)
{
	{
		TCell_Piece__ctor_m2043918716(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Nucleous::.ctor()
extern "C"  void TCell_Nucleous__ctor_m1909947938 (TCell_Nucleous_t3012779807 * __this, const MethodInfo* method)
{
	{
		TCell_Piece__ctor_m2043918716(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Pair::.ctor()
extern "C"  void TCell_Pair__ctor_m4209467636 (TCell_Pair_t2779000105 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tropism TCell_Pair::get_tropism()
extern "C"  Tropism_t3662836552  TCell_Pair_get_tropism_m4283886040 (TCell_Pair_t2779000105 * __this, const MethodInfo* method)
{
	{
		Tropism_t3662836552  L_0 = __this->get_tropism__5();
		return L_0;
	}
}
// System.Void TCell_Pair::set_tropism(Tropism)
extern "C"  void TCell_Pair_set_tropism_m3515168797 (TCell_Pair_t2779000105 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method)
{
	{
		TCell_Receptor_t2424206879 * L_0 = __this->get_receptor_2();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		TCell_Coreceptor_t1508369739 * L_2 = __this->get_coReceptor_3();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		TCell_Receptor_t2424206879 * L_4 = __this->get_receptor_2();
		Tropism_t3662836552  L_5 = ___value0;
		NullCheck(L_4);
		TCell_Receptor_set_type_m1618614825(L_4, L_5, /*hidden argument*/NULL);
		TCell_Coreceptor_t1508369739 * L_6 = __this->get_coReceptor_3();
		int32_t L_7 = (&___value0)->get_coreceptor_11();
		NullCheck(L_6);
		TCell_Coreceptor_set_type_m130603720(L_6, L_7, /*hidden argument*/NULL);
		Tropism_t3662836552  L_8 = ___value0;
		__this->set_tropism__5(L_8);
		return;
	}
}
// System.Void TCell_Pair::Scale(System.Single)
extern "C"  void TCell_Pair_Scale_m1323056481 (TCell_Pair_t2779000105 * __this, float ___s0, const MethodInfo* method)
{
	{
		float L_0 = ___s0;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_2 = ___s0;
		float L_3 = ___s0;
		float L_4 = ___s0;
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m2325460848(L_1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Pair::MovePivot(UnityEngine.Vector3)
extern "C"  void TCell_Pair_MovePivot_m1340188852 (TCell_Pair_t2779000105 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_pivot_4();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___position0;
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Pair::MovePivotAndRotate(System.Single,UnityEngine.Quaternion)
extern "C"  void TCell_Pair_MovePivotAndRotate_m1475944733 (TCell_Pair_t2779000105 * __this, float ___radius0, Quaternion_t4030073918  ___angle1, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_pivot_4();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		float L_2 = ___radius0;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, (0.0f), L_2, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = ___angle1;
		NullCheck(L_4);
		Transform_set_rotation_m3411284563(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Pair::MovePivotAndRotate(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void TCell_Pair_MovePivotAndRotate_m1238696853 (TCell_Pair_t2779000105 * __this, Vector3_t2243707580  ___translation0, Quaternion_t4030073918  ___rotation1, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_pivot_4();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___translation0;
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_pivot_4();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = ___rotation1;
		NullCheck(L_4);
		Transform_set_localRotation_m2055111962(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Pair::PivotRotation(UnityEngine.Vector3)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t TCell_Pair_PivotRotation_m1777389265_MetadataUsageId;
extern "C"  void TCell_Pair_PivotRotation_m1777389265 (TCell_Pair_t2779000105 * __this, Vector3_t2243707580  ___eulerAngles0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Pair_PivotRotation_m1777389265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = __this->get_pivot_4();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Initobj (Quaternion_t4030073918_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t4030073918  L_2 = V_0;
		NullCheck(L_1);
		Transform_set_rotation_m3411284563(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_pivot_4();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = ___eulerAngles0;
		NullCheck(L_4);
		Transform_Rotate_m1743927093(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Pair::SetLayer(System.String)
extern "C"  void TCell_Pair_SetLayer_m3073446271 (TCell_Pair_t2779000105 * __this, String_t* ___s0, const MethodInfo* method)
{
	{
		TCell_Receptor_t2424206879 * L_0 = __this->get_receptor_2();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___s0;
		int32_t L_3 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_set_layer_m2712461877(L_1, L_3, /*hidden argument*/NULL);
		TCell_Coreceptor_t1508369739 * L_4 = __this->get_coReceptor_3();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		String_t* L_6 = ___s0;
		int32_t L_7 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_set_layer_m2712461877(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_9 = ___s0;
		int32_t L_10 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_set_layer_m2712461877(L_8, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Part::.ctor()
extern "C"  void TCell_Part__ctor_m1329199869 (TCell_Part_t1972431076 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color TCell_Part::get_color()
extern "C"  Color_t2020392075  TCell_Part_get_color_m2749663366 (TCell_Part_t1972431076 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get_color__1();
		return L_0;
	}
}
// System.Void TCell_Part::set_color(UnityEngine.Color)
extern "C"  void TCell_Part_set_color_m3036638271 (TCell_Part_t1972431076 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		TCell_Piece_t3459712141 * L_0 = __this->get_instance_2();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		TCell_Piece_set_color_m3929169316(L_0, L_1, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = ___value0;
		__this->set_color__1(L_2);
		return;
	}
}
// System.Void TCell_Part::Instantiate(UnityEngine.Transform,UnityEngine.Color,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3547850815;
extern Il2CppCodeGenString* _stringLiteral441962826;
extern const uint32_t TCell_Part_Instantiate_m3847004091_MetadataUsageId;
extern "C"  void TCell_Part_Instantiate_m3847004091 (TCell_Part_t1972431076 * __this, Transform_t3275118058 * ___parent0, Color_t2020392075  ___c1, Vector3_t2243707580  ___position2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Part_Instantiate_m3847004091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_2, _stringLiteral3547850815, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		TCell_Piece_t3459712141 * L_5 = GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852(L_4, /*hidden argument*/GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852_MethodInfo_var);
		__this->set_instance_2(L_5);
		TCell_Piece_t3459712141 * L_6 = __this->get_instance_2();
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		NullReferenceException_t3156209119 * L_8 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_8, _stringLiteral441962826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004e:
	{
		TCell_Piece_t3459712141 * L_9 = __this->get_instance_2();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = ___parent0;
		NullCheck(L_10);
		Transform_set_parent_m3281327839(L_10, L_11, /*hidden argument*/NULL);
		TCell_Piece_t3459712141 * L_12 = __this->get_instance_2();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = ___position2;
		NullCheck(L_13);
		Transform_set_localPosition_m1026930133(L_13, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = ___c1;
		TCell_Part_set_color_m3036638271(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Part::Instantiate(UnityEngine.Transform,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3547850815;
extern Il2CppCodeGenString* _stringLiteral441962826;
extern const uint32_t TCell_Part_Instantiate_m328937377_MetadataUsageId;
extern "C"  void TCell_Part_Instantiate_m328937377 (TCell_Part_t1972431076 * __this, Transform_t3275118058 * ___parent0, Vector3_t2243707580  ___position1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Part_Instantiate_m328937377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_2, _stringLiteral3547850815, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		TCell_Piece_t3459712141 * L_5 = GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852(L_4, /*hidden argument*/GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852_MethodInfo_var);
		__this->set_instance_2(L_5);
		TCell_Piece_t3459712141 * L_6 = __this->get_instance_2();
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		NullReferenceException_t3156209119 * L_8 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_8, _stringLiteral441962826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004e:
	{
		Transform_t3275118058 * L_9 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		TCell_Piece_t3459712141 * L_11 = __this->get_instance_2();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = ___parent0;
		NullCheck(L_12);
		Transform_set_parent_m3281327839(L_12, L_13, /*hidden argument*/NULL);
	}

IL_006b:
	{
		TCell_Piece_t3459712141 * L_14 = __this->get_instance_2();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = ___position1;
		NullCheck(L_15);
		Transform_set_localPosition_m1026930133(L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Part::Instantiate()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3890207715;
extern Il2CppCodeGenString* _stringLiteral441962826;
extern const uint32_t TCell_Part_Instantiate_m1841548663_MetadataUsageId;
extern "C"  void TCell_Part_Instantiate_m1841548663 (TCell_Part_t1972431076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Part_Instantiate_m1841548663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_2, _stringLiteral3890207715, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		TCell_Piece_t3459712141 * L_5 = GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852(L_4, /*hidden argument*/GameObject_GetComponent_TisTCell_Piece_t3459712141_m2823476852_MethodInfo_var);
		__this->set_instance_2(L_5);
		TCell_Piece_t3459712141 * L_6 = __this->get_instance_2();
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		NullReferenceException_t3156209119 * L_8 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2372754786(L_8, _stringLiteral441962826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004e:
	{
		return;
	}
}
// System.Void TCell_Piece::.ctor()
extern "C"  void TCell_Piece__ctor_m2043918716 (TCell_Piece_t3459712141 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color TCell_Piece::get_color()
extern "C"  Color_t2020392075  TCell_Piece_get_color_m2225525357 (TCell_Piece_t3459712141 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get__color_2();
		return L_0;
	}
}
// System.Void TCell_Piece::set_color(UnityEngine.Color)
extern "C"  void TCell_Piece_set_color_m3929169316 (TCell_Piece_t3459712141 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = TCell_Piece_get_spriteRender_m1679985202(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		SpriteRenderer_set_color_m2339931967(L_0, L_1, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = ___value0;
		__this->set__color_2(L_2);
		return;
	}
}
// UnityEngine.SpriteRenderer TCell_Piece::get_spriteRender()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t TCell_Piece_get_spriteRender_m1679985202_MetadataUsageId;
extern "C"  SpriteRenderer_t1209076198 * TCell_Piece_get_spriteRender_m1679985202 (TCell_Piece_t3459712141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Piece_get_spriteRender_m1679985202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		return L_1;
	}
}
// System.Void TCell_Receptor::.ctor()
extern "C"  void TCell_Receptor__ctor_m2921416192 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Tropism TCell_Receptor::get_type()
extern "C"  Tropism_t3662836552  TCell_Receptor_get_type_m1545012622 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method)
{
	{
		Tropism_t3662836552  L_0 = __this->get__type_2();
		return L_0;
	}
}
// System.Void TCell_Receptor::set_type(Tropism)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t TCell_Receptor_set_type_m1618614825_MetadataUsageId;
extern "C"  void TCell_Receptor_set_type_m1618614825 (TCell_Receptor_t2424206879 * __this, Tropism_t3662836552  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_set_type_m1618614825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TCell_Receptor_Head_t2847248660 * L_0 = TCell_Receptor_get_head_m3435036360(__this, /*hidden argument*/NULL);
		int32_t L_1 = (&___value0)->get_receptor_10();
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Color_t2020392075  L_2 = Tropism_DefaultColor_m1098737047(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		TCell_Receptor_Head_set_color_m2322111347(L_0, L_2, /*hidden argument*/NULL);
		TCell_Receptor_Head_t2847248660 * L_3 = TCell_Receptor_get_head_m3435036360(__this, /*hidden argument*/NULL);
		int32_t L_4 = (&___value0)->get_receptor_10();
		NullCheck(L_3);
		TCell_Receptor_Head_ChangeSprite_m3064916449(L_3, (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		Tropism_t3662836552  L_5 = ___value0;
		__this->set__type_2(L_5);
		return;
	}
}
// TCell_Receptor_Head TCell_Receptor::get_head()
extern const MethodInfo* GameObject_GetComponentInChildren_TisTCell_Receptor_Head_t2847248660_m1731563457_MethodInfo_var;
extern const uint32_t TCell_Receptor_get_head_m3435036360_MetadataUsageId;
extern "C"  TCell_Receptor_Head_t2847248660 * TCell_Receptor_get_head_m3435036360 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_get_head_m3435036360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TCell_Receptor_Head_t2847248660 * L_1 = GameObject_GetComponentInChildren_TisTCell_Receptor_Head_t2847248660_m1731563457(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisTCell_Receptor_Head_t2847248660_m1731563457_MethodInfo_var);
		return L_1;
	}
}
// TCell_Receptor_Stem TCell_Receptor::get_stem()
extern const MethodInfo* GameObject_GetComponentInChildren_TisTCell_Receptor_Stem_t4177804179_m2163575762_MethodInfo_var;
extern const uint32_t TCell_Receptor_get_stem_m1676136050_MetadataUsageId;
extern "C"  TCell_Receptor_Stem_t4177804179 * TCell_Receptor_get_stem_m1676136050 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_get_stem_m1676136050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		TCell_Receptor_Stem_t4177804179 * L_1 = GameObject_GetComponentInChildren_TisTCell_Receptor_Stem_t4177804179_m2163575762(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisTCell_Receptor_Stem_t4177804179_m2163575762_MethodInfo_var);
		return L_1;
	}
}
// TCell TCell_Receptor::get_cell()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTCell_t1815110036_m427081541_MethodInfo_var;
extern const uint32_t TCell_Receptor_get_cell_m2345807034_MetadataUsageId;
extern "C"  TCell_t1815110036 * TCell_Receptor_get_cell_m2345807034 (TCell_Receptor_t2424206879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_get_cell_m2345807034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TCell_t1815110036 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_get_parent_m147407266(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_get_parent_m147407266(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_get_parent_m147407266(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		TCell_t1815110036 * L_4 = Component_GetComponent_TisTCell_t1815110036_m427081541(L_3, /*hidden argument*/Component_GetComponent_TisTCell_t1815110036_m427081541_MethodInfo_var);
		V_0 = L_4;
		TCell_t1815110036 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		NullReferenceException_t3156209119 * L_7 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m2202599572(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002d:
	{
		TCell_t1815110036 * L_8 = V_0;
		return L_8;
	}
}
// System.Void TCell_Receptor_Head::.ctor()
extern "C"  void TCell_Receptor_Head__ctor_m112972757 (TCell_Receptor_Head_t2847248660 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color TCell_Receptor_Head::get_color()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t TCell_Receptor_Head_get_color_m2836949628_MetadataUsageId;
extern "C"  Color_t2020392075  TCell_Receptor_Head_get_color_m2836949628 (TCell_Receptor_Head_t2847248660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_Head_get_color_m2836949628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		NullCheck(L_1);
		Color_t2020392075  L_2 = SpriteRenderer_get_color_m345525162(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void TCell_Receptor_Head::set_color(UnityEngine.Color)
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t TCell_Receptor_Head_set_color_m2322111347_MetadataUsageId;
extern "C"  void TCell_Receptor_Head_set_color_m2322111347 (TCell_Receptor_Head_t2847248660 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_Head_set_color_m2322111347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		Color_t2020392075  L_2 = ___value0;
		NullCheck(L_1);
		SpriteRenderer_set_color_m2339931967(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Receptor_Head::ChangeSprite(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var;
extern const uint32_t TCell_Receptor_Head_ChangeSprite_m3064916449_MetadataUsageId;
extern "C"  void TCell_Receptor_Head_ChangeSprite_m3064916449 (TCell_Receptor_Head_t2847248660 * __this, bool ___cd40, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_Head_ChangeSprite_m3064916449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t1209076198 * G_B2_0 = NULL;
	SpriteRenderer_t1209076198 * G_B1_0 = NULL;
	Sprite_t309593783 * G_B3_0 = NULL;
	SpriteRenderer_t1209076198 * G_B3_1 = NULL;
	SpriteRenderer_t1209076198 * G_B5_0 = NULL;
	SpriteRenderer_t1209076198 * G_B4_0 = NULL;
	Sprite_t309593783 * G_B6_0 = NULL;
	SpriteRenderer_t1209076198 * G_B6_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_HeadDisplayFront_6();
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		bool L_2 = ___cd40;
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_001c;
		}
	}
	{
		Sprite_t309593783 * L_3 = __this->get_CD4PositiveFront_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0022;
	}

IL_001c:
	{
		Sprite_t309593783 * L_4 = __this->get_CD4NegativeFront_4();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0022:
	{
		NullCheck(G_B3_1);
		SpriteRenderer_set_sprite_m617298623(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_HeadDisplayBack_7();
		NullCheck(L_5);
		SpriteRenderer_t1209076198 * L_6 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_5, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_MethodInfo_var);
		bool L_7 = ___cd40;
		G_B4_0 = L_6;
		if (!L_7)
		{
			G_B5_0 = L_6;
			goto IL_0043;
		}
	}
	{
		Sprite_t309593783 * L_8 = __this->get_CD4PositiveBack_3();
		G_B6_0 = L_8;
		G_B6_1 = G_B4_0;
		goto IL_0049;
	}

IL_0043:
	{
		Sprite_t309593783 * L_9 = __this->get_CD4NegativeBack_5();
		G_B6_0 = L_9;
		G_B6_1 = G_B5_0;
	}

IL_0049:
	{
		NullCheck(G_B6_1);
		SpriteRenderer_set_sprite_m617298623(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TCell_Receptor_Stem::.ctor()
extern "C"  void TCell_Receptor_Stem__ctor_m90802210 (TCell_Receptor_Stem_t4177804179 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color TCell_Receptor_Stem::get_color()
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t TCell_Receptor_Stem_get_color_m792012227_MetadataUsageId;
extern "C"  Color_t2020392075  TCell_Receptor_Stem_get_color_m792012227 (TCell_Receptor_Stem_t4177804179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_Stem_get_color_m792012227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		NullCheck(L_1);
		Color_t2020392075  L_2 = SpriteRenderer_get_color_m345525162(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void TCell_Receptor_Stem::set_color(UnityEngine.Color)
extern const MethodInfo* GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var;
extern const uint32_t TCell_Receptor_Stem_set_color_m2894468426_MetadataUsageId;
extern "C"  void TCell_Receptor_Stem_set_color_m2894468426 (TCell_Receptor_Stem_t4177804179 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TCell_Receptor_Stem_set_color_m2894468426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisSpriteRenderer_t1209076198_m2889907448_MethodInfo_var);
		Color_t2020392075  L_2 = ___value0;
		NullCheck(L_1);
		SpriteRenderer_set_color_m2339931967(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Timer::.ctor()
extern "C"  void Timer__ctor_m128890648 (Timer_t2917042437 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Timer::get_name()
extern "C"  String_t* Timer_get_name_m1851911015 (Timer_t2917042437 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__name_1();
		return L_0;
	}
}
// System.Single Timer::get_remain()
extern "C"  float Timer_get_remain_m3404491543 (Timer_t2917042437 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get__time_4();
		return L_0;
	}
}
// System.Boolean Timer::Update(System.Single)
extern "C"  bool Timer_Update_m3634683558 (Timer_t2917042437 * __this, float ___t0, const MethodInfo* method)
{
	{
		float L_0 = __this->get__time_4();
		float L_1 = ___t0;
		__this->set__time_4(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get__time_4();
		return (bool)((((int32_t)((!(((float)L_2) <= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Timer::ResetTimer()
extern "C"  void Timer_ResetTimer_m1290734566 (Timer_t2917042437 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_duration_0();
		__this->set__time_4(L_0);
		return;
	}
}
// System.Void TimerList::.ctor()
extern "C"  void TimerList__ctor_m2468386336 (TimerList_t2973412395 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Timer TimerList::get_Item(System.Int32)
extern "C"  Timer_t2917042437 * TimerList_get_Item_m499053599 (TimerList_t2973412395 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		Timer_t2917042437 * L_1 = TimerList_GetTimer_m2536853788(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Timer TimerList::get_Item(System.String)
extern "C"  Timer_t2917042437 * TimerList_get_Item_m1551049948 (TimerList_t2973412395 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Timer_t2917042437 * L_1 = TimerList_GetTimer_m3694739425(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Timer TimerList::GetTimer(System.Int32)
extern const MethodInfo* List_1_get_Count_m673458836_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m429264777_MethodInfo_var;
extern const uint32_t TimerList_GetTimer_m2536853788_MetadataUsageId;
extern "C"  Timer_t2917042437 * TimerList_GetTimer_m2536853788 (TimerList_t2973412395 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_GetTimer_m2536853788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___index0;
		List_1_t2286163569 * L_2 = __this->get_timers_2();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m673458836(L_2, /*hidden argument*/List_1_get_Count_m673458836_MethodInfo_var);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (Timer_t2917042437 *)NULL;
	}

IL_001a:
	{
		List_1_t2286163569 * L_4 = __this->get_timers_2();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		Timer_t2917042437 * L_6 = List_1_get_Item_m429264777(L_4, L_5, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		return L_6;
	}
}
// Timer TimerList::GetTimer(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m429264777_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m673458836_MethodInfo_var;
extern const uint32_t TimerList_GetTimer_m3694739425_MetadataUsageId;
extern "C"  Timer_t2917042437 * TimerList_GetTimer_m3694739425 (TimerList_t2973412395 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_GetTimer_m3694739425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0034;
	}

IL_0007:
	{
		List_1_t2286163569 * L_0 = __this->get_timers_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Timer_t2917042437 * L_2 = List_1_get_Item_m429264777(L_0, L_1, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		NullCheck(L_2);
		String_t* L_3 = Timer_get_name_m1851911015(L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		List_1_t2286163569 * L_6 = __this->get_timers_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Timer_t2917042437 * L_8 = List_1_get_Item_m429264777(L_6, L_7, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		return L_8;
	}

IL_0030:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_10 = V_0;
		List_1_t2286163569 * L_11 = __this->get_timers_2();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m673458836(L_11, /*hidden argument*/List_1_get_Count_m673458836_MethodInfo_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0007;
		}
	}
	{
		return (Timer_t2917042437 *)NULL;
	}
}
// System.Void TimerList::ResetTimer(Timer)
extern Il2CppCodeGenString* _stringLiteral2706561161;
extern const uint32_t TimerList_ResetTimer_m297055939_MetadataUsageId;
extern "C"  void TimerList_ResetTimer_m297055939 (TimerList_t2973412395 * __this, Timer_t2917042437 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_ResetTimer_m297055939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Timer_t2917042437 * L_0 = ___t0;
		Component_SendMessage_m2241432133(__this, _stringLiteral2706561161, L_0, 1, /*hidden argument*/NULL);
		Timer_t2917042437 * L_1 = ___t0;
		NullCheck(L_1);
		Timer_ResetTimer_m1290734566(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerList::Awake()
extern const MethodInfo* List_1_get_Item_m429264777_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m673458836_MethodInfo_var;
extern const uint32_t TimerList_Awake_m3789520375_MetadataUsageId;
extern "C"  void TimerList_Awake_m3789520375 (TimerList_t2973412395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_Awake_m3789520375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timer_t2917042437 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0007:
	{
		List_1_t2286163569 * L_0 = __this->get_timers_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Timer_t2917042437 * L_2 = List_1_get_Item_m429264777(L_0, L_1, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		V_1 = L_2;
		Timer_t2917042437 * L_3 = V_1;
		TimerList_ResetTimer_m297055939(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		List_1_t2286163569 * L_6 = __this->get_timers_2();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m673458836(L_6, /*hidden argument*/List_1_get_Count_m673458836_MethodInfo_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TimerList::OnEnable()
extern const MethodInfo* List_1_get_Item_m429264777_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m673458836_MethodInfo_var;
extern const uint32_t TimerList_OnEnable_m466275068_MetadataUsageId;
extern "C"  void TimerList_OnEnable_m466275068 (TimerList_t2973412395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_OnEnable_m466275068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timer_t2917042437 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0007:
	{
		List_1_t2286163569 * L_0 = __this->get_timers_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Timer_t2917042437 * L_2 = List_1_get_Item_m429264777(L_0, L_1, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		V_1 = L_2;
		Timer_t2917042437 * L_3 = V_1;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_002e;
		}
	}
	{
		Timer_t2917042437 * L_5 = V_1;
		TimerList_ResetTimer_m297055939(__this, L_5, /*hidden argument*/NULL);
		Timer_t2917042437 * L_6 = V_1;
		NullCheck(L_6);
		L_6->set_state_2(0);
	}

IL_002e:
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_8 = V_0;
		List_1_t2286163569 * L_9 = __this->get_timers_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m673458836(L_9, /*hidden argument*/List_1_get_Count_m673458836_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TimerList::LateUpdate()
extern const MethodInfo* List_1_get_Item_m429264777_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m673458836_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3234487565;
extern const uint32_t TimerList_LateUpdate_m438122191_MetadataUsageId;
extern "C"  void TimerList_LateUpdate_m438122191 (TimerList_t2973412395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_LateUpdate_m438122191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timer_t2917042437 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0059;
	}

IL_0007:
	{
		List_1_t2286163569 * L_0 = __this->get_timers_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Timer_t2917042437 * L_2 = List_1_get_Item_m429264777(L_0, L_1, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		V_1 = L_2;
		Timer_t2917042437 * L_3 = V_1;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_state_2();
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		Timer_t2917042437 * L_5 = V_1;
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_7 = Timer_Update_m3634683558(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0055;
		}
	}
	{
		Timer_t2917042437 * L_8 = V_1;
		TimerList_ResetTimer_m297055939(__this, L_8, /*hidden argument*/NULL);
		Timer_t2917042437 * L_9 = V_1;
		Component_SendMessage_m2241432133(__this, _stringLiteral3234487565, L_9, 1, /*hidden argument*/NULL);
		Timer_t2917042437 * L_10 = V_1;
		NullCheck(L_10);
		bool L_11 = L_10->get_loop_3();
		if (L_11)
		{
			goto IL_0055;
		}
	}
	{
		Timer_t2917042437 * L_12 = V_1;
		NullCheck(L_12);
		L_12->set_state_2(2);
	}

IL_0055:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0059:
	{
		int32_t L_14 = V_0;
		List_1_t2286163569 * L_15 = __this->get_timers_2();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m673458836(L_15, /*hidden argument*/List_1_get_Count_m673458836_MethodInfo_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TimerList::OnValidate()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m429264777_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m673458836_MethodInfo_var;
extern const uint32_t TimerList_OnValidate_m3943067401_MetadataUsageId;
extern "C"  void TimerList_OnValidate_m3943067401 (TimerList_t2973412395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerList_OnValidate_m3943067401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Timer_t2917042437 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0007:
	{
		List_1_t2286163569 * L_0 = __this->get_timers_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Timer_t2917042437 * L_2 = List_1_get_Item_m429264777(L_0, L_1, /*hidden argument*/List_1_get_Item_m429264777_MethodInfo_var);
		V_1 = L_2;
		Timer_t2917042437 * L_3 = V_1;
		Timer_t2917042437 * L_4 = V_1;
		NullCheck(L_4);
		float L_5 = L_4->get_duration_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_5, (0.001f), /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_duration_0(L_6);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_0;
		List_1_t2286163569 * L_9 = __this->get_timers_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m673458836(L_9, /*hidden argument*/List_1_get_Count_m673458836_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Tropism::.ctor(Tropism/ReceptorType,Tropism/CoReceptorType)
extern "C"  void Tropism__ctor_m2978840885 (Tropism_t3662836552 * __this, int32_t ___r0, int32_t ___c1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___r0;
		__this->set_receptor_10(L_0);
		int32_t L_1 = ___c1;
		__this->set_coreceptor_11(L_1);
		return;
	}
}
extern "C"  void Tropism__ctor_m2978840885_AdjustorThunk (Il2CppObject * __this, int32_t ___r0, int32_t ___c1, const MethodInfo* method)
{
	Tropism_t3662836552 * _thisAdjusted = reinterpret_cast<Tropism_t3662836552 *>(__this + 1);
	Tropism__ctor_m2978840885(_thisAdjusted, ___r0, ___c1, method);
}
// System.Boolean Tropism::Equals(System.Object)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t Tropism_Equals_m1822115980_MetadataUsageId;
extern "C"  bool Tropism_Equals_m1822115980 (Tropism_t3662836552 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_Equals_m1822115980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tropism_t3662836552  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___o0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		int32_t L_2 = Tropism_GetHashCode_m2171971080(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}
	{
		return (bool)0;
	}

IL_0019:
	{
		Il2CppObject * L_3 = ___o0;
		V_0 = ((*(Tropism_t3662836552 *)((Tropism_t3662836552 *)UnBox (L_3, Tropism_t3662836552_il2cpp_TypeInfo_var))));
		int32_t L_4 = (&V_0)->get_coreceptor_11();
		int32_t L_5 = __this->get_coreceptor_11();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_6 = (&V_0)->get_receptor_10();
		int32_t L_7 = __this->get_receptor_10();
		G_B5_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Tropism_Equals_m1822115980_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	Tropism_t3662836552 * _thisAdjusted = reinterpret_cast<Tropism_t3662836552 *>(__this + 1);
	return Tropism_Equals_m1822115980(_thisAdjusted, ___o0, method);
}
// System.Int32 Tropism::GetHashCode()
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t Tropism_GetHashCode_m2171971080_MetadataUsageId;
extern "C"  int32_t Tropism_GetHashCode_m2171971080 (Tropism_t3662836552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_GetHashCode_m2171971080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tropism_t3662836552  L_0 = (*(Tropism_t3662836552 *)__this);
		Il2CppObject * L_1 = Box(Tropism_t3662836552_il2cpp_TypeInfo_var, &L_0);
		NullCheck((ValueType_t3507792607 *)L_1);
		int32_t L_2 = ValueType_GetHashCode_m3018627007((ValueType_t3507792607 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Tropism_GetHashCode_m2171971080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Tropism_t3662836552 * _thisAdjusted = reinterpret_cast<Tropism_t3662836552 *>(__this + 1);
	return Tropism_GetHashCode_m2171971080(_thisAdjusted, method);
}
// System.Boolean Tropism::invalidHIVType(Tropism/ReceptorType)
extern "C"  bool Tropism_invalidHIVType_m720158264 (Il2CppObject * __this /* static, unused */, int32_t ___r0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___r0;
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Tropism::invalidHIVType(Tropism/CoReceptorType)
extern "C"  bool Tropism_invalidHIVType_m452341038 (Il2CppObject * __this /* static, unused */, int32_t ___c0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___c0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___c0;
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 1;
	}

IL_000d:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Tropism::invalidHIVType(Tropism)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t Tropism_invalidHIVType_m130453117_MetadataUsageId;
extern "C"  bool Tropism_invalidHIVType_m130453117 (Il2CppObject * __this /* static, unused */, Tropism_t3662836552  ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_invalidHIVType_m130453117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (&___t0)->get_receptor_10();
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		bool L_1 = Tropism_invalidHIVType_m720158264(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = (&___t0)->get_coreceptor_11();
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		bool L_3 = Tropism_invalidHIVType_m452341038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 1;
	}

IL_0020:
	{
		return (bool)G_B3_0;
	}
}
// UnityEngine.Color Tropism::DefaultColor(Tropism/ReceptorType)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const MethodInfo* Functions_Random_TisColor_t2020392075_m1668057946_MethodInfo_var;
extern const uint32_t Tropism_DefaultColor_m1098737047_MetadataUsageId;
extern "C"  Color_t2020392075  Tropism_DefaultColor_m1098737047 (Il2CppObject * __this /* static, unused */, int32_t ___r0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_DefaultColor_m1098737047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color__ctor_m3811852957((&V_0), (1.0f), (0.9411765f), (0.0f), /*hidden argument*/NULL);
		int32_t L_0 = ___r0;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		goto IL_0024;
	}

IL_0022:
	{
		Color_t2020392075  L_1 = V_0;
		return L_1;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_2 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_receptorDecoyColorList_8();
		Color_t2020392075  L_3 = Functions_Random_TisColor_t2020392075_m1668057946(NULL /*static, unused*/, L_2, /*hidden argument*/Functions_Random_TisColor_t2020392075_m1668057946_MethodInfo_var);
		return L_3;
	}
}
// UnityEngine.Color Tropism::DefaultColor(Tropism/CoReceptorType)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const MethodInfo* Functions_Random_TisColor_t2020392075_m1668057946_MethodInfo_var;
extern const uint32_t Tropism_DefaultColor_m3873948415_MetadataUsageId;
extern "C"  Color_t2020392075  Tropism_DefaultColor_m3873948415 (Il2CppObject * __this /* static, unused */, int32_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_DefaultColor_m3873948415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color__ctor_m1909920690((&V_0), (0.670588255f), (0.321568638f), (1.0f), (1.0f), /*hidden argument*/NULL);
		int32_t L_0 = ___c0;
		if (L_0 == 0)
		{
			goto IL_0042;
		}
		if (L_0 == 1)
		{
			goto IL_003c;
		}
		if (L_0 == 2)
		{
			goto IL_003a;
		}
		if (L_0 == 3)
		{
			goto IL_004e;
		}
		if (L_0 == 4)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_004e;
	}

IL_003a:
	{
		Color_t2020392075  L_1 = V_0;
		return L_1;
	}

IL_003c:
	{
		Color_t2020392075  L_2 = Color_get_cyan_m2850282709(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0042:
	{
		Color_t2020392075  L_3 = Color_get_clear_m1469108305(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0048:
	{
		Color_t2020392075  L_4 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_4;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_5 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_coreceptorDecoyColorList_9();
		Color_t2020392075  L_6 = Functions_Random_TisColor_t2020392075_m1668057946(NULL /*static, unused*/, L_5, /*hidden argument*/Functions_Random_TisColor_t2020392075_m1668057946_MethodInfo_var);
		return L_6;
	}
}
// Tropism Tropism::Random()
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t Tropism_Random_m466220907_MetadataUsageId;
extern "C"  Tropism_t3662836552  Tropism_Random_m466220907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_Random_m466220907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		ReceptorTypeU5BU5D_t2292174278* L_0 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_TCellReceptor_1();
		CoReceptorTypeU5BU5D_t3312104658* L_1 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_TCellCoreceptor_0();
		Tropism_t3662836552  L_2 = Tropism_Random_m938370599(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Tropism Tropism::Random(Tropism/ReceptorType[],Tropism/CoReceptorType[])
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const MethodInfo* Functions_Random_TisReceptorType_t1016603615_m1423371369_MethodInfo_var;
extern const MethodInfo* Functions_Random_TisCoReceptorType_t161380547_m3024276845_MethodInfo_var;
extern const uint32_t Tropism_Random_m938370599_MetadataUsageId;
extern "C"  Tropism_t3662836552  Tropism_Random_m938370599 (Il2CppObject * __this /* static, unused */, ReceptorTypeU5BU5D_t2292174278* ___r0, CoReceptorTypeU5BU5D_t3312104658* ___c1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_Random_m938370599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CoReceptorTypeU5BU5D_t3312104658* G_B2_0 = NULL;
	CoReceptorTypeU5BU5D_t3312104658* G_B1_0 = NULL;
	Dictionary_2_t1366040584 * G_B3_0 = NULL;
	CoReceptorTypeU5BU5D_t3312104658* G_B3_1 = NULL;
	{
		ReceptorTypeU5BU5D_t2292174278* L_0 = ___r0;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Dictionary_2_t346110204 * L_1 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_receptorProbabilities_4();
		int32_t L_2 = Functions_Random_TisReceptorType_t1016603615_m1423371369(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/Functions_Random_TisReceptorType_t1016603615_m1423371369_MethodInfo_var);
		V_0 = L_2;
		CoReceptorTypeU5BU5D_t3312104658* L_3 = ___c1;
		int32_t L_4 = V_0;
		G_B1_0 = L_3;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			G_B2_0 = L_3;
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Dictionary_2_t1366040584 * L_5 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_cd4CoReceptorProbabilities_6();
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		Dictionary_2_t1366040584 * L_6 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_coReceptorProbabilities_5();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		int32_t L_7 = Functions_Random_TisCoReceptorType_t161380547_m3024276845(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/Functions_Random_TisCoReceptorType_t161380547_m3024276845_MethodInfo_var);
		V_1 = L_7;
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		Tropism_t3662836552  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Tropism__ctor_m2978840885(&L_10, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// Tropism Tropism::HIVRandom()
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const MethodInfo* Functions_Random_TisCoReceptorType_t161380547_m3024276845_MethodInfo_var;
extern const uint32_t Tropism_HIVRandom_m3105810886_MetadataUsageId;
extern "C"  Tropism_t3662836552  Tropism_HIVRandom_m3105810886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_HIVRandom_m3105810886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		ReceptorTypeU5BU5D_t2292174278* L_0 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_HIVReceptor_3();
		NullCheck(L_0);
		int32_t L_1 = 0;
		int32_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		CoReceptorTypeU5BU5D_t3312104658* L_3 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_HIVCoreceptor_2();
		Dictionary_2_t1366040584 * L_4 = ((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->get_hivProbabilities_7();
		int32_t L_5 = Functions_Random_TisCoReceptorType_t161380547_m3024276845(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/Functions_Random_TisCoReceptorType_t161380547_m3024276845_MethodInfo_var);
		Tropism_t3662836552  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Tropism__ctor_m2978840885(&L_6, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Tropism::op_Equality(Tropism,Tropism)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t Tropism_op_Equality_m4254594981_MetadataUsageId;
extern "C"  bool Tropism_op_Equality_m4254594981 (Il2CppObject * __this /* static, unused */, Tropism_t3662836552  ___a0, Tropism_t3662836552  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_op_Equality_m4254594981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tropism_t3662836552  L_0 = ___b1;
		Tropism_t3662836552  L_1 = L_0;
		Il2CppObject * L_2 = Box(Tropism_t3662836552_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = Tropism_Equals_m1822115980((&___a0), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Tropism::op_Inequality(Tropism,Tropism)
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern const uint32_t Tropism_op_Inequality_m1143736396_MetadataUsageId;
extern "C"  bool Tropism_op_Inequality_m1143736396 (Il2CppObject * __this /* static, unused */, Tropism_t3662836552  ___a0, Tropism_t3662836552  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism_op_Inequality_m1143736396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tropism_t3662836552  L_0 = ___a0;
		Tropism_t3662836552  L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Tropism_t3662836552_il2cpp_TypeInfo_var);
		bool L_2 = Tropism_op_Equality_m4254594981(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Tropism::.cctor()
extern Il2CppClass* CoReceptorTypeU5BU5D_t3312104658_il2cpp_TypeInfo_var;
extern Il2CppClass* Tropism_t3662836552_il2cpp_TypeInfo_var;
extern Il2CppClass* ReceptorTypeU5BU5D_t2292174278_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t346110204_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1366040584_il2cpp_TypeInfo_var;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3169554460_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2398760724_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m547845402_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m245902210_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1_FieldInfo_var;
extern const uint32_t Tropism__cctor_m319241906_MetadataUsageId;
extern "C"  void Tropism__cctor_m319241906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tropism__cctor_m319241906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t346110204 * V_0 = NULL;
	Dictionary_2_t1366040584 * V_1 = NULL;
	{
		CoReceptorTypeU5BU5D_t3312104658* L_0 = ((CoReceptorTypeU5BU5D_t3312104658*)SZArrayNew(CoReceptorTypeU5BU5D_t3312104658_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D02E4414E7DFA0F3AA2387EE8EA7AB31431CB406A_0_FieldInfo_var), /*hidden argument*/NULL);
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_TCellCoreceptor_0(L_0);
		ReceptorTypeU5BU5D_t2292174278* L_1 = ((ReceptorTypeU5BU5D_t2292174278*)SZArrayNew(ReceptorTypeU5BU5D_t2292174278_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)1);
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_TCellReceptor_1(L_1);
		CoReceptorTypeU5BU5D_t3312104658* L_2 = ((CoReceptorTypeU5BU5D_t3312104658*)SZArrayNew(CoReceptorTypeU5BU5D_t3312104658_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D8D00B2A3DF0C96F058EBB780CEF9CD8B3C51EB18_1_FieldInfo_var), /*hidden argument*/NULL);
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_HIVCoreceptor_2(L_2);
		ReceptorTypeU5BU5D_t2292174278* L_3 = ((ReceptorTypeU5BU5D_t2292174278*)SZArrayNew(ReceptorTypeU5BU5D_t2292174278_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)1);
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_HIVReceptor_3(L_3);
		Dictionary_2_t346110204 * L_4 = (Dictionary_2_t346110204 *)il2cpp_codegen_object_new(Dictionary_2_t346110204_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3169554460(L_4, /*hidden argument*/Dictionary_2__ctor_m3169554460_MethodInfo_var);
		V_0 = L_4;
		Dictionary_2_t346110204 * L_5 = V_0;
		NullCheck(L_5);
		Dictionary_2_Add_m2398760724(L_5, 0, (0.6f), /*hidden argument*/Dictionary_2_Add_m2398760724_MethodInfo_var);
		Dictionary_2_t346110204 * L_6 = V_0;
		NullCheck(L_6);
		Dictionary_2_Add_m2398760724(L_6, 1, (0.4f), /*hidden argument*/Dictionary_2_Add_m2398760724_MethodInfo_var);
		Dictionary_2_t346110204 * L_7 = V_0;
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_receptorProbabilities_4(L_7);
		Dictionary_2_t1366040584 * L_8 = (Dictionary_2_t1366040584 *)il2cpp_codegen_object_new(Dictionary_2_t1366040584_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m547845402(L_8, /*hidden argument*/Dictionary_2__ctor_m547845402_MethodInfo_var);
		V_1 = L_8;
		Dictionary_2_t1366040584 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m245902210(L_9, 1, (0.275f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_10 = V_1;
		NullCheck(L_10);
		Dictionary_2_Add_m245902210(L_10, 2, (0.2f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_11 = V_1;
		NullCheck(L_11);
		Dictionary_2_Add_m245902210(L_11, 0, (0.2f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_12 = V_1;
		NullCheck(L_12);
		Dictionary_2_Add_m245902210(L_12, 3, (0.325f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_13 = V_1;
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_coReceptorProbabilities_5(L_13);
		Dictionary_2_t1366040584 * L_14 = (Dictionary_2_t1366040584 *)il2cpp_codegen_object_new(Dictionary_2_t1366040584_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m547845402(L_14, /*hidden argument*/Dictionary_2__ctor_m547845402_MethodInfo_var);
		V_1 = L_14;
		Dictionary_2_t1366040584 * L_15 = V_1;
		NullCheck(L_15);
		Dictionary_2_Add_m245902210(L_15, 1, (0.425f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_16 = V_1;
		NullCheck(L_16);
		Dictionary_2_Add_m245902210(L_16, 2, (0.25f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_17 = V_1;
		NullCheck(L_17);
		Dictionary_2_Add_m245902210(L_17, 0, (0.2f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_18 = V_1;
		NullCheck(L_18);
		Dictionary_2_Add_m245902210(L_18, 3, (0.125f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_19 = V_1;
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_cd4CoReceptorProbabilities_6(L_19);
		Dictionary_2_t1366040584 * L_20 = (Dictionary_2_t1366040584 *)il2cpp_codegen_object_new(Dictionary_2_t1366040584_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m547845402(L_20, /*hidden argument*/Dictionary_2__ctor_m547845402_MethodInfo_var);
		V_1 = L_20;
		Dictionary_2_t1366040584 * L_21 = V_1;
		NullCheck(L_21);
		Dictionary_2_Add_m245902210(L_21, 1, (0.55f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_22 = V_1;
		NullCheck(L_22);
		Dictionary_2_Add_m245902210(L_22, 2, (0.4f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_23 = V_1;
		NullCheck(L_23);
		Dictionary_2_Add_m245902210(L_23, 4, (0.05f), /*hidden argument*/Dictionary_2_Add_m245902210_MethodInfo_var);
		Dictionary_2_t1366040584 * L_24 = V_1;
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_hivProbabilities_7(L_24);
		ColorU5BU5D_t672350442* L_25 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_25);
		Color_t2020392075  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Color__ctor_m3811852957(&L_26, (0.6117647f), (0.7647059f), (0.8980392f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_26;
		ColorU5BU5D_t672350442* L_27 = L_25;
		NullCheck(L_27);
		Color_t2020392075  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Color__ctor_m3811852957(&L_28, (0.7490196f), (0.7490196f), (0.7490196f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_28;
		ColorU5BU5D_t672350442* L_29 = L_27;
		NullCheck(L_29);
		Color_t2020392075  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Color__ctor_m3811852957(&L_30, (0.6627451f), (0.8235294f), (0.56078434f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_30;
		ColorU5BU5D_t672350442* L_31 = L_29;
		NullCheck(L_31);
		Color_t2020392075  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Color__ctor_m3811852957(&L_32, (0.8784314f), (0.694117665f), (0.5137255f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_32;
		ColorU5BU5D_t672350442* L_33 = L_31;
		NullCheck(L_33);
		Color_t2020392075  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Color__ctor_m3811852957(&L_34, (0.5647059f), (0.572549045f), (0.7529412f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))) = L_34;
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_receptorDecoyColorList_8(L_33);
		ColorU5BU5D_t672350442* L_35 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_35);
		Color_t2020392075  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Color__ctor_m3811852957(&L_36, (0.9372549f), (0.917647064f), (0.917647064f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_36;
		ColorU5BU5D_t672350442* L_37 = L_35;
		NullCheck(L_37);
		Color_t2020392075  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Color__ctor_m3811852957(&L_38, (0.784313738f), (0.141176477f), (0.113725491f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_38;
		ColorU5BU5D_t672350442* L_39 = L_37;
		NullCheck(L_39);
		Color_t2020392075  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Color__ctor_m3811852957(&L_40, (0.003921569f), (0.6901961f), (0.3137255f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_40;
		((Tropism_t3662836552_StaticFields*)Tropism_t3662836552_il2cpp_TypeInfo_var->static_fields)->set_coreceptorDecoyColorList_9(L_39);
		return;
	}
}
// System.Void Tutorial::.ctor()
extern "C"  void Tutorial__ctor_m3772536979 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tutorial::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t Tutorial_Awake_m1360923614_MetadataUsageId;
extern "C"  void Tutorial_Awake_m1360923614 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tutorial_Awake_m1360923614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_anim_2(L_0);
		return;
	}
}
// System.Void Tutorial::OnValidate()
extern "C"  void Tutorial_OnValidate_m950451526 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_next_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Tutorial_NextFrame_m1900467925(__this, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0016:
	{
		bool L_1 = __this->get_prev_5();
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Tutorial_PreviousFrame_m327594487(__this, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_002c:
	{
		bool L_2 = __this->get_main_6();
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		Tutorial_ToMainMenu_m2698963726(__this, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Tutorial::NextFrame()
extern Il2CppCodeGenString* _stringLiteral3423761239;
extern const uint32_t Tutorial_NextFrame_m1900467925_MetadataUsageId;
extern "C"  void Tutorial_NextFrame_m1900467925 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tutorial_NextFrame_m1900467925_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_next_4((bool)0);
		Animator_t69676727 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral3423761239, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tutorial::PreviousFrame()
extern Il2CppCodeGenString* _stringLiteral1103215357;
extern const uint32_t Tutorial_PreviousFrame_m327594487_MetadataUsageId;
extern "C"  void Tutorial_PreviousFrame_m327594487 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tutorial_PreviousFrame_m327594487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_prev_5((bool)0);
		Animator_t69676727 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral1103215357, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tutorial::ToMainMenu()
extern Il2CppCodeGenString* _stringLiteral483456507;
extern const uint32_t Tutorial_ToMainMenu_m2698963726_MetadataUsageId;
extern "C"  void Tutorial_ToMainMenu_m2698963726 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tutorial_ToMainMenu_m2698963726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_main_6((bool)0);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral483456507, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Tutorial::ToGame()
extern "C"  void Tutorial_ToGame_m2662689906 (Tutorial_t735545730 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_gameSceneName_3();
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Background::.ctor()
extern "C"  void UI_Background__ctor_m1698530442 (UI_Background_t3920117671 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Image UI_Background::get_image()
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const uint32_t UI_Background_get_image_m339045305_MetadataUsageId;
extern "C"  Image_t2042527209 * UI_Background_get_image_m339045305 (UI_Background_t3920117671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Background_get_image_m339045305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		return L_0;
	}
}
// System.Void UI_Background::Start()
extern "C"  void UI_Background_Start_m408145390 (UI_Background_t3920117671 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = UI_Background_get_image_m339045305(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Background::OnGUI()
extern "C"  void UI_Background_OnGUI_m396782758 (UI_Background_t3920117671 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = UI_Background_get_image_m339045305(__this, /*hidden argument*/NULL);
		float L_1 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)((((float)L_1) == ((float)(0.0f)))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Bar::.ctor()
extern "C"  void UI_Bar__ctor_m2477021189 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Image UI_Bar::get_image()
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const uint32_t UI_Bar_get_image_m1309572970_MetadataUsageId;
extern "C"  Image_t2042527209 * UI_Bar_get_image_m1309572970 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Bar_get_image_m1309572970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_t2042527209 * L_1 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		return L_1;
	}
}
// GameController UI_Bar::get_controller()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern const uint32_t UI_Bar_get_controller_m3554715213_MetadataUsageId;
extern "C"  GameController_t3607102586 * UI_Bar_get_controller_m3554715213 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Bar_get_controller_m3554715213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_0);
		GameController_t3607102586 * L_1 = Game_get_controller_m2668402245(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HIV UI_Bar::get_selected()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UI_Bar_get_selected_m518456309_MetadataUsageId;
extern "C"  HIV_t2481767745 * UI_Bar_get_selected_m518456309 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Bar_get_selected_m518456309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HIV_t2481767745 * G_B3_0 = NULL;
	{
		GameController_t3607102586 * L_0 = UI_Bar_get_controller_m3554715213(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		GameController_t3607102586 * L_2 = UI_Bar_get_controller_m3554715213(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		HIV_t2481767745 * L_3 = GameController_get_selected_m2687499813(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0021;
	}

IL_0020:
	{
		G_B3_0 = ((HIV_t2481767745 *)(NULL));
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Void UI_Bar::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UI_Bar_Update_m3852447278_MetadataUsageId;
extern "C"  void UI_Bar_Update_m3852447278 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Bar_Update_m3852447278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = UI_Bar_get_selected_m518456309(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		HIV_t2481767745 * L_2 = __this->get__selected_2();
		HIV_t2481767745 * L_3 = UI_Bar_get_selected_m518456309(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		HIV_t2481767745 * L_5 = UI_Bar_get_selected_m518456309(__this, /*hidden argument*/NULL);
		__this->set__selected_2(L_5);
	}

IL_0032:
	{
		HIV_t2481767745 * L_6 = __this->get__selected_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006f;
		}
	}
	{
		HIV_t2481767745 * L_8 = __this->get__selected_2();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = GameObject_get_activeSelf_m313590879(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0068;
		}
	}
	{
		HIV_t2481767745 * L_11 = __this->get__selected_2();
		NullCheck(L_11);
		int32_t L_12 = HIV_get_state_m3030343602(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_006f;
		}
	}

IL_0068:
	{
		__this->set__selected_2((HIV_t2481767745 *)NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void UI_Bar::OnGUI()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UI_Bar_OnGUI_m1300265943_MetadataUsageId;
extern "C"  void UI_Bar_OnGUI_m1300265943 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Bar_OnGUI_m1300265943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2042527209 * G_B2_0 = NULL;
	Image_t2042527209 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Image_t2042527209 * G_B3_1 = NULL;
	{
		Image_t2042527209 * L_0 = UI_Bar_get_image_m1309572970(__this, /*hidden argument*/NULL);
		HIV_t2481767745 * L_1 = __this->get__selected_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_002b;
		}
	}
	{
		HIV_t2481767745 * L_3 = __this->get__selected_2();
		NullCheck(L_3);
		HIVStats_t2521285894 * L_4 = HIV_get_stats_m4074649537(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = HIVStats_get_hp_m2274879970(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_002b:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		NullCheck(G_B3_1);
		Image_set_fillAmount_m2220966753(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		Image_t2042527209 * L_6 = UI_Bar_get_image_m1309572970(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = UI_Bar_GetColor_m1110270239(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		return;
	}
}
// UnityEngine.Color UI_Bar::GetColor()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UI_Bar_GetColor_m1110270239_MetadataUsageId;
extern "C"  Color_t2020392075  UI_Bar_GetColor_m1110270239 (UI_Bar_t1358396100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Bar_GetColor_m1110270239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		V_2 = (0.0f);
		Image_t2042527209 * L_0 = UI_Bar_get_image_m1309572970(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = Image_get_fillAmount_m3354146540(L_0, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_1)))) > ((double)(0.5)))))
		{
			goto IL_004d;
		}
	}
	{
		Image_t2042527209 * L_2 = UI_Bar_get_image_m1309572970(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_3 = Image_get_fillAmount_m3354146540(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)(2.0f)-(float)((float)((float)(2.0f)*(float)L_3)))), (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = (1.0f);
		goto IL_006f;
	}

IL_004d:
	{
		V_0 = (1.0f);
		Image_t2042527209 * L_5 = UI_Bar_get_image_m1309572970(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		float L_6 = Image_get_fillAmount_m3354146540(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)(2.0f)*(float)L_6)), (0.0f), /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_006f:
	{
		float L_8 = V_0;
		float L_9 = V_1;
		float L_10 = V_2;
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m3811852957(&L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void UI_Capsid::.ctor()
extern "C"  void UI_Capsid__ctor_m2497196506 (UI_Capsid_t4027023025 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Developer_Button::.ctor()
extern "C"  void UI_Developer_Button__ctor_m2550579923 (UI_Developer_Button_t3399765548 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Developer_Button::Toggle()
extern "C"  void UI_Developer_Button_Toggle_m730574041 (UI_Developer_Button_t3399765548 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_stats_2();
		GameObject_t1756533147 * L_1 = __this->get_stats_2();
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeSelf_m313590879(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Developer_Stats::.ctor()
extern "C"  void UI_Developer_Stats__ctor_m3041775346 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Developer_Stats::OnGUI()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const uint32_t UI_Developer_Stats_OnGUI_m1670899234_MetadataUsageId;
extern "C"  void UI_Developer_Stats_OnGUI_m1670899234 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Developer_Stats_OnGUI_m1670899234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		V_0 = L_0;
		Text_t356221433 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		Text_t356221433 * L_3 = V_0;
		Text_t356221433 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_4);
		String_t* L_6 = UI_Developer_Stats_FPSCounter_m342561264(__this, /*hidden argument*/NULL);
		Il2CppChar L_7 = ((Il2CppChar)((int32_t)10));
		Il2CppObject * L_8 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Concat_m2000667605(NULL /*static, unused*/, L_5, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_9);
		Text_t356221433 * L_10 = V_0;
		Text_t356221433 * L_11 = V_0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_11);
		String_t* L_13 = UI_Developer_Stats_GetPopulation_m1501463094(__this, /*hidden argument*/NULL);
		Il2CppChar L_14 = ((Il2CppChar)((int32_t)10));
		Il2CppObject * L_15 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_14);
		String_t* L_16 = String_Concat_m2000667605(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_16);
		Text_t356221433 * L_17 = V_0;
		Text_t356221433 * L_18 = L_17;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_18);
		String_t* L_20 = UI_Developer_Stats_GetTCellPopulation_m1755623494(__this, /*hidden argument*/NULL);
		Il2CppChar L_21 = ((Il2CppChar)((int32_t)10));
		Il2CppObject * L_22 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_21);
		String_t* L_23 = String_Concat_m2000667605(NULL /*static, unused*/, L_19, L_20, L_22, /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_23);
		Text_t356221433 * L_24 = V_0;
		Text_t356221433 * L_25 = L_24;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_25);
		String_t* L_27 = UI_Developer_Stats_GetRBCPopulation_m1013808615(__this, /*hidden argument*/NULL);
		String_t* L_28 = String_Concat_m2596409543(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_28);
		return;
	}
}
// System.String UI_Developer_Stats::GetRBCPopulation()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4036799276;
extern const uint32_t UI_Developer_Stats_GetRBCPopulation_m1013808615_MetadataUsageId;
extern "C"  String_t* UI_Developer_Stats_GetRBCPopulation_m1013808615 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Developer_Stats_GetRBCPopulation_m1013808615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_2);
		List_1_t1125654279 * L_3 = Game_get_redBloodCellList_m3132366318(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m2764296230(L_3, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		V_0 = L_4;
		String_t* L_5 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4036799276, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}
}
// System.String UI_Developer_Stats::GetTCellPopulation()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m2764296230_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4267812579;
extern const uint32_t UI_Developer_Stats_GetTCellPopulation_m1755623494_MetadataUsageId;
extern "C"  String_t* UI_Developer_Stats_GetTCellPopulation_m1755623494 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Developer_Stats_GetTCellPopulation_m1755623494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_2);
		List_1_t1125654279 * L_3 = Game_get_tCellList_m3565045317(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = List_1_get_Count_m2764296230(L_3, /*hidden argument*/List_1_get_Count_m2764296230_MethodInfo_var);
		V_0 = L_4;
		String_t* L_5 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4267812579, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}
}
// System.String UI_Developer_Stats::GetPopulation()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2670229093;
extern const uint32_t UI_Developer_Stats_GetPopulation_m1501463094_MetadataUsageId;
extern "C"  String_t* UI_Developer_Stats_GetPopulation_m1501463094 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Developer_Stats_GetPopulation_m1501463094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_2);
		int32_t L_3 = Game_get_population_m752974705(L_2, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2670229093, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}
}
// System.String UI_Developer_Stats::FPSCounter()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1884668475;
extern const uint32_t UI_Developer_Stats_FPSCounter_m342561264_MetadataUsageId;
extern "C"  String_t* UI_Developer_Stats_FPSCounter_m342561264 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Developer_Stats_FPSCounter_m342561264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = UI_Developer_Stats_GetFPS_m346371025(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Single_ToString_m1813392066((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1884668475, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UI_Developer_Stats::GetFPS()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UI_Developer_Stats_GetFPS_m346371025_MetadataUsageId;
extern "C"  float UI_Developer_Stats_GetFPS_m346371025 (UI_Developer_Stats_t2682907225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Developer_Stats_GetFPS_m346371025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = bankers_roundf(((float)((float)((float)((float)(1.0f)/(float)L_0))*(float)(100.0f))));
		return ((float)((float)L_1/(float)(100.0f)));
	}
}
// System.Void UI_GP120::.ctor()
extern "C"  void UI_GP120__ctor_m1982076012 (UI_GP120_t1483396489 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UI_GP120::get_color()
extern "C"  Color_t2020392075  UI_GP120_get_color_m2651926781 (UI_GP120_t1483396489 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_displayLeft_2();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void UI_GP120::set_color(UnityEngine.Color)
extern "C"  void UI_GP120_set_color_m1014429084 (UI_GP120_t1483396489 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_displayLeft_2();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		Image_t2042527209 * L_2 = __this->get_displayRight_3();
		Color_t2020392075  L_3 = ___value0;
		NullCheck(L_2);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_2, L_3);
		return;
	}
}
// UnityEngine.Color UI_GP120::get_colorLeft()
extern "C"  Color_t2020392075  UI_GP120_get_colorLeft_m2212695404 (UI_GP120_t1483396489 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_displayLeft_2();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void UI_GP120::set_colorLeft(UnityEngine.Color)
extern "C"  void UI_GP120_set_colorLeft_m1174730493 (UI_GP120_t1483396489 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_displayLeft_2();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// UnityEngine.Color UI_GP120::get_colorRight()
extern "C"  Color_t2020392075  UI_GP120_get_colorRight_m2617596573 (UI_GP120_t1483396489 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_displayRight_3();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void UI_GP120::set_colorRight(UnityEngine.Color)
extern "C"  void UI_GP120_set_colorRight_m370336612 (UI_GP120_t1483396489 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_displayRight_3();
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void UI_GP41::.ctor()
extern "C"  void UI_GP41__ctor_m2264483998 (UI_GP41_t2232403251 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UI_GP41::get_color()
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const uint32_t UI_GP41_get_color_m360088967_MetadataUsageId;
extern "C"  Color_t2020392075  UI_GP41_get_color_m360088967 (UI_GP41_t2232403251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_GP41_get_color_m360088967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void UI_GP41::set_color(UnityEngine.Color)
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const uint32_t UI_GP41_set_color_m896254274_MetadataUsageId;
extern "C"  void UI_GP41_set_color_m896254274 (UI_GP41_t2232403251 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_GP41_set_color_m896254274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Color_t2020392075  L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void UI_HIV::.ctor()
extern "C"  void UI_HIV__ctor_m539927275 (UI_HIV_t1412098442 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// HIV UI_HIV::get_selected()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern const uint32_t UI_HIV_get_selected_m3295493963_MetadataUsageId;
extern "C"  HIV_t2481767745 * UI_HIV_get_selected_m3295493963 (UI_HIV_t1412098442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV_get_selected_m3295493963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_0);
		GameController_t3607102586 * L_1 = Game_get_controller_m2668402245(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		HIV_t2481767745 * L_2 = GameController_get_selected_m2687499813(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UI_HIV::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UI_HIV_Update_m3889700724_MetadataUsageId;
extern "C"  void UI_HIV_Update_m3889700724 (UI_HIV_t1412098442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV_Update_m3889700724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HIV_t2481767745 * L_0 = UI_HIV_get_selected_m3295493963(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		HIV_t2481767745 * L_2 = __this->get__selected_2();
		HIV_t2481767745 * L_3 = UI_HIV_get_selected_m3295493963(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		HIV_t2481767745 * L_5 = UI_HIV_get_selected_m3295493963(__this, /*hidden argument*/NULL);
		__this->set__selected_2(L_5);
	}

IL_0032:
	{
		HIV_t2481767745 * L_6 = __this->get__selected_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006f;
		}
	}
	{
		HIV_t2481767745 * L_8 = __this->get__selected_2();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = GameObject_get_activeSelf_m313590879(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0068;
		}
	}
	{
		HIV_t2481767745 * L_11 = __this->get__selected_2();
		NullCheck(L_11);
		int32_t L_12 = HIV_get_state_m3030343602(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_006f;
		}
	}

IL_0068:
	{
		__this->set__selected_2((HIV_t2481767745 *)NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void UI_HIV::OnGUI()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UI_HIV_t1412098442_il2cpp_TypeInfo_var;
extern const uint32_t UI_HIV_OnGUI_m448153665_MetadataUsageId;
extern "C"  void UI_HIV_OnGUI_m448153665 (UI_HIV_t1412098442 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV_OnGUI_m448153665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2042527209 * V_0 = NULL;
	ImageU5BU5D_t590162004* V_1 = NULL;
	int32_t V_2 = 0;
	Image_t2042527209 * V_3 = NULL;
	ImageU5BU5D_t590162004* V_4 = NULL;
	int32_t V_5 = 0;
	Image_t2042527209 * V_6 = NULL;
	ImageU5BU5D_t590162004* V_7 = NULL;
	int32_t V_8 = 0;
	Image_t2042527209 * V_9 = NULL;
	ImageU5BU5D_t590162004* V_10 = NULL;
	int32_t V_11 = 0;
	{
		HIV_t2481767745 * L_0 = __this->get__selected_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f0;
		}
	}
	{
		Image_t2042527209 * L_2 = __this->get_capsid_3();
		HIV_t2481767745 * L_3 = __this->get__selected_2();
		NullCheck(L_3);
		Color_t2020392075  L_4 = HIV_get_capsidColor_m4139347341(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_2, L_4);
		Image_t2042527209 * L_5 = __this->get_membrane_4();
		HIV_t2481767745 * L_6 = __this->get__selected_2();
		NullCheck(L_6);
		Color_t2020392075  L_7 = HIV_get_membraneColor_m1343741098(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_7);
		Image_t2042527209 * L_8 = __this->get_matrix_5();
		HIV_t2481767745 * L_9 = __this->get__selected_2();
		NullCheck(L_9);
		Color_t2020392075  L_10 = HIV_get_matrixColor_m1334576742(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_10);
		ImageU5BU5D_t590162004* L_11 = __this->get_rna_6();
		V_1 = L_11;
		V_2 = 0;
		goto IL_0079;
	}

IL_0060:
	{
		ImageU5BU5D_t590162004* L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Image_t2042527209 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_0 = L_15;
		Image_t2042527209 * L_16 = V_0;
		HIV_t2481767745 * L_17 = __this->get__selected_2();
		NullCheck(L_17);
		Color_t2020392075  L_18 = HIV_get_rnaColor_m3258134330(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_16, L_18);
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_20 = V_2;
		ImageU5BU5D_t590162004* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0060;
		}
	}
	{
		ImageU5BU5D_t590162004* L_22 = __this->get_rt_7();
		V_4 = L_22;
		V_5 = 0;
		goto IL_00af;
	}

IL_0092:
	{
		ImageU5BU5D_t590162004* L_23 = V_4;
		int32_t L_24 = V_5;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		Image_t2042527209 * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_3 = L_26;
		Image_t2042527209 * L_27 = V_3;
		HIV_t2481767745 * L_28 = __this->get__selected_2();
		NullCheck(L_28);
		Color_t2020392075  L_29 = HIV_get_rtColor_m3140351811(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_27, L_29);
		int32_t L_30 = V_5;
		V_5 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00af:
	{
		int32_t L_31 = V_5;
		ImageU5BU5D_t590162004* L_32 = V_4;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
		{
			goto IL_0092;
		}
	}
	{
		UI_SpikeList_t1985653691 * L_33 = __this->get_spikeList_8();
		HIV_t2481767745 * L_34 = __this->get__selected_2();
		NullCheck(L_34);
		Color_t2020392075  L_35 = HIV_get_GP41_Color_m4220143536(L_34, /*hidden argument*/NULL);
		HIV_t2481767745 * L_36 = __this->get__selected_2();
		NullCheck(L_36);
		Color_t2020392075  L_37 = HIV_get_GP120_Color_Left_m1171295536(L_36, /*hidden argument*/NULL);
		HIV_t2481767745 * L_38 = __this->get__selected_2();
		NullCheck(L_38);
		Color_t2020392075  L_39 = HIV_get_GP120_Color_Right_m1095216545(L_38, /*hidden argument*/NULL);
		SpikeColor_t3573234163  L_40;
		memset(&L_40, 0, sizeof(L_40));
		SpikeColor__ctor_m2112352072(&L_40, L_35, L_37, L_39, /*hidden argument*/NULL);
		NullCheck(L_33);
		UI_SpikeList_set_colors_m3164100610(L_33, L_40, /*hidden argument*/NULL);
		goto IL_01a7;
	}

IL_00f0:
	{
		Image_t2042527209 * L_41 = __this->get_capsid_3();
		IL2CPP_RUNTIME_CLASS_INIT(UI_HIV_t1412098442_il2cpp_TypeInfo_var);
		Color_t2020392075  L_42 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		NullCheck(L_41);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_41, L_42);
		Image_t2042527209 * L_43 = __this->get_membrane_4();
		Color_t2020392075  L_44 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		NullCheck(L_43);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_43, L_44);
		Image_t2042527209 * L_45 = __this->get_matrix_5();
		Color_t2020392075  L_46 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		NullCheck(L_45);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_45, L_46);
		ImageU5BU5D_t590162004* L_47 = __this->get_rna_6();
		V_7 = L_47;
		V_8 = 0;
		goto IL_0149;
	}

IL_0130:
	{
		ImageU5BU5D_t590162004* L_48 = V_7;
		int32_t L_49 = V_8;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		Image_t2042527209 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		V_6 = L_51;
		Image_t2042527209 * L_52 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(UI_HIV_t1412098442_il2cpp_TypeInfo_var);
		Color_t2020392075  L_53 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		NullCheck(L_52);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_52, L_53);
		int32_t L_54 = V_8;
		V_8 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0149:
	{
		int32_t L_55 = V_8;
		ImageU5BU5D_t590162004* L_56 = V_7;
		NullCheck(L_56);
		if ((((int32_t)L_55) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length)))))))
		{
			goto IL_0130;
		}
	}
	{
		ImageU5BU5D_t590162004* L_57 = __this->get_rt_7();
		V_10 = L_57;
		V_11 = 0;
		goto IL_017d;
	}

IL_0164:
	{
		ImageU5BU5D_t590162004* L_58 = V_10;
		int32_t L_59 = V_11;
		NullCheck(L_58);
		int32_t L_60 = L_59;
		Image_t2042527209 * L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		V_9 = L_61;
		Image_t2042527209 * L_62 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(UI_HIV_t1412098442_il2cpp_TypeInfo_var);
		Color_t2020392075  L_63 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		NullCheck(L_62);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_62, L_63);
		int32_t L_64 = V_11;
		V_11 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_017d:
	{
		int32_t L_65 = V_11;
		ImageU5BU5D_t590162004* L_66 = V_10;
		NullCheck(L_66);
		if ((((int32_t)L_65) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_66)->max_length)))))))
		{
			goto IL_0164;
		}
	}
	{
		UI_SpikeList_t1985653691 * L_67 = __this->get_spikeList_8();
		IL2CPP_RUNTIME_CLASS_INIT(UI_HIV_t1412098442_il2cpp_TypeInfo_var);
		Color_t2020392075  L_68 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		Color_t2020392075  L_69 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		Color_t2020392075  L_70 = ((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->get_defaultColor_9();
		SpikeColor_t3573234163  L_71;
		memset(&L_71, 0, sizeof(L_71));
		SpikeColor__ctor_m2112352072(&L_71, L_68, L_69, L_70, /*hidden argument*/NULL);
		NullCheck(L_67);
		UI_SpikeList_set_colors_m3164100610(L_67, L_71, /*hidden argument*/NULL);
	}

IL_01a7:
	{
		return;
	}
}
// System.Void UI_HIV::.cctor()
extern Il2CppClass* UI_HIV_t1412098442_il2cpp_TypeInfo_var;
extern const uint32_t UI_HIV__cctor_m2382461372_MetadataUsageId;
extern "C"  void UI_HIV__cctor_m2382461372 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV__cctor_m2382461372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3811852957(&L_0, (0.75f), (0.75f), (0.75f), /*hidden argument*/NULL);
		((UI_HIV_t1412098442_StaticFields*)UI_HIV_t1412098442_il2cpp_TypeInfo_var->static_fields)->set_defaultColor_9(L_0);
		return;
	}
}
// System.Void UI_HIV_Button::.ctor()
extern "C"  void UI_HIV_Button__ctor_m2573358074 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method)
{
	{
		__this->set_ready_4((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_HIV_Button::Awake()
extern Il2CppClass* UI_HIV_Button_t3180703511_il2cpp_TypeInfo_var;
extern const uint32_t UI_HIV_Button_Awake_m1137632259_MetadataUsageId;
extern "C"  void UI_HIV_Button_Awake_m1137632259 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV_Button_Awake_m1137632259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UI_HIV_Button_t3180703511_StaticFields*)UI_HIV_Button_t3180703511_il2cpp_TypeInfo_var->static_fields)->set_main_5(__this);
		return;
	}
}
// System.Void UI_HIV_Button::Start()
extern "C"  void UI_HIV_Button_Start_m1282947550 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method)
{
	{
		UI_HIV_Button_Activate_m2168596695(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_HIV_Button::Activate()
extern Il2CppCodeGenString* _stringLiteral2458609833;
extern const uint32_t UI_HIV_Button_Activate_m2168596695_MetadataUsageId;
extern "C"  void UI_HIV_Button_Activate_m2168596695 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV_Button_Activate_m2168596695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_ready_4();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Animator_t69676727 * L_1 = __this->get_anim_2();
		NullCheck(L_1);
		Animator_SetTrigger_m3418492570(L_1, _stringLiteral2458609833, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UI_HIV_Button::Deactivate()
extern Il2CppCodeGenString* _stringLiteral2217968412;
extern const uint32_t UI_HIV_Button_Deactivate_m2023060262_MetadataUsageId;
extern "C"  void UI_HIV_Button_Deactivate_m2023060262 (UI_HIV_Button_t3180703511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_HIV_Button_Deactivate_m2023060262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = __this->get_anim_2();
		NullCheck(L_0);
		Animator_SetTrigger_m3418492570(L_0, _stringLiteral2217968412, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Immunogenicity::.ctor()
extern "C"  void UI_Immunogenicity__ctor_m495285335 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UI_Immunogenicity::get_percentage()
extern "C"  float UI_Immunogenicity_get_percentage_m2444515390 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_progress_7();
		float L_1 = __this->get_maxProgress_8();
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void UI_Immunogenicity::ChangeState(UI_Immunogenicity/States)
extern "C"  void UI_Immunogenicity_ChangeState_m614690109 (UI_Immunogenicity_t1529844486 * __this, int32_t ___s0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___s0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000c;
		}
	}
	{
		goto IL_001c;
	}

IL_000c:
	{
		__this->set_fillIntended_10((0.0f));
		goto IL_001c;
	}

IL_001c:
	{
		int32_t L_1 = ___s0;
		__this->set_state_9(L_1);
		return;
	}
}
// System.Void UI_Immunogenicity::Add(System.Single)
extern "C"  void UI_Immunogenicity_Add_m607723539 (UI_Immunogenicity_t1529844486 * __this, float ___amount0, const MethodInfo* method)
{
	{
		float L_0 = __this->get_progress_7();
		float L_1 = ___amount0;
		__this->set_progress_7(((float)((float)L_0+(float)L_1)));
		float L_2 = UI_Immunogenicity_get_percentage_m2444515390(__this, /*hidden argument*/NULL);
		__this->set_fillIntended_10(L_2);
		int32_t L_3 = __this->get_state_9();
		if ((((int32_t)L_3) == ((int32_t)2)))
		{
			goto IL_002d;
		}
	}
	{
		UI_Immunogenicity_ChangeState_m614690109(__this, 0, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UI_Immunogenicity::RefreshMaximum()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UI_Immunogenicity_RefreshMaximum_m2251339466_MetadataUsageId;
extern "C"  void UI_Immunogenicity_RefreshMaximum_m2251339466 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Immunogenicity_RefreshMaximum_m2251339466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UI_Immunogenicity_t1529844486 * G_B2_0 = NULL;
	UI_Immunogenicity_t1529844486 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	UI_Immunogenicity_t1529844486 * G_B3_1 = NULL;
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_1)
		{
			G_B2_0 = __this;
			goto IL_0038;
		}
	}
	{
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_level_25();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = powf((1.23f), ((float)((float)(((float)((float)((int32_t)((int32_t)L_3-(int32_t)1)))))*(float)(1.5f))));
		G_B3_0 = ((float)((float)(0.395f)*(float)L_4));
		G_B3_1 = G_B1_0;
		goto IL_003d;
	}

IL_0038:
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B2_0;
	}

IL_003d:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_maxProgress_8(G_B3_0);
		return;
	}
}
// System.Void UI_Immunogenicity::ResetProgress()
extern "C"  void UI_Immunogenicity_ResetProgress_m748700083 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	{
		__this->set_progress_7((0.0f));
		return;
	}
}
// System.Void UI_Immunogenicity::Start()
extern Il2CppClass* UI_Immunogenicity_t1529844486_il2cpp_TypeInfo_var;
extern const uint32_t UI_Immunogenicity_Start_m1789039563_MetadataUsageId;
extern "C"  void UI_Immunogenicity_Start_m1789039563 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Immunogenicity_Start_m1789039563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_state_9(1);
		((UI_Immunogenicity_t1529844486_StaticFields*)UI_Immunogenicity_t1529844486_il2cpp_TypeInfo_var->static_fields)->set_main_11(__this);
		UI_Immunogenicity_RefreshMaximum_m2251339466(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Immunogenicity::OnGUI()
extern Il2CppCodeGenString* _stringLiteral3342545192;
extern const uint32_t UI_Immunogenicity_OnGUI_m3758604645_MetadataUsageId;
extern "C"  void UI_Immunogenicity_OnGUI_m3758604645 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Immunogenicity_OnGUI_m3758604645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Image_t2042527209 * L_0 = __this->get_barFiller_2();
		NullCheck(L_0);
		float L_1 = Image_get_fillAmount_m3354146540(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		float L_3 = __this->get_threshhold_3();
		if ((!(((float)L_2) >= ((float)L_3))))
		{
			goto IL_003e;
		}
	}
	{
		Image_t2042527209 * L_4 = __this->get_barFiller_2();
		Color_t2020392075  L_5 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_5);
		Animator_t69676727 * L_6 = __this->get_anim_4();
		NullCheck(L_6);
		Animator_SetBool_m2305662531(L_6, _stringLiteral3342545192, (bool)1, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_003e:
	{
		Animator_t69676727 * L_7 = __this->get_anim_4();
		NullCheck(L_7);
		Animator_SetBool_m2305662531(L_7, _stringLiteral3342545192, (bool)0, /*hidden argument*/NULL);
		Image_t2042527209 * L_8 = __this->get_barFiller_2();
		Image_t2042527209 * L_9 = __this->get_barFiller_2();
		NullCheck(L_9);
		float L_10 = Image_get_fillAmount_m3354146540(L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_threshhold_3();
		Color_t2020392075  L_12 = Functions_GetColorPercentage_m2869151277(NULL /*static, unused*/, ((float)((float)((float)((float)(1.0f)-(float)L_10))-(float)((float)((float)(1.0f)-(float)L_11)))), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_12);
	}

IL_007d:
	{
		return;
	}
}
// System.Void UI_Immunogenicity::Update()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern const uint32_t UI_Immunogenicity_Update_m1396367828_MetadataUsageId;
extern "C"  void UI_Immunogenicity_Update_m1396367828 (UI_Immunogenicity_t1529844486 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Immunogenicity_Update_m1396367828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_state_9();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0068;
		}
		if (L_1 == 1)
		{
			goto IL_001e;
		}
		if (L_1 == 2)
		{
			goto IL_00ea;
		}
	}
	{
		goto IL_014a;
	}

IL_001e:
	{
		Image_t2042527209 * L_2 = __this->get_barFiller_2();
		NullCheck(L_2);
		float L_3 = Image_get_fillAmount_m3354146540(L_2, /*hidden argument*/NULL);
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_003f;
		}
	}
	{
		UI_Immunogenicity_ChangeState_m614690109(__this, 3, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_003f:
	{
		Image_t2042527209 * L_4 = __this->get_barFiller_2();
		Image_t2042527209 * L_5 = L_4;
		NullCheck(L_5);
		float L_6 = Image_get_fillAmount_m3354146540(L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_fallRate_5();
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Image_set_fillAmount_m2220966753(L_5, ((float)((float)L_6-(float)((float)((float)((float)((float)L_7*(float)L_8))*(float)(60.0f))))), /*hidden argument*/NULL);
	}

IL_0063:
	{
		goto IL_014f;
	}

IL_0068:
	{
		Image_t2042527209 * L_9 = __this->get_barFiller_2();
		NullCheck(L_9);
		float L_10 = Image_get_fillAmount_m3354146540(L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_fillIntended_10();
		if ((!(((float)L_10) >= ((float)L_11))))
		{
			goto IL_008a;
		}
	}
	{
		UI_Immunogenicity_ChangeState_m614690109(__this, 1, /*hidden argument*/NULL);
		goto IL_00e5;
	}

IL_008a:
	{
		Image_t2042527209 * L_12 = __this->get_barFiller_2();
		NullCheck(L_12);
		float L_13 = Image_get_fillAmount_m3354146540(L_12, /*hidden argument*/NULL);
		if ((!(((float)L_13) == ((float)(1.0f)))))
		{
			goto IL_00c1;
		}
	}
	{
		Game_t1600141214 * L_14 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_14);
		Game_LevelUp_m1101585534(L_14, /*hidden argument*/NULL);
		UI_Immunogenicity_ResetProgress_m748700083(__this, /*hidden argument*/NULL);
		UI_Immunogenicity_ChangeState_m614690109(__this, 2, /*hidden argument*/NULL);
		UI_Immunogenicity_RefreshMaximum_m2251339466(__this, /*hidden argument*/NULL);
		goto IL_00e5;
	}

IL_00c1:
	{
		Image_t2042527209 * L_15 = __this->get_barFiller_2();
		Image_t2042527209 * L_16 = L_15;
		NullCheck(L_16);
		float L_17 = Image_get_fillAmount_m3354146540(L_16, /*hidden argument*/NULL);
		float L_18 = __this->get_fillRate_6();
		float L_19 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Image_set_fillAmount_m2220966753(L_16, ((float)((float)L_17+(float)((float)((float)((float)((float)L_18*(float)L_19))*(float)(60.0f))))), /*hidden argument*/NULL);
	}

IL_00e5:
	{
		goto IL_014f;
	}

IL_00ea:
	{
		Image_t2042527209 * L_20 = __this->get_barFiller_2();
		NullCheck(L_20);
		float L_21 = Image_get_fillAmount_m3354146540(L_20, /*hidden argument*/NULL);
		if ((((float)L_21) <= ((float)(0.0f))))
		{
			goto IL_0115;
		}
	}
	{
		Image_t2042527209 * L_22 = __this->get_barFiller_2();
		NullCheck(L_22);
		float L_23 = Image_get_fillAmount_m3354146540(L_22, /*hidden argument*/NULL);
		float L_24 = __this->get_fillIntended_10();
		if ((!(((float)L_23) <= ((float)L_24))))
		{
			goto IL_0121;
		}
	}

IL_0115:
	{
		UI_Immunogenicity_ChangeState_m614690109(__this, 1, /*hidden argument*/NULL);
		goto IL_0145;
	}

IL_0121:
	{
		Image_t2042527209 * L_25 = __this->get_barFiller_2();
		Image_t2042527209 * L_26 = L_25;
		NullCheck(L_26);
		float L_27 = Image_get_fillAmount_m3354146540(L_26, /*hidden argument*/NULL);
		float L_28 = __this->get_fillRate_6();
		float L_29 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Image_set_fillAmount_m2220966753(L_26, ((float)((float)L_27-(float)((float)((float)((float)((float)L_28*(float)L_29))*(float)(60.0f))))), /*hidden argument*/NULL);
	}

IL_0145:
	{
		goto IL_014f;
	}

IL_014a:
	{
		goto IL_014f;
	}

IL_014f:
	{
		return;
	}
}
// System.Void UI_Level::.ctor()
extern "C"  void UI_Level__ctor_m2389024474 (UI_Level_t2621343419 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Level::Awake()
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern const uint32_t UI_Level_Awake_m4278608247_MetadataUsageId;
extern "C"  void UI_Level_Awake_m4278608247 (UI_Level_t2621343419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Level_Awake_m4278608247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_text_2(L_0);
		return;
	}
}
// System.Void UI_Level::OnGUI()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UI_Level_OnGUI_m3336008194_MetadataUsageId;
extern "C"  void UI_Level_OnGUI_m3336008194 (UI_Level_t2621343419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Level_OnGUI_m3336008194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		Text_t356221433 * L_4 = __this->get_text_2();
		String_t* L_5 = __this->get__prefix_3();
		Game_t1600141214 * L_6 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_6);
		int32_t* L_7 = L_6->get_address_of_level_25();
		String_t* L_8 = Int32_ToString_m2960866144(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_9);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UI_Matrix::.ctor()
extern "C"  void UI_Matrix__ctor_m1847062659 (UI_Matrix_t2197715354 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Membrane::.ctor()
extern "C"  void UI_Membrane__ctor_m2868329911 (UI_Membrane_t3925205798 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Pause::.ctor()
extern "C"  void UI_Pause__ctor_m3520464454 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// Controller UI_Pause::get_controller()
extern "C"  Controller_t1937198888 * UI_Pause_get_controller_m2786843706 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = __this->get_U3CcontrollerU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UI_Pause::set_controller(Controller)
extern "C"  void UI_Pause_set_controller_m1138709003 (UI_Pause_t1107290467 * __this, Controller_t1937198888 * ___value0, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ___value0;
		__this->set_U3CcontrollerU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.UI.Button UI_Pause::get_button()
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var;
extern const uint32_t UI_Pause_get_button_m1688983065_MetadataUsageId;
extern "C"  Button_t2872111280 * UI_Pause_get_button_m1688983065 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Pause_get_button_m1688983065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Button_t2872111280 * L_1 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_0, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_MethodInfo_var);
		return L_1;
	}
}
// UnityEngine.RectTransform UI_Pause::get_transform()
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var;
extern const uint32_t UI_Pause_get_transform_m2136912435_MetadataUsageId;
extern "C"  RectTransform_t3349966182 * UI_Pause_get_transform_m2136912435 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Pause_get_transform_m2136912435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_t3349966182 * L_1 = GameObject_GetComponent_TisRectTransform_t3349966182_m132995507(L_0, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3349966182_m132995507_MethodInfo_var);
		return L_1;
	}
}
// System.Void UI_Pause::Pause()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UI_Pause_Pause_m2324070012_MetadataUsageId;
extern "C"  void UI_Pause_Pause_m2324070012 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Pause_Pause_m2324070012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0056;
		}
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		Image_t2042527209 * L_1 = __this->get_picture_5();
		Sprite_t309593783 * L_2 = __this->get_pauseSymbol_3();
		NullCheck(L_1);
		Image_set_sprite_m1800056820(L_1, L_2, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get__backgroundMusic_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		AudioSource_t1135106623 * L_5 = __this->get__backgroundMusic_7();
		NullCheck(L_5);
		AudioSource_Play_m353744792(L_5, /*hidden argument*/NULL);
	}

IL_0045:
	{
		GameObject_t1756533147 * L_6 = __this->get_pauseMenu_6();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		goto IL_0098;
	}

IL_0056:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		Image_t2042527209 * L_7 = __this->get_picture_5();
		Sprite_t309593783 * L_8 = __this->get_playSymbol_4();
		NullCheck(L_7);
		Image_set_sprite_m1800056820(L_7, L_8, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_9 = __this->get__backgroundMusic_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008c;
		}
	}
	{
		AudioSource_t1135106623 * L_11 = __this->get__backgroundMusic_7();
		NullCheck(L_11);
		AudioSource_Pause_m71375470(L_11, /*hidden argument*/NULL);
	}

IL_008c:
	{
		GameObject_t1756533147 * L_12 = __this->get_pauseMenu_6();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void UI_Pause::Start()
extern const MethodInfo* GameObject_GetComponent_TisController_t1937198888_m3117371903_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2665277406;
extern const uint32_t UI_Pause_Start_m3851690914_MetadataUsageId;
extern "C"  void UI_Pause_Start_m3851690914 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Pause_Start_m3851690914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral2665277406, /*hidden argument*/NULL);
		NullCheck(L_0);
		Controller_t1937198888 * L_1 = GameObject_GetComponent_TisController_t1937198888_m3117371903(L_0, /*hidden argument*/GameObject_GetComponent_TisController_t1937198888_m3117371903_MethodInfo_var);
		UI_Pause_set_controller_m1138709003(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Pause::Update()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern const uint32_t UI_Pause_Update_m1187948687_MetadataUsageId;
extern "C"  void UI_Pause_Update_m1187948687 (UI_Pause_t1107290467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Pause_Update_m1187948687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Button_t2872111280 * L_0 = UI_Pause_get_button_m1688983065(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Selectable_get_interactable_m1725029500(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Controller_t1937198888 * L_2 = UI_Pause_get_controller_m2786843706(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = Controller_GetBack_m1477624146(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		UI_Pause_Pause_m2324070012(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		Game_t1600141214 * L_4 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_4);
		int32_t L_5 = Game_get_population_m752974705(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_6 = __this->get_state_2();
		if (L_6)
		{
			goto IL_006a;
		}
	}
	{
		Game_t1600141214 * L_7 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_7);
		Game_EndGame_m3658353106(L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_pauseMenu_6();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_9 = UI_Pause_get_button_m1688983065(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Selectable_set_interactable_m63718297(L_9, (bool)0, /*hidden argument*/NULL);
		__this->set_state_2(1);
	}

IL_006a:
	{
		goto IL_0082;
	}

IL_006f:
	{
		int32_t L_10 = __this->get_state_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_state_2(0);
	}

IL_0082:
	{
		return;
	}
}
// System.Void UI_Population::.ctor()
extern "C"  void UI_Population__ctor_m2359501799 (UI_Population_t2609643076 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Population::Awake()
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const uint32_t UI_Population_Awake_m3958304368_MetadataUsageId;
extern "C"  void UI_Population_Awake_m3958304368 (UI_Population_t2609643076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Population_Awake_m3958304368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t356221433 * L_1 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_1);
		Animator_t69676727 * L_2 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_anim_3(L_2);
		return;
	}
}
// System.Void UI_Population::OnGUI()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral287059880;
extern const uint32_t UI_Population_OnGUI_m1257426857_MetadataUsageId;
extern "C"  void UI_Population_OnGUI_m1257426857 (UI_Population_t2609643076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Population_OnGUI_m1257426857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Text_t356221433 * L_0 = __this->get_text_2();
		Game_t1600141214 * L_1 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_1);
		int32_t L_2 = Game_get_population_m752974705(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029398, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		Animator_t69676727 * L_5 = __this->get_anim_3();
		Game_t1600141214 * L_6 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_6);
		int32_t L_7 = Game_get_population_m752974705(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Animator_SetBool_m2305662531(L_5, _stringLiteral287059880, (bool)((((int32_t)L_7) == ((int32_t)5))? 1 : 0), /*hidden argument*/NULL);
		Text_t356221433 * L_8 = __this->get_text_2();
		Game_t1600141214 * L_9 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_9);
		int32_t L_10 = Game_get_population_m752974705(L_9, /*hidden argument*/NULL);
		Color_t2020392075  L_11 = Functions_GetColorPercentage_m2869151277(NULL /*static, unused*/, ((float)((float)(((float)((float)L_10)))/(float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_11);
		return;
	}
}
// System.Void UI_Resume::.ctor()
extern "C"  void UI_Resume__ctor_m1556349809 (UI_Resume_t2961944548 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Resume::OnEnable()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var;
extern const uint32_t UI_Resume_OnEnable_m1803953417_MetadataUsageId;
extern "C"  void UI_Resume_OnEnable_m1803953417 (UI_Resume_t2961944548 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Resume_OnEnable_m1803953417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * G_B2_0 = NULL;
	Button_t2872111280 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Button_t2872111280 * G_B3_1 = NULL;
	{
		Button_t2872111280 * L_0 = Component_GetComponent_TisButton_t2872111280_m3412601438(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		Game_t1600141214 * L_1 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0027;
		}
	}
	{
		Game_t1600141214 * L_3 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_3);
		int32_t L_4 = Game_get_population_m752974705(L_3, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_4) > ((int32_t)0))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0028:
	{
		NullCheck(G_B3_1);
		Selectable_set_interactable_m63718297(G_B3_1, (bool)G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Resume::Resume()
extern "C"  void UI_Resume_Resume_m3838652464 (UI_Resume_t2961944548 * __this, const MethodInfo* method)
{
	{
		UI_Pause_t1107290467 * L_0 = __this->get_pause_2();
		NullCheck(L_0);
		UI_Pause_Pause_m2324070012(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_RNA::.ctor()
extern "C"  void UI_RNA__ctor_m625863521 (UI_RNA_t3307819878 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_RT::.ctor()
extern "C"  void UI_RT__ctor_m1810074864 (UI_RT_t3092214427 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Score::.ctor()
extern "C"  void UI_Score__ctor_m1301067138 (UI_Score_t2043792049 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_Score::OnGUI()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t UI_Score_OnGUI_m578752906_MetadataUsageId;
extern "C"  void UI_Score_OnGUI_m578752906 (UI_Score_t2043792049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Score_OnGUI_m578752906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_text_2();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		Game_t1600141214 * L_2 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_2);
		int32_t* L_3 = L_2->get_address_of_score_24();
		String_t* L_4 = Int32_ToString_m2960866144(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0076;
		}
	}
	{
		Text_t356221433 * L_6 = __this->get_text_2();
		Game_t1600141214 * L_7 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_7);
		int32_t* L_8 = L_7->get_address_of_score_24();
		String_t* L_9 = Int32_ToString_m2960866144(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
		Text_t356221433 * L_10 = __this->get_text_2();
		Game_t1600141214 * L_11 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_score_24();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_13 = log10f((((float)((float)L_12))));
		int32_t L_14 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_15 = Mathf_Max_m1875893177(NULL /*static, unused*/, 1, L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		Text_set_fontSize_m2101304336(L_10, ((int32_t)((int32_t)((int32_t)100)-(int32_t)((int32_t)((int32_t)((int32_t)10)*(int32_t)L_15)))), /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void UI_Spike::.ctor()
extern "C"  void UI_Spike__ctor_m1742877090 (UI_Spike_t1842279613 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UI_GP120 UI_Spike::get_gp120()
extern const MethodInfo* GameObject_GetComponentInChildren_TisUI_GP120_t1483396489_m1328587960_MethodInfo_var;
extern const uint32_t UI_Spike_get_gp120_m384494825_MetadataUsageId;
extern "C"  UI_GP120_t1483396489 * UI_Spike_get_gp120_m384494825 (UI_Spike_t1842279613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Spike_get_gp120_m384494825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_GP120_t1483396489 * L_1 = GameObject_GetComponentInChildren_TisUI_GP120_t1483396489_m1328587960(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisUI_GP120_t1483396489_m1328587960_MethodInfo_var);
		return L_1;
	}
}
// UI_GP41 UI_Spike::get_gp41()
extern const MethodInfo* GameObject_GetComponentInChildren_TisUI_GP41_t2232403251_m3759450054_MethodInfo_var;
extern const uint32_t UI_Spike_get_gp41_m470670369_MetadataUsageId;
extern "C"  UI_GP41_t2232403251 * UI_Spike_get_gp41_m470670369 (UI_Spike_t1842279613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_Spike_get_gp41_m470670369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		UI_GP41_t2232403251 * L_1 = GameObject_GetComponentInChildren_TisUI_GP41_t2232403251_m3759450054(L_0, /*hidden argument*/GameObject_GetComponentInChildren_TisUI_GP41_t2232403251_m3759450054_MethodInfo_var);
		return L_1;
	}
}
// System.Void UI_SpikeList::.ctor()
extern "C"  void UI_SpikeList__ctor_m908666082 (UI_SpikeList_t1985653691 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// SpikeColor UI_SpikeList::get_colors()
extern "C"  SpikeColor_t3573234163  UI_SpikeList_get_colors_m4254507447 (UI_SpikeList_t1985653691 * __this, const MethodInfo* method)
{
	{
		SpikeColor_t3573234163  L_0 = __this->get__colors_2();
		return L_0;
	}
}
// System.Void UI_SpikeList::set_colors(SpikeColor)
extern "C"  void UI_SpikeList_set_colors_m3164100610 (UI_SpikeList_t1985653691 * __this, SpikeColor_t3573234163  ___value0, const MethodInfo* method)
{
	UI_Spike_t1842279613 * V_0 = NULL;
	UI_SpikeU5BU5D_t2474440624* V_1 = NULL;
	int32_t V_2 = 0;
	{
		UI_SpikeU5BU5D_t2474440624* L_0 = __this->get_spikeList_3();
		V_1 = L_0;
		V_2 = 0;
		goto IL_004c;
	}

IL_000e:
	{
		UI_SpikeU5BU5D_t2474440624* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		UI_Spike_t1842279613 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		UI_Spike_t1842279613 * L_5 = V_0;
		NullCheck(L_5);
		UI_GP41_t2232403251 * L_6 = UI_Spike_get_gp41_m470670369(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = (&___value0)->get_gp41_0();
		NullCheck(L_6);
		UI_GP41_set_color_m896254274(L_6, L_7, /*hidden argument*/NULL);
		UI_Spike_t1842279613 * L_8 = V_0;
		NullCheck(L_8);
		UI_GP120_t1483396489 * L_9 = UI_Spike_get_gp120_m384494825(L_8, /*hidden argument*/NULL);
		Color_t2020392075  L_10 = (&___value0)->get_gp120_left_1();
		NullCheck(L_9);
		UI_GP120_set_colorLeft_m1174730493(L_9, L_10, /*hidden argument*/NULL);
		UI_Spike_t1842279613 * L_11 = V_0;
		NullCheck(L_11);
		UI_GP120_t1483396489 * L_12 = UI_Spike_get_gp120_m384494825(L_11, /*hidden argument*/NULL);
		Color_t2020392075  L_13 = (&___value0)->get_gp120_right_2();
		NullCheck(L_12);
		UI_GP120_set_colorRight_m370336612(L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_15 = V_2;
		UI_SpikeU5BU5D_t2474440624* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		SpikeColor_t3573234163  L_17 = ___value0;
		__this->set__colors_2(L_17);
		return;
	}
}
// System.Void UI_TryAgain::.ctor()
extern "C"  void UI_TryAgain__ctor_m3999166147 (UI_TryAgain_t3222824968 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_TryAgain::OnGUI()
extern Il2CppClass* Game_t1600141214_il2cpp_TypeInfo_var;
extern const uint32_t UI_TryAgain_OnGUI_m2697421829_MetadataUsageId;
extern "C"  void UI_TryAgain_OnGUI_m2697421829 (UI_TryAgain_t3222824968 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_TryAgain_OnGUI_m2697421829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Game_t1600141214 * L_0 = ((Game_t1600141214_StaticFields*)Game_t1600141214_il2cpp_TypeInfo_var->static_fields)->get_main_30();
		NullCheck(L_0);
		int32_t L_1 = Game_get_population_m752974705(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		Text_t356221433 * L_2 = __this->get_text_2();
		String_t* L_3 = __this->get_endGameText_4();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		goto IL_0037;
	}

IL_0026:
	{
		Text_t356221433 * L_4 = __this->get_text_2();
		String_t* L_5 = __this->get_pauseText_3();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
	}

IL_0037:
	{
		return;
	}
}
// System.Void UI_World_Filler::.ctor()
extern "C"  void UI_World_Filler__ctor_m3703992505 (UI_World_Filler_t108322328 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI_World_Filler::Awake()
extern Il2CppClass* UI_World_Filler_t108322328_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UI_World_Filler_Awake_m2757545992_MetadataUsageId;
extern "C"  void UI_World_Filler_Awake_m2757545992 (UI_World_Filler_t108322328 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UI_World_Filler_Awake_m2757545992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UI_World_Filler_t108322328 * L_0 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->set_main_2(__this);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0027:
	{
		UI_World_Filler_t108322328 * L_3 = ((UI_World_Filler_t108322328_StaticFields*)UI_World_Filler_t108322328_il2cpp_TypeInfo_var->static_fields)->get_main_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void UI_World_Filler::Fill(System.Single)
extern "C"  void UI_World_Filler_Fill_m2266385555 (UI_World_Filler_t108322328 * __this, float ___f0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get__filler_3();
		float L_1 = ___f0;
		NullCheck(L_0);
		Image_set_fillAmount_m2220966753(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VersionNumberDisplay::.ctor()
extern "C"  void VersionNumberDisplay__ctor_m4250395844 (VersionNumberDisplay_t1254145757 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VersionNumberDisplay::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3444401938;
extern const uint32_t VersionNumberDisplay_Start_m2875642844_MetadataUsageId;
extern "C"  void VersionNumberDisplay_Start_m2875642844 (VersionNumberDisplay_t1254145757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VersionNumberDisplay_Start_m2875642844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_1 = Application_get_version_m1828625161(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3444401938, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_3);
		return;
	}
}
// System.Void ZoomController::.ctor()
extern "C"  void ZoomController__ctor_m4063515714 (ZoomController_t1627556657 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// RenderCamera ZoomController::get_renderCam()
extern const MethodInfo* Component_GetComponent_TisRenderCamera_t2596733641_m551332000_MethodInfo_var;
extern const uint32_t ZoomController_get_renderCam_m479667980_MetadataUsageId;
extern "C"  RenderCamera_t2596733641 * ZoomController_get_renderCam_m479667980 (ZoomController_t1627556657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomController_get_renderCam_m479667980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RenderCamera_t2596733641 * L_0 = Component_GetComponent_TisRenderCamera_t2596733641_m551332000(__this, /*hidden argument*/Component_GetComponent_TisRenderCamera_t2596733641_m551332000_MethodInfo_var);
		return L_0;
	}
}
// Controller ZoomController::get_controller()
extern const MethodInfo* GameObject_GetComponent_TisController_t1937198888_m3117371903_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2665277406;
extern const uint32_t ZoomController_get_controller_m2798812710_MetadataUsageId;
extern "C"  Controller_t1937198888 * ZoomController_get_controller_m2798812710 (ZoomController_t1627556657 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomController_get_controller_m2798812710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral2665277406, /*hidden argument*/NULL);
		NullCheck(L_0);
		Controller_t1937198888 * L_1 = GameObject_GetComponent_TisController_t1937198888_m3117371903(L_0, /*hidden argument*/GameObject_GetComponent_TisController_t1937198888_m3117371903_MethodInfo_var);
		return L_1;
	}
}
// System.Single ZoomController::get_zoomFactor()
extern "C"  float ZoomController_get_zoomFactor_m3614507305 (ZoomController_t1627556657 * __this, const MethodInfo* method)
{
	{
		RenderCamera_t2596733641 * L_0 = ZoomController_get_renderCam_m479667980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = L_0->get_zoom_4();
		return L_1;
	}
}
// System.Void ZoomController::set_zoomFactor(System.Single)
extern const MethodInfo* Component_GetComponent_TisRenderCamera_t2596733641_m551332000_MethodInfo_var;
extern const uint32_t ZoomController_set_zoomFactor_m870427094_MetadataUsageId;
extern "C"  void ZoomController_set_zoomFactor_m870427094 (ZoomController_t1627556657 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomController_set_zoomFactor_m870427094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___value0;
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) < ((float)(1.0f)))))
		{
			goto IL_0018;
		}
	}
	{
		V_0 = (1.0f);
		goto IL_0029;
	}

IL_0018:
	{
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(10.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		V_0 = (10.0f);
	}

IL_0029:
	{
		RenderCamera_t2596733641 * L_3 = Component_GetComponent_TisRenderCamera_t2596733641_m551332000(__this, /*hidden argument*/Component_GetComponent_TisRenderCamera_t2596733641_m551332000_MethodInfo_var);
		float L_4 = V_0;
		NullCheck(L_3);
		L_3->set_zoom_4(L_4);
		return;
	}
}
// System.Void ZoomController::Start()
extern "C"  void ZoomController_Start_m23304402 (ZoomController_t1627556657 * __this, const MethodInfo* method)
{
	{
		ZoomController_set_zoomFactor_m870427094(__this, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoomController::Update()
extern "C"  void ZoomController_Update_m100697297 (ZoomController_t1627556657 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		V_0 = (0.0f);
		float L_0 = V_0;
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0107;
		}
	}
	{
		Controller_t1937198888 * L_1 = ZoomController_get_controller_m2798812710(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Controller_get_worldPosition_m2261149775(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		RenderCamera_t2596733641 * L_3 = ZoomController_get_renderCam_m479667980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_t189460977 * L_4 = L_3->get_cam_6();
		NullCheck(L_4);
		float L_5 = Camera_get_aspect_m935361871(L_4, /*hidden argument*/NULL);
		V_2 = ((float)((float)(40.0f)*(float)L_5));
		float L_6 = ZoomController_get_zoomFactor_m3614507305(__this, /*hidden argument*/NULL);
		float L_7 = V_0;
		ZoomController_set_zoomFactor_m870427094(__this, ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		RenderCamera_t2596733641 * L_8 = ZoomController_get_renderCam_m479667980(__this, /*hidden argument*/NULL);
		float L_9 = ZoomController_get_zoomFactor_m3614507305(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		float L_10 = RenderCamera_GetZoomHeight_m3395087457(L_8, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = V_3;
		RenderCamera_t2596733641 * L_12 = ZoomController_get_renderCam_m479667980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Camera_t189460977 * L_13 = L_12->get_cam_6();
		NullCheck(L_13);
		float L_14 = Camera_get_aspect_m935361871(L_13, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_11*(float)L_14));
		float L_15 = V_2;
		float L_16 = (&V_1)->get_x_1();
		float L_17 = (&V_1)->get_y_2();
		Vector3__ctor_m2720820983((&V_5), ((float)((float)((float)((float)L_15/(float)(2.0f)))-(float)L_16)), ((float)((float)(20.0f)-(float)L_17)), /*hidden argument*/NULL);
		float L_18 = (&V_5)->get_x_1();
		float L_19 = V_2;
		float L_20 = (&V_5)->get_y_2();
		Vector3__ctor_m2720820983((&V_6), ((float)((float)L_18/(float)L_19)), ((float)((float)L_20/(float)(40.0f))), /*hidden argument*/NULL);
		float L_21 = (&V_6)->get_x_1();
		float L_22 = V_4;
		float L_23 = (&V_6)->get_y_2();
		float L_24 = V_3;
		Vector3__ctor_m2720820983((&V_7), ((float)((float)L_21*(float)L_22)), ((float)((float)L_23*(float)L_24)), /*hidden argument*/NULL);
		float L_25 = V_4;
		float L_26 = (&V_7)->get_x_1();
		float L_27 = V_3;
		float L_28 = (&V_7)->get_y_2();
		Vector3__ctor_m2720820983((&V_8), ((float)((float)((float)((float)L_25/(float)(2.0f)))-(float)L_26)), ((float)((float)((float)((float)L_27/(float)(2.0f)))-(float)L_28)), /*hidden argument*/NULL);
		(&V_8)->set_z_3((-10.0f));
		RenderCamera_t2596733641 * L_29 = ZoomController_get_renderCam_m479667980(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = V_1;
		Vector3_t2243707580  L_31 = V_8;
		Vector3_t2243707580  L_32 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_difference_5(L_32);
	}

IL_0107:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
