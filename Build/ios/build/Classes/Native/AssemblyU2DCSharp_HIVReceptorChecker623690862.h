﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HIVReceptorChecker
struct  HIVReceptorChecker_t623690862  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Collider2D HIVReceptorChecker::stay
	Collider2D_t646061738 * ___stay_2;
	// System.Single HIVReceptorChecker::maxTimer
	float ___maxTimer_3;
	// System.Single HIVReceptorChecker::<_timer>k__BackingField
	float ___U3C_timerU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_stay_2() { return static_cast<int32_t>(offsetof(HIVReceptorChecker_t623690862, ___stay_2)); }
	inline Collider2D_t646061738 * get_stay_2() const { return ___stay_2; }
	inline Collider2D_t646061738 ** get_address_of_stay_2() { return &___stay_2; }
	inline void set_stay_2(Collider2D_t646061738 * value)
	{
		___stay_2 = value;
		Il2CppCodeGenWriteBarrier(&___stay_2, value);
	}

	inline static int32_t get_offset_of_maxTimer_3() { return static_cast<int32_t>(offsetof(HIVReceptorChecker_t623690862, ___maxTimer_3)); }
	inline float get_maxTimer_3() const { return ___maxTimer_3; }
	inline float* get_address_of_maxTimer_3() { return &___maxTimer_3; }
	inline void set_maxTimer_3(float value)
	{
		___maxTimer_3 = value;
	}

	inline static int32_t get_offset_of_U3C_timerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HIVReceptorChecker_t623690862, ___U3C_timerU3Ek__BackingField_4)); }
	inline float get_U3C_timerU3Ek__BackingField_4() const { return ___U3C_timerU3Ek__BackingField_4; }
	inline float* get_address_of_U3C_timerU3Ek__BackingField_4() { return &___U3C_timerU3Ek__BackingField_4; }
	inline void set_U3C_timerU3Ek__BackingField_4(float value)
	{
		___U3C_timerU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
