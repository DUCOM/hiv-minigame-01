﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Statistics>
struct DefaultComparer_t1026647240;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Statistics>::.ctor()
extern "C"  void DefaultComparer__ctor_m3436394581_gshared (DefaultComparer_t1026647240 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3436394581(__this, method) ((  void (*) (DefaultComparer_t1026647240 *, const MethodInfo*))DefaultComparer__ctor_m3436394581_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Statistics>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4040398834_gshared (DefaultComparer_t1026647240 * __this, Statistics_t2537377007  ___x0, Statistics_t2537377007  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m4040398834(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1026647240 *, Statistics_t2537377007 , Statistics_t2537377007 , const MethodInfo*))DefaultComparer_Compare_m4040398834_gshared)(__this, ___x0, ___y1, method)
