﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1057408206MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<HIV/State,HIV_State,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2772434513(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t191740304 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2625004652_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<HIV/State,HIV_State,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2359090973(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t191740304 *, int32_t, HIV_State_t57600565 *, const MethodInfo*))Transform_1_Invoke_m3585416764_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<HIV/State,HIV_State,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3704808160(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t191740304 *, int32_t, HIV_State_t57600565 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1294418267_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<HIV/State,HIV_State,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1741459943(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t191740304 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3136511590_gshared)(__this, ___result0, method)
