﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Death
struct HIV_Death_t750143594;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Death::.ctor()
extern "C"  void HIV_Death__ctor_m3815110599 (HIV_Death_t750143594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Death::Die()
extern "C"  void HIV_Death_Die_m3759854903 (HIV_Death_t750143594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Death::OnEnable()
extern "C"  void HIV_Death_OnEnable_m3110694267 (HIV_Death_t750143594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Death::Update()
extern "C"  void HIV_Death_Update_m2607022356 (HIV_Death_t750143594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
