﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1066308972.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_207556710.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3676640726_gshared (InternalEnumerator_1_t1066308972 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3676640726(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1066308972 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3676640726_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3900293718_gshared (InternalEnumerator_1_t1066308972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3900293718(__this, method) ((  void (*) (InternalEnumerator_1_t1066308972 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3900293718_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2570911272_gshared (InternalEnumerator_1_t1066308972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2570911272(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1066308972 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2570911272_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2592720395_gshared (InternalEnumerator_1_t1066308972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2592720395(__this, method) ((  void (*) (InternalEnumerator_1_t1066308972 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2592720395_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3275343790_gshared (InternalEnumerator_1_t1066308972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3275343790(__this, method) ((  bool (*) (InternalEnumerator_1_t1066308972 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3275343790_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t207556710  InternalEnumerator_1_get_Current_m3544610255_gshared (InternalEnumerator_1_t1066308972 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3544610255(__this, method) ((  KeyValuePair_2_t207556710  (*) (InternalEnumerator_1_t1066308972 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3544610255_gshared)(__this, method)
