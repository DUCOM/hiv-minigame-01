﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ErrorCode_VariableCodes489766538.h"
#include "mscorlib_System_String2029220233.h"

// System.String ErrorCode::get_E0000()
extern "C"  String_t* ErrorCode_get_E0000_m4290471955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::get_EA0VA()
extern "C"  String_t* ErrorCode_get_EA0VA_m1394698289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::get_EA0IN()
extern "C"  String_t* ErrorCode_get_EA0IN_m1408252569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::get_EA0NN()
extern "C"  String_t* ErrorCode_get_EA0NN_m1408252274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::get_EA0OR()
extern "C"  String_t* ErrorCode_get_EA0OR_m2389741663 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::get_EC0MC()
extern "C"  String_t* ErrorCode_get_EC0MC_m1677016276 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::get_EG0HT()
extern "C"  String_t* ErrorCode_get_EG0HT_m1542756000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::DisplayVariableCode(ErrorCode/VariableCodes,System.String)
extern "C"  String_t* ErrorCode_DisplayVariableCode_m2670461172 (Il2CppObject * __this /* static, unused */, int32_t ___variableCode0, String_t* ___variableName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ErrorCode::DisplayCode(System.String,System.String)
extern "C"  String_t* ErrorCode_DisplayCode_m3512282654 (Il2CppObject * __this /* static, unused */, String_t* ___number0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
