﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Statistics>
struct List_1_t1906498139;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441227813.h"
#include "AssemblyU2DCSharp_Statistics2537377007.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Statistics>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m935090778_gshared (Enumerator_t1441227813 * __this, List_1_t1906498139 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m935090778(__this, ___l0, method) ((  void (*) (Enumerator_t1441227813 *, List_1_t1906498139 *, const MethodInfo*))Enumerator__ctor_m935090778_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Statistics>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4208588792_gshared (Enumerator_t1441227813 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4208588792(__this, method) ((  void (*) (Enumerator_t1441227813 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4208588792_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Statistics>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2461919078_gshared (Enumerator_t1441227813 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2461919078(__this, method) ((  Il2CppObject * (*) (Enumerator_t1441227813 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2461919078_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Statistics>::Dispose()
extern "C"  void Enumerator_Dispose_m1453949619_gshared (Enumerator_t1441227813 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1453949619(__this, method) ((  void (*) (Enumerator_t1441227813 *, const MethodInfo*))Enumerator_Dispose_m1453949619_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Statistics>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2354754832_gshared (Enumerator_t1441227813 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2354754832(__this, method) ((  void (*) (Enumerator_t1441227813 *, const MethodInfo*))Enumerator_VerifyState_m2354754832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Statistics>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m694413241_gshared (Enumerator_t1441227813 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m694413241(__this, method) ((  bool (*) (Enumerator_t1441227813 *, const MethodInfo*))Enumerator_MoveNext_m694413241_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Statistics>::get_Current()
extern "C"  Statistics_t2537377007  Enumerator_get_Current_m3341236517_gshared (Enumerator_t1441227813 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3341236517(__this, method) ((  Statistics_t2537377007  (*) (Enumerator_t1441227813 *, const MethodInfo*))Enumerator_get_Current_m3341236517_gshared)(__this, method)
