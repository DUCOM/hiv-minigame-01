﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<SpriteRender>
struct Collection_1_t2370562103;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SpriteRender[]
struct SpriteRenderU5BU5D_t2233488264;
// System.Collections.Generic.IEnumerator`1<SpriteRender>
struct IEnumerator_1_t304341176;
// System.Collections.Generic.IList`1<SpriteRender>
struct IList_1_t3369757950;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SpriteRender2828817349.h"

// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::.ctor()
extern "C"  void Collection_1__ctor_m753671578_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1__ctor_m753671578(__this, method) ((  void (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1__ctor_m753671578_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1382385567_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1382385567(__this, method) ((  bool (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1382385567_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2317520158_gshared (Collection_1_t2370562103 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2317520158(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2370562103 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2317520158_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m666368059_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m666368059(__this, method) ((  Il2CppObject * (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m666368059_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3025424258_gshared (Collection_1_t2370562103 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3025424258(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2370562103 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3025424258_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1499996908_gshared (Collection_1_t2370562103 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1499996908(__this, ___value0, method) ((  bool (*) (Collection_1_t2370562103 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1499996908_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3876747988_gshared (Collection_1_t2370562103 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3876747988(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2370562103 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3876747988_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2824647547_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2824647547(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2824647547_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2353660683_gshared (Collection_1_t2370562103 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2353660683(__this, ___value0, method) ((  void (*) (Collection_1_t2370562103 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2353660683_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2389898810_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2389898810(__this, method) ((  bool (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2389898810_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3383282020_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3383282020(__this, method) ((  Il2CppObject * (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3383282020_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3263987755_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3263987755(__this, method) ((  bool (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3263987755_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4070375238_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4070375238(__this, method) ((  bool (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4070375238_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4046275591_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4046275591(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2370562103 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4046275591_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2237086044_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2237086044(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2237086044_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::Add(T)
extern "C"  void Collection_1_Add_m4016262895_gshared (Collection_1_t2370562103 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define Collection_1_Add_m4016262895(__this, ___item0, method) ((  void (*) (Collection_1_t2370562103 *, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_Add_m4016262895_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::Clear()
extern "C"  void Collection_1_Clear_m671687755_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_Clear_m671687755(__this, method) ((  void (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_Clear_m671687755_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3056541881_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3056541881(__this, method) ((  void (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_ClearItems_m3056541881_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::Contains(T)
extern "C"  bool Collection_1_Contains_m1496636209_gshared (Collection_1_t2370562103 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1496636209(__this, ___item0, method) ((  bool (*) (Collection_1_t2370562103 *, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_Contains_m1496636209_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1782537923_gshared (Collection_1_t2370562103 * __this, SpriteRenderU5BU5D_t2233488264* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1782537923(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2370562103 *, SpriteRenderU5BU5D_t2233488264*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1782537923_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<SpriteRender>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m946029422_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m946029422(__this, method) ((  Il2CppObject* (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_GetEnumerator_m946029422_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SpriteRender>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m357434719_gshared (Collection_1_t2370562103 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m357434719(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2370562103 *, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_IndexOf_m357434719_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2384807752_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, SpriteRender_t2828817349  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2384807752(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_Insert_m2384807752_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2605607937_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, SpriteRender_t2828817349  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2605607937(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_InsertItem_m2605607937_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::Remove(T)
extern "C"  bool Collection_1_Remove_m3600682974_gshared (Collection_1_t2370562103 * __this, SpriteRender_t2828817349  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3600682974(__this, ___item0, method) ((  bool (*) (Collection_1_t2370562103 *, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_Remove_m3600682974_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3771592980_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3771592980(__this, ___index0, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3771592980_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2450978042_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2450978042(__this, ___index0, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2450978042_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SpriteRender>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1358252162_gshared (Collection_1_t2370562103 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1358252162(__this, method) ((  int32_t (*) (Collection_1_t2370562103 *, const MethodInfo*))Collection_1_get_Count_m1358252162_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<SpriteRender>::get_Item(System.Int32)
extern "C"  SpriteRender_t2828817349  Collection_1_get_Item_m2876745528_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2876745528(__this, ___index0, method) ((  SpriteRender_t2828817349  (*) (Collection_1_t2370562103 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2876745528_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1643605063_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, SpriteRender_t2828817349  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1643605063(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_set_Item_m1643605063_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1277631020_gshared (Collection_1_t2370562103 * __this, int32_t ___index0, SpriteRender_t2828817349  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1277631020(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2370562103 *, int32_t, SpriteRender_t2828817349 , const MethodInfo*))Collection_1_SetItem_m1277631020_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m199798957_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m199798957(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m199798957_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<SpriteRender>::ConvertItem(System.Object)
extern "C"  SpriteRender_t2828817349  Collection_1_ConvertItem_m4207168841_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4207168841(__this /* static, unused */, ___item0, method) ((  SpriteRender_t2828817349  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4207168841_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SpriteRender>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1112160381_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1112160381(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1112160381_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m911343189_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m911343189(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m911343189_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SpriteRender>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2975299428_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2975299428(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2975299428_gshared)(__this /* static, unused */, ___list0, method)
