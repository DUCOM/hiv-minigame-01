﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HIV_Wander
struct HIV_Wander_t1816073829;

#include "codegen/il2cpp-codegen.h"

// System.Void HIV_Wander::.ctor()
extern "C"  void HIV_Wander__ctor_m803011428 (HIV_Wander_t1816073829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Wander::Start()
extern "C"  void HIV_Wander_Start_m3407966372 (HIV_Wander_t1816073829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Wander::OnEnable()
extern "C"  void HIV_Wander_OnEnable_m3204205284 (HIV_Wander_t1816073829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HIV_Wander::FixedUpdate()
extern "C"  void HIV_Wander_FixedUpdate_m572207003 (HIV_Wander_t1816073829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
