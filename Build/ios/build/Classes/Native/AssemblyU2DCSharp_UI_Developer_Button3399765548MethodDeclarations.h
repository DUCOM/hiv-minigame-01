﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_Developer_Button
struct UI_Developer_Button_t3399765548;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_Developer_Button::.ctor()
extern "C"  void UI_Developer_Button__ctor_m2550579923 (UI_Developer_Button_t3399765548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_Developer_Button::Toggle()
extern "C"  void UI_Developer_Button_Toggle_m730574041 (UI_Developer_Button_t3399765548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
