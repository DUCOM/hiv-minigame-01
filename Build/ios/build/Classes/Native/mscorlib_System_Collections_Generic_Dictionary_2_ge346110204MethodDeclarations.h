﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Collections.Generic.IEqualityComparer`1<Tropism/ReceptorType>
struct IEqualityComparer_1_t229236393;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>[]
struct KeyValuePair_2U5BU5D_t727157751;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/ReceptorType,System.Single>>
struct IEnumerator_1_t4168913845;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/ReceptorType,System.Single>
struct ValueCollection_t3344137343;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22398422722.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1666134906.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::.ctor()
extern "C"  void Dictionary_2__ctor_m3169554460_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3169554460(__this, method) ((  void (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2__ctor_m3169554460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2474255912_gshared (Dictionary_2_t346110204 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2474255912(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2474255912_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2797813544_gshared (Dictionary_2_t346110204 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2797813544(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t346110204 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2797813544_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m175372014_gshared (Dictionary_2_t346110204 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m175372014(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t346110204 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m175372014_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2958490927_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2958490927(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t346110204 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2958490927_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3159568658_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3159568658(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3159568658_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m948492481_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m948492481(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m948492481_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3628761718_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3628761718(__this, ___key0, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3628761718_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m503920335_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m503920335(__this, method) ((  bool (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m503920335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m355573171_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m355573171(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m355573171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3080814233_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3080814233(__this, method) ((  bool (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3080814233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4069420500_gshared (Dictionary_2_t346110204 * __this, KeyValuePair_2_t2398422722  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4069420500(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t346110204 *, KeyValuePair_2_t2398422722 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4069420500_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2538739880_gshared (Dictionary_2_t346110204 * __this, KeyValuePair_2_t2398422722  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2538739880(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t346110204 *, KeyValuePair_2_t2398422722 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2538739880_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3886499408_gshared (Dictionary_2_t346110204 * __this, KeyValuePair_2U5BU5D_t727157751* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3886499408(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t346110204 *, KeyValuePair_2U5BU5D_t727157751*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3886499408_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3563629043_gshared (Dictionary_2_t346110204 * __this, KeyValuePair_2_t2398422722  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3563629043(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t346110204 *, KeyValuePair_2_t2398422722 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3563629043_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3698783199_gshared (Dictionary_2_t346110204 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3698783199(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3698783199_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3904748118_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3904748118(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3904748118_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2941057881_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2941057881(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2941057881_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2875270548_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2875270548(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2875270548_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2782072199_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2782072199(__this, method) ((  int32_t (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_get_Count_m2782072199_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::get_Item(TKey)
extern "C"  float Dictionary_2_get_Item_m2249844904_gshared (Dictionary_2_t346110204 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2249844904(__this, ___key0, method) ((  float (*) (Dictionary_2_t346110204 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2249844904_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m2928182123_gshared (Dictionary_2_t346110204 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m2928182123(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t346110204 *, int32_t, float, const MethodInfo*))Dictionary_2_set_Item_m2928182123_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3877372043_gshared (Dictionary_2_t346110204 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3877372043(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t346110204 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3877372043_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2914160844_gshared (Dictionary_2_t346110204 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2914160844(__this, ___size0, method) ((  void (*) (Dictionary_2_t346110204 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2914160844_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m559445902_gshared (Dictionary_2_t346110204 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m559445902(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m559445902_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2398422722  Dictionary_2_make_pair_m1334073420_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1334073420(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2398422722  (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_make_pair_m1334073420_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::pick_value(TKey,TValue)
extern "C"  float Dictionary_2_pick_value_m2571838546_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2571838546(__this /* static, unused */, ___key0, ___value1, method) ((  float (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_pick_value_m2571838546_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3404493759_gshared (Dictionary_2_t346110204 * __this, KeyValuePair_2U5BU5D_t727157751* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3404493759(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t346110204 *, KeyValuePair_2U5BU5D_t727157751*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3404493759_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::Resize()
extern "C"  void Dictionary_2_Resize_m2064096977_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2064096977(__this, method) ((  void (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_Resize_m2064096977_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2398760724_gshared (Dictionary_2_t346110204 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2398760724(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t346110204 *, int32_t, float, const MethodInfo*))Dictionary_2_Add_m2398760724_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::Clear()
extern "C"  void Dictionary_2_Clear_m2063618748_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2063618748(__this, method) ((  void (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_Clear_m2063618748_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3520394696_gshared (Dictionary_2_t346110204 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3520394696(__this, ___key0, method) ((  bool (*) (Dictionary_2_t346110204 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3520394696_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m491323360_gshared (Dictionary_2_t346110204 * __this, float ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m491323360(__this, ___value0, method) ((  bool (*) (Dictionary_2_t346110204 *, float, const MethodInfo*))Dictionary_2_ContainsValue_m491323360_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3399634531_gshared (Dictionary_2_t346110204 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3399634531(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t346110204 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3399634531_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1920675507_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1920675507(__this, ___sender0, method) ((  void (*) (Dictionary_2_t346110204 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1920675507_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1552679832_gshared (Dictionary_2_t346110204 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1552679832(__this, ___key0, method) ((  bool (*) (Dictionary_2_t346110204 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1552679832_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2640264307_gshared (Dictionary_2_t346110204 * __this, int32_t ___key0, float* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2640264307(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t346110204 *, int32_t, float*, const MethodInfo*))Dictionary_2_TryGetValue_m2640264307_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::get_Values()
extern "C"  ValueCollection_t3344137343 * Dictionary_2_get_Values_m3795366120_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3795366120(__this, method) ((  ValueCollection_t3344137343 * (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_get_Values_m3795366120_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m4212605065_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4212605065(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t346110204 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4212605065_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::ToTValue(System.Object)
extern "C"  float Dictionary_2_ToTValue_m3631783721_gshared (Dictionary_2_t346110204 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3631783721(__this, ___value0, method) ((  float (*) (Dictionary_2_t346110204 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3631783721_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1585377939_gshared (Dictionary_2_t346110204 * __this, KeyValuePair_2_t2398422722  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1585377939(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t346110204 *, KeyValuePair_2_t2398422722 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1585377939_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::GetEnumerator()
extern "C"  Enumerator_t1666134906  Dictionary_2_GetEnumerator_m2498019038_gshared (Dictionary_2_t346110204 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2498019038(__this, method) ((  Enumerator_t1666134906  (*) (Dictionary_2_t346110204 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2498019038_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m1867131497_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, float ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1867131497(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, float, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1867131497_gshared)(__this /* static, unused */, ___key0, ___value1, method)
