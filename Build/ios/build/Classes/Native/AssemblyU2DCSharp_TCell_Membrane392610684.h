﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_TCell_Piece3459712141.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TCell_Membrane
struct  TCell_Membrane_t392610684  : public TCell_Piece_t3459712141
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
