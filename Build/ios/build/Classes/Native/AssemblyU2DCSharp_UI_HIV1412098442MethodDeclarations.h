﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI_HIV
struct UI_HIV_t1412098442;
// HIV
struct HIV_t2481767745;

#include "codegen/il2cpp-codegen.h"

// System.Void UI_HIV::.ctor()
extern "C"  void UI_HIV__ctor_m539927275 (UI_HIV_t1412098442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HIV UI_HIV::get_selected()
extern "C"  HIV_t2481767745 * UI_HIV_get_selected_m3295493963 (UI_HIV_t1412098442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV::Update()
extern "C"  void UI_HIV_Update_m3889700724 (UI_HIV_t1412098442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV::OnGUI()
extern "C"  void UI_HIV_OnGUI_m448153665 (UI_HIV_t1412098442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI_HIV::.cctor()
extern "C"  void UI_HIV__cctor_m2382461372 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
