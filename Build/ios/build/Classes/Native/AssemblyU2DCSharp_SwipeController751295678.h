﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwipeController
struct  SwipeController_t751295678  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SwipeController::_swipeTolerance
	float ____swipeTolerance_2;
	// System.Single SwipeController::_timeDelay
	float ____timeDelay_3;
	// UnityEngine.Vector3 SwipeController::_initialPosition
	Vector3_t2243707580  ____initialPosition_4;
	// System.Single SwipeController::_dx
	float ____dx_5;
	// System.Single SwipeController::_dy
	float ____dy_6;
	// System.Single SwipeController::_dt
	float ____dt_7;
	// UnityEngine.Vector3 SwipeController::swipeVelocity
	Vector3_t2243707580  ___swipeVelocity_8;
	// UnityEngine.GameObject SwipeController::_initialObject
	GameObject_t1756533147 * ____initialObject_9;

public:
	inline static int32_t get_offset_of__swipeTolerance_2() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____swipeTolerance_2)); }
	inline float get__swipeTolerance_2() const { return ____swipeTolerance_2; }
	inline float* get_address_of__swipeTolerance_2() { return &____swipeTolerance_2; }
	inline void set__swipeTolerance_2(float value)
	{
		____swipeTolerance_2 = value;
	}

	inline static int32_t get_offset_of__timeDelay_3() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____timeDelay_3)); }
	inline float get__timeDelay_3() const { return ____timeDelay_3; }
	inline float* get_address_of__timeDelay_3() { return &____timeDelay_3; }
	inline void set__timeDelay_3(float value)
	{
		____timeDelay_3 = value;
	}

	inline static int32_t get_offset_of__initialPosition_4() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____initialPosition_4)); }
	inline Vector3_t2243707580  get__initialPosition_4() const { return ____initialPosition_4; }
	inline Vector3_t2243707580 * get_address_of__initialPosition_4() { return &____initialPosition_4; }
	inline void set__initialPosition_4(Vector3_t2243707580  value)
	{
		____initialPosition_4 = value;
	}

	inline static int32_t get_offset_of__dx_5() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____dx_5)); }
	inline float get__dx_5() const { return ____dx_5; }
	inline float* get_address_of__dx_5() { return &____dx_5; }
	inline void set__dx_5(float value)
	{
		____dx_5 = value;
	}

	inline static int32_t get_offset_of__dy_6() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____dy_6)); }
	inline float get__dy_6() const { return ____dy_6; }
	inline float* get_address_of__dy_6() { return &____dy_6; }
	inline void set__dy_6(float value)
	{
		____dy_6 = value;
	}

	inline static int32_t get_offset_of__dt_7() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____dt_7)); }
	inline float get__dt_7() const { return ____dt_7; }
	inline float* get_address_of__dt_7() { return &____dt_7; }
	inline void set__dt_7(float value)
	{
		____dt_7 = value;
	}

	inline static int32_t get_offset_of_swipeVelocity_8() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ___swipeVelocity_8)); }
	inline Vector3_t2243707580  get_swipeVelocity_8() const { return ___swipeVelocity_8; }
	inline Vector3_t2243707580 * get_address_of_swipeVelocity_8() { return &___swipeVelocity_8; }
	inline void set_swipeVelocity_8(Vector3_t2243707580  value)
	{
		___swipeVelocity_8 = value;
	}

	inline static int32_t get_offset_of__initialObject_9() { return static_cast<int32_t>(offsetof(SwipeController_t751295678, ____initialObject_9)); }
	inline GameObject_t1756533147 * get__initialObject_9() const { return ____initialObject_9; }
	inline GameObject_t1756533147 ** get_address_of__initialObject_9() { return &____initialObject_9; }
	inline void set__initialObject_9(GameObject_t1756533147 * value)
	{
		____initialObject_9 = value;
		Il2CppCodeGenWriteBarrier(&____initialObject_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
