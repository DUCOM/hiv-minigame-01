﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Statistics>
struct Comparer_1_t1427386126;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<Statistics>::.ctor()
extern "C"  void Comparer_1__ctor_m2052766436_gshared (Comparer_1_t1427386126 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2052766436(__this, method) ((  void (*) (Comparer_1_t1427386126 *, const MethodInfo*))Comparer_1__ctor_m2052766436_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Statistics>::.cctor()
extern "C"  void Comparer_1__cctor_m3215950135_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m3215950135(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m3215950135_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Statistics>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2954925263_gshared (Comparer_1_t1427386126 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m2954925263(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t1427386126 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m2954925263_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Statistics>::get_Default()
extern "C"  Comparer_1_t1427386126 * Comparer_1_get_Default_m1410002692_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m1410002692(__this /* static, unused */, method) ((  Comparer_1_t1427386126 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m1410002692_gshared)(__this /* static, unused */, method)
