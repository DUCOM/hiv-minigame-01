﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_207556710.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3091761441_gshared (KeyValuePair_2_t207556710 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3091761441(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t207556710 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3091761441_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m621784971_gshared (KeyValuePair_2_t207556710 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m621784971(__this, method) ((  int32_t (*) (KeyValuePair_2_t207556710 *, const MethodInfo*))KeyValuePair_2_get_Key_m621784971_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2638512080_gshared (KeyValuePair_2_t207556710 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2638512080(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t207556710 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2638512080_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2717787259_gshared (KeyValuePair_2_t207556710 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2717787259(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t207556710 *, const MethodInfo*))KeyValuePair_2_get_Value_m2717787259_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1193603312_gshared (KeyValuePair_2_t207556710 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1193603312(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t207556710 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1193603312_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3238324930_gshared (KeyValuePair_2_t207556710 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3238324930(__this, method) ((  String_t* (*) (KeyValuePair_2_t207556710 *, const MethodInfo*))KeyValuePair_2_ToString_m3238324930_gshared)(__this, method)
