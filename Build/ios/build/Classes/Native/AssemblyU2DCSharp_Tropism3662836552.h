﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tropism/CoReceptorType[]
struct CoReceptorTypeU5BU5D_t3312104658;
// Tropism/ReceptorType[]
struct ReceptorTypeU5BU5D_t2292174278;
// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single>
struct Dictionary_2_t346110204;
// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_Tropism_ReceptorType1016603615.h"
#include "AssemblyU2DCSharp_Tropism_CoReceptorType161380547.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tropism
struct  Tropism_t3662836552 
{
public:
	// Tropism/ReceptorType Tropism::receptor
	int32_t ___receptor_10;
	// Tropism/CoReceptorType Tropism::coreceptor
	int32_t ___coreceptor_11;

public:
	inline static int32_t get_offset_of_receptor_10() { return static_cast<int32_t>(offsetof(Tropism_t3662836552, ___receptor_10)); }
	inline int32_t get_receptor_10() const { return ___receptor_10; }
	inline int32_t* get_address_of_receptor_10() { return &___receptor_10; }
	inline void set_receptor_10(int32_t value)
	{
		___receptor_10 = value;
	}

	inline static int32_t get_offset_of_coreceptor_11() { return static_cast<int32_t>(offsetof(Tropism_t3662836552, ___coreceptor_11)); }
	inline int32_t get_coreceptor_11() const { return ___coreceptor_11; }
	inline int32_t* get_address_of_coreceptor_11() { return &___coreceptor_11; }
	inline void set_coreceptor_11(int32_t value)
	{
		___coreceptor_11 = value;
	}
};

struct Tropism_t3662836552_StaticFields
{
public:
	// Tropism/CoReceptorType[] Tropism::TCellCoreceptor
	CoReceptorTypeU5BU5D_t3312104658* ___TCellCoreceptor_0;
	// Tropism/ReceptorType[] Tropism::TCellReceptor
	ReceptorTypeU5BU5D_t2292174278* ___TCellReceptor_1;
	// Tropism/CoReceptorType[] Tropism::HIVCoreceptor
	CoReceptorTypeU5BU5D_t3312104658* ___HIVCoreceptor_2;
	// Tropism/ReceptorType[] Tropism::HIVReceptor
	ReceptorTypeU5BU5D_t2292174278* ___HIVReceptor_3;
	// System.Collections.Generic.Dictionary`2<Tropism/ReceptorType,System.Single> Tropism::receptorProbabilities
	Dictionary_2_t346110204 * ___receptorProbabilities_4;
	// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single> Tropism::coReceptorProbabilities
	Dictionary_2_t1366040584 * ___coReceptorProbabilities_5;
	// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single> Tropism::cd4CoReceptorProbabilities
	Dictionary_2_t1366040584 * ___cd4CoReceptorProbabilities_6;
	// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single> Tropism::hivProbabilities
	Dictionary_2_t1366040584 * ___hivProbabilities_7;
	// UnityEngine.Color[] Tropism::receptorDecoyColorList
	ColorU5BU5D_t672350442* ___receptorDecoyColorList_8;
	// UnityEngine.Color[] Tropism::coreceptorDecoyColorList
	ColorU5BU5D_t672350442* ___coreceptorDecoyColorList_9;

public:
	inline static int32_t get_offset_of_TCellCoreceptor_0() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___TCellCoreceptor_0)); }
	inline CoReceptorTypeU5BU5D_t3312104658* get_TCellCoreceptor_0() const { return ___TCellCoreceptor_0; }
	inline CoReceptorTypeU5BU5D_t3312104658** get_address_of_TCellCoreceptor_0() { return &___TCellCoreceptor_0; }
	inline void set_TCellCoreceptor_0(CoReceptorTypeU5BU5D_t3312104658* value)
	{
		___TCellCoreceptor_0 = value;
		Il2CppCodeGenWriteBarrier(&___TCellCoreceptor_0, value);
	}

	inline static int32_t get_offset_of_TCellReceptor_1() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___TCellReceptor_1)); }
	inline ReceptorTypeU5BU5D_t2292174278* get_TCellReceptor_1() const { return ___TCellReceptor_1; }
	inline ReceptorTypeU5BU5D_t2292174278** get_address_of_TCellReceptor_1() { return &___TCellReceptor_1; }
	inline void set_TCellReceptor_1(ReceptorTypeU5BU5D_t2292174278* value)
	{
		___TCellReceptor_1 = value;
		Il2CppCodeGenWriteBarrier(&___TCellReceptor_1, value);
	}

	inline static int32_t get_offset_of_HIVCoreceptor_2() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___HIVCoreceptor_2)); }
	inline CoReceptorTypeU5BU5D_t3312104658* get_HIVCoreceptor_2() const { return ___HIVCoreceptor_2; }
	inline CoReceptorTypeU5BU5D_t3312104658** get_address_of_HIVCoreceptor_2() { return &___HIVCoreceptor_2; }
	inline void set_HIVCoreceptor_2(CoReceptorTypeU5BU5D_t3312104658* value)
	{
		___HIVCoreceptor_2 = value;
		Il2CppCodeGenWriteBarrier(&___HIVCoreceptor_2, value);
	}

	inline static int32_t get_offset_of_HIVReceptor_3() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___HIVReceptor_3)); }
	inline ReceptorTypeU5BU5D_t2292174278* get_HIVReceptor_3() const { return ___HIVReceptor_3; }
	inline ReceptorTypeU5BU5D_t2292174278** get_address_of_HIVReceptor_3() { return &___HIVReceptor_3; }
	inline void set_HIVReceptor_3(ReceptorTypeU5BU5D_t2292174278* value)
	{
		___HIVReceptor_3 = value;
		Il2CppCodeGenWriteBarrier(&___HIVReceptor_3, value);
	}

	inline static int32_t get_offset_of_receptorProbabilities_4() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___receptorProbabilities_4)); }
	inline Dictionary_2_t346110204 * get_receptorProbabilities_4() const { return ___receptorProbabilities_4; }
	inline Dictionary_2_t346110204 ** get_address_of_receptorProbabilities_4() { return &___receptorProbabilities_4; }
	inline void set_receptorProbabilities_4(Dictionary_2_t346110204 * value)
	{
		___receptorProbabilities_4 = value;
		Il2CppCodeGenWriteBarrier(&___receptorProbabilities_4, value);
	}

	inline static int32_t get_offset_of_coReceptorProbabilities_5() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___coReceptorProbabilities_5)); }
	inline Dictionary_2_t1366040584 * get_coReceptorProbabilities_5() const { return ___coReceptorProbabilities_5; }
	inline Dictionary_2_t1366040584 ** get_address_of_coReceptorProbabilities_5() { return &___coReceptorProbabilities_5; }
	inline void set_coReceptorProbabilities_5(Dictionary_2_t1366040584 * value)
	{
		___coReceptorProbabilities_5 = value;
		Il2CppCodeGenWriteBarrier(&___coReceptorProbabilities_5, value);
	}

	inline static int32_t get_offset_of_cd4CoReceptorProbabilities_6() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___cd4CoReceptorProbabilities_6)); }
	inline Dictionary_2_t1366040584 * get_cd4CoReceptorProbabilities_6() const { return ___cd4CoReceptorProbabilities_6; }
	inline Dictionary_2_t1366040584 ** get_address_of_cd4CoReceptorProbabilities_6() { return &___cd4CoReceptorProbabilities_6; }
	inline void set_cd4CoReceptorProbabilities_6(Dictionary_2_t1366040584 * value)
	{
		___cd4CoReceptorProbabilities_6 = value;
		Il2CppCodeGenWriteBarrier(&___cd4CoReceptorProbabilities_6, value);
	}

	inline static int32_t get_offset_of_hivProbabilities_7() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___hivProbabilities_7)); }
	inline Dictionary_2_t1366040584 * get_hivProbabilities_7() const { return ___hivProbabilities_7; }
	inline Dictionary_2_t1366040584 ** get_address_of_hivProbabilities_7() { return &___hivProbabilities_7; }
	inline void set_hivProbabilities_7(Dictionary_2_t1366040584 * value)
	{
		___hivProbabilities_7 = value;
		Il2CppCodeGenWriteBarrier(&___hivProbabilities_7, value);
	}

	inline static int32_t get_offset_of_receptorDecoyColorList_8() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___receptorDecoyColorList_8)); }
	inline ColorU5BU5D_t672350442* get_receptorDecoyColorList_8() const { return ___receptorDecoyColorList_8; }
	inline ColorU5BU5D_t672350442** get_address_of_receptorDecoyColorList_8() { return &___receptorDecoyColorList_8; }
	inline void set_receptorDecoyColorList_8(ColorU5BU5D_t672350442* value)
	{
		___receptorDecoyColorList_8 = value;
		Il2CppCodeGenWriteBarrier(&___receptorDecoyColorList_8, value);
	}

	inline static int32_t get_offset_of_coreceptorDecoyColorList_9() { return static_cast<int32_t>(offsetof(Tropism_t3662836552_StaticFields, ___coreceptorDecoyColorList_9)); }
	inline ColorU5BU5D_t672350442* get_coreceptorDecoyColorList_9() const { return ___coreceptorDecoyColorList_9; }
	inline ColorU5BU5D_t672350442** get_address_of_coreceptorDecoyColorList_9() { return &___coreceptorDecoyColorList_9; }
	inline void set_coreceptorDecoyColorList_9(ColorU5BU5D_t672350442* value)
	{
		___coreceptorDecoyColorList_9 = value;
		Il2CppCodeGenWriteBarrier(&___coreceptorDecoyColorList_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
