﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_Tropism3662836552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spike
struct  Spike_t60554746  : public MonoBehaviour_t1158329972
{
public:
	// Tropism Spike::_tropism
	Tropism_t3662836552  ____tropism_2;

public:
	inline static int32_t get_offset_of__tropism_2() { return static_cast<int32_t>(offsetof(Spike_t60554746, ____tropism_2)); }
	inline Tropism_t3662836552  get__tropism_2() const { return ____tropism_2; }
	inline Tropism_t3662836552 * get_address_of__tropism_2() { return &____tropism_2; }
	inline void set__tropism_2(Tropism_t3662836552  value)
	{
		____tropism_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
