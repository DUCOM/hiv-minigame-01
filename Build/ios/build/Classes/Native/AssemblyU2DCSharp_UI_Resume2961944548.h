﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UI_Pause
struct UI_Pause_t1107290467;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI_Resume
struct  UI_Resume_t2961944548  : public MonoBehaviour_t1158329972
{
public:
	// UI_Pause UI_Resume::pause
	UI_Pause_t1107290467 * ___pause_2;

public:
	inline static int32_t get_offset_of_pause_2() { return static_cast<int32_t>(offsetof(UI_Resume_t2961944548, ___pause_2)); }
	inline UI_Pause_t1107290467 * get_pause_2() const { return ___pause_2; }
	inline UI_Pause_t1107290467 ** get_address_of_pause_2() { return &___pause_2; }
	inline void set_pause_2(UI_Pause_t1107290467 * value)
	{
		___pause_2 = value;
		Il2CppCodeGenWriteBarrier(&___pause_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
