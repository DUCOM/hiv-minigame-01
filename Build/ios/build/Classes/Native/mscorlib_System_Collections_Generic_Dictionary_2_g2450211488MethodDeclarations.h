﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>
struct Dictionary_2_t2450211488;
// System.Collections.Generic.IEqualityComparer`1<Antibody/State>
struct IEqualityComparer_1_t342336956;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>[]
struct KeyValuePair_2U5BU5D_t3914306371;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Antibody/State,System.Object>>
struct IEnumerator_1_t1978047833;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<Antibody/State,System.Object>
struct ValueCollection_t1153271331;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_207556710.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Antibody_State1129704178.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3770236190.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3552348391_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3552348391(__this, method) ((  void (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2__ctor_m3552348391_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3567442344_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3567442344(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3567442344_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2719505340_gshared (Dictionary_2_t2450211488 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2719505340(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2450211488 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2719505340_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3079463670_gshared (Dictionary_2_t2450211488 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3079463670(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2450211488 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m3079463670_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2371462867_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2371462867(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2450211488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2371462867_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m894163986_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m894163986(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m894163986_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1473781785_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1473781785(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1473781785_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1930233190_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1930233190(__this, ___key0, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1930233190_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3901608123_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3901608123(__this, method) ((  bool (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3901608123_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3456422031_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3456422031(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3456422031_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3186529073_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3186529073(__this, method) ((  bool (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3186529073_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2219963708_gshared (Dictionary_2_t2450211488 * __this, KeyValuePair_2_t207556710  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2219963708(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2450211488 *, KeyValuePair_2_t207556710 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2219963708_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2066614756_gshared (Dictionary_2_t2450211488 * __this, KeyValuePair_2_t207556710  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2066614756(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2450211488 *, KeyValuePair_2_t207556710 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2066614756_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2804787976_gshared (Dictionary_2_t2450211488 * __this, KeyValuePair_2U5BU5D_t3914306371* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2804787976(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2450211488 *, KeyValuePair_2U5BU5D_t3914306371*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2804787976_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m853381695_gshared (Dictionary_2_t2450211488 * __this, KeyValuePair_2_t207556710  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m853381695(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2450211488 *, KeyValuePair_2_t207556710 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m853381695_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4278642299_gshared (Dictionary_2_t2450211488 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4278642299(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4278642299_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3021503570_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3021503570(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3021503570_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1938756785_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1938756785(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1938756785_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m738343636_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m738343636(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m738343636_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3974274627_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3974274627(__this, method) ((  int32_t (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_get_Count_m3974274627_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3406209688_gshared (Dictionary_2_t2450211488 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3406209688(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2450211488 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3406209688_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3553696751_gshared (Dictionary_2_t2450211488 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3553696751(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2450211488 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3553696751_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3219439631_gshared (Dictionary_2_t2450211488 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3219439631(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2450211488 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3219439631_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3049176984_gshared (Dictionary_2_t2450211488 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3049176984(__this, ___size0, method) ((  void (*) (Dictionary_2_t2450211488 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3049176984_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3313682894_gshared (Dictionary_2_t2450211488 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3313682894(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3313682894_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t207556710  Dictionary_2_make_pair_m4167953216_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4167953216(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t207556710  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m4167953216_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2406210326_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2406210326(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2406210326_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2130097243_gshared (Dictionary_2_t2450211488 * __this, KeyValuePair_2U5BU5D_t3914306371* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2130097243(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2450211488 *, KeyValuePair_2U5BU5D_t3914306371*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2130097243_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m2809446721_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2809446721(__this, method) ((  void (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_Resize_m2809446721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m162827848_gshared (Dictionary_2_t2450211488 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m162827848(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2450211488 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m162827848_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2735080552_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2735080552(__this, method) ((  void (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_Clear_m2735080552_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4148981408_gshared (Dictionary_2_t2450211488 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4148981408(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2450211488 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m4148981408_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2495751008_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2495751008(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2450211488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2495751008_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2593436087_gshared (Dictionary_2_t2450211488 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2593436087(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2450211488 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2593436087_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1193273199_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1193273199(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2450211488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1193273199_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m336002140_gshared (Dictionary_2_t2450211488 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m336002140(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2450211488 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m336002140_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1334320335_gshared (Dictionary_2_t2450211488 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1334320335(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2450211488 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1334320335_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::get_Values()
extern "C"  ValueCollection_t1153271331 * Dictionary_2_get_Values_m2074151616_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2074151616(__this, method) ((  ValueCollection_t1153271331 * (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_get_Values_m2074151616_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1988909601_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1988909601(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2450211488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1988909601_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3961521665_gshared (Dictionary_2_t2450211488 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3961521665(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t2450211488 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3961521665_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m51113687_gshared (Dictionary_2_t2450211488 * __this, KeyValuePair_2_t207556710  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m51113687(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2450211488 *, KeyValuePair_2_t207556710 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m51113687_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3770236190  Dictionary_2_GetEnumerator_m2212435794_gshared (Dictionary_2_t2450211488 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2212435794(__this, method) ((  Enumerator_t3770236190  (*) (Dictionary_2_t2450211488 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2212435794_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Antibody/State,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2530598441_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2530598441(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2530598441_gshared)(__this /* static, unused */, ___key0, ___value1, method)
