﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>
struct ValueCollection_t69100427;
// System.Collections.Generic.Dictionary`2<Tropism/CoReceptorType,System.Single>
struct Dictionary_2_t1366040584;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3847001055;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3052573348.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2545175952_gshared (ValueCollection_t69100427 * __this, Dictionary_2_t1366040584 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2545175952(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t69100427 *, Dictionary_2_t1366040584 *, const MethodInfo*))ValueCollection__ctor_m2545175952_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3549076950_gshared (ValueCollection_t69100427 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3549076950(__this, ___item0, method) ((  void (*) (ValueCollection_t69100427 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3549076950_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m767344075_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m767344075(__this, method) ((  void (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m767344075_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875299174_gshared (ValueCollection_t69100427 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875299174(__this, ___item0, method) ((  bool (*) (ValueCollection_t69100427 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875299174_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3524800051_gshared (ValueCollection_t69100427 * __this, float ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3524800051(__this, ___item0, method) ((  bool (*) (ValueCollection_t69100427 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3524800051_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1874328689_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1874328689(__this, method) ((  Il2CppObject* (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1874328689_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m59720881_gshared (ValueCollection_t69100427 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m59720881(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t69100427 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m59720881_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1497699394_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1497699394(__this, method) ((  Il2CppObject * (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1497699394_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2545275415_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2545275415(__this, method) ((  bool (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2545275415_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m116462773_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m116462773(__this, method) ((  bool (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m116462773_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1446665773_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1446665773(__this, method) ((  Il2CppObject * (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1446665773_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1848659795_gshared (ValueCollection_t69100427 * __this, SingleU5BU5D_t577127397* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1848659795(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t69100427 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1848659795_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::GetEnumerator()
extern "C"  Enumerator_t3052573348  ValueCollection_GetEnumerator_m3632326872_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3632326872(__this, method) ((  Enumerator_t3052573348  (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_GetEnumerator_m3632326872_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Tropism/CoReceptorType,System.Single>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3811002129_gshared (ValueCollection_t69100427 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3811002129(__this, method) ((  int32_t (*) (ValueCollection_t69100427 *, const MethodInfo*))ValueCollection_get_Count_m3811002129_gshared)(__this, method)
