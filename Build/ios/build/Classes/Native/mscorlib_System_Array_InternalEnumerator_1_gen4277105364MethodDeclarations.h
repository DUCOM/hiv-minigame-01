﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4277105364.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23418353102.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3243173460_gshared (InternalEnumerator_1_t4277105364 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3243173460(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4277105364 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3243173460_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3250922452_gshared (InternalEnumerator_1_t4277105364 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3250922452(__this, method) ((  void (*) (InternalEnumerator_1_t4277105364 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3250922452_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2359047842_gshared (InternalEnumerator_1_t4277105364 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2359047842(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4277105364 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2359047842_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m498457359_gshared (InternalEnumerator_1_t4277105364 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m498457359(__this, method) ((  void (*) (InternalEnumerator_1_t4277105364 *, const MethodInfo*))InternalEnumerator_1_Dispose_m498457359_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2120511900_gshared (InternalEnumerator_1_t4277105364 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2120511900(__this, method) ((  bool (*) (InternalEnumerator_1_t4277105364 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2120511900_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Tropism/CoReceptorType,System.Single>>::get_Current()
extern "C"  KeyValuePair_2_t3418353102  InternalEnumerator_1_get_Current_m2202666363_gshared (InternalEnumerator_1_t4277105364 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2202666363(__this, method) ((  KeyValuePair_2_t3418353102  (*) (InternalEnumerator_1_t4277105364 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2202666363_gshared)(__this, method)
