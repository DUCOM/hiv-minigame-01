﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<SpriteRender>
struct List_1_t2197938481;
// System.Collections.Generic.List`1<ImageRender>
struct List_1_t3149308319;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupColors
struct  GroupColors_t153290803  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<SpriteRender> GroupColors::spriteList
	List_1_t2197938481 * ___spriteList_2;
	// System.Collections.Generic.List`1<ImageRender> GroupColors::imageList
	List_1_t3149308319 * ___imageList_3;
	// System.Boolean GroupColors::common
	bool ___common_4;
	// UnityEngine.Color GroupColors::commonColor
	Color_t2020392075  ___commonColor_5;

public:
	inline static int32_t get_offset_of_spriteList_2() { return static_cast<int32_t>(offsetof(GroupColors_t153290803, ___spriteList_2)); }
	inline List_1_t2197938481 * get_spriteList_2() const { return ___spriteList_2; }
	inline List_1_t2197938481 ** get_address_of_spriteList_2() { return &___spriteList_2; }
	inline void set_spriteList_2(List_1_t2197938481 * value)
	{
		___spriteList_2 = value;
		Il2CppCodeGenWriteBarrier(&___spriteList_2, value);
	}

	inline static int32_t get_offset_of_imageList_3() { return static_cast<int32_t>(offsetof(GroupColors_t153290803, ___imageList_3)); }
	inline List_1_t3149308319 * get_imageList_3() const { return ___imageList_3; }
	inline List_1_t3149308319 ** get_address_of_imageList_3() { return &___imageList_3; }
	inline void set_imageList_3(List_1_t3149308319 * value)
	{
		___imageList_3 = value;
		Il2CppCodeGenWriteBarrier(&___imageList_3, value);
	}

	inline static int32_t get_offset_of_common_4() { return static_cast<int32_t>(offsetof(GroupColors_t153290803, ___common_4)); }
	inline bool get_common_4() const { return ___common_4; }
	inline bool* get_address_of_common_4() { return &___common_4; }
	inline void set_common_4(bool value)
	{
		___common_4 = value;
	}

	inline static int32_t get_offset_of_commonColor_5() { return static_cast<int32_t>(offsetof(GroupColors_t153290803, ___commonColor_5)); }
	inline Color_t2020392075  get_commonColor_5() const { return ___commonColor_5; }
	inline Color_t2020392075 * get_address_of_commonColor_5() { return &___commonColor_5; }
	inline void set_commonColor_5(Color_t2020392075  value)
	{
		___commonColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
