﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Bar_Slider : MonoBehaviour {

    RectTransform r;

    private float timer = 1f;
    public void SetTimer(float t)
    {
        if (t <= 0f) t = 0f;
        timer = t;
    }
    [SerializeField]
    private float moveSpeed;

    private float speed;

    [SerializeField]
    private float interpolation;

	// Use this for initialization
	void Start () {
        r = GetComponent<RectTransform>();
        speed = 0;
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
	}

    void FixedUpdate()
    {
        if (timer < 0f)
        {
            speed = Mathf.Lerp(speed, moveSpeed, interpolation);
            r.position += new Vector3(speed, 0);
        }
    }

    void OnGui()
    {

    }
}
