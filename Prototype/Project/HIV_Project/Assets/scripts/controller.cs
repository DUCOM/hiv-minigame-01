﻿using UnityEngine;
using System.Collections;
using System;

/*
 *  Name:           controller.cs
 *  Version:        0.0.1
 *  Author:         Vincent Mills
 *  Date:           27 September 2016
 *  Last Edited by: Vincent Mills
 */


/// <summary>
/// CONTROLS STRUCT:
///     Shows the basic keyboard controls for the game.
///     NOT FOR MOUSE CONTROLS NOR TOUCH CONTROLS
/// </summary>
struct Controls
{
    public KeyCode up; //the up button
    public KeyCode down;
    public KeyCode left;
    public KeyCode right;
    public KeyCode enter;
    public KeyCode back;
}



/// <summary>
/// CONTROLLER CLASS:
///     sets the controls of the game at the start. 
/// </summary>
public class Controller
{
    public enum Buttons
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        ENTER,
        BACK
    }

    private Controls c;

    public KeyCode up { get { return c.up; } }
    public KeyCode down { get { return c.down; } }
    public KeyCode left { get { return c.left; } }
    public KeyCode right { get { return c.right; } }
    public KeyCode enter { get { return c.enter; } }
    public KeyCode back { get { return c.back; } }

    private KeyCode FromString(string s)
    {
        s = s.ToLower();
        if (s == "up") return up;
        if (s == "down") return down;
        if (s == "left") return left;
        if (s == "right") return right;
        if (s == "enter" || s == "return") return enter;
        if (s == "back") return back;
        Debug.LogWarning("\"" + s + "\""+ " is an invalid control option!");
        return KeyCode.None;
    }

    public KeyCode findKey(Buttons b)
    {
        switch (b)
        {
            case Buttons.BACK:
                return back;
            case Buttons.DOWN:
                return down;
            case Buttons.ENTER:
                return enter;
            case Buttons.LEFT:
                return left;
            case Buttons.RIGHT:
                return right;
            case Buttons.UP:
                return up;
            default:
                return KeyCode.None;
        }
    }

    public bool getKey(Buttons b) { return Input.GetKey(findKey(b)); }
    public bool getKeyDown(Buttons b) { return Input.GetKeyDown(findKey(b)); }
    public bool getKeyUp(Buttons b) { return Input.GetKeyUp(findKey(b)); }

    public bool getKey(string s) { return Input.GetKey(FromString(s)); }
    public bool getKeyDown(string s) { return Input.GetKeyDown(FromString(s)); }
    public bool getKeyUp(string s) { return Input.GetKeyUp(FromString(s)); }

    //I know this looks a bit weird
    public Controller   (   KeyCode up = KeyCode.UpArrow,
                            KeyCode down = KeyCode.DownArrow,
                            KeyCode left = KeyCode.LeftArrow,
                            KeyCode right = KeyCode.RightArrow,
                            KeyCode enter = KeyCode.Space,
                            KeyCode back = KeyCode.Escape
                        )
    {
        c.up = up;
        c.down = down;
        c.left = left;
        c.right = right;
        c.back = back;
        c.enter = enter;
    }
}

public class MouseController
{
    public float x { get { return Input.mousePosition.x;  } }
    public float y { get { return Input.mousePosition.y; } }
    public KeyCode l_click { get { return KeyCode.Mouse0; } }
    public KeyCode r_click { get { return KeyCode.Mouse1; } }
}
