﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The different options for controls:
/// </summary>
enum ControlOptions
{    
    KEYBOARD,
    MOUSE,
    TOUCH
}


public class MoveController : MonoBehaviour {

    [SerializeField]
    private float max_speed;

    [SerializeField]
    private ControlOptions options;

    [SerializeField]
    private float cutoff_;

    private float cutoff { get { return cutoff_; } }

    Controller controller_ = new Controller();

    private Rigidbody2D _rb;

    public Controller controller { get { return controller_; } }

    private bool init = false;

    


	// Use this for initialization
	void Start () {
	    if (!init)
        {
            _rb = gameObject.GetComponent<Rigidbody2D>();
            if (_rb == null)
                _rb = gameObject.AddComponent<Rigidbody2D>();
            _rb.gravityScale = 0f;

            init = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
        move();
        //if (Input.GetKey(KeyCode.Escape)) Application.Quit();
	}

    /// <summary>
    /// The movement for the MoveController
    /// </summary>
    void move()
    {
        switch (options)
        {
            case ControlOptions.KEYBOARD:
                Vector2 direction = setDirection();
                _rb.velocity = Vector2.Lerp(_rb.velocity, direction.normalized * max_speed, 0.1f);

                break;
        }

    }

    /// <summary>
    /// SETS the direction of an object:
    ///     only for Keyboard Controls for now
    /// </summary>
    /// <returns>A normalized Vector on which way to go</returns>
    Vector2 setDirection(ControlOptions c = ControlOptions.KEYBOARD)
    {
        float x = 0f, y = 0f;
        float aspectRatio = (float)Camera.main.pixelWidth / (float)Camera.main.pixelHeight;

        switch (c)
        {
            case ControlOptions.MOUSE:
                break;
            default:
                if (controller.getKey("left") && transform.position.x > -Camera.main.orthographicSize * aspectRatio * cutoff)
                    x = -1f;
                else if (controller.getKey("right") && transform.position.x < Camera.main.orthographicSize * aspectRatio * cutoff)
                    x = 1f;

                if (controller.getKey("up") && transform.position.y < Camera.main.orthographicSize * cutoff)
                    y = 1f;
                else if (controller.getKey("down") && transform.position.y > -Camera.main.orthographicSize * cutoff)
                    y = -1f;
                break;
        }
        return new Vector2(x, y).normalized;
    }


    public bool confirm()
    {
        switch (options)
        {
            case ControlOptions.KEYBOARD:
                return Input.GetKeyDown(controller.enter);
            case ControlOptions.MOUSE:
                return Input.GetMouseButtonDown(0);
            default:
                return false;
        }

    }
}
