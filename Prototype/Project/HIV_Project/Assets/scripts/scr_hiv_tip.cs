﻿using UnityEngine;
using System.Collections;

public class scr_hiv_tip : MonoBehaviour {


    private MoveController controller_;
    private scr_hiv parent_;

    public MoveController controller { get { return controller_; } }

    /// <summary>
    /// The compatible spike that the gp-120 is colliding with
    /// </summary>
    Transform t;

    Transform spike;


    // Use this for initialization
    void Start() {
        t = null;
        parent_ = getParent(gameObject.transform).gameObject.GetComponent<scr_hiv>();
        //parent_ = getParent(gameObject.transform);
        if (parent_ != null)
            controller_ = parent_.controller;
        else
            Destroy(gameObject);

        Debug.Log(parent_.ToString());
    }

    // Update is called once per frame
    void Update() {
        if (controller_ == null)
            Start();
        if (parent_.wanderTimer <= 0f && controller_.confirm() && t != null)
        {
            if (parent_.getState() == scr_hiv.State.WANDER)
            {
                parent_.setHost(t.GetComponentInParent<t_cell>());
                if (parent_.host == null)
                    throw new System.Exception("This is not attachced to a t-cell");
                parent_.switchState(scr_hiv.State.CHECK);
                Vector2 distance = new Vector2();
                if (transform.childCount <= 0)
                    distance = spike.position - gameObject.transform.position;
                else
                    distance = spike.position - transform.GetChild(0).position;
                parent_.transform.Translate(distance);

                parent_.setCameraInstance(transform);
            }
        }
    }

    Transform getParent(Transform current)
    {
        scr_hiv result = current.gameObject.GetComponent<scr_hiv>();
        if (current == null || current.gameObject == null) return null;
        if (result) return result.gameObject.transform;
        if (current.parent == null || current.parent.transform == null)
            return null;
        return getParent(current.parent.transform);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        Receptor r = c.gameObject.GetComponent<Receptor>();
        if (r)
        {
            scr_pairing p = r.parent;
            if (p)
            {
                if (p.pair == parent_.pair)
                    t = r.gameObject.transform;
            }
            spike = r.transform;
        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.transform == t)
            t = null;
    }

    void OnTriggerStay2D(Collider2D c)
    {

    }
}
