﻿using UnityEngine;
using System.Collections;

public class Display : MonoBehaviour {

    SpriteRenderer sprite;
	// Use this for initialization
	public void Start () {
        refreshSprite();
	}
	
    void refreshSprite()
    {
        if (sprite == null)
        {
            sprite = GetComponent<SpriteRenderer>();
            if (sprite == null)
                sprite = gameObject.AddComponent<SpriteRenderer>();
        }
    }
    public void set_saturation(float s)
    {
        refreshSprite();
        sprite.color = new Color(s, s, s);
    }
    public float get_saturation()
    {
        refreshSprite();
        return 1 - Functions.getSaturation(sprite.color);
    }
}
