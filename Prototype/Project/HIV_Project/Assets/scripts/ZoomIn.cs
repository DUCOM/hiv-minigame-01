﻿using UnityEngine;
using System.Collections;

public class ZoomIn : MonoBehaviour {

    /// <summary>
    /// The Corner of the camera you need
    /// </summary>
    enum Corner
    {
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        TOP_LEFT,
        TOP_RIGHT,
        CENTER
    }

    [SerializeField]
    private Transform center_;

    private Camera c_;


    [SerializeField]
    private float zoom_;

    [SerializeField]
    private Vector2 pos;

    [SerializeField]
    private Vector2 size;


	// Use this for initialization
	void Start () {
        c_ = gameObject.GetComponent<Camera>();
        if (c_ == null)
            Debug.LogWarning("Camera not found", this.gameObject);
        setCamera();
        setCenter(center_);
        moveTo();
	}

    void setCamera()
    {
        if (zoom_ == 0) zoom_ = 1;
        //c_.orthographicSize = Camera.main.orthographicSize / zoom_;
        c_.rect = new Rect(pos, size);
        c_.orthographicSize = Camera.main.orthographicSize * c_.rect.height / zoom_;
    }

    public void setCenter(Transform t)
    {
        if (t == transform || center_ == t) return;
        center_ = t;
        moveTo(); 
    }

    /// <summary>
    /// The corner used when moving by default
    /// </summary>
    private const Corner defaultCorner = Corner.CENTER;

    void moveTo(Corner c = defaultCorner) { moveTo(center_, c); }

    void moveTo(Transform t, Corner c = defaultCorner)
    {
        if (t == null) return;
        moveTo(t.position, c);
    }

    void moveTo(Vector2 worldPos, Corner c = defaultCorner)
    {
        if (c_ == null)
            Start();

        transform.position = new Vector3(worldPos.x, worldPos.y, -10f);
        Vector2 ca = Camera.main.WorldToViewportPoint(worldPos);
        switch (c)
        {
            case Corner.BOTTOM_LEFT:
                pos = ca;
                break;
            case Corner.BOTTOM_RIGHT:
                pos = ca + new Vector2(c_.rect.width, 0);
                break;
            case Corner.TOP_LEFT:
                pos = ca - new Vector2(0, c_.rect.height);
                break;
            case Corner.CENTER:
                pos = ca - new Vector2(c_.rect.width / 2, c_.rect.height / 2);// - new Vector2(s.x, 0);
                break;
            default:
                pos = ca - new Vector2(c_.rect.width, c_.rect.height);
                break;
        }
        
        c_.rect = new Rect(pos, size);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return)) moveTo(Corner.CENTER);
	}
}
