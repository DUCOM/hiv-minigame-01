﻿using UnityEngine;


public class scr_pairing : MonoBehaviour {

    

    [SerializeField]
    private Pairing pair_;

    public Pairing pair { get { return pair_; } }


    public Receptor_Type type { get { return pair.type; } }
    public Co_Receptor_Type ctype { get { return pair.ctype; } }


    CoReceptor co_receptor;
    Receptor receptor;

	// Use this for initialization
	public void Start () {
        findReceptors();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void findReceptors()
    {
        receptor = gameObject.GetComponentInChildren<Receptor>();
        if (receptor != null)
            receptor.changeColor(getColor(type));
        co_receptor = gameObject.GetComponentInChildren<CoReceptor>();
        if (co_receptor != null)
            co_receptor.changeColor(getColor(ctype));
    }

    public void setType(Receptor_Type t)
    {
        setType(t, ctype);
    }

    public void setType(Receptor_Type t, Co_Receptor_Type ct)
    {
        if (t != type)
            receptor.changeColor(getColor(t));
        if (ct != ctype)
            co_receptor.changeColor(getColor(ct));

        pair_ = new Pairing(t, ct);
    }
    
    Color getColor(Receptor_Type t)
    {

        switch (t)
        {
            case Receptor_Type.CD4_POSITIVE:
                return Color.red;
            case Receptor_Type.CD4_NEGATIVE:
                return Color.blue;
            default:
                return Color.white;
        }
    }

    Color getColor(Co_Receptor_Type ct)
    {
        switch (ct)
        {
            case Co_Receptor_Type.CCR5:
                return Color.yellow;
            case Co_Receptor_Type.CXCR4:
                return Color.black;
            default:
                return Color.white;
        }
    }
    public void changeSatuation(float s)
    {
        receptor.changeColor(Functions.changeSaturation(receptor.getColor(), s));
        co_receptor.changeColor(Functions.changeSaturation(co_receptor.getColor(), s));
    }
}
