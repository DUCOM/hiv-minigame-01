﻿using UnityEngine;
using System.Collections;

public class CoReceptor : MonoBehaviour {

    SpriteRenderer sprite;

    // Use this for initialization
    public void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        if (sprite == null)
            sprite = gameObject.AddComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public Color getColor() { return sprite.color; }
    public void changeColor(Color c)
    {
        if (sprite == null)
            Start();
        sprite.color = c;
    }

    public void changeTag(string t)
    {
        string s = tag;
        gameObject.tag = t;
        if (gameObject.tag == null)
            gameObject.tag = s;
    }
}
