﻿using UnityEngine;
#if false
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;

public class t_cell : MonoBehaviour {

    [SerializeField]
    Receptor_Type type;

    [SerializeField]
    Co_Receptor_Type ctype;

    [SerializeField]
    GameObject spikePair;

    [SerializeField]
    float radius;

    [SerializeField]
    int spikeNumber;

    [SerializeField]
    float spikeScale;

    [SerializeField]
    GameObject parasite;

    [SerializeField]
    private float replication_timer_max;
    
    private float replication_timer;

    public enum State
    {
        FREE,
        TAKEN
    }
    List<scr_pairing> t_spikes;

    State cell_state;

    // Use this for initialization
    void Start() {
        //parasite = null
        cell_state = State.FREE;
        t_spikes = new List<scr_pairing>();
        setRadius(radius);
        createSpikes();
    }

    // Update is called once per frame
    void Update() {
#if false
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (infectable(parasite))
                infect(parasite);
        }
        if (Input.GetKeyDown(KeyCode.Return))
            replicate();
#else            
        if (cell_state == State.TAKEN && replication_timer_max > 0f)
        {
            if (replication_timer <= 0f)
            {
                replicate();
                replication_timer = replication_timer_max;
            }
            else
                replication_timer -= Time.deltaTime;
#endif
        }
    }

    public bool infectable(GameObject parasite)
    {
        scr_hiv p = parasite.GetComponent<scr_hiv>();
        if (p)
            return p.ctype == ctype && p.type == type;
        return false;
    }


    void createSpikes()
    {
        float separation = 360f / (float)spikeNumber;
        for(int i = 0; i < spikeNumber; i++)
        {
            float s = Random.Range(-separation / 2, separation / 2);
            GameObject spike = (GameObject)Instantiate(spikePair, this.gameObject.transform);
            spike.transform.rotation = Quaternion.identity;
            spike.transform.Rotate(0, 0, i * separation + s);
            spike.transform.position = transform.position;
            Vector2 a = Quaternion.AngleAxis(i * separation + s, Vector3.up) * Vector2.up * radius;
            spike.transform.Translate(a);
            spike.GetComponent<scr_pairing>().Start();
            spike.GetComponent<scr_pairing>().setType(type, ctype);

            spike.transform.localScale = new Vector3(Mathf.Min(spikeScale / transform.localScale.x, 1), Mathf.Min(spikeScale / transform.localScale.y, 1), 1f);
            t_spikes.Add(spike.GetComponent<scr_pairing>());
        }
    }

    public void changeState(t_cell.State s)
    {
        if (s == cell_state) return;

        switch(cell_state)
        {
            case State.TAKEN:
                //parasite = null;
                gameObject.layer = LayerMask.NameToLayer("t-cell");
                gameObject.tag = "Benefactor";
                replication_timer = replication_timer_max;
                change_saturation(1f);
                break;
            case State.FREE:
                gameObject.layer = LayerMask.NameToLayer("t-cell-infected");
                gameObject.tag = "Infected";
                change_saturation(0.5f);
                break;
        }

        cell_state = s;
    }

    public void infect(GameObject g)
    {
        if (cell_state == State.TAKEN) return;
        changeState(State.TAKEN);
        parasite = g;
        
    }

    private int replication_number;
    public void setRepNumber(int i)
    {
        //Debug.Log("replication number: " + i.ToString());
        if (i <= 0) return;
        replication_number = i;
    }

    public int getRepNumber()
    { return replication_number; }

    public void setRepTimer(float t)
    {
        if (t <= 0f) replication_timer_max = -1f;
        else
        {
            replication_timer_max = t;
            replication_timer = replication_timer_max;
        }
    }

    public void replicate()
    {
        if (scr_hiv.wandernumber > scr_hiv.maximum) return;
        if (cell_state == State.FREE) return;
        for (int i = 0; i < replication_number; i++)
        {
            Vector3 spawnLocation = new Vector3(transform.position.x + Random.Range(-radius / 4f, radius / 4f), transform.position.y + Random.Range(-radius / 4f, radius / 4f), transform.position.z);
            startParasite(Instantiate(parasite, spawnLocation, transform.rotation) as GameObject);

        }
    }

    void startParasite(GameObject g)
    {
        g.transform.position = transform.position;
        scr_hiv self = g.GetComponent<scr_hiv>();
        if (self != null)
        {
            //CircleCollider2D c = g.GetComponent<CircleCollider2D>();
            //if (c) c.enabled = false;
            self.setTimer(radius);
            self.switchState(scr_hiv.State.BUD);
            Vector2 destination = Functions.eulerRadiusToCartesian(radius, 60f);
            self.setDestination(destination);
            if (self.gameObject.activeSelf == false)
            {
                self.gameObject.SetActive(true);
            }
        }
    }

    public GameObject getParasite()
    {
        if (cell_state == State.FREE) return null;
        return parasite;
    }

    float get_saturation()
    {
        return gameObject.GetComponentInChildren<Display>().get_saturation();
    }

    void change_saturation(float s)
    {
        foreach(var i in gameObject.GetComponentsInChildren<SpriteRenderer>())
        {
            if (i.gameObject.GetComponent<Display>() == null)
                i.color = Functions.changeSaturation(i.color, s);
        }
        Display d = gameObject.GetComponentInChildren<Display>();
        if (d == null)
            Debug.Log("whoops");
        else d.set_saturation(s);

    }
    float getRadius() { return radius; }
    public void setRadius(float r)
    {
        if (r == 0) return;
        radius = r;
        float s = r / 5;
        transform.localScale = new Vector2(s, s);
    }


}
