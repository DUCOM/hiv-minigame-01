﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           scr_hiv.cs
 *  Version:        0.0.5
 *  Author:         Vincent Mills
 *  Date:           3 October 2016
 *  Last Edited by: Vincent Mills
 */

/*
 * SCR_HIV.CS:
 *  This script is a basic controls script for the HIV
 *  
 * 
*/


public class scr_hiv : MonoBehaviour {

    /*
     *  PRIVATE VARIBALES
     * 
     */
    [SerializeField]
    private GameObject scroller;

    [SerializeField]
    private GameObject zoomCamera;

    private GameObject cameraInstance = null;

    public static int wandernumber = 0;

    [SerializeField]
    private float wanderTimer_;

    public float wanderTimer { get { return wanderTimer_; } }

    [SerializeField]
    private Pairing pair_ = new Pairing(Receptor_Type.CD4_POSITIVE, Co_Receptor_Type.CCR5);
    public Pairing pair { get { return pair_; } }


    public const int maximum = 16;

    public Receptor_Type type { get { return pair.type; } }
    public Co_Receptor_Type ctype { get { return pair.ctype; } }

    private bool init = false;


    [SerializeField]
    private float timer;
    private Vector2 destination; //destination for when the object is trying to bud out

    //private float speed; // the current speed of the virus
    private Rigidbody2D _rb; //the rigidbody

    [SerializeField]
    private MoveController controller_;

    public MoveController controller { get { return controller_;} }

    private Transform host_;

    public Transform host { get { return host_; } }

    public void setHost(Transform t, bool spike = false)
    {
        if (spike)
            if (t.parent.gameObject.GetComponent<t_cell>())
                host_ = t.parent;

        if (t.gameObject.GetComponent<t_cell>())
            host_ = t;

        host_ = null;
    }

    public void setHost(t_cell t)
    {
        host_ = t.gameObject.transform;
    }


    public enum State
    {
        WANDER,
        CHECK,
        INFECT,
        BUD
    }

    State state_;

    [SerializeField]
    private float max_speed;

    /// <summary>
    /// FUNCTIONS
    /// </summary>

	// Use this for initialization
	void Start () {
        if (!init)
        {
            cameraInstance = null;
            if (controller_ == null)
                controller_ = findController();
            if (controller_ == null)
                createController();
            
            _rb = gameObject.GetComponent<Rigidbody2D>();

            state_ = State.WANDER;
            wandernumber++;
            init = true;
        }
	}


    // Update is called once per frame
    void Update()
    {

        switch (state_)
        {
            case State.WANDER:
                wanderTimer_ -= Time.deltaTime;
                wander();
                break;
            case State.BUD:
                bud();
                break;
            case State.INFECT:
                Debug.Log("hello");
                infect();
                break;
            case State.CHECK:
                /*
                if (controller.controller.getKeyDown(Controller.Buttons.ENTER) && checkTimer < 0f)
                    switchState(State.INFECT);
                else checkTimer -= Time.deltaTime;
                */
                break;
            default:
                Debug.Log("in check again");
                break;
        }
    }

    public void receiveTranslator(bool message)
    {
        if (state_ != State.CHECK) return;
        switchState(message ? State.INFECT : State.WANDER);
    }

    MoveController findController()
    {
        GameObject[] g = GameObject.FindGameObjectsWithTag("Player");
        foreach(var i in g)
        {
            MoveController c = i.GetComponent<MoveController>();
            if (c != null)
                return c;
        }
        return null;
    }
    void createController()
    {
        controller_ = gameObject.GetComponent<MoveController>();
        if (controller_ == null)
            controller_ = gameObject.AddComponent<MoveController>();
    }
	
    private GameObject findCamera()
    {
        return findCamera(gameObject.transform);
    }

    private GameObject findCamera(Transform t)
    {
        if (zoomCamera == null) Debug.LogError("does not contain camera to instantiate");
        if (cameraInstance != null) return cameraInstance;

        Object o = Instantiate(zoomCamera);

        if (!(o is GameObject)) throw new System.Exception("Zoom Camera is not a Game Object");
        GameObject g = o as GameObject;

        g.GetComponent<ZoomIn>().setCenter(t);

        return o as GameObject;
    }

    private void destroyCamera()
    {
        if (cameraInstance == null) return;
        Destroy(cameraInstance);
        cameraInstance = null;
    }

    private void infect()
    {
        _rb.velocity = Functions.velocityTowards(transform.position, host.gameObject.transform.position, max_speed, 0.1f);
        if (_rb.velocity == Vector2.zero)
        {
            t_cell t = host_.GetComponent<t_cell>();
            t.setRepTimer(2f);
            t.setRepNumber(2);
            t.replicate();
            gameObject.SetActive(false);
        }   
    }

    private void bud()
    {
        if (timer <= 0f)
            switchState(State.WANDER);
        else timer -= Time.deltaTime;
        Debug.Log(destination);
    }

    private void wander()
    {
        moveTowardsController();
    }

    void moveTowardsController()
    {
        _rb.velocity = Functions.velocityTowards(transform.position, controller.gameObject.transform.position, max_speed, 0.1f);
    }

    public void switchState(State s)
    {
        Start();
        if (s == state_) return;
        if (state_ == State.WANDER)
        {
            _rb.velocity = new Vector2();
            wandernumber--;
        }
        switch (s)
        {
            case State.CHECK:
                Vector2 c = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>().pixelRect.size;

                GameObject g = Instantiate(scroller, c, Quaternion.identity) as GameObject;
                UIBar u = g.GetComponent<UIBar>();
                u.moveTo(Camera.main.WorldToScreenPoint(transform.position) - Camera.main.ViewportToScreenPoint(new Vector2(0.15f, 0)));
                u.setTimer(timer);
                u.setReceiver(this);
                break;
            case State.BUD:
                _rb.velocity = Functions.velocityTowards(transform.position, destination, max_speed / 2);
                if (timer <= 0f)
                    setTimer(3f);
                break;
            case State.INFECT:
                destroyCamera();
                host_.GetComponent<t_cell>().infect(gameObject);
                setDestination(host_.position);
                break;
            case State.WANDER:
                destroyCamera();
                wanderTimer_ = Time.deltaTime;
                wandernumber++;
                break;
        }
        state_ = s;
    }

    public void setCameraInstance(Transform t)
    {
        if (t == null) return;
        if (cameraInstance != null)
            cameraInstance.GetComponent<ZoomIn>().setCenter(t);
        else
        {
            cameraInstance = findCamera(t);
        }
    }

    public void setTimer(float seconds)
    {
        if (seconds <= 0f) return;
        timer = seconds;
    }

    public State getState() { return state_; }

    public void setDestination(Vector2 d)
    {
        if (state_ == State.BUD)
            destination = d;
    }


    public override string ToString()
    {
        return "HIV: " + type.ToString() + " " + ctype.ToString();
    }
}
