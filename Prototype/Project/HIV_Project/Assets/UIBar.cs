﻿using UnityEngine;
using System.Collections;

public class UIBar : MonoBehaviour {

    RectTransform t;
    Canvas c;

    CheckSystem systemCheck_;
    Bar_Slider slider_;

    bool __init__ = false;

    Vector3 scale_;

    float timer_;
    float maxTimer_;

    public void moveTo(Vector3 position)
    {
        Start();
        t.position = new Vector3(0, 0);
        Debug.Log(t.position - t.position + position);
        for (var i = 0; i < t.childCount; ++i)
        {
            t.GetChild(i).localPosition.Set(0, 0, 0);
            t.GetChild(i).Translate(position);
        }
        t.position = position;
    }
	// Use this for initialization
	void Start () {
        if (__init__) return;
        if (transform.parent == null || GetComponentInParent<Canvas>() == null)
        {
            t = GetComponent<RectTransform>();
            GameObject g = GameObject.FindGameObjectWithTag("Canvas");
            if (g == null) Destroy(gameObject);
            else
            {
                c = g.GetComponent<Canvas>();
                if (c)
                {
                    Vector2 size = c.pixelRect.size;

                }
                else Destroy(gameObject);
                slider_ = GetComponentInChildren<Bar_Slider>();
                systemCheck_ = GetComponentInChildren<CheckSystem>();
            }
        }
        else { c = GetComponentInParent<Canvas>(); }
        __init__ = true;
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void setReceiver(scr_hiv h)
    {
        if (h == null) return;
        systemCheck_.SetReceiver(h);
    }

    public void setTimer(float t)
    {
        if (t <= 0f) t = 0f;
        timer_ = t;
        maxTimer_ = t;
        slider_.SetTimer(t);
        systemCheck_.SetTimer(t);
    }
}
