﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckSystem : MonoBehaviour {

    [SerializeField]
    private Transform slider;

    private RectTransform r;

    [SerializeField]
    private scr_hiv receiver;

    private float timer = 1f;

    [SerializeField]
    private Transform tooFar;

    public void SetReceiver(GameObject g) { SetReceiver(g.GetComponent<scr_hiv>()); }

    public void SetReceiver(scr_hiv h) { if (h != null) receiver = h; }

    public void SetTimer(float t)
    {
        if (t <= 0f) t = 0f;
        timer = t;
    }

    // Use this for initialization
    void Start () {
        if (r == null) r = GetComponent<RectTransform>();
        if (receiver == null) Destroy(transform.parent.gameObject);
        transform.parent.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform);
        transform.parent.position = new Vector3();
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
	}

    void OnGUI()
    {
        if (timer <= 0f)
        {
            if (receiver.controller.controller.getKey(Controller.Buttons.ENTER))
            {
                sendMessage(checkInside());
            }
            else if (slider.position.x >= tooFar.position.x)
            {
                sendMessage(false);
            }
        }
    }

    void sendMessage(bool m)
    {
        receiver.receiveTranslator(checkInside());
        Destroy(transform.parent.gameObject);
    }

    bool checkInside()
    {
        float x = slider.position.x;
        return x <= r.position.x + r.rect.width / 2 && x >= r.position.x - r.rect.width / 2;
    }
}
