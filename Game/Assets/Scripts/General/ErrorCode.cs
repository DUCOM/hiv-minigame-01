﻿/*
 *  Name:           ErrorCode.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017 
 */

/// <summary>
/// Error Code Class. Returns the name of the error in a jiffy so you don't have to memorize them. All codes begin with an E
/// </summary>
public static class ErrorCode
{
	#region codes

	#region default

	/// <summary> Default error message. </summary>
	/// <value>The error message.</value>
	public static string E0000 { get { return DisplayCode("0000", "default error message."); } }

	#endregion

	#region arguments

	/// <summary> Couldn't find an appropriate value. </summary>
	/// <value>The error message.</value>
	public static string EA0VA { get { return DisplayCode("A0VA", "Could not find an appropriate value."); } }

	/// <summary> Invalid Number of arguments. </summary>
	/// <value>The error message.</value>
	public static string EA0IN { get { return DisplayCode("AOIN", "Invalid number of arguments."); } }

	/// <summary> Argument cannot be negative. </summary>
	/// <value>The error message.</value>
	public static string EA0NN { get { return DisplayCode("AONN", "Argument cannot be negative."); } }

	/// <summary> Argument is out of range. </summary>
	/// <value>The error message.</value>
	public static string EA0OR { get { return DisplayCode("EA0OR", "Given argument is out of range."); } }

	#endregion

	#region Components
	/// <summary> Null or missing components in the current object. </summary>
	/// <value>The error message.</value>
	public static string EC0MC { get { return DisplayCode("EC0MC", "Null or missing components in the current object."); } }

	#endregion

	#region variableCodes

	/// <summary> VariableCodes for use for display. use <c>DisplayCode()</c> to use it properly </summary>
	public enum VariableCodes
	{
		/// <summary> Variable Name cannot be negative </summary>
		EV0NN
	}

	#endregion

	#region game specific

	/// <summary> Invalid GP120 tropism. </summary>
	/// <value>The error message.</value>
	public static string EG0HT { get { return DisplayCode("EG0HT", "Invalid tropism for GP120"); } }


	#endregion

	#endregion codes

	/// <summary> Display the error code based on variable. </summary>
	/// <param name="variableCode">The error variable code</param>
	/// <param name="variableName">The given variable's name.</param>
	/// <returns>The error message.</returns>
	public static string DisplayVariableCode(VariableCodes variableCode, string variableName)
	{
		switch(variableCode)
		{
			case VariableCodes.EV0NN:
				return DisplayCode("EVONN", variableName + " cannot be negative.");

			default:
				return DisplayCode("NULL", variableName + "is wrong somehow. I don't know how.");
		}
	}

	static string DisplayCode(string number, string message)
	{
		return "Error Code " + number + ": " + message;
	}
}
