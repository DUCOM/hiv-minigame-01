﻿using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AudioDatabase.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  27 March 2017
 */

/// <summary>
///		Audio Database class. Keeps track of all sounds and musics the game object can make. 
///		<para>Requires AudioSource.</para>
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class AudioDatabase : MonoBehaviour {

	#region Variables
	[SerializeField] List<Sound> soundList; //the list of sounds that the database can rely on
	[SerializeField] bool playOnAwake; //whether to play the current index upon awake
	[SerializeField] int currentIndex; //the current sound index that is playing
	AudioSource source; //the AudioSource component of the game object

	Dictionary<string, int> indexDict; //the index of names for use in sound name referencing
	#endregion

	#region Methods
	/// <summary>
	/// Gets the <see cref="Sound"/>with the specified index. Returns null if out of range.
	/// </summary>
	/// <param name="i">The index.</param>
	public Sound this[int i]
	{
		get { return GetSound (i); }
	}

	/// <summary>
	/// Gets the <see cref="Sound"/> with the specified name. Returns null if out of range.
	/// </summary>
	/// <param name="name">The desired sound name.</param>
	public Sound this [string name] {
		get { return GetSound (name); }
	}

	/// <summary>
	/// Gets the <see cref="Sound"/>with the specified index. Returns null if out of range.
	/// </summary>
	/// <param name="index">The desired Sound index.</param>
	public Sound GetSound(int index)
	{
		if (index <= 0 || index >= soundList.Count)
			return null;
		return soundList [index];
	}

	/// <summary>
	/// Gets the <see cref="Sound"/> with the specified name. Returns null if out of range.
	/// </summary>
	/// <param name="name">The desired sound name.</param>
	public Sound GetSound(string name)
	{
		if (indexDict.ContainsKey (name)) {
			return GetSound (indexDict [name]);
		}
		return null;
	}


	/// <summary>
	/// Plays the current sound.
	/// </summary>
	public void Play()
	{
		if (source) {
			source.clip = soundList [currentIndex].sound;
			source.loop = soundList [currentIndex].loop;
			source.Play ();
		}
	}

	/// <summary>
	/// Plays the desired sound via index.
	/// </summary>
	/// <param name="index">The desired sound index.</param>
	public void Play(int index)
	{
		if (index < 0 || index > soundList.Count)
			return;
		
		ChangeIndex (index);
		Play ();
	}

	/// <summary>
	/// Plays the desired sound via name.
	/// </summary>
	/// <param name="name">The desired sound name.</param>
	public void Play(string name)
	{
		if (indexDict.ContainsKey (name)) {
			Play (indexDict [name]);
		}
	}

	/// <summary>
	/// Change the current index of the database.
	/// </summary>
	/// <param name="index">The desired index.</param>
	public void ChangeIndex(int index)
	{
		if (index < 0 || index > soundList.Count)
			return;

		currentIndex = index;
	}

	/// <summary>
	/// Refreshes the dictionary between names and indicies
	/// </summary>
	void RefreshSoundIndex()
	{
		indexDict = new Dictionary<string, int> ();
		for (var i = 0; i < soundList.Count; ++i) {
			indexDict.Add (soundList [i].name, i);
		}
	}
		
	#region Monodevelop
	void OnValidate()
	{
		if (currentIndex >= soundList.Count) {
			currentIndex = soundList.Count - 1;
		} else if (currentIndex < 0) {
			currentIndex = 0;
		}

	}

	void Awake()
	{
		if (indexDict == null)
			RefreshSoundIndex ();
		source = GetComponent<AudioSource> ();
		if (playOnAwake) {
			Play ();
		}
	}


	#endregion Monodevelop
	#endregion Methods
}

/// <summary>
/// The Sound class. Takes in an audioclip and gives it more information for game usage
/// </summary>
[System.Serializable]
public class Sound
{
	/// <summary>
	/// Is the sound Music or Effects
	/// </summary>
	public enum SoundType
	{
		MUSIC,
		SFX
	}
		
	public AudioClip sound;
	public string name;
	public SoundType type;
	public bool loop;
}