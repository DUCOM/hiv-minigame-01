﻿using UnityEngine;

/*
 *  Name:           Rotate.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> Rotate class. Constantly rotate the object based on rotationspeed. Based on FixedUpdate(). </summary>
public class Rotate : MonoBehaviour {

	public float rotationSpeed;

	void FixedUpdate()
	{
		transform.Rotate(0f, 0f, rotationSpeed);
	}
}
