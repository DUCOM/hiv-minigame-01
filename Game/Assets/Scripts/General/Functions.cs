﻿using System;
using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           Functions.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.1 (Added Random() templated functiions for just a simple list and a simple array)
 *  Last Modified:  26 January 2017 
 */

/// <summary> Overall functions for use throught the project. </summary>
public static class Functions
{
	#region list functions

	/// <summary> Gets the last index of a list </summary>
	/// <typeparam name="Type">The type to return</typeparam>
	/// <param name="list">The given list</param>
	/// <returns>The last index of the list</returns>
	public static Type GetLast<Type>(List<Type> list)
	{
		if(list == null) throw new ArgumentNullException();
		if(list.Count == 0) return list[0];
		return list[list.Count - 1];
	}

	#endregion

	#region border functions

	/// <summary> finds a random point inside the rectangle </summary>
	/// <param name="r">The intended rectangle</param>
	/// <returns>A random point on the border of the rectangle</returns>
	public static Vector2 RandomBorder(Rect r)
	{
		float y = UnityEngine.Random.Range(r.yMin, r.yMax); //find a random y value on the rectangle
		float x = UnityEngine.Random.Range(r.xMin, r.xMax); //find a random x value on the rectangle

		float random = UnityEngine.Random.Range(0f, 1f); //picks a random side

		Vector2 location = new Vector2(x, y); //the main location

		if(random <= 0.25f) location.x = r.xMin; //go to the left side
        else if(random <= 0.5f) location.y = r.yMax; //go to the top side
        else if(random <= 0.75f) location.x = r.xMax; //go to the right side
        else location.y = r.yMin; //go to the bottom side

		return location;
	}

	/// <summary> Creates a rectangle that will convert a border rectangle to a world one </summary>
	/// <param name="c">the intended camera</param>
	/// <param name="border">The border in viewport parameters</param>
	/// <returns>the equivalent world rectangle</returns>
	public static Rect CameraToWorldRect(Camera c, Rect border)
	{
		Vector2 topRight = c.ViewportToWorldPoint(new Vector3(border.xMax, border.yMax));
		Vector2 bottomLeft = c.ViewportToWorldPoint(new Vector3(border.xMin, border.yMin));
		float width = topRight.x - bottomLeft.x;
		float height = topRight.y - bottomLeft.y;
		return new Rect(bottomLeft, new Vector2(width, height));
	}

	/// <summary> Check if position that is ouside of the given rectangle. </summary>
	/// <returns><c>true</c>, if position is ouside of the rectangle, <c>false</c> otherwise.</returns>
	/// <param name="pos">intended position.</param>
	/// <param name="rect">The rectangle position.</param>
	public static bool OutsideRect(Vector3 pos, Rect rect)
	{
		return pos.x > rect.xMax || pos.x < rect.xMin || pos.y > rect.yMax || pos.y < rect.yMin;
	}

	#endregion

	#region color functions

	/// <summary> Gets the color based on the percentage of the object </summary>
	/// <returns>The intended color of the game.</returns>
	/// <param name="percent">Intended percentage. Make it in between 0 and 1.</param>
	public static Color GetColorPercentage(float percent)
	{
		if(percent < 0) percent = 0f;
		if(percent > 1) percent = 1f;
		float r, g, b = 0f;
		if(percent > 0.5)
		{
			r = Mathf.Max(2 - 2 * percent, 0f);
			g = 1;
		}
		else
		{
			r = 1;
			g = Mathf.Max(2 * percent, 0f);
		}

		return new Color(r, g, b);
	}

	#endregion color functions

	#region Templated functions

	/// <summary>
	/// Finds  a random value in the list given probability values
	/// </summary>
	/// <param name="randList">The given list to pick out of.</param>
	/// <param name="probVal">The weighted probabilities to pick out of the list.</param>
	/// <typeparam name="T">The picking list's type.</typeparam>
	public static T Random<T>(T[] randList, Dictionary<T, float> probVal)
	{
		if(randList.Length < 1) throw new System.ArgumentNullException(ErrorCode.EA0IN);
		if(randList.Length < 2) return randList[0];

		float threshhold = 0.001f;
		Dictionary<T, float> newValues = new Dictionary<T, float>();
		float total = 0f;

		foreach(var i in randList)
		{
			if(probVal[i] > 0f) newValues.Add(i, probVal[i]);
			total += probVal[i];
		}

		if(Mathf.Abs(total - 1f) > threshhold)
		{

			foreach(var k in randList)
			{
				if(newValues.ContainsKey(k)) newValues[k] /= total;
			}
		}

		float z = UnityEngine.Random.value;

		foreach(var s in newValues)
		{
			
			if(z < s.Value) return s.Key;
			else z -= s.Value;
		}
		throw new System.ArgumentNullException(ErrorCode.EA0VA);
	}

	/// <summary> Finds picks a random instance from the list. </summary>
	/// <param name="randList">The array to pick out of.</param>
	/// <typeparam name="T">The type parameter.</typeparam>
	public static T Random<T>(T[] randList)
	{
		if(randList.Length == 1) return randList[0];
		return randList[UnityEngine.Random.Range(0, randList.Length - 1)];
	}

	/// <summary> Finds picks a random instance from the list. </summary>
	/// <param name="randList">The list to pick out of.</param>
	/// <typeparam name="T">The type parameter.</typeparam>
	public static T Random<T>(List<T> randList)
	{
		if(randList.Count == 1) return randList[0];
		return randList[UnityEngine.Random.Range(0, randList.Count - 1)];
	}

	#endregion Templated functions
}