﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           GroupColors.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Added CommonColors and ImageList)
 *  Last Modified:  1 March 2017
 */

public class GroupColors : MonoBehaviour
{

	[SerializeField] List<SpriteRender> spriteList;
	[SerializeField] List<ImageRender> imageList;

	[SerializeField] bool common = false;
	[SerializeField] Color commonColor;

	void OnValidate( )
	{
		foreach(var i in spriteList)
		{
			if(i.render) i.render.color = common ? commonColor : i._color;
		}

		foreach(var i in imageList)
		{
			if(i.render) i.render.color = common ? commonColor : i._color;
		}
	}
}

[System.Serializable]
public struct SpriteRender
{
	public SpriteRenderer render;
	public Color _color;

}

[System.Serializable]
public struct ImageRender
{
	public Image render;
	public Color _color;
}