﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           ZoomController.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Got rid of the MouseScrollwheel for now)
 *  Last Modified:  1 December 2016
 */


public class ZoomController : MonoBehaviour
{
	//[SerializeField] float screenMultiplier = 0.5f;

	/// <summary>
	/// Gets the render camera of the object.
	/// </summary>
	/// <value>The object's render camera.</value>
	public RenderCamera renderCam { get { return GetComponent<RenderCamera>(); } }

	/// <summary>
	/// Gets the controller object.
	/// </summary>
	/// <value>The controller.</value>
	public Controller controller { get { return GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>(); } }

	/// <summary>
	/// Gets or sets the how far the camera is zoomed in.
	/// </summary>
	/// <value>The zoom factor.</value>
	float zoomFactor
	{
		get { return renderCam.zoom; }
		set
		{
			float v = value;
			if(v < 1) v = 1;
			else if(v > 10) v = 10;
			GetComponent<RenderCamera>().zoom = v;
		}
	}

	// Use this for initialization
	void Start( )
	{
		zoomFactor = 1;
	}
	// Update is called once per frame
	void Update( )
	{
		var i = 0f; //= Input.GetAxis("Mouse ScrollWheel");
		if(i != 0f)
		{
			//Debug.Log(controller.worldPosition);

			var original = controller.worldPosition;
			
			var w = 40f * renderCam.cam.aspect;
			zoomFactor += i;

			var height = renderCam.GetZoomHeight(zoomFactor);
			var width = height * renderCam.cam.aspect;

			var d = new Vector3(w / 2 - original.x, 40f / 2 - original.y);
			var p = new Vector3(d.x / w, d.y / 40f);
			var s = new Vector3(p.x * width, p.y * height);
			var pos = new Vector3(width / 2 - s.x, height / 2 - s.y);

			pos.z = -10f;

			renderCam.difference = (original - pos);
		}
	}
}
