﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           QuitButton.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> VersionNumberDisplay class. Displays the version number. Require Text. </summary>
[RequireComponent(typeof(Text))]
public class VersionNumberDisplay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Text>().text = "Version " + Application.version.ToString();
	}
}
