﻿using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           HIVReceptorChecker.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// HIV receptor Checker class. Checks whether the HIV would check if the T cell receptor would be able to be correct to infect them
/// </summary>
public class HIVReceptorChecker : MonoBehaviour
{
	#region Variables
	public Tropism tropism { get { return main.GetSpike(0).GetComponent<Spike>().tropism; } }

	public HIV main { get { return GetComponentInParent<HIV>(); } }

	public Collider2D stay;

	public float maxTimer;

	public float _timer { get; set; }
	#endregion Variables

	#region Methods

	#region Monodevelop
	void OnTriggerEnter2D(Collider2D c)
	{

		if(!main.selected) return;

		if(c.gameObject.GetComponent<TCell_Receptor>() == null) return;

		#if UNITY_EDITOR
		if(main.selected && !main.weak)
		#else
		if((main.selected) && validCombination(c.gameObject.GetComponent<TCell_Receptor>()) && !main.weak)
		#endif
		{
			_timer = maxTimer;
			stay = c;
			UI_World_Filler.main.gameObject.SetActive(true);
			UI_World_Filler.main.transform.position = transform.position + new Vector3(5f, 5f);

			//var cell = s.cell;
			//Infect(cell);
		}
	}

	void OnTriggerStay2D(Collider2D c)
	{
		if(c == stay)
		{
			if(main.selected)
			{
				_timer -= Time.deltaTime;
				UI_World_Filler.main.transform.position = transform.position + new Vector3(5f, 5f);
				UI_World_Filler.main.Fill(1f - (_timer / maxTimer));
				if(_timer < 0)
				{
					Infect(c.gameObject.GetComponent<TCell_Receptor>().cell);
					UI_World_Filler.main.gameObject.SetActive(false);
				}
			}
			else
			{
				stay = null;
				UI_World_Filler.main.gameObject.SetActive(false);
				return;
			}
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if(c == stay)
		{
			stay = null;
			UI_World_Filler.main.gameObject.SetActive(false);
		}
	}
	#endregion Monodevelop

	bool validCombination(TCell_Receptor check)
	{
		if(check && check.type.receptor == Tropism.ReceptorType.CD4_POSITIVE)
		{
			return tropism.coreceptor == Tropism.CoReceptorType.DUAL ? check.type.coreceptor == Tropism.CoReceptorType.CCR5 || check.type.coreceptor == Tropism.CoReceptorType.CXCR4 
					: tropism.coreceptor == check.type.coreceptor;
		}
		return false;
	}

	void Infect(TCell c)
	{
		if(c.infected) return;

		c.infected = true;
		main.ChangeState(HIV.State.INFECT, c.gameObject);
		int scoreAdd = 10 * Game.main.population * (tropism.coreceptor == Tropism.CoReceptorType.DUAL ? 2 : 1);
		Game.main.score += scoreAdd;

		if(UI_Immunogenicity.main) UI_Immunogenicity.main.Add(0.1f);
		ScoreDisplay.main.ShowScore (transform.position, scoreAdd, 1.5f);
		main.gameObject.GetComponent<AudioDatabase> ().Play ("entry");
	}
	#endregion Methods
}
