﻿using System.Collections.Generic;
using UnityEngine;

/*
 *	Name:			HIV_Weak.cs
 *	Author:			Vincent Mills
 *	Version:		1.0.0 (Release)
 *	Last Modified:	29 March 2017
 */

/// <summary> HIV_Weak State. Unallows the HIV to bind with receptors until the antibody is gone. </summary>
public class HIV_Weak : MonoBehaviour
{
	#region Variables
	[SerializeField] GameObject bubble;
	Color gp120_l, gp120_r, gp41;
	[Range(0, 7)] [SerializeField] int excludedIndex;
	[HideInInspector] public Antibody antibody;

	Vector3 prev_velocity;

	public int shakeNumber { get; private set; }

	#region Components
	HIV main { get { return GetComponent<HIV>(); } }
	#endregion

	#endregion Variables

	#region Methods

	#region Monodevelop
	void OnEnable( )
	{
		bubble.SetActive(true);
		gp120_l = main.GP120_Color_Left;
		gp120_r = main.GP120_Color_Right;
		gp41 = main.GP41_Color;
		main.weak = true;
		Spike a = main.GetSpike(excludedIndex).GetComponent<Spike>();
		a.gp41.spriteRender.color = gp41;
		a.gp120.displayLeft.color = gp120_l;
		a.gp120.displayRight.color = gp120_r;
		main.GetComponent<AudioDatabase> ().Play ("bubble");

		prev_velocity = new Vector3();
		shakeNumber = 0;
	}

	void OnDisable( )
	{
		bubble.SetActive(false);
		main.GP120_Color_Left = gp120_l;
		main.GP120_Color_Right = gp120_r;
		main.GP41_Color = gp41;
		main.weak = false;
		if(antibody) antibody.GetComponent<AntibodyLatch>().Kill();
		shakeNumber = 0;
	}

	void FixedUpdate( )
	{
		if(main.state == HIV.State.SELECTED)
		{
			if((prev_velocity.x > 0 && main.move.velocity.x < 0) || (prev_velocity.x < 0 && main.move.velocity.x > 0)) ++shakeNumber;
			prev_velocity = main.move.velocity;
		}
	}
	#endregion Monodevelop

	public void ChangeExcludedIndex(int index)
	{
		if(enabled)
		{
			main.weak = true;
			Spike a = main.GetSpike(index).GetComponent<Spike>();
			a.gp41.spriteRender.color = gp41;
			a.gp120.displayLeft.color = gp120_l;
			a.gp120.displayRight.color = gp120_r;
		}

		excludedIndex = index;
	}

	#endregion
}
