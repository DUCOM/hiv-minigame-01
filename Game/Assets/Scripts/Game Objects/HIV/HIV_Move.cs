﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           HIV_Move.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.3 (Collapsed the main bulk of the code to ObjectMovement2D)
 *  Last Modified:  23 January 2017
 */

/// <summary>
/// Movement Class for the HIV
/// 	= Controls the vectoring of the game 
/// 	= has its own follow state
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class HIV_Move : ObjectMovement2D
{

	#region Variables

	#region Components

	/// <summary> The Instance component of the instance. </summary>
	HIV main;

	#endregion

	new public float maxSpeed { get { return main ? main.stats.stats[Statistics.Type.SPEED] : Statistics.base_stats[Statistics.Type.SPEED]; } }

	#endregion

	#region Functions

	#region Monodevelop

	// Use this for initialization
	new protected void Awake( )
	{
		_rb = GetComponent<Rigidbody2D>();
		main = GetComponent<HIV>();
	}

	void FixedUpdate( )
	{
		Move();
	}

	#endregion

	#endregion
}
