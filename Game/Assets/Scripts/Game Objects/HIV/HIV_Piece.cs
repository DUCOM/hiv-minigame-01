﻿using UnityEngine;

/*
 *  Name:           HIV_Piece.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.6 (Added lockColor, able to keep the render color as is while keeping the statistical color intact)
 *  Last Modified:  20 November 2016
 */

#region HIV_PIECE

/// <summary> Abstract. The script that each piece of HIV inherits from. Not to be confused with HIV_Part. </summary>
public abstract class HIV_Piece : MonoBehaviour
{
	#region Variables

	#region color

	public bool lockColor;

	protected Color _color;

	/// <summary> Encapsulated. The color of the object. </summary>
	public Color color
	{
		get { return _color; }
		set
		{
			if(!lockColor) spriteRender.color = value;
			ChangeStats();
			_color = value;
		}
	}

	#endregion color

	#region spriteRenderer

	/// <summary> Gets the sprite renderer. </summary>
	/// <value>The sprite renderer of the gameObject.</value>
	protected SpriteRenderer spriteRender
	{
		get { return gameObject.GetComponentInChildren<SpriteRenderer>(); }
	}

	#endregion meshrender and materials

	#region statistics

	protected Statistics _stats;

	/// <summary> Encapsulated. The Statistics of the object. Get Only. </summary>
	public Statistics stats { get { return _stats; } }

	#endregion statistics

	#endregion Variables

	#region Abstract Functions

	/// <summary> Abstract. Changes the Stats of the object based on its features. </summary>
	protected abstract void ChangeStats( );

	#endregion
}

#endregion HIV_PIECE


#region HIV_PART

/// <summary> a part of the HIV, consists of a color, prefab, and a script. Not to be confused with HIV_Piece </summary>
[System.Serializable]
class HIV_Part
{
	#region Variables

	/// <summary> the part's game object with the right script </summary>
	[SerializeField] public GameObject prefab;

	/// <summary>
	/// the part's color
	/// </summary>
	public Color color
	{
		get {
			return _script.color;
		}
		set { _script.color = value; }
	}

	private HIV_Piece _script;

	/// <summary> Encapsulated. The Script of the HIV_Piece. Same as the instance </summary>
	public HIV_Piece script { get { return _script; } }

	/// <summary> The instantiated game object Null if none </summary>
	public GameObject instance
	{
		get {
			if(_script == null) return null;
			return script.gameObject;
		}
	}

	#endregion Variables

	#region Methods

	#region Instaniate

	/// <summary> Instantiates a part. Use the script to find the Instance </summary>
	public void Instantiate( )
	{
		if(prefab == null) throw new System.NullReferenceException("invalid or missing game object");
		_script = Object.Instantiate(prefab).GetComponent<HIV_Piece>();
		if(_script == null) throw new System.NullReferenceException("Null script found");
	}

	/// <summary> Instantiates a part with a given parent and a given position </summary>
	/// <param name="parent">gameObject's parent. null if none</param>
	/// <param name="position">starting position</param>
	public void Instantiate(Transform parent, Vector3 position = new Vector3())
	{
		if(prefab == null) throw new System.NullReferenceException("Invalid or mising GameObject");
		_script = Object.Instantiate(prefab).GetComponent<HIV_Piece>();
		if(_script == null)
		{
			throw new System.NullReferenceException("Null script found");
		}
		else
		{

			if(parent != null) _script.transform.SetParent(parent);
			_script.transform.localPosition = position;
		}
	}

	/// <summary> Instantiates a part with a given parent, color, and position </summary>
	/// <param name="parent">gameObject's parent. null if none</param>
	/// <param name="c">intended color</param>
	/// <param name="position">starting position</param>
	public void Instantiate(Transform parent, Color c, Vector3 position = new Vector3())
	{
		if(prefab == null) throw new System.NullReferenceException("Invalid or mising GameObject");
		_script = Object.Instantiate(prefab).GetComponent<HIV_Piece>();
		if(_script == null)
		{
			throw new System.NullReferenceException("Null script found");
		}
		else
		{

			_script.transform.SetParent(parent);
			_script.transform.localPosition = position;
			_script.color = c;
			color = c;
		}
	}

	#endregion Instantiate

	#region Changes

	#endregion Changes

	#endregion Methods
}

#endregion