﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           HIV_Infect.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// HIV Infect state
/// 	- State functions to infect a designated TCells
///		- State Changes:
/// 		- Changes to wander if te TCell disappears
/// 		- buds and changes to budding when successfully infects a TCell
/// </summary>
public class HIV_Infect : HIV_State
{
	#region Variables

	/// <summary> The distance required to infect the intended TCell. </summary>
	[SerializeField]float infectDistance;

	#endregion

	#region Functions

	/// <summary> Replicate the HIV Virons. </summary>
	/// <returns> <c>true</c> if the game creates just the right amount of replicants of HIV. <c>false</c> if the game has too many HIV pieces as is.</returns>
	bool Replicate( )
	{
		for(var i = 0; i < main.stats.stats[Statistics.Type.REPLICATION_RATE]; ++i)
		{
			var g = Game.main.CreateHIV(transform.position, Quaternion.identity);
			if(g)
			{
				var a = g.GetComponent<HIV>();
				a.ChangeState(HIV.State.BUD, host.gameObject);
			}
			else
			{
				return false; //the game either has a maximum amount of HIV particles in it already or something went wrong
			}
		}
		return true;
	}

	#region Monobehavior

	void OnEnable( )
	{
		main.move.ChangeFollowState(host.transform);
	}

	// Update is called once per frame
	void Update( )
	{
		if(Vector2.Distance(transform.position, host.transform.position) < infectDistance)
		{
			if(Replicate()) gameObject.SetActive(false); //disable itself if it doesn't need to use itself for replication.
			else
			{
				//refresh yourself when you're needed to replicate
				main.ChangeType();
				if(main.stats) main.stats.RefreshStats(main.statList);
				main.ChangeState(HIV.State.BUD, host.gameObject);
			}
		}
		else if(!host.gameObject.activeSelf) main.ChangeState(HIV.State.WANDER); //wander if the tcell deactivates itself
	}

	#endregion

	#endregion
}
