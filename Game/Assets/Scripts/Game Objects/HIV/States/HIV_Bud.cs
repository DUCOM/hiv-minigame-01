﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 *  Name:           HIV_Bud.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary> HIV Bud state. </summary>
public class HIV_Bud : HIV_State
{

	void OnEnable( )
	{
		if(host)
		{
			main.move.ChangeFollowState();
			main.move.velocity = Quaternion.Euler(0f, 0f, Random.Range(0, 360)) * Vector2.up * main.stats.stats[Statistics.Type.SPEED];
			if(main.GetSpike(0).GetComponent<Spike>().tropism.coreceptor == Tropism.CoReceptorType.DUAL)
				main.GetComponent<AudioDatabase> ().Play ("dual");
			main.grouped = false;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate( )
	{
		if(!host || !host.gameObject.activeSelf || Vector2.Distance(transform.position, host.transform.position) > host.radius)
		{
			main.ChangeState(HIV.State.RETURN);
		}
	}
}
