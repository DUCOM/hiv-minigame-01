﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 *  Name:           HIV_Selected.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// HIV Selected
/// 	- State machine when the HIV is selected.
/// 	- Changes to the wander state once deselected.
/// </summary>
public class HIV_Selected : HIV_State
{
	#region Variables

	/// <summary> The intended destination. Where the finger is in world space. </summary>
	[HideInInspector] public Vector3 destination;
	[SerializeField] Rect slowdownBorder;

	#endregion

	#region Functions

	#if UNITY_EDITOR
	void OnEnable( )
	{
		if(main.instantiatedOnStart) main.glow.SetActive(true);
		main.move.ChangeFollowState(destination, 5f);
	}

	void OnDisable( )
	{
		if(main.instantiatedOnStart) main.glow.SetActive(false);
	}
	#else
	void OnEnable()
	{
		main.move.ChangeFollowState(destination, 5f);
	}

	#endif

	// Update is called once per frame
	void FixedUpdate( )
	{
		//don't do FixedUpdate when it instantiated
		#if UNITY_EDITOR
		if(main.instantiatedOnStart) return;
		#endif

		if(!main.selected)
		{
			main.ChangeState(HIV.State.WANDER);
		}
		else
		{
			if(main.move.followState != HIV_Move.State.VECTOR) main.move.ChangeFollowState(destination);
			else main.move.followVector = destination;
			if(!main.weak) transform.Rotate(0, 0, 3f);
		}

		if(OutOfBounds())
		{
			main.move.ChangeMultiplier(1.5f);
		}
		else
		{
			main.move.ChangeMultiplier(5f);
		}

	}

	/// <summary>
	/// Checks whether or not the object is out of bounds to start moving the camera.
	/// </summary>
	/// <returns><c>true</c>, if it's far enough to be out of bounds, <c>false</c> otherwise.</returns>
	public bool OutOfBounds( )
	{
		if(main.move.velocity.x > 0 && transform.position.x > Functions.CameraToWorldRect(Camera.main, slowdownBorder).xMax) return true;
		if(main.move.velocity.x < 0 && transform.position.x < Functions.CameraToWorldRect(Camera.main, slowdownBorder).xMin) return true;
		if(main.move.velocity.y > 0 && transform.position.y > Functions.CameraToWorldRect(Camera.main, slowdownBorder).yMax) return true;
		if(main.move.velocity.y < 0 && transform.position.y < Functions.CameraToWorldRect(Camera.main, slowdownBorder).yMin) return true;

		return false;
	}

	#endregion
}
