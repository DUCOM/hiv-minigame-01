﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           HIV_State.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Got rid of the move component; it's a part of the main component now)
 *  Last Modified:  11 January 2017
 */

/// <summary>
/// HIV State. A basis for all State Machines for the HIV
/// </summary>
[RequireComponent(typeof(HIV))]
public class HIV_State : MonoBehaviour
{
	/// <summary> The HIV instance object </summary>
	protected HIV main;

	/// <summary> The host object for infection purposes (not used on most states) </summary>
	[HideInInspector]public TCell host;

	/// <summary>
	/// Awake this instance. Initializes the main and move variables for further usage
	/// </summary>
	protected void Awake( )
	{
		main = GetComponent<HIV>();
	}
}
