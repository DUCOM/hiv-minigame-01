﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           HIV_Return.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// HIV Return. Returns back to the focal point.
/// 	- Return ensures that the HIV is going back to the focal point as fast as possible.
/// 	- Changes to the wander state when the HIV gets close enough to the focal point.
/// </summary>
public class HIV_Return : HIV_State
{
	#region Monodevelop
	void OnEnable( )
	{
		main.move.ChangeFollowState(main.focalPoint.transform, 2f);
	}

	void Update( )
	{
		if(main.focalPoint.GetComponent<FocalPoint>().GetDistance(transform) < main.focalPoint.GetComponent<FocalPoint>().midpointCutoff)
		{
			//change to the wander state when the virion gets close enough to the focal point
			main.ChangeState(HIV.State.WANDER, main.focalPoint);
		}
	}
	#endregion
}
