﻿using UnityEngine;

/*
 *  Name:           HIV_Death.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary> HIV Death state </summary>
public class HIV_Death : HIV_State
{
	[SerializeField] Animator anim;

	/// <summary> How transparent the object is going to be. </summary>
	public float alpha;

	#region Methods

	public void Die( )
	{
		main.alpha = 1;
		gameObject.SetActive(false);
	}

	#region Monodevelop

	void OnEnable( )
	{
		alpha = 1;
		main.alpha = 1;
		anim.SetTrigger("die");
	}

	// Update is called once per frame
	void Update( )
	{
		main.alpha = alpha;
	}

	#endregion Monodevelop

	#endregion Methods
}
