﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           HIV_Wander.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.5 (Now changes whether or not the HIV is grouped upon enable.)
 *  Last Modified:  23 March 2017
 */

/// <summary> 
/// HIV Wander Class. The Wander state of a virion. 
/// 	- this state follows the focal point until it's either selected or it manages to stray too far
/// </summary>
public class HIV_Wander : HIV_State
{

	/// <summary> Start this instance. For initialization </summary>
	void Start( )
	{
		if(!(main.focalPoint)) enabled = false; //disable if you can't find a focal point
		else main.move.ChangeFollowState(main.focalPoint.transform, 1.5f); //change the follow state to follow the focal point
	}

	void OnEnable()
	{
		main.grouped = true;
	}

	/// <summary> Vector calculations. </summary>
	void FixedUpdate( )
	{
		if(main.selected)
		{
			//change to the selected state if the HIV is selected
			main.ChangeState(HIV.State.SELECTED); 
		}
		else
		{
			FocalPoint p = main.focalPoint.GetComponent<FocalPoint>();
			switch(main.move.followState)
			{
				case HIV_Move.State.OBJECT:

					// don't conventionally follow the focal point when you're close enough to it
					if(p.GetDistance(transform) < p.maxDistance) main.move.ChangeFollowState(null, 1.5f); //change to null follow state if it's going too far
					else if(p.GetDistance(transform) > p.distanceCutoff) main.ChangeState(HIV.State.RETURN); //use only when the threshhold gets too high and it's being blocked by a red blood cell
					break;

				case HIV_Move.State.NONE:
					
					if(p.GetDistance(transform) > p.midpointCutoff) main.move.ChangeFollowState(main.focalPoint.transform, 1.5f); //run after the focal point if the HIV gets halfway between the cutoff and the regular maximum
					else main.move.velocity = p.move.velocity.normalized * Mathf.Lerp(main.move.velocity.magnitude, main.move.speed / 2 > p.move.velocity.magnitude ? main.stats.stats[Statistics.Type.SPEED] : p.move.velocity.magnitude, 0.1f); //move at the same direction of the focalpoint at its own speed.
					break;

				default:
					main.move.ChangeFollowState(null); //change to null follow state if it's somwhere else
					break;
			}
		}
	}
}
