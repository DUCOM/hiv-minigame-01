﻿using UnityEngine;

/*
 *  Name:           Statistics.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */


/// <summary>
/// Given statistics for the HIV
/// </summary>
[System.Serializable]
public struct Statistics
{
	#region enumerations

	/// <summary> THE TYPES OF STATISTICS IN THE STRUCT </summary>
	public enum Type
	{
		SPEED = 0,
		ACCELERATION,
		LIFESPAN,
		DOCKING,
		REPLICATION_RATE
	}

	#endregion

	#region Variables

	float speed;
	float acceleration;
	float lifespan;
	float docking;
	uint replication_rate;

	#endregion

	#region Methods

	#region Constructors

	/// <summary> Creates a new <see cref="Statistics"/> struct </summary>
	/// <param name="s">movement speed</param>
	/// <param name="a">acceleration</param>
	/// <param name="l">lifespan</param>
	/// <param name="d">docking score</param>
	/// <param name="r">replication rate</param>
	public Statistics(float s, float a = 0, float l = 0, float d = 0, uint r = 0)
	{
		speed = s;
		acceleration = a;
		lifespan = l;
		docking = d;
		replication_rate = r;
	}

	/// <summary>
	/// Creates a copied <see cref="Statistics"/>struct.
	/// </summary>
	/// <param name="s">The original <see cref="Statistics"/>.</param>
	public Statistics(Statistics s = new Statistics())
	{
		if(s == new Statistics()) s = base_stats;
		this = s;
	}

	#endregion Constructors

	#region Inspectors

	/// <summary> gets the stat number for the HIV </summary>
	/// <param name="t">the statistic needed</param>
	/// <returns>the stat number</returns>
	public float GetIndex(Type t)
	{
		switch(t)
		{
			case Type.ACCELERATION:
				return acceleration;
			case Type.DOCKING:
				return docking;
			case Type.LIFESPAN:
				return lifespan;
			case Type.REPLICATION_RATE:
				return replication_rate;
			case Type.SPEED:
				return speed;
			default:
				throw new System.IndexOutOfRangeException();
		}
	}

	#endregion Inspectors

	#region Mutators

	/// <summary> Sets the stat needed to given value </summary>
	/// <param name="t">the stats you request to change</param>
	/// <param name="value">the value to change the stat to</param>
	public void SetIndex(Type t, float value)
	{
		switch(t)
		{
			case Type.ACCELERATION:
				acceleration = value;
				break;
			case Type.DOCKING:
				docking = value;
				break;
			case Type.LIFESPAN:
				lifespan = value;
				break;
			case Type.REPLICATION_RATE:
				replication_rate = (uint)value;
				break;
			case Type.SPEED:
				speed = value;
				break;
			default:
				throw new System.IndexOutOfRangeException();
		}
	}

	/// <summary> Levels out the stats to the top possible stats possible </summary>
	/// <param name="s">the statistic to level out</param>
	public static void LevelStats(ref Statistics s)
	{
		setMax(ref s, Type.ACCELERATION);
		setMax(ref s, Type.DOCKING);
		setMax(ref s, Type.LIFESPAN);
		setMax(ref s, Type.REPLICATION_RATE);
		setMax(ref s, Type.SPEED);
	}

	/// <summary> Sets the stat to at least the minimum</summary>
	/// <param name="s">the statistic</param>
	/// <param name="t">the stat type</param>
	static void setMax(ref Statistics s, Type t)
	{
		s[t] = Mathf.Max(minimum_stats[t], s[t]);
	}

	/// <summary> Levels the stats and returns it. For making a copy. </summary>
	/// <param name="s">the intended statistic </param>
	/// <returns> a modified copy of the given statistic</returns>
	public static Statistics LevelStats(Statistics s)
	{
		LevelStats(ref s);
		return s;
	}

	#endregion Mutators

	#region Facilitators

	/// <summary> Adds two statistics </summary>
	/// <param name="stat">the other statistic you want to add</param>
	/// <returns>a new statistic with the added stats</returns>
	public Statistics Add(Statistics stat)
	{
		float s = speed + stat[Type.SPEED];
		float a = acceleration + stat[Type.ACCELERATION];
		float l = lifespan + stat[Type.LIFESPAN];
		float d = docking + stat[Type.DOCKING];
		uint r = replication_rate + (uint)stat[Type.REPLICATION_RATE];

		return new Statistics(s, a, l, d, r);
	}

	/// <summary> Checks if two objects are equal </summary> 
	/// <param name="o"> the object intended to compare</param>
	/// <returns> false if not the same type or a different statistic. True if the same stats. </returns>
	public override bool Equals(object o)
	{
		if(o.GetHashCode() != GetHashCode()) return false;

		Statistics s = (Statistics)o;

		for(var i = (Type)0; i != Type.REPLICATION_RATE; i++) if(s[i] != GetIndex(i)) return false;
		return true;
	}

	/// <summary> Override of the hash code of the value type. For use of overriding </summary>
	/// <returns> The Hash codeof the Statistic type. </returns>
	public override int GetHashCode( )
	{
		return base.GetHashCode();
	}

	#endregion

	#region Operators

	/// <summary> square bracket operator for statitics </summary>
	/// <param name="t">the type of stat needed</param>
	/// <returns>value for the stat needed</returns>
	public float this[Type t]
	{
		get { return GetIndex(t); }
		set { SetIndex(t, value); }
	}

	/// <summary> Adds two Statistics together </summary>
	/// <param name="a">the first statistic</param>
	/// <param name="b">the second statistic</param>
	/// <returns>a new statistic with values added</returns>
	public static Statistics operator +(Statistics a, Statistics b)
	{
		return a.Add(b);
	}

	/// <summary> Checks if two Statistics are equal to each other. </summary>
	/// <param name="a">The first statistic.</param>
	/// <param name="b">The second statistic.</param>
	/// <returns> True if the two statistics are equal. </returns>
	public static bool operator ==(Statistics a, Statistics b)
	{
		return a.Equals(b);
	}

	/// <summary> Checks if two Statistics are not equal to each other. </summary>
	/// <param name="a">The first statistic.</param>
	/// <param name="b">The second statistic.</param>
	/// <returns> True if the two statistics are not equal. </returns>
	public static bool operator !=(Statistics a, Statistics b)
	{
		return !(a.Equals(b));
	}

	#endregion Operators

	#endregion Methods

	#region Defaults

	/// <summary>
	/// the base stats of the statistic
	/// </summary>
	public static Statistics base_stats
	{
		get { return new Statistics(40, 2, 20, 4, 2); }
	}

	/// <summary>
	/// the minimum stats posible
	/// </summary>
	public static Statistics minimum_stats
	{
		get { return new Statistics(1f); }
	}

	#endregion
}