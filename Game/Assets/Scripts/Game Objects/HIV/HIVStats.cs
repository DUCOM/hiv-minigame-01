﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 *  Name:           HIVStats.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2016
 */

/// <summary> HIV stats class.The statistics of each individual virion </summary>
public class HIVStats : MonoBehaviour
{
	/// <summary> Encapsulated. Gets the statistics of the HIV </summary>
	/// <value>The HIV's statistics. </value>
	public Statistics stats { get; private set; }

	/// <summary>  Gets the health of the HIV. </summary>
	/// <value>The health.</value>
	public float health { get; private set; }

	/// <summary>  Checks whether or not the HIV is dying. </summary>
	public bool dying = true;

	/// <summary> Gets a value indicating whether this <see cref="HIV"/> is alive. </summary>
	/// <value><c>true</c> if alive; otherwise, <c>false</c>.</value>
	public bool alive { get { return !(dying && health < 0); } }

	protected HIV_Piece lifeColor;

	protected Color startColor;

	/// <summary> Gets the Health Percentage of the HIV. </summary>
	/// <value>The Health Percentage of the HIV.</value>
	public float hp { get { return (health / stats[Statistics.Type.LIFESPAN]); } }

	/// <summary>  Refreshes the stats of the HIV. </summary>
	/// <param name="statChanges">The List of stat changes from a base stat.</param>
	/// <param name="c">The piece to change color based on life.</param>
	public void RefreshStats(List<Statistics> statChanges, HIV_Piece c = null)
	{
		stats = Statistics.base_stats;

		//change the statistics based on the parts that are given to them
		foreach(var s in statChanges)
		{
			stats = stats + s;
		}
		//levels out the stats so they don't get too high or too low
		stats = Statistics.LevelStats(stats);
		ResetTimer();
		dying = true;

		lifeColor = c;
		if(c) startColor = c.color;
	}

	/// <summary>  Resets the death timer. Does not change whether or not it's dying </summary>
	public void ResetTimer( )
	{
		health = stats[Statistics.Type.LIFESPAN]; //sets health to that
	}

	/// <summary>  the current color based on the start color </summary>
	/// <returns>The current color of the HIV.</returns>
	protected Color CurrentColor( )
	{
		if(health <= 0) return new Color(0.5f, 0.5f, 0.5f);
		float r = (startColor.r - 0.5f) * hp + 0.5f;
		float g = (startColor.g - 0.5f) * hp + 0.5f;
		float b = (startColor.b - 0.5f) * hp + 0.5f;
		return new Color(r, g, b);
	}


	void Update( )
	{
		if(lifeColor)
		{
			lifeColor.color = CurrentColor();
		}
	}

	void LateUpdate( )
	{
		if(!gameObject.activeSelf)
		{
			dying = false;
		}
		if(dying)
		{
			health -= Time.deltaTime;
		}
		//Debug.Log(health);
	}
}
