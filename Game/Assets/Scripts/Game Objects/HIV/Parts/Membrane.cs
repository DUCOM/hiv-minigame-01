﻿using UnityEngine;

/*
 *  Name:           Membrane.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Added a STAT_CHANGE) "contstant" for use in later functions
 *  Last Modified:  21 October 2016
 */

/// <summary>
/// The membrane of the HIV
/// </summary>
public class Membrane : HIV_Piece {

    /// <summary> the maximum number of difference to change of base stats </summary>
    static Statistics STAT_CHANGE { get { return new Statistics(4f); } }

    /// <summary> Changes the statistics based on the color. </summary>
    protected override void ChangeStats()
    {
        float hue, saturation, value;
        Color.RGBToHSV(color, out hue, out saturation, out value);

        _stats[Statistics.Type.SPEED] = STAT_CHANGE[Statistics.Type.SPEED] * (9 * hue * hue - 9 * hue + 1f);
    }
}
