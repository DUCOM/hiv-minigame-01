﻿
/*
 *  Name:           Capsid.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.0.1 (Creation)
 *  Last Modified:  14 October 2016
 */

/// <summary>
/// The capsid of the HIV. Empty for now
/// </summary>
public class Capsid : HIV_Piece
{
	protected override void ChangeStats( )
	{
	}
}