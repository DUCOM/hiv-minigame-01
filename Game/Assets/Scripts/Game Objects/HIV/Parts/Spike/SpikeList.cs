﻿using UnityEngine;


/*
 *  Name:           SpikeList.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2A (Now uses Error Code class.)
 *  Last Modified:  25 January 2017
 */

/// <summary> the list of spikes on an HIV </summary>
[System.Serializable]
public class SpikeList
{
	#region Variables

	#region Properties

	/// <summary> The Spike prefab </summary>
	public GameObject prefab;

	/// <summary> The Spike list tropism </summary>
	public Tropism tropism;

	/// <summary> The number of spikes wanted in the spikeList </summary>
	[SerializeField]
	private uint _count;

	/// <summary> Encapsulated. The number of spikes wanted on the spikeList. Get only </summary>
	public int count { get { return objectList == null ? 0 : objectList.Count; } }

	#endregion

	#region Object Lists

	/// <summary> The list of instances in the spikeList </summary>
	[System.NonSerialized]
	public System.Collections.Generic.List<GameObject> objectList;
	//the list of the game objects intended
	[System.NonSerialized]
	public System.Collections.Generic.List<Spike> scriptList;

	/// <summary> The index of the instance needed </summary>
	/// <param name="i">The index number</param>
	/// <returns>The spike instance for the index</returns>
	public Spike this[int i] { get { return (objectList == null || i >= count) ? null : objectList[i].GetComponent<Spike>(); } }

	#endregion lists

	#endregion Variables

	#region Methods

	#region Instantiate

	/// <summary> Instantiates a spikeList with a parent and a radius </summary>
	/// <param name="parent">the parent of the objects. Null if none</param>
	/// <param name="radius">the radius of the objects.</param>
	public void Instantiate(Transform parent, float radius)
	{
		objectList = new System.Collections.Generic.List<GameObject>(); //creates a new objectList for usage
		scriptList = new System.Collections.Generic.List<Spike>();
		for(var i = 0; i < _count; ++i)
		{
			GameObject a = Object.Instantiate(prefab);
			objectList.Add(a);
			Spike s = a.GetComponent<Spike>();
			if(s != null) //check if the last script that was just added was null
			{
				scriptList.Add(s);
				if(parent != null) s.transform.SetParent(parent);
				s.transform.localPosition = new Vector3();
				float angle = i / (float)_count * (float)360.0;
				s.MoveAndRotate(radius, Quaternion.Euler(0, 0, angle));
				s.tropism = tropism;
			}
			else throw new System.FieldAccessException(ErrorCode.EC0MC); //throw an exception if the spike game Object doesn't have a spike script
		}
	}

	/// <summary>
	/// Instantiates a spikeList with a parent, a radius, and a count
	/// </summary>
	/// <param name="parent">the parent of the object. null if none</param>
	/// <param name="radius">the radius of the object.</param>
	/// <param name="c">the amount of spikes you want</param>
	public void Instantiate(Transform parent, float radius, uint c)
	{
		_count = c;
		Instantiate(parent, radius);
	}

	/// <summary>
	/// Instantiates a spikeList with a parent, a radius, and a count
	/// </summary>
	/// <param name="parent">the parent of the object. null if none</param>
	/// <param name="radius">the radius of the object.</param>
	/// <param name="c">the amount of spikes you want</param>
	/// <param name="t">the intended tropism of the spike</param>
	public void Instantiate(Transform parent, float radius, uint c, Tropism t)
	{
		tropism = t;
		Instantiate(parent, radius, c);
	}

	#endregion Instantiate

	/// <summary>
	/// Change the tropism of all of the objects
	/// </summary>
	/// <param name="t">the intended tropism</param>
	public void ChangeTropism(Tropism t)
	{
		tropism = t;
		for(var i = 0; i < count; ++i) this[i].tropism = t;
	}

	/// <summary>
	/// changes the scaling on the spikes. Does not change size
	/// </summary>
	/// <param name="a">the scale multiplier. Does nothing at 0</param>
	public void Scale(float a)
	{
		if(a == 0) return;
		for(var i = 0; i < count; ++i) this[i].Scale(a);
	}


	#endregion methods
}

