﻿using UnityEngine;


/*
 *  Name:           Spike.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.5 (Now uses error code class)
 *  Last Modified:  25 January 2016
 */

/// <summary> A spike consisting of a GP120 and a GP 41 </summary>
public class Spike : MonoBehaviour
{

	#region variables

	#region parts

	#region gp120

	/// <summary> Encapsulated. The GP120 of the spike. Get Only. </summary>
	public GP120 gp120 { get { return gameObject.GetComponentInChildren<GP120>(); } }

	#endregion

	#region gp41

	/// <summary> Encapsulated. The GP41 of the spike. Get only. </summary>
	public GP41 gp41 { get { return gameObject.GetComponentInChildren<GP41>(); } }

	#endregion gp41

	#endregion parts

	#region tropism

	private Tropism _tropism;
	//NOTE: GETTING RID OF THIS WILL CAUSE AN ERROR
	/// <summary> Encapsulated. The tropism of the spike. Get Only </summary>
	public Tropism tropism
	{
		get { return _tropism; }
		set
		{
			if(value.coreceptor == Tropism.CoReceptorType.NONE || value.coreceptor == Tropism.CoReceptorType.OTHER) throw new System.ArgumentException(ErrorCode.EG0HT);
			gp120.top = value;
			_tropism = value;
		}
	}

	#endregion tropism

	#region pivot

	/// <summary> Encapsulated. The pivot of the spike. Get Only. </summary>
	public Pivot pivot { get { return gameObject.GetComponentInChildren<Pivot>(); } }

	#endregion pivot

	#region Components

	/// <summary>  The Interaction Script for the spike  </summary>
	/// <value>The interaction.</value>
	public SpikeInteract interaction { get { return gameObject.GetComponentInChildren<SpikeInteract>(); } }

	#endregion Components

	#endregion variables

	#region methods

	#region functions

	#region Scale

	/// <summary>
	/// Scale the HIV based on a number.
	/// </summary>
	/// <param name="s">The amount to scale by.</param>
	public void Scale(float s)
	{
		if(s == 0) return;
		transform.localScale = new Vector3(s, s, s);
	}

	#endregion Scale

	#region Move

	/// <summary>
	/// translates the pivot
	/// </summary>
	/// <param name="translation">a vector of how to move</param>
	public void MoveTo(Vector3 translation)
	{
		pivot.transform.localPosition = translation;
	}

	#region AndRotate

	public void MoveAndRotate(Vector3 translation, Quaternion rotation)
	{
		MoveTo(translation);
		pivot.transform.localRotation = rotation;
	}

	public void MoveAndRotate(float radius, Quaternion angle)
	{
		pivot.transform.localPosition = new Vector3(0, radius, 0);
		transform.rotation = angle;
	}

	#endregion AndRotate

	#endregion Move

	#region PivotRotation

	/// <summary>
	/// rotates a pivot from zero
	/// </summary>
	/// <param name="eulerAngles">The angle to rotate in degrees</param>
	public void PivotRotation(Vector3 eulerAngles)
	{
		pivot.transform.rotation = new Quaternion();
		pivot.transform.Rotate(eulerAngles);
	}

	/// <summary>
	/// rotates a pivot from zero
	/// </summary>
	/// <param name="angle">The angle to rotate with quaternions</param>
	public void PivotRotation(Quaternion angle)
	{
		PivotRotation(angle.eulerAngles);
	}

	#endregion PivotRotation

	#endregion functions

	#endregion methods
}