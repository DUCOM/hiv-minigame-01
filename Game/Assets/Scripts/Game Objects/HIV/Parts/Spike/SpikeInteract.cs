﻿using UnityEngine;

/*
 *  Name:           SpikeInteract.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.11 (Spikes now are unable to interact with cells if the HIV is weakened through an antibody)
 *  Last Modified:  10 February 2017
 */


/// <summary> The Interaction script between a Spike and a Receptor </summary>
public class SpikeInteract : MonoBehaviour
{
	#region Variables

	#region Properties

	/// <summary>
	/// The tropism of the spike. Get only.
	/// </summary>
	/// <value>The tropism of the spike.</value>
	public Tropism tropism { get { return spike.tropism; } }

	#endregion Properties

	#region Components

	/// <summary> The spike script </summary>
	public Spike spike { get { return gameObject.GetComponentInParent<Spike>(); } }

	/// <summary> Gets the HIV parent component </summary>
	/// <value>The HIV parent.</value>
	public HIV hiv { get { return gameObject.GetComponentInParent<HIV>(); } }

	#endregion Components


	#endregion Variables

	#region Methods

	bool validCombination(TCell_Receptor check)
	{
		if(check && check.type.receptor == Tropism.ReceptorType.CD4_POSITIVE)
		{
			return tropism.coreceptor == Tropism.CoReceptorType.DUAL ? check.type.coreceptor == Tropism.CoReceptorType.CCR5 || check.type.coreceptor == Tropism.CoReceptorType.CXCR4 
					: tropism.coreceptor == check.type.coreceptor;
		}
		return false;
	}

	#region Monodevelop

	#if false
	/// <summary> Select the receptor instance upon entering. </summary>
	void OnTriggerEnter2D(Collider2D c)
	{
		if(!hiv.selected) return;
		TCell_Receptor s;

		if(c.tag == "Receptor" && c.gameObject.GetComponent<TCell_Receptor>()) s = c.gameObject.GetComponent<TCell_Receptor>();
		else return;

		
#if UNITY_EDITOR
		if(hiv.selected && !hiv.weak)
		
#else
		if((hiv.selected) && validCombination(s) && !hiv.weak)
		#endif
		{
			var cell = s.cell;
			if(!cell.infected)
			{
				cell.infected = true;
				hiv.ChangeState(HIV.State.INFECT, cell.gameObject);
				Game.score += 10 * Game.main.population * (tropism.coreceptor == Tropism.CoReceptorType.DUAL ? 2 : 1);
			}

			//fill the immunogenicity bar by a tenth every time you infect a tcell
			if(UI_Immunogenicity.main) UI_Immunogenicity.main.Add(0.1f);
			
		}
	}
	#endif

	#endregion Monodevelop

	#endregion Methods
}
