﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           Tropism.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release.)
 *  Last Modified:  27 March 2017
 */

/// <summary> The List of possible tropisms for both HIV and TCells </summary>
[System.Serializable]
public struct Tropism
{
	#region Possibilities

	/// <summary> The possible types that a receptor can have. </summary>
	public enum ReceptorType
	{
		CD4_NEGATIVE = 0,
		CD4_POSITIVE = 1,
	}

	/// <summary> The possible types that a coreceptor can have. </summary>
	public enum CoReceptorType
	{
		NONE = 0,
		CCR5 = 1,
		CXCR4 = 2,
		OTHER = 3,
		DUAL = 4
	}

	public static CoReceptorType[] TCellCoreceptor = {
		CoReceptorType.NONE,
		CoReceptorType.CCR5,
		CoReceptorType.CXCR4,
		CoReceptorType.OTHER
	};

	public static ReceptorType[] TCellReceptor = {
		ReceptorType.CD4_NEGATIVE,
		ReceptorType.CD4_POSITIVE
	};

	public static CoReceptorType[] HIVCoreceptor = {
		CoReceptorType.CCR5,
		CoReceptorType.CXCR4,
		CoReceptorType.DUAL
	};

	public static ReceptorType[] HIVReceptor = {
		ReceptorType.CD4_POSITIVE
	};

	#endregion

	#region randomization values

	/// <summary>
	/// The probabilities for a TCell receptor to receive which receptor type 
	/// </summary>
	private static Dictionary<ReceptorType, float> receptorProbabilities = new Dictionary<ReceptorType, float>() {
		{ ReceptorType.CD4_NEGATIVE, 0.6f },
		{ ReceptorType.CD4_POSITIVE, 0.4f }
	};

	/// <summary>
	/// The probabilities for a TCell receptor to receive which coreceptor type
	/// </summary>
	private static Dictionary<CoReceptorType, float> coReceptorProbabilities = new Dictionary<CoReceptorType, float>() {
		{ CoReceptorType.CCR5, 0.275f },
		{ CoReceptorType.CXCR4, 0.2f },
		{ CoReceptorType.NONE, 0.2f },
		{ CoReceptorType.OTHER, 0.325f }
	};

	private static Dictionary<CoReceptorType, float> cd4CoReceptorProbabilities = new Dictionary<CoReceptorType, float>() {
		{ CoReceptorType.CCR5, 0.425f },
		{ CoReceptorType.CXCR4, 0.25f },
		{ CoReceptorType.NONE, 0.2f },
		{ CoReceptorType.OTHER, 0.125f }
	};

	/// <summary>
	/// The probabilities for an HIV Virion to receive which coreceptor type
	/// </summary>
	private static Dictionary<CoReceptorType, float> hivProbabilities = new Dictionary<CoReceptorType, float>() {
		{ CoReceptorType.CCR5, 0.55f },
		{ CoReceptorType.CXCR4, 0.4f },
		{ CoReceptorType.DUAL, 0.05f }
	};

	#endregion

	#region Decoy Colors

	/// <summary> The color list for the Receptor </summary>
	static Color[] receptorDecoyColorList = {
		new Color(156f / 255f, 195f / 255f, 229f / 255f),
		new Color(191f / 255f, 191f / 255f, 191f / 255f),
		new Color(169f / 255f, 210f / 255f, 143f / 255f),
		new Color(224f / 255f, 177f / 255f, 131f / 255f),
		new Color(144f / 255f, 146f / 255f, 192f / 255f)
	};

	/// <summary> The color list for the CoReceptor </summary>
	static Color[] coreceptorDecoyColorList = {
		new Color(239f / 255f, 234f / 255f, 234f / 255f),
		//new Color(170f / 255f, 66f / 255f, 191f / 255f),
		new Color(200f / 255f, 36f / 255f, 29f / 255f),
		//new Color(112f / 255f, 48f / 255f, 160f / 255f),
		new Color(1f / 255f, 176f / 255f, 80f / 255f)
	};

	#endregion

	#region variables

	/// <summary> The receptor type of the tropism. </summary>
	public ReceptorType receptor;

	/// <summary> The coreceptor type of the tropism. </summary>
	public CoReceptorType coreceptor;

	#endregion

	#region methods

	#region Initilization

	/// <summary> Creates a torpism given both types </summary>
	/// <param name="r">the type of receptor</param>
	/// <param name="c">the type of coreceptor</param>
	public Tropism(ReceptorType r = ReceptorType.CD4_NEGATIVE, CoReceptorType c = CoReceptorType.NONE)
	{
		receptor = r;
		coreceptor = c;
	}

	#endregion Initilization

	#region Operations

	/// <summary> Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="Tropism"/>.  </summary>
	/// <param name="o">The <see cref="System.Object"/> to compare with the current <see cref="Tropism"/>.</param>
	/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current <see cref="Tropism"/>; otherwise, <c>false</c>.</returns>
	public override bool Equals(object o)
	{
		if(o.GetHashCode() != GetHashCode()) return false;
		Tropism t = (Tropism)o;
		return t.coreceptor == coreceptor && t.receptor == receptor;
	}

	/// <summary> Serves as a hash function for a particular type. </summary>
	/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
	public override int GetHashCode( )
	{
		return base.GetHashCode();
	}

	#endregion Operations

	#region static functions

	#region invalidHIVType

	/// <summary> Checks if the receptor type is invalid for HIV </summary>
	/// <param name="r">the given receptor type</param>
	/// <returns>true if HIV cannot have that receptor type.</returns>
	public static bool invalidHIVType(ReceptorType r)
	{
		return r == ReceptorType.CD4_NEGATIVE;
	}

	/// <summary> Checks if the coreceptor type is invalid for HIV </summary>
	/// <param name="c">the given coreceptor type</param>
	/// <returns>true if HIV cannot have that coreceptor type.</returns>
	public static bool invalidHIVType(CoReceptorType c)
	{
		return c == CoReceptorType.NONE || c == CoReceptorType.OTHER;
	}

	/// <summary> Checks if tropism is invalid for HIV </summary>
	/// <param name="t">the given tropism</param>
	/// <returns>true if HIV cannot have that tropism.</returns>
	public static bool invalidHIVType(Tropism t)
	{
		return invalidHIVType(t.receptor) || invalidHIVType(t.coreceptor);
	}

	#endregion

	#region defaultcolor

	/// <summary> finds and returns the default color for a receptor type </summary>
	/// <param name="r">the receptor type</param>
	/// <returns>The default color of that receptor type</returns>
	public static Color DefaultColor(ReceptorType r)
	{
		Color GOLDEN = new Color(1, 240f / 255f, 0f);
		switch(r)
		{
			case ReceptorType.CD4_POSITIVE:
				return GOLDEN; //show a gold color when it's CD4 positive
			default:
				return Functions.Random(receptorDecoyColorList);  //returns a random color from the list if it's not CD4 positive
		}
	}

	/// <summary> finds and returns the default color for a coreceptor type </summary>
	/// <param name="r">the coreceptor type</param>
	/// <returns>The default color of that coreceptor type</returns>
	public static Color DefaultColor(CoReceptorType c)
	{
		//Color LIME = new Color(0.5f, 1f, 0f, 1f);
		Color PURPLE = new Color(171f / 255f, 82f / 255f, 255f / 255f, 1f);
		switch(c)
		{
			case CoReceptorType.CXCR4:
				return PURPLE;
			case CoReceptorType.CCR5:
				return Color.cyan;
			case CoReceptorType.NONE:
				return Color.clear;
			case CoReceptorType.DUAL:
				return Color.red;
			default:
				return Functions.Random(coreceptorDecoyColorList); //returns a random color from the list
		}
	}

	#endregion defaultcolor

	#region random functions

	/// <summary> Creates a random tropism </summary>
	/// <returns> a completely random tropism. </returns>
	public static Tropism Random( )
	{
		return Random(Tropism.TCellReceptor, Tropism.TCellCoreceptor);
	}

	public static Tropism Random(ReceptorType[] r, CoReceptorType[] c)
	{
		ReceptorType a = Functions.Random<ReceptorType>(r, receptorProbabilities);
		CoReceptorType b = Functions.Random<CoReceptorType>(c, a == ReceptorType.CD4_POSITIVE ? cd4CoReceptorProbabilities : coReceptorProbabilities);
		return new Tropism(a, b);
	}

	/// <summary> Creates a random tropism for HIV usage </summary>
	/// <returns>a random tropism that is not none or other </returns>
	public static Tropism HIVRandom( )
	{
		return new Tropism(Tropism.HIVReceptor[0], Functions.Random<CoReceptorType>(Tropism.HIVCoreceptor, hivProbabilities));
	}

	#endregion random functions

	#region Operators

	public static bool operator ==(Tropism a, Tropism b)
	{
		return a.Equals(b);
	}

	public static bool operator !=(Tropism a, Tropism b)
	{
		return !(a == b);
	}

	#endregion Operators

	#endregion static functions

	#endregion methods
}
