﻿using UnityEngine;

/*
 *  Name:           Pivot.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.0 (changed spike to parent for a more broad usage. Made a MoveAbout function!)
 *  Last Modified:  16 February 2017
 */

/// <summary>
/// The pivot of the Spike
/// </summary>
public class Pivot : MonoBehaviour
{
	[SerializeField] GameObject _parent;

	public GameObject parent { get { return _parent; } }

	/// <summary> moves to a position and rotates to a translation using polar coordinates </summary>
	/// <param name="radius">radius to rotate about</param>
	/// <param name="angle">angle to rotate</param>
	public void MoveAbout(float radius, Quaternion angle)
	{
		transform.localPosition = new Vector3(0, radius, 0);
		parent.transform.localRotation = angle;
	}

	/// <summary> moves pivot with the stuf in it to a position and rotates using a Vector </summary>
	/// <param name="translation">translation</param>
	/// <param name="rotation">rotation</param>
	public void MoveAbout(Vector3 translation, Quaternion rotation)
	{
		transform.localPosition = translation;
		parent.transform.localRotation = rotation;
	}
}
