﻿using UnityEngine;

/*
 *  Name:           GP120.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.1 (Added the Dual Tropism to the default color list. Also added a particle system for whenever dual tropism is shown)
 *  Last Modified:  2 February 2017
 */

/// <summary>
/// The GP120 of the HIV
/// </summary>
public class GP120 : MonoBehaviour
{

	#region Variables


	private Tropism _top;
	// NOTE: NOT DOING SO GIVES A STACK OVERFLOW ERROR

	/// <summary> Encapsulated. The Receltor type of the GP120 </summary>
	/// <value>The top.</value>
	public Tropism top
	{
		get { return _top; }
		set
		{
			particles.gameObject.SetActive(value.coreceptor == Tropism.CoReceptorType.DUAL);
			displayLeft.color = LeftDefaultColor(value.coreceptor);
			displayRight.color = RightDefaultColor(value.coreceptor);
			_top = value;
		}
	}

	/// <summary> Encapsulated. The statistics of the GP120. Get Only. </summary>
	public Statistics stats
	{
		get {
			Statistics s = new Statistics();
			switch(top.receptor)
			{
				case Tropism.ReceptorType.CD4_POSITIVE:
					s[Statistics.Type.REPLICATION_RATE] = 9;
					break;
				case Tropism.ReceptorType.CD4_NEGATIVE:
					s[Statistics.Type.REPLICATION_RATE] = 2;
					break;
			}
			return s;
		}
	}

	[SerializeField]SpriteRenderer _displayLeft;
	[SerializeField]SpriteRenderer _displayRight;
	[SerializeField] ParticleSystem _particles;

	public SpriteRenderer displayLeft { get { return _displayLeft; } }

	public SpriteRenderer displayRight { get { return _displayRight; } }

	public ParticleSystem particles { get { return _particles; } }

	#endregion

	public Color GetColorLeft( )
	{
		return displayLeft.color;
	}

	public Color GetRightColor( )
	{
		return displayRight.color;
	}

	public Color LeftDefaultColor(Tropism.CoReceptorType c)
	{
		
		return c == Tropism.CoReceptorType.DUAL ? Tropism.DefaultColor(Tropism.CoReceptorType.CCR5) : Tropism.DefaultColor(c);
	}

	public Color RightDefaultColor(Tropism.CoReceptorType c)
	{
		return c == Tropism.CoReceptorType.DUAL ? Tropism.DefaultColor(Tropism.CoReceptorType.CXCR4) : Tropism.DefaultColor(c);
		;
	}
}
