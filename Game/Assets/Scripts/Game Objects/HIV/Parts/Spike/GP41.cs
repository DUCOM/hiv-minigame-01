﻿using UnityEngine;

/*
 *  Name:           GP41.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.5 (Took the tropism out)
 *  Last Modified:  14 November 2016
 */

/// <summary> The GP41 of the HIV </summary>
public class GP41 : MonoBehaviour
{
	#region Variables



	/// <summary> Encapsulated. Gets the statistics of the GP41 </summary>
	public Statistics stats
	{
		get { return new Statistics(); }
	}

	public SpriteRenderer spriteRender
	{
		get { return gameObject.GetComponentInChildren<SpriteRenderer>(); }
	}

	/// <summary> Encapsulated. Gets te MeshRenderer of the GP41. Get Only. null if no meshrenderer in children. </summary>
	public MeshRenderer meshRenderer
	{
		get { return gameObject.GetComponentInChildren<MeshRenderer>(); }
	}

	#endregion
}
