﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           HIV_Glow.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Small comments. Now Obselete)
 *  Last Modified:  20 December 2016
 */

/// <summary>
/// The script for the glow object for HIV. WARNING: THIS IS OBSELETE!
/// </summary>
public class HIV_Glow : MonoBehaviour
{
	#region Variables

	[Header("Properties")]
	/// <summary>
	/// The sprite render.
	/// </summary>
	[SerializeField] SpriteRenderer spriteRender;

	/// <summary>
	/// The radius of the circle.
	/// </summary>
	[SerializeField] float radius;

	/// <summary>
	/// The alpha component of the color.
	/// </summary>
	[Range(0, 1)][SerializeField] float alpha;


	float t;
	bool up;

	float radiusMedium;

	const float RADIUS_ORIG = 5.12f;

	[Header("Change Timer")]
	[Range(0.1f, 10)][SerializeField] float timeDilation;

	[Header("Radius Changes")]
	/// <summary>
	/// Whether or not the user wants to change the radius
	/// </summary>
	[SerializeField] bool radiusChange;

	[Range(5, 35)][SerializeField] float radiusMin;
	[Range(5, 35)][SerializeField] float radiusMax;

	[Header("Alpha Changes")]
	[SerializeField] bool alphaChange;
	[Range(0, 0.9f)][SerializeField] float alphaOffset;





	#endregion Variables

	// Use this for initialization
	void Start( )
	{
		t = 0f;
		radiusMedium = (radiusMax + radiusMin) / 2;
		if(radiusChange) radius = radiusMedium;
		else radius = radiusMax;
		if(alphaChange) alpha = 0.5f;
		else alpha = 1;
	}
	
	// Update is called once per frame
	void Update( )
	{
		t += up ? Time.deltaTime : -Time.deltaTime;
		if((up && t >= timeDilation) || (!up && t <= -timeDilation)) up = !up;

		radius = (t * (radiusMax - radiusMin) + radiusMedium * 2) / 2;
		if(alphaChange) alpha = Mathf.Min((t + 1) / 2 + alphaOffset, 1);



		transform.localScale = Vector3.one * radius / RADIUS_ORIG;
		spriteRender.color = new Color(spriteRender.color.r, spriteRender.color.g, spriteRender.color.b, alpha);
	}
}
