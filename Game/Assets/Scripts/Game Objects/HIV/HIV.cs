﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           HIV.cs
 *  Author:         Vincent Mills
 *  Version:        0.2.10 (Now has a grouped boolean to dictate whether or not the HIV ever grouped with the rest of them.)
 *  Last Modified:  23 March 2017
 */


/// <summary> The HIV Script. </summary>
/// <remarks>Requires HIV_Move.</remarks>
[RequireComponent(typeof(HIV_Move))]
public class HIV : MonoBehaviour
{

	#region enumerations

	/// <summary> The List of states the HIV can be in. </summary>
	public enum State
	{
		NONE = -1,
		WANDER = 0,
		SELECTED = 1,
		RETURN = 2,
		INFECT = 3,
		BUD = 4,
		DEATH = 5
	}

	#endregion

	#region Constants

	/// <summary> The original radius of the HIV Game Object </summary>
	const float RADIUS_ORIGINAL = 7f;

	#endregion

	#region Variables

	/// <summary>
	/// The Focal Point Game Object.
	/// </summary>
	public GameObject focalPoint;

	#region Properties

	[Header("Properties")]

	public bool grouped;

	/// <summary>
	/// The radius of the HIV.
	/// </summary>
	[SerializeField] private float _radius;

	/// <summary> Encapsulated. The public radius of the HIV. Automatically scales. </summary>
	public float radius
	{
		get { return _radius; }
		set
		{
			float scale = value / RADIUS_ORIGINAL;

			transform.localScale = new Vector3(scale, scale, scale);
			if(capsid.script != null) capsid.script.transform.localScale = Vector3.one; //resets the scale for the cytoplasm
			if(membrane.script != null) membrane.script.transform.localScale = Vector3.one; //resets the scale for the membrane
			if(matrix.script != null) matrix.script.transform.localScale = Vector3.one; //resets the scale for the nucleous
			foreach(var r in rna) if(r.script != null) r.script.transform.localScale = Vector3.one; //resets the scale for the cytoplasm
			foreach(var r in rt) if(r.script != null) r.script.transform.localScale = Vector3.one; //resets the scale for the cytoplasm

			spikeList.Scale(1 / scale);
			_radius = value;
		}
	}

	/// <summary> The list of statistics for each of the HIV's pieces </summary>
	/// <value>The stat list.</value>
	public List<Statistics> statList
	{ 
		get {
			List<Statistics> a = new List<Statistics>();
			a.Add(capsid.script.stats);
			a.Add(membrane.script.stats);
			a.Add(matrix.script.stats);
			a.Add(rna[0].script.stats);
			a.Add(rt[0].script.stats);

			return a;
		}
	}

	/// <summary> The HIV statistics </summary>
	/// <value>The stats of the HIV as a whole.</value>
	public HIVStats stats { get { return gameObject.GetComponent<HIVStats>(); } }

	/// <summary> Encapsulated. The current state of the HIV object </summary>
	/// <value> The intended state of the HIV object</value>
	public State state { get; private set; }

	Dictionary<State, HIV_State> stateList;

	/// <summary> Gets a value indicating whether this <see cref="HIV"/> is selected. </summary>
	/// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
	[HideInInspector] public bool selected;

	private bool _weakened;

	public bool weak
	{
		get { return _weakened; }
		set
		{
			if(value) GP120_Color_Left = GP120_Color_Right = GP41_Color = Color.gray; 
			_weakened = value;
		}
	}

	//NOTE: for use for debugging purposes. Will not be used during normal gameplay
	#if UNITY_EDITOR
	/// <summary> Checks whether or not the HIV will instantiate the moment it's created. </summary>
	[SerializeField] bool instantiateOnStart;

	public bool instantiatedOnStart { get; protected set; }

	/// <summary>
	/// Gets the glow game object for debugging purposes
	/// </summary>
	/// <value>The glow game object.</value>
	public GameObject glow { get { return sparkle; } }
	#endif

	#endregion Properties

	#region Parts

	[Header("HIV Parts")]
    

	[SerializeField]
	private HIV_Part capsid;

	/// <summary> Gets the color of the capsid. </summary>
	/// <value>The color of the capsid.</value>
	public Color capsidColor { get { return capsid.color; } private set { capsid.color = value; } }


	[SerializeField] private HIV_Part matrix;

	/// <summary> Gets the color of the matrix. </summary>
	/// <value>The color of the matrix.</value>
	public Color matrixColor { get { return matrix.color; } private set { matrix.color = value; } }

	[SerializeField] private HIV_Part membrane;

	/// <summary> Gets the color of the membrane. </summary>
	/// <value>The color of the membrane.</value>
	public Color membraneColor { get { return membrane.color; } private set { membrane.color = value; } }

	[SerializeField] private HIV_Part[] rna;

	/// <summary> Gets the color of the rna. </summary>
	/// <value>The color of the rna.</value>
	public Color rnaColor
	{
		get { return rna[0].color; }
		private set
		{
			foreach(var a in rna) a.color = value;
		}
	}

	[SerializeField] private HIV_Part[] rt;

	/// <summary> Gets the color of the reverse transcriptace. </summary>
	/// <value>The color of the rt.</value>
	public Color rtColor
	{
		get { return rt[0].color; }
		private set
		{
			foreach(var a in rt) a.color = value;
		}
	}

	[SerializeField] private SpikeList spikeList;

	public Color GP120_Color_Left
	{
		get { return spikeList.scriptList[0].gp120.displayLeft.color; }
		set
		{ 
			foreach(var a in spikeList.scriptList) a.gp120.displayLeft.color = value;
		}
	}

	public Color GP120_Color_Right
	{
		get { return spikeList.scriptList[0].gp120.displayRight.color; }
		set
		{ 
			foreach(var a in spikeList.scriptList) a.gp120.displayRight.color = value;
		}
	}

	/// <summary> Encapsulated. Gives the color of the GP 41. Get only. </summary>
	/// <value>The color of the GP 41.</value>
	public Color GP41_Color
	{
		get { return spikeList.scriptList[0].gp41.spriteRender.color; }
		set
		{
			foreach(var a in spikeList.scriptList) a.gp41.spriteRender.color = value;
		}
	}


	public float alpha
	{
		get {
			return capsidColor.a;
		}
		set
		{
			capsidColor = new Color(capsidColor.r, capsidColor.g, capsidColor.b, value);
			matrixColor = new Color(matrixColor.r, matrixColor.g, matrixColor.b, value);
			membraneColor = new Color(membraneColor.r, membraneColor.g, membraneColor.b, value);
			rnaColor = new Color(rnaColor.r, rnaColor.g, rnaColor.b, value);
			rtColor = new Color(rtColor.r, rtColor.g, rtColor.b, value);
			GP120_Color_Left = new Color(GP120_Color_Right.r, GP120_Color_Right.g, GP120_Color_Right.b, value);
			GP120_Color_Right = new Color(GP120_Color_Right.r, GP120_Color_Right.g, GP120_Color_Right.b, value);
			GP41_Color = new Color(GP41_Color.r, GP41_Color.g, GP41_Color.b, value);
		}
	}

	/// <summary> The gameobject that distinguishes the selected HIV </summary>
	[SerializeField] private GameObject sparkle;

	#endregion Parts

	#region Components

	/// <summary>
	/// The movement component of HIV
	/// </summary>
	/// <value>The movement component.</value>
	public HIV_Move move { get; protected set; }

	#endregion

	#endregion Variables

	#region Methods

	#region Component Functions

	/// <summary>
	/// Finds and returns the spike needed
	/// </summary>
	/// <returns>The spike game object.</returns>
	/// <param name="spikeNumber">The spike index of the desired HIV.</param>
	public GameObject GetSpike(int spikeNumber)
	{
		if(spikeNumber > spikeList.count || spikeNumber < 0) throw new System.Exception(ErrorCode.EA0OR);

		return spikeList.scriptList[spikeNumber].gameObject;
		
	}

	#endregion Component Functions

	#region ChangeType

	/// <summary> Changes the type of the HIV to a certain type </summary>
	/// <param name="matrixColor">the color of the matrix</param>
	/// <param name="capsidColor">the color of the capsid</param>
	/// <param name="membraneColor">the color of the membrane</param>
	/// <param name="rnaColor">the color of the rna</param>
	/// <param name="rtColor">the color of the RT</param>
	/// <param name="t">The tropism of the HIV</param>
	public void ChangeType(Color matrixColor, Color capsidColor, Color membraneColor, Color rnaColor, Color rtColor, Tropism t)
	{
		matrix.color = matrixColor;
		capsid.color = capsidColor;
		membrane.color = membraneColor;

		foreach(var r in rna) r.color = rnaColor;

		foreach(var r in rt) r.color = rtColor;

		spikeList.ChangeTropism(t);
		stats.RefreshStats(statList, matrix.script);
	}

	public void ChangeType( )
	{
		ChangeType(Color.red, capsid.color, membrane.color, rna[0].color, rt[0].color, Tropism.HIVRandom()); //make sure to change the type as if it's a new HIV
	}

	#endregion

	#region Instantiate

	/// <summary> Creates an HIV with default colors </summary>
	/// <param name="r">Intended radius. Set 0 or none at all for its existing radius. </param>
	public void Instantiate(float r = 0f)
	{
		if(r < 0f) throw new System.ArgumentException(ErrorCode.DisplayVariableCode(ErrorCode.VariableCodes.EV0NN, "radius"));
		if(r == 0f) r = radius;

		capsid.Instantiate(gameObject.transform);
		matrix.Instantiate(gameObject.transform);
		membrane.Instantiate(gameObject.transform);
		for(var i = 0; i < rna.Length; ++i) rna[i].Instantiate(transform, new Vector3(0, i * 2 - 1, 0));
		for(var i = 0; i < rt.Length; ++i) rt[i].Instantiate(transform, new Vector3(0, i * 2 - 1, 0));
		spikeList.Instantiate(transform, r);
		radius = r;
	}

	/// <summary> Creates an HIV with the following colors: </summary>
	/// <param name="capsidColor">Capsid Color</param>
	/// <param name="matrixColor">Matrix Color</param>
	/// <param name="membraneColor">membrane color</param>
	/// <param name="rnaColor">rna color</param>
	/// <param name="rtColor">rt color</param>
	/// <param name="GP120Color">GP120 color</param>
	/// <param name="GP41Color">GP 41 color</param>
	/// <param name="r">Intended radius. set 0 or none at all for its existing radius.</param>
	public void Instantiate(Color matrixColor, Color capsidColor, Color membraneColor, Color rnaColor, Color rtColor, Color GP120Color, Color GP41Color, float r = 0f)
	{
		if(r < 0f) throw new System.ArgumentException(ErrorCode.DisplayVariableCode(ErrorCode.VariableCodes.EV0NN, "radius"));
		if(r == 0f) r = radius;

		capsid.Instantiate(gameObject.transform, capsidColor);
		matrix.Instantiate(gameObject.transform, matrixColor);
		membrane.Instantiate(gameObject.transform, membraneColor);
		for(var i = 0; i < rna.Length; ++i) rna[i].Instantiate(transform, rnaColor, new Vector3(0, i * 2 - 1, 0));
		for(var i = 0; i < rt.Length; ++i) rt[i].Instantiate(transform, rtColor, new Vector3(0, i * 2 - 1, 0));
		spikeList.Instantiate(transform, r);

		radius = r;
	}

	#endregion Instantiate

	#region StateFunctions

	public void ChangeState(State s, GameObject g = null)
	{
		if(state != State.NONE) stateList[state].enabled = false; //disable the script that is associated with that state

		CircleCollider2D collider = gameObject.GetComponentInChildren<HIVCollision>().GetComponent<CircleCollider2D>(); //find the collider component to turn on or off
		switch(s)
		{

			case State.SELECTED:
				collider.enabled = true; // keep the collider on
				collider.gameObject.layer = LayerMask.NameToLayer("HIV Selected"); //collides with all things that HIV can, except for other HIV
				sparkle.SetActive(true); // you need the sparkle to glow properly
				if(stats) stats.dying = true;
				transform.position = new Vector3(transform.position.x, transform.position.y, -1); //set the position to -1 for viewing purposes
				break;

			case State.WANDER:
				collider.enabled = true; //turn on the collider because the AI needs it to block the way
				collider.gameObject.layer = LayerMask.NameToLayer("HIV"); //collides with all things that HIV can including itself
				sparkle.SetActive(false); //you're not selected so you don't need the glow
				if(stats) stats.dying = true;
				transform.position = new Vector3(transform.position.x, transform.position.y, -1);
				break;

			case State.RETURN:
				collider.enabled = false; //turn off the collider so the HIV can get close enough to make things happen
				sparkle.SetActive(false); //this isn't selected so it doesn't need the sparkle
				if(stats) stats.dying = true;
				transform.position = new Vector3(transform.position.x, transform.position.y, -1);				
				break;

			case State.INFECT: //infect and bud are similar to each other so they don't need their own set of rules
			case State.BUD: 
				if(g) stateList[s].host = g.GetComponent<TCell>(); //find the host and bleed on to the other states
				collider.enabled = false; //disable the collider
				sparkle.SetActive(false); //you don't need the glow
				if(stats) stats.dying = false; 
				transform.position = new Vector3(transform.position.x, transform.position.y, 0f); //set the position to 0 for viewing purposes
				break;
			case State.DEATH:
				move.velocity = Vector2.zero; //stop
				collider.enabled = false; //turn off the collider so it can die properly
				sparkle.SetActive(false); //turn off the sparkle
				foreach(var i in spikeList.scriptList)
				{
					i.gp120.particles.gameObject.SetActive(false); //disable particle effects
				}
				break;
		}
		state = s;
		if(s != State.NONE) stateList[state].enabled = true;
	}


	public bool CheckStateDying( )
	{
		return state == State.WANDER || state == State.RETURN || state == State.SELECTED;
	}

	#endregion

	/// <summary>
	/// Checks the index of the spike on the HIV given the spike. Returns -1 if there is none
	/// </summary>
	/// <returns>The spike index. -1 if there is no index</returns>
	/// <param name="s">The spike to use for the search.</param>
	public int GetSpikeNumber(Spike s)
	{
		for(var i = 0; i < spikeList.count; ++i)
		{
			if(spikeList[i] == s) return i;
		}
		return -1;
	}

	#region Monodevelop

	void Awake( )
	{
		move = GetComponent<HIV_Move>();

		stateList = new Dictionary<State, HIV_State>() {
			{ State.BUD, GetComponent<HIV_Bud>() },
			{ State.INFECT, GetComponent<HIV_Infect>() },
			{ State.SELECTED, GetComponent<HIV_Selected>() },
			{ State.RETURN, GetComponent<HIV_Return>() },
			{ State.WANDER, GetComponent<HIV_Wander>() },
			{ State.DEATH, GetComponent<HIV_Death>() },
		};
		focalPoint = FocalPoint.main ? FocalPoint.main.gameObject : null;

		#if UNITY_EDITOR
		instantiatedOnStart = focalPoint ? false : instantiateOnStart; //editor only; for debugging purposes
		if(!focalPoint)
		{
			if(instantiateOnStart)
			{
				Instantiate(Color.red, Color.green, Color.yellow, Color.white, Color.white, Color.cyan, Color.gray, radius); //default colors when initialized. Change when it's time to change
				stats.dying = false; //don't want the HIV to die
			}
			enabled = false; //disable when there's no focal point around
		}
		#else
		if(!focalPoint) enabled = false; //disable when there's no focal point around
		#endif
	}

	void Start( )
	{

		foreach(var i in stateList)
		{
			i.Value.enabled = false;
		}
		ChangeState(State.BUD);
	}

	void OnEnable( )
	{
		weak = false;
		ChangeState(State.BUD);
	}

	void OnDisable( )
	{
		weak = false;
		ChangeState(State.BUD);
	}

	void LateUpdate( )
	{
		if(stats && !stats.alive)
		{
			if(state != State.DEATH) ChangeState(State.DEATH);
		}

	}

	void FixedUpdate()
	{
		if (!weak && (state == State.RETURN || state == State.WANDER))
			transform.Rotate (0f, 0f, -1.5f);
	}
	#endregion

	#endregion Methods
}