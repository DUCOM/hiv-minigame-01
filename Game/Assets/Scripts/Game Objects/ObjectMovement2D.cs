﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           ObjectMovement2D.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Created a "ChangeMultiplier" function for use on the fly.)
 *  Last Modified:  15 February 2017
 */

/// <summary>
/// Object Movement 2D class
/// 	= grants the ability to move along 2D spaces
/// 	= allows the user to follow objects with a set speed
/// 	= all you have to do is to tell it what to follow and it will do so
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class ObjectMovement2D : MonoBehaviour
{
	#region enumerations

	/// <summary> 
	/// 	The following state of the move object. 
	/// 	<c>NONE<c> if not following anything, 
	/// 	<c>OBJECT</c> if following a transform, 
	/// 	<c>VECTOR</c> if following a position 
	/// </summary>
	public enum State
	{
		OBJECT,
		VECTOR,
		NONE
	}

	#endregion

	#region Variables

	#region Components

	/// <summary> The Rigidbody2D component of the instance. </summary>
	protected Rigidbody2D _rb;

	#endregion

	#region follow state

	/// <summary> Gets the followState of the instance. </summary>
	/// <value>The state of the instance.</value>
	public State followState { get; protected set; }

	[HideInInspector]public Transform followObject;

	[HideInInspector]public Vector3 followVector;

	#endregion follow state

	#region Properties

	/// <summary> Gets or sets the velocity of the HIV. </summary>
	/// <value>The velocity of the instance.</value>
	public Vector2 velocity
	{
		get { return _rb.velocity; }
		set { _rb.velocity = value; }
	}

	/// <summary> Gets the maximum speed of the HIV </summary>
	/// <value>The maximum speed of the instance.</value>
	public float maxSpeed { get; set; }

	/// <summary> Gets the current speed of the HIV. </summary>
	/// <value>The current speed of the instance.</value>
	public float speed { get { return _rb.velocity.magnitude; } }

	/// <summary> Gets the speed multiplier of the HIV </summary>
	/// <value>The speed multiplier of the instance.</value>
	public float multiplier { get; protected set; }

	/// <summary> Whether or not the object teleports to the location instead of simply moving. </summary>
	[HideInInspector] public bool teleportEnabled;

	/// <summary> if the object gets within this amount of distance, they will stop </summary>
	public float distanceThreshhold;

	#endregion Properties

	#endregion

	#region Functions

	public void ChangeMultiplier(float a)
	{
		if(a <= 0f) return;
		multiplier = a;
	}

	#region follow state

	/// <summary>
	/// Changes where the main object will follow
	/// </summary>
	/// <param name="follow">The object to Follow. Null if you don't want to follow anything.</param>
	/// <param name="multiple">Speed multiplier if needed.</param>
	/// <param name="teleport">If set to <c>true</c> then teleportation will be enabled.</param>
	/// <param name="trail">If set to <c>true</c> then, if there is no object to follow, maintain that same direction. False if you want to go to a complete stop.</param>
	public void ChangeFollowState(Transform follow = null, float multiple = 1f, bool teleport = false, bool trail = false)
	{
		followObject = follow;
		if(follow == null)
		{
			followState = State.NONE;
			if(!trail) velocity = Vector2.zero;
		}
		else followState = State.OBJECT;
		multiplier = multiple <= 0 ? 1f : multiple;
		teleportEnabled = teleport;
	}

	/// <summary>
	/// Changes where the main object will follow
	/// </summary>
	/// <param name="follow">The position to follow.</param>
	/// <param name="multiple">Speed multiplier if needed.</param>
	/// <param name="teleport">If set to <c>true</c> then teleportation will be enabled.</param>
	public void ChangeFollowState(Vector2 follow, float multiple = 1f, bool teleport = false)
	{
		followState = State.VECTOR;
		followVector = follow;
		multiplier = multiple <= 0f ? 1f : multiple;
		teleportEnabled = teleport;
	}

	#endregion follow state

	#region move functions

	/// <summary> Moves to or teleports to the intended poition </summary>
	/// <param name="pos">The intended position.</param>
	protected void MoveTo(Vector3 pos)
	{
		if(teleportEnabled) transform.position = pos;
		else
		{
			Vector3 d = pos - transform.position;

			if(d.magnitude < maxSpeed / 50f) velocity = d.normalized * Mathf.Lerp(speed, 0f, 1f);
			else _rb.velocity = d.normalized * Mathf.Lerp(speed, (maxSpeed * multiplier), 0.1f);
		}
	}

	#endregion

	#region Monodevelop

	// Use this for initialization
	protected void Awake( )
	{
		_rb = GetComponent<Rigidbody2D>();
	}

	protected void Start( )
	{
		if(maxSpeed == 0) maxSpeed = 50f;
	}

	protected void Move( )
	{
		switch(followState)
		{
			case State.OBJECT:
				if(followObject && followObject.gameObject.activeInHierarchy) MoveTo(followObject.position);
				else ChangeFollowState(null, multiplier, teleportEnabled, true);
				break;
			case State.VECTOR:
				MoveTo(followVector);
				break;
		}	
	}

	void FixedUpdate( )
	{
		Move();
	}

	#endregion

	#endregion
}
