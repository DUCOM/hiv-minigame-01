﻿using UnityEngine;

/*
 *  Name:           TCell_Piece.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.4 (Got rid of MeshRenderer in favor of SpriteRenderer.)
 *  Last Modified:  18 November 2016
 */

/// <summary> Abstract. A piece for every t-cell. DO NOT USE ALONE! </summary>
public abstract class TCell_Piece : MonoBehaviour
{

	#region variables

	#region color


	private Color _color;

	/// <summary>
	/// Encapsulated. The Color of the TCell_Piece.
	/// </summary>
	public Color color
	{
		get { return _color; }
		set
		{
			spriteRender.color = value;
			_color = value;
		}
	}

	#endregion

	#region spriteRender

	/// <summary> Gets the sprite renderer of the TCell_Piece. </summary>
	/// <value>The sprite renderer.</value>
	protected SpriteRenderer spriteRender { get { return gameObject.GetComponentInChildren<SpriteRenderer>(); } }

	#endregion MeshRender

	#endregion variables
}


/// <summary> A part of a TCell. For direct use with a TCell. </summary>
[System.Serializable]
public class TCell_Part
{
	#region Variables

	[SerializeField]
	public GameObject prefab;

	[SerializeField]
	private Color color_;

	/// <summary>
	/// Encapsulated. Gets the color of the object.
	/// </summary>
	public Color color
	{
		get {
			return color_;
		}
		set
		{
			instance.color = value;
			color_ = value;
		}
	}

	[System.NonSerialized]
	public TCell_Piece instance;

	#endregion

	#region Methods

	#region Instantiate

	/// <summary> instantiates an instance of the TCell part with given color </summary>
	/// <param name="parent">The parent of the object. Null if none</param>
	/// <param name="c">the color of the object</param>
	/// <param name="position">The object position</param>
	public void Instantiate(Transform parent, Color c, Vector3 position = new Vector3())
	{
		if(prefab == null) throw new System.NullReferenceException("Invalid or missing GameObject");

		instance = Object.Instantiate(prefab).GetComponent<TCell_Piece>();

		if(instance == null)
		{
			throw new System.NullReferenceException("Null script found");
		}
		else
		{
			instance.transform.parent = parent;
			instance.transform.localPosition = position;
			color = c;
		}
	}

	/// <summary> instantiates an instance of the TCell part with default color </summary>
	/// <param name="parent">The parent of the object</param>
	/// <param name="position">The local position to put it</param>
	public void Instantiate(Transform parent, Vector3 position = new Vector3())
	{
		if(prefab == null) throw new System.NullReferenceException("Invalid or missing GameObject");

		instance = Object.Instantiate(prefab).GetComponent<TCell_Piece>();

		if(instance == null)
		{
			throw new System.NullReferenceException("Null script found");
		}
		else
		{
			if(parent != null) instance.transform.parent = parent;
			instance.transform.localPosition = position;
		}
	}

	/// <summary> Creates an instance of the TCell part with default position and color </summary>
	public void Instantiate( )
	{
		if(prefab == null) throw new System.NullReferenceException("invalid or missing game object");
		instance = Object.Instantiate(prefab).GetComponent<TCell_Piece>();
		if(instance == null) throw new System.NullReferenceException("Null script found");
	}

	#endregion Instantiate

	#endregion Methods
}



