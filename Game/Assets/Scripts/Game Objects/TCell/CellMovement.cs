﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           CellMovement.cs
 *  Author:         Vincent Mills
 *  Version:        0.2.0 (Renamed from "TCellMove" to "CellMovement" for better clarity. Deleted the cell variable).
 *  Last Modified:  16 December 2016
 */

/// <summary> the movement script for the tcell. For separation purposes </summary>
public class CellMovement : MonoBehaviour
{
	
	#region Variables

	#region Properties

	/// <summary>
	/// The speed of the tcell
	/// </summary>
	[Header("Properties")]
	[SerializeField] private float speed;

	#endregion Properties

	#region Components

	#region Rigidbody

	/// <summary> Encapsulated. The rigidbody (3D) of the game. </summary>
	public Rigidbody2D rb
	{
		get {
			var r = gameObject.GetComponent<Rigidbody2D>();
			if(r == null)
			{
				r = gameObject.AddComponent<Rigidbody2D>();
				if(r == null) throw new System.NotSupportedException("Invalid component");
				r.gravityScale = 0f;
				r.constraints = RigidbodyConstraints2D.FreezeRotation;
			}
			return r;
		}
	}

	#endregion


	#endregion Components

	#endregion Variables

	#region Methods

	#region disable

	/// <summary> Disables self </summary>
	public void disable( )
	{
		gameObject.SetActive(false);
	}

	#endregion disable

	#region Set Velocity

	/// <summary> Changes the Velocity starting rotation around right direction. </summary>
	/// <param name="speed">the speed of the velocity</param>
	/// <param name="angle">the angle to rotate about the right vedctor</param>
	public void SetVelocity(float speed, float angle)
	{
		ChangeVelocity(speed, angle, Vector3.right);
	}

	/// <summary> Changes the velocity starting rotation around an angle </summary>
	/// <param name="speed">the intended speed</param>
	/// <param name="angle">the angle to rotate about the starting vector</param>
	/// <param name="startingVector">the vector to rotate about</param>
	public void ChangeVelocity(float speed, float angle, Vector3 startingVector)
	{
		SetVelocity(Quaternion.Euler(0f, 0f, angle) * startingVector, speed);
	}

	/// <summary> Changes the velocity to a given velocity </summary>
	/// <param name="velocity">the given velocity</param>
	public void SetVelocity(Vector3 velocity)
	{
		rb.velocity = velocity;
	}

	/// <summary> Changes the velocity with just a direction and speed </summary>
	/// <param name="direction">the direction of the intended velocity</param>
	/// <param name="speed">The speed of the intended velocity</param>
	public void SetVelocity(Vector3 direction, float speed)
	{
		SetVelocity(direction.normalized * speed);
	}

	#endregion Set Velocity

	#region ShootTowards

	/// <summary> Shoot towards a position given a speed and position </summary>
	/// <param name="position">intended position</param>
	/// <param name="speed">Intended speed</param>
	public void ShootTowards(Vector3 position, float speed)
	{
		SetVelocity(position - transform.position, speed);
	}

	#endregion

	#region Monodevelop

	#endregion Monodevelop

	#endregion Methods
}
