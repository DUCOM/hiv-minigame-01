﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           TCell_Receptor_Stem.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation).
 *  Last Modified:  11 November 2016
 */

public class TCell_Receptor_Stem : MonoBehaviour
{

	public Color color
	{ 
		get { return gameObject.GetComponentInChildren<SpriteRenderer>().color; } 
		set { gameObject.GetComponentInChildren<SpriteRenderer>().color = value; } 
	}

}
