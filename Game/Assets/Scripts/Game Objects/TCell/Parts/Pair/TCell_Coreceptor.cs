﻿using UnityEngine;

/*
 *  Name:           TCell_Coreceptor.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.4 (Slight tweaks. Got rid of the meshrenderer)
 *  Last Modified:  25 January 2017
 */

/// <summary> The Corecpetor of a TCell. </summary>
public class TCell_Coreceptor : MonoBehaviour
{
	#region Variables

	#region Type

	private Tropism.CoReceptorType _type;

	/// <summary> Encapsulated. The tropism type of the Coreceptor. </summary>
	public Tropism.CoReceptorType type
	{
		get { return _type; }
		set
		{
			spriteRender.color = Tropism.DefaultColor(value);
			_type = value;
		}
	}

	#endregion Type

	#region MeshRender

	public SpriteRenderer spriteRender { get { return gameObject.GetComponentInChildren<SpriteRenderer>(); } }

	#endregion MeshRender

	#endregion Variables
}
