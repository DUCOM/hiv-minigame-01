﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           TCell_Pair.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.10 (now calls Tropism.Random() without any extra variables.)
 *  Last Modified:  2 February 2017
 */

/// <summary> A pair of receptor and coreceptor </summary>
public class TCell_Pair : MonoBehaviour
{

	#region Variables

	#region Components

	/// <summary> Gets the receptor of the pair. </summary>
	[SerializeField] TCell_Receptor receptor;

	/// <summary> Gets the coreceptor of the pair. </summary>
	[SerializeField] TCell_Coreceptor coReceptor;

	/// <summary> Encapsulated. gets the pivot of the Pair. Get only </summary>
	[SerializeField] GameObject pivot;

	#endregion Components

	#region tropism

	private Tropism tropism_;

	/// <summary>
	/// Encapsulated. the tropism of the pair. Get only
	/// </summary>
	public Tropism tropism
	{
		get { return tropism_; }
		set
		{
			receptor.gameObject.SetActive(true);
			coReceptor.gameObject.SetActive(true);
			receptor.type = value;
			coReceptor.type = value.coreceptor;
			tropism_ = value;
		}
	}

	#endregion tropism



	#endregion Variables

	#region Methods

	#region Scale

	/// <summary> sets the scale for the pair to a value. 1 if original </summary>
	/// <param name="s">the scale value</param>
	public void Scale(float s)
	{
		if(s == 0) return;
		transform.localScale = new Vector3(s, s, s);
	}

	#endregion Scale

	#region Move

	/// <summary> translates the pivot </summary>
	/// <param name="position">a vector of how to move</param>
	public void MovePivot(Vector3 position)
	{
		pivot.transform.localPosition = position;
	}

	#region AndRotate

	/// <summary> moves to a position and rotates to a translation using polar coordinates </summary>
	/// <param name="radius">radius to rotate about</param>
	/// <param name="angle">angle to rotate</param>
	public void MovePivotAndRotate(float radius, Quaternion angle)
	{
		pivot.transform.localPosition = new Vector3(0, radius, 0);
		transform.rotation = angle;
	}

	/// <summary> moves pivot with the stuf in it to a position and rotates using a Vector </summary>
	/// <param name="translation">translation</param>
	/// <param name="rotation">rotation</param>
	public void MovePivotAndRotate(Vector3 translation, Quaternion rotation)
	{
		pivot.transform.localPosition = translation;
		pivot.transform.localRotation = rotation;
	}

	#endregion AndRotate

	#endregion Move

	#region Pivot Rotation

	/// <summary> Rotates around the pivot and not the entire object </summary>
	/// <param name="eulerAngles">The angles in degrees you wish to rotate around.</param>
	public void PivotRotation(Vector3 eulerAngles)
	{
		pivot.transform.rotation = new Quaternion();
		pivot.transform.Rotate(eulerAngles);
	}

	#endregion Pivot Rotation

	public void SetLayer(string s)
	{
		receptor.gameObject.layer = LayerMask.NameToLayer(s);
		coReceptor.gameObject.layer = LayerMask.NameToLayer(s);
		gameObject.layer = LayerMask.NameToLayer(s);
	}

	#endregion Methods
}

/// <summary> A list of TCell Pairs </summary>
[System.Serializable]
public class PairList
{

	#region Variables

	#region Properties

	/// <summary> The Pair prefab </summary>
	public GameObject prefab;

	/// <summary> The number of pairs wanted on the T-Cell </summary>
	[SerializeField]
	private uint _count;

	bool _instantiated = false;

	/// <summary> Gets the amount of instances </summary>
	/// <value>The count.</value>
	public int count
	{
		get {
			if(objectList == null) return 0;
			return objectList.Count;
		}
	}

	#endregion

	#region Object Lists

	/// <summary> The list of instances in the pairlist </summary>
	[System.NonSerialized]
	public List<GameObject> objectList;

	/// <summary>
	/// The list of scripts in the pairList
	/// </summary>
	/// <param name="i">the intended index</param>
	/// <returns>The TCell_Pair instance at that index</returns>
	public TCell_Pair this[int i] { get { return (objectList == null || i >= count) ? null : objectList[i].GetComponent<TCell_Pair>(); } }

	#endregion Object Lists

	#endregion Properties

	#region Methods

	/// <summary> Changes the tropism to a randm Tropism for each one </summary>
	public void ChangeTropism( )
	{
		for(int i = 0; i < count; ++i)
		{
			this[i].tropism = Tropism.Random();
		}
	}

	#region Instantiate

	/// <summary> Instantiates the pairlist with a parent and a radius </summary>
	/// <param name="parent">the parent of the object list. Null if none</param>
	/// <param name="radius">the radius of the object list.</param>
	public void Instantiate(Transform parent, float radius)
	{
		if(_instantiated) return;

		if(objectList == null) objectList = new List<GameObject>();
        
		for(var i = 0; i < _count; ++i)
		{
			if(objectList.Count >= i)
			{
				GameObject a = Object.Instantiate(prefab);
				objectList.Add(a);

				var s = this[count - 1];

				if(s != null)
				{
					if(parent != null) s.transform.SetParent(parent);
					s.transform.localPosition = new Vector3();

					float r = Random.Range(-(float)90.0f / (float)_count, (float)90.0f / (float)_count);
					//TODO: ADD RANDOM ELEMENT
					float angle = i / (float)_count * (float)360.0 + r;

					s.MovePivotAndRotate(radius, Quaternion.Euler(0, 0, angle));
					if(radius != 0) s.transform.localScale = new Vector3(10 / radius, 10 / radius, 10 / radius);
					s.tropism = Tropism.Random();
				}
				else throw new System.FieldAccessException();
			}
			else
			{
				if(!objectList[i].activeSelf)
				{
					objectList[i].SetActive(true);
					Debug.Log(objectList[i].transform.parent);
				}
			}
		}
		_instantiated = true;
	}

	/// <summary> Instantiates the pairlist with a parent and a radius </summary>
	/// <param name="parent">the parent of the object list. Null if none</param>
	/// <param name="radius">the radius of the object list.</param>
	/// <param name="c">the number of pairs to generate for the pairlist</param>
	public void Instantiate(Transform parent, float radius, uint c)
	{
		_count = c;
		Instantiate(parent, radius);
	}

	#endregion Instantiate

	/// <summary> changes the scaling on the pairs. Does not change size </summary>
	/// <param name="a">the scale multiplier. Does nothing at 0</param>
	public void Scale(float a)
	{
		if(a == 0) return;
		for(var i = 0; i < count; ++i) this[i].Scale(a);
	}

	/// <summary> Disable the list of instances. </summary>
	public void Disable( )
	{
		foreach(var i in objectList) i.SetActive(false);
	}

	/// <summary> Re-enables the list of instances. </summary>
	public void Enable(TCell parent = null)
	{
		foreach(var i in objectList)
		{
			i.SetActive(true);
			i.transform.SetParent(parent.transform);
		}

	}

	#endregion Methods
}