﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           TCell_Receptor.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.1 (Receptor color no longer is based on the coreceptor color. Changes receptor sprite based on whether or not the receptor is CD4 positive.)
 *  Last Modified:  25 January 2017
 */

/// <summary> The Receptor of the T-Cell </summary>
public class TCell_Receptor : MonoBehaviour
{

	#region Variables

	#region Properties


	private Tropism _type;

	/// <summary> Encapsulated. Gets the type of the Receptor. </summary>
	public Tropism type
	{
		get { return _type; }
		set
		{
			head.color = Tropism.DefaultColor(value.receptor);
			head.ChangeSprite(value.receptor == Tropism.ReceptorType.CD4_POSITIVE);
			_type = value;
		}
	}

	#endregion Properties

	#region Components

	public TCell_Receptor_Head head { get { return gameObject.GetComponentInChildren<TCell_Receptor_Head>(); } }

	public TCell_Receptor_Stem stem { get { return gameObject.GetComponentInChildren<TCell_Receptor_Stem>(); } }

	public TCell cell
	{
		get {
			//TODO: change the way transform.parent.parent.parent works XD
			var t = transform.parent.parent.parent.GetComponent<TCell>();
			if(t == null) throw new System.NullReferenceException();
			return t;
		}

	}

	#endregion Components

	#endregion Variables

}
