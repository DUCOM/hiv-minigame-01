﻿using UnityEngine;

/*
 *  Name:           TCell_Receptor_Head.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.0 (Now takes in spriterenderers for the front and back of the head. Now is able to change the sprite of the head with a single boolean)
 *  Last Modified:  25 January 2017
 */

/// <summary> The TCell Receptor's head. Only has color </summary>
public class TCell_Receptor_Head : MonoBehaviour
{

	#region Variables

	[Header("Sprites")]
	[SerializeField] Sprite CD4PositiveFront;
	[SerializeField] Sprite CD4PositiveBack;
	[SerializeField] Sprite CD4NegativeFront;
	[SerializeField] Sprite CD4NegativeBack;

	[Header("GameObjects")]
	[SerializeField] GameObject HeadDisplayFront;
	[SerializeField] GameObject HeadDisplayBack;

	/// <summary> Encapsulated. The color of the Head. </summary>
	/// <value>The color of the head.</value>
	public Color color
	{ 
		get { return gameObject.GetComponentInChildren<SpriteRenderer>().color; } 
		set { gameObject.GetComponentInChildren<SpriteRenderer>().color = value; } 
	}

	#endregion

	#region Methods

	/// <summary> Changes the spite of the objects to whether or not the object is CD4 Positive. </summary>
	/// <param name="cd4">If set to <c>true</c> set to a CD4 sprite.</param>
	public void ChangeSprite(bool cd4)
	{
		HeadDisplayFront.GetComponent<SpriteRenderer>().sprite = cd4 ? CD4PositiveFront : CD4NegativeFront;
		HeadDisplayBack.GetComponent<SpriteRenderer>().sprite = cd4 ? CD4PositiveBack : CD4NegativeBack;
	}

	#endregion
}
