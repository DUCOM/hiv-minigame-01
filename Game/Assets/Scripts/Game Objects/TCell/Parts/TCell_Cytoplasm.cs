﻿/*
 *  Name:           TCell_Cytoplasm.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1
 *  Last Modified:  14 October 2016
 */

/// <summary>
/// the cytoplasm of the TCell. Nothing for now.
/// </summary>
public class TCell_Cytoplasm : TCell_Piece
{

}
