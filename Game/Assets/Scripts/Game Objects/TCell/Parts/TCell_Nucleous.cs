﻿/*
 *  Name:           TCell_Nucleous.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1
 *  Last Modified:  14 October 2016
 */

/// <summary>
/// the nucleous of the TCell. Nothing for now.
/// </summary>
public class TCell_Nucleous : TCell_Piece
{
}
