﻿using UnityEngine;

/*
 *  Name:           TCell.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.8 (TCell game object now goes based on the main static Game class instance. Got rid of Game game) 
 *  Last Modified:  20 January 2017
 */


/// <summary> The Base TCell Class </summary>
[RequireComponent(typeof(CellMovement))]
public class TCell : MonoBehaviour
{

	#region Constants

	const float DEFAULT_RADIUS = 4f;

	#endregion Constants

	#region Variables

	#region properties

	[Header("Properties")]

	[SerializeField] private float _radius;

	/// <summary> Encapsulated. Gets the the radius of the TCell </summary>
	public float radius
	{
		get { return _radius; }
		set
		{
			float scale = value / DEFAULT_RADIUS;
			transform.localScale = new Vector3(scale, scale, scale);

			if(cytoplasm.instance != null) cytoplasm.instance.transform.localScale = Vector3.one; //resets the scale for the cytoplasm
			if(membrane.instance != null) membrane.instance.transform.localScale = Vector3.one; //resets the scale for the membrane
			if(nucleous.instance != null) nucleous.instance.transform.localScale = Vector3.one; //resets the scale for the nucleous
			
			pairList.Scale(1 / scale);
			for(var i = 0; i < pairList.count; ++i)
			{
				float angle = i / (float)pairList.count * 360f;
				pairList[i].MovePivotAndRotate(value, Quaternion.Euler(0, 0, angle));
			}
			_radius = value;
		}
	}

	[SerializeField] private bool instantiateOnStart;


	private bool _infected = false;

	/// <summary> Encapsulated. Checks whether or not the TCell is infected. Changes things accordingly. </summary>
	public bool infected
	{
		get {
			return _infected;
		}
		set
		{
			if(value)
			{
				//gameObject.layer = LayerMask.NameToLayer("None"); //lose all collision with HIV and other pieces
				Color lightGray = new Color(0.3f, 0.3f, 0.3f);
				Color tLightGray = new Color(lightGray.r, lightGray.g, lightGray.b, 0.5f); //transparent version of light gray
				ChangeType(tLightGray, lightGray, lightGray);
				pairList.Disable();
				Game.main.CreateTCell(Quaternion.identity);
			}
			else
			{
				//gameObject.layer = LayerMask.NameToLayer("TCell"); //gains default collision as intended
				pairList.Enable(this);
			}
		}

	}

	#endregion properties

	#region parts

	[Header("TCell Parts")]
	[SerializeField] private TCell_Part cytoplasm;
	[SerializeField] private TCell_Part membrane;
	[SerializeField] private TCell_Part nucleous;
	[SerializeField] private PairList pairList;

	#endregion parts

	#region Components

	/// <summary> Gets the movement component of the TCell </summary>
	/// <value>The movement comoponent.</value>
	public CellMovement move { get { return gameObject.GetComponent<CellMovement>(); } }

	#endregion

	#endregion Variables

	#region Methods

	#region Functions

	/// <summary> Changes the type of the TCell </summary>
	/// <param name="cytoplasmColor">Changes the color of the cytoplasm</param>
	/// <param name="membraneColor">changes the color of the membrane</param>
	/// <param name="nucleousColor">changes the color of the nucleous</param>
	public void ChangeType(Color cytoplasmColor, Color membraneColor, Color nucleousColor)
	{
		cytoplasm.color = cytoplasmColor;
		membrane.color = membraneColor;
		nucleous.color = nucleousColor;
		pairList.ChangeTropism();
	}


	#region Instantiate

	/// <summary> Instantiates the TCell with default colors. </summary>
	/// <param name="r">the intended radius. 0 if default.</param>
	public void Instantiate(float r = 0f)
	{
		if(r < 0f) throw new System.ArgumentException("Radius cannot be negative.");
		else if(r == 0f) r = DEFAULT_RADIUS;

		cytoplasm.Instantiate(gameObject.transform);
		membrane.Instantiate(gameObject.transform);
		nucleous.Instantiate(gameObject.transform, new Vector3(0, 0, 1));
		pairList.Instantiate(gameObject.transform, r);
		radius = r;
	}

	/// <summary> Instantiates the TCell with a given set of colors </summary>
	/// <param name="cytoplasmColor">The color of the cytoplasm</param>
	/// <param name="membraneColor">The color of the membrane</param>
	/// <param name="nucleousColor">The color of the nucleous</param>
	/// <param name="r">The intended radius. 0f or none if default</param> 
	public void Instantiate(Color cytoplasmColor, Color membraneColor, Color nucleousColor, float r = 0f)
	{
		if(r < 0f) throw new System.ArgumentException("Radius cannot be negative.");
		else if(r == 0f) r = DEFAULT_RADIUS;

		cytoplasm.Instantiate(gameObject.transform, cytoplasmColor);
		membrane.Instantiate(gameObject.transform, membraneColor);
		nucleous.Instantiate(gameObject.transform, nucleousColor);
		pairList.Instantiate(transform, radius);
		radius = r;
	}

	#endregion Instantiate

	#endregion Funcions

	#region Monodevelop

	void Start( )
	{
		if(instantiateOnStart) Instantiate(radius);
		if(!Game.main) enabled = false;
	}

	float _timer = 0f;
	float _rotate;

	void FixedUpdate( )
	{
		if(_timer <= 0f)
		{
			_rotate = Random.Range(-0.5f, 0.5f);
			_timer = 2f;
		}
		transform.Rotate(0, 0, _rotate);
		_timer -= Time.fixedDeltaTime;
	}

	void LateUpdate( )
	{
		if(Functions.OutsideRect(transform.position, Functions.CameraToWorldRect(Camera.main, Game.main._tCellDisable)))
		{
			Game.main.CreateTCell(Quaternion.identity);
			gameObject.SetActive(false);
		}
	}

	#endregion Monodevelop

	#endregion methods
}
