﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AntibodyLatch.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Kills the Antibody when the amount of shakes has reached the maximum)
 *  Last Modified:  22 February 2016
 */

/// <summary>
/// Antibody Latch State
/// 	- Latches onto the HIV
/// 	- Dies and changes to the Die State wen the Kill is called
/// 	- Change to the Wander state when the HIV dies
/// </summary>
public class AntibodyLatch : AntibodyState
{
	#region Variables

	/// <summary> Keeps track of the old movement speed of the antivirus before giving it back. </summary>
	private float oldMax;

	/// <summary> The original distance between the HIV and the antibody. </summary>
	private Vector2 displacement;

	[SerializeField] float maxTime;
	float _timer;

	/// <summary> Kills the antibody. </summary>
	[SerializeField]
	private bool death;

	[SerializeField]
	private int maxShakeNumber;

	#endregion Variables

	#region Methods

	/// <summary> Kill the Antibody. </summary>
	public void Kill( )
	{
		death = true;
	}

	#region Monodevelop

	private void OnEnable( )
	{
		oldMax = main.move.maxSpeed;
		main.move.ChangeFollowState(null);
		if(target)
		{
			main.move.maxSpeed = target.move.maxSpeed;
			displacement = target.transform.position - transform.position;
			_timer = maxTime;
		}
	}

	private void OnDisable( )
	{
		main.move.maxSpeed = oldMax;
		if(target) target.GetComponent<HIV_Weak>().enabled = false;
		death = false;
	}

	// Update is called once per frame
	void Update( )
	{
		if(target)
		{
			transform.position = target.transform.position - (Vector3)displacement; //keeps up with the infected HIV
			_timer -= Time.deltaTime;
			if(_timer < 0f) death = true;
		}
		else main.ChangeState(Antibody.State.WANDER); //go back to wandering

		if(target.GetComponent<HIV_Weak>().shakeNumber >= maxShakeNumber || death) main.ChangeState(Antibody.State.DEATH, target);
	}

	#endregion Monodevelop

	#endregion Methods
}
