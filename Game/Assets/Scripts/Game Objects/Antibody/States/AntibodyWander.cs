﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AntibodyWander.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.3 (Added an additional condition for the Antibody to latch onto a virion)
 *  Last Modified:  23 March 2017
 */

/// <summary>
/// Wander state of an antibody
/// 	-continues to wander until something tells it that an HIV is around to attach to
/// </summary>
public class AntibodyWander : AntibodyState
{

	//TODO: Put latching mechanism onto here

	#region Methods

	float AngleToTarget(HIV t)
	{
		const float RAD_TO_DEG = 180f / Mathf.PI;
		Vector2 displacement = t.transform.position - transform.position;
		if(displacement.magnitude == 0) return 0;
		return t.transform.position.x > transform.position.x ? Mathf.Asin(displacement.y / displacement.magnitude) * RAD_TO_DEG : -Mathf.Asin(displacement.y / displacement.magnitude) * RAD_TO_DEG + 180f;
	}

	void OnEnable( )
	{
		target = null;
	}

	void OnDisable( )
	{
		target = null;
	}

	//TODO: Move this over to the wander state; let the wander state just move straight to the latch state.
	/// <summary> Latch the antibody to the intended HIV. </summary>
	void Latch(SpikeInteract destination)
	{
		if(destination.hiv.gameObject.activeSelf)
		{
			var spike = destination.spike;
			int spikeNumber = destination.hiv.GetSpikeNumber(spike);

			if(spikeNumber >= 0)
			{
				transform.position = (spike.gp41.transform.position);
				HIV_Weak weaken = destination.hiv.GetComponent<HIV_Weak>();
				weaken.ChangeExcludedIndex(spikeNumber);
				weaken.enabled = true;
				weaken.antibody = main;
				main.ChangeState(Antibody.State.LATCH, destination.hiv);

				transform.rotation = Quaternion.Euler(0f, 0f, AngleToTarget(destination.hiv));
			}
		}
		else main.ChangeState(Antibody.State.WANDER);
	}

	// Update is called once per frame
	void LateUpdate( )
	{
		if(target) main.ChangeState(Antibody.State.ATTACK, target);
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if(isActiveAndEnabled)
		{
			if(c.tag == "Spike")
			{
				SpikeInteract s = c.gameObject.GetComponent<SpikeInteract>();
				if(s && s.hiv.grouped && (s.hiv.state == HIV.State.SELECTED || s.hiv.state == HIV.State.WANDER || s.hiv.state == HIV.State.RETURN) && !s.hiv.weak)
				{
					var a = s.hiv;
					Latch(s);
				}
			}

		}
	}

	#endregion Methods
}
