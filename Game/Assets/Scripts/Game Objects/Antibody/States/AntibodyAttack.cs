﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AntibodyState.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.4 (DEPRICATED. Not used anymore.)
 *  Last Modified:  15 February 2017
 */

/// <summary>
/// Antibody Attack state.
/// 	- run towards the HIV
/// 	- latches on and infects the HIV upon contact.
/// </summary>
public class AntibodyAttack : AntibodyState
{
	
	#region Variables

	/// <summary> The destination of the HIV </summary>
	Transform destination;

	#endregion

	#region Methods

	/// <summary> Find the angle betweeen the antibody and the HIV </summary>
	/// <returns>The angle between the antibody and the HIV.</returns>
	float AngleToTarget( )
	{
		const float RAD_TO_DEG = 180f / Mathf.PI;
		Vector2 displacement = target.transform.position - transform.position;
		if(displacement.magnitude == 0) return 0;
		return target.transform.position.x > transform.position.x ? Mathf.Asin(displacement.y / displacement.magnitude) * RAD_TO_DEG : -Mathf.Asin(displacement.y / displacement.magnitude) * RAD_TO_DEG + 180f;
	}

	//TODO: Move this over to the wander state; let the wander state just move straight to the latch state.
	/// <summary> Latch the antibody to the intended HIV. </summary>
	void Latch( )
	{
		if(target.gameObject.activeSelf)
		{
			var spike = destination.GetComponent<Pivot>() == null ? destination.GetComponent<Spike>() : destination.GetComponent<Pivot>().parent.GetComponent<Spike>();
			int spikeNumber = target.GetSpikeNumber(spike);

			if(spikeNumber >= 0)
			{
				transform.position = (spike.gp41.transform.position);
				HIV_Weak weaken = target.GetComponent<HIV_Weak>();
				weaken.ChangeExcludedIndex(spikeNumber);
				weaken.enabled = true;
				weaken.antibody = main;
				main.ChangeState(Antibody.State.LATCH, target);

				transform.rotation = Quaternion.Euler(0f, 0f, AngleToTarget());
			}
		}
		else main.ChangeState(Antibody.State.WANDER);
	}

	#region Monodevelop

	void OnEnable( )
	{
		if(target)
		{
			destination = target.GetSpike(Random.Range(0, 8)).GetComponent<Spike>().pivot.transform;
			main.move.ChangeFollowState(destination, 5);
		}
	}

	void OnDisable( )
	{
		destination = null;
		main.move.ChangeFollowState();
	}

	void Update( )
	{
		if(!target.gameObject.activeSelf) main.ChangeState(Antibody.State.WANDER);
		{
			const float rotateSpeed = 5f;
			var angle = AngleToTarget();

			var rotation = transform.rotation.eulerAngles.z;

			var rotate = angle > rotation ? Mathf.Min(rotation + rotateSpeed, angle) : Mathf.Max(rotation - rotateSpeed, angle);
			transform.rotation = Quaternion.Euler(0f, 0f, rotate);
		}
	}

	void OnTriggerExit2D(Collider2D c)
	{
		if(isActiveAndEnabled && destination)
		{
			if(c.tag == "Spike")
			{
				SpikeInteract s = c.gameObject.GetComponent<SpikeInteract>();
				if(s && (s.hiv.state == HIV.State.SELECTED || s.hiv.state == HIV.State.WANDER || s.hiv.state == HIV.State.RETURN) && !s.hiv.weak)
				{
					destination = s.transform;
					target = s.hiv;
					Latch(); //latch on to the target as soon as you can.
				}
			}
		}
	}

	#endregion

	#endregion Methods
}
