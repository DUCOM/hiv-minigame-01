﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AntibodyDeath.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  9 February 2017
 */


public class AntibodyDeath : AntibodyState
{
	[SerializeField] [Range(0f, 30f)] float rotationSpeed;

	void OnEnable( )
	{
		GetComponent<Rigidbody2D>().gravityScale = 3;
		main.move.ChangeFollowState(null);
		if(target) main.move.velocity = new Vector2(
				target.transform.position.x < transform.position.x ? 3f : -3f, 30f);
		else main.move.velocity = new Vector2(0, 30f);
	}

	void OnDisable( )
	{
		main.ChangeState(Antibody.State.WANDER);
		this.enabled = false;
		GetComponent<Rigidbody2D>().gravityScale = 0;
		transform.rotation = Quaternion.identity;
	}

	void Update( )
	{
		if(main.move.velocity.y < 0) GetComponent<Rigidbody2D>().gravityScale = 6;
		transform.Rotate(new Vector3(0f, 0f, rotationSpeed * 60f * Time.deltaTime));
	}
}
