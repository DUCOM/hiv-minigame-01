﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AntibodyState.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  8 February 2017
 */

/// <summary>
/// The base state of an antibody state. All states for the antibody will inherit from this.
/// </summary>
[RequireComponent(typeof(AntibodyState))]
public class AntibodyState : MonoBehaviour
{

	protected Antibody main;
	public HIV target;

	protected void Awake( )
	{
		main = GetComponent<Antibody>();
	}
}
