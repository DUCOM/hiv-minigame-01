﻿using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           Antibody.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.4 (Antibodies now go away when outside a certain range no matter what state they are in.)
 *  Last Modified:  15 February 2017
 */

/// <summary> Antibody class. Handles all states of the antibody and is the central hub of the object. </summary>
[RequireComponent(typeof(AntibodyMove))]
[RequireComponent(typeof(AntibodyState))]
public class Antibody : MonoBehaviour
{
	#region enumerations

	public enum State
	{
		NONE = -1,
		WANDER = 0,
		ATTACK = 1,
		LATCH = 2,
		DEATH
	}

	#endregion

	#region Variables

	#region Properties

	public State state { get; private set; }

	Dictionary<State, AntibodyState> stateList;

	[SerializeField] float speed;

	public HIV target
	{ 
		private get { return state == State.NONE ? null : stateList[state].target; } 
		set
		{
			if(state != State.NONE) stateList[state].target = value;
		}
	}

	#endregion Properties

	#region Components

	[SerializeField] AntibodyMove _move;

	public AntibodyMove move { get { return _move; } }


	SpriteRenderer _display;

	#endregion Components

	#endregion Variables

	#region Methods

	#region State Methods

	public void ChangeState(State s, HIV target = null)
	{
		if(state != State.NONE) stateList[state].enabled = false;


		state = s;

		
		if(s != State.NONE)
		{
			stateList[state].target = target;

			if(state != State.NONE) stateList[state].enabled = true;
		}
	}

	#endregion State Methods

	#region Monodevelop

	// Use this for initialization
	void Awake( )
	{
		move.maxSpeed = speed;

		stateList = new Dictionary<State, AntibodyState>() {
			{ State.WANDER, GetComponent<AntibodyWander>() },
			{ State.LATCH, GetComponent<AntibodyLatch>() },
			{ State.DEATH, GetComponent<AntibodyDeath>() }
		};
	}

	void Start( )
	{
		foreach(var i in stateList)
		{
			i.Value.enabled = false;
		}

		ChangeState(State.WANDER);
	}

	void Update( )
	{
		if(Functions.OutsideRect(transform.position, Functions.CameraToWorldRect(Camera.main, new Rect(-1f, -1f, 4f, 4f))))
		{
			gameObject.SetActive(false);
		}
	}

	void OnEnable( )
	{
		ChangeState(State.WANDER);
	}

	#endregion Monodevelop

	#endregion Methods
}
