﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_Developer_Stats.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> UI_Developer Stats. Displays all the statistics given onto text. </summary>
/// <remarks> Requires Text. </remarks>
[RequireComponent(typeof(Text))]
public class UI_Developer_Stats : MonoBehaviour
{
	#region Methods
	#region Monodevelop
	void OnGUI( )
	{
		Text text = GetComponent<Text>();
		text.text = "";
		text.text = text.text + FPSCounter() + '\n';
		text.text = text.text + GetPopulation() + '\n';
		text.text += GetTCellPopulation() + '\n';
		text.text += GetRBCPopulation();
	}
	#endregion Monodevelop

	string GetRBCPopulation( )
	{
		if(Game.main) return "Red Blood Cell Population: " + Game.main.redBloodCellList.Count.ToString();
		return "";
	}

	string GetTCellPopulation( )
	{
		if(Game.main) return "TCell Population: " + Game.main.tCellList.Count.ToString();
		return "";
	}

	string GetPopulation( )
	{
		if(Game.main) return "Population: " + Game.main.population;
		return "";
	}

	string FPSCounter( )
	{
		return "FPS: " + GetFPS().ToString();
	}

	float GetFPS( )
	{
		return Mathf.Round((1f / Time.unscaledDeltaTime) * 100f) / 100f;
	}
	#endregion Methods
}
