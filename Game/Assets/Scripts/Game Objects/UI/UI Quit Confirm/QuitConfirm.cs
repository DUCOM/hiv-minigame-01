﻿using UnityEngine;

/*
 *  Name:           QuitConfirm.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Added an Update where if you Press the back button, it deactivates.)
 *  Last Modified:  2 February 2017
 */

/// <summary> The Quit Confirm base class. to use at one's leisure </summary>
public class QuitConfirm : MonoBehaviour
{
	/// <summary> The list of buttons to activate once the game object disappears </summary>
	[System.NonSerialized] public UnityEngine.UI.Button[] buttonList;

	/// <summary> Deactivate this instance. </summary>
	public void Deactivate( )
	{
		foreach(var b in buttonList)
		{
			b.interactable = true;
		}
		gameObject.SetActive(false);
	}

	void Update( )
	{
		if(Controller.main.GetBack()) Deactivate();
	}
}
