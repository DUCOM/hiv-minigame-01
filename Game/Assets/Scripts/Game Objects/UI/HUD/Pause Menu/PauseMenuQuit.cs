﻿using UnityEngine;

/*
 *  Name:           PauseMenuQuit.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> Pause Menu Quit class. Basically is only a class that activates a Quit Confirm class whenever it's called. </summary>
public class PauseMenuQuit : MonoBehaviour
{

	[SerializeField] UnityEngine.UI.Button[] activationList;

	public void QuitConfirm(GameObject quitConfirm)
	{
		Debug.Log("Checking for Quit");
		foreach(var b in activationList)
		{
			b.interactable = false;
		}
		quitConfirm.SetActive(true);
		quitConfirm.GetComponent<QuitConfirm>().buttonList = activationList;
	}
}
