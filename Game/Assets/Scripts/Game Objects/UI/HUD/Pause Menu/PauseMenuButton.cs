﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/*
 *  Name:           PauseMenuButton.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  21 December 2016
 */

/// <summary> Pause menu button options. Handles all of the different button options that the user may need. </summary>
public class PauseMenuButton : MonoBehaviour
{
	
	/// <summary> Restarts the level. </summary>
	void RestartLevel( )
	{
		Time.timeScale = 1; //be able to start the game immediately after restarting the level
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	/// <summary> Restart the level after a delay. </summary>
	/// <param name="delay">The delay to restart in seconds.</param>
	public void Restart(float delay)
	{
		if(delay <= 0f) RestartLevel();
		else Invoke("RestartLevel", delay);
	}

	/// <summary> Quit the game when clicked. </summary>
	public void Quit( )
	{
		Debug.Log("Quit");
		Application.Quit();
	}

	/// <summary> Go to the beginning of the game (initialization/menu) </summary>
	public void ToMenu( )
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(0);
	}

}
