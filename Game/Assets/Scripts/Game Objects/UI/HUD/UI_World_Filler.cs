﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_World_Filler.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// UI World Filler class. Singleton design. Enables filling for other places. 
/// </summary>
public class UI_World_Filler : MonoBehaviour
{

	public static UI_World_Filler main;

	[SerializeField] Image _filler;

	void Awake( )
	{
		if(main == null)
		{
			main = this;
			gameObject.SetActive(false);
		}
		else if(main != this)
		{
			Destroy(gameObject);
		}
	}

	public void Fill(float f)
	{
		_filler.fillAmount = f;
	}
}
