﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_Population.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */


/// <summary> Resume button class. </summary>
[RequireComponent(typeof(Button))]
public class UI_Resume : MonoBehaviour
{
	[SerializeField] UI_Pause pause;

	public void OnEnable( )
	{
		GetComponent<Button>().interactable = Game.main ? Game.main.population > 0 : false;
	}

	public void Resume( )
	{
		pause.Pause();
	}
}
