﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *  Name:           UI_Score.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary> The UI element for score. </summary>
[RequireComponent(typeof(Text))]
public class UI_Score : MonoBehaviour
{
	[SerializeField]Text text;

	void OnGUI( )
	{
		if (text.text != Game.main.score.ToString ()) {
			text.text = Game.main.score.ToString ();
			text.fontSize = 100 - 10 * Mathf.Max (1, Mathf.RoundToInt (Mathf.Log10 (Game.main.score)));
		}
	}
}
