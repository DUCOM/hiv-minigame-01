﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           HIV.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.1 (Now has a prefix that it can display itself on)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// UI Level class. Gets a Prefix and displays it along with the level number
/// </summary>
public class UI_Level : MonoBehaviour {

	Text text;
	[SerializeField] string _prefix;

	void Awake()
	{
		text = GetComponent<Text> ();
	}

	// Update is called once per frame
	void OnGUI () {
		if (text && Game.main != null)
			text.text = _prefix + Game.main.level.ToString ();
	}
}
