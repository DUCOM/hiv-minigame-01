﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 *  Name:           UI_TryAgain.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.5A (Genericized pauseText and endGameText.)
 *  Last Modified:  22 January 2017
 */

/// <summary> The Try Again button object </summary>
public class UI_TryAgain : MonoBehaviour
{

	[SerializeField] Text text;
	[SerializeField] string pauseText;
	[SerializeField] string endGameText;

	void OnGUI( )
	{
		if(Game.main.population <= 0) text.text = endGameText;
		else text.text = pauseText;
	}
}
