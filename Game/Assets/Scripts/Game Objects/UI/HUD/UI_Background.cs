﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *  Name:           UI_Background.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2016
 */

/// <summary> the UI background object. Used to be an overlay when the screen is paused. </summary>
public class UI_Background : MonoBehaviour
{
	/// <summary> The image component of the object. </summary>
	/// <value>The image component.</value>
	Image image { get { return GetComponent<Image>(); } }

	// Use this for initialization
	void Start( )
	{
		image.enabled = false;
	}

	void OnGUI( )
	{
		image.enabled = Time.timeScale == 0f; //enable the image when the time scale is 0 (paused)
	}

}
