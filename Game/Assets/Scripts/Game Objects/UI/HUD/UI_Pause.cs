﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_Pause.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary>
/// The Pause UI Game Object.
/// 	For use to pause or unpause the game.
/// </summary>
/// <remarks>Requires Button</remarks>
[RequireComponent(typeof(Button))]
public class UI_Pause : MonoBehaviour
{
	enum States
	{
		BEGIN,
		END
	}

	#region Variables

	#region Properties

	States state;
	[SerializeField]Sprite pauseSymbol;
	[SerializeField]Sprite playSymbol;

	[SerializeField] Image picture;

	#endregion Properties

	[SerializeField] GameObject pauseMenu;
	[SerializeField] AudioSource _backgroundMusic;

	#region Components

	public Controller controller { get; private set; }

	Button button { get { return gameObject.GetComponent<Button>(); } }

	new RectTransform transform { get { return gameObject.GetComponent<RectTransform>(); } }

	#endregion Components

	#endregion Variables

	#region Functions

	/// <summary>
	/// Toggle between paused and unpaused based on whether or not the time is being scaled.
	/// </summary>
	public void Pause( )
	{
		if(Time.timeScale == 0f)
		{
			Time.timeScale = 1f;
			picture.sprite = pauseSymbol;
			if (_backgroundMusic)
				_backgroundMusic.Play ();
			pauseMenu.SetActive(false);
		}
		else
		{
			Time.timeScale = 0f;
			picture.sprite = playSymbol;
			if (_backgroundMusic)
				_backgroundMusic.Pause ();
			pauseMenu.SetActive(true);
		}
	}

	#region Monobehavior

	void Start( )
	{
		controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>();
	}

	void Update( )
	{
		if(button.interactable && controller.GetBack()) Pause(); //Quit the application when going back.
		if(Game.main.population <= 0)
		{
			if(state == States.BEGIN)
			{
				Game.main.EndGame();
				pauseMenu.SetActive(true);
				button.interactable = false;
				state = States.END;
			}
		}
		else
		{
			if(state == States.END) state = States.BEGIN;
		}
	}

	#endregion

	#endregion
}
