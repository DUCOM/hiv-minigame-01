﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           UI_SpikeList.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.0 (Split the GP120 into two halves and added a spikecolor struct to keep track of everything.)
 *  Last Modified:  21 December 2016
 */

/// <summary> The Group of spikes in the UI HIV. </summary>
public class UI_SpikeList : MonoBehaviour
{

	private SpikeColor _colors;

	/// <summary>
	/// The pair of colors given to the Spikes.
	/// value 0 is for GP 120.
	/// value 1 is for GP 41.
	/// </summary>
	/// <value>The spike colors.</value>
	public SpikeColor colors
	{
		get { return _colors; }
		set
		{
			foreach(var i in spikeList)
			{
				i.gp41.color = value.gp41;
				i.gp120.colorLeft = value.gp120_left;
				i.gp120.colorRight = value.gp120_right;
			}
			_colors = value;
		}
	}

	[SerializeField] protected UI_Spike[] spikeList;
}


public struct SpikeColor
{
	public Color gp41;
	public Color gp120_left;
	public Color gp120_right;

	public SpikeColor(Color _gp41, Color _gp120_l, Color _gp120_r)
	{
		gp41 = _gp41;
		gp120_left = _gp120_l;
		gp120_right = _gp120_r;

	}
}