﻿using System.Collections;
using UnityEngine;

public class UI_HIV_Button : MonoBehaviour
{
	[SerializeField]protected Animator anim;

	[HideInInspector]public bool active = false;
	[HideInInspector]public bool ready = true;
	public static UI_HIV_Button main;

	void Awake()
	{
		main = this;
	}

	void Start()
	{
		Activate ();
	}


	public void Activate( )
	{
		if(ready)
		{
			anim.SetTrigger ("Activate");
		}
	}

	public void Deactivate()
	{
		anim.SetTrigger ("Deactivate");
	}
}
