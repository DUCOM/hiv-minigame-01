﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *  Name:           UI_GP120.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.0 (Added a left and right for the GP120 to refer to.)
 *  Last Modified:  1 February 2016
 */

public class UI_GP120 : MonoBehaviour
{

	[SerializeField]Image displayLeft;
	[SerializeField]Image displayRight;

	/// <summary> Gets or sets the color of the image. </summary>
	/// <value>The color of the image.</value>
	public Color color
	{
		get { return displayLeft.color; }
		set
		{
			displayLeft.color = value;
			displayRight.color = value;
		}
	}

	public Color colorLeft 
	{ 
		get {return displayLeft.color; } 
		set { displayLeft.color = value; }
	}
	public Color colorRight
	{
		get { return displayRight.color; }
		set { displayRight.color = value; }
	}
}
