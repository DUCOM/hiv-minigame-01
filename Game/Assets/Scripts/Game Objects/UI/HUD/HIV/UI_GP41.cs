﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *  Name:           UI_GP41.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Commented code.)
 *  Last Modified:  21 December 2016
 */

/// <summary> GP 41 of the UI HIV. Only has color for now. </summary>
public class UI_GP41 : MonoBehaviour
{

	/// <summary> Gets or sets the color of the image. </summary>
	/// <value>The color of the image.</value>
	public Color color
	{
		get { return GetComponent<Image>().color; }
		set
		{
			GetComponent<Image>().color = value;
		}
	}
}
