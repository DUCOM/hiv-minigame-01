﻿using UnityEngine;
using System.Collections;


/*
 *  Name:           UI_Spike.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  21 November 2016
 */

/// <summary> A single spike of the UI HIV. Consists of a GP120 and a GP41 </summary>
public class UI_Spike : MonoBehaviour
{

	public UI_GP120 gp120 { get { return gameObject.GetComponentInChildren<UI_GP120>(); } }

	public UI_GP41 gp41 { get { return gameObject.GetComponentInChildren<UI_GP41>(); } }
}
