﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *  Name:           UI_Bar.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1A (Changed the Update Function to OnGUI. Small readjustments)
 *  Last Modified:  11 January 2016
 */

public class UI_Bar : MonoBehaviour
{

	/// <summary>
	/// The display image of the bar
	/// </summary>
	/// <value>The image.</value>
	Image image { get { return gameObject.GetComponent<Image>(); } }

	/// <summary>
	/// Gets the controller.
	/// </summary>
	/// <value>The controller.</value>
	GameController controller { get { return Game.main.controller; } }

	HIV selected { get { return controller ? controller.selected : null; } }

	HIV _selected = null;

	void Update( )
	{
		if(selected && _selected != selected)
		{
			_selected = selected;
		}
		if(_selected)
		{
			if(!_selected.gameObject.activeSelf || _selected.state == HIV.State.INFECT)
			{
				_selected = null;
			}
		}
	}

	// Update is called once per frame
	void OnGUI( )
	{

		image.fillAmount = _selected ? _selected.stats.hp : 0f;
		image.color = GetColor();
	}

	/// <summary>
	/// Gets the color of the image.
	/// </summary>
	/// <returns>The color of the image.</returns>
	Color GetColor( )
	{
		float r, g, b = 0f;
		if(image.fillAmount > 0.5)
		{
			r = Mathf.Max(2 - 2 * image.fillAmount, 0f);
			g = 1;
		}
		else
		{
			r = 1;
			g = Mathf.Max(2 * image.fillAmount, 0f);
		}

		return new Color(r, g, b);
	}
}
