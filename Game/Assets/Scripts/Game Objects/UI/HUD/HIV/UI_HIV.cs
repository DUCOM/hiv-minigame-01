﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 *  Name:           UI_HIV.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.3 (Slight optimization fixes; Changed the controller to the main controller and whether or not an object is selected to that only. Now supports SpikeList)
 *  Last Modified:  1 February 2017
 */

/// <summary>
/// The UI's HIV display. It shows the selected HIV's pieces
/// </summary>
public class UI_HIV : MonoBehaviour
{
	#region Variables

	#region components

	HIV _selected = null;
	HIV selected { get { return Game.main.controller.selected; } }

	[Header("Components")]
	[SerializeField] Image capsid;
	[SerializeField] Image membrane;
	[SerializeField] Image matrix;
	[SerializeField] Image[] rna;
	[SerializeField] Image[] rt;

	[SerializeField] UI_SpikeList spikeList;

	static Color defaultColor = new Color(0.75f, 0.75f, 0.75f);

	#endregion components

	#endregion Variables

	#region Methods

	#region Monodevelop

	void Update()
	{
		if (selected && _selected != selected) {
			_selected = selected;
		}

		if(_selected)
		{
			if(!_selected.gameObject.activeSelf || _selected.state == HIV.State.INFECT)
			{
				_selected = null;
			}
		}

	}


	// Update is called once per frame
	void OnGUI( )
	{
		if(_selected)
		{
			capsid.color = _selected.capsidColor;
			membrane.color = _selected.membraneColor;
			matrix.color = _selected.matrixColor;
			foreach(var i in rna)
			{
				i.color = _selected.rnaColor;
			}
			foreach(var i in rt)
			{
				i.color = _selected.rtColor;
			}
			spikeList.colors = new SpikeColor (_selected.GP41_Color, _selected.GP120_Color_Left, _selected.GP120_Color_Right);
		}
		else
		{
			capsid.color = defaultColor;
			membrane.color = defaultColor;
			matrix.color = defaultColor;
			foreach(var i in rna)
			{
				i.color = defaultColor;
			}
			foreach(var i in rt)
			{
				i.color = defaultColor;
			}
			spikeList.colors = new SpikeColor(defaultColor, defaultColor, defaultColor);
		}
	}

	#endregion Monodevelop

	#endregion Methods
}
