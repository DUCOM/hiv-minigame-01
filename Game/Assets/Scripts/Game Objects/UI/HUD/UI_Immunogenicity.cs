﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_Immunogenicity.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  27 March 2017
 */

/// <summary>
/// UI_Immunogenicity class. Controls the bar to level up.
/// </summary>
/// <remarks>Requires Image component.</remarks>
[RequireComponent(typeof(Image))]
public class UI_Immunogenicity : MonoBehaviour
{
	#region enumerations
	enum States
	{
		FILL,
		FALL,
		RETURN,
		NONE
	}
	#endregion

	#region variables
	[SerializeField] Image barFiller;
	[Range(0.75f, 1f)] [SerializeField] float threshhold;
	[SerializeField] Animator anim;
	[SerializeField] float fallRate;
	[SerializeField] float fillRate;

	[SerializeField] float progress;
	[SerializeField] float maxProgress;


	States state;
	float fillIntended;

	/// <summary>
	/// Gets the amount that apporaches the next level.
	/// </summary>
	/// <value>The percentage.</value>
	public float percentage { get { return progress / maxProgress; } }

	/// <summary>
	/// The main bar. There can only be one at a time.
	/// </summary>
	public static UI_Immunogenicity main;
	#endregion variables

	#region Methods

	void ChangeState(States s)
	{
		switch(s)
		{
			case States.RETURN:
				fillIntended = 0f;
				break;
		}
		state = s;
	}

	/// <summary> Add to the immunogenicity bar by a desired amount. </summary>
	/// <param name="amount">the desired amount to add.</param>
	public void Add(float amount)
	{
		progress += amount;
		fillIntended = percentage;
		if(state != States.RETURN) ChangeState(States.FILL);
	}

	/// <summary>
	/// Refresh the maximum based on the game level.
	/// </summary>
	public void RefreshMaximum()
	{
		maxProgress = Game.main ? 0.395f * Mathf.Pow(1.23f, (float)(Game.main.level - 1) * 1.5f) : 1f;
	}

	/// <summary>
	/// Sets the progress bar back to zero.
	/// </summary>
	public void ResetProgress()
	{
		progress = 0f;
	}

	#region Monodevelop
	void Start( )
	{
		state = States.FALL;
		main = this;
		RefreshMaximum ();
	}

	void OnGUI( )
	{
		float fillAmount = barFiller.fillAmount;

		if(fillAmount >= threshhold)
		{
			barFiller.color = Color.red;
			anim.SetBool("blink", true);
		}
		else
		{
			anim.SetBool("blink", false);
			barFiller.color = Functions.GetColorPercentage((1f - barFiller.fillAmount) - (1f - threshhold));
		}
	}

	void Update( )
	{
		switch(state)
		{
			
			case States.FALL:
				if(barFiller.fillAmount <= 0f) ChangeState(States.NONE);
				else barFiller.fillAmount -= fallRate * Time.deltaTime * 60f;	
				break;

			case States.FILL:
				if(barFiller.fillAmount >= fillIntended) ChangeState(States.FALL);
				else if(barFiller.fillAmount == 1f)
				{
					Game.main.LevelUp ();
					ResetProgress ();
					ChangeState(States.RETURN);
					RefreshMaximum ();
				}
				else barFiller.fillAmount += fillRate * Time.deltaTime * 60f;
				break;

			case States.RETURN:
				if(barFiller.fillAmount <= 0 || barFiller.fillAmount <= fillIntended) ChangeState(States.FALL);
				else barFiller.fillAmount -= fillRate * Time.deltaTime * 60f;
				break;
			
			default:
				break;
		}
	}
	#endregion Monodevelop

	#endregion Methods
}
