﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           UI_Population.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary> The UI element for the population. </summary>
[RequireComponent(typeof(Text))]
[RequireComponent(typeof(Animator))]
public class UI_Population : MonoBehaviour
{
	#region Variables

	/// <summary> The text component of the Population </summary>
	Text text;

	/// <summary> The Animator component of the UI Population </summary>
	Animator anim;

	#endregion

	#region Functions

	#region Monobehavior

	void Awake( )
	{
		text = gameObject.GetComponent<Text>();
		anim = GetComponent<Animator>();
	}

	void OnGUI( )
	{
		text.text = "x" + Game.main.population.ToString();
		anim.SetBool("maximum", Game.main.population == 5);
		text.color = Functions.GetColorPercentage((float)Game.main.population / 5f);
	}

	#endregion

	#endregion
}
