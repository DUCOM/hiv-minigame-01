﻿using UnityEngine;

/*
 *  Name:           UI_Developer_Button.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>
/// UI_Developer_Button class. Simply swaps the stats on and off.
/// </summary>
public class UI_Developer_Button : MonoBehaviour
{
	[SerializeField] GameObject stats;

	public void Toggle( )
	{
		stats.SetActive(!stats.activeSelf);
	}
}
