﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/*
 *  Name:           MainMenuOption.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  9 December 2016
 */

/// <summary> Specific option for main menu button </summary>
public class MainMenuOption : MonoBehaviour
{
	/// <summary> The scene name to goto when clicked </summary>
	[SerializeField] string sceneName;

	/// <summary>  Goto requested scene when clicked. </summary>
	public void Goto( )
	{
		SceneManager.LoadScene(sceneName);
	}
}
