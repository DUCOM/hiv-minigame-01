﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           QuitButton.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Displays Quit in the debugger.)
 *  Last Modified:  21 December 2016
 */

[RequireComponent(typeof(AudioSource))]
/// <summary> Button to quit the application when clicked. </summary>
public class QuitButton : MonoBehaviour
{
	/// <summary> Quit the game when clicked. </summary>
	public void Quit( )
	{
		Debug.Log("Quit");
		Application.Quit();
	}

	/// <summary> Create an object that ensures that the user wants to quit </summary>
	/// <param name="quitConfirm">The GameObject to activate.</param>
	public void QuitConfirm(GameObject quitConfirm)
	{
		Debug.Log("Checking for Quit");

		quitConfirm.SetActive(true);
	}

	public void PlaySound()
	{
		AudioSource c = GetComponent<AudioSource> ();
		c.Play();
	}
}
