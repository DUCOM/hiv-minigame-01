﻿using UnityEngine;

/*
 *  Name:           BackQuitCheck.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation.)
 *  Last Modified:  2 February 2017
 */

[RequireComponent(typeof(QuitButton))]
public class BackQuitCheck : MonoBehaviour
{
	// Update is called once per frame
	void Update( )
	{
		if(Controller.main.GetBack()) GetComponent<QuitButton>().Quit();
	}
}
