﻿using UnityEngine;

/*
 *  Name:           BCellMaster.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation.)
 *  Last Modified:  17 February 2017
 */

public class BCellMaster : MonoBehaviour
{
	[SerializeField] Pivot pivot;
	[SerializeField] float radius;


	void OnValidate( )
	{
		pivot.MoveAbout(radius, transform.rotation);
	}

}
