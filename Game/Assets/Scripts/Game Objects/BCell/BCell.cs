﻿using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           BCell.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.2 (Took out all of the timers and made an OnTimerEnd class that shoots and vanishes)
 *  Last Modified:  17 February 2017
 */

public class BCell : MonoBehaviour
{

	[SerializeField] GameObject _shooter;
	[SerializeField] GameObject _master;
	List<AntibodyShooter> _shooters;

	[SerializeField] uint _number;

	[SerializeField] float _radius;

	[SerializeField] float _rotationSpeed;

	int currentIndex = 0;

	void OnTimerEnd(Timer t)
	{
		switch(t.name)
		{
			case "vanish":
				_master.SetActive(false);
				break;
			case "shoot":
				Shoot(currentIndex);
				currentIndex = (currentIndex + 1) % _shooters.Count;
				break;
		}
	}

	void Awake( )
	{
		transform.localScale = new Vector3(1f, 1f, _radius);
		_shooters = new List<AntibodyShooter>();
		float pivotAngle = 360f / _number;
		for(var i = 0; i < _number; ++i)
		{
			float angle = pivotAngle * i;
			GameObject a = Instantiate(_shooter, gameObject.transform) as GameObject;
			a.transform.localPosition = Vector3.zero;
			AntibodyShooter shot = a.GetComponent<AntibodyShooter>();
			_shooters.Add(shot);

			shot.pivot.MoveAbout(_radius, Quaternion.Euler(0f, 0f, angle));
		}
	}

	void Shoot(int index)
	{
		if(index < 0 || index >= _shooters.Count) throw new System.Exception(ErrorCode.EA0OR);

		var s = _shooters[index];

		s.Shoot();
	}

	void Update( )
	{
		transform.Rotate(0f, 0f, _rotationSpeed * 60f * Time.deltaTime);
	}
}