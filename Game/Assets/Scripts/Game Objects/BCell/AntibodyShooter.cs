﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Name:           AntibodyShooter.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation.)
 *  Last Modified:  16 February 2017
 */

public class AntibodyShooter : MonoBehaviour
{

	#region Variables

	[SerializeField] GameObject _antibody;
	[SerializeField] float _shotSpeed;
	[SerializeField] Pivot _pivot;
	List<GameObject> _bulletList;

	public Pivot pivot
	{
		get { return _pivot; }
	}

	#endregion Variables

	void Awake( )
	{
		_bulletList = new List<GameObject>();
	}

	void OnEnable( )
	{
		if(_bulletList == null) _bulletList = new List<GameObject>();
	}

	public Antibody Shoot( )
	{
		GameObject shot = null;
		foreach(var a in _bulletList)
		{
			if(!a.activeSelf)
			{
				a.transform.rotation = transform.rotation;
				a.transform.position = pivot.transform.position;
				a.SetActive(true);
				shot = a;
				break;
			}
		}
		if(shot == null)
		{
			shot = Instantiate(_antibody, pivot.transform.position, transform.rotation) as GameObject;
			_bulletList.Add(shot);
		}
		shot.GetComponent<Rigidbody2D>().velocity = (Vector2)transform.TransformDirection(Vector3.right) * _shotSpeed;
		return shot.GetComponent<Antibody> ();
	}
}
