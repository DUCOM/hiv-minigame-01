﻿using UnityEngine;

/*
 *  Name:           RedBloodCell.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:	29 March 2017
 */

/// <summary>
/// Red Blood Cell Class. Moves and disables itself if it gets too far from the screen.
/// </summary>
public class RedBloodCell : MonoBehaviour
{
	#region Constants

	/// <summary> The default radius of a red blood cell </summary>
	const float RADIUS_DEFAULT = 1.5f;

	#endregion

	#region Variables

	#region Components

	/// <summary> The movement script for the Red Blood Cell. </summary>
	public CellMovement move { get; private set; }

	#endregion

	#region radius

	/// <summary>
	/// The private radius of the red blood cell
	/// </summary>
	float _radius;

	/// <summary> Gets or sets the radius of the red blood cell. </summary>
	/// <value>The radius of the red blood cell.</value>
	public float radius
	{
		get {
			return _radius;
		}
		set
		{
			gameObject.transform.localScale = new Vector3(value / RADIUS_DEFAULT, value / RADIUS_DEFAULT, 1f);
			_radius = value;
		}
	}

	#endregion

	#region Outside Rect
	[SerializeField] Rect _disableRect;
	#endregion Outside Rect

	#endregion

	#region Functions

	#region Monodevelop

	void Awake( )
	{
		move = GetComponent<CellMovement>();
	}

	void Start( )
	{
		if(Game.main == null) enabled = false; //disable itself when there's no main game.
	}
	

	void LateUpdate( )
	{
		if(Functions.OutsideRect(transform.position, Functions.CameraToWorldRect(Camera.main, _disableRect)))
		{
			gameObject.SetActive(false);
		}
	}

	#endregion

	#endregion
}
