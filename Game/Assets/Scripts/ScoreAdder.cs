﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           ScoreAdder.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>
/// ScoreAdder class. Updates the score list based on the Input Field. <para>Requires InputField.</para>
/// </summary>
[RequireComponent(typeof(InputField))]
public class ScoreAdder : MonoBehaviour
{

	InputField input;

	void Awake( )
	{
		input = GetComponent<InputField>();
	}

	/// <summary>
	/// 
	/// </summary>
	public void UpdateScoreList( )
	{
		int x = 0;
		if(int.TryParse(input.text, out x))
		{
			Debug.Log(HighScores.main.Insert(new Score(x)));
		}
		else
		{
			Debug.Log("what?");
		}
	}

}
