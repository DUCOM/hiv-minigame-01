﻿using UnityEngine;

/*
 *  Name:           Movement_Basic.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>  Basic Movement Script. Use for Testing. <para>Requires Rigidbody2D.</para></summary>
[RequireComponent(typeof(Rigidbody2D))]
public class Movement_Basic : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The requested movement speed for the object
    /// </summary>
    [Range(1, 100)][SerializeField] float moveSpeed;

	/// <summary>
	/// The rigidbody of the object
	/// </summary>
	Rigidbody2D _rb;

	[SerializeField] bool _start;
	[SerializeField] Vector2 _startingVelocity;

    [Header("Key Codes")]
    [SerializeField] KeyCode up;
    [SerializeField] KeyCode down;
    [SerializeField] KeyCode left;
    [SerializeField] KeyCode right;
    #endregion

    #region Monodevelop
    // Use this for initialization
    void Awake( )
	{
		_rb = GetComponent<Rigidbody2D>();
		if(!_rb) enabled = false;
	}

	void Start()
	{
		if (_start)
			_rb.velocity = _startingVelocity; //start moving at the given velocity if start is enabled.
	}

	// Update is called once per frame
	void Update( )
	{
		Vector2 direction = _rb.velocity; //find the direction
		if(Input.GetKey(up)) direction.y = 1f; //go up if the up arrow is pressed
		else if(Input.GetKey(down)) direction.y = -1f; //go down if the down arrow is pressed

		if (Input.GetKey (left))
			direction.x = -1f;
		else if (Input.GetKey (right))
			direction.x = 1;
		
		_rb.velocity = direction.normalized * moveSpeed;
	}
    #endregion Monodevelop
}
