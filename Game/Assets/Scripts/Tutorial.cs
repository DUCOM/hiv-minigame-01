﻿using UnityEngine;
using UnityEngine.SceneManagement;

/*
 *  Name:           Tutorial.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>
///		Tutorial Class. Swaps between frames using next and previous. Can start game or load the game scene. 
///		<para>Requires Animator.</para>
/// </summary>
[RequireComponent(typeof(Animator))]
public class Tutorial : MonoBehaviour
{
	#region Variables
	Animator anim;

	[SerializeField]
	string gameSceneName;

	[SerializeField]bool next, prev, main;
	#endregion

	#region Methods

	#region Monodevelop
	void Awake( )
	{
		anim = GetComponent<Animator>();
	}

	void OnValidate( )
	{
		if(next)
		{
			NextFrame();
		}
		else if(prev)
		{
			PreviousFrame();
		}
		else if(main)
		{
			ToMainMenu();
		}
	}
	#endregion

	/// <summary> Changes to the next frame. </summary>
	public void NextFrame( )
	{
		next = false;
		anim.SetTrigger("next");
	}

	/// <summary> Changes to the previous frame. </summary>
	public void PreviousFrame( )
	{
		prev = false;
		anim.SetTrigger("previous");
	}

	/// <summary> Changes to Main Menu. </summary>
	public void ToMainMenu( )
	{
		main = false;
		SceneManager.LoadScene("main_menu");
	}

	/// <summary> Starts the game. </summary>
	public void ToGame( )
	{
		SceneManager.LoadScene(gameSceneName);
	}
	#endregion
}
