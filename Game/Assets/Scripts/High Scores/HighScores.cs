﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/*
 *  Name:           HighScores.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> Highscores Class. Singleton design. Gets, loads, and saves the top 10 scores given to it. </summary>
[System.Serializable]
public class HighScores
{
	const uint MAX_LIST = 10; //the maximum number of highscores in a list. Change it if you want more or less than 10.

	static string _filePath = Application.persistentDataPath + "/highScores.dat";

	ScoreList _highScores;
	
	/// <summary>
	/// The sorted list of high scores. Get only.
	/// </summary>
	public ScoreList scores { get { return _highScores; } }

	/// <summary> The main highscore class. </summary>
	public static HighScores main;

	#region Constructor

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="saveToMain"><c>true</c> if you want this to save to the main singleton.</param>
	/// <param name="overwrite"><c>true</c> if, given the main exists, you want this to overwrite it.</param>
	public HighScores(bool saveToMain = true, bool overwrite = false)
	{
		_highScores = new ScoreList();
		_highScores.highScores = new Score[MAX_LIST];
		if(saveToMain)
		{
			if(main == null || overwrite)
			{
				main = this;
			}
		}
	}

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="scoreList">A list of scores that you would like to place at HighScores.</param>
	/// <param name="saveToMain"><c>true</c> if you want this to save to the main singleton.</param>
	/// <param name="overwrite"><c>true</c> if, given the main exists, you want this to overwrite it.</param>
	public HighScores(Score[] scoreList, bool saveToMain = true, bool overwrite = false)
	{
		_highScores = new ScoreList();
		_highScores.highScores = new Score[MAX_LIST];
		for(var i = 0; i < Mathf.Min(scoreList.Length, MAX_LIST); ++i)
		{
			_highScores.highScores[i] = scoreList[i];
		}

		if(saveToMain)
		{
			if(main == null || overwrite)
			{
				main = this;
			}
		}
	}

	#endregion

	/// <summary> Insert the givent score to the High Score list. </summary>
	/// <param name="s">The score to give to the high score table.</param>
	/// <returns><code>-1</code> if the score is too low to rank. Otherwise, gives the location where the given score fits.</returns>
	public int Insert(Score s)
	{
		_highScores.Sort();
		if(_highScores.highScores[_highScores.highScores.Length - 1] < s)
		{
			_highScores.highScores[_highScores.highScores.Length - 1] = s;
			return _highScores.Sort(_highScores.highScores.Length - 1);
		}
		return -1;
	}

	/// <summary> Resets the High Score table to defaults. </summary>
	public void Reset( )
	{
		_highScores = new ScoreList();
		_highScores.highScores = new Score[MAX_LIST];
	}

	static HighScores Default
	{
		get {
			return new HighScores(false);
		}
	}

	public bool Save( )
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(_filePath, FileMode.OpenOrCreate);

		ScoreList data = new ScoreList();
		data.highScores = _highScores.highScores;

		bf.Serialize(file, data);
		file.Close();
		Debug.Log("Save Complete");
		return true;
	}

	public static bool ResetSave( )
	{
		HighScores h = Default;
		h.Save();
		Debug.Log("Reset Complete");
		return true;
	}

	public bool Load( )
	{
		if(File.Exists(_filePath))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(_filePath, FileMode.Open);
			ScoreList data = (ScoreList)bf.Deserialize(file);
			file.Close();

			_highScores.highScores = data.highScores;
			Debug.Log("Load Complete");
			return true;
		}
		else
		{
			Debug.Log("Load Failed. Could not find file path.");
			return false;
		}
	}
}

/// <summary>
/// ScoreList class. A list of highscores. Uses MergeSort to sort itself.
/// </summary>
[System.Serializable]
public class ScoreList
{
	public Score[] highScores;

	public override string ToString( )
	{
		string s = "";

		for(var i = 0; i < highScores.Length - 1; ++i)
		{
			s = s + highScores[i] + '\n';
		}

		s += highScores[highScores.Length - 1];

		return s;
	}

	public int Sort(int trackingIndex = 0)
	{
		return MergeSort(0, highScores.Length - 1, trackingIndex);
	}

	//implements Mergesort
	int MergeSort(int lo, int hi, int track = 0)
	{
		if(lo == hi) return track;
		bool change = lo <= track && hi >= track;

		int length = hi - lo + 1;
		int pivot = (lo + hi) / 2;
		track = MergeSort(lo, pivot, track);
		track = MergeSort(pivot + 1, hi, track);

		Score[] working = new Score[length];
		for(int i = 0; i < length; ++i) working[i] = highScores[lo + i];
		int m1 = 0;
		int m2 = pivot - lo + 1;

		int changeValue = track - lo;

		for(int i = 0; i < length; ++i)
		{
			if(m2 <= hi - lo)
			{
				if(m1 <= pivot - lo)
				{
					if(working[m1] < working[m2])
					{
						if(change && changeValue == m2)
						{
							changeValue = i;
						}
						highScores[i + lo] = working[m2++];
					}
					else
					{
						if(change && changeValue == m1)
						{
							changeValue = i;
						}
						highScores[i + lo] = working[m1++];
					}
				}
				else
				{
					if(change && changeValue == m2)
					{
						changeValue = i;
					}
					highScores[i + lo] = working[m2++];
				}
			}
			else
			{
				if(change && changeValue == m1)
				{
					changeValue = i;
				}
				highScores[i + lo] = working[m1++];
			}
		}
		if(change)
		{
			track = changeValue + lo;
		}
		return track;
	}
}

/// <summary> Score class. A single score with its own background information. </summary>
[System.Serializable]
public struct Score
{
	[SerializeField]
	public int score;

	public Score(int s = 0)
	{
		score = s;
	}

	/// <summary> Checks if two objects are equal </summary> 
	/// <param name="o"> the object intended to compare</param>
	/// <returns> false if not the same type or a different score. True if the same score. </returns>
	public override bool Equals(object o)
	{
		if(o.GetHashCode() != GetHashCode()) return false;

		Score s = (Score)o;

		return s.score == score;
	}

	/// <summary> Override of the hash code of the value type. For use of overriding </summary>
	/// <returns> The Hash codeof the Statistic type. </returns>
	public override int GetHashCode( )
	{
		return base.GetHashCode();
	}

	bool LessThan(Score s)
	{
		return score < s.score;
	}

	public static bool operator ==(Score a, Score b)
	{
		return a.Equals(b);
	}

	public static bool operator <(Score a, Score b)
	{
		return a.LessThan(b);
	}

	public static bool operator >(Score a, Score b)
	{
		return b.LessThan(a);
	}

	public static bool operator !=(Score a, Score b)
	{
		return !(a == b);
	}

	public static bool operator <=(Score a, Score b)
	{
		return a < b || a == b;
	}

	public static bool operator >=(Score a, Score b)
	{
		return a > b || a == b;
	}

	public override string ToString( )
	{
		return score.ToString();
	}
}