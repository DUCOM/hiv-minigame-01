﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           HighScoreDisplaySingle.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation)
 *  Last Modified:  20 February 2017
 */

public class HighScoreDisplaySingle : MonoBehaviour
{

	Text text;

	// Use this for initialization
	void Awake( )
	{
		if(HighScores.main == null) HighScores.main = new HighScores();
		HighScores.main.Load();

		text = GetComponent<Text>();
	}

	void OnGUI( )
	{
		text.text = "Hi Score : " + HighScores.main.scores.highScores[0].ToString();
	}
}
