﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           HighScoreDisplay.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release.)
 *  Last Modified:  27 March 2017
 */

/// <summary> High Score Display Class. Shows a list of high scores. Requires Text component. </summary>
[RequireComponent(typeof(Text))]
public class HighScoreDisplay : MonoBehaviour
{
	#region enumerations
	/// <summary>
	/// Whether to disply in a numbered list or a cenered one.
	/// </summary>
	enum DisplayState
	{
		DISPLAY,
		LIST
	}

	/// <summary>
	/// Whether to display the full list or just a part of it.
	/// </summary>
	enum ListingState
	{
		FULL,
		PART
	}
	#endregion enumerations

	#region variables
	Text text;
	[SerializeField] DisplayState display;
	[SerializeField] ListingState show;
	[SerializeField] [Range(1, 10)] int lowNumber; //the lower number
	[SerializeField] [Range(1, 10)] int highNumber; //the higher number
	[SerializeField] int spaces; //the amount of spaces between the number order and the score.
	#endregion variables

	#region Monodevelop
	void Awake( )
	{
		if(HighScores.main == null) HighScores.main = new HighScores();
		HighScores.main.Load();

		text = GetComponent<Text>();
	}

	void OnGUI( )
	{
		text.text = "";
		switch(display)
		{
		case DisplayState.DISPLAY:
			text.alignment = TextAnchor.UpperCenter;
			text.text = HighScores.main.scores.ToString();
			break;
		case DisplayState.LIST:
			int low = show == ListingState.FULL ? 1 : lowNumber;
			int high = show == ListingState.FULL ? HighScores.main.scores.highScores.Length : highNumber;
			for(var i = low - 1; i < high; ++i)
			{
				text.alignment = TextAnchor.MiddleLeft;
				text.text = text.text + (i + 1).ToString() + ".";
				for(var j = spaces; j > (i + 1).ToString().Length; --j) text.text += ' ';

				text.text = text.text + HighScores.main.scores.highScores[i] + '\n';
			}
			break;
		}
	}
	#endregion Monodevelop
}
