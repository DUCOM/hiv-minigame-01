﻿using UnityEngine;
using UnityEngine.SceneManagement;

/*
 *  Name:           SceneMove.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>
/// SceneMove class. Changes the scene upon start.
/// </summary>
public class SceneMove : MonoBehaviour {

	[SerializeField] string sceneName;

	// Use this for initialization
	void Start () {
		SceneManager.LoadScene (sceneName);
	}
}
