﻿using UnityEngine;

/*
 *  Name:           LayerBackground.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> LayerBackground class. Shows a background layer that moves based on its layer number. </summary>
public class LayerBackground : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// The render camera of the game
    /// </summary>
    [SerializeField] Camera render;

	/// <summary>
	/// The background picture for reference
	/// </summary>
	[SerializeField] Sprite picture;

	/// <summary>
	/// How far back the background is. 1 for furthest forward
	/// </summary>
	[Range(1, 3)] [SerializeField] int layer;
	/// <summary>
	/// The width of the render image
	/// </summary>
	float render_width;
	/// <summary>
	/// The height of the render image.
	/// </summary>
	float render_height;

	[SerializeField] Color _backgroundBlend;
	[SerializeField] SpriteRenderer[] _backgroundSprites;

	/// <summary>
	/// The camera's previous position
	/// </summary>
	Vector3 cameraPosPrev;
    // Use this for initialization

    #endregion

    #region Methods

    #region Monodevelop
    void Start( )
	{
		if(layer < 1) layer = 1;
		render_width = picture.rect.width / picture.pixelsPerUnit;
		render_height = picture.rect.height / picture.pixelsPerUnit;


		transform.localScale = new Vector3(render.orthographicSize / 5f, render.orthographicSize / 5f, 1f);
		render_width *= render.orthographicSize / 5f;
		render_height *= render.orthographicSize / 5f;

		cameraPosPrev = render.transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate( )
	{
		Move();
		//find the distance between te image and the camera
		float distX = transform.position.x - render.transform.position.x, distY = transform.position.y - render.transform.position.y; 

		//move the image if it's going too far
		if(distX < -render_width) transform.position += new Vector3(render_width, 0);
		else if(distX > render_width) transform.Translate(new Vector3(-render_width, 0));
	
		if(distY < -render_height) transform.Translate(new Vector3(0, render_height));
		else if(distY > render_height) transform.Translate(new Vector3(0, -render_height));
		//Debug.Log(distX.ToString() + ' ' + distY.ToString());
	}

	void OnValidate()
	{
		foreach (var i in _backgroundSprites) {
			if(i)
				i.color = _backgroundBlend;
		}
	}

    #endregion
    /// <summary>
    /// Move this instance based on its layer.
    /// </summary>
    void Move()
    {
        if (cameraPosPrev == render.transform.position) return;

        float distX = render.transform.position.x - cameraPosPrev.x;
        float distY = render.transform.position.y - cameraPosPrev.y;

        transform.position += new Vector3(distX - distX / layer, distY - distY / layer);

        cameraPosPrev = render.transform.position;
    }
    #endregion
}
