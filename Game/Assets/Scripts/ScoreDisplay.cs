﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  Name:           ScoreDisplay.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> 
///		ScoreDisplay class. Singleton design. Shows the given score. 
///		<para>Requires Animator.</para>
///	</summary>
[RequireComponent(typeof(Animator))]
public class ScoreDisplay : MonoBehaviour {

    #region Variables

    /// <summary> The main ScoreDisplay object. Singleton design. </summary>
    public static ScoreDisplay main;

	Animator _anim;
    #endregion

    #region Methods

    #region Monodevelop
    void Awake ()
	{
		_anim = GetComponent<Animator> ();
		main = this;
	}
    #endregion Monodevelop

    public void ShowScore(Vector2 position, int score, float scale = 1f, string previousText = "")
	{
		transform.position = position;
		transform.localScale = new Vector3 (scale, scale);
		GetComponentInChildren<Text> ().text = previousText + "+" + score.ToString ();
		GetComponentInChildren<Text> ().fontSize = (int)(15f * scale);
		_anim.SetTrigger ("Refresh");
	}
    #endregion Methods
}
