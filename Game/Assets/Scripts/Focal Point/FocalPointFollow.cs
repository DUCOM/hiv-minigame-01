﻿using UnityEngine;

/*
 *  Name:           FocalPointFollow.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1 (Creation.)
 *  Last Modified:  17 February 2017
 */

public class FocalPointFollow : MonoBehaviour
{

	void Update( )
	{
		if(FocalPoint.main)
		{
			var a = FocalPoint.main.move.velocity.x < 0 ? Vector2.Angle(Vector2.down, FocalPoint.main.move.velocity) : -Vector2.Angle(Vector2.up, FocalPoint.main.move.velocity);
			transform.rotation = Quaternion.Euler(0f, 0f, a);
		}
	}
}
