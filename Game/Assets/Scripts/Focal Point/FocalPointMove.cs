﻿using UnityEngine;

/*
 *  Name:           FocalPointMove.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> 
///		Movement Script of the focal point.
///		<para>Based on ObjectMovement2D</para>
///		<para>Requires Rigidbody2D</para>
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class FocalPointMove : ObjectMovement2D
{

}
