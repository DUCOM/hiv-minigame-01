﻿using UnityEngine;


/*
 *  Name:           FocalPoint.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.2 (doubled the speed of the focal point when it's going to follow state.)
 *  Last Modified:  13 February 2017
 */

/// <summary>
/// FocalPoint Class.
/// 	The main class of a focal point.
/// 	Handles all behavior of the Focal Point
/// </summary>
[RequireComponent(typeof(ObjectMovement2D))]
public class FocalPoint : MonoBehaviour
{
	enum State
	{
		NONE,
		WANDER,
		FOLLOW
	}

	#region Variables

	#region Components

	[Header("Components")]
	[SerializeField] FocalPointMove _move;

	/// <summary>
	/// The movement component of the focal point. Get only.
	/// </summary>
	public FocalPointMove move { get { return _move; } }

	#endregion Components

	#region Properties

	[Header("Properties")]
	[SerializeField] float _maxDistance;
	[SerializeField] float _additionalCutoff;
	[Tooltip("The speed to move the focal point")][SerializeField] float _speed;

	public float speed { get { return _speed; } }

	/// <summary> Gets the desired max distance </summary>
	/// <value>The maximum distance between the focal point and the virion.</value>
	public float maxDistance { get { return _maxDistance; } }

	public float distanceCutoff { get { return _maxDistance + _additionalCutoff; } }

	public float midpointCutoff { get { return (_maxDistance + distanceCutoff) / 2f; } }

	/// <summary> Gets the position. </summary>
	/// <value>The position.</value>
	public Vector3 position { get { return transform.position; } }

	State state;

	#endregion Properties

	#region Static Variables

	public static FocalPoint main;

	#endregion Static Variables

	#endregion variables

	#region Functions

	/// <summary> Calculates the distance between the given transform and the focal point  </summary>
	/// <returns>The distance between the focal point and the transform</returns>
	/// <param name="t">The given transform to compare to</param>
	public float GetDistance(Transform t)
	{
		return Vector2.Distance(t.position, position);
	}

	#region Monodevelop

	void Awake( )
	{
		if(main == null) main = this;
		else if(main != this) Destroy(gameObject);
	}

	void Start( )
	{
		if(!Game.main) enabled = false;
		state = State.WANDER;
		move.maxSpeed = speed;
	}

	void Update( )
	{
		switch(state)
		{
			case State.WANDER:
				if(Game.main.controller && Game.main.controller.selected)
				{
					move.ChangeFollowState(Game.main.controller.selected.transform);
					move.maxSpeed = Game.main.controller.selected.move.maxSpeed * 2;
					state = State.FOLLOW;
				}
				else
				{
					//what do you want to do while you wander?
					move.velocity = move.velocity.normalized * Mathf.Lerp(move.speed, speed, 1f);
				}
				break;
			case State.FOLLOW:
				if(!(Game.main.controller && Game.main.controller.selected))
				{
					move.ChangeFollowState(null, move.multiplier, move.teleportEnabled, true);
					move.maxSpeed = speed;
					state = State.WANDER;
				}
				else
				{
					move.velocity = Game.main.controller.selected.move.velocity;
					//what do you want to do while you're following?
				}
				break;
			case State.NONE:
				break;
		}
	}

	void OnDestroy( )
	{
		if(main == this) main = null;
	}

	#endregion

	#endregion Functions
}
