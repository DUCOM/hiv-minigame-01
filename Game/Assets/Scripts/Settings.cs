﻿using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/*
 *  Name:           Settings.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary> Settings class. Semi-singleton design. </summary>
public class Settings : MonoBehaviour {

    #region Variables
    [SerializeField] Slider VolumeSlider;

	static SettingList _settings;

    /// <summary> Get the the current setting list for viewing. Read Only. </summary>
    public static SettingList settings { get { return _settings; } }
    #endregion

    #region Methods

    #region Monodevelop
    public void OnEnable()
	{
		Load (); //load upon enabling
		VolumeSlider.value = _settings.volume;
	}

	public void OnDisable()
	{
		Save (); //save the settings upon disabling
	}
    #endregion Monodevelop

    /// <summary> Change the volume of the settings. Must be between 0 and 1. </summary>
    /// <param name="v">The desired volume. Must be between 0 and 1.</param>
    public void ChangeVolume(float v)
	{
		if (v > 1f)
            v = 1f;
		if (v < 0f)
            v = 0f;

		AudioListener.volume = v;
		_settings.volume = v;
	}

    /// <summary>
    /// Save the current settings
    /// </summary>
    /// <returns><c>true</c>if it saved justly.</returns>
	public static bool Save()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Open (SettingList.FilePath(), FileMode.OpenOrCreate);

		SettingList data = new SettingList ();
		data.volume = _settings.volume;

		bf.Serialize (file, data);
		file.Close ();
		Debug.Log ("Settings Saved!");
		return true;
	}

    /// <summary>
    /// Load settings and puts it on the singleton.
    /// </summary>
    /// <returns><c>true</c> if the load was successful. <c>false</c>if the file doesn't exist.</returns>
	public static bool Load()
	{
		if (File.Exists (SettingList.FilePath())) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (SettingList.FilePath(), FileMode.Open);
			SettingList data = (SettingList)bf.Deserialize (file);
			file.Close ();

			_settings.volume = data.volume;
			Refresh ();
			Debug.Log ("Settings Loaded.");
			return true;
		}

		Debug.Log ("Now using default values");
		_settings.SetDefault ();

		Refresh ();

		return false;
	}

    /// <summary>
    /// Refresh the output of the settings
    /// </summary>
	public static void Refresh()
	{
		AudioListener.volume = _settings.volume;
	}

    /// <summary> Reset the settings to the previous Load sequence </summary>
    public static void ResetToLoad()
    {
        Load();
    }
    #endregion Methods
}

[System.Serializable]
public struct SettingList
{
	public float volume;
    public float musicVolume; //TODO: for use later
    public float effectsVolume; //TODO: for use later

    /// <summary> gets the intended file path for the settings. </summary>
    /// <returns>The settings' file path</returns>
	public static string FilePath()
    {
        return Application.persistentDataPath + "/settings.dat";
    }

    public void SetDefault()
	{
		volume = 1f;
	}
}