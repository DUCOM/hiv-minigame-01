﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           SpriteDisplay.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

public class SpriteDisplay : MonoBehaviour
{
	#region Enumerators
	enum SpriteChange
	{
		UP,
		DOWN,
		NO_CHANGE
	}
	#endregion

	#region variables
	[SerializeField] protected SpriteRenderer spriteRender;
	[Tooltip("The list of sizes of the sprites from largest to smallest")][SerializeField] protected List<Sprite> spriteList;
	[SerializeField] protected int index = 0;

	protected int spriteCount { get { return spriteList.Count; } }

	protected int maxSprite { get { return spriteList.Count - 1; } }


	public bool up = false;
	public bool down = false;
	#endregion

	#region Monodevelop
	void Start( )
	{
		UpdateSprite();
		Scale(Mathf.Pow(2, index));
	}

	void UpdateSprite( )
	{
		if(index >= spriteCount) index = maxSprite;
		spriteRender.sprite = spriteList[index];
	}

	#if true //only use when debugging
	void Update( )
	{
		if(up)
		{
			UpdateSprite(SpriteChange.UP);
			up = false;
		}
		else if(down)
		{
			UpdateSprite(SpriteChange.DOWN);
			down = false;
		}
	}
	#endif
	#endregion

	void UpdateSprite(SpriteChange s)
	{
		if(s == SpriteChange.UP && index > 0)
		{
			index--;
			UpdateSprite();
			Scale(0.5f);
		}
		else if(s == SpriteChange.DOWN && index < maxSprite)
		{
			index++;
			UpdateSprite();
			Scale(2f);
		}
	}

	void Scale(float s)
	{
		transform.localScale = new Vector3(transform.localScale.x * s, transform.localScale.y * s, transform.localScale.z * s);
	}
}
