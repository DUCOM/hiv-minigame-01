﻿using UnityEngine;


/*
 *  Name:           Controller.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.5 (Added a static main controller. Will only have a single controller with it.)
 *  Last Modified:  1 February 2017
 */


public class Controller : MonoBehaviour
{
	public static Controller main;

	void Awake()
	{
		if (main == null) {
			main = this;
			DontDestroyOnLoad (gameObject);
		} else if (main != this) {
			Destroy (gameObject);
		}

	}

	#if UNITY_ANDROID || UNITY_IOS
	enum LeftState
	{
		NONE,
		TOUCH,
		DRAG,
		RELEASE
	}

	enum RightState
	{
		NONE,
		TOUCH,
		DRAG,
		RELEASE
	}

	LeftState l_state = LeftState.NONE;

	void LeftUpdate( )
	{
		switch(l_state)
		{
			case LeftState.NONE:
				if(Input.touchCount > 0) l_state = LeftState.TOUCH;
				break;
			case LeftState.TOUCH:
				if(Input.touchCount > 0) l_state = LeftState.DRAG;
				else if(Input.touchCount == 0 || Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) l_state = LeftState.RELEASE;
				break;
			case LeftState.DRAG:
				if(Input.touchCount == 0 || Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled) l_state = LeftState.RELEASE;
				break;
			case LeftState.RELEASE:
				if(Input.touchCount > 0) l_state = LeftState.TOUCH;
				else l_state = LeftState.NONE;
				break;
			default:
				l_state = LeftState.NONE;
				break;
		}
	}

	RightState r_state = RightState.NONE;

	void RightUpdate( )
	{
		switch(r_state)
		{
			case RightState.NONE:
				if(Input.touchCount > 1) r_state = RightState.TOUCH;
				break;
			case RightState.TOUCH:
				if(Input.touchCount > 1) r_state = RightState.DRAG;
				else if(Input.touchCount <= 1 || Input.GetTouch(1).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Canceled) r_state = RightState.RELEASE;
				break;
			case RightState.DRAG:
				if(Input.touchCount <= 1 || Input.GetTouch(1).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Canceled) r_state = RightState.RELEASE;
				break;
			case RightState.RELEASE:
				if(Input.touchCount > 1) r_state = RightState.TOUCH;
				else r_state = RightState.NONE;
				break;
			default:
				r_state = RightState.NONE;
				break;
		}
	}

	private Vector2 lastLocation = new Vector2(Screen.width / 2, Screen.height / 2);

	/// <summary> The position of the cursor in the world </summary>
	public Vector3 worldPosition { get { return RenderCamera.main.ScreenToWorldPoint(screenPosition); } }

	/// <summary> The position of the cursor on the screen </summary>
	public Vector3 screenPosition { get { return Input.touchCount > 0 ? Input.GetTouch(0).position : lastLocation; } }

	/// <summary>Finds the object that the cursor is pointing to. </summary>
	/// <returns>The Transform of the object that the cursor selected at a point</returns>
	public Transform getSelected( )
	{
		//var ray = RenderCamera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit2D hit = Physics2D.Raycast(worldPosition, Vector2.zero, 0f); //raycast information

		if(hit) return hit.transform;

		return null; //return none if nothing is hitting the cursor
	}

	public bool LeftClick( )
	{
		return l_state == LeftState.TOUCH;
	}

	public bool LeftDrag( )
	{
		return l_state == LeftState.DRAG;
	}

	public bool LeftRelease( )
	{
		return l_state == LeftState.RELEASE;
	}

	public bool RightClick( )
	{
		return r_state == RightState.TOUCH;
	}

	public bool RightDrag( )
	{
		return r_state == RightState.DRAG;
	}

	public bool RightRelease( )
	{
		return r_state == RightState.RELEASE;
	}

	/// <summary> Checks if the user is going back. </summary>
	/// <returns>true if the user presses the escape key</returns>
	public bool GetBack( )
	{
		return Input.GetKeyDown(KeyCode.Escape);
	}

	void Update( )
	{
		LeftUpdate();
		RightUpdate();
	}

	#else
	/// <summary> The position of the cursor in the world </summary>
	public Vector3 worldPosition { get { return RenderCamera.main.ScreenToWorldPoint(screenPosition); } }

	/// <summary> The position of the cursor on the screen </summary>
	public Vector3 screenPosition { get { return Input.mousePosition; } }

	/// <summary>Finds the object that the cursor is pointing to. </summary>
	/// <returns>The Transform of the object that the cursor selected at a point</returns>
	public Transform getSelected( )
	{

		RaycastHit2D hit = Physics2D.Raycast(RenderCamera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 0f); //raycast information

		if(hit)
		{
			return hit.transform;
		}
		return null; //return none if nothing is hitting the cursor
	}

	/// <summary> Checks left mouse was clicked </summary>
	/// <returns><c>true</c>, if user left clicked, <c>false</c> otherwise.</returns>
	public bool LeftClick( )
	{
		return Input.GetMouseButtonDown(0);
	}

	/// <summary> Checks left mouse is held </summary>
	/// <returns><c>true</c>, if user is holding the left mouse, <c>false</c> otherwise.</returns>
	public bool LeftDrag( )
	{
		return Input.GetMouseButton(0);
	}

	/// <summary> Checks left mouse was released </summary>
	/// <returns><c>true</c>, if user released the left mouse, <c>false</c> otherwise.</returns>
	public bool LeftRelease( )
	{
		return Input.GetMouseButtonUp(0);
	}


	/// <summary> Checks right mouse was clicked </summary>
	/// <returns><c>true</c>, if user clicked the right mouse, <c>false</c> otherwise.</returns>
	public bool RightClick( )
	{
		return Input.GetMouseButtonDown(1);
	}

	/// <summary> Checks right mouse was dragged </summary>
	/// <returns><c>true</c>, if user is dragging the right mouse, <c>false</c> otherwise.</returns>
	public bool RightDrag( )
	{
		return Input.GetMouseButton(1);
	}

	/// <summary> Checks right mouse was released </summary>
	/// <returns><c>true</c>, if user released the right mouse, <c>false</c> otherwise.</returns>
	public bool RightRelease( )
	{
		return Input.GetMouseButtonUp(1);
	}

	/// <summary> Checks if the user is going back. </summary>
	/// <returns>true if the user presses the escape key</returns>
	public bool GetBack( )
	{
		return Input.GetKeyDown(KeyCode.Escape);
	}
	#endif
}
