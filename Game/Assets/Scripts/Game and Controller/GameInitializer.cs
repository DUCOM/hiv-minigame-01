﻿using UnityEngine;

/*
 *  Name:           Gameinitializer.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  28 March 2017
 */

/// <summary>
/// Game Initializer Class.
/// 	<para>- Initializes The game at the beginning of the game</para>
/// 	<para>- changes depending on what device you're using</para>
/// </summary>
public class GameInitializer : MonoBehaviour
{
	#if UNITY_IOS || UNITY_ANDROID
	void Start () {
		Application.targetFrameRate = 60;
		HighScores.main = new HighScores();
		if (!HighScores.main.Load ())
			HighScores.main.Save ();
		Settings.Load();
	}



#else
	void Start( )
	{
		HighScores.main = new HighScores();
		HighScores.main.Load();
		Settings.Load ();
	}
	#endif
}
