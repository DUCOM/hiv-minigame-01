﻿using UnityEngine;
using System.Collections.Generic;

/*
 *  Name:           Game.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.1 (The B Cell timers now change itself based on level)
 *  Last Modified:  29 March 2017
 */

#region Game
/// <summary>
///		Game class. Singleton design. Used for the main bulk of handling things across the board.
///	</summary>
///	<remarks>Requires GameController, TimerList</remarks>
[RequireComponent(typeof(GameController))]
[RequireComponent(typeof(TimerList))]
public class Game : MonoBehaviour
{
	TimerList _timers;

	#region Variabls

	#region HIV

	/// <summary> The HIV base prefab </summary>
	[Header("HIV")]
	[SerializeField]
	private GameObject hiv;

	/// <summary> The intended radius of the HIV </summary>
	[SerializeField]
	private float hivRadius;
    
	/// <summary> The location to spawn the beginning HIV in the world </summary>
	[SerializeField]
	private Vector3 hivSpawn;
    
	/// <summary> the maximum population of the HIV </summary>
	[SerializeField]
	private int populationMax;

	/// <summary> The list of HIV game objects </summary>
	private List<GameObject> _hivList;

	/// <summary> Encapsulated. The list of HIV game objects. Get only. </summary>
	public List<GameObject> hivList { get { return _hivList; } }

	/// <summary> The list of HIV scripts. </summary>
	private List<HIV> _hivScripts;

	/// <summary> Encapsulated. The list of HIV scripts. Get only </summary>
	public List<HIV> hivScripts { get { return _hivScripts; } }

	public int population
	{ 
		get {
			if(hivList == null) return 0;

			int c = 0;
			foreach(var i in hivList)
			{
				if(i.activeSelf) ++c;
			}
			return c;
		} 
	}

	public bool extinct { get { return population <= 0; } }

	#endregion

	#region TCells

	/// <summary> The TCell Base Prefab </summary>
	[Header("TCell")]
	[SerializeField]
	private GameObject _tCell;

	/// <summary> The intended radius of the tCell </summary>
	[SerializeField]
	private float _tCellRadius;

	[SerializeField]
	private float _tCellRadiusMin;

	[SerializeField]
	private float _tCellRadiusMax;


	/// <summary> The spawn boarder ot the TCell. </summary>
	[SerializeField]
	private float _spawnBorder;

	/// <summary> The area that TCells can spawn. They spawn on the borders of the rectangle </summary>
	private Rect _tCellSpawn { get { return new Rect(1 - _spawnBorder, 1 - _spawnBorder, _spawnBorder * 2 - 1, _spawnBorder * 2 - 1); } }

	/// <summary> The area where TCells can be disabled when outside </summary>
	public Rect _tCellDisable
	{
		get {
			var b = _spawnBorder + 0.25f;
			return new Rect(1 - b, 1 - b, b * 2 - 1, b * 2 - 1);
		}
	}

	/// <summary> The maximum population of TCells </summary>
	[Range(5, 50)][SerializeField]
	private int _tCellPopMax;

	/// <summary> The list of TCell Instances </summary>
	private List<GameObject> _tCellList;

	/// <summary> Encapsulated. The list of TCell Instances. Get only. </summary>
	public List<GameObject> tCellList { get { return _tCellList; } }

	/// <summary> The list of TCell Instance Scripts </summary>
	private List<TCell> _tCellScripts;

	/// <summary> Encapsulated. The list of TCell Scripts. Get only </summary>
	public List<TCell> tCellScripts { get { return _tCellScripts; } }

	#endregion TCells

	#region RBC

	[SerializeField] GameObject redBloodCell;
	private List<GameObject> _rbcList;

	public List<GameObject> redBloodCellList { get { return _rbcList; } }

	private List<RedBloodCell> _rbcScripts;

	/// <summary> The maximum population of Red Blood Cells
	[Range(5, 50)][SerializeField]
	private int _rbcPopMax;

	float rbcTimer; //the timer to spawn red blood cells

	#endregion

	#region Antibodies

	[Header("Enemies")]
	[SerializeField] AntibodyShooter _antibodyShooter;
	List<Antibody> _antibodyList = new List<Antibody>();

	#endregion Antibodies

	#region Statistics
	[Header("Statistics")]
	/// <summary> The current score. </summary>
	public int score = 0;

	/// <summary> The current level number. </summary>
	[SerializeField] public int level = 0;

	#endregion Statistics

	#region Enemies

	[Header("B cells")]
	[SerializeField] GameObject bCellObject;
	[SerializeField] TimerList bCellTimer;

	[Header("Other")]
	[SerializeField] GameObject warning;
	[SerializeField] float battleTimer;
	#endregion

	/// <summary>
	/// The game instance to initialize upon awaking.
	/// </summary>
	public static Game main;

	/// <summary>
	/// Gets the controller.
	/// </summary>
	/// <value>The controller object.</value>
	public GameController controller { get { return GetComponent<GameController>(); } }

	#endregion Variables

	#region Methods

	#region Create HIV

	/// <summary>
	/// Prepares a list of HIV game objects to spawn.
	/// </summary>
	/// <param name="instantiate">whether you want to instantiate a basic position</param>
	void CreateHIVList(bool instantiate = false)
	{
		_hivList = new List<GameObject>();
		_hivScripts = new List<HIV>();
		if(instantiate) CreateHIV(hivSpawn, Quaternion.identity);
	}


	/// <summary>
	/// Create an HIV instance at a location
	/// </summary>
	/// <param name="position">starting position</param>
	/// <param name="rotation">starting rotation</param>
	/// <returns>Returns the instantiated HIV</returns>
	public GameObject CreateHIV(Vector3 position, Quaternion rotation)
	{
		Color CAPSID = Color.green;
		Color MATRIX = Color.red;
		Color MEMBRANE = Color.yellow;
		Color RNA = Color.black;
		Color RT = Color.blue;


		GameObject s = null;

		for(var i = 0; i < _hivList.Count; ++i)
		{
			GameObject g = _hivList[i];
			if(!g.activeSelf)
			{
				g.SetActive(true);
				s = g;
				var a = s.GetComponent<HIV>();
				a.radius = hivRadius;
				a.ChangeType(MATRIX, CAPSID, MEMBRANE, RNA, RT, Tropism.HIVRandom());
				s.transform.position = position;
				return s;
			}
		}
		if(s == null)
		{
			if(_hivList.Count > 0 && _hivList.Count >= populationMax) return null;
			_hivList.Add(Instantiate(hiv, position, rotation) as GameObject);
			_hivScripts.Add(Functions.GetLast(_hivList).GetComponent<HIV>());
			s = Functions.GetLast(_hivList);
		}
		var b = s.GetComponent<HIV>();
		b.Instantiate(hivRadius);
		b.ChangeType(MATRIX, CAPSID, MEMBRANE, RNA, RT, Tropism.HIVRandom());
		return s;
	}

	#endregion Create HIV

	#region Create TCell

	/// <summary>
	/// Prepares a list of TCell game objects to spawn.
	/// </summary>
	/// <param name="instantiate">Whether or not to instanitate one at the start</param>
	void CreateTCellList(bool instantiate = false)
	{
		_tCellList = new List<GameObject>();
		_tCellScripts = new List<TCell>();
		if(instantiate) CreateTCell();
	}

	/// <summary>  Creates a TCell instance at a location. No arguments. </summary>
	/// <returns>Returns the Instantiated TCell.</returns>
	public GameObject CreateTCell( )
	{
		return CreateTCell(Quaternion.identity);
	}

	/// <summary>
	/// Creates a TCell instance at a location with a given velocity
	/// </summary>
	/// <param name="rotation">starting rotation</param>
	/// <param name="speed">speed to shoot towards</param>
	/// <returns>Returns the instantiated TCell</returns>
	public GameObject CreateTCell(Quaternion rotation, float speed = 9f, Vector3 pos = new Vector3())
	{
		//TODO: change this later
		Color CYTOPLASM_COLOR = new Color(0.4f, 0.4f, 1f);
		Color MEMBRANE_COLOR = Color.blue;
		Color NUCLEOUS_COLOR = new Color(0.2f, 0.7f, 0.2f);

		//set the position of the tcell
		Vector3 position;
		if(pos == new Vector3()) position = Functions.RandomBorder(Functions.CameraToWorldRect(Camera.main, _tCellSpawn));
		else position = pos;

		foreach(var g in _tCellList)
		{
			if(!g.activeSelf)
			{
				g.SetActive(true); //activate the instance
				var a = g.GetComponent<TCell>(); //get its tcell component

				a.infected = false;
				a.radius = Random.Range(_tCellRadiusMin, _tCellRadiusMax);
				a.ChangeType(CYTOPLASM_COLOR, MEMBRANE_COLOR, NUCLEOUS_COLOR);
				a.transform.position = position;
				if(a.move) a.move.ShootTowards(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), speed);
				return g;
			}
		}
		if(_tCellList.Count < _tCellPopMax)
		{
			GameObject g = Instantiate(_tCell, position, rotation) as GameObject;
			_tCellList.Add(g);

			var s = g.GetComponent<TCell>();
			_tCellScripts.Add(s);


			s.Instantiate(Random.Range(_tCellRadiusMin, _tCellRadiusMax));
			s.ChangeType(CYTOPLASM_COLOR, MEMBRANE_COLOR, NUCLEOUS_COLOR);
			s.infected = false;
			if(s.move != null) s.move.ShootTowards(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), speed);
			return g;
		}

		return null;
	}

	#endregion Create TCell

	#region Create RBC

	/// <summary>
	/// Prepares a list of TCell game objects to spawn.
	/// </summary>
	/// <param name="instantiate">Whether or not to instanitate one at the start</param>
	void CreateRBCList(bool instantiate = false)
	{
		_rbcList = new List<GameObject>();
		_rbcScripts = new List<RedBloodCell>();
		if(instantiate) CreateRBC();
	}

	/// <summary> Creates a Red Blood Cell instance by default. No arguments. </summary>
	public GameObject CreateRBC( )
	{
		return CreateRBC(Quaternion.identity);
	}

	/// <summary>
	/// Creates a Red Blood Cell instance at a location with a given velocity
	/// </summary>
	/// <param name="rotation">starting rotation</param>
	/// <param name="speed">speed to shoot towards</param>
	/// <param name="pos">starting position of the red blood cell</param>
	/// <returns>Returns the instantiated Red Blood Cell</returns>
	public GameObject CreateRBC(Quaternion rotation, float speed = 18f, Vector3 pos = new Vector3())
	{
		const float RBC_RESIZE_SCALE = 0.5f; //the scale of the red blood cell in comparison to the T-Cell

		Vector3 position; //the intended position of the 
		if(pos == new Vector3()) position = Functions.RandomBorder(Functions.CameraToWorldRect(Camera.main, _tCellSpawn));
		else position = pos;



		foreach(var g in _rbcList)
		{
			if(!g.activeSelf)
			{
				g.SetActive(true);

				var a = g.GetComponent<RedBloodCell>();
				a.radius = Random.Range(_tCellRadiusMin * RBC_RESIZE_SCALE, _tCellRadiusMax * RBC_RESIZE_SCALE);
				a.transform.position = position;
				a.move.ShootTowards(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), speed); //go towards the middle of the screen 
				return g;
			}
		}
		if(_rbcList.Count < _rbcPopMax)
		{
			GameObject g = Instantiate(redBloodCell, position, rotation) as GameObject;
			_rbcList.Add(g);
			RedBloodCell script = g.GetComponent<RedBloodCell>();
			_rbcScripts.Add(script);

			g.GetComponent<CellMovement>().ShootTowards(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f)), speed);
			script.radius = Random.Range(_tCellRadiusMin * RBC_RESIZE_SCALE, _tCellRadiusMax * RBC_RESIZE_SCALE);
			return g;
		}

		return null;
	}

	#endregion Create TCell

	/// <summary> Calculate and Level Up the current game process. </summary>
	public void LevelUp()
	{
		level += 1;
		score += LevelGain(level - 1);
		ScoreDisplay.main.ShowScore (Camera.main.transform.position, LevelGain (level - 1), 2f, "Level Up! ");
		warning.SetActive (true);
		bCellObject.SetActive (false);

		//adjust the B cell timers every level based on calculations
		if (bCellTimer) {
			if(bCellTimer["vanish"] != null)
				bCellTimer ["vanish"].duration = WaveDurationPerLevel (level);
			if(bCellTimer["shoot"] != null)
				bCellTimer ["shoot"].duration = ShooterDurationPerLevel (level);
		}
		_antibodyShooter.gameObject.SetActive (false);
		_timers["Shoot"].duration *= 0.75f;
		Invoke("Battle", battleTimer);
		UI_HIV_Button.main.Activate ();
	}

	void Battle()
	{
		warning.SetActive (false);
		_antibodyShooter.gameObject.SetActive (true);
		bCellObject.SetActive (true);
	}

	/// <summary>
	/// Checks the amount of score gained on a specified level
	/// </summary>
	/// <returns>The amount of score gained.</returns>
	/// <param name="level">the given level.</param>
	public static int LevelGain(int level)
	{
		int b = (int)( 50f * Mathf.Pow(1.5f, level / 1.5f));

		b -= b % 10;

		return b;
	}

	/// <summary> Calculate the amount of time between shots depending on the level. </summary>
	/// <returns>The duration on the given level.</returns>
	/// <param name="level">level.</param>
	public float ShooterDurationPerLevel(int level)
	{
		level -= 1;
		float bullets_per_second = level * level - 2 * level + 5;
		return 1f / bullets_per_second;
	}

	/// <summary> Calculate the wave time depending on the level </summary>
	/// <returns>The wave time on the given level.</returns>
	/// <param name="level">level.</param>
	public float WaveDurationPerLevel(int level)
	{
		float a = 8.5f, b = -42f, c = 5f;
		return a + (b / (level + c));
	}

	/// <summary> Does calculations every time the player gets a game over. </summary>
	public void EndGame( )
	{
		if(HighScores.main != null)
		{
			if(HighScores.main.Insert(new Score(score)) != -1) HighScores.main.Save();
		}
		else
		{
			HighScores.main = new HighScores();
			if(HighScores.main.Load())
			{
				if(HighScores.main.Insert(new Score(score)) != -1) HighScores.main.Save();
			}
			else
			{
				HighScores.main.Insert(new Score(score));
				HighScores.main.Save();
			}
		}
	}

	#region Monodevelop

	protected void Awake( )
	{
		main = this; //have this be the game of choice
		_timers = GetComponent<TimerList>();
	}

	/// <summary>
	/// Initializes the instance.
	/// </summary>
	void Start( )
	{
		score = 0;
		level = 1;

		CreateHIVList(false);
		CreateHIV(new Vector3(-25f, 0f, 0f), Quaternion.identity);
		FocalPoint.main.transform.position = new Vector3 (-25, 0f, 0f);
		FocalPoint.main.move.velocity = Vector2.right;

		CreateTCellList(false);
		CreateTCell(Quaternion.identity, 9f, new Vector3(0f, 20f)).GetComponent<Rigidbody2D>().velocity = new Vector2();
		CreateTCell(Quaternion.identity, 9f, new Vector3(25f, -20f)).GetComponent<Rigidbody2D>().velocity = new Vector2();

		for (var i = 0; i < 2; ++i) {
			tCellScripts [i].move.SetVelocity (FocalPoint.main.move.velocity.normalized * (FocalPoint.main.speed * Random.Range(0.25f, 0.5f)));
		}
		
		for(var i = 2; i < 4; ++i)
		{
			CreateTCell(Quaternion.identity);
		}
		rbcTimer = 2f; // set the timer to 2 seconds
		CreateRBCList(false);
	}

	void Update( )
	{

		rbcTimer -= Time.deltaTime;
		if(rbcTimer < 0) //reset timer and spawn a red blood cell when the time ris up
		{
			rbcTimer = 2f;
			CreateRBC(Quaternion.identity);
		}
	}

	void OnTimerEnd (Timer t)
	{
		switch (t.name) {
		case "Shoot":
			if (level > 1)
				_antibodyList.Add (_antibodyShooter.Shoot ());
			break;
		}
	}

	#endregion Monodevelop

	#endregion Methods
}
#endregion

[System.Serializable]
struct GameItem
{
	[SerializeField] GameObject itemReference;
	[SerializeField] int _maximumNumber;

	private List<GameObject> _itemList;

	public List<GameObject> itemList { get { return _itemList; } }

	public int population { get { return _itemList.Count; } }

	public int populationMax { get { return _maximumNumber; } }

	public GameObject item { get { return itemReference; } }

	public void Initialize(bool instantiate = false)
	{
		_itemList = new List<GameObject>();
	}
}