﻿using UnityEngine;


/*
 *  Name:           SwipeController.cs
 *  Author:         Vincent Mills
 *  Version:        0.1.0 (Added a starting object for the swipe. Not needed as of now)
 *  Last Modified:  26 October 2016
 */

/// <summary> The Swipe Control Script </summary>
public class SwipeController : MonoBehaviour
{
	#region Variables

	public Controller controller { get { return GameObject.FindGameObjectWithTag("Controller").GetComponent<Controller>(); } }

	/// <summary> The Swipe Tolerance of the swipe controller </summary>
	public float _swipeTolerance = 20;

	/// <summary> The Time delay of the swipe controller </summary>
	public float _timeDelay = 0.5f;

	/// <summary> The Initial position when pressing the button. Vector3.zero when never used. </summary>
	Vector3 _initialPosition = Vector3.zero;
    
	/// <summary> The distance in x when swiping. </summary>
	float _dx = 0;

	/// <summary> The distance in y when swiping. </summary>
	float _dy = 0;

	/// <summary> The distance travelled on a swipe. </summary>
	public float Distance { get { return new Vector3(_dx, _dy).magnitude; } }

	/// <summary> The change of time when swiping. </summary>
	float _dt = 0;

	/// <summary> Encapsulated. The change of time when swiping. Get only. </summary>
	public float DeltaTime { get { return _dt; } }

	/// <summary> The swipe velocity of the last/current swipe </summary>
	Vector3 swipeVelocity;

	/// <summary> Encapsulted. The swipe velocity of the last/current swipe </summary>
	public Vector3 SwipeVelocity { get { return swipeVelocity; } }

	/// <summary> The Direction that the swipe is going towards </summary>
	public Vector3 SwipeDirection { get { return SwipeVelocity.normalized; } }

	private GameObject _initialObject;

	public GameObject InitialObject { get { return _initialObject; } }

	#endregion Variables

	#region Methods

	#region Swipe Controls

	/// <summary> Start the Swipe once clicked. </summary>
	void StartSwipe( )
	{
		if(controller.getSelected() != null) _initialObject = controller.getSelected().gameObject;

		_initialPosition = controller.screenPosition;
		updateTime(true);
		swipeVelocity = Vector3.zero;
	}

	/// <summary> Update swipe if held down </summary>
	void UpdateSwipe( )
	{
		_dx = controller.screenPosition.x - _initialPosition.x;
		_dy = controller.screenPosition.y - _initialPosition.y;
		updateTime();
		CalculateVelocity();
	}

	/// <summary> Calculate the swipe Velocity </summary>
	void CalculateVelocity( )
	{
		if(_dt > Time.unscaledDeltaTime) swipeVelocity = new Vector3(_dx, _dy) / _dt;
		else swipeVelocity = new Vector3();
	}

	/// <summary> updates the change in time or resets it. </summary>
	/// <param name="reset">whether or not the user wants to reset the time</param>
	void updateTime(bool reset = false)
	{
		_dt = reset ? 0f : _dt + Time.unscaledDeltaTime;
	}

	/// <summary> Release the swipe. </summary>
	void Release( )
	{
		CalculateVelocity();
		_initialPosition = Vector3.zero;
	}

	/// <summary> Resets the swipe calculator </summary>
	void Reset( )
	{
		_dx = 0;
		_dy = 0;
		_initialPosition = Vector3.zero;
		_initialObject = null;
	}

	#endregion Swipe Controls

	#region Monodevelop

	void Update( )
	{
		if(controller.LeftClick()) StartSwipe();
		else if(controller.LeftDrag()) UpdateSwipe();
		else if(controller.LeftRelease()) Release();
		else
		{
			Reset();
			updateTime(true);
		}
	}

	#endregion Monodevelop

	#endregion Methods
}
