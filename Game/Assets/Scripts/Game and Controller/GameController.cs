﻿using UnityEngine;

/*
 *  Name:           GameController.cs
 *  Author:         Vincent Mills
 *  Version:        1.0.0 (Release)
 *  Last Modified:  29 March 2017
 */

/// <summary> The script that handles what happens with Game Control input. </summary>
public class GameController : MonoBehaviour
{
	/// <summary> The type of swipe to take in </summary>
	enum SwipeType
	{
		LEFT,
		RIGHT
	}

	#region Varables

	/// <summary> The selected HIV for use of controls. </summary>
	HIV _selected;

	/// <summary> Encapsulated. The selected HIV for use of controls. Get Only. </summary>
	/// <value> The selected HIV object. </value>
	public HIV selected
	{ 
		get { return _selected; }
		private set
		{
			if(_selected == value) return;

			if(_selected)
			{
				if(_selected.state == HIV.State.SELECTED) _selected.ChangeState(HIV.State.WANDER);
				_selected.selected = false;
			}

			if(value)
			{
				value.selected = true;
				value.ChangeState(HIV.State.SELECTED);
			}

			_selected = value;

		}
	}

	#endregion Variables

	#region Methods

	#region Input

	#region Left Mouse (1 finger)

	/// <summary> Function to handle lect clicking. </summary>
	void OnLeftClick( )
	{
		Transform t = Controller.main.getSelected(); //checks the scene for clicked objects

		if(t)
		{
			if(t.gameObject.GetComponent<HIV>())
			{
				var s = t.gameObject.GetComponent<HIV>(); //find the HIV that you clicked
				if(s.state == HIV.State.WANDER || s.state == HIV.State.RETURN)
				{
					selected = s; //toggle your selection on if you clicked something not selected
				}
			}
		}
	}

	/// <summary> Checks when handling left dragging </summary>
	void OnLeftDrag( )
	{
		if(selected) selected.GetComponent<HIV_Selected>().destination = (Vector2)(Controller.main.worldPosition);
	}

	/// <summary> Function when handling when the user releases the left area </summary>
	void OnLeftRelease( )
	{
		//deselect the HIV when you let go and be avaiable for swiping again.
		selected = null; 
	}

	#endregion

	#region Right Mouse (2 fingers)

	/// <summary>
	/// Function to handle when right clicking
	/// </summary>
	void OnRightClick( )
	{
	}

	/// <summary>
	/// Function tohandle right dragging
	/// </summary>
	void OnRightDrag( )
	{
	}

	/// <summary> Function to handle right releasing </summary>
	void OnRightRelease( )
	{
	}

	#endregion

	#region Swipe

	void OnSwipe(SwipeType s)
	{
		switch(s)
		{
			case SwipeType.LEFT:
				break;
			case SwipeType.RIGHT:
				break;
			default:
				break;
		}
	}

	#endregion Swipe

	#endregion Input

	#region Monodevelop

	// Update is called once per frame
	void Update( )
	{
		#region Input

		if(Time.timeScale != 0) //don't do these things when paused
		{
			if(selected) if(!(selected.state == HIV.State.SELECTED) || selected.isActiveAndEnabled == false) selected = null;

			if(Controller.main.LeftClick()) OnLeftClick();
			else if(Controller.main.LeftDrag()) OnLeftDrag();
			else if(Controller.main.LeftRelease()) OnLeftRelease();

			#if false
			//activate this only when you have something for right clicking and dragging
			if(Controller.main.RightClick()) OnRightClick();
			else if(Controller.main.RightDrag()) OnRightDrag();
			else if(Controller.main.RightRelease()) OnRightRelease();
			#endif
		}

		
		#endregion Input
	}

	#endregion Monodevelop

	#endregion Methods
}
