﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 	Name:			FollowCam.cs
 * 	Author:			Vincent Mills
 * 	Version:		0.0.1A (FollowCam now requires both Camera and Rigidbody)
 * 	Last Modified:	20 January 2017
 */


/// <summary>
/// Follow Camera Class
/// 	- Follows an object with a rigidbody2D component
/// 	- Moves with the object when the object reaches outside the border rectangle
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Camera))]
public class FollowCam : MonoBehaviour
{
	
	#region Variables

	[SerializeField]Transform follow;
	Rigidbody2D followRigidbody;

	[SerializeField]Rect border;

	Camera cam;
	Rigidbody2D rb;

	#endregion

	#region Functions

	// Use this for initialization
	void Awake( )
	{
		cam = GetComponent<Camera>();
		rb = GetComponent<Rigidbody2D>();
		if(!(follow && follow.GetComponent<Rigidbody2D>())) enabled = false;
		else followRigidbody = follow.GetComponent<Rigidbody2D>();
	}

		
	// Update is called once per frame
	void FixedUpdate( )
	{
		Rect worldBorder = Functions.CameraToWorldRect(cam, border);

		Vector2 velocity = new Vector2(0f, 0f);

		if((followRigidbody.velocity.x > 0 && follow.position.x > worldBorder.xMax) || (followRigidbody.velocity.x < 0 && (follow.position.x < worldBorder.xMin))) velocity.x = followRigidbody.velocity.x;

		if((followRigidbody.velocity.y > 0 && follow.position.y > worldBorder.yMax) || (followRigidbody.velocity.y < 0 && follow.position.y < worldBorder.yMin)) velocity.y = followRigidbody.velocity.y;

		rb.velocity = velocity;
	}

	#endregion

}
