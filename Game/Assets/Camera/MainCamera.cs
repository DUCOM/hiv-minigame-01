﻿using UnityEngine;
using System.Collections;

/*
 * 	Name: 			MainCamera.cs
 * 	Author: 		Vincent Millls
 * 	Version: 		0.0.3 (changed a single variable name (rigidbody). Deleted Start and Update.)
 * 	Last Modified:	10 January 2017
 * 
 */

public class MainCamera : MonoBehaviour
{
	#region Variables

	#region Properties

	[SerializeField] private float _speed;


	/// <summary>
	/// The speed of the object
	/// </summary>
	/// <value>The maximum speed of the camera</value>
	public float speed { get { return _speed; } }

	public Rigidbody2D rb { get { return gameObject.GetComponent<Rigidbody2D>(); } }

	#endregion Properties

	#endregion Variables

	#region Methods

	/// <summary>
	/// Changes the direction of the camera. Also moves the camera
	/// </summary>
	/// <param name="d">The Vecotr direction to move to</param>
	public void ChangeDirection(Vector3 d)
	{
		rb.velocity = d.normalized * speed;
	}

	public void Stop( )
	{
		rb.velocity = Vector3.zero;
	}

	#region MonoDevelop

	#endregion

	#endregion Methods
}

