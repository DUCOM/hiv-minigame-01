﻿using UnityEngine;
using System.Collections;

/*
 *  Name:           RenderCamera.cs
 *  Author:         Vincent Mills
 *  Version:        0.0.1B (More organization)
 *  Last Modified:  23 January 2017
 */


/// <summary>
/// RenderCamera class:
/// 	TODO: make a description for the RenderCamera class
/// </summary>
public class RenderCamera : MonoBehaviour
{
	#region Variables

	#region Static Variables

	public static Camera main;

	#endregion

	#region Constants

	public const float ZOOM_BASE = 40f;

	#endregion

	#region Properties

	[Range(1, 10)]  public float zoom = 1;

	public Vector3 difference = new Vector3();

	#endregion

	#region Components

	public Camera cam;

	#endregion

	public Vector3 origin { get { return GameObject.FindGameObjectWithTag("MainCamera").transform.position; } }

	#endregion Variables

	#region Functions

	public float GetZoomHeight(float zoom)
	{
		if(zoom < 1f) zoom = 1f;
		return ZOOM_BASE / zoom;
	}

	void Move( )
	{
		transform.position = origin + difference;
	}

	public void Zoom(float f)
	{
		if(f < 1f) f = 1f;
		cam.orthographicSize = ZOOM_BASE / f;
	}

	#region MonoDevelop

	void Awake( )
	{
		cam = GetComponent<Camera>();
		main = cam;
	}

	void FixedUpdate( )
	{
		Move();
	}

	#endregion

	#endregion
}
